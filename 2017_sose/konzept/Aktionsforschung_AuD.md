# Was ich schon weiß

* Gildamesh und Dozentron haben für den meisten Frust gesorgt.
* Die Veranstaltung wird vermutlich insgesamt schlechter bewertet als OOP, weil der Inhalt schwieriger ist.
* Das Konzept kam unter dem Strich nicht schlechter oder besser an als andere neue A&D-Konzepte.
* Gamification wird deutlich öfter positiv als negativ bewertet.

# Was ich noch wissen will

## Funktioniert Gamification?

Das ist die große Frage, mit der ich angetreten bin. Testen kann ich natürlich jetzt nur, ob diese Form der Gamification funktioniert hat.

Was ich mir erhofft habe:
* mehr Lernmotivation
* mehr eigenständige Recherche
* regelmäßigeres Auseinandersetzen mit dem Stoff
* mehr Stunden in die Veranstaltung investiert
* Stoff ist zugänglicher für Studierende
* Studierende behalten sich Inhalte besser/länger
* Bonusaufgaben fördern besonders gute Studierende, geben aber auch den schwächeren eine Chance punkte zu sammeln.
* Mehr Feedback/bessere Übersicht über Vorankommen der Studenten.

### Führt Gamification tatsächlich nur zu extrinsischer Motivation, oder fördert sie auch die intrinsische Motivation?

Das lässt sich eigentlich erst im nächsten Semester sagen, wenn man sieht ob die Studierenden dann auch Protokolle/Mitschriften machen. Man kann aber zumindest fragen, ob sie planen weiterhin Mitschriften zu machen oder ähnliches.

### Kann Gamification im laufenden Betrieb Studierende überzeugen, die zu Anfang noch skeptisch waren?

Z.b.: "Was dachtest du vorher über Gamification?" - "Wie bewertest du Gamification im Nachhinein?"

## Gibt es Gruppen, die die Aktion besonders gut oder besonders schlecht abgeholt hat?

* Frauen vs Männer?
* Wiederholer vs Ersthörer?
* Vorerfahrung im Programmieren
* Persönlichkeitseigenschaften
* OOP bestanden vs. OOP nicht bestanden
* Studiengang
* Spieler vs Nicht-Spieler
    - Spiele allgemein
    - Fantasy-Rollenspiele im speziellen

## Mit welchen Persönlichkeiten habe ich es eigentlich in der Informatik zu tun?

Das wäre hilfreich um zu wissen, in welche Richtung ich das Spielkonzept weiterentwickeln sollte.

* Big 5
* Spielertypen

## Was hätte ich besser machen können?

Mir ist wichtig: Die Studierenden dürfen träumen. Das ist ein großes Projekt und manchmal sind große Ideen ja auch schon im Kleinen umsetzbar.

* Wie würden sich die Studierenden die Idealform von Gildamesh/Dozentron vorstellen
    - Oder: Was wäre die ideale Alternative zu diesen Systemen?
* Was wäre die Idealform von Übungen?
* Was wäre die Idealform einer Vorlesung?

## Welche Fähigkeiten kamen gut an, welche würden sich die Studierenden noch wünschen?

Die Nutzungsstatistiken sagen hier natürlich schon etwas aus, aber da fehlen natürlich auch Informationen. Ich habe z.B. das Feedback bekommen, dass die meisten Skills uninteressant sind, weil sie schwer zu erreichen oder nicht so klausurrelevant sind und weil die Koordination der Gruppenskills nicht so gut funktioniert. Was ich aber nicht weiß ist, welche Skills die Studierenden stattdessen gerne gehabt hätten.
