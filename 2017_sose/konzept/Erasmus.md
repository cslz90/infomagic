# Vereinbarung für Erasmus-Studierende

## Prüfung

Mündliche Prüfung am Ende des Semesters

## Inhalt

Buch: Lafore, Data structures and algorithms in Java, 2nd Edition

Ausgenommen folgende Kapitel:
* 4 (Stacks and Queues)
* 9 (Red-Black Trees)
* 10 (2-3-4 Trees and External Storage)

## Betreuung

Ein Tutor (Alissia?) trifft sich wöchentlich mit den Studierenden, bespricht Fragen und wählt passende Übungsaufgaben aus dem Buch aus.