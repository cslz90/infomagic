//// Folgend wird unabhänig von der Gamification bezeichnung das wort SP als Questbelohnung verwendet.



# Gamification

  ### Leveling  

  Fällt weg da das derzeitige System keine Level unterstützt.


```
  ### Skills   
  Es wird erwartet das der Durschnittliche Student am ende des Semesters 1.170 SP/SP angesammelt hat.
  Diese Zahl errechnet sich aus 6 Hausübungen â 4std bei 50% Belohnung folglich ((4*60*6)/2) 180 SP und 66std Heimarbeit bewertet mit 1SP in 4min folglich 990SP.

  Die SP sind so gelegt das ein Student der die nach Modulhandbuch üblichen Stunden gearbeitet hat am ende des Semesters 2 Tier III Skills erwerben.
  Bonus quellen wie Notizen ect. wurden nicht beachtet um sie wirklich als Belohnung und nciht als kalkuliertes Income zu sehen.

  ### Klausuren Punkte
  Aufschlüsselung der 20 Klausuren Punkte.

  --Work in Progresse--
    Die Grobe aufteilung wird 1% pro Bestandene Hausarbeit sein das bedeutet innerhalb von 180SP 7%.
    Dannach wird zwischen 180 und 1.170 SP 13 weitere % geben.
    Eine Liste folgt.

  ##### Skill Tree´s
  ```
        Die verschiedenen Skills sind in verschiedene Sub-trees eingeteilt.  
        In anbetracht des Open World aspektes wurde hier geteilt zwischen Wegen und Schulen.
        Die Fünf Schulen sind (Arbeitstitel):
          Weg der Geseligkeit: Fesmahl herbeirufen...
          Weg des Wissens: Zauberstab...
          Schule des Fluches: Fluch der Logorrhö ...
          Schule der Täuschung: Post-It des Herumtreibers...
          Schule der Wiederherstellung: Selbstheilung...

  ```
  ##### Gruppen Skills
  ```
Jeder Gruppen Skill sollte nur einmal innerhalb des Kurses durchsetzbar sein, Ein Thema aus der Klausur lassen ist akzSPtabel doch 3-4 währe etwas so gravierend.
In anlehnung an den Vorschlag seitens KiM bin ich für eine änderung des Gruppenskill Systems.
Anstatt eines Skills der von n Studenten aktiviert werden muss sollte am ende des Entsprechenden Baumes lediglich ein Skill sein der zur Teilnahme am Brauen des Trankes berechtigt.
Das Brauen an sich findet mit dem Abgeben überschüssiger SP statt.
So können Studierende so viel arbeiten wie sie wünschen um ihren Skill durchzusetzen was folglich viel areit belohnt da dadurch mehr in den Trank gesetzt werden kann. Die Gnade des Goldgorgonen wird hierdurch wie angesetzt nur in Notfällen, notwending sein um ggf einen unerfüllten Trank am ende des Semesters durchzuwinken trotz der Aktivierung eines anderen.
  ```
  ##### Tier-Klassen
  ```    
        Tier 0    - einfache Magie -   25 SP    
        Tier I    -  normale Magie -   50 SP   
        Tier II   -  größere Magie -  100 SP
        Tier III  - mächtige Magie -  250 SP  
 ```
 ##### Tier-Einteilung
 ```
        Tier 0    - einfache Magie:
                                    Fluch der Logorrhö,
                                    Post-It des Herumtreibers,
                                    Zauberstab,
                                    Fesmahl herbeirufen35,
                                    Selbstheilung,

        Tier I    -  normale Magie:
                                    Klausurvisionen,
                                    Karteikarte des Herumtreibers,
                                    Fluch der Albernheit,
                                    Beschwörung eines Gamification-Djinn,
                                    Robe des Erfolgs,
                                    Trank der Redseligkeit,
                                    Bonbonregen,
                                    Bezauberung,
                                    Heilung anderer,

        Tier II   -  größere Magie:
                                    Erinnerungen löschen,
                                    Gedankenkontrolle,
                                    Notizzettel des Herumtreibers,
                                    Fluch des vorzeitigen Endes,
                                    Beschwörung eines großen Djinn,
                                    Beschwörung eines Musterlösungskobolds,
                                    Trank der übersprühenden Kreativität,
                                    Majestätische Aura,
                                    Wiederbelebung,
                                    Schutzzauber,

        Tier III  - mächtige Magie:
                                    Wahrheitsserum,
                                    Gestaltwandel,
                                    Karte des Herumtreibers,
                                    Blick in die Zukunft,
                                    Trank des Vergessens,
                                    Phönix,

 ```
