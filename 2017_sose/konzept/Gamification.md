# Setting
THM =  Thaumaturgische Hochschule der Magie

## Entsprechungen

### Algorithmen = Zauber

- Brute Force = Polymorph in einen Ork (einen "Brute")
-- Standardgegner, bei dem die Lösung bekannt ist
-- bei einem Drachen eine gute Idee
-- bei einem Kaninchen doof, aber ok
-- bei einer Horde von 100 Kaninchen eher suizdal
- Backtracking = Zeitmanipulation
-- Man probiert etwas aus bis es schief geht und dreht dann die Zeit zurück
- Gier = Ortungszauber
-- funktionieren wie Wünschelruten
-- Man weiß nicht wie der richtige weg ist, aber man weiß ob eine abzweigung näher oder weiter weg vom ziel führt
- Teile-und-Herrsche = Beschwörungen
-- Man erschafft kleine Helferlein, die einem die Teilprobleme lösen
- dynamische Programmierung = Gedankenlesen
-- Jeder Magier löst nur eine kleine Teilaufgabe, aber jeder kann alle anderen fragen

### Datenstrukturen = Kreaturen

Listen = Basilisten
  - Verkettete Basilisten fressen andere Basilisten
  - Array-Basilisten haben ganz viele Köpfe
Bäume = Baumdrachen/-hydren (mit mehreren Köpfen)
Graphen = Blutgraphen (mit Telepathie, Blutsbund)

### Programmiersprachen = Länder

- Java-Insel
- Haskellland

# Mechaniken

## Belohnenswerte Dinge

### Praxis

- 10 XP: Invarianten
*Bei Ihren fleißigen magischen Recherchen sind Sie sicher schon auf die Immerwahrs gestoßen: Klein, buntblühend und sehr empfindlich gegenüber fehlerhafter Magie. Zweimal im Jahr florieren sie und erreichen ihre größte Ausbreitung interessanterweise immer am Ende des Semesters. Dabei ernährt sich jedes kleine Immerwahr von seinem eigenen Zauber. Wenn Sie also den passenden Zauber für ein Immerwahr wirken, erblüht es in schönster Form und Farbe. Dazu müssen sie aber natürlich das richtige Immerwahr für ihren Zauber entdecken!*
Stellen Sie eine sinnvolle Schleifen- oder Klasseninvariante auf für Algorithmen bzw. Klassen aus der Vorlesung. Laden Sie Ihre Abgabe in [Moodle](https://moodle.thm.de/mod/assign/view.php?id=162068) hoch.
- 20 XP + Badge: Gültigkeitsbeweis einer Invarianten
*Bei Ihren fleißigen magischen Recherchen sind Sie sicher schon auf die Immerwahrs gestoßen: Klein, buntblühend und sehr empfindlich gegenüber fehlerhafter Magie. Zweimal im Jahr florieren sie und erreichen ihre größte Ausbreitung interessanterweise immer am Ende des Semesters. Dabei ernährt sich jedes kleine Immerwahr von seinem eigenen Zauber. Wenn Sie also den passenden Zauber für ein Immerwahr wirken, erblüht es in schönster Form und Farbe. Dazu müssen sie aber natürlich das richtige Immerwahr für ihren Zauber entdecken! Können Sie außerdem physiologisch erklären, warum das kleine Immerwahr auf diesen speziellen Zauber reagiert?*
Stellen Sie eine sinnvolle Schleifen- oder Klasseninvariante auf für Algorithmen bzw. Klassen aus der Vorlesung. Entwickeln Sie zusätzlich einen Beweis ihrer Gültigkeit. Laden Sie Ihre Abgabe in [Moodle](https://moodle.thm.de/mod/assign/view.php?id=162068) hoch.
- (!) 10 XP: Pseudocode eines nicht besprochenen Algorithmus
*Nur wer wagt, der auch gewinnt! Ein mutiger Novize geht nicht nur ausgetrampelte Pfade, sondern betritt Neuland! Versuchen Sie sich an der Skizzierung eines neuen, noch nicht im Kurs besprochenen Zaubers!*
Schreiben Sie einen Pseudocode zu einem **nicht** in der Veranstaltung besprochenen Algorithmus. Dies muss in eigenen Worten und nach der Pseudocode-Konvention der Vorlesung erfolgen. Laden Sie den Code dann in [Moodle](https://moodle.thm.de/mod/assign/view.php?id=161586) hoch.
- (!) 20 XP + Badge: Implementierung eines nicht besprochenen Algorithmus
*Nur wer wagt, der auch gewinnt! Ein mutiger Novize geht nicht nur ausgetrampelte Pfade, sondern betritt Neuland! Versuchen Sie sich an der vollständigen Entwicklung eines neuen, noch nicht im Kurs besprochenen Zaubers!*
Implementieren Sie einen **nicht** in der Veranstaltung besprochenen Algorithmus in einer beliebigen Sprache mit vollständiger Dokumentation. Laden Sie den Code dann in [Moodle](https://moodle.thm.de/mod/assign/view.php?id=161587) hoch.
- (!) 8 XP: Anwendungsbeispiel für einen Algorithmus/eine Datenstruktur
*Ein Zauber ist nur zu etwas nütze, wenn man auch weiß, wann und in welcher Situation man ihn anwenden kann. Sie haben bereits einen Fundus an Zaubern in ihrem magischen Folianten. Wie und wann kann man sie eigentlich anwenden?*
Nennen Sie einen Problemfall und erstellen Sie eine Lösungsskizze, die beschreibt, wie man mit einem Algorithmus/einer Datenstruktur aus der Vorlesung ein Problem der realen Welt lösen kann. Laden Sie Ihre Arbeit dann in [Moodle](https://moodle.thm.de/mod/assign/view.php?id=161588) hoch.
- (!) 20 XP: Implementierung eines Anwendungsbeispiels
*Ein Zauber ist nur zu etwas nütze, wenn man auch weiß, wann und in welcher Situation man ihn anwenden kann. Sie haben bereits einen Fundus an Zaubern in ihrem magischen Folianten und sich vielleicht schon überlegt, wie und wann man sie anwenden kann. Die Thaumaturgische Hochschule der Magie ist keine merkwürdige schottische Zaubererakademie, die ihren Schülern verbietet, die neu gelernten Zauber auch auszuprobieren. Suchen Sie sich einen Zauber aus, wenden Sie ihn auf ein reales Problem an und erleichtern Sie sich Ihren Alltag!*
Implementieren sie ein Anwendungsbeispiel eines in der Vorlesung gelernten Algorithmus'/einer Datenstruktur. Laden Sie Ihre Arbeit dann in [Moodle](https://moodle.thm.de/mod/assign/view.php?id=161589) hoch.
- (!) 30 XP + Badge: Korrektheitsbeweis für einen besprochenen Algorithmus
*Zauber sind ein kompliziertes Unterfangen. Noch komplizierter allerdings ist das Entwickeln solcher. Deshalb kann das Einsetzen eines neuen, bisher ungetesteten Zaubers sehr gefährlich werden. Wolfur der Blaue etwa ging in die Geschichte des Objektorients ein, als er die uralte Bibliothek in Brand setzte und damit auslöschte, anstatt sie wie gewünscht zu kopieren. Um dies zu verhindern, kann die korrekte Wirkung eines Zaubers zuvor analysiert werden. Führen Sie dies an einem von Ihnen gewählten Zauber durch!*
Führen Sie einen Korrektheitsbeweis an einem besprochenen Algorithmus aus. Laden Sie Ihre Arbeit dann in [Moodle](https://moodle.thm.de/mod/assign/view.php?id=161591) hoch.
- (!) 10 XP: Laufzeitanalyse für einen besprochenen Algorithmus
*Zauber sind ein kompliziertes Unterfangen. Noch komplizierter allerdings ist das Entwickeln solcher. Deshalb kann das Einsetzen eines neuen, bisher ungetesteten Zaubers sehr gefährlich werden. Nicht selten wurde ein unvorsichtiger Infomagier von dem gefürchteten Endloswirbel erfasst und in dessen immerwährenden Kreislauf gezogen. Selbst, wenn der Zauber nicht ganz so schief läuft, muss doch zuvor geprüft werden, ob sich die hervorgerufene Magie in akzeptabler Zeit wieder einstellen lässt. Analysieren Sie die Dauer eines von Ihnen gewählten Zaubers!*
Führen Sie eine Laufzeitanalyse für einen besprochenen Algorithmus durch (Tatsächlich/Durchschnittlich/Worst-Case/amortisiert). Laden Sie Ihre Arbeit dann in [Moodle](https://moodle.thm.de/mod/assign/view.php?id=161592) hoch.

### Theorie
- (!) 5 XP: Beschreibung eines Begriffs im Wiki
*Die [Enzyklopedia Algomantika](https://moodle.thm.de/mod/wiki/create.php?wid=680&group&uid=0&title=Algomantik%20und%20Datenkreaturen) galt stets als* ***das*** *Nachschlagewerk eines Infomagiers. Als jedoch Wolfur der Blaue versehentlich die uralte Bibliothek niederbrannte, ist es bedauerlicherweise zerstört worden. Helfen Sie mit, das unschätzbare Wissen wieder zusammenzutragen!*
Schreiben Sie einen Eintrag im Wiki zu einem in der Veranstaltung besprochenen Stoff.
- (!) 2 XP: Korrektur eines Eintrags im Wiki
*Jeder Infomagier macht mal Fehler, selbst die hervorragenden Novizen an der Thaumaturgischen Hochschule für Magie. Die [Enzyklopedia Algomantika](https://moodle.thm.de/mod/wiki/create.php?wid=680&group&uid=0&title=Algomantik%20und%20Datenkreaturen) soll jedoch wieder in ihrer alten Pracht erblühen, deswegen darf sie keine Fehler enthalten. Korrigieren Sie alle Mangelhaftigkeiten, die Sie darin finden sollten.*
- (!) 5 XP: Vorlesungsmitschrift
*Wer den berühmten Goldgorgonen kennt, weiß, er ist auch berüchtigt: Der* ***Confusio Professorii*** *Fluch hat ihn schon sehr früh getroffen. Deswegen ist es sehr hilfreich in seinen Veranstaltungen, Buch zu führen, um den Gedankensprüngen und begeisterten Anekdoten folgen zu können.*
Senden Sie ihre Vorlesungsmitschrift über [Moodle](https://moodle.thm.de/mod/assign/view.php?id=160755) ein.
- (!) 10 XP: Stundenprotokoll anfertigen
*Eine Mitschrift während der Vorträge des Goldgorgonen anzufertigen ist hilfreich, bereitet aber noch lange nicht auf die Abschlussprüfung vor. Dafür müssen die wirren Kritzeleien und Gedanken erst noch in Form gebracht werden, damit sie Ihnen später auch helfen können. Berühmt geworden für seine Stundenprotokolle, die er Pergament um Pergament anfertigte, ist Seidur der Blaue in jungen Jahren. Nach seinem Studium der Infomagie bewahrte er die Protokolle in der uralten Bibliothek auf.*
Schreiben Sie ein Stundenprotokoll, dass den Stoff der Vorlesung in geordneter, bestenfalls bebildeter Form zusammenfasst und geben Sie es in [Moodle](https://moodle.thm.de/mod/assign/view.php?id=160756) ab.
- (!) 2 XP: Anwesenheit in der Vorlesung/Übung (Anwesenheitscode)
***Dabei sein ist alles!*** *Dieser Muggle-Spruch trifft auf das Studium der Infomagie tatsächlich zu. Wer nicht anwesend ist, verpasst ausführliche Erklärungen, nützliche Hilfen und nicht zuletzt die spannenden Anekdoten des Goldgorgonen.*
Sie erhalten jedes Mal XP, wenn Sie in der Vorlesung oder Übung anwesend sind.
- (!) 1 XP: Inhaltliche Frage stellen
*Wer, wie, was?*
Stellen Sie in der Vorlesung, Übung oder im Forum eine inhaltliche Frage.
- (!) 1 XP: Inhaltliche Frage beantworten
*Der, die, das!*
Beantworten Sie in der Vorlesung, Übung oder im Forum eine inhaltliche Frage.
- (!) 3 XP: Vorstellen einer Aufgabe in der Übung
*Infomagister zu sein ist eine anstrengende, kräftezehrende Aufgabe. Selbst ein Goldgorgone braucht hin und wieder Pause. Unterstützen Sie Ihren Lieblingsinfomagister und stellen Sie in der Übung die Lösung einer Aufgabe vor.*

### Adeptenanwärter
- (!) 2 XP: einen inhaltlichen Fehler in Folien/Übungen gefunden
*Selbst ein Goldgorgone macht Fehler. Zum Glück jedoch ist er stets bemüht sich zu verbessern und dankbar, wenn Sie ihn auf Fehler hinweisen. Also suchen Sie!*
- Klausurfragen vorschlagen
- Feedback zur Veranstaltung geben
- Eigene Erklärung zu einem Konzept der Vorlesung schreiben
- (!) 20 XP: Aufgaben mit Musterlösung erstellen
*Das Lösen von Aufgaben bereitet Sie auf Ihre Abschlussprüfung vor. Einen tiefergreifenderen Einblick in die Materie der Infomagie erhalten sie jedoch, wenn Sie selbst Aufgaben kreieren und diese lösen. Die Konzeption einer eigenen Aufgabe lässt Sie exakter über das Zusammenwirken von Magie, Algomantik und Datenkreaturen nachdenken und führt zu einem besseren und nachhaltigen Verständnis.*
Schicken Sie Ihre Aufgabe mit Musterlösung per [Moodle](https://moodle.thm.de/mod/assign/view.php?id=160758) ein.

### Secrets
- Secret-Belohnung: Zur Sprechstunde kommen (Kuhlevel)

### Wie soll das in einer Aufgabe umgesetzt werden?
- Arbeitsblätter lösen
  - Verständnisfragen
  - Implementieren von vorgegebenen Algorithmen
  - Entwickeln eigener Algorithmen
- irgendwas zur Klausurvorbereitung
- Lernfragen (Multiple-Choice) in Moodle beantworten: Ist das noch aktuell? Wären das nicht einfach Bonusaufgaben?

## Mögliche Belohnungen

### Regeln für Gruppenskills
"Gruppenskill" bedeutet, dass der Skill an eine Bedingung der Form "Wenn mehr als X Studierende den Skill verwenden..." geknüpft ist.

Anmerkung: Eventuell ist die Regelung über "X% der aktiven Teilnehmer" bei Skills mit hohem X sinnvoller.

### Belohnungen für Herausragende Studenten
- Willkürlich am Ende der Vorlesung nochmal XP vergeben
  - vllt. auch ohne XP, einfach nur lobend erwähnen

### Heilungsfähigkeiten

- Selbstheilung (HP-Verlust durch verspätete Abgabe ausgleichen)
- Andere Studierende heilen
- Schadenslimiter (verspätete Aufgaben werden nach 5 Tagen automatisch abgehakt)
- Wiederbelebung (den bevorstehenden Tod eines Kommilitonen verhindern)
- Phönix (beim nächsten Tod wiederauferstehen)

### Trickser

- ein Tutor oder der Dozent fixt einen Bug in der eigenen Abgabe
- Abgabe eines Kommilitonen als eigene einreichen
- Gruppenskill: Dozent muss eine Klausuraufgabe vor der Klausur bekannt geben
- Gruppenskill: Dozent muss ein Thema aus der Klausur auslassen.
- Gruppenskill: Dozent muss Aufgabenarten bzw. Struktur der Klausur angeben

### Spickzettel

- Spickzettel für Klausur (verschiedene Themen/Größen)

### Socializer

- Getränke/Essen in der Vorlesung
- Hinweise vom Dozenten zu einer Übungsabgabe
- Frage an Tutoren stellen
- Gruppenskill: Dozent muss das Thema der letzten Vorlesung noch einmal wiederholen
- Gruppenskill: Dozent muss eine Frage zu Beginn der nächsten Stunde beantworten
- Gruppenskill: Dozent muss an jeden, der eine Frage stellt oder beantwortet Süßigkeiten verteilen
- Gruppenskill: Dozent muss eine Zusammenfassungsfolie (ein Skript) zu einem konkreten Thema erstellen
- Gruppenskill: Dozent muss eine Probeklausur erstellen.
- Gruppenskill: Musterlösung zu einem Programm hochladen.


### Disruptor

- Skillvorschlag machen
- Dozent muss irgendeinen Text in der nächsten Vorlesung vorlesen
- Gruppenskill: Dozent muss albernen Hut o.ä. tragen.
- Themenvorschlag für letzte Woche
- Gruppenskill: Vorlesung eine Woche früher beenden.

### Player

- Skill, der (feste) Bonus-XP gibt
- Bonus-XP pro badge
- Badges
  - Erster, der Level X erreicht hat
  - unter den ersten 10, die Level X erreicht haben
  - für besonders schwere Aufgaben
  - als erster Thema X im Wiki beschrieben
  - Yer a Programmer, Harry
- Schnellster Algorithmus für Problem X
- unter den 10 schnellsten Algorithmen für Problem X


### Allgemein

- Bonuspunkte für Klausur (relativ günstiger Skill, höhe der Bonuspunkte ans Level gekoppelt)
  - über Skilltree lösen?


## Punktesystem

* Fleißige Studenten arbeiten 8 h/Woche
  - Modulhandbuch sieht 90 Stunden Arbeit zu Hause vor
  - Semester hat 13 Wochen, A&D fällt aber in 3 davon aus
  - => zwischen 90/10 = 9 u. 90/13 ~= 7 Stunden Arbeit pro Woche
* Das Aufgabenblatt soll pro Woche maximal 4h kosten, also insgesamt 8h
  - ein fleißiger Student sollte maximal die Hälfte seiner Zeit am Aufgabenblatt sitzen
* Für freiwillige Aufgaben sind 4h pro Woche vorgesehen
* Eine freiwillige Aufgabe gibt ein XP pro 4 min Arbeit
* Ein Aufgabenblatt gibt 64 XP
  - ca. ein XP pro 8 min Arbeit
  - freiwillige Aufgaben sollen sich mehr lohnen als Pflichtaufgaben
* Ein fleißiger Student erhält also pro Woche ca. 90 XP
  - 32 XP von einer Hälfte des Aufgabenblattes
  - 60 XP von 4 * 60 min / 4 min/XP freiwilligen Aufgaben
* Alle 64 XP steigt man ein Level auf
  - wer nur das Minimum macht (Aufgabenblätter), steigt alle zwei Wochen auf
  - wer alles macht, steigt jede Woche 1,5 Level auf
* Level 14 ist das voraussichtliche "Maximallevel"
  - 90 XP * 10 Wochen / 64 XP 


## Mögliche Dozentenskills

* Feuerblitz: Ball ins Publikum werfen, wer fängt bzw. getroffen wird muss antworten.
* Du kannst nicht vorbei: Vorlesung um 10 min überziehen.
* Körpertausch: Ein Student muss die nächsten 10 min der Vorlesung übernehmen. (mächtiger Skill)
* Aura des Wissensdurstes: Der Dozent darf 10 Minuten über irgendein anekdotisches Thema referieren.
* Fluch des nassen Lappens: Der Dozent darf einen Studenten zwingen, für die ganze Stunde die Tafel zu wischen.
* Fluch der Stille: Der Kurs muss leise sein. (kein Zwang, nur Spielregeln)
* Dozent darf Snacken
* Dozent darf Rechtschreibfehler korrigieren, bevor ein Student die Chance hat
* Zuspätkommer rauswerfen/müssen iwas machen

## Skills der rechten Hand des Goldgorgonen

Schule des Trolls
* Fluch der Logorrhö, 64 XP, CD 2 Wochen: Beschreibung s. Studentenskill
   * Fluch der Albernheit: 128 XP, CD 4 Wochen: Beschreibung s. Studentenskill
   * Fluch von Gogh, 128 XP, CD 4 Wochen: Die Rechte Hand kann für eine kommende Veranstaltung ein Bild auswählen, das auf jeder Folie des Foliensatzes auftauchen muss
   * Heimliche Übernahme: 128 XP, CD 2 Wochen: Eine Skillanwendung, die durch den flüchtigen Blick abgewehrt wird, kann von der rechten Hand erneut mit eigenen Bedingungen angewendet werden.
      * Heimtückischer Spiegelschild: 256 XP, 4 Wochen: Wenn der Goldgorgone der rechten Hand eine Aufgabe zuweist, wird diese Aufgabe zurückgeworfen und er muss sie selbst erledigen.

Schule der Fröhlichkeit
* Colors of the Wind, 64 XP, CD 2 Wochen: einen Tag lang muss jeder, der in Gitlab schreibt, dabei mindestens drei unterschiedliche Emojis benutzen.
* Is this an issue which I see before me?, 64 XP, CD 2 Wochen: Der Goldgorgone muss das nächste Issue in Reimform schreiben.

Schule der Verfressenheit
* Tiefschwarze Erweckung, 128 XP, CD 4 Wochen: Der Goldgorgone muss seiner rechten Hand einen Kaffee servieren.
   * Tiefschwarze Besänftigung, 256 XP, CD 6 Wochen: Manchmal hilft nur noch eines: Schokolade. Der Goldgorgone muss eine milde Gabe in das geheime Schokoladenfach legen, um die Zerstörerin zu besänftigen.
      * Gemeiner Mundraub, 386 XP, CD 6 Wochen: Die rechte Hand kann dem Goldgorgonen sein Mittagessen stibitzen

## Mögliche Tutorenskills

* Unsichtbarkeit: Der Adept ist so gut trainiert, dass er es schafft, sich an einem meeting vorbeizuschleichen
* Mächtige Unsichtbarkeit: Die illusionistischen Fähigkeiten des Adepten sind so mächtig, dass ihm eine Woche lang keine Aufgaben zugewiesen werden können (und er demnach 1/4 seiner Arbeitszeit weniger Arbeiten muss)
* Güldener Geysir: Wenn fünf Adepten diese Fähigkeiten anwenden, spendiert der Goldgorgone einen Kasten Gerstensaft zur Hebung der Stimmung
* Fettiges Futter: Wenn fünf Adepten diese Fähigkeiten anwenden, spendiert der Goldgorgone eine Runde Pizza zur Hebung der Stimmung
* Krümelige Knusperchen: Wenn fünf Adepten diese Fähigkeiten anwenden, spendiert der Goldgorgone Kekse zur Hebung der Stimmung
* Waffe der Macht: Der Goldgorgone oder Valiola kreieren dem Adepten eine Waffe, die seiner würdig ist

# Bedingungen an das Gamification-System:

- Text + Dateiabgabe bei Aufgabenabgabe
- Textfeld bei Skillaktivierung ("Ziel" des Skills)
- Übersicht über "noch zu bewertende Abgaben"
- Skilltree auch als Baum darstellen

# Anzeige von Gamification-Abschnitten

- Gamifizierte abschnitte: :dragon:
- Nicht-gamifizierte abschnitte: :open_book:
- Für Kennzeichnung an Orten, die keine Emojis unterstützen (z.B. Latex): [mozilla emojis](https://github.com/mozilla/fxemoji)
