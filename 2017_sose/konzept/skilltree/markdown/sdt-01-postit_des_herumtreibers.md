# Post-It des Herumtreibers

Bei der Klausur kann ein handschriftlicher Spickzettel der Größe DIN A9 (z.B. kleines Post-It) scheinbar unbemerkt eingeschmuggelt werden.

* Kosten: 64 XP
* Cooldown: passiv