# Trank der Klausurvisionen

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Der Dozent muss Informationen über die Aufgabentypen (Programmieraufgabe, Pseudocode, Multiple-Choice, ...) und deren Verteilung in der Klausur angeben. Visionen sind allerdings nicht immer 100% zutreffend, so lange die Klausur noch nicht fertig erstellt ist.

* Voraussetzung: Post-it des Herumtreibers
* Kosten: 128 XP
* Cooldown: 52 Wochen
* Anwendungen nötig: 30
