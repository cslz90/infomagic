# Segen der Sprache

Sie erhalten die Fähigkeit, von jeder Sprache in jede andere zu übersetzen. Dadurch können Sie eine Aufgabe (freiwillig oder Aufgabenblatt) in jeder beliebigen Programmiersprache abgeben statt in der geforderten Sprache.

* Voraussetzung: Verbesserter Zauberstab
* Kosten: 128 XP
* Cooldown: 2 Wochen
* Texteingabe: Aufgabennummer und Sprache