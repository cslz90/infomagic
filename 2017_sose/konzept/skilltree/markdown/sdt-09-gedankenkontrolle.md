# Gedankenkontrolle

Ein Tutor oder Dozent wird gezwungen, einen Bug in einem Stück Code oder Pseudocode von nicht mehr als 20 Zeilen zu suchen und die Lösung dem Anwender dieses Skills mitzuteilen.

* Voraussetzung: Trank der Klausurenvision
* Kosten: 128 XP
* Cooldown: 3 Wochen
* Texteingabe: Betreffzeile der E-Mail mit dem Code
