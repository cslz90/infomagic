# Majestätische Aura

Eine stärkere Form der Bezauberung, die auf Infomagister (Dozenten) wirkt.

* Voraussetzung: Bezauberung
* Kosten: 128 XP
* Cooldown: 4 Wochen