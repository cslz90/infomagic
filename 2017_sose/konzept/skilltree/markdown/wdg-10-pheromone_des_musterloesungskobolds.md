# Pheromone des Musterlösungskobolds

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Lockt einen Musterlösungskobold an, der eine Musterlösung für ein bereits abgelaufenes Aufgabenblatt erstellt und dem Kurs zur Verfügung stellt.

* Voraussetzung: Trank der Redseligkeit
* Kosten: 128 XP
* Cooldown: 2 Wochen
* Anwendungen nötig: 15