# Trank des Vergessens

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Der Dozent vergisst, dass er die letzte Vorlesung gehalten hat und muss sie noch einmal wiederholen.

* Voraussetzung: Trank der übersprühenden Kreativität
* Kosten: 128 XP
* Cooldown: 4 Wochen
* Anwendungen nötig: 30