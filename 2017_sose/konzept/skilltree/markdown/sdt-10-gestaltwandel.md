# Gestaldwandel

Der Anwender dieser Fähigkeit kann eine Lösung eines Kommilitonen als eigene Lösung für eine Aufgabe einreichen. Der Kommilitone erhält natürlich trotzdem auch selbst noch die Punkte dafür.

* Voraussetzung: Gedankenkontrolle
* Kosten: 256 XP
* Cooldown: 3 Wochen
* Texteingabe: Name des Kommilitonen
