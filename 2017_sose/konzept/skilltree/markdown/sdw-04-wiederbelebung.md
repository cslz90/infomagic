# Wiederbelebung

Wenn der Kommilitone, der Ziel dieses Zaubers ist, das nächste mal sterben würde, wird er stattdessen mit der Hälfte seiner maximalen Lebenspunkte wiederbelebt.

* Voraussetzung: Kleine Heilung anderer
* Kosten: 256 XP
* Cooldown: 8 Wochen
* Texteingabe: Name des Ziels