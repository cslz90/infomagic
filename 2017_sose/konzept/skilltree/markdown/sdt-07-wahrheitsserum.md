# Wahrheitsserum

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Der Dozent muss eine Klausuraufgabe bekannt geben.

* Voraussetzung: Trank des Blackouts
* Kosten: 128 XP
* Cooldown: 52 Wochen
* Anwendungen nötig: 15
