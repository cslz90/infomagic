# Mimikry

Durch ihre soziale Ader haben Sie ihre Kommilitonen so gut kennengelernt, dass Sie deren Fähigkeiten nachahmen können.
Diese Fähigkeit kann nur einmal aktiviert werden, gewährt dann aber dauerhaft irgendeinen andere Fähigkeit aus dem gesamten Baum.

* Voraussetzung: Trank des Vergessens oder Blick in die Zukunft
* Kosten: 64 XP
* Cooldown: 52 Wochen
* Texteingabe: Name der Fähigkeit