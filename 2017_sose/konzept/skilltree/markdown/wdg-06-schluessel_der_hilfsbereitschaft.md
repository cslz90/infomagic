# Schlüssel der Hilfsbereitschaft

Öffnet die Türe zum Büro des Infomagisters und stimmt diesen für 15 Minuten so wohlwollend, dass er jede Frage beantwortet.

* Voraussetzung: Trank des Bonbonregens
* Kosten: 64 XP
* Cooldown: 2 Wochen
* Texteingabe: Terminvorschläge