# Skilltree

* [Kleine Heilung anderer (Schule der Wiederherstellung)](sdw-01-kleine_heilung_anderer.md)
* [Festmahl herbeirufen (Weg der Geselligkeit)](wdg-01-festmahl_herbeirufen.md)
    - [Bezauberung](wdg-02-bezauberung.md)
        + [Majestätische Aura](wdg-03-majestaetische_aura.md)
    - [Trank des Bonbonregens](wdg-04-trank_des_bonbonregens.md)
    - [Trank der Redseligkeit](wdg-05-trank_der_redseligkeit.md)
* [Verbesserter Zauberstab (Weg des Wissens)](wdw-01-verbesserter_zauberstab.md)
* [Fluch der Logorrhö (Schule des Fluches)](sdf-01-fluch_der_logorrhoe.md)
* [Post-It des Herumtreibers (Schule der Täuschung)](sdt-01-postit_des_herumtreibers.md)

# Datei-Benennung

Die Markdown-Versionen der Skilldateien werden alle in den selben Ordner gespeichert, in dem auch diese Datei liegt. Das Benennungsschema lautet wie folgt:

```
pfadkuerzel-id-skillname_mit_unterstrichen
```

Dabei ist die `id` einfach nur eine fortlaufende Nummerierung, die zusammen mit dem Pfadkürzel einen eindeutigen Bezeichner ergibt.
Sie hat keine weitere Bedeutung.

Das `pfadkuerzel` ist eines der folgenden:

* `sdw` für "Schule der Wiederherstellung"
* `wdg` für "Weg der Geselligkeit"
* `wdw` für "Weg des Wissens"
* `sdf` für "Schule des Fluches"
* `sdt` für "Schule der Täuschung"

Der `skillname_mit_unterstrichen` sollte keine Umlaute oder Sonderzeichen enthalten und auch kein `-`, damit ein verrückter Dozent irgendwann auch die Skills mit einem Skript bearbeiten könnte, das einfach nur das `-` als Trennzeichen für die drei Teile des Benennungsschemas verwendet.
