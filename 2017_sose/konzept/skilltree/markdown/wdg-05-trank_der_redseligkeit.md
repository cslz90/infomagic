# Trank der Redseligkeit

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Der Infomagister muss zu Beginn der Veranstaltung eine inhaltliche Verständnisfrage in Form einer 10-minütigen Wiederholung beantworten. Dieser Skill kann mehrfach in der gleichen Stunde verwendet werden.

* Voraussetzung: Festmahl herbeirufen
* Kosten: 128 XP
* Cooldown: 2 Wochen
* Texteingabe: zu beantwortende Frage
* Anwendungen nötig: 10