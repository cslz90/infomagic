# Trank der Albernheit

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Der Dozent muss irgendein albernes Kleidungsstück in der nächsten Vorlesung oder Übung tragen. Das Kleidungsstück muss natürlich von denen, die den Trank brauen, gestellt werden.

* Voraussetzung: Fluch der Logorrhö 
* Kosten: 128 XP
* Cooldown: 4 Wochen
* Anwendungen nötig: 10
