# Feder der Freiheit

Die Feder der Freiheit erlaubt es jedes Detail einer einzigen Aufgabe auf einem Aufgabenblatt umzuschreiben. Die Aufgabe ist dadurch nur für den Anwender dieser Fähigkeit geändert. Der Rest des Kurses muss die Aufgabe ganz normal bearbeiten. (Gilt für die Oberste Ebene der Nummerierung, also z.B. für Aufgabe 3, nicht nur Aufgabe 3.1)

* Voraussetzung: Segen der Sprache
* Kosten: 128 XP
* Cooldown: 3 Wochen
* Texteingabe: Aufgabennummer und Änderung