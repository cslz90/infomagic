# Trank der Übersprühenden Kreativität

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Der Dozent bekommt auf einmal Lust, sich kreativ auszuleben und muss eine schöne Übersichtsfolie erstellen, die die letzte Stunde zusammenfasst.

* Voraussetzung: Trank der Redseligkeit
* Kosten: 64 XP
* Cooldown: 3 Wochen
* Anwendungen nötig: 15