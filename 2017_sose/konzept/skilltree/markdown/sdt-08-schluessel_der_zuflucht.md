# Schlüssel der Zuflucht

Sie erhalten Zugang zur geheimen Zuflucht, die Schutz vor den Schrecken der Klausur bietet. Sie dürfen an einer Vorbereitungsstunde für die Klausur teilnehmen, in der der Dozent mit Ihnen Aufgaben von dem Typ übt, wie sie in der Klausur vorkommen könnten. Außerdem dürfen Sie die Klausur in einem kleinen gemütlichen Raum in ruhiger Atmosphäre getrennt von dem sonstigen Trubel schreiben.

* Voraussetung: Wahrheitsserum
* Kosten: 64 XP
* Cooldown: passiv
