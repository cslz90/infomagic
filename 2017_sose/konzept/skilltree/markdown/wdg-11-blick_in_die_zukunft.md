# Blick in die Zukunft

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Der Dozent muss in der nächsten Stunde an jeden, der eine Frage stellt oder beantwortet Süßigkeiten verteilen.

* Voraussetzung: Pheromone des Musterlösungskobolds
* Kosten: 128 XP
* Cooldown: 52 Wochen
* Anwendungen nötig: 30