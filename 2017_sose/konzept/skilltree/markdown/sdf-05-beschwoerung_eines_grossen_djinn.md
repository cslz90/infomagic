# Beschwörung eines großen Djinn

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Wirkung: Bei der Anwendung dieses Skills kann ein Thema für die letzte Woche der Vorlesung vorgeschlagen werden. Wenn insgesamt mehr als 15 solcher Vorschläge zusammen kommen, wird die letzte Woche aus diesen Wunschinhalten bestehen.

* Voraussetzung: Beschwörung eines Gamification-Djinn
* Kosten: 128 XP
* Cooldown: 52 Wochen
* Anwendungen nötig: 15
* Texteingabe: Themenvorschlag
