# Magierrüstung

Beschwört eine magische Rüstung herauf, die den Schaden von verärgerten Datenkreaturen und verpatzten algomantischen Zauberformeln mindert.
Statt 4 HP ziehen missglückte Aufgabenblätter jetzt nur noch 3 HP ab.

* Voraussetzung: Kleine Heilung anderer
* Kosten: 256 XP
* Cooldown: passiv