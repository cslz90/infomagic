# 8bit-Robe des Erfolgs

Für jedes erlangte Abzeichen gewährt die Robe des Erfolgs 8 Bonus-XP.

* Voraussetzung: Verbesserter Zauberstab
* Kosten: 128 XP
* Cooldown: passiv