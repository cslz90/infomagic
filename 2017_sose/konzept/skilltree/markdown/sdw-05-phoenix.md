# Phönix

Der nächste eigene Tod wird ignoriert. Stattdessen werden die Lebenspunkte auf die Hälfte der maximalen Lebenspunkte gesetzt.

* Voraussetzung: Wiederbelebung
* Kosten: 64 XP
* Cooldown: 10 Wochen