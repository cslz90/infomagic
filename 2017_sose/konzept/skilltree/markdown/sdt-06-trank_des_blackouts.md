# Trank des Blackouts

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Der Dozent vergisst das Thema eines einzelnen Vorlesungsblocks und kann in der Klausur keine Aufgaben dazu stellen.

* Voraussetzung: Trank der Klausurvisionen
* Kosten: 128 XP
* Cooldown: 52 Wochen
* Anwendungen nötig: 30
* Texteingabe: zu vergessendes Thema
