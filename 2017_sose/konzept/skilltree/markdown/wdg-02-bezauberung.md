# Bezauberung

Die Adepten (Tutoren) sind so beeindruckt von dem Anwender/der Anwenderin, dass sie ihm/ihr einen Hinweis zu einer Aufgabe geben.

* Voraussetzung: Fesmahl herbeirufen 
* Kosten: 128 XP
* Cooldown: 4 Wochen
* Texteingabe: Nummer und Name der Aufgabe