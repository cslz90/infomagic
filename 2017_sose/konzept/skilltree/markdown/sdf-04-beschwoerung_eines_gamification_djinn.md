# Beschwörung eines Gamification-Djinn

Erlaubt es, einen Vorschlag für einen Skill, eine Aufgabe oder eine sonstige Änderung des Gamification-Systems zu stellen. Wenn es keine triftigen Gründe gibt, den Wunsch abzulehnen findet er seinen Weg in die Veranstaltung.

* Voraussetzung: Fluch der Logorrhö 
* Kosten: 128 XP
* Cooldown: 1 Woche
