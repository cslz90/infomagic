# Fluch der Logorrhö

Der Dozent muss am Anfang der nächsten Vorlesung einen Text von bis zu 140 Zeichen, den der Anwender vorgibt, unkommentiert vorlesen. Bei zu vulgären oder diskriminierenden Texten kann er den Fluch jedoch abschütteln.

* Kosten: 64 XP
* Cooldown: 3 Wochen