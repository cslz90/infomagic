# Verbesserter Zauberstab

Wertet den eigenen Zauberstab durch einen Kern aus Einhornhaar, Drachenblut oder Greifenfedern auf. Erhalte doppelte XP bei der Abgabe der nächsten freiwilligen Aufgabe.

* Kosten: 64 XP
* Cooldown: 2 Wochen