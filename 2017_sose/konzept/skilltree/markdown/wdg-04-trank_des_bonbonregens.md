# Trank des Bonbonregens

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Der Dozent muss in der nächsten Stunde an jeden, der eine Frage stellt oder beantwortet Süßigkeiten verteilen.

* Voraussetzung: Festmahl herbeirufen
* Kosten: 64 XP
* Cooldown: 2 Wochen
* Anwendungen nötig: 10