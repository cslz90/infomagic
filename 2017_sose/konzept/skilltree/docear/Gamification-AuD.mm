<map version="docear 1.1" dcr_id="1487153453444_8succp5ezrfbjmh3ore61fog7" project="15A414028242FF0LISK2RQYZKGS7SCL2FTO3" project_last_home="file:/D:/Dokumente/Lehre/A&amp;D/ad-cs/2017_ss/gamification/docear">
<!--To view this file, download Docear - The Academic Literature Suite from http://www.docear.org -->
<node TEXT="Skilltree" FOLDED="false" ID="ID_1951517423" CREATED="1487153453370" MODIFIED="1487153462870">
<hook NAME="AutomaticEdgeColor" COUNTER="3"/>
<hook NAME="MapStyle" zoom="1.103">
    <properties show_icon_for_attributes="true" show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="300" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Kleine Heilung anderer (Schule der Wiederherstellung)" POSITION="right" ID="ID_572161729" CREATED="1487153491872" MODIFIED="1491838877301" MOVED="1491835425490">
<edge COLOR="#ff0000"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Heilt sofort 1 HP eines Kommilitonen.
    </p>
  </body>
</html>
</richcontent>
<attribute_layout NAME_WIDTH="78" VALUE_WIDTH="99"/>
<attribute NAME="Kosten" VALUE="64 XP" OBJECT="org.freeplane.features.format.FormattedObject|64 XP|number:decimal:#0.####"/>
<attribute NAME="Cooldown" VALUE="4 Wochen"/>
<attribute NAME="Texteingabe" VALUE="Name des Ziels"/>
<node TEXT="Selbstheilung" ID="ID_1295693565" CREATED="1487153626815" MODIFIED="1491838002301" MOVED="1491835419574"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Heilt sofort 5 eigene HP.
    </p>
  </body>
</html>
</richcontent>
<attribute NAME="Kosten" VALUE="256 XP"/>
<attribute NAME="Cooldown" VALUE="8 Wochen"/>
</node>
<node TEXT="Magierr&#xfc;stung" ID="ID_452825507" CREATED="1487153658198" MODIFIED="1491838007276"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Beschw&#246;rt eine magische R&#252;stung herauf, die den Schaden von ver&#228;rgerten Datenkreaturen und verpatzten algomantischen Zauberformeln mindert.
    </p>
    <p>
      Statt 4 HP ziehen missgl&#252;ckte Aufgabenbl&#228;tter jetzt nur noch 3 HP ab.
    </p>
  </body>
</html>
</richcontent>
<attribute NAME="Kosten" VALUE="256 XP"/>
<attribute NAME="Cooldown" VALUE="passiv"/>
</node>
<node TEXT="Wiederbelebung" ID="ID_676194699" CREATED="1487153686102" MODIFIED="1491838894603"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Wenn der Kommilitone, der Ziel dieses Zaubers ist, das n&#228;chste mal sterben w&#252;rde, wird er stattdessen mit der H&#228;lfte seiner maximalen Lebenspunkte wiederbelebt.
    </p>
  </body>
</html>
</richcontent>
<attribute_layout NAME_WIDTH="78" VALUE_WIDTH="99"/>
<attribute NAME="Kosten" VALUE="256 XP"/>
<attribute NAME="Cooldown" VALUE="8 Wochen"/>
<attribute NAME="Texteingabe" VALUE="Name des Ziels"/>
<node TEXT="Ph&#xf6;nix" ID="ID_1573519709" CREATED="1487153691078" MODIFIED="1491838350018" MOVED="1488911687801"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Der n&#228;chste eigene Tod wird ignoriert. Stattdessen werden die Lebenspunkte auf die H&#228;lfte der maximalen Lebenspunkte gesetzt.
    </p>
  </body>
</html>
</richcontent>
<attribute NAME="Kosten" VALUE="128 XP"/>
<attribute NAME="Cooldown" VALUE="10 Wochen"/>
</node>
</node>
</node>
<node TEXT="Post-It des Herumtreibers (Schule der T&#xe4;uschung)" POSITION="left" ID="ID_600166477" CREATED="1487158971423" MODIFIED="1491838251342" MOVED="1488911371694">
<edge COLOR="#7c0000"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Bei der Klausur kann ein handschriftlicher Spickzettel der Gr&#246;&#223;e DIN A9 (z.B. kleines Post-It) scheinbar unbemerkt eingeschmuggelt werden.
    </p>
  </body>
</html>
</richcontent>
<attribute NAME="Kosten" VALUE="64 XP"/>
<attribute NAME="Cooldown" VALUE="passiv"/>
<node TEXT="Karteikarte des Herumtreibers" ID="ID_1905042814" CREATED="1487161340990" MODIFIED="1491899790128"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Der Spickzettel darf jetzt insgesamt das Format DIN A7 haben.
    </p>
  </body>
</html>

</richcontent>
<attribute NAME="Kosten" VALUE="128 XP"/>
<attribute NAME="Cooldown" VALUE="passiv"/>
<node TEXT="Notizzettel des Herumtreibers" ID="ID_357245623" CREATED="1487161349903" MODIFIED="1491899797593" MOVED="1488911634661"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Der Spickzettel darf jetzt insgesamt das Format DIN A5 haben.
    </p>
  </body>
</html>
</richcontent>
<attribute NAME="Kosten" VALUE="256 XP"/>
<attribute NAME="Cooldown" VALUE="passiv"/>
<node TEXT="Karte des Herumtreibers" ID="ID_790663650" CREATED="1487161679533" MODIFIED="1491899819383"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Der Spickzettel darf jetzt insgesamt das Format DIN A4 haben.
    </p>
  </body>
</html>
</richcontent>
<attribute NAME="Kosten" VALUE="256 XP"/>
<attribute NAME="Cooldown" VALUE="passiv"/>
</node>
</node>
</node>
<node TEXT="Trank der Klausurvisionen" ID="ID_301739275" CREATED="1487160442564" MODIFIED="1491901210046"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Trank: Jede Anwendung erh&#246;ht den F&#252;llstand f&#252;r den gesamten Kurs um 1. Ist der maximale F&#252;llstand erreicht, entfaltet der Trank seine Wirkung.
    </p>
    <p>
      
    </p>
    <p>
      Wirkung: Der Dozent muss Informationen &#252;ber die Aufgabentypen (Programmieraufgabe, Pseudocode, Multiple-Choice, ...) und deren Verteilung in der Klausur angeben. Visionen sind allerdings nicht immer 100% zutreffend, so lange die Klausur noch nicht fertig erstellt ist.
    </p>
  </body>
</html>

</richcontent>
<attribute_layout NAME_WIDTH="120" VALUE_WIDTH="120"/>
<attribute NAME="Kosten" VALUE="128 XP"/>
<attribute NAME="Cooldown" VALUE="52 Wochen"/>
<attribute NAME="Anwendungen n&#xf6;tig" VALUE="30" OBJECT="org.freeplane.features.format.FormattedNumber|30|#0.####"/>
<node TEXT="Trank des Blackouts" ID="ID_118669340" CREATED="1487153709702" MODIFIED="1491900595408" MOVED="1488911618678"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Trank: Jede Anwendung erh&#246;ht den F&#252;llstand f&#252;r den gesamten Kurs um 1. Ist der maximale F&#252;llstand erreicht, entfaltet der Trank seine Wirkung.
    </p>
    <p>
      
    </p>
    <p>
      Wirkung: Der Dozent vergisst das Thema eines einzelnen Vorlesungsblocks und kann in der Klausur keine Aufgaben dazu stellen.
    </p>
  </body>
</html>

</richcontent>
<attribute_layout NAME_WIDTH="120" VALUE_WIDTH="147"/>
<attribute NAME="Kosten" VALUE="256 XP"/>
<attribute NAME="Cooldown" VALUE="52 Wochen"/>
<attribute NAME="Anwendungen n&#xf6;tig" VALUE="30" OBJECT="org.freeplane.features.format.FormattedNumber|30"/>
<attribute NAME="Texteingabe" VALUE="zu vergessendes Thema"/>
<node TEXT="Wahrheitsserum" ID="ID_399665947" CREATED="1487159799570" MODIFIED="1491900574565"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Trank: Jede Anwendung erh&#246;ht den F&#252;llstand f&#252;r den gesamten Kurs um 1. Ist der maximale F&#252;llstand erreicht, entfaltet der Trank seine Wirkung.
    </p>
    <p>
      
    </p>
    <p>
      Wirkung: Der Dozent muss eine Klausuraufgabe bekannt geben.
    </p>
  </body>
</html>

</richcontent>
<attribute_layout NAME_WIDTH="120" VALUE_WIDTH="120"/>
<attribute NAME="Kosten" VALUE="256 XP"/>
<attribute NAME="Cooldown" VALUE="52 Wochen"/>
<attribute NAME="Anwendungen n&#xf6;tig" VALUE="15" OBJECT="org.freeplane.features.format.FormattedNumber|15|#0.####"/>
<node TEXT="Schl&#xfc;ssel der Zuflucht" ID="ID_1466636246" CREATED="1491840371108" MODIFIED="1491900569949"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Sie erhalten Zugang zur geheimen Zuflucht, die Schutz vor den Schrecken der Klausur bietet. Sie d&#252;rfen an einer Vorbereitungsstunde f&#252;r die Klausur teilnehmen, in der der Dozent mit Ihnen Aufgaben von dem Typ &#252;bt, wie sie in der Klausur vorkommen k&#246;nnten. Au&#223;erdem d&#252;rfen Sie die Klausur in einem kleinen gem&#252;tlichen Raum in ruhiger Atmosph&#228;re getrennt von dem sonstigen Trubel schreiben.
    </p>
  </body>
</html>

</richcontent>
<attribute NAME="Kosten" VALUE="64 XP"/>
<attribute NAME="Cooldown" VALUE="passiv"/>
</node>
</node>
</node>
<node TEXT="Gedankenkontrolle" ID="ID_1691211829" CREATED="1487159623083" MODIFIED="1491900770794" MOVED="1488911618727"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Ein Tutor oder Dozent wird gezwungen, einen Bug in einem St&#252;ck Code oder Pseudocode von nicht mehr als 20 Zeilen zu suchen und die L&#246;sung dem Anwender dieses Skills mitzuteilen.
    </p>
  </body>
</html>
</richcontent>
<attribute_layout NAME_WIDTH="78" VALUE_WIDTH="210"/>
<attribute NAME="Kosten" VALUE="256 XP"/>
<attribute NAME="Cooldown" VALUE="3 Wochen"/>
<attribute NAME="Texteingabe" VALUE="Betreffzeile der E-Mail mit dem Code"/>
<node TEXT="Gestaltwandel" ID="ID_98799547" CREATED="1487161240615" MODIFIED="1491900829601"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Der Anwender dieser F&#228;higkeit kann eine L&#246;sung eines Kommilitonen als eigene L&#246;sung f&#252;r eine Aufgabe einreichen. Der Kommilitone erh&#228;lt nat&#252;rlich trotzdem auch selbst noch die Punkte daf&#252;r.
    </p>
  </body>
</html>

</richcontent>
<attribute_layout NAME_WIDTH="78" VALUE_WIDTH="149"/>
<attribute NAME="Kosten" VALUE="512 XP"/>
<attribute NAME="Cooldown" VALUE="3 Wochen"/>
<attribute NAME="Texteingabe" VALUE="Name des Kommilitonen"/>
</node>
</node>
</node>
</node>
<node TEXT="Fesmahl herbeirufen (Weg der Geselligkeit)" POSITION="right" ID="ID_12523213" CREATED="1487161745387" MODIFIED="1491838209910">
<edge COLOR="#00007c"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Der Studierende kann mitten in der Vorlesung oder &#220;bung beliebig viel Essen oder Getr&#228;nke erscheinen lassen und nat&#252;rlich auch verzehren. Ohne diese F&#228;higkeit ist nur eine einzige Trinkflasche erlaubt.
    </p>
  </body>
</html>
</richcontent>
<attribute NAME="Kosten" VALUE="64 XP" OBJECT="org.freeplane.features.format.FormattedObject|64 XP|number:decimal:#0.####"/>
<attribute NAME="Cooldown" VALUE="2 Wochen"/>
<node TEXT="Bezauberung" ID="ID_1097711089" CREATED="1487162096985" MODIFIED="1491839006550"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Die Adepten (Tutoren) sind so beeindruckt von dem Anwender/der Anwenderin, dass sie ihm/ihr einen Hinweis zu einer Aufgabe geben.
    </p>
  </body>
</html>
</richcontent>
<attribute_layout NAME_WIDTH="78" VALUE_WIDTH="190"/>
<attribute NAME="Kosten" VALUE="128 XP"/>
<attribute NAME="Cooldown" VALUE="4 Wochen"/>
<attribute NAME="Texteingabe" VALUE="Nummer und Name der Aufgabe"/>
<node TEXT="Majest&#xe4;tische Aura" ID="ID_695234638" CREATED="1487163085378" MODIFIED="1491839005310" MOVED="1488911702883"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Eine st&#228;rkere Form der Bezauberung, die auf Infomagister (Dozenten) wirkt.
    </p>
  </body>
</html>
</richcontent>
<attribute_layout NAME_WIDTH="78" VALUE_WIDTH="190"/>
<attribute NAME="Kosten" VALUE="256 XP"/>
<attribute NAME="Cooldown" VALUE="4 Wochen"/>
<attribute NAME="Texteingabe" VALUE="Nummer und Name der Aufgabe"/>
</node>
</node>
<node TEXT="Trank des Bonbonregens" ID="ID_1722597721" CREATED="1487163198945" MODIFIED="1491841321637"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Trank: Jede Anwendung erh&#246;ht den F&#252;llstand f&#252;r den gesamten Kurs um 1. Ist der maximale F&#252;llstand erreicht, entfaltet der Trank seine Wirkung.
    </p>
    <p>
      
    </p>
    <p>
      Wirkung: Der Dozent muss in der n&#228;chsten Stunde an jeden, der eine Frage stellt oder beantwortet S&#252;&#223;igkeiten verteilen.
    </p>
  </body>
</html>
</richcontent>
<attribute_layout NAME_WIDTH="120" VALUE_WIDTH="120"/>
<attribute NAME="Kosten" VALUE="64 XP"/>
<attribute NAME="Cooldown" VALUE="2 Wochen"/>
<attribute NAME="Anwendungen n&#xf6;tig" VALUE="10" OBJECT="org.freeplane.features.format.FormattedNumber|10"/>
<node TEXT="Schl&#xfc;ssel der Hilfsbereitschaft" ID="ID_761937326" CREATED="1491840645346" MODIFIED="1491841456920">
<attribute_layout NAME_WIDTH="78" VALUE_WIDTH="110"/>
<attribute NAME="Kosten" VALUE="128 XP"/>
<attribute NAME="Cooldown" VALUE="2 Wochen"/>
<attribute NAME="Texteingabe" VALUE="Terminvorschl&#xe4;ge"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#214;ffnet die T&#252;re zum B&#252;ro des Infomagisters und stimmt diesen f&#252;r 15 Minuten so wohlwollend, dass er jede Frage beantwortet.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Trank der Redseligkeit" ID="ID_1537793906" CREATED="1487163237497" MODIFIED="1491841844547"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Trank: Jede Anwendung erh&#246;ht den F&#252;llstand f&#252;r den gesamten Kurs um 1. Ist der maximale F&#252;llstand erreicht, entfaltet der Trank seine Wirkung.
    </p>
    <p>
      
    </p>
    <p>
      Wirkung: Der Infomagister muss zu Beginn der Veranstaltung eine inhaltliche Verst&#228;ndnisfrage in Form einer 10-min&#252;tigen Wiederholung beantworten. Dieser Skill kann mehrfach in der gleichen Stunde verwendet werden.
    </p>
  </body>
</html>
</richcontent>
<attribute_layout NAME_WIDTH="120" VALUE_WIDTH="143"/>
<attribute NAME="Kosten" VALUE="64 XP"/>
<attribute NAME="Cooldown" VALUE="2 Wochen"/>
<attribute NAME="Texteingabe" VALUE="zu beantwortende Frage"/>
<attribute NAME="Anwendungen n&#xf6;tig" VALUE="10" OBJECT="org.freeplane.features.format.FormattedNumber|10"/>
<node TEXT="Trank der &#xfc;berspr&#xfc;henden Kreativit&#xe4;t" ID="ID_1442840969" CREATED="1487163825388" MODIFIED="1491900369183" MOVED="1488911710360"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Trank: Jede Anwendung erh&#246;ht den F&#252;llstand f&#252;r den gesamten Kurs um 1. Ist der maximale F&#252;llstand erreicht, entfaltet der Trank seine Wirkung.
    </p>
    <p>
      
    </p>
    <p>
      Wirkung: Der Dozent bekommt auf einmal Lust, sich kreativ auszuleben und muss eine sch&#246;ne &#220;bersichtsfolie erstellen, die die letzte Stunde zusammenfasst.
    </p>
  </body>
</html>

</richcontent>
<attribute_layout NAME_WIDTH="120" VALUE_WIDTH="120"/>
<attribute NAME="Kosten" VALUE="128 XP"/>
<attribute NAME="Cooldown" VALUE="3 Wochen"/>
<attribute NAME="Anwendungen n&#xf6;tig" VALUE="15" OBJECT="org.freeplane.features.format.FormattedNumber|15|#0.####"/>
<node TEXT="Trank des Vergessens" ID="ID_608897457" CREATED="1487163902797" MODIFIED="1491842358174"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Trank: Jede Anwendung erh&#246;ht den F&#252;llstand f&#252;r den gesamten Kurs um 1. Ist der maximale F&#252;llstand erreicht, entfaltet der Trank seine Wirkung.
    </p>
    <p>
      
    </p>
    <p>
      Wirkung: Der Dozent vergisst, dass er die letzte Vorlesung gehalten hat und muss sie noch einmal wiederholen.
    </p>
  </body>
</html>
</richcontent>
<attribute_layout NAME_WIDTH="120" VALUE_WIDTH="120"/>
<attribute NAME="Kosten" VALUE="256 XP"/>
<attribute NAME="Cooldown" VALUE="4 Wochen"/>
<attribute NAME="Anwendungen n&#xf6;tig" VALUE="30" OBJECT="org.freeplane.features.format.FormattedNumber|30|#0.####"/>
<node TEXT="Mimikry" ID="ID_1352601016" CREATED="1491839151301" MODIFIED="1491842518661">
<attribute_layout NAME_WIDTH="78" VALUE_WIDTH="119"/>
<attribute NAME="Kosten" VALUE="192 XP"/>
<attribute NAME="Cooldown" VALUE="52 Wochen"/>
<attribute NAME="Texteingabe" VALUE="Name der F&#xe4;higkeit"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Durch ihre soziale Ader haben Sie ihre Kommilitonen so gut kennengelernt, dass Sie deren F&#228;higkeiten nachahmen k&#246;nnen.
    </p>
    <p>
      Diese F&#228;higkeit kann nur einmal aktiviert werden, gew&#228;hrt dann aber dauerhaft irgendeinen andere F&#228;higkeit aus dem gesamten Baum.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Pheromone des Musterl&#xf6;sungskobolds" ID="ID_1767135802" CREATED="1487163935573" MODIFIED="1491842420328" MOVED="1488911710387"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Trank: Jede Anwendung erh&#246;ht den F&#252;llstand f&#252;r den gesamten Kurs um 1. Ist der maximale F&#252;llstand erreicht, entfaltet der Trank seine Wirkung.
    </p>
    <p>
      
    </p>
    <p>
      Wirkung: Lockt einen Musterl&#246;sungskobold an, der eine Musterl&#246;sung f&#252;r ein bereits abgelaufenes Aufgabenblatt erstellt und dem Kurs zur Verf&#252;gung stellt.
    </p>
  </body>
</html>
</richcontent>
<attribute_layout NAME_WIDTH="120" VALUE_WIDTH="120"/>
<attribute NAME="Kosten" VALUE="256 XP"/>
<attribute NAME="Cooldown" VALUE="2 Wochen"/>
<attribute NAME="Anwendungen n&#xf6;tig" VALUE="15" OBJECT="org.freeplane.features.format.FormattedNumber|15|#0.####"/>
<node TEXT="Blick in die Zukunft" ID="ID_1195507219" CREATED="1487163939612" MODIFIED="1491842414739"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Trank: Jede Anwendung erh&#246;ht den F&#252;llstand f&#252;r den gesamten Kurs um 1. Ist der maximale F&#252;llstand erreicht, entfaltet der Trank seine Wirkung
    </p>
    <p>
      
    </p>
    <p>
      Wirkung: Der Dozent muss eine Probeklausur erstellen.
    </p>
  </body>
</html>
</richcontent>
<attribute_layout NAME_WIDTH="120" VALUE_WIDTH="120"/>
<attribute NAME="Kosten" VALUE="256 XP"/>
<attribute NAME="Cooldown" VALUE="52 Wochen"/>
<attribute NAME="Anwendungen n&#xf6;tig" VALUE="30" OBJECT="org.freeplane.features.format.FormattedNumber|30|#0.####"/>
<node TEXT="Mimikry" ID="ID_1942228397" CREATED="1491839151301" MODIFIED="1491839426280">
<attribute_layout NAME_WIDTH="78" VALUE_WIDTH="119"/>
<attribute NAME="Kosten" VALUE="64 XP"/>
<attribute NAME="Cooldown" VALUE="52 Wochen"/>
<attribute NAME="Texteingabe" VALUE="Name der F&#xe4;higkeit"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Durch ihre soziale Ader haben Sie ihre Kommilitonen so gut kennengelernt, dass Sie deren F&#228;higkeiten nachahmen k&#246;nnen.
    </p>
    <p>
      Diese F&#228;higkeit kann nur einmal aktiviert werden, gew&#228;hrt dann aber dauerhaft irgendeinen andere F&#228;higkeit aus dem gesamten Baum.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Fluch der Logorrh&#xf6; (Schule des Fluches)" POSITION="left" ID="ID_31307344" CREATED="1487164048043" MODIFIED="1491838232830">
<edge COLOR="#007c00"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Der Dozent muss am Anfang der n&#228;chsten Vorlesung einen Text von bis zu 140 Zeichen, den der Anwender vorgibt, unkommentiert vorlesen. Bei zu vulg&#228;ren oder diskriminierenden Texten kann er den Fluch jedoch absch&#252;tteln.
    </p>
  </body>
</html>
</richcontent>
<attribute NAME="Kosten" VALUE="64 XP"/>
<attribute NAME="Cooldown" VALUE="3 Wochen"/>
<node TEXT="Beschw&#xf6;rung eines Gamification-Djinn" ID="ID_28013024" CREATED="1487164070475" MODIFIED="1491900900831"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Erlaubt es, einen Vorschlag f&#252;r einen Skill, eine Aufgabe oder eine sonstige &#196;nderung des Gamification-Systems zu stellen. Wenn es keine triftigen Gr&#252;nde gibt, den Wunsch abzulehnen findet er seinen Weg in die Veranstaltung.
    </p>
  </body>
</html>

</richcontent>
<attribute NAME="Kosten" VALUE="128 XP"/>
<attribute NAME="Cooldown" VALUE="1 Woche"/>
<node TEXT="Beschw&#xf6;rung eines gro&#xdf;en Djinn" ID="ID_662672010" CREATED="1487164133763" MODIFIED="1491901016503" MOVED="1488911653017"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Trank: Jede Anwendung erh&#246;ht den F&#252;llstand f&#252;r den gesamten Kurs um 1. Ist der maximale F&#252;llstand erreicht, entfaltet der Trank seine Wirkung.
    </p>
    <p>
      
    </p>
    <p>
      Wirkung: Bei der Anwendung dieses Skills kann ein Thema f&#252;r die letzte Woche der Vorlesung vorgeschlagen werden. Wenn insgesamt mehr als 15 solcher Vorschl&#228;ge zusammen kommen, wird die letzte Woche aus diesen Wunschinhalten bestehen.
    </p>
  </body>
</html>

</richcontent>
<attribute_layout NAME_WIDTH="120" VALUE_WIDTH="120"/>
<attribute NAME="Kosten" VALUE="256 XP"/>
<attribute NAME="Cooldown" VALUE="52 Wochen"/>
<attribute NAME="Texteingabe" VALUE="Themenvorschlag"/>
<attribute NAME="Anwendungen n&#xf6;tig" VALUE="15" OBJECT="org.freeplane.features.format.FormattedNumber|15"/>
<node TEXT="???" ID="ID_1270821101" CREATED="1491839564786" MODIFIED="1491901507036"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Oh, da sind Sie wohl auf einen Fehler im System gesto&#223;en. Diese F&#228;higkeit ist noch nicht fertig erstellt. W&#228;hlen Sie sich eine beliebige Wirkungsweise dieser F&#228;higkeit aus. Der Infomagister muss der F&#228;higkeit zustimmen, bevor sie aktiviert werden kann.
    </p>
  </body>
</html>

</richcontent>
<attribute_layout NAME_WIDTH="78" VALUE_WIDTH="191"/>
<attribute NAME="Kosten" VALUE="512 XP"/>
<attribute NAME="Cooldown" VALUE="52 Wochen"/>
<attribute NAME="Texteingabe" VALUE="Wirkungsweise des neuen Skills"/>
</node>
</node>
</node>
<node TEXT="Gebr&#xe4;u der Albernheit" ID="ID_568391604" CREATED="1487164058460" MODIFIED="1491901383152"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Trank: Jede Anwendung erh&#246;ht den F&#252;llstand f&#252;r den gesamten Kurs um 1. Ist der maximale F&#252;llstand erreicht, entfaltet der Trank seine Wirkung.
    </p>
    <p>
      
    </p>
    <p>
      Wirkung: Der Dozent muss irgendein albernes Kleidungsst&#252;ck in der n&#228;chsten Vorlesung oder &#220;bung tragen. Das Kleidungsst&#252;ck muss nat&#252;rlich von denen, die den Trank brauen, gestellt werden.
    </p>
  </body>
</html>

</richcontent>
<attribute_layout NAME_WIDTH="120" VALUE_WIDTH="120"/>
<attribute NAME="Kosten" VALUE="128 XP"/>
<attribute NAME="Cooldown" VALUE="4 Wochen"/>
<attribute NAME="Anwendungen n&#xf6;tig" VALUE="10" OBJECT="org.freeplane.features.format.FormattedNumber|10"/>
<node TEXT="Gebr&#xe4;u des vorzeitigen Endes" ID="ID_999028412" CREATED="1487164143986" MODIFIED="1491901389685" MOVED="1488911660043"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Trank: Jede Anwendung erh&#246;ht den F&#252;llstand f&#252;r den gesamten Kurs um 1. Ist der maximale F&#252;llstand erreicht, entfaltet der Trank seine Wirkung.
    </p>
    <p>
      
    </p>
    <p>
      Gruppenskill: Das Raum-Zeit-Gef&#252;ge bricht auseinander und die Vorlesung endet eine Woche fr&#252;her.
    </p>
  </body>
</html>

</richcontent>
<attribute_layout NAME_WIDTH="120" VALUE_WIDTH="120"/>
<attribute NAME="Kosten" VALUE="256 XP"/>
<attribute NAME="Cooldown" VALUE="52 Wochen"/>
<attribute NAME="Anwendungen n&#xf6;tig" VALUE="30" OBJECT="org.freeplane.features.format.FormattedNumber|30|#0.####"/>
</node>
</node>
</node>
<node TEXT="Verbesserter Zauberstab (Weg des Wissens)" POSITION="right" ID="ID_1191919047" CREATED="1487164357273" MODIFIED="1491838217454" MOVED="1487170497833">
<edge COLOR="#7c7c00"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Wertet den eigenen Zauberstab durch einen Kern aus Einhornhaar, Drachenblut oder Greifenfedern auf. Erhalte doppelte XP bei der Abgabe der n&#228;chsten freiwilligen Aufgabe.
    </p>
  </body>
</html>
</richcontent>
<attribute NAME="Kosten" VALUE="64 XP"/>
<attribute NAME="Cooldown" VALUE="2 Wochen"/>
<node TEXT="8bit-Robe des Erfolgs" ID="ID_791997764" CREATED="1487164350553" MODIFIED="1491899233367"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      F&#252;r jedes erlangte Abzeichen gew&#228;hrt die Robe des Erfolgs 8 Bonus-XP.
    </p>
  </body>
</html>

</richcontent>
<attribute NAME="Kosten" VALUE="128 XP"/>
<attribute NAME="Cooldown" VALUE="passiv"/>
</node>
<node TEXT="Segen der Sprache" ID="ID_1844207800" CREATED="1491840124838" MODIFIED="1491899547649"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Sie erhalten die F&#228;higkeit, von jeder Sprache in jede andere zu &#252;bersetzen. Dadurch k&#246;nnen Sie eine Aufgabe (freiwillig oder Aufgabenblatt) in jeder beliebigen Programmiersprache abgeben statt in der geforderten Sprache.
    </p>
  </body>
</html>

</richcontent>
<attribute_layout NAME_WIDTH="78" VALUE_WIDTH="183"/>
<attribute NAME="Kosten" VALUE="128 XP"/>
<attribute NAME="Cooldown" VALUE="2 Wochen"/>
<attribute NAME="Texteingabe" VALUE="Aufgabennummer und Sprache"/>
<node TEXT="Feder der Freiheit" ID="ID_1429331377" CREATED="1491840163390" MODIFIED="1491899619028"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Die Feder der Freiheit erlaubt es jedes Detail einer einzigen Aufgabe auf einem Aufgabenblatt umzuschreiben. Die Aufgabe ist dadurch nur f&#252;r den Anwender dieser F&#228;higkeit ge&#228;ndert. Der Rest des Kurses muss die Aufgabe ganz normal bearbeiten. (Gilt f&#252;r die Oberste Ebene der Nummerierung, also z.B. f&#252;r Aufgabe 3, nicht nur Aufgabe 3.1)
    </p>
  </body>
</html>

</richcontent>
<attribute_layout NAME_WIDTH="78" VALUE_WIDTH="190"/>
<attribute NAME="Kosten" VALUE="256 XP"/>
<attribute NAME="Cooldown" VALUE="3 Wochen"/>
<attribute NAME="Texteingabe" VALUE="Aufgabennummer und &#xc4;nderung"/>
</node>
</node>
</node>
</node>
</map>
