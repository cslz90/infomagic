# :sparkles: Spielregeln für A&D :sparkles:

## :point_up: Vorwort

Das Wort [Gamifizierung](https://de.wikipedia.org/wiki/Gamification) bezeichnet die Verwendung von Techniken des Game-Design in anderen Kontexten.
In diesem Semester nehmen Sie an einer *gamifizierten* Variante der Vorlesung "Algorithmen und Datenstrukturen" teil.

Wie jedes gute Spiel braucht natürlich auch unsere Vorlesung ein Regelwerk, das wir hier zusammengetragen haben.
Bevor wir uns aber den eigentlichen Spielregeln widmen, müssen ein paar wichtige Punkte klargestellt werden:

* Die Teilnahme an der Gamification ist *freiwillig*.
  Wenn Sie keine Lust auf dieses Angebot haben, müssen Sie einfach nur Ihre Arbeitsblätter rechtzeitig abgeben und eine Klausur schreiben, wie in jedem anderen Kurs auch.
* Das Konzept beruht auf Vertrauen - also darauf, dass Sie sich an die Spielregeln halten.
  Sollte es sich herausstellen, dass das System zu sehr ausgenutzt wird, müssen wir es schlimmstenfalls im Laufe des Semesters einstellen.
* Die Erklärungen auf den Vorlesungsfolien oder in Aufgabentexten gehen manchmal ins Lächerliche.
    - Das ist gewollt, da ungewöhnliche Dinge besser im Gedächtnis bleiben als gewohnte Strukturen.
    - Das ändert nichts an den eigentlichen Inhalten.
      Sie erlernen die gleichen Techniken, die auch in den bisherigen Semestern in der Veranstaltung vermittelt wurden.

## :crystal_ball: Einleitung

![THM-Logo gamifiziert](https://homepages.thm.de/~cslz90/kurse/ad17/static/img/THM_gamified.png)

Wir befinden uns an der *Thaumaturgischen Hochschule für Magie (THM)*.
Viele weise und mächtige Infomagier haben einst in diesen Hallen ihr Handwerk erlernt.
Auf den Schultern dieser Riesen (keine echten Riesen, die meisten Infomagier an der THM sind menschlich) haben nun auch Sie Ihre ersten Berührungen mit den magischen Künsten gemacht.
Jetzt wird es Zeit, die einfachen Grundzauber wie *Rekursio*, *Iteratio* oder *Excepto Catchionum* zu größeren Zauberformeln zu verbinden und diese auf gefährlichere Kreaturen als *Stringnattern* und *Integerlinge* anzuwenden.

Der Kurs *Algomantik und Datenkreaturen* wird ihnen dabei helfen, indem Sie ein grundlegendes abstrakteres Verständnis erlangen, wie solche Zauberformeln und Wesen aufgebaut sind, und wie Sie diese beherrschen können.

## :heart: Lebenspunkte (HP)

Als Infomagier sind Sie ein Sterblicher, der mit den großen Kräften des Universums spielt.
Das heißt natürlich, dass Sie auf Ihre Gesundheit achten müssen.
Diese spiegelt sich in Ihren Lebenspunkten wieder.

Sie starten mit vollen 10 Lebenspunkten ins Semester.
Um Ihre Zulassung zu der Prüfung am Ende des Semesters zu erhalten, müssen Sie insgesamt 6 Aufgabenblätter bearbeiten.
Diese Aufgabenblätter bringen Sie in den Kontakt mit gefährlichen Zauberformeln und Datenkreaturen.
Wenn Sie diese unsachgemäß behandeln oder bis zur Abgabefrist nicht beachten, ziehen sie Ihnen pro Aufgabenblatt 4 Lebenspunkte ab.
Die in der Veranstaltung behandelten Datenkreaturen fühlen sich sachgemäß behandelt, wenn sie 70% des jeweiligen Aufgabenblattes korrekt lösen.

Sinken Ihre Lebenspunkte auf Null, müssen Sie auf die Krankenstation und scheiden daher aus dem Vorlesungsbetrieb für dieses Semester aus.
Sie erhalten dann keine Klausurzulassung.
Auch als Infomagier haben Sie also nur ein Leben.
Nutzen Sie es weise!

**Zusammenfassung:**

* 10 Lebenspunkte
* -4 Lebenspunkte pro nicht akzeptiertem Aufgabenblatt
* Aufgabenblatt wird akzeptiert, wenn es zu mindestens 70 % korrekt gelöst ist

## :bulb: Erfahrungspunkte (XP)

Wissen ist Macht.
Das weiß jeder Infomagier.
Ihr gesammeltes Wissen wird (weil Infomagier sowieso gerne alles in Zahlen ausdrücken) mit Erfahrungspunkten bemessen.
Erfahrungspunkte gibt es generell für jede sinnvolle (und messbare) Beteiligung an der Veranstaltung.
Dazu gehören verschiedene *Aufgaben* wie die Anwesenheit bei der Vorlesung, das Anfertigen von Mitschriften, das Lösen von Übungsaufgaben, aber auch z.B. das Finden von Fehlern in den Übungsunterlagen.

Welche Aufgaben Ihnen Erfahrungspunkte geben, erfahren Sie in dem Gamificationsystem [Gildamesh](http://gildamesh.mni.thm.de/).
Manche der optionalen Aufgaben sind ein wenig mysteriöser formuliert.
Es liegt an Ihnen, diese Aufgaben zu entschlüsseln, um die Belohnung zu erhalten.

Die Aufgaben verlangen meistens eine Abgabe in Form von Text oder einer Datei.
Je nach Aufgabentyp muss diese Abgabe entweder direkt in Gildamesh, in [Moodle](https://moodle.thm.de/course/view.php?id=5014) oder auf dem Abgabenserver [Dozentron](http://dozentron.mni.thm.de/) (nur über THM-Netz oder [VPN](https://www.thm.de/its/services/netzdienste/vpn/allgemeine-hinweise.html) erreichbar) eingereicht werden.
In jedem Fall muss der Infomagister (von Muggeln auch *Dozent* genannt) oder ein Adept (von Muggeln auch *Tutor* genannt) die Erledigung der Aufgabe bestätigen.

**Zusammenfassung:**

* XP erlangt man durch Aufgaben (sinnvolle und messbare Beteiligung)
* Aufgaben findet man in [Gildamesh](http://gildamesh.mni.thm.de/)
* Abgabe erfolgt je nach Aufgabe über [Gildamesh](http://gildamesh.mni.thm.de/), [Moodle](https://moodle.thm.de/course/view.php?id=5014) oder [Dozentron](http://dozentron.mni.thm.de/)
* XP erst nach Bestätigung durch Dozenten/Tutoren

## :rocket: Stufe (Level)

Wie kaum etwas anderes im Leben lässt sich der Wissensfortschritt eines Infomagiers genau bemessen.
Das liegt an einem intensiven Gespür, das Sie als Novize der Infomagie gleich zu Beginn ihres Studiums erwerben.
Im Laufe Ihrer Studien und Recherchen eignen Sie sich einzelne Wissensaspekte an, die als Erfahrungspunkte oder kurz XP bezeichnet werden.

Nach jeweils 64 erreichten Erfahrungspunkten spüren Sie, dass die Zeit gekommen ist, um das Gelernte zu verinnerlichen.
Sie ziehen sich zur Meditation zurück und vollziehen den Ritus des *Stufenaufstiegs*.
Beginnend mit Stufe 0 (auch *Level* 0 genannt) steigen Sie damit immer weiter auf und erlangen immer tiefere Einblicke in das Wesen der Magie sowie deren Konzepte und faszinierende Kreaturen.

Dieses Verständnis von Zusammenhängen lässt Sie ein Gespür entwickeln, das Ihnen bei der Lösung von Aufgaben und vor allem beim Absolvieren der Abschlussprüfung hilft.
Umfassende Forschungen an überwiegend freiwilligen Subjekten haben ergeben, dass man dabei mit der folgenden Verbesserung der Abschlussnote durch das Level rechnen kann:

|Level|Bonuspunkte|Gesamt|
| --- | --------- | ---- |
|   0 |         - |   0% |
|   8 |       +5% |   5% |
|   9 |       +4% |   9% |
|  10 |       +2% |  11% |
|  11 |       +2% |  13% |
|  12 |       +2% |  15% |
|  13 |       +4% |  19% |
|  14 |       +5% |  24% |


**Zusammenfassung:**

* jeder Novize startet bei Level 0
* pro 64 gesammelter XP steigt man ein Level auf
* je höher das Level, desto mehr Bonuspunkte für die Klausur

## :stars: Fähigkeiten (Skills)

Selbst die größte Menge an Wissen ist unnütz, wenn man sie nicht einsetzt (hoffentlich für das Gute).
Ihr Wissen können Sie in *Fähigkeiten*, auch englisch *Skills* genannt, investieren.
Beim Kauf einer Fähigkeit werden Ihnen die Erfahrungspunkte abgezogen.
Die Gesamtzahl der über das Semester hinweg erreichten Erfahrungspunkte wird aber trotzdem festgehalten, da aus diesem Ihre Stufe (s.o.) errechnet wird.

Es gibt zwei verschiedene Sorten von Fähigkeiten:

* *Passive Fähigkeiten* wirken nach dem Kauf für das gesamte Semester.
  Dazu zählen zum Beispiel zusätzliche Hilfsmittel für die Klausur, die sie ergattern können.
* *Aktive Fähigkeiten* müssen, wie der Name sagt, erst aktiviert werden, bevor sie eine Wirkung erzielen.
  Da das Einsetzen dieser Fähigkeiten anstrengend ist, müssen Sie sich danach erst einmal ausruhen, bevor sie die Fähigkeit erneut verwenden können.
  Diese Ruhepause, auch *Abkühlzeit* (engl. *cooldown*), ist unterschiedlich lang, abhängig von der Potenz der eingesetzten Fähigkeit.
  Die Anwendung einer Fähigkeit kann so wie die Erledigung einer Aufgabe vom Infomagister entweder bestätigt oder abgelehnt werden.

  Manche aktiven Fähigkeiten benötigen beim Einsetzen noch eine zusätzliche Information, *wie* bzw. *wofür* die Fähigkeit verwendet werden soll.
  Diese kann in Gildamesh über ein entsprechendes Textfeld eingegeben werden.

Prinzipiell können Sie alle Fähigkeiten erlernen, aber die stärkeren Fähigkeiten bauen natürlich auf den schwächeren auf.
Durch diese Beziehung ergibt sich ein Fähigkeitenbaum (engl. *Skill-Tree*), der die Abhängigkeiten zwischen den Fähigkeiten darstellt.
In diesem Baum startet man an der Wurzel und kann immer nur die Fähigkeiten erlernen, die unmittelbar mit einer Fähigkeit verknüpft sind, die man bereits besitzt.

![Skilltree](img/skilltree.png)

Eine besondere Klasse von aktiven Fähigkeiten sind die *Zaubertränke*.
Diese haben sehr mächtige Effekte, aber da diese Zaubertränke eine lange Liste obskurer Zutaten (wie zum Beispiel die Tränen eines Erstsemesters nach der OOP-Klausur) benötigen, braucht es mehrere Magier, um sie vor dem Semesterende fertig zu brauen.
Jeder Infomagier, der einen Teil der Zutaten beisteuert, kann natürlich auch Einfluss darauf nehmen, welchen Effekt der Trank haben wird.
Am Ende entscheidet der Mehrheitsbeschluss.

Es kann bei so vielen Fähigkeiten natürlich auch passieren, dass Sie sich in Ihrer Wahl vertun und mit dem Weg, den sie eingeschlagen haben, später nicht mehr glücklich sind.
Infomagister sind erfreulicherweise in der Lage, die Vergangenheit nach Belieben umzuschreiben.
Wenn Sie also im Nachhinein unglücklich mit Ihrer Wahl sind, wenden Sie sich an den leitenden Infomagister der Veranstaltung.
Vielleicht beherrscht er ja diesen Zauber und ist so freundlich ihn für Sie einzusetzen.
Dennoch gilt: Wählen Sie weise!
Auch für Infomagister herrschen die Gesetze der Magie und ein mächtiger Zauber aus der Schule der Temporalrevision ist nur mit großem Aufwand einsetzbar.

**Zusammenfassung:**

* Skills kosten XP
* Das Level richtet sich nach den Gesamt-XP. Es sinkt nicht dadurch, dass man Skills kauft.
* Passive Skills wirken immer, aktive Skills haben eine Abkühlzeit
* jeder kann jeden Skill erlernen
* schwächere Skills sind Voraussetzung für stärkere
* Tränke müssen von mehreren Personen gleichzeitig aktiviert werden
* hat man sich "verskillt", kann der Infomagister im Notfall helfen

## :tophat: Bewertung des Infomagisters

Die magischen Fähigkeiten von Infomagistern, wie dem, der diese Veranstaltung leitet, beruhen im Grunde auf denselben Gesetzmäßigkeiten, an die sich auch Novizen halten müssen.
Genauso wie Novizen dadurch lernen, dass sie die Wirkung oder den spektakulären Fehlschlag ihrer Zauber beobachten, brauchen aber auch Infomagister eine Rückmeldung über ihr Können.

Sie als Novize können ihrem Infomagister damit helfen, indem Sie ihm in jeder Vorlesung und in jeder Übung XP für gute Lehre erteilen.
Dazu malen Sie ein magisches Symbol (1 oder 0), gerne auch mit einer weiterführenden Erklärung, auf einen Zettel und werfen ihn am Ende der Veranstaltung in den Kelch der Kritik.

Konstruktive Verbesserungsvorschläge helfen ihrem Infomagister, seine wirren Ideen und Gedanken besser zu sortieren, um die Veranstaltung für Sie ansprechender zu gestalten.
Für jede 1 in dem Kelch der Kritik erhält er außerdem einen Erfahrungspunkt.

Auch Infomagister besitzen einige Fähigkeiten wie zum Beispiel den Feuerblitz der erzwungenen Antworten oder die bereits angesprochene Temporalrevision.
Anders als bei Novizen, sind die Fähigkeiten eines Infomagisters aber so mächtig, dass er innerhalb eines Semesters keine neuen Fähigkeiten erlernen kann. Die Erfahrungspunkte dienen in dieser Veranstaltung also nur als Indikator dafür, wie erfolgreich die Veranstaltung ist.

Die XP ihres Infomagisters können Sie jederzeit in einem [Dokument im Netz der großen Google-Spinne](https://docs.google.com/document/d/1YjRkaoiOS-HGmrlNFoXgVj8glARMmiLEpZ4NhCJJcAk/edit?usp=sharing) einsehen.

**Zusammenfassung:**

* auch der Infomagister hat Skills
* XP werden von Studierenden vergeben
    - jeder Student kann 1 XP am Ende jeder Vorlesung vergeben
* Regeln für Skills für Infomagister gibt es in Moodle


## :earth_africa: Globales Kuriosum (World-Event)

Meistens tun die magischen Ströme in der Welt das, was Infomagier ihnen befehlen.
Besondere Ereignisse können aber dazu führen, dass Zauber entfesselt werden, die sich auf die gesamte Welt auswirken, sogenannte globale Kuriosa (engl. *World-Events*).
Man munkelt von Bonbonregen, weltumfassenden Heilzaubern oder Gedankenstürmen, die alle erlernten Fähigkeiten durcheinanderwirbeln.
Da diese Zauber aber nur sehr selten und unter besonderen Bedingungen geschehen, ist wenig Genaues über sie bekannt.
Meist versteht man die Wirkung eines solchen Kuriosums erst nachdem es schon geschehen ist.

**Zusammenfassung:**

* bestimmte Umstände aktivieren weltumfassende Zauber
* Bedingung und Auswirkung der Zauber werden erst bei der Aktivierung bekanntgegeben