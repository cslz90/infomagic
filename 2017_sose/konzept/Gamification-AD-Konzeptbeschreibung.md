# Basilisten, Baumhydren und Blutgraphen - Gamification in der Informatik
Christopher Schölzel

## Die Idee

Die Veranstaltung "Algorithmen und Datenstrukturen" im zweiten Semester der Informatikstudiengänge der THM setzt sich zum Ziel, das Programmierverständnis der Studierenden auf eine abstraktere Stufe zu heben.
Sowohl der damit verbundene Theoriegehalt wie auch die anspruchsvolleren praktischen Programmieraufgaben stellen für manche Studierende eine große aber gleichermaßen wichtige Hürde dar, die nur mit konsequentem und kontinuierlichem Übungsaufwand zu bewältigen ist.

Um die Studierenden dazu zu motivieren, diesen Übungsaufwand freiwillig aufzubringen und gleichzeitig die abstrakten Konzepte mit einprägsamen Metaphern greifbarer zu machen, bietet sich *Gamification*, also die Verwendung von Techniken aus dem Game-Design, an.
Da bei der Veranstaltung mit hohen Studierendenzahlen zu rechnen ist, ist ein solcher Zusatzaufwand aber nur mit geeigneter technischer und personeller Unterstützung möglich.

Im Folgenden möchte Ich ein solches Konzept vorstellen, das mit zwei entsprechenden Onlineanwendungen arbeitet, die beide jeweils von einem Studenten der THM in einer Bachelorarbeit entwickelt wurden.

## Gamification mit Gildamesh

Der Gamification-Server *Gildamesh* setzt die Grundtechniken von Computerrollenspielen für die Lehre an Hochschulen um.
Studierende können Aufgaben einreichen, die ihnen *Erfahrungspunkte* geben, wenn der Dozent die Einreichung akzeptiert.
Für diese Erfahrungspunkte können sie sich dann *Fähigkeiten* kaufen, die ihnen im Verlauf des Semesters gewisse Vorteile gewähren.

Für diese Veranstaltung gibt es zwei Typen von Aufgaben:

* *Pflichtaufgaben* entsprechen den Übungsblättern, die für die Klausurzulassung notwendig sind.
  Werden diese Aufgaben nicht rechtzeitig abgegeben, verlieren die Studierenden kontinuierlich *Lebenspunkte*.
  Wenn sie keine Lebenspunkte mehr übrig haben, erhalten sie keine Klausurzulassung.
* *Freiwillige Aufgaben* umfassen alles weitere "belohnenswerte" Verhalten von Studierenden - von der bloßen Anwesenheit in der Vorlesung über die Anfertigung einer Mitschrift oder die Mitarbeit an einem Wiki bis hin zum Lösen von zusätzlichen Übungsaufgaben.

Besonders schwere Aufgaben oder die besonders gute oder schnelle Bearbeitung von bestimmten Ausgaben wird außerdem mit *Abzeichen* belohnt.
Diese Abzeichen haben keinen weiteren spielerischen Mehrwert, sondern dienen nur als Symbol der Anerkennung.

Die Fähigkeiten, die die Studierenden erwerben können, wirken sich auf die Gestaltung der Vorlesung und Übung, die Klausurzulassung und die Klausur selbst aus.
So können Studierende sich zum Beispiel die Erlaubnis eines Spickzettels für die Klausur, die Wiederholung einer Vorlesungsstunde, verlängerte Abgabezeiten oder Tipps zu Übungsaufgaben erkaufen.

## Automatische Überprüfung von Abgaben mit Dozentron

Der Abgabenserver *Dozentron* wird in diesem Konzept verwendet, um die gesteigerten Anzahl der Studierendenabgaben beherrschbar zu machen und den Tutoren und Tutorinnen zu erlauben, sich auf inhaltliches Feedback zu konzentrieren statt die grundlegende Korrektheit der Abgaben prüfen zu müssen.

Dozentron kennt zwei verschiedene Aufgabentypen:

* *Java-Aufgaben* werden mit JUnit-Tests geprüft.
  Dazu müssen auf dem Server Testdateien existieren, die den Studierenden auch genaue Vorgaben für die Implementierung machen.
  Die Studierenden erhalten eine genaue Übersicht, welcher Teil ihrer Programme fehlschlägt und was der Grund dafür war.
* *Sprachunabhängige Aufgaben* werden in Form eines Input-Output-Tests geprüft.
  Auf dem Server existieren für jede Aufgabe zwei Programme.
  Eines generiert für jeden Studierenden eine Input-Datei (z.B. 1000 Zahlen, die sortiert werden müssen).
  Das andere Programm ist eine Musterlösung der Aufgabe, so dass die Antwort des Studenten auf die Prüfungsfrage (z.B. "An welcher Stelle befindet sich die Zahl 999, wenn alle Zahlen aus der Eingabe sortiert wurden?") auf ihre Korrektheit geprüft werden kann.
  Bei diesem Aufgabentyp kann der Server nur eine Ja-/Nein-Antwort zurückliefern.

Dieser Kurs sieht es vor, dass Studierende ihre Lösungen bis zum Ablauf der Abgabefrist beliebig oft testen und korrigieren können.
Dadurch soll vermieden werden, dass die unbarmherzige technische Überprüfung zu Frust führt.

## Einsatz von studentischen Hilfskräften

Wie bereits eingangs erwähnt kann dieses Kurskonzept nur mit einer erhöhten Anzahl von Tutoren und Tutorinnen funktionieren.
Diese Hilfskräfte sollen vor allem Aufgaben ausarbeiten, Musterlösungen und Tests für den Betrieb von Dozentron erstellen und den Studierenden Feedback zu jeder Form von Abgabe geben, die über Gildamesh oder Dozentron getätigt werden kann.