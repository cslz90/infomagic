# Notizen für die Zukunft

## Einleitung in Dozentron

* Moodle-Test mit typischen Fehlermeldungen
    - "Helfen Sie Ihrem fiktiven Kommilitonen..."

## Skillbaum

* 10 XP am Anfang schenken, in der ersten Woche kosten die Startskills nur 10 XP
    - XP aus anderen Aufgaben werden erst am Ende der Woche eingetragen
* Skill: Du bekommst die Zulassung, aber keine Boni?
    - für diejenigen, die es im nächsten Jahr nochmal probieren wollen
* Neue Skills:
    - Aufgabe für die Klausur erstellen
    - XP-Boost für eine Woche/einen Tag

## Gamification auf Folien

* ganz strikt zwischen gamifiziert und ungamifiziert trennen?
* "Rolle" statt "Familie" für ADTs
* "Iterator" => "Dompteur"
* Greedy ist Kurzsichtig

## O-Notation

* mehr Beispiele, mehr Vergleich der Klassen direkt nach der Einführungsfolie

## Algorithmische Techniken

* typischer Pseudocode-Aufbau direkt nach der Einführung der Technik

## Bäume

* in-order, pre-order, post-order explizit besprechen

## Dozenten-XP

* in Gildamesh

## Pseudocode

* Typsystem mit Subtypen einführen?
* Deklaration von Datentypen mit Operationen (z.b. wie bei Elixir mit modules?)
* vllt auch einfach ersten Parameter bei Operationen "this" nennen?
* keine "getter" oder Sichtbarkeitskonzepte einbauen
