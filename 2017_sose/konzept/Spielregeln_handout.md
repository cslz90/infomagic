# Vereinbarungen

* Gamification ist freiwillig (Aufgabenblätter aber nicht :wink: )
* Wer mitspielt, hält sich an die Spielregeln

# Die Technik

* Gildamesh
    - gildamesh.mni.thm.de
    - Gamification-Server
    - verwaltet Lebenspunkte, XP, Skills, ...
    - enthält Aufgabenbeschreibungen
* Dozentron
    - dozentron.mni.thm.de
        + nur an der THM oder über VPN zu erreichen
    - prüft Codeabgaben
        + Abgabe muss trotzdem auch in Gildamesh erfolgen
* Moodle
    - enthält Vorlesungsunterlagen und organisatorische Informationen
    - enthält Multiple-Choice-Tests
        + Abgabe muss trotzdem auch in Gildamesh erfolgen

# Aufgaben

* zu finden in Gildamesh
* 6 Aufgabenblätter (Pflicht)
    - je zwei Wochen Bearbeitungszeit
    - <70% korrekte Lösungen => -4 :heart:
    - >=70% korrekte Lösungen => 64 XP
* freiwillige Aufgaben
    - keine Auswirkung auf :heart:
    - i.A. unbegrenzte Bearbeitungszeit
    - geben XP je nach geschätztem Zeitaufwand 
    - Beispiele für Aufgaben:
        + Frage stellen: 1 XP
        + Frage beantworten: 1 XP
        + Sinnvoller Forenbeitrag: 1 XP
        + Inhaltliche Fehler in den Unterlagen finden: 2 XP
        + Anwesenheit in Vorlesung/Übung: 2 XP
        + Aufgabe in der Übung vorstellen: 3 XP
        + Beschreibung eines Begriffs im Wiki: 5 XP
        + Stichworthafte Mitschrift: 5 XP
        + Stundenprotokoll in ganzen Sätzen: 10 XP
        + Zusätzliche Übungsaufgaben lösen: variabel nach Aufwand

# Lebenspunkte

* Start: 10 :heart:
* - 4 :heart: pro Aufgabenblatt mit < 70% korrekten Lösungen
* bei 0 :heart:: keine Klausurzulassung
* 0 :heart: heißt 0 :heart:, es gibt keine Zusatzleben

# Erfahrungspunkte

* gesammelt durch Aufgaben
* ausgegeben für Skills (ohne Auswirkung auf Level)
* gesammelte XP bestimmen das Level

# Level

* Start: Level 0
* pro 64 XP ein Levelup
* höheres Level gibt Bonuspunkte in Klausur (siehe Tabelle)

# Fähigkeiten

* werden mit XP gekauft (ohne Auswirkung auf Level)
* bringen Vorteile in der Veranstaltung und der Klausur
* schwächere Skills sind Voraussetzung für stärkere
* passive Skills wirken dauerhaft
* aktive Skills haben Abkühlzeit
* Tränke sind Gruppenskills
* Basis-Skilltree:
    - Heilung anderer: Heilt sofort 1 HP eines Kommilitonen (64 XP, Cooldown: 4 Wochen)
    - Festmahl herbeirufen: beliebig viel Essen/Getränke in Vorlesung mitbringen (64 XP, Cooldown: 2 Wochen)
    - Fluch der Logorrhö: Zwingt den Dozenten am Anfang der nächsten Vorlesung einen Text von 140 Zeichen vorzulesen (64 XP, Cooldown: 3 Wochen)
    - Post-It des Herumtreibers: DIN A9-Spickzettel für Klausur (64 XP, permanent)
    - Zauberstab: Erhalte doppelte XP bei der Abgabe der nächsten freiwilligen Aufgabe. (64 XP, Cooldown: 2 Wochen)

# Dozentenbewertung

* Feedback-System zur Veranstaltung
* am Ende der Vorlesung 1 oder 0 auf Zettel schreiben und vorne abgeben
* jede 1 gibt dem Dozenten 1 XP
* Dozent hat auch Skills (in Moodle einzusehen)

# World-Events

* sind an Bedingungen geknüpft
* wirken sich auf den ganzen Kurs aus
* werden erst bei Aktivierung bekannt gegeben

# Vorschlag für Aufteilung

* Vorderseite
    - Vereinbarungen
    - HP
    - XP
    - Level
    - Skills
    - World-Events
    - Dozentenbewertung
* Rückseite
    - Die Technik
    - Beispiele für wöchentliche Aufgaben
    - Basis-Skilltree
    - Tabelle: Bonus pro Level