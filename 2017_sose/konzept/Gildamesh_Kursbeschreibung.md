Wir befinden uns an der *Thaumaturgischen Hochschule für Magie (THM)*.
Viele weise und mächtige Infomagier haben einst in diesen Hallen ihr Handwerk erlernt.
Auf den Schultern dieser Riesen (keine echten Riesen, die meisten Infomagier an der THM sind menschlich) haben nun auch Sie Ihre ersten Berührungen mit den magischen Künsten gemacht.
Jetzt wird es Zeit, die einfachen Grundzauber wie *Rekursio*, *Iteratio* oder *Excepto Catchionum* zu größeren Zauberformeln zu verbinden und diese auf gefährlichere Kreaturen als *Stringnattern* und *Integerlinge* anzuwenden.

Der Kurs *Algomantik und Datenkreaturen* wird ihnen dabei helfen, indem Sie ein grundlegendes abstrakteres Verständnis erlangen, wie solche Zauberformeln und Wesen aufgebaut sind, und wie Sie diese beherrschen können.

Für alle Muggel, die von diesen Worten irritiert und verängstigt sind, sowie alle begeisterten Novizen, die mehr über dieses System lernen wollen, gibt es die [Spielregeln](https://homepages.thm.de/~cslz90/kurse/ad17/static/Spielregeln.html).