# Übung 2

## Sprachunabhängige Aufgaben

* Aufgabe 2 (Analyse sozialer Ungleichheiten) von Aufgabenblatt 1 kann in *jeder beliebigen* Programmiersprache abgegeben werden.
* Sie schreiben den Code und führen ihn auf Ihrem Rechner aus.
* Die Konsolenausgabe geben Sie in Dozentron ein.
* Zusätzlich geben Sie ihren Quellcode (als einzelne Quellcode-Datei oder Zip-Archiv) ab.
    - damit wir sehen können, dass Sie tatsächlich die Aufgabe selbst gelöst haben und evtl. Feedback zum Code geben können.

## Invarianten

* Vorsicht Begriffsverwirrung: Invarianz != Invariante
    - Invarianz: Erzwungene Typengleichheit
    - Invariante: Bedingung, die immer gelten soll
    - beide Begriffe haben inhaltlich nichts miteinander zu tun.
* Beispiel für Invarianten: Siehe Abschnitt "Korrektheitsbeweise"

## Korrektheitsbeweise

```
algorithm fak(n: int)
  res := 1
  for i := 1 to n + 1 step 1 do
    res := res * i
  return res
```

Für unsere mathematischen Ausdrücke verwenden wir die Bezeichnung res<sub>k</sub> für den Wert der Variable `res` *vor* dem Schleifendurchlauf für `i = k`. D.h. die Anweisung `res := res * i` entspricht der mathematischen Gleichung  res<sub>i+1</sub> = res<sub>i</sub> &sdot; i.

Schleifeninvariante: res<sub>i</sub> = (i-1)!

Beweis der Schleifeninvariante:
* IA: res<sub>1</sub> = 0! = 1
    - korrekt :heavy_check_mark:
* IV:
    - gegeben: res<sub>i</sub> = (i-1)!
    - frage: res<sub>i+1</sub> = i! ?
    - Beweis:
        + res<sub>i+1</sub> = res<sub>i</sub> &sdot; i
        + res<sub>i+1</sub> = (i-1)! &sdot; i = i!
        + => korrekt :heavy_check_mark:

Vollständiger Korrektheitsbeweis:
* Schleifeninvariante: res<sub>i</sub> = (i-1)! für alle i
* Rückgabewert: res<sub>n+1</sub>
    - res<sub>n+1</sub> = n!
    - => fak(n) = n! :heavy_check_mark:

## O-Notation

### Vergleiche

Die O-Notation ist nur eine obere Schranke. D.h. wenn f &isin; O(n) gilt auch f &isin; O(n²) oder f &isin; O(n<sup>n</sup>).

Bei komplizierteren Ausdrücken dominiert der am schnellsten wachsende Term (wie bei der Grenzwertbetrachtung): O(5n + 3n² + 7n³) = O(n³)

### Berechnung

Frage: Wie kann ich herausfinden in welcher Effizienzklasse mein Algorithmus liegt?

Beispiel:

```
for i := 0 to n step 1 do
    //do something in O(1)
```

Schleife läuft n mal durch => Laufzeit ist n &middot; O(1) = O(n)

```
for i := 0 to sqrt(n) step 1 do
    //do something in O(1)
```

`sqrt` berechnet die Wurzel. => Schleife läuft sqrt(n) mal durch => Laufzeit ist sqrt(n) &middot; O(1) = O(sqrt(n))

```
for i := 0 to sqrt(n) step 1 do
    for j := 0 to n step 1 do
        //do something in O(1)
```

Innere Schleife hat O(n) => Gesamtlaufzeit = O(n) + O(n) + ... + O(n) (insges. sqrt(n) mal) => Gesamtlaufzeit ist in O(n sqrt(n))

Generelle Regel: Laufzeiten von ineinander verschachtelten Schleifen werden multipliziert.

```
for i := 0 to sqrt(n) step 1 do
    // do something in O(1)
for j := 0 to n step 1 do
    //do something in O(1)
```

Gesamtlaufzeit = O(sqrt(n)) + O(n) = O(sqrt(n) + n) = O(n)

Generelle Regel: Laufzeit von aufeinanderfolgenden Befehlen (auch Schleifen) werden addiert.


## Textdateien einlesen

Standard-Beispiel:

```java
Path p = Paths.get("morse-code.txt");
Charset utf8 = StandardCharsets.UTF_8;
try(BufferedReader br = Files.newBufferedReader(p, utf8)) {
    String line = "";
    while((line = br.readLine()) != null) {
        // do something with line
    }
} catch (IOException e) {
    e.printStackTrace();
}
```

Beispiel mit Streams und Lambdas (Java 8):

```java
Path p = Paths.get("morse-code.txt");
Charset utf8 = StandardCharsets.UTF_8;
try {
    Files.lines(p, utf8).forEach(l -> {
        // do something with l
    });
} catch (IOException e) {
    e.printStackTrace();
}
```

Regex helfen beim Parsen von Textdateien. Zum Erstellen des korrekten regulären Ausdrucks helfen [Onlinetools](https://regex101.com/).

## Java-8 Lambdas

### Allgemein

```java
final List<Integer> lst = null;
```

Möchte man in einem Lambda-Ausdruck auf lokale Variablen zugreifen, müssen diese entweder mit dem Schlüsselwort `final` deklariert oder *effectively final* sein. Ersteres bedeutet, dass es für die Variable nur eine einzige Zuweisung geben darf. Letzteres bedeutet, dass es für die Variable nur eine einzige Zuweisung *gibt*, auch wenn sie nicht explizit als `final` deklariert wurde.

```java
IntStream.range(0,10).forEach(i -> lst.add(i));
```

Dieser Lambda-Ausdruck wird vor dem Übersetzen vom Java-Compiler in den folgenden Ausdruck umgewandelt:

```java
IntStream.range(0,10).forEach(new IntConsumer() {
    @Override
    public void accept(int value) {
        lst.add(value);
    }
});
```

Das ist die Syntax zur Definition einer *anonymen inneren Klasse*. Hier wird eine neue Klasse definiert, die das Interface `IntConsumer` implementiert und zugleich wird ein Objekt der Klasse erzeugt. Die Klasse hat keinen Namen und heißt daher *anonyme Klasse*. Man kann sie nur an exakt dieser Stelle im Code verwenden.

Dieser Ausdruck wird vor dem Übersetzen noch einmal in eine ausführlichere Definition verwandelt:

```java
class Lambda1 implements IntConsumer {
    @Override
    public void accept(int value) {
        lst.add(value);
    }
}
IntStream.range(0,10).forEach(new Lambda1());
```

Eigentlich erzeugt ein Lambda-Ausdruck also die Definition einer neuen Klasse, die das jeweilige funktionale Interface implementiert, auf das der Ausdruck passen soll. Die Schreibweise als Lambda-Ausdruck (oder Methodenreferenz) nennt man *syntactic sugar*, weil sie eine einfachere kürzere Syntax für ein Verhalten bietet, das eigentlich nicht die Fähigkeiten der Sprache erweitert, sondern bereits in der Sprache enthalten ist.

### Lambdas mit mehr als einer Zeile Code

Da sich mehrere Studierende überrascht gezeigt haben, dass das geht, hier ein Beispiel für Lambdas mit mehr als einer Zeile Code:

```java
IntStream.range(0,10).map(x -> {
    if(x < 5) {
        return x;
    } else {
        return x*x;
    }
}).forEach(System.out::println);
```
