# Algorithmen und Datenstrukturen 2017
Christopher Schölzel

## Überblick

In diesem Verzeichnis finden Sie alle Unterlagen, die ich für das Modul "Algorithmen und Datenstrukturen" zusammengestellt habe.

* `anleitungen`: Anleitungen für die Tutor\*innen zum Erstellen von Aufgaben
* `aufgaben`: Aufgabentexte für Moodle oder unser Hauseigenes e-Learning-System Dozentron
* `aufgaben/code`: Unittests und Musterlösungen für die Programmieraufgaben in `aufgaben`.
* `folien`: LaTeX-Folien für die Veranstaltung
* `klausur`: Klausur und Probeklausuren als LaTeX-Dokument
* `konzept`: Konzeptdateien für das Gamification-System
* `sonstige_medien`: Ein einzelnes Badge für die dialogische Evaluation
* `uebungen`: Protokoll der Übungsstunden für die Studierenden
* `generated`: Fertige PDFs der Folien
* `include/texmf-cs`: Eigene LaTeX-Templates und -Makros, die zum Übersetzen der Folien benötigt werden

Bitte beachten Sie, dass nicht alle Aufgaben in `aufgaben` fertiggestellt sind und daher auch nicht zu allen Aufgaben entsprechender Code in `aufgaben/code` existiert.
Leider habe ich auch keine übersichtlichen Aufzeichnungen mehr darüber, welche Aufgaben sich genau als Pflichtaufgaben auf den sechs Aufgabenblättern befanden, die ich über das Semester elektronisch herausgegeben habe.

## Autor\*innen

Die Inhalte in dem Ordner `aufgaben` (und zum Teil auch in `anleitungen`) wurden größtenteils von meinen Tutor\*innen erstellt. In diesem Semester waren das die folgenden Personen:

* Alissia Sauer
* Björn Pfarr
* Dennis Tentscher
* Dominik Brühl
* Dominik Fahlenberg
* Elena Gilewskaia
* Jan Sladek
* Julia Jelitzki
* Marcel Hoppe
* Nicola Justus
* Sebastian Engel
* Stefan Glasenhardt
* Thomas Lenz
* Tristan Hisgen

Außerdem hatte ich die tatkräftige Unterstützung meiner Kollegin Valeria Blesius, die die Arbeit der vielen Tutor\*innen koordinierte.

## Übersetzung der Inhalte

### LaTeX-Dokumente

Die LaTeX-Dokumente in diesem Ordner habe ich zu beginn des Semesters noch mit Pdflatex geschrieben, später dann aber mit Lualatex.
Ich empfehle einfach für alle Dokumente Lualatex zu verwenden.
Unabhängig von dem verwendeten Befehl ist es nötig, die LaTeX-Dateien in `includes/texmf-cs` einzubinden.
Jede LaTeX-Distribution bietet Möglichkeiten, solche lokalen texmf-Verzeichnisse einzubinden.
Wer sich damit aber nicht herumschlagen möchte, kann einfach die Umgebungsvariable `TEXINPUTS` wie folgt verwenden:

```bash
BASEDIR=../include/texmf-cs/tex/latex
export TEXINPUTS=.:$BASEDIR/base:$BASEDIR/listings:$BASEDIR/beamer/custom/themes/csthm:
lualatex -interaction=nonstopmode NAME_DER_TEX_DATEI
```

Das Beispiel ist für ein Linux-System geschrieben.
Unter Windows lässt sich natürlich das gleiche mit einem Bash-Script erreichen.

Um die Arbeit für meine Tutor\*innen zu erleichtern habe ich auch bereits ein Script in `folien/lualatex-texinputs.sh` erstellt, das genau diese Aufgabe übernimmt, so dass man einfach den Befehl `./lualatex-texinputs DATEINAME` verwenden kann.

### Markdown-Dokumente

Die Markdown-Dokumente (Endung `.md`), die den Studierenden zugänglich sein sollten wurden mit [Pandoc](https://pandoc.org/) in statische HTML-Dokumente übersetzt.
Dazu muss man den folgenden Befehl in dem Ordner, in dem sich auch diese Datei befindet, ausführen:

```
pandoc -s --mathjax -H konzept/css/markdown_css_pandoc.html -f markdown_github-hard_line_breaks+tex_math_dollars -t html NAME.md -o NAME.html
```

Dabei steht `NAME` für den Namen der Markdown-Datei (ohne Endung), die übersetzt werden soll.

## Lizenzen

Alle Bilder, Quellcodes, Texte und andere Materialien, die von mir oder meinen Tutor\*innen für dieses Modul erstellt wurden entlasse ich im Rahmen der [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)-Lizenz in die Gemeinfreiheit.

Dies gilt für alle Dateien in dem Ordner in dem sich diese Datei befindet sowie den entsprechenden Unterordnern.
Dateien, die Werke von Dritten enthalten und unter einer anderen Lizenz stehen sind im folgenden aufgelistet.
Wenn LaTeX-Dateien Code von Dritten enthalten, ist das innerhalb der Datei gekennzeichnet.

### Pixabay-Bilder: [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)

Einige Bilder auf den Folien stammen von [Pixabay](https://pixabay.com/de/) und wurden bereits unter der CC0 veröffentlicht.
Da diese damit gleich behandelt werden können wie meine Werke und die meiner Tutor\*innen sind sie nicht im einzelnen aufgelistet.

### Illustrationen von Julia Jelitzki: [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)

Die handgezeichneten Basilisten, Speichereichen und Knotlinge stammen aus der Feder meiner Tutorin Julia Jelitzki, die diese unter die Lizenz CC-BY 4.0 gestellt hat. Dazu zählen die folgenden Bilder:

* `folien/img/arrayList*.*`
* `folien/img/AdjacencyMatrix.png`
* `folien/img/binary_tree.pdf`
* `folien/img/DisjointSet.pdf`
* `folien/img/Graph.png`
* `folien/img/GraphDirected.png`
* `folien/img/Hashtable.png`
* `folien/img/ìddfs4.pdf`
* `folien/img/IntegerArrayList*.*`
* `folien/img/Kruskal.pdf`
* `folien/img/LinkedList*.*`
* `folien/img/Rouletteranunkula.png`
* `folien/img/RunzligeRanunkula2.png`
* `folien/img/Schlafbaum.pdf`
* `folien/img/Sorted_ArraySmalpng.png`
* `aufgaben/AB4/AB4-GraphZyklen/Knotlinge.png`
* `klausur/img/SingeltonRedSm.png`
* `klausur/img/SingeltonTransparentSm.png`


### Fxemoji: [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)

Alle Bilder mit dem Namensschema `uXXXXX-name.pdf` stammen von [Fxemoji](https://github.com/mozilla/fxemoji).

### Twitter Emoji: [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)

The Bird emoji `folien/img/twemoji_1f426_bird.pdf` stammt von [Twemoji](https://twemoji.twitter.com/content/twemoji-twitter/en.html).

### Andere Bilder

* `folien/img/Karte_V1_5.jpg` und `konzept/Karte_V1_5.jpg`: (c) [WorldAnvil](https://www.worldanvil.com)
* `folien/img/Christopher_gamified.png`: (c) [Face Your Manga](https://www.faceyourmanga.com/)
* `folien/img/419px-2kg_Gewicht.jpg`: [CC-By-sa 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en), [LoKiLeCh, Wikimedia](https://commons.wikimedia.org/wiki/File:2kg_Gewicht_freigeschnitten.jpg).
* `folien/img/snake-tree.jpg`: (c) [Strangehistory.net](http://www.strangehistory.net/2015/10/11/the-snake-tree/)
* `folien/img/moodle-logo.pdf`: (c) [Moodle Pty Ltd](https://moodle.org/mod/page/view.php?id=8338&forceview=1)
* `include/texmf-cs/tex/latex/base/img/MNI-logo.pdf`: (c) [Technische Hochschule Mittelhessen](https://www.thm.de)
* `folien/img/THM_gamified.pdf`: THM-Logo (c) [Technische Hochschule Mittelhessen](https://www.thm.de)
* `folien/img/Aminoacids_table.pdf`: [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) (Mouagip, Wikimedia)

## Danksagungen

Das Projekt wäre in dieser Größe nicht ohne weitere Unterstützung möglich gewesen.

Die Finanzierung und Qualifikation der Tutor\*innen und die didaktische Beratung lief über das Projekt Klasse in der Masse (KIM) unter der Leitung von Gisa von Marcard an unserem Zentrum für kooperatives Lehren und Lernen (ZekoLL).
Hier möchte ich insbesondere Daniela Kamutzki, Marion Heiser, Meike Hölscher, Fabian Rudzinski, Janine Crow und Sabine Fidorski danken.
Außerdem hat mir Jennifer Heiny vom ZekoLL bei der Erstellung und Auswertung einer speziell auf die Veranstaltung angepassten Umfrage geholfen.

Für die restliche Finanzierung der Tutor\*innen bedanke ich mich beim Dekanat des Fachbereichs MNI, insbesondere dem Studiendekan Prof. Dworschak.

Auch für die Inhalte der Vorlesung habe ich mich frei an den mentalen und zeitlichen Ressourcen meiner Kolleg\*innen bedient. Hier möchte ich Prof. Letschert, Prof. Gogol-Döring, Prof. Dominik, Prof. Just, Eric Hartmann und Peter Koch hervorheben.

Für diese Veranstaltung habe ich zeitweise das Dreifache meiner eigentlichen Arbeitsstunden investiert.
Meiner Lebensgefährtin Annina Hofferberth danke ich daher ganz besonders für ihre Geduld, ihre Unterstützung und ihre inhaltlich didaktische Beratung.

Last but not least möchte ich auch meinen Studierenden danken, die mit ihrem Engagement und ihrem umfangreichen und unermüdlichen konstruktiven Feedback maßgeblich zum Erfolg der Veranstaltung und den Verbesserungen für die folgenden Semester beigetragen haben.
