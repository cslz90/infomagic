# Klausur AuD SS 2017

## Themen

* Foliensatz 01 - Array-Listen in Java
* Foliensatz 02 - Verkettete Listen in Haskell
* Foliensatz 03 - O-Notation
* Foliensatz 04 - Gier + Backtracking
* Foliensatz 05 - Teile und Herrsche + Dynamische Programmierung
* Foliensatz 06 - Listen, Iteratoren, Stacks, Queues
* Foliensatz 07 - Sortieren von Listen
* Foliensatz 08 - Suchen in Listen
* Foliensatz 09 - Traversierung von Bäumen (Tiefensuche, Breitensuche, Iterative Tiefensuche)
* Foliensatz 10 - Binäre Suchbäume, Bäume als Arrays, Algorithmen für Spielbäume
* Foliensatz 11 - Graphen: Definition, Eigenschaften, Traversierung
* Foliensatz 12 - Graphen: Dijkstra, Prioritätswarteschleifen
* Foliensatz 13 - Graphen: Kruskal, Disjunkte Mengen
* Foliensatz 14 - Assoziative Arrays, Hashtabellen

## Aufgabentypen

### Reproduktion

* Pseudocode für einen besprochenen Algorithmus angeben
* Algorithmische Technik beschreiben
* Effizienzklassen für bekannte Algorithmen angeben
* Effizienzklassen für die Operationen bekannter Datenstrukturen angeben
* Bedingungen nennen, unter denen ein Algorithmus korrekt/optimal ist

### Anwendung

* Effizienzklasse von vorgegebenem Pseudocode bestimmen
* Vergleich von Effizienzklassen
* Pseudocode vorgegeben, Frage: Was macht der Algorithmus?
* Pseudocode für Algorithmus angeben und Effizienz bestimmen
* Schleifeninvariante beweisen (für einfachen Pseudocode mit einer Schleife)
* Baum/Graph gegeben: In welcher Reihenfolge werden Knoten besucht?
    - bei Breitensuche (pre-order)
    - bei Tiefensuche
* Beispieldaten angeben für die ein Algorithmus in den worst-case läuft.
* Einen besprochenen Algorithmus "von Hand" ausführen und Variablenwerte aufschreiben
* Algorithmische Technik erkennen
* Reihe von Problemen: Welche Datenstruktur ist geeignet?
* Datenstruktur im Speicher zeichnen
* Eingabebeispiel für das ein Algorithmus gut/schlecht/nicht funktioniert
* Vergleich von zwei Algorithmen anhand ihrer Effizienzklasse

### Transfer

* Vereinfache folgenden Pseudocode, so dass der Algorithmus in O(...) liegt
* Variante eines besprochenen Algorithmus auf ihre Eigenschaften untersuchen
* Fehler in Pseudocode (oder Java-Code) finden
* Pseudocode zur Lösung eines Problems (mit bekanntem Algorithmus)
* Eigenen Algorithmus für kleines Problem schreiben und Eigenschaften diskutieren.
* Eigenen kleinen Algorithmus implementieren mit vorgegebener Effizienzklasse.

## Konkrete Ideen

### Schleifeninvariante

* Beweisen Sie die Schleifeninvariante des folgenden Algorithmus:
    ```
    algorithm poly31(x: int, coeffs: int[])
      p := 1
      for c in coeffs do
        // invariant: p = 31^(i) * c[0] + 31^(i-1) * c[1] + ... + 31^0 * c
        p := p * 31 + c
    ```

### Algorithmische Techniken

* Welche der folgenden Eigenschaften sind charakteristisch für einen Algorithmus der die Technik X verwendet?
    - Multiple-Choice antworten
* Liste von Pseudocode: Welche algorithmische Technik wird hier beschrieben?

### Datenstrukturen allgemein

* Welche Datenstruktur/abstrakter Datentyp ist für die folgenden Probleme/Anforderungen geeignet?
    - Speichern von bereits bewerteten Spielständen bei Minimax/Alpha-Beta
        + Hinzufügen in O(1)
        + Finden in O(1)
    - Speichern von nach Häufigkeit sortierten Passwörtern
        + Maximum entfernen in O(log n)
        + Neues Passwort einfügen in O(log n)
    - Speichern von einer Liste von Musikstücken.
        + Neue Liste mit nur einem Genre/Album/Künstler erzeugen in O(n)
        + Iterieren in O(n)
        + Zwei Positionen tauschen in O(1)
    - Soziales Netzwerk von Facebook-Profilen
        + Finden aller Freunde in O(1)
    - Datentypen mit Subtypenrelation ("implements")
        + Herausfinden ob Typ A subtyp von Typ B ist in O(1)
    - Fragen für 20-Questions
        + finden der nächsten Frage in O(1)

### O-Notation

* Tabelle mit besprochenen Datenstrukturen und typischen Operationen
    - Was ist die beste Schätzung für die Effizienzklasse?
* Ist O(|V|² + |E|) im worst case schlimmer, oder O(|V| + |E| log |E|)?
    - Sie haben zwei Algorithmen A und B mit diesen Laufzeiten
    - Welchen würden sie wählen für |E| in O(1), O(log |V|), O(|V|), O(|V|²)?
* Code vorgeben, Effizienzklasse bestimmen
    * zwei schleifen ineinander, eine konstant
    * zwei schleifen ineinander
    * zwei schleifen nacheinander
    * zwei schleifen ineinander, äußere durchläuft queue
    * rekursion mit log n

### Bäume

* Beispielbaum gegeben, Frage: In welcher Reihenfolge werden Knoten besucht?
    - Breitensuche
    - Tiefensuche (pre-order)
    - Iterative Tiefensuche
* Baum gegeben: Geben Sie ein Haystack-Element an für das der Speicherverbrauch von DFS/BFS minimal/maximal ist.
* Zeichnen Sie einen Binären Suchbaum mit den Elementen 4, 7, 13, 5, 1, -4
* Zeichnen Sie den Spielbaum beginnend mit der folgenden TTT-Spielsituation
    - Wie wäre die Minimax-Bewertung für die einzelnen Spielzüge?
* Warum werden in der Regel nur vollständige Binärbäume als Array abgespeichert? Nennen Sie ein Problem, das in einer Worst-Case-Situation bei einem unvollständigen Baum entstehen könnte. [Diese Frage herausgeben?]

### Sortieren

* Beschreiben Sie den Mergesort-Algorithmus als Pseudocode.

* Beschreiben Sie irgendeinen Sortieralgorithmus als Pseudocode.

* Definieren Sie einen Algorithmus removeEven, der gerade Elemente aus einer Liste entfernt.
    - Was ist die Effizienzklasse? Hängt diese von der Art der verwendeten Liste und/oder der Art der Iteration ab?

* Insertion-Sort vorgeben
    - Funktionsweise beschreiben
    - Effizienzklasse bestimmen
    - Auf Beispieldaten ausführen (Ergebnis nach jedem äußeren Schleifendurchlauf)

### Suchen

* Sie sollen das genaue Alter einer Person erraten. Sie dürfen nur konkrete Zahlen raten. Je nach ihrer Antwort gibt es drei mögliche Reaktionen:
    - Haben Sie richtig geraten, gratuliert Ihnen die Person zur Lösung der Aufgabe.
    - Haben Sie zu niedrig geschätzt, fühlt Ihr Gegenüber sich geschmeichelt.
    - Haben Sie zu hoch geschätzt, wirkt Ihr Gegenüber verärgert.

  Beschreiben Sie eine Technik, wie Sie mit möglichst wenig Rateversuchen die Lösung finden. Gehen Sie davon aus, dass Sie abschätzen können, dass die Person zwischen 40 und 56 ist. Wieviele Fragen brauchen Sie im schlimmsten Fall, um die Lösung zu finden?

### Dijkstra

* Geben Sie ein Beispiel mit negativen Kanten an, für das Dijkstra nicht funktioniert.
* Mit welcher Art von Graphen arbeitet Dijkstra?
    - Gerichtet, ungerichtet, beide?
    - Gewichtet, ungewichtet, beide?
    - Verbunden, unverbunden, beide?

### Kruskal

* Nennen Sie die Kanten aus folgendem Graphen, die der Kruskal-Algorithmus für einen minimalen Spannbaum auswählt (in der richtigen Reihenfolge).
* Hat Kruskal auch Probleme mit negativen Kanten? Warum/Warum nicht?
* Mit welcher Art von Graphen arbeitet Kruskal?
    - Gerichtet, ungerichtet, beide?
    - Gewichtet, ungewichtet, beide?
    - Verbunden, unverbunden, beide?

### Hashes

* Autogenerierte Hash-Funktion für eigene Set-Implementierung: Warum geht das nicht?

