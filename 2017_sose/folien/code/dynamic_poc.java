interface GameState {
  List<GameState> moves();
  boolean isWin();
}

interface TTable {
  ScoredState get(GameState st);
  void store(ScoredState st);
}

class ScoredState {
  public double score;
  public GameState state;
  public ScoredState(double score, GameState state) {
    this.score = score;
    this.state = state;
  }
}

class Algorithms {
  public ScoredState alphaBeta(GameState state) {
    return alphaBeta(state, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
  }
  public ScoredState alphaBeta(GameState state, double alpha, double beta, TTable table) {
    if (state.isWin()) {
      return new ScoredState(-1, null);
    }
    double best = new ScoredState(alpha, null);
    for(GameState move: state.moves()) {
      ScoredState sst = alphaBetaTT(state, -beta, -maxScore, table);
      sst.score *= -1;
      if(sst.score > best.score) {
        best = sst;
        if(best.score >= beta) {
          break;
        }
      }
      if(best.move == null) {
        return new ScoredState(0, null);
      } else {
        return best;
      }
    }
  }
  public ScoredState alphaBetaTT(GameState state, double alpha, double beta, TTable table) {
    ScoredState res = table.get(state);
    if (res != null) {
      return res;
    }
    res = alphaBeta(state, alpha, beta, table);
    table.store(res);
    return res;
  }
}