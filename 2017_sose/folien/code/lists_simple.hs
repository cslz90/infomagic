data Lst a = Nil | Cons a (Lst a) 
  deriving (Show)

size lst = case lst of
  Nil      -> 0
  Cons h t -> 1 + size t

add a lst = case lst of
  Nil      -> Cons a Nil
  Cons h t -> Cons h (add a t)

get idx lst = case lst of
  Nil      -> Nothing
  Cons h t -> case idx of 
    0 -> Just h
    n -> get (n-1) t

set a idx lst = case lst of
  Nil      -> Nil
  Cons h t -> case idx of
    0 -> Cons a t
    n -> Cons h (set a (n-1) t)

remove idx lst = case lst of
  Nil      -> Nil
  Cons h t -> case idx of 
    0 -> t
    n -> Cons h (remove (n-1) t)

insert a idx lst = case lst of
  Nil -> Nil
  Cons h t -> case idx of
    0 -> Cons a (Cons h t)
    n -> Cons h (insert a (n-1) t)

main = do
  let lst = Cons 1 (Cons 2 Nil) :: Lst Integer
  let lst2 = Cons 7 lst
  let lst3 = add 5 lst2
  let lst4 = insert 10 1 (remove 2 (set 0 3 lst3))
  let get0 = get 0
  putStrLn $ "Lst3:      " ++ show lst3
  putStrLn $ "size Lst3: " ++ show (size lst3)
  putStrLn $ "Lst3[2]:   " ++ show (get 2 lst3)
  putStrLn $ "Lst3[0]:   " ++ show (get0 lst3)
  putStrLn $ "Lst4:      " ++ show lst4