data Basilist a = Nil | Body a (Basilist a) 
  deriving (Show)

size Nil        = 0
size (Body h t) = 1 + size t

add a Nil        = Body a Nil
add a (Body h t) = Body h (add a t)

get idx Nil        = Nothing
get 0   (Body h t) = Just h
get idx (Body h t) = get (idx-1) t

set a idx Nil        = Nil
set a 0   (Body h t) = Body a t
set a idx (Body h t) = Body h (set a (idx-1) t)

remove idx Nil        = Nil
remove 0   (Body h t) = t
remove idx (Body h t) = Body h (remove (idx-1) t)

insert a 0 Nil          = Body a Nil
insert a idx Nil        = Nil
insert a 0   (Body h t) = Body a (Body h t)
insert a idx (Body h t) = Body h (insert a (idx-1) t)

main = do
  let lst = Body 1 (Body 2 Nil) :: Basilist Integer
  let lst2 = Body 7 lst
  let lst3 = add 5 lst2
  let lst4 = insert 10 1 (remove 2 (set 0 3 lst3))
  let get0 = get 0
  putStrLn ("Lst3:      " ++ show lst3)
  putStrLn ("size Lst3: " ++ show (size lst3))
  putStrLn ("Lst3[2]:   " ++ show (get 2 lst3))
  putStrLn ("Lst3[0]:   " ++ show (get0 lst3))
  putStrLn ("Lst4:      " ++ show lst4)