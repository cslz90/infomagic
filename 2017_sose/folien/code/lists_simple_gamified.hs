data Basilist a = Nil | Body a (Basilist a) 
  deriving (Show)

-- size lst = case lst of
--   Nil      -> 0
--   Body h t -> 1 + size t

size Nil = 0
size (Body h t) = 1 + size t

add a lst = case lst of
  Nil      -> Body a Nil
  Body h t -> Body h (add a t)

get idx lst = case lst of
  Nil      -> Nothing
  Body h t -> case idx of 
    0 -> Just h
    n -> get (n-1) t

set a idx lst = case lst of
  Nil      -> Nil
  Body h t -> case idx of
    0 -> Body a t
    n -> Body h (set a (n-1) t)

remove idx lst = case lst of
  Nil      -> Nil
  Body h t -> case idx of 
    0 -> t
    n -> Body h (remove (n-1) t)

insert a idx lst = case lst of
  Nil -> Nil
  Body h t -> case idx of
    0 -> Body a (Body h t)
    n -> Body h (insert a (n-1) t)

main = do
  let lst = Body 1 (Body 2 Nil) :: Basilist Integer
  let lst2 = Body 7 lst
  let lst3 = add 5 lst2
  let lst4 = insert 10 1 (remove 2 (set 0 3 lst3))
  let get0 = get 0
  putStrLn $ "Lst3:      " ++ show lst3
  putStrLn $ "size Lst3: " ++ show (size lst3)
  putStrLn $ "Lst3[2]:   " ++ show (get 2 lst3)
  putStrLn $ "Lst3[0]:   " ++ show (get0 lst3)
  putStrLn $ "Lst4:      " ++ show lst4