import Data.Foldable as F
import Data.Monoid
import Data.Traversable as T
import Control.Applicative (pure, (<*>))

-- cause I love haskell's (infinite) list comprehensions
evens :: [Int]
evens = map fromIntegral [x | x <- [0..], even x]

-- a custom list type
-- either empty (Nil) or an element and the rest of the list (Cons element tail)
data Lst a = Nil | Cons a (Lst a)
  deriving (Eq)

-- implement custom show instance so that the string representation is:
-- "{1,2,3}" instead of "(Cons 1 (Cons 2 (Cons 3 Nil)))"
instance Show a => Show (Lst a) where
  show lst =
    -- the beauty of pattern matching used explicitly and in the function arguments ;)
    case lst of
      Nil -> "{}"
      c@(Cons _ _) -> "{" ++ show' c ++ "}"
    where
      show' (Cons v Nil) = show v
      show' (Cons v c@(Cons _ _)) = show v ++ ", " ++ show' c

-- prepend an element in O(1)
cons :: a -> Lst a -> Lst a
cons a lst =
  case lst of
    Nil -> Cons a Nil
    cons@(Cons v lst) -> Cons a cons

-- append an element in O(n)
append :: a -> Lst a -> Lst a
append v lst =
  case lst of
    Nil -> Cons v Nil
    (Cons v2 rest) -> Cons v2 (append v rest)

-- map each element of the list in O(n)
lstMap :: (a -> b) -> Lst a -> Lst b
lstMap fn Nil = Nil
lstMap fn (Cons v rest) = Cons (fn v) (lstMap fn rest)

-- transform a haskell list into own list representation
fromList :: [a] -> Lst a
fromList [] = Nil
fromList (x:xs) =
  let rest = fromList xs
    in cons x rest

lstZip :: Lst a -> Lst b -> Lst (a, b)
lstZip Nil Nil = Nil
lstZip _ Nil = Nil
lstZip Nil _ = Nil
lstZip (Cons x xs) (Cons y ys) = cons (x,y) (lstZip xs ys)


-- ================================ here starts the cool haskell stuff ;-)

-- any datastructre that doesn't have a map function is useless ;-)
instance Functor Lst where
  fmap fn Nil = Nil
  fmap fn (Cons v tl) = Cons (fn v) (fmap fn tl)

instance Foldable Lst where
  -- 'foldMap' maps each element to a monoid using the given monoid instance and combines them using the monoid
  -- (http://hackage.haskell.org/package/base-4.9.1.0/docs/Data-Monoid.html)
  foldMap toMonoid Nil = mempty
  foldMap toMonoid (Cons v tl) = mappend (toMonoid v) (foldMap toMonoid tl)

-- Foldable gives us cool things like (see http://hackage.haskell.org/package/base-4.9.1.0/docs/Data-Foldable.html):
-- sum, maximum, minimum, product, toList (in this case: Lst a -> [a])

instance Traversable Lst where
  traverse fn lst = case lst of
                      Nil -> pure Nil
                      -- i don't understand the <*> but it works ;) (http://hackage.haskell.org/package/base-4.9.1.0/docs/Control-Applicative.html)
                      Cons v rest -> fmap cons (fn v) <*> (traverse fn rest)

-- Traversable gives as traverse & sequence;
-- the ultimate combinators for combination of a Lst of (monadic) actions

-- compute the length based-on foldl :)
lstLength :: Lst a -> Int
lstLength = F.foldl (\acc x -> acc+1) 0

-- use foldl to reverse a Lst
lstReverse :: Lst a -> Lst a
lstReverse = F.foldl (flip cons) Nil -- 'flip' swaps the order of 2 arguments of a function

-- compute the mean of Lst
mean :: Lst Int -> Float
mean lst =
  let size = lstLength lst
    in fromIntegral (F.sum lst) / fromIntegral size

evenOpt :: Int -> Maybe Int
evenOpt x = if even x
              then Just x
              else Nothing

-- main entry point
main = do
  -- create a Lst of the first 20 even Ints
  let lst = fromList (take 20 evens)
  let lst2 = fromList [1..20]
  -- use traverse to check if the lists only contain even numbers; produces Lst (Maybe Int)
  let nothings = T.traverse evenOpt lst2
  let onlyEvens = T.traverse evenOpt lst
  putStrLn $ "Lst: " ++ show lst
  putStrLn $ "Length is: " ++ show (lstLength lst)
  putStrLn $ "Minimum is: " ++ show (F.minimum lst)
  putStrLn $ "Maximum is: " ++ show (F.maximum lst)
  putStrLn $ "Mean is: " ++ show (mean lst)
  putStrLn $ "Reverse is: " ++ show (lstReverse lst)
  putStrLn $ "Zipped: " ++ show (lstZip lst2 lst)
  putStrLn $ "No evens: " ++ show nothings
  putStrLn $ "Evens: " ++ show onlyEvens
