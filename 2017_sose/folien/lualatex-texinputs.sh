#!/bin/bash
BASEDIR=../include/texmf-cs/tex/latex
export TEXINPUTS=.:$BASEDIR/base:$BASEDIR/listings:$BASEDIR/beamer/custom/themes/csthm:
lualatex -interaction=nonstopmode $@
