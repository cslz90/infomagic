# Literaturempfehlungen für AuD - Sommersemester 2017
Christopher Schölzel

## Einführung - Deutsch

1. M. Dietzfelbinger, K. Mehlhorn und P. Sanders, *Algorithmen und Datenstrukturen* Online-Ausg.. Deutschland, Berlin-Heidelberg: Springer, 2014.
    * [in der THM-Bibliothek als eBook](https://hds.hebis.de/thm/Record/HEB342961438) vorhanden

## Einführung - Englisch

1. R. Lafore, *Data Structures and Algorithms in Java*, 2. Ausgabe. England, London: Pearson Education, 2002.
    * ISBN: 978-0672324536
    * arbeitet mit älterer Java-Version
    * wird aber in internationalen Fachkreisen als "bestes Einsteigerwerk" bezeichnet
    * bestellt für die THM-Bibliothek

## Nachschlagewerke

1. T.H. Cormen, C.E. Leiserson, R.L. Rivest und C. Stein, *Introduction to Algorithms*, 3. Ausgabe. Cambrige, MA: MIT Press, 2009.
    * ISBN: 9780262533058
    * Bekannt als "der CLRS"
    * *Das* Standardwerk für Algorithmen und Datenstrukturen
    * In der THM-Bibliothek vorhanden
2. T.H. Cormen, C.E. Leiserson, R.L. Rivest und C. Stein, *Algorithmen - Eine Einführung*, 4. Ausgabe. Deutschland, München: De Gruyter Oldenbourg, 2013.
    * deutsche Übersetzung des CLRS
    * ISBN: 978-3486748611
3. Steven S. Skiena, *The Algorithm Design Manual*, 2. Ausgabe. Deutschland, Berlin-Heidelberg: Springer, 2011.
    * ebenfalls ein sehr gutes Standardwerk
    * andere (praktischere?) Herangehensweise als CLRS
    * In der THM-Bibliothek vorhanden
