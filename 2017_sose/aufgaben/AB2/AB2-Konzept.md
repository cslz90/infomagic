# Aufgabenblatt 2

|Nr | Thema                         | Gamifiziert                          | Dauer in min  | %   | Ref gamifiziert  |
|---| ---                           | ---                                  | ---           | --- |---               |
|01 | Mergesort                     | Divide et impera und doch geordnet!  | 40            | 35  |
|02 | Wählen von Techniken          | Das Wissen um das Wie                | 20            | 17  | 
|03 | Erkennen von Techniken        | Schund oder Kostbarkeiten            | 40            | 35  |
|04 | AoC                           | Passwörter der Orks II               | 15            | 13  |
