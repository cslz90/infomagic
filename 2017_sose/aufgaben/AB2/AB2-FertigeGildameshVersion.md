## Aufgabe 1: Divide et impera und doch geordnet!

*Wie Sie inzwischen bestimmt wissen, sind Basilisten sehr ordentliche Datenkreaturen und lassen sich liebend gerne sortieren.*
*Eine recht effiziente Form dies mit einem Zauber zu bewirken ist "Mergesort":*

*Bei diesem Zauber werden die Basilisten erst vorsichtig aufgetrennt, um sie nachher in entsprechender Reihenfolge wieder zusammenzuführen.*

*Da ein einzelner Kopf eines Basilisten in Panik verfallen würde, wenn man ihn vom Rest trennen würde versucht man immer möglichst gleichmäßig aufzuteilen.*
*Zudem lässt man üblicherweise ca. zwei Köpfe beisammen, um die Kreaturen nicht zu sehr zu stressen.*

*Um unnötigen Stress für Novizen und Basilisten zu vermeiden, wird dieser Zauber erst mal auf Arrays geübt.*
*Diese sind vor allem den ArrayBasilisten sehr ähnlich, jedoch sind sie von der Handhabung viel einfacher.*

*Aus der Thaumaturgischen Hochschule der Magie haben Sie den groben Aufbau eines Merge-Zaubers erfahren.*
*Nun liegt es an Ihnen diesen Zauber zu perfektionieren, damit Sie eines Tages auch auf das Sortieren von ArrayBasilisten mit Mergesort umsteigen können.*
*Bedenken Sie hierbei, dass es sich um einen rekursiven Zauber handelt.*

```java
package mergesort;

public class MergeSort {

    public static void sort(int[] arr) {
        sort(arr, 0, arr.length);
    }

    // fromIndex inclusive, toIndex exclusive
    private static void sort(int[] arr, int fromIndex, int toIndex) {
        //
    }

    // first subarray: fromIndex inclusive, middleIndex exclusive
    // second subarray: middleIndex inclusive, toIndex exclusive
    static void merge(int[] arr, int fromIndex, int middleIndex, int toIndex) {
        //
    }

}
```

[Abgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/67)

## Aufgabe 2: Das Wissen um das Wie

*Es gibt verschiedene Zaubertechniken, die mal mehr, mal weniger gut geeignet sind, gewisse Aufgaben zu bewältigen.*
*Als gewiefter Novize müssen Sie lernen diese Techniken zu erkennen und die zu wählen, die für das vorhandene Problem am besten geeignet ist.*

### Aufgabe 2.1: Die Suche nach dem doppelten Element

*Als Sie mit einem befreundeten Novizen spazieren gehen, kommen Sie an eine Gruppe sortierter Arraybasilisten vorbei.*
*Neugierig lauschen Sie den Köpfen der Basilisten, welche verbittert am streiten sind.*
*Sie hören, dass jeweils ein ranghöherer Basilistenkopf unzufrieden ist, weil der ihm folgende einen gleichwertigen Intergerling gefressen hat.*
*Das verstößt natürlich gegen die Nahrungsordnung und diese ist den sortierten Arraybasilisten als ordnungsliebende Datenkreaturen besonders wichtig.*

*Es ist Ihnen vor lauter wütendem Gezische nicht möglich herauszuhören welche Köpfe den nun die falschen Intergelinge gefressen haben.*
*Auch die Basilisten scheinen vor Aufregung selbst den Überblick verloren zu haben.*
*Als erfahrener Basilistenzüchter können Sie es nicht verantworten die Kreaturen in einem so gestressten Zustand zurückzulassen.*

*Gemeinsam mit Ihrem Kollegen überlegen Sie, wie Sie den armen Basilisten aus dieser Krise helfen können.*
*Sie konnten aus dem wilden Durcheinander in Erfahrung bringen, dass der erste Kopf immer eine 0 zu sich nimmt.*
*Jeder weitere Kopf darf nur den nächsthöheren Integerling fressen, hierbei bevorzugen alle jedoch die niedrigsten Integerlinge.*
*Bei jedem Arraybasilisten hat sich genau ein Kopf nicht daran gehalten und das selbe wie sein Vorgänger gefressen.*
*Hieraus ergibt sich, dass ein Basilist mit n Köpfen Integerlinge von `0` bis `n-2` zu sich genommen hat. `z.B. [0,1,2,2,3,4,5]`*

*Um herauzufinden, welcher Kopf denn nun das falsche Futter erwischt hat, schlägt Ihr Wegbegleiter vor, jeden Basilistenkopf zu Fragen, was er gefressen hat.*
*Sie erkennen jedoch sofort, dass dieser naive Ansatz, nach den Weisen von O, `n` Zauberstabbewegungen (`O(n)`) benötigt.*
*Da Sie gerne so schnell es geht zum Kaffee und Kuchen wieder Zuhause sein wollen, machen sie einen Alternativvorschlag, der weniger Zauberstabbewegungen abverlangt.*

*Sie fragen jeweils erst den mittleren Kopf des Basilisten nach seiner Mahlzeit und finden so heraus, ob sich alle vorigen Basilisten an die Rangordnung beim Fressen gehalten haben.*
*So können Sie mit jeder Frage die Hälfte der verbleibenden verdächtigen Köpfe des Basilisten ausschließen.*
*Ihr Verfahren braucht nur `O(log n)` und ermöglicht Ihnen so eine zeitige Rückkehr.*
*Ihr Kollege ist überwältigt von Ihrem brillianten Einfall. Als guter Freund nennen Sie ihm natürlich die von Ihnen angewandte Zauber-Technik.*


### Aufgabe 2.2: Die Trennung mit dem minimalen Unterschied

*Auch in der thaumaturgischen Welt ist nicht alles für die Ewigkeit.*
*Sie nehmen eine Halbtagsstelle in einer Scheidungsagentur an, wo Sie das Vermögen zwischen beiden Parteien möglichst gerecht aufteilen sollen.*

*Da Sie nicht bei jeder Scheidung selber rechnen wollen, entwickeln sie einen Zauber der das Eigentum nach Wert aufteilt, sodass jede Partei möglichst gleich viel erhält.*
*Um den Zauber zu beschleunigen wenden Sie eine Technik an, bei der Zwischenergebnisse behalten werden, damit es möglichst wenig Zeit braucht alle Aufteilungen zu erstellen um die gerechteste Eigentumsverteilung zu finden.*
*Wie heißt diese Technik?*

### Aufgabe 2.3: Sudoku

*Auch in der magischen Welt hat das knifflige Zahlenrätsel viele begeisterte Spieler.*
*Sie wollen sich trainieren, um auf einem Turnier eine Reise zu gewinnen.*
*Hierzu entwickeln Sie einen Zauber. Eine Möglichkeit wäre jedes Feld mit jeder Zahl ausfüllen, das wäre jedoch sehr ineffizient.*

*Sie entscheiden sich dafür, dass Ihr Zauber direkt die Einhaltung der Regeln prüfen soll und nur gültige Ergebnisse zurückliefert.*
*Wenn für eine beliebiges Feld überprüft wurde welche Zahl passt, wird sie eingetragen und für die weiteren Plätze wird rekursiv das selbe getan.*
*Was ist der Name dieser Technik?*

### Aufgabe 2.4: Reisen

*Die "Highspeed Thaumaturgic Teleportation Product Services" (HTTPS) haben die schnellste sichere Methode für Reisen zwischen zwei Orten entwickelt.*
*Heutzutage gibt es kaum noch einen Ort der nicht über das Netzwerk der HTTPS erreichbar ist.*
*Aus Sicherheitsgründen gibt es nur begrenzte Reiselängen, so kommt man nicht unbedingt direkt von A nach B.*

*Sie entwickeln einen Zauber der Ihnen für Reisen zwischen zwei nicht direkt miteinander verbundenen Orten die schnellste Strecke heraussucht.*
*Dieser Zauber wird an jedem HTTPS Portal aufgerufen und wählt das Reiseziel des Portals aus, das am schnellsten zum endgültigen Reiseziel führt.*


[Abgabe in Moodle](https://moodle.thm.de/mod/quiz/view.php?id=163935)

## Aufgabe 3: Schund oder Kostbarkeiten
*Ein alter Zwerg, der schon lange in dunklen Minen nach Schätzen gräbt, ist in einer vielversprechenden Höhle auf eine seltsame Schriftsammlung gestoßen. Er hat mit seinen trüben Augen nicht viel Bedarf an Schriftstücken.*
*Deshalb will er sie an jemanden Verkaufen, der bereit ist ihm Juwelen dafür zu geben, deren funkelnde Farbpracht ihn in dunklen Schächten daran erinnern sollen, warum er schon so lange gräbt. Da sein Augenlicht durch die Jahre im Düsteren geschwächt ist, bringt er Ihnen die Sammlung, damit Sie ihm helfen sie einzuschätzen.*

*Schauen Sie sich die Schriftstücke an und erläutern Sie ihm, um welche Zauber-Techniken es sich handelt. Achten Sie dabei darauf, dass sie dem Zwerge erklären, woran man diese Techniken erkennt. So helfen Sie dem ruhigen alten Herrn sich vor bösartigen Händlern zu schützen, da sie ihn wegen seiner Unwissenheit übervorteilen würden.*

### Aufgabe 3.1

```
algo insertInOrder(sorted_lst, elem):
if elem.value < sorted_lst.first.value:
  elem.next = sorted_lst.first
else:
  i := sorted_lst.first
  while i.hasnext and i.next.value < elem.value:
    i := i.next
  if i.hasnext:
    elem.next := i.next
  i.next := elem
  ```

### Aufgabe 3.2

```
algo insertInOrder(sorted_lst, low, high, elem):
mid := sorted_lst[low.getIndex + (high.getIndex - low.getIndex) / 2]
if elem < low:
  low.prepend(elem)
else if elem > high:
  high.append(elem)
else if elem < mid:
  if mid.getIndex - low.getIndex > 1:
    call insertInOrder(sorted_lst, low, mid, elem)
  else:
    mid.prepend(elem)
else:
  if high.getIndex - mid.getIndex < 1:
    call insertInOrder(sorted_lst, mid, high, elem)
  else:
    mid.append(elem)
```

### Aufgabe 3.3

```
algo maze():
  while onPath:
    moveForward()
    if pathsToChoseFrom.size >= 1:
      if notInForks:
        forks.append(thisFork)
      path := choseNextPath
      markAsTaken(path)
      pathsToChoseFrom.remove(path)
    if deadEnd:
      goToLastFork()
      unmarkPathsFromDeadEndToLastFork()
    if finish:
      return markedAsTaken
    if pathsToChoseFrom = 0:
      return "no way through this maze"
      ```
### Aufgabe 3.4
```
algo maze():
  possiblePaths := 0
  moveForward()
  if pathsToChoseFrom.size >= 1:
    for path in pathsToChoseFrom:
      possiblePaths += path.maze()
  if Finish:
    possiblePaths += 1
  return possiblePaths
  ```

[Abgabe in Moodle](https://moodle.thm.de/mod/assign/view.php?id=164217)

## Aufgabe 4: Passwörter der Orks II

*Seit Ihrem letzten Besuch im Dorf Ihres befreundeten Orks, haben die Orks an ihren kryptografischen Systemen gearbeitet. Da diese sich lange Passwörter, wie beispielsweise "grunz", nicht merken können, haben sie diese nun durch Zahlen ersetzt. Die Wachen wechseln das Passwort täglich. Das jeweils neue Passwort wird von ihnen jeden Morgen aus dem Zahlenbereich 0 (inklusive) bis 1024 (exklusive) ausgewürfelt. Leider ist den Wachen nicht bewusst, dass ihr kryptografisches System eine Sicherheitslücke hat: Sie haben vergessen festzulegen, dass man Fremden gegenüber immer nur mit "Ja" oder "Nein" antworten sollte. So kommt es nicht selten vor, dass eine Wache mit "Ja", "Nein, mein Passwort ist kleiner" oder "Nein, mein Passwort ist größer" antwortet.*

*Die Orks haben vergessen Sie über diese Änderungen aufzuklären. Um dennoch Ihren befreundeten Ork besuchen zu können, müssen Sie den Wachen wie gewohnt die Passwörter aufsagen. Keine Sorge: Wie gehabt, vergessen diese schnell wieder, dass Sie bereits ein Passwort genannt haben. Allerdings können Sie nicht mehr einfach alle Passwörter aufzählen. Da die Wachen das Passwort aus Sicherheitsgründen täglich ändern, vergessen sie es regelmäßig selbst. Um es nachzuschlagen muss eine der Wachen zur Garnison ans andere Ende des Dorfes laufen. Würden Sie alle möglichen 1024 Passwörter aufzählen, wäre ein ganzer Tag vergangen. Somit wäre auch das Passwort wieder ein anderes.*

*Ihr befreundeter Ork hat, genau wie die Wachen, Probleme sich die Passwörter zu merken. Sein schlechtes Gedächtnis macht er, aber mit Einfallsreichtum wieder wett. So kommt es, dass er eine Strategie entwickelt hat mit der man möglichst schnell an das Passwort kommt.*

*Er ist ziemlich schnell im Kopfrechnen und kann sich gut merken, dass das Passwort zwischen 0 und 1024(exklusive) liegt. Um mit jedem Schritt näher ans Ziel zu kommen Frag er erst ob die Zahl der mittlere Wert aus der Menge ist. Je nach Antwort der Wachen, kommt er entweder durch, weiß dass er nochmal die Hälfte der letzten Zahl dazu gerechneten Zahl nochmal hinzufügen muss (Passwort > Zahl) oder die Hälfte dieser  Zahl abziehen muss. In anderen Worten immer den Mittelwert zwischen seiner ermittelten Ober- und Untergrenze.*

*So könnte er zB 512 + 256 - 128 + 64 rechnen um sich an die Zahl  704 anzunähern.*

[Aufgabe in Dozentron](http://dozentron.mni.thm.de/input_output_tasks/66)
