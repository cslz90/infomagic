# Erkennen von Techniken

## Musterlösung

### Aufgabe 1:

**Antwort:** Brute Force

**Begründung:** die Schleife läuft einfach über die ganze Liste und überprüft die Werte der allen Elemente, was typisch für Brute Force ist.


### Aufgabe 2:

**Antwort:** Teile-und-Herrsche

**Begründung:** der Algorithmus zerlegt das Problem rekursiv in zwei Teile (in diesem Fall von `low` bis `mid` und von `mid` bis `high`), bis das Problem so klein wird, dass es beherrscht werden kann.


### Aufgabe 3:

**Antwort:** Dynamisches Programmieren

**Begründung:** hier werden Teilergebnisse in der Variable `pathsToChoseFrom` gespeichert, was für die dynamische Programmierung spricht.


### Aufgabe 4:

**Antwort:** Backtracking

**Begründung:** wenn der Algorithmus eine Sackgasse findet, geht der Algorithmus bis zur letzten Gabel zurück, wo er einen alternativen Weg folgen kann. Diese Fähigkeit eigene Schritte zurückzuverfolgen ist das Merkmal von Backtracking.
