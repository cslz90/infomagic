## Erkennen von Techniken
*Ein alter Zwerg, der schon lange in dunklen Minen nach Schätzen gräbt, ist in einer vielversprechenden Höhle auf eine seltsame Schriftsammlung gestoßen. Er hat mit seinen trüben Augen nicht viel Bedarf an Schriftstücken.*
*Deshalb will er sie an jemanden Verkaufen, der bereit ist ihm Juwelen dafür zu geben, deren funkelnde Farbpracht ihn in dunklen Schächten daran erinnern sollen, warum er schon so lange gräbt. Da sein Augenlicht durch die Jahre im Düsteren geschwächt ist, bringt er Ihnen die Sammlung, damit Sie ihm helfen sie einzuschätzen.*

*Schauen Sie sich die Schriftstücke an und erläutern Sie ihm, um welche Zauber-Techniken es sich handelt. Achten Sie dabei darauf, dass sie dem Zwerge erklären, woran man diese Techniken erkennt. So helfen Sie dem ruhigen alten Herrn sich vor bösartigen Händlern zu schützen, da sie ihn wegen seiner Unwissenheit übervorteilen würden.*

### Aufgabe 1

`algo insertInOrder(sorted_lst, elem):
if elem.value < sorted_lst.first.value:
  elem.next = sorted_lst.first
else:
  i := sorted_lst.first
  while i.hasnext and i.next.value < elem.value:
    i := i.next
  if i.hasnext:
    elem.next := i.next
  i.next := elem`

### Aufgabe 2

`algo insertInOrder(sorted_lst, low, high, elem):
mid := sorted_lst[low.getIndex + (high.getIndex - low.getIndex) / 2]
if elem < low:
  low.prepend(elem)
else if elem > high:
  high.append(elem)
else if elem < mid:
  if mid.getIndex - low.getIndex > 1:
    call insertInOrder(sorted_lst, low, mid, elem)
  else:
    mid.prepend(elem)
else:
  if high.getIndex - mid.getIndex < 1:
    call insertInOrder(sorted_lst, mid, high, elem)
  else:
    mid.append(elem)
`

### Aufgabe 3

`algo maze():
  while onPath:
    moveForward()
    if pathsToChoseFrom.size >= 1:
      if notInForks:
        forks.append(thisFork)
      path := choseNextPath
      markAsTaken(path)
      pathsToChoseFrom.remove(path)
    if deadEnd:
      goToLastFork()
      unmarkPathsFromDeadEndToLastFork()
    if finish:
      return markedAsTaken
    if pathsToChoseFrom = 0:
      return "no way through this maze"
      `
### Aufgabe 4

`algo maze():
  possiblePaths := 0
  moveForward()
  if pathsToChoseFrom.size >= 1:
    for path in pathsToChoseFrom:
      possiblePaths += path.maze()
  if Finish:
    possiblePaths += 1
  return possiblePaths
  `

[Abgabe in Moodle](https://moodle.thm.de/mod/assign/view.php?id=164217)
