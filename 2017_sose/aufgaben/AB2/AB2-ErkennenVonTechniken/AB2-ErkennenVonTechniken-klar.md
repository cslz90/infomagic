# Aufgabenblatt 2

## Erkennen von Techniken

Ordnen Sie in den folgenden Aufgaben jedem Pseudocode-Algorithmus eine Technik zu.
Mögliche Techniken sind:
* Teile-und-Herrsche
* Dynamisches Programmieren
* Gier
* Brute Force
* Backtracking.

Begründen Sie Ihre Antworten.

### Aufgabe 1

`algo insertInOrder(sorted_lst, elem):
if elem.value < sorted_lst.first.value:
  elem.next = sorted_lst.first
else:
  i := sorted_lst.first
  while i.hasnext and i.next.value < elem.value:
    i := i.next
  if i.hasnext:
    elem.next := i.next
  i.next := elem`

### Aufgabe 2

`algo insertInOrder(sorted_lst, low, high, elem):
mid := sorted_lst[low.getIndex + (high.getIndex - low.getIndex) / 2]
if elem < low:
  low.prepend(elem)
else if elem > high:
  high.append(elem)
else if elem < mid:
  if mid.getIndex - low.getIndex > 1:
    call insertInOrder(sorted_lst, low, mid, elem)
  else:
    mid.prepend(elem)
else:
  if high.getIndex - mid.getIndex < 1:
    call insertInOrder(sorted_lst, mid, high, elem)
  else:
    mid.append(elem)
`

### Aufgabe 3

`algo maze():
  while onPath:
    moveForward()
    if pathsToChoseFrom.size >= 1:
      if notInForks:
        forks.append(thisFork)
      path := choseNextPath
      markAsTaken(path)
      pathsToChoseFrom.remove(path)
    if deadEnd:
      goToLastFork()
      unmarkPathsFromDeadEndToLastFork()
    if finish:
      return markedAsTaken
    if pathsToChoseFrom = 0:
      return "no way through this maze"
      `
### Aufgabe 4

`algo maze():
  possiblePaths := 0
  moveForward()
  if pathsToChoseFrom.size >= 1:
    for path in pathsToChoseFrom:
      possiblePaths += path.maze()
  if Finish:
    possiblePaths += 1
  return possiblePaths
  `
