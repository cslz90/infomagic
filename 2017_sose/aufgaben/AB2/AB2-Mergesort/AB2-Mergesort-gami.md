## Mergesort

*Wie Sie inzwischen bestimmt wissen, sind Basilisten sehr ordentliche Datenkreaturen und lassen sich liebend gerne sortieren.*
*Eine recht effiziente Form dies mit einem Zauber zu bewirken ist "Mergesort":*

*Bei diesem Zauber werden die Basilisten erst vorsichtig aufgetrennt, um sie nachher in entsprechender Reihenfolge wieder zusammenzuführen.*

*Da ein einzelner Kopf eines Basilisten in Panik verfallen würde, wenn man ihn vom Rest trennen würde versucht man immer möglichst gleichmäßig aufzuteilen.*
*Zudem lässt man üblicherweise ca. zwei Köpfe beisammen, um die Kreaturen nicht zu sehr zu stressen.*

*Um unnötigen Stress für Novizen und Basilisten zu vermeiden, wird dieser Zauber erst mal auf Arrays geübt.*
*Diese sind vor allem den ArrayBasilisten sehr ähnlich, jedoch sind sie von der Handhabung viel einfacher.*

*Aus der Thaumaturgischen Hochschule der Magie haben Sie den groben Aufbau eines Merge-Zaubers erfahren.*
*Nun liegt es an Ihnen diesen Zauber zu perfektionieren, damit Sie eines Tages auch auf das Sortieren von ArrayBasilisten mit Mergesort umsteigen können.*
*Bedenken Sie hierbei, dass es sich um einen rekursiven Zauber handelt.*

```java
package mergesort;

public class MergeSort {

    public static void sort(int[] arr) {
        sort(arr, 0, arr.length);
    }

    // fromIndex inclusive, toIndex exclusive
    private static void sort(int[] arr, int fromIndex, int toIndex) {
        //
    }

    // first subarray: fromIndex inclusive, middleIndex exclusive
    // second subarray: middleIndex inclusive, toIndex exclusive
    static void merge(int[] arr, int fromIndex, int middleIndex, int toIndex) {
        //
    }

}
```

[Link zur Abgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/67)