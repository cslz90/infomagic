# Mergesort

Eine Aufgabe in Dozentron

Zeit: 40 Minuten

Implementieren Sie Mergesort um ein Array zu sortieren. Dieser Algorithmus arbeitet nach dem Prinzip "Teile und Herrsche":

Er zerlegt das Array in zwei Teile, sortiert sie rekursiv und verschmilzt am Ende die Teile wieder (engl. merge).

Der folgende Code kann als Hilfe dienen:

```java
package mergesort;

public class MergeSort {

    public static void sort(int[] arr) {
        sort(arr, 0, arr.length);
    }

    // fromIndex inclusive, toIndex exclusive
    private static void sort(int[] arr, int fromIndex, int toIndex) {
        //
    }

    // first subarray: fromIndex inclusive, middleIndex exclusive
    // second subarray: middleIndex inclusive, toIndex exclusive
    static void merge(int[] arr, int fromIndex, int middleIndex, int toIndex) {
        //
    }

}
```

Die Vorgabe liegt [hier](https://moodle.thm.de/mod/resource/view.php?id=164169).

Getestet werden zwei Methoden:
* die Methode `sort`, die ein `int`-Array übernehmen und sortieren muss
* die Methode `merge`, die zwei sortierte `arr`-Subarrays von `fromIndex` bis `middleIndex` und von `middleIndex` bis `toIndex` verschmelzt.

> Beachten Sie, dass untere Grenzen immer inklusive und obere Grenzen immer exklusive gegeben sind.
> Ein Beispiel zur Methode `sort`: Das erste Element ist `arr[fromIndex]`, das letzte Element ist `arr[toIndex - 1]`.
