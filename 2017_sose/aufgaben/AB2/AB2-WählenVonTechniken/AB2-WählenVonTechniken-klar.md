# Wählen von Techniken

Ein Multiple-Choice-Test in Moodle

Zeit: 4 * 5 min = 20 Minuten

## 1. Die Suche nach dem doppelten Element

**Aufgabe**: Gegeben ist ein sortiertes Array der Länge `n`, das die Zahlen von `0` bis zu `n - 2` enthält. Zum Beispiel: `[0, 1, 2, 3, 4, 4, 5]`. Eine Zahl kommt zweimal vor. Die Aufgabe besteht darin, die duplizierte Zahl zu finden.

**Lösung**: Eine naive Lösung ist das Array durchzulaufen und jedes Element und dessen Nachfolger auf Äquivalenz zu überprüfen, aber sie fordert `O(n)` Operationen. Alternativ kann man eine Funktion schreiben, die nur das Element in der Mitte überprüft. Abhängig vom Index und dem Wert des Elementes kann man den linken oder rechten Teil des Arrays mit Hilfe der gleichen Funktion überprüfen. Das ergibt `O(log n)` Komplexität. Wie heißt die Technik, die in der zweiten Lösung angewendet wurde?

- Gier
- Brute Force
- Backtracking
+ Teile und herrsche
- Dynamische Programmierung


## 2. Die Trennung mit dem minimalen Unterschied

**Aufgabe**: Gegeben ist die Menge der Zahlen `M`, die in zwei Teile `M1` und `M2` getrennt werden muss. Die Aufgabe ist die gegebene Menge so teilen, dass die absolute Differenz zwischen Summen der Elemente `M1` und `M2` minimal ist. Zum Beispiel: `M = [1, 4, 6]`, dann ist die richtige Trennung `M1 = [1, 4]` und `M2 = [6]`, weil `sum(M2) - sum(M1) = (6) - (1 + 4) = 6 - 5 = 1`.

**Lösung**: Eine mögliche Lösung ist eine rekursive Funktion, die ein Subarray übernimmt und zwei Trennungen (inklusive und exklusive des ersten Elements des Subarrays) vergleicht. Der Nachteil dieser Lösung besteht darin, dass einige Varianten der Trennungen mehrere Male gerechnet werden. Um dieses Problem zu beheben kann man die gerechneten Ergebnisse im Cache speichern und wiederverwenden. Wie heißt diese Technik?

- Gier
- Brute Force
- Backtracking
- Teile und herrsche
+ Dynamische Programmierung


## 3. Sudoku

**Aufgabe**: Gegeben ist ein zweidimensionales 9x9-Array, dass teilweise gefüllt ist und das Sudoku-Feld repräsentiert. Das Ziel von Sudoku ist es, alle leeren Felder auf dem Spielfeld so ausfüllen, dass jede Zeile, jede Spalte und jedes 3x3-Subarray genau einmal die Zahlen von 1 bis 9 enthält.

**Lösung**: Der Algorithmus, für den man sich geistig am wenigsten anstrengen muss, ist die Überprüfung aller möglichen Belegungen der freien Felder auf Regelkonformität. Es gibt aber einen effektiveren Algorithmus, der kontrolliert, ob die Regeln eingehalten werden, bevor er eine Zahl in ein leeres Feld des Sudokus einträgt. Nach der Kontrolle überprüft das Programm rekursiv weitere leere Plätze und Zahlen um die Lösung zu finden oder kehrt einen Schritt zurück. Wie heißt diese Technik?

- Gier
- Brute Force
+ Backtracking
- Teile und herrsche
- Dynamische Programmierung


## 4. Reisen

**Aufgabe**: Eine Person will von Stadt A nach B fahren. Es gibt keinen direkten Weg zwischen A und B, deswegen muss die Person durch andere Städte reisen. Welchen Weg wählt die Person?

**Lösung**: Eine mögliche Lösung ist es, wenn die Person an jedem Punkt der Reiseroute zu der Stadt fährt, die am schnellsten zum Ziel führt. Wie heißt diese Technik? Beachten Sie: dieser Algorithmus garantiert nicht, dass der minimale Weg gefunden wird.

+ Gier
- Brute Force
- Backtracking
- Teile und herrsche
- Dynamische Programmierung
