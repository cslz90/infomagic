## Wählen von Techniken

*Es gibt verschiedene Zaubertechniken, die mal mehr, mal weniger gut geeignet sind, gewisse Aufgaben zu bewältigen.*
*Als gewiefter Novize müssen Sie lernen diese Techniken zu erkennen und die zu wählen, die für das vorhandene Problem am besten geeignet ist.*

### Aufgabe 1: Die Suche nach dem doppelten Element

*Als Sie mit einem befreundeten Novizen spazieren gehen, kommen Sie an eine Gruppe sortierter Arraybasilisten vorbei.*
*Neugierig lauschen Sie den Köpfen der Basilisten, welche verbittert am streiten sind.*
*Sie hören, dass jeweils ein ranghöherer Basilistenkopf unzufrieden ist, weil der ihm folgende einen gleichwertigen Intergerling gefressen hat.*
*Das verstößt natürlich gegen die Nahrungsordnung und diese ist den sortierten Arraybasilisten als ordnungsliebende Datenkreaturen besonders wichtig.*

*Es ist Ihnen vor lauter wütendem Gezische nicht möglich herauszuhören welche Köpfe den nun die falschen Intergelinge gefressen haben.*
*Auch die Basilisten scheinen vor Aufregung selbst den Überblick verloren zu haben.*
*Als erfahrener Basilistenzüchter können Sie es nicht verantworten die Kreaturen in einem so gestressten Zustand zurückzulassen.*

*Gemeinsam mit Ihrem Kollegen überlegen Sie, wie Sie den armen Basilisten aus dieser Krise helfen können.*
*Sie konnten aus dem wilden Durcheinander in Erfahrung bringen, dass der erste Kopf immer eine 0 zu sich nimmt.*
*Jeder weitere Kopf darf nur den nächsthöheren Integerling fressen, hierbei bevorzugen alle jedoch die niedrigsten Integerlinge.*
*Bei jedem Arraybasilisten hat sich genau ein Kopf nicht daran gehalten und das selbe wie sein Vorgänger gefressen.*
*Hieraus ergibt sich, dass ein Basilist mit n Köpfen Integerlinge von `0` bis `n-2` zu sich genommen hat. `z.B. [0,1,2,2,3,4,5]`*

*Um herauzufinden, welcher Kopf denn nun das falsche Futter erwischt hat, schlägt Ihr Wegbegleiter vor, jeden Basilistenkopf zu Fragen, was er gefressen hat.*
*Sie erkennen jedoch sofort, dass dieser naive Ansatz, nach den Weisen von O, `n` Zauberstabbewegungen (`O(n)`) benötigt.*
*Da Sie gerne so schnell es geht zum Kaffee und Kuchen wieder Zuhause sein wollen, machen sie einen Alternativvorschlag, der weniger Zauberstabbewegungen abverlangt.*

*Sie fragen jeweils erst den mittleren Kopf des Basilisten nach seiner Mahlzeit und finden so heraus, ob sich alle vorigen Basilisten an die Rangordnung beim Fressen gehalten haben.*
*So können Sie mit jeder Frage die Hälfte der verbleibenden verdächtigen Köpfe des Basilisten ausschließen.*
*Ihr Verfahren braucht nur `O(log n)` und ermöglicht Ihnen so eine zeitige Rückkehr.*
*Ihr Kollege ist überwältigt von Ihrem brillianten Einfall. Als guter Freund nennen Sie ihm natürlich die von Ihnen angewandte Zauber-Technik.*


### Aufgabe 2: Die Trennung mit dem minimalen Unterschied

*Auch in der thaumaturgischen Welt ist nicht alles für die Ewigkeit.*
*Sie nehmen eine Halbtagsstelle in einer Scheidungsagentur an, wo Sie das Vermögen zwischen beiden Parteien möglichst gerecht aufteilen sollen.*

*Da Sie nicht bei jeder Scheidung selber rechnen wollen, entwickeln sie einen Zauber der das Eigentum nach Wert aufteilt, sodass jede Partei möglichst gleich viel erhält.*
*Um den Zauber zu beschleunigen wenden Sie eine Technik an, bei der Zwischenergebnisse behalten werden, damit es möglichst wenig Zeit braucht alle Aufteilungen zu erstellen um die gerechteste Eigentumsverteilung zu finden.*
*Wie heißt diese Technik?*

### Aufgabe 3: Sudoku

*Auch in der magischen Welt hat das knifflige Zahlenrätsel viele begeisterte Spieler.*
*Sie wollen sich trainieren, um auf einem Turnier eine Reise zu gewinnen.*
*Hierzu entwickeln Sie einen Zauber. Eine Möglichkeit wäre jedes Feld mit jeder Zahl ausfüllen, das wäre jedoch sehr ineffizient.*

*Sie entscheiden sich dafür, dass Ihr Zauber direkt die Einhaltung der Regeln prüfen soll und nur gültige Ergebnisse zurückliefert.*
*Wenn für eine beliebiges Feld überprüft wurde welche Zahl passt, wird sie eingetragen und für die weiteren Plätze wird rekursiv das selbe getan.*
*Was ist der Name dieser Technik?*

### Aufgabe 4: Reisen

*Die "Highspeed Thaumaturgic Teleportation Product Services" (HTTPS) haben die schnellste sichere Methode für Reisen zwischen zwei Orten entwickelt.*
*Heutzutage gibt es kaum noch einen Ort der nicht über das Netzwerk der HTTPS erreichbar ist.*
*Aus Sicherheitsgründen gibt es nur begrenzte Reiselängen, so kommt man nicht unbedingt direkt von A nach B.*

*Sie entwickeln einen Zauber der Ihnen für Reisen zwischen zwei nicht direkt miteinander verbundenen Orten die schnellste Strecke heraussucht.*
*Dieser Zauber wird an jedem HTTPS Portal aufgerufen und wählt das Reiseziel des Portals aus, das am schnellsten zum endgültigen Reiseziel führt.*


[Abgabe in Moodle](https://moodle.thm.de/mod/quiz/view.php?id=163935)
