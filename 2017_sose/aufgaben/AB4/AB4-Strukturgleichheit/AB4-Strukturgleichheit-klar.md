# Strukturgleichheit

Verwenden Sie für die folgende Programmierübung Ihre Lösung aus der Aufgabe "Hieroglyphen aus dem Altlisp'schen Reich" vom Aufgabenblatt 3.

## Aufgabe 1

Implementieren Sie die, im folgenden Interface `ExpTree` aufgelistete Methode `isomorph` in jeder Ihrer Klassen (`AddTree`, `SubTree`, `MulTree`, `DivTree` und `NumTree`). Sie dürfen hierbei gerne mit roher Gewalt vorgehen. Wie auch in der Methode `evaluate` dürfen und sollen Sie eine `NoSuchElementException` werfen, wenn einer der Operatorknoten keinen Kindknoten besitzt.

```java
package tree;

import java.util.List;
import java.util.NoSuchElementException;

public interface ExpTree {

  public abstract int evaluate() throws NoSuchElementException;  /* evaluate the value of this tree */

  public abstract List<ExpTree> children(); /* return all children */

  public abstract void appendChild(ExpTree child); /* append child to this tree */

  public abstract boolean isomorph(ExpTree other) throws NoSuchElementException; /* check if `this` and `other` are isomorph */

}
```

## Hinweise

- Denken Sie daran, dass per Definition aus der Aufgabe "Hieroglyphen aus dem Altlisp'schen Reich" ein Knoten eine `java.util.NoSuchElementException` wirft, wenn er **keine** Kinder hat. Dies bedeutet nicht, dass ein Knoten mit nur einem Kind nicht legitim ist.
    - Die Vereinbarung ist, dass ein Operatorknoten wie beispielsweise `SubTree` seine Kindknoten von links nach rechts voneinander subtrahiert. Besitzt dieser nur ein Kind, wird dieses ohne Weiteres zurückgegeben.
- Im Allgemeinen sind zwei Graphen isomorph, wenn ihre Struktur **ohne** Berücksichtigung der Knotenbenennung gleich ist.
    - Beachten Sie allerdings, dass unser Ausdrucksbaum ein gerichteter und gewurzelter Baum ist. Dies erleichtert Ihnen die Arbeit enorm.
    - Beispielsweise wären die ungerichteten Graphen für die Ausdrücke `(+ 3 (* 4 6))` und `(- (/ (+ 5 9)))` strukturgleich.
    - Die Ausdrucksbäume für die Ausdrücke `(+ 3 (* 4 6))` und `(- (/ (+ 5 9)))` sind allerdings nicht strukturgleich.
- Die Tests werden auch die Korrektheit der ursprünglichen Aufgabe vom Aufgabenblatt 3 testen. Sie sollten daher Ihre alte Aufgabe übernehmen.
