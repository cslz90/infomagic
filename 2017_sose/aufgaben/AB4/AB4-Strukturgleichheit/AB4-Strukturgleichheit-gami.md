# Strukturgleichheit

*Die Züchtung der polyverästelten Speichereichen hat Ihnen bereits tiefe Einblicke in die Thematik der Altertumslehre gewährt.*
*Insbesondere Ihr Verständnis für die Hieroglyphen in den Ruinen des Altlisp'schen Reiches hat sich sehr erweitert.*
*Doch Sie merken schnell, je weitgehender Sie sich mit den alten Schriften befassen, desto mehr Fragen kommen auf.*
*Aus purer Neugierde fragen Sie einen erfahrenen Infomagister, welcher an der Thaumaturgische Hochschule der Magie lehrt, nach Hilfestellungen.*
*Dieser gibt Ihnen den Tipp, die Hieroglyphen einmal untereinander zu vergleichen.*
*Mit einem breiten Grinsen im Gesicht stellt er Ihnen die Frage, was die folgenden Inschriften gemeinsam haben.*
```lisp
(-
  (* 3 (+ 3 6))
  (/ 2))
```
```lisp
(*
  (- 9)
  (- (/ 20 4) 13))
```
*Als Sie auf eine Erklärung drängen, grinst der Infomagister nur noch breiter, wendet sich ab und redet etwas vor sich hin.*
*Sie konnten seine Worte leider nicht ganz verstehen, doch Sie meinen etwas wie "... Isomorphie ... doch nicht so schwer sein." gehört zu haben.*

*Nach einem langen Tag an der Thaumaturgischen Hochschule der Magie kommen Sie zuhause an.*
*Nebst vielen anderen Dingen, wovon Sie viele für sehr unbedeutsam halten, die Sie heute gelernt haben, beschäftigt Sie noch immer die Frage des Infomagisters.*
*Noch lange nicht erschöpft vom Alltag, greifen Sie zu Stift und Pergament und machen sich Notizen zu dem Problem.*
*Als Vorbild für Ihre Analyse nehmen Sie Ihre Schöpfung: Die polyverästelten Speichereichen.*
*Schnell fällt Ihnen auf, dass* **nicht** *die Bedeutung der Inschriften die Antwort auf die Frage sein kann.*

## Aufgabe 1

*Sie kommen auf die Idee, die Anatomie Ihrer polyverästelten Speichereichen zu vergleichen.*
*Da diese identisch mit der Struktur der Hieroglyphen ist, muss in diesem Ansatz die Lösung liegen.*

```java
package tree;

import java.util.List;
import java.util.NoSuchElementException;

public interface ExpTree {

  public abstract int evaluate() throws NoSuchElementException;  /* evaluate the value of this tree */

  public abstract List<ExpTree> children(); /* return all children */

  public abstract void appendChild(ExpTree child); /* append child to this tree */

  public abstract boolean isomorph(ExpTree other) throws NoSuchElementException; /* check if `this` and `other` are isomorph */

}
```

## Hinweise

*Am nächsten Tag fangen Sie den erfahrenen Infomagister direkt vor seinem Büro ab.*
*Auf weiteres drängen Ihrerseits, verrät er Ihnen schließlich doch die folgenden Dinge.*

- *Denken Sie daran, dass eine polyverästelte Speichereiche ohne Knotenfrüchte (hiervon ausgenommen ist die polyverästelte Speichereiche* `NumTree` *) nicht legitim ist.*
    - *Eine polyverästelte Speichereiche mit genau einer Knotenfrucht ist allerdings legitim.*
- *Im Allgemeinen nennt man zwei Rudel von Knotlingen isomorph, wenn ihre Rudelstruktur ohne Berücksichtigung der Namen gleich ist.*
    - *Der Infomagister betont allerdings, dass Sie ein einfacheres Problem vor sich haben und behauptet, dass Ihre polyverästelten Speichereichen gewurzelt und gerichtet sind.*
- *Da Ihnen das alles nicht weiter hilft, gibt er Ihnen den sehr wertvollen Tipp über den Begriff Isomorphie zu recherchieren.*
    - *Im Laufe Ihres Gesprächs fallen außerdem die Namen drei großer Infomagister: Aho, Hopcroft und Ullman.*

## Abgabe

Den Link zur Abgabe auf Dozentron finden Sie [hier: http://dozentron.mni.thm.de/submission_tasks/77](http://dozentron.mni.thm.de/submission_tasks/77).
