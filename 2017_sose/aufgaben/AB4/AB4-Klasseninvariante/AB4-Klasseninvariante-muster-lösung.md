# Klasseninvariante - Musterlösung

Dauer zur Erstellung der Musterlösung: 3h

## Aufgabe 1)

Der gegebene Binäre Suchbaum kann keine Werte doppelt enthalten. 
Das sieht man unter anderem an der `add`-Funktion, die ein Element in einen Baum einfügt (`add(BTree<E>, E)`).
Nur wenn wirklich ein Element hinzugefügt wurde, wird das Größenfeld (`size`-Attribut) inkrementiert.
Die Methode `addChild(BNode<E>, E)` gibt stets dann `true` zurück, wenn ein neues Element eingefügt wurde. 
Exsistiert der einzufügende Wert bereits im Baum, returniert sie `false`.

## Aufgabe 2)

### Aufgabe 2.1)

Sei T ein leerer BTree.
Im Konstruktor wird die Wurzel (`root`-Knoten) des Baums auf None gesetzt und das Größenfeld auf 0 initialisiert.
Deswegen gilt für T:

1)
```latex
nodes(root(T)) = \emptyset
```

2)
```latex
size(T) = 0
```

#### Klasseninvariante 1)

Die Klassenvariante 1 gilt nach dem Aufruf des Konstruktors.

```latex
\begin{align*}
size(T) &= | \; nodes(root(T)) \; | && \;\; mit \; nodes(root(T)) = \emptyset, size(T) = 0 \\
0 &= | \; \emptyset \; | \\
0 &= 0 \\
t&rue
\end{align*}
```
q.e.d. :blossom:

#### Klasseninvariante 2)

Die Klassenvariante 2 gilt nach dem Aufruf des Konstruktors.

```latex
\begin{align*}
\forall N \in nodes(root(T)) : &(\forall A \in nodes(left(N)) : value(A) < value(N)) \wedge \\
&(\forall B \in nodes(right(N)) : value(B) > value(N)) \\
mit \;\; nodes(root(T)) &= \emptyset \\
\\
\forall N \in \emptyset : &(\forall A \in nodes(left(N)) : value(A) < value(N)) \wedge \\
&(\forall B \in nodes(right(N)) : value(B) > value(N)) \\
mit \;\; \forall x \in \emptyset : p &= true \\
\\
&true
\end{align*}
```
q.e.d. :blossom:

### Aufgabe 2.2)

Für die öffentlichen Prozeduren `root(BTree<E>)`, `isEmpty(BTree<E>)` und `size(BTree<E>)` gilt der Klassenkontrakt vor und nach jedem Aufruf, da sie den Zustand der Datenstruktur nicht verändern. :blossom:

Im folgenden wird gezeigt, dass die Prozedur `add(BTree<E>, E)` nicht gegen die Klasseninvariante verstößt.

Sei T der Baum vor dem Einfügen eines Elementes E und T' der Baum nach dem Einfügen.

Für T gilt:
1) Vor dem Einfügen sind n Elemente im Binärbaum abgelegt.
```latex
size(T) = n
```
2) Vor dem Einfügen hat der Binärbaum n Knoten.
```latex
| \; nodes(root(T)) \; | = n
```

#### Klasseninvariante 1)

Die Klassenvariante 1 gilt nach dem Aufruf von `add`.

Genau dann, wenn das Element in den Baum eingefügt wurde, wird auch das Größenfeld (`size`-Attribut) inkrementiert.

Es müssen zwei Fälle unterschieden werden:
1) Das einzufügende Element existiert bereits im Baum
```latex
E \in [ \; value(N) \; | \; N <- nodes(root(T)) \; ]
```
In diesem Fall wird nichts am Binärbaum geändert, somit ist T = T' und die Klasseninvariante gilt.
:blossom:

2) Das einzufügende Element existiert noch nicht im Baum
```latex
E \notin [ \; value(N) \; | \; N <- nodes(root(T)) \; ]
```

```latex
\begin{align*}
size(T) + 1 &= | \; nodes(root(T)) \cup \{E\} \; | && \;\; mit \; size(T) = n, | \; nodes(root(T)) \; | = n, E \notin [ \; value(N) \; | \; N \leftarrow nodes(root(T)) \; ] \\
n + 1 &= n + 1 \\
true
\end{align*}
```
q.e.d. :blossom:

#### Klasseninvariante 2)

Die Klassenvariante 2 gilt nach dem Aufruf von `add`.

Es müssen auch für die Klasseninvariante 2 zwei Fälle unterschieden werden:
1) Das einzufügende Element existiert bereits im Baum
```latex
E \in [ \; value(N) \; | \; N <- nodes(root(T)) \; ]
```

In diesem Fall wird nichts am Binärbaum geändert, somit ist T = T' und die Klasseninvariante gilt.
:blossom:

2) Das einzufügende Element existiert noch nicht im Baum
```latex
E \in [ \; value(N) \; | \; N <- nodes(root(T)) \; ]
```

  - Wenn T leer war, gilt nach dem Einfügen:

```latex
\begin{align*}
size(T') &= 1 \\
\\
nodes(root(T')) &= \{ \; BNode(E, None, None) \; \} \\
\\
nodes(left(root(T'))) &= \emptyset \\
nodes(right(root(T'))) &= \emptyset
\end{align*}
```

Und somit:

```latex
\begin{align*}
\forall N \in nodes(root(T')) : &(\forall A \in nodes(left(N)) : value(A) < value(N)) \wedge \\
&(\forall B \in nodes(right(N)) : value(B) > value(N)) \\
mit \;\; nodes(root(T')) &= \{ \; BNode(E, None, None) \; \} \\
\\
\forall N \in \{ \; BNode(E, None, None) \; \} : &(\forall A \in nodes(left(N)) : value(A) < value(N)) \wedge \\
&(\forall B \in nodes(right(N)) : value(B) > value(N)) \\
mit \;\; nodes(left(root(N))) &= \emptyset \\
und \;\; nodes(right(root(N))) &= \emptyset \\
\\
\forall N \in \{ \; BNode(E, None, None) \; \} : &(\forall A \in \emptyset : value(A) < value(N)) \wedge \\
&(\forall B \in \emptyset : value(B) > value(N)) \\
mit \;\; \forall x \in \emptyset : p &= true \\
\\
\forall N \in \{ \; BNode(E, None, None) \; \} : &(true \wedge true) \\
\\
\forall N \in \{ \; BNode(E, None, None) \; \} : &true \\
mit \;\; \forall x : true &= true \\
\\
true
\end{align*}
```
:blossom:

  - Wenn T nicht leer war, gilt nach dem Einfügen:

```latex
\begin{align*}
nodes(root(T')) &= N_{T'} \\
N_{T'} &= \{ \; root(T') \; \} \cup \; nodes(left(root(T'))) \cup \; nodes(right(root(T')))
\end{align*}
```

Beim Einfügen wird an jedem Knoten K von T entschieden, ob der einzufügende Wert E kleiner oder größer als value(K) ist.
(Der Wert E kann nicht gleich value(K) sein, wir befinden uns in der Fallunterscheidung - Fall 2.)
Somit kann bei jedem Knoten jeweils das Problem auf entweder den linken oder den rechten Teilbaum reduziert werden.
Beim rekursiven Abstieg wird die Stelle, an dem der neue Teilbaum eingefügt werden soll (Insertion Position), gesucht.
Solange diese Insertion Position noch nicht gefunden wurde wird die Datenstruktur nicht mutiert, die Klasseninvariante bleibt hier erhalten. :blossom:

Sei B ein Blattknoten aus T, für den die Klasseninvariante gilt.
Nach dem Einfügen gilt 
```latex
\begin{align*}
value(left(B')) < value(B') \wedge right(B') &= None \;\;\; falls \; E < value(B') \\
mit \; value(left(B')) &= E
\end{align*}
```
Das bedeutet, dass E in den linken Teilbaum von B eingefügt wurde.

und 
```latex
\begin{align*}
value(right(B')) > value(B') \wedge left(B') &= None \;\;\; falls \; E > value(B')\\
mit \; value(right(B')) &= E
\end{align*}
```
Das bedeutet, dass E in den rechten Teilbaum von B eingefügt wurde.

Für beide Fälle gilt die Klasseninvariante und somit auch für B'.
Da B' der Knoten ist, der als einziges verändert wurde, gilt die Klasseninvariante für T'.

q.e.d. :blossom:
