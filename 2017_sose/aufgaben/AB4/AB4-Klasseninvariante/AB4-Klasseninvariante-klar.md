# Klasseninvariante

Aufgabenart: Freitextaufgabe

geschätzer Aufwand: ?

Im Folgenden wird der Pseudocode zu einem Binärbaum gegeben.

```
# Definitions for the binary tree

structure BTree<E> { root: BNode<E>, size: Int }

algorithm root(this: BTree<E>)
  return copy(this.root)

algorithm isEmpty(this: BTree<E>)
  return this.root = None

algorithm size(this: BTree<E>)
  return this.size

algorithm add(this: BTree<E>, value: E)
  if isEmpty(this) then
    this.root = singletonNode(value)
    size += 1
  else if addChild(this.root, value) then
    size += 1

# Definitions for nodes of the binary tree

structure BNode<E> { value: E, left: BNode<E>, right: BNode<E> }

algorithm value(this: BNode<E>)
  return copy(this.value)

algorithm left(this: BNode<E>)
  return copy(this.left)

algorithm right(this: BNode<E>)
  return copy(this.right)

algorithm addChild(this: BNode<E>, value: E): boolean
  if value < value(this) then
    if left(this) = None then
      this.left = singletonNode(value)
      return true
    else
      return addChild(left(this), value)
  else if value > value(this) then
    if right(this) = None then
      this.right = singletonNode(value)
      return true
    else
      return addChild(right(this), value)
  else
    return false

# Helper definitions for creating an empty binary tree
# and a node for the binary tree with a value, but no child
# (i.e. the constructors)

algorithm emptyTree(): BTree<E>
  tree = new BTree<E>
  tree.root = None
  tree.size = 0
  return tree

algorithm singletonNode(value: E): BNode<E>
  node = new BNode<E>
  node.value = value
  node.left  = None
  node.right = None
  return node
```

## Aufgabe 1
Kann der oben gegebene Binärbaum Elemente doppelt enthalten oder nicht?
An welchem Ausdruck bzw. an welcher Anweisung lässt sich diese Eigenschaft feststellen?

## Aufgabe 2
Sei N ein Knoten des Binärbaums `BTree`, dann ist nodes(N) die Menge aller Knoten (inklusive N selbst) in N.
nodes(N) ist definiert als:
```latex
\begin{align*}
nodes(None) &= \emptyset \\
nodes(node) &= \{ \; node \; \} \cup nodes(left(node)) \cup nodes(right(node))
\end{align*}
```

Beweisen Sie, dass die folgenden Klasseninvarianten stets für alle `BTree` gelten.

Sei T ein Binärbaum, dann sind die Klasseninvarianten:

#### Klasseninvariante 1

```latex
size(T) = | \; nodes(root(T)) \; |
```
**Beschreibung**: Die Anzahl der Werte, die T speichert, ist stets die Anzahl der Knoten im selben Baum.

#### Klasseninvariante 2

```latex
\begin{align*}
\forall N \in nodes(root(T)) : &(\forall A \in nodes(left(N)) : value(A) < value(N)) \wedge \\
&(\forall B \in nodes(right(N)) : value(B) > value(N))
\end{align*}
```
**Beschreibung**: Für alle Knoten N in einem beliebigen Binärbaum T gilt: Jeder Knoten A des linken Teilbaums hat einen kleineren Wert als N und jeder Knoten B des rechten Teilbaums hat einen größeren Wert als N.

### Aufgabe 2.1
Beweisen Sie, dass die Klasseninvarianten nach dem Aufruf des Konstruktors gelten.
Da der in den Folien verwendete Pseudocode keine "richtigen" Konstruktoren hat, soll im Rahmen dieser Aufgabe die Prozedur `emptyTree()` als Konstruktor angesehen werden.

### Aufgabe 2.2
Sie dürfen davon ausgehen, dass die Invarianten vor dem Aufruf der Prozeduren gelten.
Beweisen Sie, dass die öffentlichen Prozeduren `root(BTree<E>)`, `isEmpty(BTree<E>)`, `size(BTree<E>)` und `add(BTree<E>, E)` nicht gegen die Klasseninvariante verstoßen.
