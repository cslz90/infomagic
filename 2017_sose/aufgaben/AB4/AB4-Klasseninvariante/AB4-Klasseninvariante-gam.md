## Von Immerwahrs und Datenkreaturen / Klasseninvariante

Aufgabenart: Freitextaufgabe

geschätzer Aufwand: ?

*Im Pflanzenkundeunterricht an der Thaumaturgischen Hochschule für Magie haben Sie bereits die Immerwahrs und die binärborkigen Speichereichen erforscht, gezüchtet und zum Blühen gebracht.*
*Um die Pracht der magischen Flora aufrechtzuerhalten, entschließen Sie sich, die Gesundheit der wertvollen, groß gewachsenen Immerwahrs am Stamm der binärborkigen Speichereichen zu kontrollieren.
Mit strengen Kontrollen kann der Bestand der Pflanzen gesichert und somit für eine blumige Zukunft gesorgt werden.*

### Aufgabe 1
*Da die binärborkigen Speichereichen und die Immerwahrs ein eigenes, kleines, magisches Ökosystem bilden, ist es notwendig, die Eigenschaften der Speichereichen ebenfalls zu protokollieren.*
*Sie fragen sich als Erstes, ob es gleichartige Knotenfrüchte mehrfach in dem betrachteten Exemplar der Speichereichen geben kann.*

### Aufgabe 2
*Nach der Untersuchung der Eigenschaften der Speichereichen können Sie sich nun der Immerwahrs am Stamm der Speichereichen widmen.*

#### Aufgabe 2.1
*Manche der Immerwahrs verblühen, wenn die Anzahl der Knotenfrüchte in der binärborkigen Speichereiche nicht mit der Anzahl der Astgabelungen übereinstimmt.*
*Sie müssen also sicherstellen, dass diese Eigenschaft stets erfüllt ist.*

#### Aufgabe 2.2
*Manche der Immerwahrs verblühen, wenn die wichtigste der Eigenschaften der binärborkigen Speichereichen, nicht erfüllt ist:*
*Jede Knotenfrucht im linken Teilbaum muss kleiner sein, als die Knotenfrucht an der betrachteten Astgabelung und jede Knotenfrucht im rechten Teilbaum muss größer sein, als die Knotenfrucht der Astgabelung.*
