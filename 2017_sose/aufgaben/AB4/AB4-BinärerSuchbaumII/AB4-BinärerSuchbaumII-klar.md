# Binärer Suchbaum II

Aufgabenart: Dozentron-Abgabe, die von Unit-Tests geprüft wird.
Geschätzte Dauer zur Lösung der Aufgabe von den Studenten: 60min
Tatsächliche Dauer zur Erstellung der Musterlösung (inklusive UnitTests): 30min

## Aufgabe 1)

In der Aufgabe "Binärborkige Speichereichen I" von AB3 sollte bereits ein binären Suchbaum implementiert werden, in dem `int`s gespeichert werden können.
Verallgemeinern Sie Ihre Implementierung, sodass diese potenziell alle Typen enthalten können.
Realisieren Sie im Package `binarytree` einen binären Suchbaum für alle Datentypen, die vergleichbar sind.
Legen Sie dazu eine Klasse `BinaryTree` in besagtem Package an, die die folgende Schnittstelle implementiert:

```java
package binarytree;

import java.util.Comparator;

public abstract class ABinaryTree<T> {

  protected final Comparator<T> comparator;

  public ABinaryTree(Comparator<T> comparator) {
    this.comparator = comparator;
  }

  /**
   * @return the root node of this tree.
   */
  public abstract ABinaryTreeNode<T> root();

  /**
   * Add a value of type T to this binary tree.
   */
  public abstract void add(T value);

  /**
   * Remove a value of type T from this binary tree.
   */
  public abstract void remove(T value);

  /**
   * Check, if the value of type T exists in this tree.
   */
  public abstract boolean contains(T value);

  /**
   * @return true, if this tree is empty, false otherwise.
   */
  public abstract boolean isEmpty();

  /**
   * The number of elements in this tree.
   */
  public abstract int size();

}
```

Ein einzelner Knoten des `BinaryTree` wird von der Schnittstelle `ABinaryTreeNode` repräsentiert, von der Sie ebenfalls eine Realisierung anfertigen sollen.
Die Schnittstelle dafür wird im Folgenden aufgeführt:

```java
package binarytree;

public abstract class ABinaryTreeNode<T> {

  /**
   * @return the value stored in this node.
   */
  public abstract T value();

  /**
   * @return the left subtree of this node.
   */
  public abstract ABinaryTreeNode<T> left();

  /**
   * @return the right subtree of this node.
   */
  public abstract ABinaryTreeNode<T> right();

}
```

### Hinweise
* Sie können als Vorlage Ihre Implementierung der Aufgabe "Binärborkige Speichereichen I" von AB3 verwenden.
* Dieser binäre Suchbaum soll keine Elemente doppelt enthalten.
* Nutzen Sie die bereitgestellten JUnit-Tests während Ihrer Arbeit.

## Aufgabe 2)

Fügen Sie die beiden folgenden Methoden der abstrakten Klasse `ABinaryTree` hinzu und implementieren Sie sie.

```java
/**
 * Return an iterator, which traverses this tree in breadth-first-order.
 */
public abstract java.util.Iterator<T> breadthFirstIterator();

/**
 * Return an iterator, which traverses this tree in depth-first in-order.
 */
public abstract java.util.Iterator<T> depthFirstIterator();
```

### Hinweis
* Der in dieser Aufgabe zu implementierende Iterator muss lediglich die Operationen `hasNext()` und `next()` implementieren. Interessierte Studenten dürfen sich auch gerne an der `remove()`-Operation versuchen. Die Implementierung dieser Operation ist optional und wird deswegen nicht von den UnitTest überprüft.
