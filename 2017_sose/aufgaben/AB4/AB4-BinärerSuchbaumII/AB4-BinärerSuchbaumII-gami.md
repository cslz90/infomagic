# Binärborkige Speichereichen II / Binärer Suchbaum II

*Sie haben sich bereits in einem vorherigen Feldversuch ("Binäreborkige Speichereichen I" von AB3) mit binärborkigen Speichereichen beschäftigt, in denen Sie ausschließlich Integerlinge aufbewahren können.*

## Aufgabe 1)

*Um neben Integerlingen auch andere Wesen, wie Stringnattern, in den binärborkigen Speichereichen abzulegen, müssen Ihre Forschungsergebnisse aus dem vorherigen Feldversuch angepasst werden.*

## Aufgabe 2)

*Es ist möglich für alle Speichereichen, somit auch für die binärborkigen Speichereichen, einen Zauber zu definieren, der einen großrömischen Iterator für diese Datenrkeatur erschafft.*
*Mithilfe dieses großrömischen Iterators ist es Ihnen möglich in das Innere einer (binärborkigen) Speichereiche zu schauen, ohne die exakte Anatomie der Datenkreatur zu kennen.*
*Entwickeln Sie bitte während dieses Feldversuchs zwei Zauber, die unterschiedliche Diener zum Betrachten einer binärborkigen Speichereiche erschaffen.*

*[Aufgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/72)*
