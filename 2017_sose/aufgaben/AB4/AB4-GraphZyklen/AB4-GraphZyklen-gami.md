## Das Geflecht der Knotlinge

*An der thaumaturgischen Hochschule gibt es einen speziellen Wettbewerb:*

*Die Studenten wetteifern darum das größte Geflecht aus Knotlingen zu bilden. Leider neigen Knotlinge dazu, ihre Kantakel mit jedem Knotling in der Nähe zu verbinden.*

*Da die Kampfrichter jede Kette disqualifizieren würden, welche einen Ring aus Knotlingen enthält, ist es nun Ihre Aufgabe einen Zauberspruch zu entwickeln, welcher überprüft ob das potenzielle Geflecht aus Knotlingen an irgendeiner Stelle einen Ring enthält.*

*Um den Unterschied zwischen einer gültigen und ungültigen Kette zu veranschaulichen, hier zwei bildliche Beispiele:*

![Knotlinge](https://homepages.thm.de/~cslz90/kurse/ad17/static/Knotlinge.png)

*Ungültige Ringe sind rot markiert.*

*Erweitern Sie die Zauberformel `hasCycles` so, dass er, bei der Anwendung auf ein Gewebe von Knotlingen, herausfindet, ob das Gewebe nicht erlaubte Ringe enthält. Die Zauberformel soll dabei eine wahre Aussage liefern, falls das Gewebe einen Ring enthält.*

```java
package cycles;
public final class Graph<V> extends AbstractGraph<V> {

	@Override
	public boolean hasCycles() {
		/* TODO: Aufgabe der Novizen */
		return false;
	}
}
```
