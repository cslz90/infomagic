## Das Geflecht der Knotlinge

Graphen sind eine Standardstruktur der Informatik und werden Ihnen in Ihrer Karriere sehr häufig begegnen.

Ihre Aufgabe besteht darin in einem vorgegebenen Graphen (Kette) Zyklen zu finden. Für diese Aufgabenstellung sind gültige Graphen solche ohne Zyklen, ungültige enthalten mindestens einen Zyklus.

Um den Unterschied zwischen einem gültigen und ungültigen Graphen zu veranschaulichen, hier zwei bildliche Beispiele:

![Knotlinge](https://homepages.thm.de/~cslz90/kurse/ad17/static/Knotlinge.png)

Ungültige Zyklen sind rot markiert.

Erweitern Sie die Methode `hasCycles` so, dass sie ein "True" zurückgibt, falls ein Graph einen Zyklus enthält.

```java
package cycles;
public final class Graph<V> extends AbstractGraph<V> {

	@Override
	public boolean hasCycles() {
		/* TODO: Aufgabe der Studenten */
		return false;
	}
}
```
