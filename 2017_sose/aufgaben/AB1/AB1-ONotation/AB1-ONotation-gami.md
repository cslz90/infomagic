## Ranführung an die Weisen von O [1 h]

*Als guter Novize sollten Sie nicht nur wissen, wofür man welchen Zauber anwenden kann, sondern auch verstehen, wann es sinnvoll ist, einen bestimmten Zauber anzuwenden. Die Weisen von O helfen Ihnen auf einen Blick zu erkennen welche Zauber wie lange brauchen, bis die Wirkung erreicht ist. Denn es ist möglich, dass ein Zauber nur aus wenigen Worten besteht, diese aber oft wiederholt werden müssen, um zu wirken. Manchmal kommt man nicht umher die Zeit aufzuwenden, wenn nur ein Zauber existiert der die nötige Aufgabe erfüllen kann. Wenn man hingegen wenig Zeit hat, ist man möglicherweise besser beraten viele weniger mächtige Zauber zu sprechen, die schneller wirken.*

*Um mit Ihren erworbenen Fähigkeiten zu prahlen, können Sie an einem O-Turnier teilnehmen. Hier werden diverse Fähigkeiten abgefragt, zum Beispiel das Einordnen verschiedener Schreibweisen von O nach der Länge der so bezeichneten Zaubersprüche, Zuordnen von Schreibweisen von O zu gewissen Zaubern und Allgemeines über die Weisen von O.*

*Um Sie nicht nur zu testen, sondern Ihnen auch noch einen gewissen Mehrwert zu bieten, stellt der Veranstalter des Turniers noch zusätzliche Informationen zur Verfügung, welche ebenfalls für ein Bestehen gefordert sind. Es geht dabei um selten besprochene, mythologische Geschichten über die Weisen des O.*  

*Zur Teilnahme am Turnier geht es [hier](https://moodle.thm.de/mod/quiz/view.php?id=161852)*
