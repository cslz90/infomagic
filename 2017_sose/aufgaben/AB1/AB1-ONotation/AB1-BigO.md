Beobachten Sie die folgende Methode:
```java
public class MagicList extends LinkedList {
    public void add(Object o) {
        Thread.sleep(60000);
        super.add(o);
    }
}
```
Welche Aussagen sind wahr?
- die Komplexität ist O(1)
- die Komplexität ist O(60000)
- die Komplexität ist O(60000 + 1)
- die Komplexität ist O(n)
- die Komplexität ist O(60000 + n)
- die Komplexität ist O(60000 * n)
- die Methode ist sehr schnell
- die Methode ist ziemlich langsam
- keine richtige Antwort



Markieren Sie alle richtigen Antworten:
- Die Methoden mit O(1)-Komplexität laufen immer sehr schnell
- Die Methoden mit O(1)-Komplexität laufen immer schneller als 100 ms
- Die Methoden mit O(1)-Komplexität laufen immer langsam
- Wie schnell die Methoden mit O(1)-Komplexität laufen hängt von der Größe der Daten ab
- Wie schnell die Methoden mit O(1)-Komplexität laufen hängt NICHT von der Größe der Daten ab
- keine richtige Antwort


Markieren Sie alle richtigen Antworten:
- 2n^2 + 2n + 2 =	O(1)
- 2n^2 + 2n + 2 =	O(n)
- 2n^2 + 2n + 2 = O(n^2)
- 2n^2 + 2n + 2 = O(n^3)
- keine richtige Antwort

Omega bezeichnet die asymptotische untere Schranke der Funktion. Mehr Information gibt es [hier](https://www.khanacademy.org/computing/computer-science/algorithms/asymptotic-notation/a/big-big-omega-notation).
Markieren Sie alle richtigen Antworten:
- 2n^2 + 2n + 2 =	Omega(1)
- 2n^2 + 2n + 2 =	Omega(n)
- 2n^2 + 2n + 2 = Omega(n^2)
- 2n^2 + 2n + 2 = Omega(n^3)
- keine richtige Antwort

Theta bezeichnet die asymptotische untere und obere Schranke der Funktion. Mehr Information gibt es [hier](https://www.khanacademy.org/computing/computer-science/algorithms/asymptotic-notation/a/big-big-theta-notation).
Markieren Sie alle richtigen Antworten:
- 2n^2 + 2n + 2 =	Theta(1)
- 2n^2 + 2n + 2 =	Theta(n)
- 2n^2 + 2n + 2 = Theta(n^2)
- 2n^2 + 2n + 2 = Theta(n^3)
- keine richtige Antwort

Markieren Sie alle richtigen Antworten:
- 2^(n+1) = O(2^n)
- 2^(n+1) = Omega(2^n)
- 2^(n+1) = Theta(2^n)
- keine richtige Antwort

Gegeben sind zwei Funktionen f1(n) = O(g1(n)) und f2(n) = O(g2(n)) und die Funktion h(n) = f1(n) + f2(n).
Markieren Sie alle richtigen Antworten:
- h(n) = O(g1(n))
- h(n) = O(g2(n))
- h(n) = O(g1(n) + g2(n))
- h(n) = O(max(g1(n), g2(n))
- keine richtige Antwort

Gegeben sind zwei Funktionen f1(n) = O(g1(n)) und f2(n) = O(g2(n)) und die Funktion h(n) = f1(n) * f2(n).
Markieren Sie alle richtigen Antworten:
- h(n) = O(g1(n))
- h(n) = O(g2(n))
- h(n) = O(g1(n) * g2(n))
- h(n) = O(max(g1(n), g2(n))
- keine richtige Antwort

Gegeben sind eine Funktionen f(n) = O(g(n)) und eine Konstante c > 0.
Markieren Sie alle richtigen Antworten:
- c * f(n) = O(g(n))
- c + f(n) = O(g(n))
- keine richtige Antwort

Gegeben sind zwei Funktionen f1(n) = O(f2(n)) und f2(n) = O(f3(n)).
Markieren Sie alle richtigen Antworten:
- f1(n) = O(f3(n))
- f3(n) = O(f1(n))
- keine richtige Antwort
