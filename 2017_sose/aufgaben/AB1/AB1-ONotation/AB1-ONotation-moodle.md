## O-Notation für Moodle [1 h]
### Einordnen nach Größe
Welche der folgenden Sortierungen sind Korrekt?
* O(n^2)<O(n)<O(\(\sqrt{n}\))
* O(1)<O(n^2)
* O(\( \sqrt{n} \)) < O(1)
* O(1)<O(n^n)


### Größe angeben
Welche Laufzeitkomplexität hat ein Algorithmus, der alle Permutationen einer Liste durchlaufen muss?
* O(n)
* O(\( \sqrt{n} \))
* O(log(n))
* O(n*log(n))
* O(1)
* O(n^n)
* O(n^2)
* O(n!)


### Größenanalyse
Die Laufzeit T eines Algorithmus hänge von der Eingabelänge `n ∈ N` wie folgt ab:

 `T(n) = n^2 + 4 n + 3000`

* T ist O(n)
* T ist O(n^2)
* T ist O(3000)
* T ist O(n · log (n))
* Es gibt eine Konstante `c`, so dass `T(n) ≤ c · n^2` für fast alle `n ∈ N`.

### Laufzeitkomplexität von Code
Welche Laufzeitkomplexität haben die zwei folgenden Codeabschnitte?
`for(int i = 0; i< n; i++){
  for(int j = 0; j < 3; j++){
    System.out.println(i+j);
  }
  } `


  `for(int i = n; i > 0; i--){
    for(int j = 0; j < n ; j++){
    System.out.println(i+j);
  }
    }`

* beide O(n)
* beide O(n^2)
* der erste O(n), der zweite O(n^2)
* der erste O(n^2), der zweite O(n)
* beide O(n*log(n))
* die erste O(n*log(n)), die zweite O(n^2)
* die erste O(n^3), die zweite O(n^n)

### MagicList
Beobachten Sie die folgende Methode:
`
public class MagicList extends LinkedList {
    public void add(Object o) {
        Thread.sleep(60000);
        super.add(o);
    }
}
`

Welche Aussagen sind wahr?
- die Komplexität ist O(1)
- die Komplexität ist O(60000)
- die Komplexität ist O(60000 + 1)
- die Komplexität ist O(n)
- die Komplexität ist O(60000 + n)
- die Komplexität ist O(60000 * n)
- die Methode ist sehr schnell
- die Methode ist ziemlich langsam
- keine richtige Antwort


### Know your O
#### Geschwindigkeit erkennen
Markieren Sie alle richtigen Antworten:
- Die Methoden mit O(1)-Komplexität laufen immer sehr schnell
- Die Methoden mit O(1)-Komplexität laufen immer schneller als 100 ms
- Die Methoden mit O(1)-Komplexität laufen immer langsam
- Wie schnell die Methoden mit O(1)-Komplexität laufen hängt von der Größe der Daten ab
- Wie schnell die Methoden mit O(1)-Komplexität laufen hängt NICHT von der Größe der Daten ab
- keine richtige Antwort

#### Gleich und Gleich zuordnen
Markieren Sie alle richtigen Antworten:
- 2n^2 + 2n + 2 =	O(1)
- 2n^2 + 2n + 2 =	O(n)
- 2n^2 + 2n + 2 = O(n^2)
- 2n^2 + 2n + 2 = O(n^3)
- keine richtige Antwort


### Funktionen

#### Funtionen 1
Gegeben sind zwei Funktionen f1(n) = O(g1(n)) und f2(n) = O(g2(n)) und die Funktion h(n) = f1(n) + f2(n).
Markieren Sie alle richtigen Antworten:
- h(n) = O(g1(n))
- h(n) = O(g2(n))
- h(n) = O(g1(n) + g2(n))
- h(n) = O(max(g1(n), g2(n))
- keine richtige Antwort

#### Funktionen 2
Gegeben sind zwei Funktionen f1(n) = O(g1(n)) und f2(n) = O(g2(n)) und die Funktion h(n) = f1(n) * f2(n).
Markieren Sie alle richtigen Antworten:
- h(n) = O(g1(n))
- h(n) = O(g2(n))
- h(n) = O(g1(n) * g2(n))
- h(n) = O(max(g1(n), g2(n))
- keine richtige Antwort

#### Funktionen 3
Gegeben sind eine Funktionen f(n) = O(g(n)) und eine Konstante c > 0.
Markieren Sie alle richtigen Antworten:
- c * f(n) = O(g(n))
- c + f(n) = O(g(n))
- keine richtige Antwort

#### Funktionen 4
Gegeben sind zwei Funktionen f1(n) = O(f2(n)) und f2(n) = O(f3(n)).
Markieren Sie alle richtigen Antworten:
- f1(n) = O(f3(n))
- f3(n) = O(f1(n))
- keine richtige Antwort
