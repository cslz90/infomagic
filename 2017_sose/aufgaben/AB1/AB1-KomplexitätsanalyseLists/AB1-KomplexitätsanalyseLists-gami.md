# Komplexitätsanalyse

## Hüter der Kreaturen
Sie haben im Rahmen Ihrer magischen Ausbildung die wunderlichsten Dinge kennen gelernt und gehen heute schon mit ganz anderen Augen durch die Welt.
Sehen Sie Felder mit verwelkten Blumen auf Ihrem Weg zur Thaumaturgischen Hochschule, denken Sie sofort an Novizen, die sich an ihren ersten Zaubern versuchen, sehen Sie hungrige Tiere, denken Sie direkt an Ihren Kontakt mit magischen Datenkreaturen uvm.

Der Goldgorgone ist mit Ihren Leistungen äußerst zufrieden und als Zeichen seines Vertrauens und seiner Anerkennung beauftragt er Sie mit der Betreuung seines jüngsten Wurfes von kleinen, süßen, verspielten Datenkreaturen.

Natürlich hat der weise Goldgorgone Sie nicht ohne Hintergedanken dafür auserkoren.
Sie sollen mit den kleinen, niedlichen Tierchen Kunststücke üben und dabei Ihr Wissen über die Zeit der Großmeister von O verwenden, um zu erfassen, wie lange die unterschieldlichen Kreaturen dafür brauchen.
