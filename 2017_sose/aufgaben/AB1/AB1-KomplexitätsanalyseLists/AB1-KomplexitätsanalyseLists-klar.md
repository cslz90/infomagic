# Komplexitätsanalyse [23 Fragen * 2-3 min = 1 Stunde]

### Aufgabe: Schätzen Sie die folgenden Vorgänge der unterschiedlichen Datenstrukturen zeitlich nach O-Notation ein.

Multiple-choise-Test:
[Varianten immer: O(1), O(lg n), O(n), O(n * lg n), O(n^2), O(n^3), O(2^n), O(n!)]


Schätzen Sie die Komplexität der Methoden der Java-Datenstrukturen.


## LinkedList:

### add(element)
Wählen Sie die beste Schätzung der Komplexität für die Methode `add(element)` von `LinkedList`
O(1)

### add(index, element)
Wählen Sie die beste Schätzung der Komplexität für die Methode `add(index, element)` von `LinkedList`
O(n)

### add(element) eines `ListIterator`-Objekts
Wählen Sie die beste Schätzung der Komplexität für die Methode `add(element)` eines `ListIterator`-Objekts, das von einem `LinkedList`-Objekt erzeugt wurde
O(1)

### remove()
Wählen Sie die beste Schätzung der Komplexität für die Methode `remove()` von `LinkedList`
O(1)

### remove(index)
Wählen Sie die beste Schätzung der Komplexität für die Methode `remove(index)` von `LinkedList`
O(n)

### remove() eines `ListIterator`-Objektes
Wählen Sie die beste Schätzung der Komplexität für die Methode `remove()` eines `ListIterator`-Objektes, das von einem `LinkedList`-Objekt erzeugt wurde
O(1)

### get(index)
Wählen Sie die beste Schätzung der Komplexität für die Methode `get(index)` von `LinkedList`
O(n)

### set(index, element)
Wählen Sie die beste Schätzung der Komplexität für die Methode `set(index, element)` von `LinkedList`
O(n)

### contains(element)
Wählen Sie die beste Schätzung der Komplexität für die Methode `contains(element)` von `LinkedList`
O(n)

### isEmpty()
Wählen Sie die beste Schätzung der Komplexität für die Methode `isEmpty()` von `LinkedList`
O(1)

### indexOf(element)
Wählen Sie die beste Schätzung der Komplexität für die Methode `indexOf(element)` von `LinkedList`
O(n)


## ArrayList:

### amortisiert add(element)
Wählen Sie die beste (amortisierte) Schätzung der Komplexität für die Methode `add(element)` von `ArrayList`
O(1)

### worst-case add(element)
Wählen Sie die beste (worst-case) Schätzung der Komplexität für die Methode `add(element)` von `ArrayList`
O(n)

### add(index, element)
Wählen Sie die beste Schätzung der Komplexität für die Methode `add(index, element)` von `ArrayList`
O(n)

### remove(index)
Wählen Sie die beste Schätzung der Komplexität für die Methode `remove(index)` von `ArrayList`
O(n)

### get(index)
Wählen Sie die beste Schätzung der Komplexität für die Methode `get(index)` von `ArrayList`
O(1)

### set(index, element)
Wählen Sie die beste Schätzung der Komplexität für die Methode `set(index, element)` von `ArrayList`
O(1)

### contains(element)
Wählen Sie die beste Schätzung der Komplexität für die Methode `contains(element)` von `ArrayList`
O(n)

### isEmpty()
Wählen Sie die beste Schätzung der Komplexität für die Methode `isEmpty()` von `ArrayList`
O(1)

### indexOf(element)
Wählen Sie die beste Schätzung der Komplexität für die Methode `indexOf(element)` von `ArrayList`
O(n)

## Welche Datenstruktur passt am besten:

### Datenstruktur 1
Welche Datenstruktur passt am besten, wenn man oft Lesezugriff auf die Elemente an verschiedenen Stellen braucht?
+ ArrayList
- LinkedList

### Datenstruktur 2
Welche Datenstruktur passt am besten, wenn man oft Elemente in die Mitte einfügt?
- ArrayList
+ LinkedList

### Datenstruktur 3
Welche Datenstruktur passt am besten, wenn man oft Elemente am Anfang und Ende einfügt?
- ArrayList
+ LinkedList
