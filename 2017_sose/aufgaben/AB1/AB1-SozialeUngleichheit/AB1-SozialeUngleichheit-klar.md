## Analyse sozialer Ungleichheiten (Geschätzte Dauer: 30 Min.)

[Hier, noch leer](nag) erhalten Sie von uns eine Liste mit Zahlen. Speichern Sie diese Daten in eine Arrayliste mit Integern. Finden Sie in dieser Liste jeweils das Minimum und das Maximum. Beides soll gleichzeitig ermittelt werden.

Als Hilfe soll Ihnen der hier gegebene Pseudocode dienen:

```
algorithm minmax(a):
  min := a[0]
  max := a[0]
  for i := 1 ... a.length()-1
	   if a[i] < min then
		   min := a[i]
	   if a[i] > max then
		   max := a[i]
return min,max
```

Da sie einen Input erhalten und in Dozentron nur der Output getestet wird, dürfen Sie diese Aufgabe in einer beliebigen Sprache lösen. Der Code wird natürlich dennoch kontrolliert (s. Aufgabe 3).

### Aufgabe 1: Minimum und Maximum

* Schreiben Sie eine Methode`minmax` welche eine Arrayliste übernimmt und das Maximum und das Minimum dieser Liste liefert.

* Alle Zahlen in der gelieferten Liste sind positiv.

[Geben Sie Ihre Ergebnisse in Dozentron ab, link noch leer]()

Bitte geben Sie die Zahlen in dem folgenden Format ab:

```
min  = <Zahl>
max  = <Zahl>
```

### Aufgabe 2: Friede den Adepten, Krieg den Überbezahlten

Finden Sie in der obigen bereitgestellten Liste mit Hilfe einer Methode `top5` die fünf größten Zahlen und lassen Sie sich diese ausgeben. Die Methode soll eine maximale Laufzeit von O(n) besitzen. 

 [Stichprobe, link noch leer]().*

Übertragen Sie die ermitteln Zahlen an den folgenden Link:

[Abgabe in Dozentron, link noch leer]()

Bitte geben Sie die Zahlen in dem folgenden Format ab:

```
top1 = <Zahl>
top2 = <Zahl>
top3 = <Zahl>
top4 = <Zahl>
top5 = <Zahl>
```


### Aufgabe 3: Magische Korrektheitsprüfung

Bitte reichen Sie Ihren Quellcode zur Korrektheitsprüfung bei [Moodle](https://moodle.thm.de) ein. Beide Methoden sollen dabei in einer Klasse sein.