## magische Naturkunde: "Die Immerwahrs" (geschätzte Dauer 45 min.)

Sehen Sie sich den folgenden Pseudocode zum Algorithmus "lassMichBlühen" genau an:

```
algorithm lassMichBlühen (a, b ∈ ℕ ):
  q := 0
  r := a
  while r >= b
    q = q + 1
    r = r - b
  return r

```

Schleifeninvariante: `q * b + r = a`

### Aufgabe 1: Algorithmus

Was macht der Algorithmus? 

Hinweis: Warum ist die Schleifeninvariante sinnvoll?

### Aufgabe 2: Beweis
Beweisen Sie, dass die folgende Schleifeninvariante für den Pseudocodealgorithmus "lassMichBlühen" gilt.

Hinweis zur Beweisführung: Prüfen Sie die Gültigkeit der Schleifeninvariante vor dem ersten, vor dem i-ten (i-1) sowie zum i-ten Schleifendurchlauf.



## Musterlösung:
Vor erstem Schleifendurchlauf:

```
q = 0, r = a
  => q * b + r
      = 0 * b + a
      = a
```
Es gelte die Schleifeninvariante vor dem i-ten Schleifendurchlauf (i-1):

q<sub>i-1</sub> \* b + r<sub>i-1</sub> = a

Beim i-ten Schleifendurchlauf gilt:<br /> q<sub>i</sub> = q<sub>i-1</sub> + 1 und r<sub>i</sub> = r<sub>i-1</sub> - b

=> q<sub>i</sub> \* b + r<sub>i</sub> = (q<sub>i-1</sub> + 1) \* b + r<sub>i-1</sub> - b <br />= q<sub>i-1</sub> \* b + r<sub>i-1</sub> <br \>= a
