## Magische Naturkunde: "Die Immerwahrs"
### Zeiteinschätzung 45 min.

*Ihnen und vielen anderen Novizen ist wahrscheinlich schon aufgefallen, dass die Tage wieder länger werden. Wenn Sie donnerstags zum ersten Block in die Übung von Algomantik und Datenkreaturen gehen, können sie mittlerweile im Hellen und ganz ohne künstliches Licht ans Ziel gelangen. Aber was passiert eigentlich draußen, während Sie aufmerksam den interessanten Geschichten des Goldgorgonen lauschen?  <br />Natürlich recht wenig, da viele Ihrer Mitstreiter immer noch gemütlich im Bett liegen und die Übungen in einem späteren Block besuchen.
(Tipp: Vielleicht sollten Sie Ihren Zauberstab untersuchen lassen. Der Stab Ihrer Mitstreiter konnte selbige wohl schneller in einer späteren Übungsgruppe anmelden)
Mit geschultem Novizenblick kann man aber dennoch einige Veränderungen in der Umwelt ausmachen. Es wird nämlich wieder bunt. Vor den Toren der Thaumaturgischen Hochschule  erstrahlt ein reinstes Blütenmeer.  <br />Das war nicht immer so: Erst kürzlich während der letzten Klausurenphase konnten wir einen rapiden Rückgang an blühenden Immerwahrs verzeichnen.
Luftbilder und Schätzungen belegen, dass der gesamte, umliegende Immerwahr-Bestand auf einen geringen Anteil von ca. 36% zurückgegangen ist. Viele Forscher und Großmeister sind sich über die genauen Gründe dieser kleinen Naturkatastrophe im Unklaren, aber sehr zuversichtlich, dass sich der Bestand in nächster Zeit wieder erholen wird.  <br />Eine Besonderheit dieser Pflanze ist nämlich, dass sie im Regelfall zweimal im Jahr blüht - "zufällig" immer dann, wenn an unserer Thaumaturgischen Hochschule für Magie die Vorlesung Algomantik und Datenkreaturen gehalten wird. <br />
Da es sich um eine magische Pflanze handelt, hat sie auch magische Eigenschaften. Das äußert sich folgendermaßen: Wenn sich eine Immerwahrblume in der Nähe eines Magiers befindet und dieser einen korrekten, störungsfreien Zauberspruch vollführt, erblüht sie in schönster Form und Farbe. Falsche Gestenwiederholungen beispielsweise sind jedoch Gift für diese empfindlichen, kleinen Pflänzchen, denn wie ihr Name schon sagt, können sie nur in der Nähe von Zaubern leben, in denen sie wahr sind. Ist das nahelebende Immerwahr allerdings einmal am Anfang oder Ende einer schleifenhaften Gestenwiederholung nicht wahr, muss es leider vor Scham eingehen. Der Botanomagiker in Ihnen möchte den armen Immerwahrs nun hoffentlich dieses Ende ersparen.*

*Darum soll in dieser Aufgabe Ihre Empathie zu dieser Pflanze geschult werden.: Versetzen Sie sich bitte in die Rolle der Pflanze und sehen Sie sich den folgenden Zauberspruch genau an:*

*Form des Immerwahrs:* `q * b + r = a`

```
algorithm lassMichBlühen (a, b aus den unmagischen Zahlen):
  q := 0
  r := a
  while r >= b
    q = q + 1
    r = r - b
  return r
```  

### Der Zauber

*Sie als kleines Immerwahr müssen erst einmal herausfinden, ob der Zauberspruch der für Sie richtige ist. Was bewirkt er?*

### Die Kombination

*Zeigen Sie, ob Sie blühen dürfen oder nicht.*

*Hinweise:*
- *unmagisch = natürlich*
- *Um zu beweisen, dass sie tatsächlich blühen dürfen, sollten Sie Ihre Korrektheit vor der ersten, vor der i-ten (i-1) sowie zur i-ten Gestenwiederholung prüfen.*

[Abgabe 3.1 und 3.2 in Moodle](https://moodle.thm.de/mod/assign/view.php?id=162796)


## Musterlösung:
Vor erstem Schleifendurchlauf:

q = 0, r = a <br />
=> q \* b + r = 0 \* b + a = a

Es gelte die Schleifeninvariante vor dem i-ten Schleifendurchlauf (i-1): <br />
q<sub>i-1</sub> \* b + r<sub>i-1</sub> = a

Beim i-ten Schleifendurchlauf gilt:<br /> q<sub>i</sub> = q<sub>i-1</sub> + 1 und r<sub>i</sub> = r<sub>i-1</sub> - b

=> q<sub>i</sub> \* b + r<sub>i</sub> = (q<sub>i-1</sub> + 1) \* b + r<sub>i-1</sub> - b = q<sub>i-1</sub> \* b + r<sub>i-1</sub> = a
