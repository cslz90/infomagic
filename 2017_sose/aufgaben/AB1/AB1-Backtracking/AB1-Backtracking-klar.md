# Backtracking (allgemeine Verständnisfragen) [5-10 min]

Welche Probleme wäre es sinnvoll durch Backtracking zu lösen:
+ Suche nach allen Permutationen
+ Suche nach allen Teilmengen
+ Suche nach allen Wegen zwischen zwei Knoten in einem Graphen
+ Sudoku
- Suche nach dem Element in einem ungeordneten Array
- Sortierung des Arrays
- keine richige Antwort

Markieren Sie alle richtigen Antworten:
+ Backtracking versucht die Teillösungen zu einer Gesamtlösung auszubauen
+ Backtracking wird normalerweise durch Rekursion implementiert
+ Backtracking ist normalerweise effektiver als Brute Force
- Backtracking kann nicht effektiver als Brute Force sein
- keine richige Antwort

Markieren Sie alle richtigen Antworten:
+ Backtracking läuft durch alle sinnvolle Konfigurationen
- Durchprobieren der Konfigurationen beim Backtracking ist zufällig und kann sinnvolle Konfigurationen überspringen
- Backtracking muss unbedingt alle möglichen Konfigurationen ausprobieren
+ Jede Konfiguration wird maximal ein mal ausprobiert
- keine richige Antwort

Wenn Backtracking keine Antwort gefunden hat, dann:
+ das bedeutet, dass es keine Antwort gibt
- wir müssen einen anderen Algorithmus anwenden, weil Backtracking nicht garantiert, dass die Antwort gefunden wird
- keine richige Antwort

Backtracking baut einen Lösungsbaum, wo Knoten Teillösungen repräsentieren. Markieren Sie alle richtigen Antworten:
+ die Kante vom Knoten X zum Knoten Y bedeutet, dass Y mithilfe von X gebaut wurde
- die Zahl von Knoten stimmt mit der Zahl der Gesamtlösungen überein
+ wenn alle Permutationen der Buchstaben (`a` bis `z`) durch Backtracking gesucht werden, gibt es unter anderem die Knoten `a`, `ab` und `abet` im Baum
- wenn alle Permutationen der Buchstaben (`a` bis `z`) durch Backtracking gesucht werden, gibt es unter anderem die Knoten `abc`, `abb` und `abbz` im Baum
- keine richige Antwort
- 