# Bruteforce / Kennwörter der Orks
- Aufgabentyp: Anwendung
- geschätze Dauer: 30-60 min
- Bewertungsschema: Abgabe auf Dozentron, Prüfung durch UnitTests

Erzeugen Sie bitte eine ArrayList mit allen möglichen Wörtern mit den folgenden Eigenschaften:
1. Jedes Wort fängt mit "grunz" an.
2. Jedes Wort hat eine Länge von maximal 9 Zeichen.
3. Jedes Wort besteht nur aus den Buchstaben 'a' bis 'z' und 'A' bis 'Z'.

Folgende Formalia sollen eingehalten werden:
- Erstellen Sie eine Klasse `OrcPasswords` in dem package `orcpasswords`.
- Implementieren Sie in der Klasse `OrcPasswords` eine Funktion mit der Signatur `public static ArrayList<String> passwords();`. Diese soll, wie oben beschrieben, alle möglichen Wörter erzeugen.
