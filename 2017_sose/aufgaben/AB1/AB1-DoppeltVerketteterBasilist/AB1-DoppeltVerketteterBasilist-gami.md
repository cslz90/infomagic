# Doubly-Linked-Basilist

Als leidenschaftlicher Züchter von Linked-Basilisten gehören Sie zu den Ersten die im Objektorient von den Neuigkeiten aus Funktionalia hören: In einer Ausgrabungsstätte nahe Haskell wurden die Überreste einer, bereits lange ausgestorbenen Kreatur gefunden. Große Zauber wurden gewirkt um herauszufinden, wie diese Kreatur gelebt haben muss. Man gab ihr den Namen Doubly-Linked-Basilist. Die Ergebnisse der Untersuchungen über die Anatomie und die Verhaltensweisen des Doubly-Linked-Basilist finden Sie im Folgenden.

## Untersuchungsergebnisse

Ein Doubly-Linked-Basilist ist dem Linked-Basilist sehr ähnlich. Anscheinend handelt es sich bei letzterem um einen entfernten Nachfahren. Statt einem Kopf und einem Schwanz hat dieser zwei Köpfe und zwei Schwänze. Da sich damals Rivalenkämpfe noch sehr viel heftiger zugetragen haben, beißt ein Doubly-Linked-Basilist nicht nur in den Schwanz seines Vorgängers sondern auch in einen der beiden Schwänze seines Nachfolgers. So kann er einen stärkeren Zusammenhalt schaffen. Sowohl am Anfang als auch am Ende einer Kette befinden sich Doubly-Linked-Basilisten im Larvenstadium. Der zweite Schwanz und der zweite Kopf einer Larve bilden sich jedoch erst später. Somit ähnelt die Larve eines Doubly-Linked-Basilist stark der eines Linked-Basilist.

## Aufgabe 1)

Motiviert von dieser neuen Erkenntnis über die Vorfahren Ihrer liebgewonnen Linked-Basilisten, wollen Sie die Doubly-Linked-Basilisten wiederauferstehen lassen. Hierzu planen Sie eine Kreuzung aus einem `GenericBasilist` und den in Funktionalia beheimateten Linked-Basilisten. Als Grundlage für Ihre Kreuzung dient einzig und allein die folgende Notiz die Sie in einer Ihrer Vorlesungen an der Thaumaturgische Hochschule der Magie mitgeschrieben haben:

```java
package basilist;

public interface GenericBasilist<E> {
  E get(int idx); // retrieve element
  void set(E el, int idx); // overwrite element
  int size(); // number of elements
  void add(E el); // append to end
  void remove(int idx); // remove at index
  void insert(E el, int idx); // insert at index
}
```

## Aufgabe 2)

Ihre Kreuzung ist geglückt. Allerdings unterlief Ihnen, als Novize, ein Fehler: Zu spät erst, mussten Sie feststellen, dass Ihre Notizen über den `GenericBasilist` unvollständig sind. Einer Ihrer Kommilitonen aus der Thaumaturgischen Hochschule der Magie hat Ihnen die korrigierte Version zukommen lassen:

```java
package basilist;

import java.util.function.Function;

public interface GenericBasilist<E> {
  E get(int idx); // retrieve element
  void set(E el, int idx); // overwrite element
  int size(); // number of elements
  void add(E el); // append to end
  void remove(int idx); // remove at index
  void insert(E el, int idx); // insert at index
  <T> GenericBasilist<T> map(Function<E, T> f); // call `f` for each element and return a mapped list
	<T> GenericBasilist<T> reverseMap(Function<E, T> f); // like `map` but traverse in reverse order
}
```

Unglücklicherweise beeinflusst Ihr Fehler die Bewegungsfreiheit der noch jungen Doubly-Linked-Basilisten. Korrigieren Sie dies schleunigst, um eine Auseinandersetzung mit dem Veterinäramt von Ja´va zu verhindern!

#### Ein einfacher Test

Um wirklich auf Nummer sicher zu gehen, sollten Sie folgenden, einfachen Zauberspruch durchführen. Wenn er erfolgreich ist, wissen Sie zumindest, dass Ihnen die Auseinandersetzung mit dem Veterinäramt von Ja´va erspart bleibt.

```java
public static void main(String[] args) {
		GenericBasilist<Integer> zahlen = new DoublyLinkedBasilist();
    IntStream.range(0, 10).forEach(zahlen::add);
		GenericBasilist<String> a = zahlen.map(Object::toString);
		GenericBasilist<String> b = zahlen.reverseMap(Object::toString);
		System.out.printf("Vergleiche %s mit %s ...\n", a, b);
		if (!a.equals(b)) {
        System.err.println("Test fehlgeschlagen!");
			  System.exit(1);
		} else {
			  System.out.println("Test erfolgreich!");
			  System.exit(0);
		}
}
