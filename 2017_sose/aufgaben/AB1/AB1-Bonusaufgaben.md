# Bonusaufgaben zu Aufgabenblatt 1

|Nr | Thema                         | Gamifiziert                    | Dauer in min  | XP  | %   | Ref gamifiziert  |
|---| ---                           | ---                            | ---           | --- | --- |---               |
|00 | Komplexitätsanalyse           | ?                              | XX            | ?   | ??  | [](./entwurf/.md)|
|00 | Doppelt verkettete Liste      | Züchtung einer neuen Art       | XX            | ?   | ??  | [Link](./entwurf/groupname-goes-here/ab1-doppelt-verketteter-basilist-gam.md )|
|00 | Laufzeitanalyse               | Mag. Selbsteinschätzung/Gesten | 40            | 10   | ??  | [Link](./entwurf/groupname-goes-here/ab1-laufzeitanalyse-magische-selbsteinschaetzung-gam.md)|
|00 | Greedy                        | Zwerge und Münzen              | 90            | 22   | ??  | [Link](./entwurf/Knopftierchen/ab1-Greedy_Algorithmus-Gierige_Zwerge-gami.md)|
|00 | Backtracking                  | ?                              | XX            | ?   | ??  | [](./entwurf/.md)|

