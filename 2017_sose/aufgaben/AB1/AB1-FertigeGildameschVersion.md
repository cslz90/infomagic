## Aufgabe 1: Kennwörter der Orks [1,5 h]

*Aufgrund des großen Krieges zwischen den magischen Wesen in der Vergangenheit, stehen gerade die älteren, konservativ eingestellten Orks den Freundschaften mit Magiern eher misstrauisch gegenüber. Nur wirklich gute Freunde und vertrauenswürdige Magier werden in die Dörfer der Orks eingeladen. In einer heiligen Zeremonie wird den menschlichen Gästen das Kennwort genannt, mit dem sie als willkommene Gäste gelten.*

*Sie wollen einen befreundeten Ork in seinem Dorf besuchen, haben aber das Kennwort vergessen. Da das Vergessen des heiligen Kennwortes als Vertrauensbruch gilt, wollen Sie sich einen Zauber überlegen, mit dem Sie das vergessene Kennwort wiederfinden können. Da Orks sehr vergesslich sind, können Sie den Wachen des Orkdorfes alle Kennwörter nach und nach nennen, ohne dass diese merken, dass etwas nicht stimmt.*

*Sie konnten bereits in der Bibliothek der Thaumaturgischen Hochschule für Magie recherchieren und folgende Informationen über die Kennwörter sammeln:*

1. *Jedes Kennwort fängt mit "grunz" an.*
2. *Jedes Kennwort hat eine Länge von maximal 9 Zeichen.*
3. *Jedes Kennwort besteht nur aus den Buchstaben 'a' bis 'z' und 'A' bis 'Z'.*

*Entwickeln Sie einen Zauber, der alle möglichen Kennwörter erzeugt und sie in einer ArrayList abspeichert.*

*Der Zauber könnte (muss aber nicht) aus den folgenden, einfacheren Teilzaubern bestehen:*
- *Nenne alle Kennwörter der Länge 6.*
  - *Das ist trivial: Kombiniere "grunz" mit jedem möglichen Buchstaben. Also: "grunza", ..., "grunzz", "grunzA", ..., "grunzZ".*
- *Wiederhole solange, bis alle möglichen Kennwörter genannt sind:*
  - *Erweitere jedes bis hierhin genannte Kennwort um einen Buchstaben.*

[Vorgabe in Moodle (Link fehlt noch)]()
[Abgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/57)

## Aufgabe 2: Analyse sozialer Ungleichheiten [2 h]

*Zur grundlegenden Ausbildung eines magischen Novizen gehört, neben deftigem Substanzmissbrauch und übertriebener Prokrastination, natürlich auch die kritische Auseinandersetzung mit gesellschaftlichen Misständen. Dem vorbildlichen Novizen sollten zum Beispiel globale Probleme mit überbezahlten Schreihälsen und deren hungernden Untergebenen nicht unbekannt sein.*

*Hier offenbart sich die in der nichtmagischen Welt gängige Praxis der generellen Falschbezahlung. Es ist üblich die Gehälter aller schwer arbeitenden Wesen, wie etwa unser nichtsahnendes Cafeteriapersonal, viel zu gering zu halten. Bei stichprobenhaften Befragungen von Wehrlosen empfindet sich ein jeder, prinzipiell unterbezahlt. Gegenteilig wird die Bezahlung von gleichwertigen Kollegen als völlig ungerechtfertigt und zu hoch eingestuft. Wie Sie alle sicher wissen, ist dies in unserer magischen Welt natürlich niemals der Fall. Die Relevanz dieses Themas ist darum für Sie nur scholastischer Natur.*

*[Hier, noch leer](nag) erhalten Sie von uns einen Liste mit Gehältern aus der Stichprobe. Speichern Sie die Daten in einen Integerlinge fressenden Arraybasilisten. Untersuchen Sie die Mägen dieses Arraybasilisten und finden Sie das maximale Gehalt, um seinen Bezieher als Hassobjekt klassifizieren zu können. Zusätzlich soll derselbe Zauber ebenfalls das minimale Gehalt aus unserem Basilisten liefern, um eine Briefeule an Magier ohne Grenzen abschicken zu können.*

*Als Hilfe soll Ihnen der hier gegebene, in Pseudobabel verfasste, Spruch dienen:*

```
algorithm minmax(a):
  min := a[0]
  max := a[0]
  for i := 1 ... a.length()-1
	   if a[i] < min then
		   min := a[i]
	   if a[i] > max then
		   max := a[i]
return min,max
```
### Aufgabe 2.1: Minimum und Maximum
*Schreiben Sie einen Zauber `minmax` welcher einen Arraybasilisten übernimmt und das Maximum und Minimum liefert.*
- *Bedenken Sie, dass es selten zu negativen Gehältern kommt*

[Geben Sie Ihre Ergebnisse in Dozentron ab.](http://dozentron.mni.thm.de/input_output_tasks/60)

### Aufgabe 2.2: Friede den Adepten, Krieg den Überbezahlten

*Ebenfalls eine Eigenschaft eines gut gebildeten Adepten ist der Mut zu Veränderungen. Die von uns aufgedeckten Missstände sollen vor dem internationalen, obersten Zauberergerichtshof eingereicht werden. Hierzu sollen Sie ganz in Hass und Eifersucht versunken eine Liste der fünf besten Gehälter erstellen.*

*Für Ihre Analyse bekommen Sie hier eine weitere [Stichprobe, link noch leer](). Entwerfen Sie einen Zauber `top5`, welcher eine Liste/Array von mindestens 5 Elementen übernimmt und eine maximalen Dauer von O(n) hat.*

[Abgabe in Dozentron](http://dozentron.mni.thm.de/input_output_tasks/61)

## Aufgabe 3: Magische Naturkunde: "Die Immerwahrs" [1h]


*Ihnen und vielen anderen Novizen ist wahrscheinlich schon aufgefallen, dass die Tage wieder länger werden. Wenn Sie donnerstags zum ersten Block in die Übung von Algomantik und Datenkreaturen gehen, können sie mittlerweilse im Hellen und ganz ohne künstliches Licht ans Ziel gelangen. Aber was passiert eigentlich draußen, während Sie aufmerksam den interessanten Geschichten des Goldgorgonen lauschen?  <br />Natürlich recht wenig, da viele Ihrer Mitstreiter immer noch gemütlich im Bett liegen und die Übungen in einem späteren Block besuchen.
(Tipp: Vielleicht sollten Sie Ihren Zauberstab untersuchen lassen. Der Stab Ihrer Mitstreiter konnte selbige wohl schneller in einer späteren Übungsgruppe anmelden)
Mit geschultem Novizenblick kann man aber dennoch einige Veränderungen in der Umwelt ausmachen. Es wird nämlich wieder bunt. Vor den Toren der Thaumaturgischen Hochschule  erstrahlt ein reinstes Blütenmeer.  <br />Das war nicht immer so: Erst kürzlich während der letzten Klausurenphase konnten wir einen rapiden Rückgang an blühenden Immerwahrs verzeichnen.
Luftbilder und Schätzungen belegen, dass der gesamte, umliegende Immerwahr-Bestand auf einen geringen Anteil von ca. 36% zurückgegangen ist. Viele Forscher und Großmeister sind sich über die genauen Gründe dieser kleinen Naturkatastrophe im Unklaren, aber sehr zuversichtlich, dass sich der Bestand in nächster Zeit wieder erholen wird.  <br />Eine Besonderheit dieser Pflanze ist nämlich, dass sie im Regelfall zweimal im Jahr blüht - "zufällig" immer dann, wenn an unserer Thaumaturgischen Hochschule für Magie die Vorlesung Algomantik und Datenkreaturen gehalten wird. <br />
Da es sich um eine magische Pflanze handelt, hat sie auch magische Eigenschaften. Das äußert sich folgendermaßen: Wenn sich eine Immerwahrblume in der Nähe eines Magiers befindet und dieser einen korrekten, störungsfreien Zauberspruch vollführt, erblüht sie in schönster Form und Farbe. Falsche Gestenwiederholungen beispielsweise sind jedoch Gift für diese empfindlichen, kleinen Pflänzchen, denn wie ihr Name schon sagt, können sie nur in der Nähe von Zaubern leben, in denen sie wahr sind. Ist das nahelebende Immerwahr allerdings einmal am Anfang oder Ende einer schleifenhaften Gestenwiederholung nicht wahr, muss es leider vor Scham eingehen. Der Botanomagiker in Ihnen möchte den armen Immerwahrs nun hoffentlich dieses Ende ersparen.*

*Darum soll in dieser Aufgabe Ihre Empathie zu dieser Pflanze geschult werden.: Versetzen Sie sich bitte in die Rolle der Pflanze und sehen Sie sich den folgenden Zauberspruch genau an:*

*Form des Immerwahrs:* `q * b + r = a`

```
algorithm lassMichBlühen (a, b aus den unmagischen Zahlen):
  q := 0
  r := a
  while r >= b
    q = q + 1
    r = r - b
  return r
```  

*Hinweis:*
- *unmagisch = natürlich*

### Aufgabe 3.1: Der Zauber

*Sie als kleines Immerwahr müssen erst einmal herausfinden, ob der Zauberspruch der für Sie richtige ist. Was bewirkt er?*

### Aufgabe 3.2: Die Kombination

*Zeigen Sie, ob Sie blühen dürfen oder nicht.*

*Hinweis:*
- *Um zu beweisen, dass sie tatsächlich blühen dürfen, sollten Sie Ihre Korrektheit vor der ersten, vor der i-ten (i-1) sowie zur i-ten Gestenwiederholung prüfen.*

[Abgabe 3.1 sowie 3.2 in Moodle](https://moodle.thm.de/mod/assign/view.php?id=162796)


## Aufgabe 4: Ranführung an die Weisen von O [1h]

*Als guter Novize sollten Sie nicht nur wissen wofür man welchen Zauber benötigt, sondern auch verstehen wann es sinnvoll ist einen bestimmten Zauber anzuwenden. Die Weisen von O helfen Ihnen auf einen Blick zu erkennen welche Zauber wie lange brauchen, bis die Wirkung erreicht ist. Denn es ist möglich, dass ein Zauber nur aus wenigen Worten besteht, diese aber oft wiederholt werden müssen um zu wirken. Manchmal kommt man nicht umher diese Zeit aufzuwenden, wenn nur ein Zauber existiert, welcher die nötige Aufgabe erfüllen kann. Wenn man hingegen wenig Zeit hat, ist man möglicherweise besser beraten, viele weniger mächtige Zauber zu sprechen, die schneller wirken.*

*Um mit Ihren erworbenen Fähigkeiten zu prahlen, können Sie an einem O-Turnier teilnehmen. Hier werden diverse Fähigkeiten abgefragt, zum Beispiel das Einordnen verschiedener Schreibweisen von O nach der Länge der so bezeichneten Zaubersprüche, Zuordnen von Schreibweisen von O zu gewissen Zaubern und Allgemeines über die Weisen von O.*

*Um Sie nicht nur zu testen, sondern Ihnen auch noch einen gewissen Mehrwert zu bieten, stellt der Veranstalter des Turniers noch zusätzliche Informationen zur Verfügung, welche ebenfalls für ein Bestehen gefordert sind. Es geht dabei um selten besprochene, mythologische Geschichten über die Weisen des O.*

*Zur Teilnahme am Turnier geht es [hier](https://moodle.thm.de/mod/quiz/view.php?id=161852)*
