# Magische Selbsteinschätzung
- Aufgabenart: Anwendung
- geschätze Dauer: max. 20 min
- mögliche Bewertungsschemata: Single Choice (Multiple Choice, aber nur eine Antwort ist richtig)

*Jeder Zauber verlangt eine Anzahl von bestimmten Gesten, für die der Magier ein gewisses Maß an Konzentration benötigt.
Das trifft sowohl auf den unerfahrenen Novizen, den erfahrenen Infomagister und sogar auf den Goldgorgonen zu.
Je länger ein Zauber wirkt, desto mehr muss sich der Magier konzentrieren.
Ist ein Magier der mentalen Belastung beim Wirken eines Zaubers nicht gewachsen, verlässt ihn das Bewusstsein. Mit etwas Ruhe und einem Stück Schokolade kann der Magier sich zwar erholen, eine solche Anstrengung zehrt dennoch an den Nerven.*

*Bevor man solch anstrengende Zauber wirkt, ist es also ratsam einzuschätzen, ob man den abverlangten Anforderungen gewachsen ist.*

*Um die Fähigkeit zu erlangen, die Komplexität eines beliebigen Zaubers einschätzen zu können, muss der Novize diese Fertigkeit anhand einfacher Zauber erproben. Mit genug Übung wird er dann in der Lage sein auch komplexe Zaubersprüche zu meistern.*

*Ziemlich einfache Zaubersprüche sind zum Beispiel:*
1. *"sit numerus primis": Dieser Spruch ermöglicht es dem Magier herauszufinden, ob eine unmagische (natürliche) Zahl eine Primzahl ist.*
2. *"omnia primis": Der Zauberspruch findet alle Primzahlen in einem gewünschten Bereich der unmagischen (natürlichen) Zahlen.*
3. *"numerus quadratum": Der Spruch zum Quadrieren einer unmagischen (natürlichen) Zahl.*
4. *"combinatoris sortere": Ein Zauberspruch zum Sortieren eines ArrayBasilisten, der unmagische (natürlichen) Zahlen gefressen hat.*

*An diesen Zaubern sollen die Novizen abschätzen, ob sie der Dauer und der Anstrengung, die auf sie beim Wirken dieser mächtigen Zauber zukommt, gewachsen sind.*

*Der Test zur Aufgabe ist auf [Moodle](https://moodle.thm.de/mod/quiz/view.php?id=162167) zu finden.*
