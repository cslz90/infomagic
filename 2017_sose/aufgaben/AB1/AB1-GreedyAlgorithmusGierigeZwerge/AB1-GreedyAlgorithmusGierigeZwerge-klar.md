## Gierige Zwerge
- Aufwand: ca. 40-45 Minuten

Schreiben Sie einen Algorithmus, welcher das sogenannte "Change-making problem" (Geldwechselproblem) umsetzt und folgende Regeln beachtet:

1. Der zu bezahlende Gesamtbetrag beträgt 99 Credits
2. Es gibt Münzen im Wert von 50,20,10,5,2 und 1 Credit(s)
3. Es gibt keine Einschränkung bei der Anzahl der einzelnen Münzen (Jeder Münzbetrag kann beliebig oft eingesetzt werden)
4. Der Betrag muss exakt erreicht werden.
5. Die eingesetzten Münzen sollen an einer ArrayListe gespeichert werden. Der Datentyp ist dabei Integer.
6. Die Methode muss den Namen "lassDenZwergWeinen"  in der Klasse "GierigeZwerge" haben.
7. Als Methodenparameter soll der Gesamtbetrag als Integer übergeben werden. Der Rückgabewert ist die ArrayListe aus Punkt 5.
