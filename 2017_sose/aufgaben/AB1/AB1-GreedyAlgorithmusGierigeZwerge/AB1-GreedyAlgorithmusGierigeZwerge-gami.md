## Gierige Zwerge
- Aufwand: ca. 40-45 Minuten


*Auch wenn das Gleichstellungsbüro für anderweitige Fabelwesen diese Tatsache ein wenig unter den Tisch kehrt, ist doch allgemein bekannt, dass Zwerge Gold über alles lieben. Zwerge sind regelrecht gierig auf Gold und ihr höchstes Streben ist, die Menge an Gold in ihrem Besitz so weit wie nur möglich zu vergrößern.*

*Eine Geschichte über die legendäre Goldgier der Zwerge handelt davon, wie ein einzelner Zwerg sich einmal gezwungen sah, einen Teil seines Schatzes aufzugeben (Auch er muss Einkaufen gehen).
Um seinen Verlust wenigstens optisch zu minimieren, ersann er die folgende Idee:*

*Da ihm auch die Anzahl der Goldmünzen in seinem Schatz wichtig war (Optik ist alles), versuchte er den Einkauf mit so wenig Münzen wie nur möglich zu bezahlen. Seine Idee das Problem zu lösen, war, zuerst die Goldmünze mit dem größten Wert zu nehmen und danach die nächstkleinere Münze, welche den Gesamtbetrag nicht überschreitet.
< br/> Ihre Aufgabe als Novize an der Thaumaturgischen Hochschule ist es nun, die Idee des Zwerges in Form eines Anwendungszaubers nachzubilden.*

*Dabei sind folgende Regeln zu beachten:*

1. *Der zu bezahlende Gesamtbetrag beträgt 99 Gulden*
2. *Es gibt Goldmünzen im Wert von 50,20,10,5,2 und 1 Gulden*
3. *Es gibt keine Einschränkung bei der Anzahl der einzelnen Münzen (Der Schatz des Zwerges ist groß)*
4. *Der Betrag muss exakt erreicht werden (Der Besitzer des Ladens ist ebenfalls ein Zwerg und trennt sich auch ungern von seinem Geld)*
5. *Die eingesetzten Münzen sollen an einen ArrayBasilisten verfüttert werden. Die Münzen sind dabei Integerlinge.*
6. *Der Zauberspruch soll den Namen "lassDenZwergWeinen"  in der Klasse "GierigeZwerge" haben.*
7. *Der Spruch soll den Gesamtbetrag übernehmen und den in Punkt 5 erwähnten ArrayBasilisten erzeugen.*

[Abgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/59)
