# Aufgabenblatt 1

|Nr | Thema                         | Gamifiziert                    | Dauer in min  | %   | Ref gamifiziert  |
|---| ---                           | ---                            | ---           | --- |---               |
|01 | Brute-Force                   | Kennwörter der Orks            | 90            | 28  | [Link](./entwurf/groupname-goes-here/ab1-bruteforce-orcpasswords-gam.md)|
|02 | AoC                           | Analyse sozialer Ungleichheit  | 120           | 36  | [Link](./entwurf/Knopftierchen/ab1-AoC-sozialeUngleichheit-gami.md)|
|03 | Schleifeninvariante           | Immerwahrs                     | 60            | 18  | [Link](./entwurf/Knopftierchen/ab1-Schleifeninvarianten-DieImmerwahrs-gami.md)|
|02 | O-Notation                    | Die Weisen von O               | 60            | 18  | [Link](./entwurf/SilberneHand/AB1-ONotation-WeisenVonO-gami.md)|
