# Speicherstrukturen [30 min]

An der Thaumaturgischen Hochschule der Magie lernt man unter anderem den Umgang mit Zauberkästen.
Zwar besitzen die meisten Novizen schon seit Jahren selbst solche Zauberkästen, aber erst seit dem Kontakt mit dem Urgestein des Wissens erhalten sie die Fähigkeit ins Innere der Zauberboxen zu blicken.
Schaffen sie Zauber die ihren Zauberkasten von innen aussehen lassen, wie in den folgenden Abbildungen?

## Aufgabe 1)
![](memory1.svg)

## Aufgabe 2)
![](memory2.svg)

Damit Sie Ihre magischen Zauberkästen auf Korrektheit überprüfen können, bietet das Zaubereiministerium Ihnen einen Beamten an, der nur dafür zuständig ist. (Damit ist `magicboxes.BoxVerification` gemeint.)
