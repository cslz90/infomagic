# Speicherstrukturen [30 min]

Schreiben Sie je ein Programm, um die folgenden Zustände des Stacks und Heaps zu erzeugen:

## Aufgabe 1)
![](memory1.svg)

## Aufgabe 2)
![](memory2.svg)

Was ist der entscheidende Unterschied zur vorherigen Variante?

## Hinweise
- `Stack` ist der Java (Call-) Stack
- `Heap` ist der Java (Laufzeit-) Heap
- `main`, `fn2` sind Funktionsnamen
- `[1]` ist ein Listenobjekt. Bitte verwenden Sie eine selbstimplementierte, unveränderliche, einfach-verkettete Liste für die Realisierug der Liste in Teilaufgabe 1 und 2. Klassenname, Name und Sichtbarkeit der Attribute der Liste spielen für diese Aufgabe keine Rolle.
- Um den erzeugten Zustand ihres Stacks und Heaps zu überprüfen wird eine Schnittstelle `magicboxes.BoxVerification` zur Verfügung gestellt. Bitte erzeugen Sie die verlangten Zustände und rufen Sie zur Verifikation Ihres Speicherlayouts die Methode `BoxVerification.verify(Object)` mit Ihrer erzeugten Box auf. Also mit der Instanz, die Sie verifizieren wollen. (Sie wollen wahrscheinlich keine Instanz der Klasse `magicboxes.MagicBoxes1` oder `magicboxes.MagicBoxes2` verifizieren.)

```java
package magicboxes;

public interface BoxVerification {
  void verify(Object box);
}

```

## Formalia
Erstellen Sie eine Klasse `MagicBoxes1` und `MagicBoxes2` im Package `magicboxes`. Realisieren Sie die erste, bzw die zweite Aufgabe in der Klasse mit entsprechendem Suffix. Beide Klassen sollen die Methode mit folgender Signatur implementieren:
`public void main(final BoxVerification boxVerification)`.
(Zur Erklärung, was eine `BoxVerification`-Instanz tut und wozu sie benötigt wird, schauen Sie bitte auf die obenstehenden Hinweise.)

## Minimalbeispiel
Als zusätzliche Hilfe ist im folgenden ein Minimalbeispiel zur Veranschaulichung gegeben:

Folgende Speicherstruktur:

![](memory-minimal.svg)

kann mit folgendem Java-Code implementiert werden:
```java

public final class Example {
  static class Element<A> {
    private final A element;
    public Element(A element) {
      this.element = element;
    }
  }

  public static void main(final BoxVerification boxVerification) {
    // new Element<>(2) constructs a new item on the heap
    // `el` is a local variable of the procedure `main`
    Element<Integer> el = new Element<>(2);

    // verify box `el`
    boxVerification.verify(el);
  }
}
```
