## Inhaltsverzeichnis
* Einleitung
* Zauberkiste
* Reise in den ObjektOrient
* Ork Grunzer
* Linked-Basilisten
* Verteidigung gegen magische Wesen
* Generic-Array-Basilisten
* Besen fliegen
* Beutel und Basilisten

## Einleitung (10%)
*Im Rahmen Ihrer magischen Ausbildung an der Thaumaturgischen Hochschule der Magie ist es manchmal nötig Gelerntes zu rekapitulieren und Grundlagen für ihren magischen Fortschritt zu festigen. Da Sie bisher noch nicht das Wahlpflichtmodul „Hellsehen 1“ belegen durften und Sie folglich auch nicht die Lizenz zum Erwerb einer USB-Zauberkugel besitzen, werden wir Ihnen mit den folgenden Aufgaben ein wenig unter die Arme greifen, um Inhalte des letzten zauberhaften Semesters zu wiederholen.*

[OOP-Test](https://moodle.thm.de/mod/quiz/view.php?id=156109)

## Aufgabe 1: Zauberkiste (5%)
*Jeder von Ihnen verfügt über Gegenstände, die ihm persönlich sehr wichtig sind und die er nicht unbedingt mit anderen teilen möchte. Zu diesem Zweck gibt es magische Zauberkisten. Diese magischen Zauberkisten bestehen aus vielen ineinander verschachtelten Zauberkisten, die mit einem Zauberspruch verschlossen sind. Sofern Sie über den nötigen Zauberspruch verfügen, können Sie die äußere Zauberkiste öffnen und die innen-liegende Zauberkiste herausholen. Diesen Vorgang müssen Sie dann so häufig wiederholen, bis Sie irgendwann aus der innersten Zauberkiste Ihren Gegenstand entnehmen können.*

*(Hierbei handelt es sich um ein vereinfachtes Modell, bei dem sich alle verschachtelten Zauberkisten mit dem gleichen Zauberspruch öffnen lassen. Da die Zauberkästen verschachtelt sind, wirkt der Zauberspruch immer nur auf die äußere Zauberkiste.)*

*Weil es sich bei dieser Aufgabe um eine Wiederholung handelt und Sie bereits bewiesen haben, dass Sie diese Inhalte verstanden haben, geben wir Ihnen noch sinnvolle Ratschläge, damit Sie sich möglichst schnell mit den neuen wirklich spannenden Aufgaben zur Vorlesung beschäftigen können:*

- Zauberspruch zum Öffnen jeder Zauberkiste lautet: *„cistam te aperi!“*

[Aufgabe in Moodle](https://moodle.thm.de/mod/assign/view.php?id=159708)

## Aufgabe 2: Reise in den ObjektOrient (6%)
### Aufgabe 2.1 Zaubern
*Ein Novize aus Funktionalia hat von Ihrer Reise durch den Objektorient gehört und will mehr über diesen, für ihn fremden, Ort hören. Um ihm zu erklären wie die Einheimischen sind, zeigen Sie ihm die Java-Klasse Person, die `name`, `vorname` und `alter` enthält. Auf dieser kann mit den Sprüchen  `equals`, `hashCode` und `toString` gearbeitet werden. Um zu verdeutlichen wie sehr sich die Einwohner von Java an einander messen, machen Sie ihre Klasse zusätzlich [Comparable](https://docs.oracle.com/javase/7/docs/api/java/lang/Comparable.html).
Fasziniert von Ihrer Erzählung bittet er Sie, ihm auch noch was von den dortigen Studenten und Professoren zu erzählen. Erstellen sie hierzu zwei weitere Klassen die auf von `Person` abgeleitet sind. Ein `Student` hat zusätzlich eine  `Matrikelnummer` und ein  `Professor` einen `Titel`.*

[Abgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/18)

### Aufgabe 2.2 - 2.6
*Sie reisen durch den ObjektOrient und treffen auf die wunderlichsten Konstrukte.*

[Aufgabe in Moodle](https://moodle.thm.de/mod/quiz/view.php?id=159037)


## Aufgabe 3: Ork Grunzer (20%)
### Aufgabe 3.1
*Das Gleichstellungsbüro für anderweitige Fabelwesen bietet Hilfe bei der Kommunikation mit den neu eingeschriebenen Orks. Dafür wurde folgendes Pergament mit der Übersetzung von Grunz-Lauten in lexikografische Zeichen bereitgelegt:*

[Datei in Moodle](https://moodle.thm.de/pluginfile.php/319381/mod_resource/content/1/morse-code.txt)

*Parsen Sie dieses Pergament und generieren Sie eine [JavaMap](https://docs.oracle.com/javase/7/docs/api/java/util/Map.html), die jedes lexikografische Zeichen auf seine zugehörige Grunzfolge abbildet:
`Character => Character` - `Map<String,String>()`.*

*Da das ständige Herumtragen des Pergaments recht unhandlich ist und wohl kein Ork die Geduld aufbringt jedes Wort erneut zu grunzen bis sie es verstanden haben, empfiehlt ihnen der Sachbearbeiter des Gleichstellungsbüros für anderweitige Fabelwesen einen Zauberspruch zu entwickeln, der ihr gesprochenes Wort in Grunzen übersetzt und umgekehrt.*

*Ihr Sachbearbeiter klärt sie auf das Orks sehr viel wert auf das korrekte Grunzen eines Satzes legen. Da es in der Vergangenheit öfter zu Zwischenfällen aufgrund eines grammatikalischen Fehlers kam, bittet ihr Sachbearbeiter sie nun “Weit hinten, hinter den Wortbergen, fern der Laender Vokalien und Konsonantien leben die Blindtexte. Abgeschieden wohnen sie in Buchstabhausen an der Kueste des Semantik, eines großen Sprachozeans.” mit Hilfe ihrer Formel einmal vorzugrunzen.*

[Abgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/21)

### Aufgabe 3.2
*Damit ein erfolgreiches Finden der Wörter möglich ist, ist es wichtig das die verschiedenen Völker unserer Thaumaturgischen Hochschule das gleiche Alphabet zu benutzen.*

[Aufgabe in Moodle](https://moodle.thm.de/mod/assign/view.php?id=159823)


## Aufgabe 4: Linked-Basilisten (9%)

*Das anmutige und friedvolle Wesen der Array-Basilisten hat sie so fasziniert, dass sie nach Funktionalia reisen um einen Verwandten des Tieres in Funktionalia nahe Haskell zu beobachten: den linked-Basilist. Nach ihrer Rückkehr nach Java fehlen ihnen jedoch diese treuen Geschöpfe. Darum entscheiden sie sich, einen eigenen linked-Basilisten im Objektorient zu züchten. Ihr Notizen aus Funktionalia können ihnen dabei weiterhelfen:*

``` {.haskell}
-- a custom list type
-- either empty (Nil) or an element and the rest of the list (Cons element tail)
data Lst a = Nil | Cons a (Lst a)
```

*Da der linked-Basilist im ObjektOrient zu einer bedrohten Spezies gehört, legt ihnen das Veterinäramt von Java strenge Haltungsvorschriften auf. Achten sie bei der Züchtung nun darauf, dass sie folgende Interface Statuten einhalten:*

``` {.java}
public interface ImmutableList<A> {
  // Prepends elem infront of this list
  ImmutableList<A> cons(A elem);

  // Appends elem at the end of this list.
  ImmutableList<A> append(A elem);

  // Returns the size of the list.
  int size();

  // Returns the element at the index idx.
  A getAt(int idx) throws IndexOutOfBoundsException;
}
```

*Fasziniert von ihrem Engagement gibt ihnen ihr Dozent der Thaumaturgischen Hochschule der Magie einige seltsam verfasste Hinweise:*

>   *Definieren Sie 2 Subklassen, die das Interface `ImmutableList` implementieren. Einmal die leere Liste `Nil` und die nicht-leere Liste `Cons`.*
>   *Die Liste sollte im Speicher wie folgt aussehen: `[5]->[3]->[10]->Nil`. (Die Zahlen sind Beispielwerte. Elemente, Elementtypen und Längen der Liste sind variabel zu halten.)*



*Während ihres Züchtungsversuches erweitert Java den Punkt
`ImmutableList` der Interface Statute um folgende `map()` Methode:*

``` {.java}
ImmutableList<B> map(Function<A,B> fn);
```

*Erneut kommt ihnen ihr Dozent mit fragwürdigen Hinweisen zur Hilfe:*

> *Der Ausdruck `i -> i*2` ist ein [Closure/Lambda](https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html).*
> *Also eine anonyme Funktion, die der Methode `map` übergeben wird.*

[Vorgabe](https://moodle.thm.de/mod/resource/view.php?id=159628)
[Abgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/19)

## Aufgabe 5: Verteidigung gegen magische Wesen (14%)

*Einer der beliebtesten Kurse der Thaumaturgischen Hochschule der Magie ist die Verteidigung gegen magische Wesen. Deshalb kommt es oft dazu, dass mehr Novizen den Kurs belegen wollen als Platz zur verfügung steht. Um dieser Situation Herr zu werden, muss sich jeder Novize auf dem Anmelde Pergament eintragen. Das Pergament wird anschließend in einer `novizen.csv` versiegelt.
Als einfallsreicher Novize fällt Ihnen schnell auf, dass ständiges Aufrollen der Rolle doch sehr mühsam ist. Darum entwickeln Sie eine magische Formel, welche die Informationen der `novizen.csv` einliest.*

[Datei in Moodle](https://moodle.thm.de/mod/resource/view.php?id=159840)

*Da der leitende Erzmagier Prof.Dr.Mag. C. Goldgorgone aus dem ObjektOrient stammt, weigert er sich das Pergament oder dessen Inhalt einzusehen bis es in einer geeigneten magischen Klasse enthalten ist.*

*Bedauerlicherweise wurde während des letzten Kurses ein Novize von einem Array-Basilisten gebissen. Als Folge dieses Zwischenfalles gab die Thaumaturgischen Hochschule der Magie bekannt, dass von nun an alle Novizen das Mindestalter von 18 Jahren erreicht haben müssen, um einen magischen Haftungsausschluss unterschreiben zu können. Verfeinern Sie Ihre Formel, dass sie automatisch alle minderjährigen Novizen herausfiltert.*

*Aus ästhetischen Gründen wünscht Prof. Dr. Mag. C. Goldgorgone nicht, dass die alte Liste mit den entfernten Novizen ausgehängt wird. Darum fordert er, dass Ihre magische Formel ein neues Pergament erstellt und es `berechtigte_novizen.csv` nennt.*

[Abgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/22)

## Aufgabe 6: Generic-Array-Basilisten (13%)

*Im Rahmen Ihrer wunderbar magischen ersten Vorlesung im Hörsaal 20 1/36, durften Sie bereits im Streichelzoo der Datenkreaturen Ihren ersten Kontakt mit dem Array-Basilisten herstellen. Wie bei vielen anderen seltsamen Geschöpfen auf unserem zauberhaften Planeten gilt auch für den Array-Basilisten, dass er nicht einfach von Anfang an in der Form existierte, wie Sie ihn heute kennen. Seine Entwicklung ist allerdings weniger magisch, sondern eher evolutionär zu erklären. Man spricht hier von informatischem "Darwinismus", oder auch „form follows function“. Die spezielle Gattung des Array-Basilisten aus der Vorlesung hat sich mit der Zeit und durch Nahrungsvorkommen in seinem Umfeld auf “Doubles” als Nahrungsquelle spezialisiert und kann sich nun nur noch von diesen ernähren.*

*Aus historischen Überlieferungen der beiden Großmeister Burbetschert und Dumble-Döring wissen wir, dass vorher ein Array-Basilist existiert haben muss, der diese Spezialisierung noch nicht aufwies. Man spricht hier vom sogenannten Generic-Array-Basilist. Dieser hatte die Veranlagung sich von allen Nahrungsquellen zu ernähren, wenn er sich jedoch für seine favorisierte Nahrung entschieden hat, blieb er auch dabei.*

*Wie bei engagierten Forschern üblich, vergessen sie auch manchmal etwas. In unserem Fall ist es die vollständige Implementierung des Generic-Array-Basilisten, dessen fossile Überreste auf Folie 19 angesprochen, aber nicht zu Ende geführt wurden. Ihre Aufgabe bis zur ersten Zauberübung wird sein, den im ObjekOrient heimischen Generic-Array-Basilisten zu rekonstruieren.*

*(Hinweis zu dieser Aufgabe: Namensähnlichkeiten zu Mitgliedern der Technischen Hochschule Mittelhessen sind rein zufällig!)*

[Vorgabe](https://moodle.thm.de/mod/resource/view.php?id=159819)
[Abgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/20)

## Aufgabe 7: Besen fliegen (3%)

*Auf der Thaumaturgischen Hochschule der Magie lernen Sie natürlich auch einen Besen zu fliegen, dem offiziell anerkannten Transportmittel eines jeden Magiers und folglich auch Novizen. Zwar bietet das Reisen durch die Lüfte durchaus seine Reize, doch ist gerade bei Flügen über Java eine Zulassung erforderlich. Für den Erwerb einer solchen Zulassung benötigen Sie eine genormte Identifikationsplakette nach folgendem Muster.*

`^([A-Z]{1,3})-([A-Z]{1,2})-(\d{2,4})$`

[Aufgabe in Moodle](https://moodle.thm.de/mod/quiz/view.php?id=159872)

## Aufgabe 8: Beutel und Basilisten (14%)

*Im Rahmen ihrer magischen Ausbildung wird ihnen auch ein Teil der Generischen Kunst oder Generics, wie man im Objektorient sagt, näher gebracht. Es ist Tradition an der Thaumaturgischen Hochschule der Magie, dass jeder Novize einen von generischen Künstlern handgenähten magischen Beutel erhält, um seine Materialien einfacher zu transportieren.*

### Aufgabe 8.1
*Da ein magischer Beutel unbegrenzt Platz bietet, ist es verlockend alle möglichen Dinge darin zu lagern. Dies kann jedoch dazu führen, dass die Tasche zu schwer wird. Entwickeln sie zur Lösung dieses Problems einen Zauber der generischen Kunst, der ihnen den größten Gegenstand aus der Tasche gibt.*

*Es ist nicht schwer in einer unbegrenzt großen Tasche einen kleinen Gegenstand, wie einen Schlüssel oder eine USB-Zauberkugel, zu verlieren. Als findiger Novize verfassen sie jedoch einen generischen Spruch, der ihnen den kleinsten Gegenstand aus der Tasche gibt.*

[Aufgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/53)

### Aufgabe 8.2
*Ein mächtiger Erzmagus schuf einst den Zauber [Collections.copy](<https://docs.oracle.com/javase/7/docs/api/java/util/Collections.html#copy(java.util.List,%20java.util.List)>).
Über ihn ist nur seine Signatur bekannt:*

`java   public static <T> void copy(List<? super T> dest,List<? extends T> src)`

*Man sagt der Erzmagus, der diesen Zauber schuf, war ein wahnsinniges Genie. Skeptisch ob dies wahr sein könnte, hinterfragen sie diesen Zauber.*

-   *Wieso ist der generische Typ von `dest` `? super T` und nicht `T` ?*
-   *Wieso ist der generische Typ von `src` `? extends T`?*
-   *Was bedeutet überhaupt `? super T` und `? extends T`?*

[Aufgabe in Moodle](https://moodle.thm.de/mod/assign/view.php?id=159846)

### Aufgabe 8.3
*Im Rahmen ihrer Recherche der generischen Künste sind ihnen nun öfter die Aspekte der Kovarianz und Invarianz aufgefallen. Wie würden sie diese einem orkischen Novizen erklären?*

[Aufgabe in Moodle](https://moodle.thm.de/mod/assign/view.php?id=159844)


### Aufgabe 8.4
*Während ihres Besuches im Streichelzoo der Datenkreaturen haben sie das erste Mal einen Basilisten angetroffen. Neben seinem Gehege wurde eine Tafel angebracht, die von seinem Vorfahren erzählt: dem Generic-Basilisten. Würde er dem generischen Aspekt Kovariant oder Invariant folgen? Wieso würde er diesen Aspekt haben? Wäre die Behandlung `List<Object> xs = new ArrayList<Animal>()` laut Java zulässig?*

[Aufgabe in Moodle](https://moodle.thm.de/mod/assign/view.php?id=159875)

### Aufgabe 8.5
*Ist `Object[] objs = new Integer[5];` ein für Array-Basilisten natürliches Verhalten?*

[Aufgabe in Moodle](https://moodle.thm.de/mod/assign/view.php?id=159877)
