## Einleitung für Wiederholungsteil:
Im Rahmen Ihrer *magischen* Ausbildung an der Thaumaturgischen Hochschule der Magie ist es manchmal nötig Gelerntes zu rekapitulieren und Grundlagen für ihren magischen Fortschritt zu festigen. Da Sie bisher noch nicht das Wahlpflichtmodul *„Hellsehen 1“* belegen durften und Sie folglich auch nicht die Lizenz zum Erwerb einer *USB-Zauberkugel* besitzen, werden wir Ihnen mit den folgenden Aufgaben ein wenig unter die Arme greifen, um Inhalte des letzten zauberhaften Semesters zu wiederholen.

## Aufgabe zu Rekursion:
### Zauberkiste:	(geschätzte Dauer 15 min. => 4 XP)
Jeder von Ihnen verfügt über Gegenstände, die ihm persönlich sehr wichtig sind und die er nicht unbedingt mit anderen teilen möchte. Zu diesem Zweck gibt es magische Zauberkisten. Diese magischen Zauberkisten bestehen aus vielen ineinander verschachtelten Zauberkisten, die mit einem Zauberspruch verschlossen sind. Sofern Sie über den nötigen Zauberspruch verfügen, können Sie die äußere Zauberkiste öffnen und die innenliegende Zauberkiste herausholen. Diesen Vorgang müssen Sie dann so häufig wiederholen, bis Sie irgendwann aus der innersten Zauberkiste Ihren Gegenstand entnehmen können.

 *(Hierbei handelt es sich um ein vereinfachtes Modell, bei dem sich alle verschachtelten Zauberkisten mit dem gleichen Zauberspruch öffnen lassen. Da die Zauberkästen verschachtelt sind, wirkt der Zauberspruch immer nur auf die äußere Zauberkiste.)*

Weil es sich bei dieser Aufgabe um eine Wiederholung handelt und Sie bereits bewiesen haben, dass Sie diese Inhalte verstanden haben, geben wir Ihnen noch sinnvolle Ratschläge, damit Sie sich möglichst schnell mit den neuen wirklich spannenden Aufgaben zur Vorlesung beschäftigen können:

- Zauberspruch zum Öffnen jeder Zauberkiste lautet: *„aperire!“*

Ihre **Aufgabe** soll nun sein, dass Sie einen rekursiven abstrakten Zauberspruch (Pseudocode) formulieren, mit dem Sie alle Zauberkisten öffnen, bis Sie an den Gegenstand gelangt sind. Als Orientierungshilfe erhalten Sie hier einen iterativen abstrakten Zauberspruch:

    aperire! (Zauberkiste){
          äußere_Zauberkiste = Zauberkiste
          while (äußere_Zauberkiste enthält nicht den Gegenstand){
                	innere_Zauberkiste = äußere_Zauberkiste.extrahiere_Inhalt
äußere_Zauberkiste = innere_Zauberkiste
          }
          nimm Gegenstand
    }

### Lösung rekursiv:
    aperire! (Zauberkiste){
          if (Zauberkiste enthält Gegenstand){
                nimm Gegenstand
          }
          else {
            Z = innere Zauberkiste aus Zauberkiste
            aperire! (Z)
          }
    }


## Aufgabe Basilist:	(geschätzte Dauer: 30-45 min. je nach Umfang der Folie => 7-11 XP)

Im Rahmen ihrer wunderbar-magischen ersten Vorlesung im Hörsaal 20 1/36, durften Sie bereits im Streichelzoo der Datenkreaturen Ihren ersten Kontakt mit dem Array-Basilisten herstellen. Wie bei vielen anderen seltsamen Geschöpfen auf unserem zauberhaften Planeten, gilt auch für den Array-Basilisten, dass er nicht einfach von Anfang an in der Form  existierte, wie Sie ihn heute kennen. Seine Entwicklung ist allerdings weniger magisch, sondern eher evolutionär zu erklären. Man spricht hier von “Informatischem Darwinismus”, oder auch „form follows function“. Die spezielle Gattung des Array-Basilisten aus der Vorlesung hat sich mit der Zeit und durch Nahrungsvorkommen in seinen Umfeld auf “Doubles” als Nahrungsquelle spezialisiert und kann sich nun nur noch von diesen ernähren.

Aus historischen Überlieferungen der beiden Großmeister Burbetschert und Dumble-Döring wissen wir, dass vorher ein Array-Basilist existiert haben muss, der diese Spezifizierung noch nicht aufwies. Man spricht hier vom sogenannten Generic-Array-Basilist. Dieser hatte die Veranlagung sich von allen Nahrungsquellen zu ernähren, wenn er sich jedoch für seine favorisierte Nahrung entschieden hat, blieb er auch dabei.

***Aufgabe:***

Wie bei engagierten Forschern üblich, vergessen Sie auch manchmal etwas. In unserem Fall ist es die vollständige Implementierung des Generic-Array-Basilisten, dessen fossile Überreste auf Folie 19 angesprochen, aber nicht zu Ende geführt wurde. Ihre Aufgabe bis zur ersten Zauberübung wird sein, den, im Javaorient heimischen, Generic-Array-Basilisten zu rekonstruieren.

(Hinweis zu dieser Aufgabe: Namensähnlichkeiten zu Mitgliedern der Technischen Hochschule Mittelhessen sind nicht beabsichtigt!)

### Lösung:
```java
    import java.lang.reflect.Array;
    public class GenericArrayBasilist<E> implements GenericBasilist<E> {
        E[] content;
        int size;
        public GenericArrayBasilist(Class<E> cls, int capacity){
            content = (E[]) Array.newInstance(cls, capacity);
            size = 0;
        }
        public E get(int idx) {
            return content[idx];
        }
        public void set(E el, int idx) {
            content[idx] = (E) el;
        }
        public int size() {
            return size;
        }
        public void add(E el) {
            content[size] = el;
            size++;
        }
        public void remove(int idx) {
            content[idx] = null;
        }
        public void insert(E el, int idx) {
        }
    }
```

## Aufgabe File IO:	(geschätzte Dauer: 45 min. => 11 XP)

Der technische Fortschritt hält auch in unserer magischen Hochschule Einzug. Jeder Student läuft heute, mit einem magischen flachen Quader in der Hand, über den Campus und zeigt seine übermäßige Zuneigung durch viel Berührung und Blickkontakt. Uns wurde nach Rücksprache mit vertrauenswürdigen Vertretern der Technischen Hochschule Mittelhessen mitgeteilt, dass es diese Geräte auch bei ihnen gibt, und dass diese ganz ohne Magie funktionieren. Die Studenten haben dadurch die Möglichkeit, neben der Pflege ihrer sozialen Kontakte und dem Betrachten von Katzenvideos, ebenso wichtige Informationen für ihr Studium zu recherchieren. Dies geht auf diese moderne Weise sehr schnell, da die Studenten anhand von Schlagwörtern im Text suchen können, ob sich das gewählte Schriftstück oder die gefundene Passage als Quelle der Erkenntnis eignet.

**Aufgabe:**

Weil unsere magische Hochschule stets bemüht ist, dass gerade unsere Magie-Informatik-Novizen ihre "sozialen Kontakte" pflegen, bitten wir Sie als ersten Feldversuch diesen Aufgabentext, den wir Ihnen in Form einer zusätzlichen Datei zur Verfügung stellen, als magischen Strom in ihre Entwicklungsumgebung wie auf dem Javaorient üblich einzulesen und dann mit Hilfe von regulären Ausdrücken nach den Pattern "magisch" und "zauber" zu durchsuchen. Ihr Programm soll am Ende die Häufigkeit der gesuchten Ausdrücke ausgeben.

Ihre Ausgabe soll folgendermaßen aussehen:

    Gefundene Matches für magisch = xx
    Gefundene Matches für zauber = xx

Hinweis zur Aufgabe:
- Bitte stellen Sie beim Einlesen Ihr CharSet auf UTF-8
- es empfiehlt sich, den aus dem Stream gewonnen String für die Suche in ein einheitliches Format (alles groß oder klein geschrieben) zu bringen.
- Sie können auch Strings als Pattern verwenden.

### Lösung:
```java
    import java.io.*;
    import java.util.regex.Matcher;
    import java.util.regex.Pattern;

    public class Regex {

        public static void searchString (String str, String pattern){
            int counter = 0;
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(str);

            while (m.find()){
                counter++;
            }
            System.out.println("Gefundene Matches für "+pattern+" = "+counter);
        }

        public static void main(String[] args){

            try {
                File fileDir = new File("Blatt0.txt");
                String result;
                StringBuffer SB = new StringBuffer();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(
                                new FileInputStream(fileDir), "UTF8"));

                String str;

                while ((str = in.readLine()) != null) {
                    SB.append(str);                         //schreibt kompletten Text der Datei in eine Zeile
                }
                result = SB.toString();
                result = result.toLowerCase();              //alles kleingeschrieben für Suche

                searchString(result, "magisch");
                searchString(result, "zauber");

                in.close();
            }
            catch (UnsupportedEncodingException e)
            {
                System.out.println(e.getMessage());
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        }
    }
```
## The magical Pot

Wir werden des Öfteren von Mitgliedern der Technischen Hochschule Mittelhessen danach gefragt, warum unsere Novizen so glücklich und zufrieden sind. Wir antworten dann nur, dass sie viel trinken. Wir verraten natürlich nicht was sie trinken. Es wird auch häufig nicht weiter gefragt. Es tritt dann häufig ein betretenes Schweigen ein und man verabschiedet sich nur schnell.
Man kann natürlich nicht den Leuten auf die Nase binden, dass sie alle so glücklich sind, weil sie in den besonderen Genuss unserer Veranstaltung Zaubersprüche und Datenkreaturen gekommen sind. Das hätten selbst Sie nicht geglaubt, bevor Sie die erste Vorlesung erlebt hätten. Aber was hat das mit dem „Trinken“ zu tun? Sie haben Recht: Direkt nichts. Aber die folgende Aufgabe beschäftigt sich damit. Sie werden nämlich eingeführt in die Kunst des Tränke-Brauens. Vielleicht gelingt ihnen dann auch ein Trank, mit dem sie sich und ihr Umfeld glücklicher machen können. Wir sind es zumindest schon, wenn Sie sich ernsthaft mit der Aufgabe auseinandersetzen.

Wir werden des Öfteren von Mitgliedern der Technischen Hochschule Mittelhessen danach gefragt, warum unsere Novizen so aufgeweckt und glücklich sind. Wir antworten dann nur, dass sie raue Mengen eines energetischen Modegetränks zu sich nehmen. Wir verraten natürlich nicht die genaue Zusammensetzung. Es wird auch häufig nicht weiter gefragt. Es tritt dann oftmals lediglich betretenes Schweigen ein und man verabschiedet sich nur schnell.
Man kann natürlich nicht den Leuten auf die Nase binden, dass Sie alle so aufgeweckt und glücklich sind, weil Sie neben des Konsums unseres magischen Belebungstranks auch in den besonderen Genuss unserer Veranstaltung “Zaubersprüche und Datenkreaturen” gekommen sind. Das hätten selbst Sie nicht geglaubt, bevor Sie die erste Vorlesung erlebt hätten. Aber nun zurück zu den Tränken. In der folgenden Aufgabe sollen Sie sich mit der magischen Braukunst beschäftigen. Damit auch sie vielleicht in den Genuss eines solchen magischen Belebungstranks kommen können.

Es gibt einen Kessel der auf wundersame Weise das Rezept, das er bekommt, alleine brauen kann. Die magischen Zutaten, die für das jeweilige Rezept benötigt werden, haben eine besondere Eigenschaft: Sie erhöhen den Füllstand des Kessels oft stärker als erwartet. Zum Beispiel erhöht ein Schuss Wolfsmilch den Füllstand um 1/4.
Aus diesem Grund muss der Kessel seinen Füllstand selbst überwachen können, denn wenn er überläuft, ist der Trank hinüber.
#### Aufgabe 1
Implementieren Sie die Klasse "MagicalPot" mit einem geeigneten Konstruktor, den Attributen aktuelles Volumen, maximales Volumen, einen Vorratsschrank und folgenden Zauberformeln:
* brew (volumeWater, recipe)
* isOverrun () : boolean

###### Anmerkungen:
* Achten Sie auf die passenden Datentypen, sofern diese nicht schon angegeben sind.
* Das Rezept (recipe) kann vorerst eine Liste von beliebig vielen Zutaten sein.
* Als weitere Vereinfachung nehmen wir an, dass der MagicalPot eine Map als Vorratsschrank besitzt. Diese map bildet die Zutaten und ihre Auswirkungen auf den Füllstand als Key-Value-Paare ab.

* Diese Map können sie nutzen:

		private Map ingredients = new HashMap();
     	ingredients.put("Wolfsmilch", 0.25);
 		ingredients.put("Krötenwurz", 2);
 		ingredients.put("Sonnenkraut", 0.1);
 		ingredients.put("Sesampulver", 3);
 		ingredients.put("Schnarchessenz", 0.5);
 		ingredients.put("Hasenpfote", 0.7);

#### Aufgabe 2 (noch in Arbeit)

Wie sie bereits wissen, verfügt unsere Thaumaturgische Hochschule der Magie über ein großes Netzwerk von Kooperationspartnern in vielen Bereichen der Industrie und Forschung. Eben ein solches Marktforschungsunternehmen veröffentlichte erst kürzlich eine sehr aussagekräftige Umfrage bzgl. der Zufriedenheit mit den aktuell im Umlauf befindlichen Zauberkesseln und Wünschen für künftige Gerätegenerationen. Sie kennen das ja selber: Alles wird immer schnelllebiger und da der Zauber mit der Fähigkeit an mehreren Orten Gleichzeitig zu sein noch nicht fehlerfrei praktiziert werden kann, schleichen sich beim Brauen oft Fehler ein.
Dies war auch das Hauptgesprächsthema bei unserem magischen Alumni-Treffen von vor zwei Wochen. Ein Alumni, der zur Zeit in der magischen Entwicklungsabteilung eines namhaften Herstellers für Küchen- und Haushaltsgeräte tätig ist, möchte daher mit uns einen neuen Prototyp eines Zauberkessels entwickeln. (Der Hersteller ist auch in der Welt der Muggel für sein sehr hochpreisiges Kochgerät bekannt, welches sich dadurch auszeichnet, dass es mehrere Küchengeräte ineinander kombiniert und wunderbare Gerichte in kurzer Zeit mit wenig Aufwand herstellen kann.)

Ihre Aufgabe soll daher sein…

Implementierung von
* Rezept
* Vorratslager (keine Map mehr)
* Zutat
* Zaubertrank (als erfolgreiches Ergebnis von brew(..))

als Klassen um möglichst vollständige OOP zu bekommen. Zutaten wäre auch eine mögliche Klasse.

(comming soon ;-) )
