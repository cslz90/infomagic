# Aufgabenblatt 0

| Thema                 | Team          | Aufgabe               | Dauer         | XP  | %   | Ref  |
| ---                   | ---           | ---                   | ---           | --- | --- |---   |
| GenericArrayBasilist  | KT            | Basilist              | 30-45 min     | 8   | 13  | [Link](./entwurf/Knopftierchen/ab0-generics-ArrayBasilist.md)|
| LinkedBasilist        | SH            | Linked Lists          | 30 min        | 6   | 9   | [Link](./entwurf/SilberneHand/AB0-LinkedList-LinkedBasilist.md)|
| Objektorientierung    | SH            | Objektorientierung    | 25 min        | 4   | 6   | [Link](./entwurf/SilberneHand/AB0-General-ObjektOrient.md)|
| Rekursion             | KT            | Zauberkiste           | 15 min        | 3   | 5   | [Link](./entwurf/Knopftierchen/ab0-rekursion-Zauberkiste.md)|
| Collections           | SH            | Collections: Abb.     | 60-80 min     | 13  | 20  | [Link](./entwurf/SilberneHand/AB0-Collections-OrcGrunzer.md)|
| File-I/O              | SH            | Verteidigung          | 45 min        | 9   | 14  | [Link](./entwurf/SilberneHand/AB0-FileIO-VerteidigungGegenMagischeWesen.md)|
| Regex                 | SH            | Regex                 | 5-10 min      | 2   | 3   | [Link](./entwurf/SilberneHand/AB0-Regex-Besenfliegen.md)|
| Generics              | SH            | Generics              | 40-50 min     | 9   | 14  | [Link](./entwurf/SilberneHand/AB0-Generics-BeutelUndBasilisten.md)|
|                       |               |                       | ~ 4-5 Std.    |     |     |       | 
| + OOP-Moodleaufgaben  |               |                       |               | 10  | 16  |       |
|                       |               |                       | ~ 1 Std.      |     |     |       | 
