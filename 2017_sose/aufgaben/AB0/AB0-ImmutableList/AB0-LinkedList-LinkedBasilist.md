# Linked Lists [30 min]

[G] Das anmutige und friedvolle Wesen der Array-Basilisten hat sie so fasziniert, dass sie nach Funktionalia reisen um einen Verwandten des Tieres in Funktionalia nahe Haskell zu beobachten: den linked-Basilist.
Nach ihrer Rückkehr nach Java fehlen ihnen jedoch diese treuen Geschöpfe.
Darum entscheiden sie sich, einen eigenen linked-Basilisten im Objektorient zu züchten.
Ihr Notizen aus Funktionalia können ihnen dabei weiterhelfen:

```haskell
-- a custom list type
-- either empty (Nil) or an element and the rest of the list (Cons element tail)
data Lst a = Nil | Cons a (Lst a)
```

1.

[G] Da der linked-Basilist im Objektorient zu einer bedrohten Spezies gehört, legt ihnen das Veterinäramt von Java strenge Haltungsvorschriften  auf.
Achten sie bei der Züchtung nun darauf, dass sie folgende Interface Statuten  einhalten:

[K] Implementieren Sie die oben stehende Haskell Variante einer verketteten Liste in Java.
Die Liste sollte dem folgenden Interface genügen:

```java
public interface ImmutableList<A> {
  /** Prepends elem infront of this list */
  ImmutableList<A> cons(A elem);
  /** Appends elem at the end of this list. */
  ImmutableList<A> append(A elem);
  /** Returns the size of the list. */
  int size();
  /** Returns the element at the index idx. */
  A getAt(int idx) throws IndexOutOfBoundsException;
}
```

Fasziniert von ihrem Engagement gibt ihnen ihr Dozent der Thaumaturgischen Hochschule der Magie einige seltsam verfasste Hinweise:
> - Definieren Sie 2 Subklassen, die das Interface `ImmutableList` implementieren.
Einmal die leere Liste `Nil` und die nicht-leere Liste `Cons`.
- Die Liste sollte im Speicher wie folgt aussehen: `[5]->[3]->[10]->Nil`.
(Die Zahlen sind Beispielwerte.
Elemente, Elementtypen und Längen der Liste sind variabel zu halten.)


2.

[G] Während ihres Züchtungsversuches erweitert Java den Punkt `ImmutableList` der Interface Statute um folgende `map()` Methode:

[K] Erweitern Sie das Interface `ImmutableList` um eine `map()` Methode:

```java
ImmutableList<B> map(Function<A,B> fn);
```

`map` wendet auf jedes Element der Liste die gegebene Funktion an.
Sie könnte wie folgt verwendet werden:

```java
ImmutableList<Integer> lst = ...;
//functional (Java 8) style
ImmutableList<Integer> doubled = lst.map(i -> i*2);
//anonymous class alternative
ImmutableList<Integer> doubled = lst.map(new Function<Integer,Integer>(){
  @Override Integer apply(Integer i) {
    return i*2;
  }
});
```

Erneut kommt ihnen ihr Dozent mit fragwürdigen Hinweisen zur Hilfe:
> Der Ausdruck `i -> i*2` ist ein [Closure/Lambda](https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html).
Also eine anonyme Funktion, die der Methode `map` übergeben wird.
