#Generics[40 min]
Im Rahmen ihrer magischen Ausbildung wird ihnen auch ein Teil der Generischen Kunst oder Generics, wie man im Objektorient sagt, nähergebracht.
Es ist Tradition an der Thaumaturgischen Hochschule der Magie, dass jeder Novize einen von generischen Künstlern handgenähten magischen Beutel erhält, um seine Materialien einfacher zu transportieren.

1.[G] Da ein magischer Beutel unbegrenzt Platz bietet, ist es verlockend alle möglichen Dinge darin zu lagern.
Dies kann jedoch dazu führen, dass die Tasche zu schwer wird.
Entwickeln sie zur Lösung dieses Problems einen Zauber der generischen Kunst, der ihnen den größten Gegenstand aus der Tasche gibt.

[K] Definieren Sie eine generische Funktion `max`, die das Maximum einer beliebigen Kollektion von vergleichbaren (comparable) Elementen liefert.


2.[G] Es ist nicht schwer in einer unbegrenzt großen Tasche einen kleinen Gegenstand, wie einen Schlüssel oder eine USB-Zauberkugel, zu verlieren.
Als findiger Novize verfassen sie jedoch einen generischen Spruch, der ihnen den kleinsten Gegenstand aus der Tasche gibt.

[K] Definieren Sie eine generische Funktion `min`, die das Minimum einer beliebigen Kollektion von vergleichbaren (comparable) Elementen liefert.


3.[G] Ein mächtiger Erzmagus schuf einst den Zauber [Collections.copy](https://docs.oracle.com/javase/7/docs/api/java/util/Collections.html#copy(java.util.List,%20java.util.List).
Über ihn ist nur seine Signatur bekannt:

[K] Die Funktion [Collections.copy](https://docs.oracle.com/javase/7/docs/api/java/util/Collections.html#copy(java.util.List,%20java.util.List))
    hat die folgende Signatur:

  ```java
  public static <T> void copy(List<? super T> dest,List<? extends T> src)
  ```
[G] Man sagt der Erzmagus, der diesen Zauber schuf, war ein wahnsinniges Genie.
Skeptisch ob dies wahr sein könnte, hinterfragen sie diesen Zauber.

  * Wieso ist der generische Typ von `dest` `? super T` und nicht `T` ?
  * Wieso ist der generische Typ von `src` `? extends T`?
  * Was bedeutet überhaupt `? super T` und `? extends T`?


4.[G] Im Rahmen ihrer Recherche der generischen Künste sind ihnen nun öfter die Aspekte der Kovarianz und  Invarianz aufgefallen.
Wie würden sie diese einem orkischen Novizen erklären?

[K]Was ist Kovarianz und Invarianz?

5.[G] Während ihres Besuches im Streichelzoo der Datenkreaturen haben sie das erste Mal einen Basilisten angetroffen.
Neben seinem Gehege wurde eine Tafel angebracht, die von seinem Vorfahren erzählt: dem Generic-Basilisten.
Würde er dem generischen Aspekt *Kovariant* oder *Invariant* folgen?
Wieso würde er diesen Aspekt haben?
Wäre die  Behandlung `List<Object> xs = new ArrayList<Animal>()` laut Java zulässig?

[K] Sind generische Typen `List<A>` *Kovariant* oder *Invariant*? Wieso sind sie es?
  Ist die Zuweisung `List<Object> xs = new ArrayList<Animal>()` valide?

6.[G] Ist `Object[] objs = new Integer[5];` ein für Array-Basilisten natürliches Verhalten?

[K] Verhalten sich Arrays genauso? `Object[] objs = new Integer[5];`
