## Einleitung für Wiederholungsteil:
Im Rahmen Ihrer *magischen* Ausbildung an der Thaumaturgischen Hochschule der Magie ist es manchmal nötig Gelerntes zu rekapitulieren und Grundlagen für ihren magischen Fortschritt zu festigen.
Da Sie bisher noch nicht das Wahlpflichtmodul *„Hellsehen 1“* belegen durften und Sie folglich auch nicht die Lizenz zum Erwerb einer *USB-Zauberkugel* besitzen, werden wir Ihnen mit den folgenden Aufgaben ein wenig unter die Arme greifen, um Inhalte des letzten zauberhaften Semesters zu wiederholen.
