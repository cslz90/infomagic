# File IO [20 min]

[G] Einer der beliebtesten Kurse der Thaumaturgischen Hochschule der Magie ist die Verteidigung gegen magische Wesen.
Deshalb kommt es oft dazu, dass mehr Novizen den Kurs belegen wollen als Platz zur verfügung steht.
Um dieser Situation Herr zu werden, muss sich jeder Novize auf dem Anmelde Pergament eintragen.
Das Pergament wird anschließend in einer `novizen.csv` versiegelt.

Als einfallsreicher Novize fällt ihnen schnell auf, dass ständiges Aufrollen der Rolle doch sehr mühsam ist.
Darum entwickeln sie eine magische Formel, die die Informationen der `novizen.csv` einliest.

[K]  Lesen Sie die folgende strukturierte Datei (`personen.csv`) in eine Java Liste ein:
```
Nicolarius, 22
Alissiara, 19
Denneris, 17
Christopherus, 30
Anhara, 18
Alcaperus, 7
Prunak, 80
```
[G] Da der leitende Erzmagier Prof.Dr.Mag. C. Goldgorgone aus dem Objektorient stammt, weigert er sich das Pergament oder dessen Inhalt einzusehen bis es in einer geeigneten magischen Klasse enthalten ist.

[K] Definieren Sie eine geeignete Klasse, die die Felder der `personen.csv` Datei enthält.

[G] Bedauerlicherweise wurde während des letzten Kurses ein Novize von einem Array-Basilisten gebissen.
Als Folge dieses Zwischenfalles gab die  Thaumaturgischen Hochschule der Magie das von nun an alle Novizen das Mindestalter von 18 Jahren erreicht haben müssen um  einen magischen Haftungsausschluss unterschreiben zu können.
Verfeinern sie ihre Formel, dass sie automatisch alle minderjährigen Novizen herausfiltert.

[K]  Filtern Sie alle Personen heraus, die jünger als 18 sind.

[G] Aus ästhetischen Gründen wünscht Prof.Dr.Mag. C. Goldgorgone nicht, dass die alte Liste mit den entfernten Novizen ausgehängt wird.
Darum fordert er, dass ihre magische Formel ein neues Pergament erstellt und es `berechtigte_novizen.csv` nennt.

[K] Schreiben Sie die resultierende Liste in eine neue Datei (`neue-personen.csv`).
> *Hinweis: Achten Sie auf das Encoding (UTF-8) des Pergamentes*
