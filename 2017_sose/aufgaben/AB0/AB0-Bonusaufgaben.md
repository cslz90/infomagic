# Aufgabenblatt 0

| Team          | Aufgabe               | Dauer	        | XP  | Ref  |
| ---           | ---                   | ---           | --- |---   |
| KT            | FileIO                | 45 min        | 11  | [Link](./entwurf/Knopftierchen/ab0-fileIO_Regex-magical_DRY_counter.md)|
| KT            | OOP                   | 15 min        | 4   | [Link](./entwurf/Knopftierchen/ab0-OOP-magicalPot.md)|
| SH            | Collections           | 20 min        | 5   | [Link](./entwurf/SilberneHand/AB0-Collections-BasilistenDiät.md)|
| SH            | Zipfsches Gesetz      | 120 min       | 30  | [Link](./entwurf/SilberneHand/AB0-General-ZipfschesGesetz.md)|
| SH            | Rekursion             | 30 min        | 8   | [Link](./entwurf/SilberneHand/AB0-Recursion-BonsaiHydrachen.md)|
| SH            | Speicherstrukturen    | 30 min        | 8   | [Link](./entwurf/SilberneHand/AB0-Speicherstrukturen-Zauberkasten.md)|

Summe: 66 XP

Bei 128 XP/Woche bleiben übrig: 128 - 32 (AB) - 66/2 (Bonus) = 63 XP zum freien Sammeln
