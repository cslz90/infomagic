# Zipfsches Gesetz [file io / regex / collections / generics] [2 Stunden]

[G] Im Rahmen des bürokratischen Umbruchs innerhalb des Statistischen Ordens, bittet Java die Thaumaturgischen Hochschule der Magie als neutrale Instanz eines der neuen Gesetze auf magische Art zu belegen.

[Das Zipfsche Gesetz](https://en.wikipedia.org/wiki/Zipf's_law) besagt: wenn die Elemente einer Menge – beispielsweise die Wörter eines Textes – nach ihrer Häufigkeit geordnet werden, ist die Wahrscheinlichkeit ihres Auftretens umgekehrt proportional zur Position innerhalb der Reihenfolge.
In anderen Worten kommt das häufigste Wort zweimal häufiger als das zweithäufigste Wort vor, dreimal häufiger als das dritthäufigste Wort usw.

Ihr Spruch soll ein beliebig in deutscher Sprache verfasstes, versiegeltes Stück Pergament in eine geeignete Collection nach Java Norm einlesen.
Nutzen sie die regex Norm um Sonderzeichen oder kurze Worte (length < 4) zu ignorieren.

Sortieren Sie die Wörter nach ihrer Häufigkeit: Entweder sie verwenden `sort` aus den Java Collections oder sie implementieren BubbleSort (BubbleSort kennen sie aus ihrer Exkursion in den Objektorient), um die Bürokraten in Java zu beeindrucken.

Wie hoch ist die Abweichung (in %) bei Ihrem Text? Wie viele Wörter gibt es in dem Text und wie viele Wörter muss ein Ausländer mindestens lernen, um 50% des Textes zu verstehen?

Dieses Gesetz gilt nicht nur in der Linguistik, sondern auch in anderen Bereichen.
Zum Beispiel kann man versuchen vorherzusagen, wie sich die Zahl der Einwohner in unseren Städten ändern wird.
Dazu lesen Sie das folgende Pergament in eine Collection ein:
```
Stadt / Land, Kürzel  Einwohner
Java/ Objektorient, JA		9.007.346
.Net / Objektorient, NT	8.000.000
Haskell/ Funktionalia, HA	72
Lisp / Funktionalia, LS	46.750.359
```
Wir wollen den bestehenden Spruch für Wörter möglichst wenig ändern aber auch ermöglichen die Einwohnerzahlen mithilfe des Gesetzes zu analysieren.
Was hilft dabei?

[K] [Das Zipfsche Gesetz](https://en.wikipedia.org/wiki/Zipf's_law) besagt: wenn die Elemente einer Menge – beispielsweise die Wörter eines Textes – nach ihrer Häufigkeit geordnet werden, ist die Wahrscheinlichkeit ihres Auftretens umgekehrt proportional zur Position innerhalb der Reihenfolge.
In anderen Worten kommt das häufigste Wort zweimal häufiger als das zweithäufigste Wort vor, dreimal häufiger als das dritthäufigste Wort usw.

Lesen Sie die Wörter aus einer beliebigen Datei mit dem deutschen Text in eine geeignete Java Collection ein.
Nutzen Sie reguläre Ausdrücke um Sonderzeichen und kurze Wörter (length < 4) zu ignorieren.
Sortieren Sie die Wörter nach ihrer Häufigkeit: Entweder sie verwenden `sort` aus den Java Collections oder sie implementieren BubbleSort.

Überprüfen Sie das Zipfsche Gesetz: wie hoch ist die Abweichung (in %) bei Ihrem Text?
Wie viele Wörter gibt es in dem Text und wie viele Wörter muss ein Ausländer mindestens lernen, um 50% des Textes zu verstehen?

Dieses Gesetz gilt nicht nur in der Linguistik, sondern auch in anderen Bereichen.
Zum Beispiel kann man versuchen vorherzusagen, wie sich die Zahl der Einwohner in deutschen Städten ändern wird.
Dazu lesen Sie die folgende Datei in eine Collection ein:
```
Stadt / Land, Kürzel  Einwohner
Frankfurt am Main / Hessen, HE  732.688
Berlin / Berlin, BE	3.520.031
Stuttgart / Baden-Württemberg, BW 623.738
Düsseldorf / Nordrhein-Westfalen, NW  535.753
Leipzig / Sachsen, SN 560.472
Dortmund / Nordrhein-Westfalen, NW  586.181
Essen / Nordrhein-Westfalen, NW 582.624
Hamburg / Hamburg, HH	1.787.408
München / Bayern, BY	1.450.381
Köln / Nordrhein-Westfalen, NW	1.060.582
```
Wir wollen das bestehende Programm für Wörter möglichst wenig ändern aber auch ermöglichen die Einwohnerzahlen mithilfe des Gesetzes zu analysieren.
Was hilft dabei?
