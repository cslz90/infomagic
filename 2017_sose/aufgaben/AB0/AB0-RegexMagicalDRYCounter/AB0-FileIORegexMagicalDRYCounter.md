## Aufgabe File IO:	(geschätzte Dauer: 45 min. => 11 XP)
## Bonusaufgabe: Wie viel Magie ist erträglich?
*Der technische Fortschritt hält auch in unserer magischen Hochschule Einzug. Jeder Student läuft heute, mit einem scheinbar magischen flachen Quader in der Hand, über den Campus und zeigt seine übermäßige Zuneigung durch viel Berührung und Blickkontakt.
Uns wurde nach Rücksprache mit vertrauenswürdigen Vertretern der Technischen Hochschule Mittelhessen mitgeteilt, dass es diese Geräte auch bei ihnen gibt, und dass diese ganz ohne Magie funktionieren. Die Studenten haben dadurch die Möglichkeit, neben der Pflege ihrer sozialen Kontakte und dem Betrachten von Katzenvideos, ebenso wichtige Informationen für ihr Studium zu recherchieren.
Dies geht auf diese moderne Weise sehr schnell, da die Studenten anhand von Schlagwörtern im Text suchen können, ob sich das gewählte Schriftstück oder die gefundene Passage als Quelle der Erkenntnis eignet.*

*Weil unsere magische Hochschule stets bemüht ist, dass gerade unsere Magie-Informatik-Novizen ihre "sozialen Kontakte" pflegen, bitten wir Sie als ersten Feldversuch den Text, den wir Ihnen in Form einer zusätzlichen Datei zur Verfügung stellen, einzulesen. Durchsuchen Sie mit Hilfe von regulären Ausdrücken nach folgenden Besonderheiten im Text:*
* *magische Worte*
* *"zauber"-Worte*
* *magische Tunnel (Links)*
* *Code-Schnipsel*

[Datei in Moodle link noch einfügen]()

*Entwickeln Sie einen Zauberspruch `numerare` welcher ein Pattern übernimmt und die Häufigkeit der gesuchten Besonderheiten ermittelt.*

*Hinweis zur Aufgabe:*
- *Bitte stellen Sie, beim Einlesen, Ihr CharSet auf UTF-8*

[Abgabe in Dozentron, link noch einfügen]()

***Aufgabe in Klartext***
Bitte lesen Sie die bereitgestellte [Datei, link noch einfügen]() als FileInputStream in Java ein.
Verwenden Sie reguläre Ausdrücke um den Text nach den Worten mit "magisch" und "zauber" zu durchsuchen, sowie alle Code-Schnipsel und Links zu finden.

Erstellen Sie eine Klasse mit dem Namen `RegexScan` im package `fileio`.
Entwickeln Sie in der Klasse `RegexScan` eine Prozedur mit der Signatur `public static String readEntireFile(final String filePath)` die den gesamten Inhalt der Datei mit Dateipfad `filePath` einließt und als String returniert.
Entwerfen sie eine zweite Prozedur (ebenfalls in der Klasse `RegexScan`) mit der Signatur `public static int numerare(String str, String pattern)` die in `str` alle Vorkommnisse des Musters `pattern` sucht und diese zählt.

Hinweise:
- Das Pattern `pattern`, dass der Prozedur `numerare` gegeben wird ist ein valider Regex.
- Stellen Sie Ihr Charset für das Einlesen der Datei auf UTF-8!
- Wenn beim Lesen der Datei eine Ausname (Exception) auftritt, soll die Prozedur `readEntireFile` `null` zurückgeben.
