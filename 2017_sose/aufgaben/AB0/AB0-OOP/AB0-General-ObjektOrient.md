# Objektorientierung - Basic Java [10 + 5 + 5 + 5 * 1 = 25 min]

1.[G] Ein Novize aus Funktionalia hat von Ihrer Reise durch den Objektorient gehört und will mehr über diesen, für ihn fremden, Ort hören.
Um ihm zu erklären wie die Einheimischen sind, zeigen Sie ihm die Java-Klasse Person, die `name`, `vorname` und `alter` enthält. Auf dieser kann mit den Sprüchen  `equals`, `hashCode` und `toString`gearbeitet werden.

[K] Definieren Sie eine Klasse Person mit den Feldern `name`, `vorname` und `alter`. Überschreiben Sie die Methoden `equals`, `hashCode` und `toString`.

2.[G] Um zu verdeutlichen wie sehr sich die Einwohner von Java an einander messen, machen Sie ihre Klasse zusätzlich [Comparable](https://docs.oracle.com/javase/7/docs/api/java/lang/Comparable.html).

[K] Machen Sie die Klasse Person vergleichbar, in dem Sie [Comparable](https://docs.oracle.com/javase/7/docs/api/java/lang/Comparable.html) implementieren. Überlegen Sie sich, anhand welcher
  Eigenschaft eine Person verglichen werden kann.

3.[G] Fasziniert von Ihrer Erzählung bittet er Sie, ihm auch noch was von den dortigen Studenten und Professoren zu erzählen.
Erstellen sie hierzu zwei weitere Klassen die auf von `Person` abgeleitet sind.
Ein `Student` hat zusätzlich eine *Matrikelnummer* und ein `Professor` einen *Titel*

[K] Erstellen Sie 2 Subklassen von Person: `Student` und `Professor`.
Ein Student hat zusätzlich eine *Matrikelnummer* und ein Professor einen *Titel*.

4.[G] Beeindruckt von Ihrem Wissen fragt er nun, ob Sie ihm einige Verwendungen von `final` erläutern können, auf die er gestoßen ist während er selbst über den Objektorient nachgelesen hatte.
Im ersten Fall handelt es sich um den `final` Modifizieren vor einer Klasse.
Was bewirkt dieser?

[K] Was bewirkt der `final` Modifizierer vor Klassen?
    ```java
    public final class Foo {}
    ```

5.[G] Im zweiten Beispiel handelt es sich um den `final` Modifizierer vor einer Methode.

[K] Was bewirkt der `final` Modifizierer vor Methoden?
    ```java
    public final void foo();
    ```

6.[G] Nun sind auch Sie ins Schwärmen geraten und erzählen ihm ohne weitere Aufforderung, wozu man den `static` Modifizierer bei *geschachtelten* Klassen verwendet und welchen Aufwand man betreiben muss, um ein Objekt aus einer geschachtelten Klasse zu erzeugen, die nicht mit `static` gekennzeichnet ist.

[K] Was bewirkt der `static` Modifizierer vor *geschachtelten* Klassen?
  Wie würden Instanzen von `Bar` erzeugt werden, wenn das `static` fehlt?
  ```java
  public class Foo {
    static class Bar {
    }
  }
  ```

7.[G] Als Sie mit Ihrem Exkurs fertig sind, fragt er Sie noch zu einem weiteren Phänomen, dass er während seiner Recherche entdeckt hat.
Für den Sinn des `...` in folgendem Beispiel konnte er keine gute Erklärung finden.

[K] Was bewirken die `...` hinter dem generischen Datentypen `A`?
  ```java
  List<A> toList(A... args);
  ```
>Werfen Sie einen Blick in die [ObservableList](https://docs.oracle.com/javase/8/javafx/api/javafx/collections/ObservableList.html).

8.[G] Um ihm auch die Schattenseiten des Objektorients zu offenbaren, zeigen Sie ihm zum Abschluss noch das Problem der default Sichtbarkeit von Variablen in Java anhand des folgenden Beispiels.

[K] Was für eine Sichtbarkeit hat die Variable `x` im folgenden Beispiel?
  Welche Probleme können auftreten?
  ```java
    public class X {
      int x;
    }
  ```
