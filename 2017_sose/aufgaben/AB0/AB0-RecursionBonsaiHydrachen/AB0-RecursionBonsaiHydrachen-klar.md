# Reihenfolgen ohne Doppel-Null

Die gesuchten Reihenfolgen bestehen aus Nullen und Einsen.
Zwei aufeinanderfolgende Nullen sind in diesen Reihenfolgen nicht erlaubt. Beispielsweise sind die folgenden Reihenfolgen erlaubt:
- `11111`
- `1`
- `1010`
- `0111`
- ...

Die Reihenfolge `10100` ist nicht erlaubt, da sie am Ende zwei direkt aufeinanderfolgende Nullen enthält.
Schreiben Sie eine rekursive Funktion `f(a, b)`, die die Anzahl der erlaubten Reihenfolgen für angegebene `a` (die Anzahl von Nullen) und `b` (die Anzahl von Einsen) zurückliefert.

# Hinweise

Die Thaumaturgischen Hochschule der Magie erteilt hierfür folgende Verarbeitungshinweise:

- Zeichnen Sie einen Binärbaum, in dem jede Kante 0 oder 1 entspricht (jeder Knoten stellt eine Reihenfolge dar).
Was stellen die Blätter, deren Weg von der Wurzel `a` Null-Kanten und `b` Eins-Kanten enthält, dar?
- Schauen wir ein Beispiel an: `a = 1, b = 2`, die Länge die gesuchten Reihenfolgen ist also `3`.
An der ersten Stelle der Reihenfolge kann entweder Null oder Eins stehen.
Der Rest der Reihenfolge hat die Länge `2` und kann als ein Problem mit den Parametern `a = 0, b = 2` bzw. `a = 1, b = 1` betrachtet werden.
Auf diese Weise wird die Aufgabe in zwei einfacheren Aufgaben zerlegt, die man durch Rekursion implementieren kann.
