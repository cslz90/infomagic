# Bonsai-Hydrachen

*Um auch nicht magiebegabten Wesen einen Einblick in unsere magische Welt zu gewähren, fertigen Novizen der Thaumaturgischen Hochschule der Magie kleine Souvenirboxen, in denen kleine Baumhydren (sogenannte Bonsai-Hydrachen) leben, an.*

*Diese Wesen sind besonders handzahm und umgänglich, erfordern jedoch eine strenge Ernährung.*

*Bonsai-Hydrachen ernähren sich von magischen Einsen und Nullen.*
*Dabei dürfen sie jedoch niemals nach Mitternacht gefüttert werden oder 2 Nullen hintereinander fressen.*
*Da die Erklärung, weshalb ein Bonsai-Hydrache keine zwei Nullen fressen darf, den Verstand eines nichtmagischen Wesens übersteigt, muss eine magische Lösung gefunden werden.*
*Diese muss aus einer gegebenen Menge magischer Einsen und Nullen die Anzahl der möglichen gesunden Kombinationen berechnen.*

*So weiß das nichtmagische Wesen, dass bei einer Zahl größer als eins, die Fütterung unbedenklich ist.*

*Um auch im Objektorient verkauft werden zu können, muss die Box gemäß MJN (magische Java Norm) den Spruch Rekursio enthalten.*

## Hinweise

Die Thaumaturgischen Hochschule der Magie erteilt hierfür folgende Verarbeitungshinweise:

- Zeichnen Sie einen Binärbaum, in dem jede Kante 0 oder 1 entspricht (jeder Knoten stellt eine Reihenfolge dar).
Was stellen die Blätter, deren Weg von der Wurzel `a` Null-Kanten und `b` Eins-Kanten enthält, dar?
- Betrachten wir ein Beispiel an: `a = 1, b = 2`, die Länge die gesuchten Reihenfolgen ist also `3`.
An der ersten Stelle der Reihenfolge kann entweder Null oder Eins stehen.
Der Rest der Reihenfolge hat die Länge `2` und kann als ein Problem mit den Parametern `a = 0, b = 2` bzw. `a = 1, b = 1` betrachtet werden.
Auf diese Weise wird die Aufgabe in zwei einfacheren Aufgaben zerlegt, die man durch Rekursion implementieren kann.

## Abgabe

Den Link zur Abgabe auf Dozentron finden Sie hier: [http://dozentron.mni.thm.de/input_output_tasks/78](http://dozentron.mni.thm.de/input_output_tasks/78)
