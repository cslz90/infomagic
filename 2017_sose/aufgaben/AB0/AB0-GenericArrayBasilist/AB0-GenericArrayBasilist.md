## Aufgabe Basilist:	(geschätzte Dauer: 30-45 min. je nach Umfang der Folie => 7-11 XP)

Im Rahmen ihrer wunderbar-magischen ersten Vorlesung im Hörsaal 20 1/36, durften Sie bereits im Streichelzoo der Datenkreaturen Ihren ersten Kontakt mit dem Array-Basilisten herstellen.
Wie bei vielen anderen seltsamen Geschöpfen auf unserem zauberhaften Planeten gilt auch für den Array-Basilisten, dass er nicht einfach von Anfang an in der Form existierte, wie Sie ihn heute kennen.
Seine Entwicklung ist allerdings weniger magisch, sondern eher evolutionär zu erklären.
Man spricht hier von “Informatischem Darwinismus”, oder auch „form follows function“.
Die spezielle Gattung des Array-Basilisten aus der Vorlesung hat sich mit der Zeit und durch Nahrungsvorkommen in seinem Umfeld auf “Doubles” als Nahrungsquelle spezialisiert und kann sich nun nur noch von diesen ernähren.

Aus historischen Überlieferungen der beiden Großmeister Burbetschert und Dumble-Döring wissen wir, dass vorher ein Array-Basilist existiert haben muss, der diese Spezifizierung noch nicht aufwies.
Man spricht hier vom sogenannten Generic-Array-Basilist.
Dieser hatte die Veranlagung sich von allen Nahrungsquellen zu ernähren, wenn er sich jedoch für seine favorisierte Nahrung entschieden hat, blieb er auch dabei.

***Aufgabe:***

Wie bei engagierten Forschern üblich, vergessen Sie auch manchmal etwas.
In unserem Fall ist es die vollständige Implementierung des Generic-Array-Basilisten, dessen fossile Überreste auf Folie 29 angesprochen, aber nicht zu Ende geführt wurden.
Ihre Aufgabe bis zur ersten Zauberübung wird sein, den im Javaorient heimischen Generic-Array-Basilisten zu rekonstruieren.

(Hinweis zu dieser Aufgabe: Namensähnlichkeiten zu Mitgliedern der Technischen Hochschule Mittelhessen sind rein zufällig!)

***Aufgabe in Klartext:***
Erstellen Sie in Java eine generische Klasse GenericArrayBasilist, die das vorgegebene generische Interface GenericBasilist aus Folie 29 implementiert, und vervollständigen Sie die Methoden.

### Lösung:
```java
import java.lang.reflect.Array;

public class GenericArrayBasilist<E> implements GenericBasilist<E> {
    private E[] content;
    private int size;
    private int capacity;
    private final Class<E> cls;

    @SuppressWarnings("unchecked")
    public GenericArrayBasilist(Class<E> cls, int capacity){
        content = (E[]) Array.newInstance(cls, capacity);
        size = 0;
        this.capacity = capacity;
        this.cls = cls;
    }

  private void increaseCapacity() {
    E[] tmp = (E[]) Array.newInstance(cls, size*2);
    for (int i = 0; i < content.length; i++) {
      tmp[i] = content[i];
    }
    capacity = size*2;
    content = tmp;
  }

    public E get(int idx) {
      return content[idx];
    }
    public void set(E el, int idx) {
        content[idx] = (E) el;
    }
    public int size() {
        return size;
    }
    public void add(E el) {
      if (size == content.length) {
        increaseCapacity();
      }
        content[size] = el;
        size++;
    }
    public void remove(int idx) {
        content[idx] = null;
        size--;
    }
    public void insert(E el, int idx) {
      if(idx < 0 || idx > capacity)
        throw new IndexOutOfBoundsException();
      else if (size == content.length) {
        increaseCapacity();
      }

      //shift all elements with index > idx to the right
      for(int i = size - 1; i >= idx; i--) {
        content[i + 1] = content[i];
      }

      content[idx] = el;
      size++;
    }
}
```
