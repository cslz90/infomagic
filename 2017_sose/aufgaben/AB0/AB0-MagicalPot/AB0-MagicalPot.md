## The magical Pot (geschätzte Dauer: 15 Min. => 4 XP)

Wir werden des Öfteren von Mitgliedern der Technischen Hochschule Mittelhessen danach gefragt, warum unsere Novizen so aufgeweckt und glücklich sind.
Wir antworten dann nur, dass sie raue Mengen eines energetischen Modegetränks zu sich nehmen.
Wir verraten natürlich nicht die genaue Zusammensetzung.
Es wird auch häufig nicht weiter gefragt.
Es tritt dann oftmals lediglich betretenes Schweigen ein und man verabschiedet sich nur schnell.
Man kann natürlich nicht den Leuten auf die Nase binden, dass Sie alle so aufgeweckt und glücklich sind, weil Sie neben des Konsums unseres magischen Belebungstranks auch in den besonderen Genuss unserer Veranstaltung “Zaubersprüche und Datenkreaturen” gekommen sind.
Das hätten selbst Sie nicht geglaubt, bevor Sie die erste Vorlesung erlebt hätten.
Aber nun zurück zu den Tränken. In der folgenden Aufgabe sollen Sie sich mit der magischen Braukunst beschäftigen. Damit auch Sie vielleicht in den Genuss eines solchen magischen Belebungstranks kommen können.

Es gibt einen Kessel der auf wundersame Weise das Rezept, das er bekommt, alleine brauen kann.
Die magischen Zutaten, die für das jeweilige Rezept benötigt werden, haben eine besondere Eigenschaft:
Sie erhöhen den Füllstand des Kessels oft stärker als erwartet.
Zum Beispiel erhöht ein Schuss Wolfsmilch den Füllstand um 1/4.
Aus diesem Grund muss der Kessel seinen Füllstand selbst überwachen können, denn wenn er überläuft, ist der Trank hinüber.
##### Aufgabe 1
Implementieren Sie in Java die Klasse "MagicalPot" mit einem geeigneten Konstruktor, den Attributen aktuelles Volumen, maximales Volumen, einen Vorratsschrank und folgenden Zauberformeln:
* brew (volumeWater, recipe)
* isOverrun () : boolean

##### Aufgabe in Klartext
Implementieren Sie die Klasse "MagicalPot" mit einem geeigneten Konstruktor, den Attributen aktuelles Volumen, maximales Volumen und Vorratsschrank, sowie folgenden Methoden:
* brew (volumeWater, recipe)
* isOverrun () : boolean

Hierbei fügt die brew-Methode erst volumeWater zum aktuellen Füllstand hinzu, dann nach und nach die Zutaten aus recipe. Die Methode isOverrun gibt, wenn sie Aufgerufen wird, zurück ob das maximale Volumen überschritten wurde. Diese Überprüfung findet erst nach dem brauen statt.


###### Anmerkungen:
* Achten Sie auf die passenden Datentypen, sofern diese nicht schon angegeben sind.
* Das Rezept (recipe) kann vorerst eine Liste von beliebig vielen Zutaten sein.
* Als weitere Vereinfachung nehmen wir an, dass der MagicalPot eine Map als Vorratsschrank besitzt. Diese map bildet die Zutaten und ihre Auswirkungen auf den Füllstand als Key-Value-Paare ab.
* Die Auswirkung auf den Füllstand ist ein Faktor, keine konkrete Menge. So ist nach hinzufügen von "Krötenwurz" der neue Füllstand = alter Füllstand + Zweifaches des alten Füllstandes.


* Diese Map können Sie nutzen:

		private Map ingredients = new HashMap();
     	ingredients.put("Wolfsmilch", 0.25);
 		ingredients.put("Krötenwurz", 2);
 		ingredients.put("Sonnenkraut", 0.1);
 		ingredients.put("Sesampulver", 3);
 		ingredients.put("Schnarchessenz", 0.5);
 		ingredients.put("Hasenpfote", 0.7);

#### Aufgabe 2 (geschätzte Dauer: 45 Min. => 11 XP)

Wie Sie bereits wissen, verfügt unsere Thaumaturgische Hochschule der Magie über ein großes Netzwerk von Kooperationspartnern in vielen Bereichen der Industrie und Forschung.
Eben ein solches Marktforschungsunternehmen veröffentlichte erst kürzlich eine sehr aussagekräftige Umfrage bzgl. der Zufriedenheit mit den aktuell im Umlauf befindlichen Zauberkesseln und Wünschen für künftige Gerätegenerationen.
Sie kennen das ja selber: Alles wird immer schnelllebiger und da der Zauber mit der Fähigkeit an mehreren Orten Gleichzeitig zu sein noch nicht fehlerfrei praktiziert werden kann, schleichen sich beim Brauen oft Fehler ein.
Dies war auch das Hauptgesprächsthema bei unserem magischen Alumni-Treffen von vor zwei Wochen.
Ein Alumni, der zur Zeit in der magischen Entwicklungsabteilung eines namhaften Herstellers für Küchen- und Haushaltsgeräte tätig ist, möchte daher mit uns einen neuen Prototyp eines Zauberkessels entwickeln.

##### Aufgabe
Implementieren Sie in Java eine vollständig objektorierte Version des MagicalPot.
Zutat (Ingredient), Rezept (Recipe), Vorratslager (Stock) und ihre Eigenschaften sollen nun ebenfalls als Klassen dargestellt werden.
Zusätzlich soll die brew-Methode, nach erfolgreichem Brauen, ein Zaubertrank-Objekt (MagicPotion) zurückliefern.
Wenn der Zaubertrank misslingt, dann soll eine passende Exception geworfen werden.

##### Anmerkungen:
* Achten Sie bei den jeweiligen Eigenschaften und Methoden auf passende Zugriffsmodifikatoren (public/private), auf public sollte möglichst verzichtet werden.
* Implementieren Sie sinnvolle Getter- und Setter-Methoden
