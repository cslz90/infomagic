# Collections

## Abbildungen [1 Stunde]
1.[G] Das Gleichstellungsbüro für anderweitige Fabelwesen bietet Hilfe bei der Kommunikation mit den neu-eingeschriebenen Orks.
Dafür wurde folgendes Pergament mit der Übersetzung von Grunz-Lauten in lexikografische Zeichen bereitgelegt:

[K] Gegeben sei die folgende Zuordnung von Buchstaben zu Morse-Code Darstellung (`morse-code.txt`):
  ```
  "A" => ".-",
  "B" => "-...",
  "C" => "-.-.",
  "D" => "-..",
  "E" => ".",
  "F" => "..-.",
  "G" => "--.",
  "H" => "....",
  "I" => "..",
  "J" => ".---",
  "K" => "-.-",
  "L" => ".-..",
  "M" => "--",
  "N" => "-.",
  "O" => "---",
  "P" => ".--.",
  "Q" => "--.-",
  "R" => ".-.",
  "S" => "...",
  "T" => "-",
  "U" => "..-",
  "V" => "...-",
  "W" => ".--",
  "X" => "-..-",
  "Y" => "-.--",
  "Z" => "--..",
  "0" => "-----",
  "1" => ".----",
  "2" => "..---",
  "3" => "...--",
  "4" => "....-",
  "5" => ".....",
  "6" => "-....",
  "7" => "--...",
  "8" => "---..",
  "9" => "----.",
  "." => ".-.-.-",
  "," => "--..--",
  "-" => "-....-",
  " " => " "
  ```
[G]  Parsen Sie dieses Pergament und generieren Sie eine [Java Map](https://docs.oracle.com/javase/7/docs/api/java/util/Map.html), die jedes lexikografische Zeichen auf seine zugehörige Grunzfolge abbildet: `Character => String` - `Map<Character,String>()`.

[K] Parsen Sie diese Datei und generieren Sie eine [Java Map](https://docs.oracle.com/javase/7/docs/api/java/util/Map.html), die jeden Buchstaben auf seinen Morse-Code abbildet: `Character => String` - `Map<Character,String>()`.

  >Zum Parsing eignen sich reguläre Ausdrücke.
  Ein visueller Editor für reguläre Ausdrücke ist [hier](https://regex101.com/) zu finden.


2.[G] Da das ständige herumtragen des Pergaments recht unhandlich ist und wohl kein Ork die Geduld aufbringt jedes Wort erneut zu grunzen bis sie es verstanden haben, empfiehlt ihnen der Sachbearbeiter des Gleichstellungsbüros für anderweitige Fabelwesen einen Zauberspruch zu entwickeln, der ihr gesprochenes Wort in Grunzen übersetzt und umgekehrt.

[K] Schreiben Sie eine Funktion `encode` die mit Hilfe dieser Abbildung einen gegebenen String in seinen Morse-Code enkodiert.

  >Zwischen Gross- und Kleinschreibung wird nicht unterschieden.

3.[G] Ihr Sachbearbeiter klärt sie auf das Orks sehr viel wert auf das korrekte Grunzen eines Satzes legen.
Da es in der Vergangenheit öfter zu Zwischenfällen aufgrund eines grammatikalischen Fehlers kam, bittet ihr Sachbearbeiter sie nun “Hallo mein Name ist [Name].” mit Hilfe ihrer Formel einmal vor zu grunzen.

[K] Kodieren Sie den Satz "Hallo mein Name ist [Name]." in Morse-Code und geben Sie ihn aus.

4.[K] Die Schlüssel einer Abbildung sollten vergleichbar (comparable) sein? Wieso?
  Werfen Sie einen Blick in die Java API.
