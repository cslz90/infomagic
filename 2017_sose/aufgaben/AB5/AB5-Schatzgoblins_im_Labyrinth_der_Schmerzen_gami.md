## Schatzgoblins im Labyrinth der Schmerzen
*Leider kommt es des Öfteren vor, dass Goblins in die Schatzkammer der Thaumathurgischen Hochschule eindringen, und versuchen wertvolle Kleinodien zu stehlen. Die Schatzkammer wird dabei von einem Labyrinth geschützt, welches sich mehrmals am Tag ändert. Aber Goblins scheinen einen 7. Sinn für den korrekten Weg durch das Labyrinth zu besitzen und schaffen es immer wieder, in die Schatzkammer einzudringen.*

*Aus diesem Grund wurde ein Alarmsystem eingeführt, welches jeden Raum des Labyrinths automatisch nach 60s verriegelt, wenn ein Gegenstand unerlaubt und unangekündigt die Schatzkammer verlässt.*

*Diese Verriegelung ist sehr effektiv und hält die diebischen Goblins davon ab, mit dem Diebesgut zu entkommen. Leider ist das Labyrinth sehr groß und die Wächter, die losgeschickt werden, um dem Goblin zur Strafe den Hintern zu versohlen, sind relativ tumb im Geiste, weshalb diese bisher jeden einzelnen Raum des Labyrinths nach den Goblins abgesucht haben.*

*Da die Imps, welche das Labyrinth jede Woche neu bauen, faul sind, besteht dieses aus genormten immer gleich großen Räumen, bei denen immer nur die Türen neu gesetzt werden und auf Flure wird gleich ganz verzichtet. Bisherige Beobachtungen ergaben, dass die besten Goblins bis zu 60 Räume schafften, bevor die Verriegelung aktiv wurde. Es wurden aber auch schon Goblins gefangen, die panisch immer wieder durch die gleiche Tür rannten.*

*Ihre Aufgabe ist es, die armen Wächter bei der Suche nach den diebischen Goblins zu unterstützen und einen Zauberspruch `findeDenGoblin` in der Klasse `GoblinDresche` zu entwickeln, welcher die maximale Reichweite übernimmt und eine Liste mit den Raumnummern, die der Goblin erreichen könnte, zurückgibt.*

*Zum Testen Ihres Zaubers, wurden von den Imps kleinere Labyrinthe aufgebaut. Ein alter Goblin hilft Ihnen beim Testen, indem er versucht, durch diese Labyrinthe hindurchzukommen. Jeder Raum in diesen Testlabyrinthen hat eine eindeutige Nummer.*

*Für einen besseren Start bekommen Sie einige [Vorgaben](https://moodle.thm.de/mod/resource/view.php?id=167827).*

[Aufgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/80)
*Um den Sachverhalt zu verdeutlichen, hier ein Ausschnitt des Labyrinths und die Darstellung dieses als Graph:*

![Labyrinth_klein](https://homepages.thm.de/~cslz90/kurse/ad17/static/Labyrinth_klein.png)
