## Schatzgoblins im Labyrinth der Schmerzen
Sie bekommen von uns Labyrinthe in Form von Graphen vorgegeben. Ihre Aufgabe besteht darin eine Liste aller Räume auszugeben, welche maximal 60 Schritte vom Startknoten entfernt sind.

Überlegen Sie sich einen Algorithmus, welcher diese Aufgabe erfüllen kann und implementieren Sie diesen in der Methode `findeDenGoblin` in der Klasse `GoblinDresche`. Die Methode übernimmt das Labyrinth, die maximale Schrittweite und liefert eine Liste mit den Raumnummern zurück.

Für den besseren Einstieg erhalten Sie Java-Code als [Vorgaben](https://moodle.thm.de/mod/resource/view.php?id=167827).

Um den Sachverhalt zu verdeutlichen, hier ein Ausschnitt des Labyrinths und die Darstellung dieses als Graph:

![Labyrinth_klein](https://homepages.thm.de/~cslz90/kurse/ad17/static/Labyrinth_klein.png)
