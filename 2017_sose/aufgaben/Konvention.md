# Konventionen für die Ordnerstruktur

### Aufgabenstellungen

Aufgabenstellungen landen in dem jeweiligen Unterordner für das Aufgabenblatt und dort
im jeweiligen Unterordner für die Aufgabe.

Aufgabenstellungen gibt es in zwei Versionen:
- Gamifizierte Aufgabe (Suffix "-gami")
- Klartext Aufgabe (Suffix "-klar")

Die Datei- und Ordnernamen enthalten immer das Aufgabenblatt als Prefix.

#### Beispiel

Aufgabe "Super Tolle Aufgabe" für Aufgabenblatt 99. Die Dateipfade sehen wie folgt aus:

Gamifizierte Version: "aufgaben/AB99/AB99-SuperTolleAufgabe/AB99-SuperTolleAufgabe-gami.md"
Klartext Version: "aufgaben/AB99/AB99-SuperTolleAufgabe/AB99-SuperTolleAufgabe-klar.md"

### Code

Programmcode gehört in den Unterordner "aufgaben/code". Dort werden allerdings keine Unterordner
für die Aufgabenblätter erzeugt. Die einzelnen Code-Projekte werden flach dort eingepflegt.
Dies erleichtert das Bauen mit Gradle.

#### Beispiel

Programmcode für die Aufgabe "Super Tolle Aufgabe" aus dem Aufgabenblatt 99. Der Ordnerpfad sieht wie
folgt aus: "aufgaben/code/AB99-SuperTolleAufgabe/".
