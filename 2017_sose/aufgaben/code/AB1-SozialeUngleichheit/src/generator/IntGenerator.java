package generator;

import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public final class IntGenerator {
	private static final Random random = new Random();

	private IntGenerator() {}

	public static void main(final String[] arguments) {
		IntStream.range(0, 4000)
			.map(i -> random.nextInt(Integer.MAX_VALUE))
			.mapToObj(Integer::toString)
			.flatMap(s -> Stream.of(",", s))
			.skip(1).forEach(System.out::print);
		System.out.println();
	}

}
