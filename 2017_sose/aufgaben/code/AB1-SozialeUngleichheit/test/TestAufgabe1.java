package test;

import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public final class TestAufgabe1 {

	private TestAufgabe1() {}

	private static String matchArgument(final String[] arguments, final String prefix) {
		return Arrays.stream(arguments)
			.filter(argument -> argument.startsWith(prefix + "="))
			.map(argument -> argument.substring(prefix.length() + 1))
			.findFirst().orElseThrow(() -> new RuntimeException());
	}

	private static int[] fileToArray(final String path) {
		try {
			final String content = new String(Files.readAllBytes(Paths.get(path)), StandardCharsets.UTF_8);
			final int[] result = Arrays.stream(content.split(","))
				.map(s -> s.trim())
				.filter(s -> s.matches("[0-9]+"))
				.mapToInt(Integer::parseInt)
				.toArray();

			if (result.length != 4000) {
				throw new IllegalArgumentException(String.format("Ungültiges Dateiformat ('%s')", path));
			}

			return result;
		} catch (final Throwable t) {
			throw new IllegalArgumentException(String.format("Ungültiges Dateiformat ('%s')", path));
		}
	}

	private static Map<String, Integer> loadFromProperties(final Properties properties, final String ... keys) {
		final Map<String, Integer> result = new HashMap<>();
		for (final String key: keys) {
			if (!properties.containsKey(key)) {
				throw new IllegalArgumentException(String.format("Fehlender Wert für '%s' (Bitte eine positive Dezimalzahl angeben)", key));
			}
			final String value = properties.getProperty(key);
			if (value == null || !value.matches("[0-9]+")) {
				throw new IllegalArgumentException(String.format("'%s' ist ein ungültiger Wert für '%s' (Bitte eine positive Dezimalzahl angeben)", value, key));
			}
			result.put(key, Integer.parseInt(value));
		}
		return result;
	}

	private static Map<String, Integer> parseStudentOutput(final String studentOutputFile) throws Throwable {
		Properties properties = new Properties();
		properties.load(new FileInputStream(studentOutputFile));
		return loadFromProperties(properties, "min", "max");
	}

	private static Map<String, Integer> computeTestOutput(final String inputFile) {
		final int[] input = fileToArray(inputFile);
		final Map<String, Integer> result = new HashMap<>();

		final int[] minmaxResult = minmax(input);
		result.put("min", minmaxResult[0]);
		result.put("max", minmaxResult[1]);

		return result;
	}

	public static void main(final String[] arguments) {
		try {
			if (arguments.length == 0) {
				System.exit(0);
			}

			final String inputFile = matchArgument(arguments, "input");
			final String studentOutputFile = matchArgument(arguments, "student-output");

			final Map<String, Integer> input = computeTestOutput(inputFile);
			final Map<String, Integer> output = parseStudentOutput(studentOutputFile);

			input.forEach((key, value) -> {
				if (!output.containsKey(key)) {
					throw new IllegalArgumentException(String.format("Das Ergebnis für '%s' fehlt", key));
				}
				if (!output.get(key).equals(value)) {
					throw new IllegalArgumentException(String.format("Das eingegebene Ergebnis für '%s' ist falsch!", key));
				}
			});

		} catch (final Throwable t) {
			if (t.getMessage() != null && t.getMessage().trim().length() > 0) {
				System.out.println(t.getMessage().trim());
			}
			System.exit(0);
		}
	}

	private static int[] minmax(final int[] array) {
		int min = array[0];
		int max = array[0];
		for (int i = 1; i < array.length; i++) {
			if (array[i] < min) {
				min = array[i];
			}
			if (array[i] > max) {
				max = array[i];
			}
		}
		return new int[] { min, max };
	}

}
