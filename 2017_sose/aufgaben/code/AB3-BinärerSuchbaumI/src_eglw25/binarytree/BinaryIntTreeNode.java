package binarytree;

public class BinaryIntTreeNode extends ABinaryIntTreeNode {

    private int value;

    private ABinaryIntTreeNode right;

    private ABinaryIntTreeNode left;

    public BinaryIntTreeNode(int value) {
        this.value = value;
    }

    @Override
    public int value() {
        return value;
    }

    @Override
    public ABinaryIntTreeNode left() {
        return left;
    }

    @Override
    public ABinaryIntTreeNode right() {
        return right;
    }

    @Override
    public void add(int value) {
        if (value > this.value) {
            right = addInternal(value, right);
        } else if (value < this.value) {
            left = addInternal(value, left);
        }
    }

    private ABinaryIntTreeNode addInternal(int value, ABinaryIntTreeNode node) {
        if (node == null) {
            node = new BinaryIntTreeNode(value);
        } else {
            node.add(value);
        }
        return node;
    }

    @Override
    public void remove(ABinaryIntTreeNode parent, int value) {
        if (value > this.value) {
            if (right != null) {
                right.remove(this, value);
            }
        } else if (value < this.value) {
            if (left != null) {
                left.remove(this, value);
            }
        } else {
            // value == this.value
            if (right == null && left == null) {
                if (parent != null) {
                    if (parent.left() == this) {
                        parent.setLeft(null);
                    } else {
                        parent.setRight(null);
                    }
                }
            } else if (right == null) {
                assignNode(left);
            } else if (left == null) {
                assignNode(right);
            } else {
                // both children are not null
                int rightMinValue = right.findMin().value();
                right.remove(this, rightMinValue);
                this.value = rightMinValue;
            }
        }
    }

    private void assignNode(ABinaryIntTreeNode node) {
        value = node.value();
        right = node.right();
        left = node.left();
    }

    @Override
    public ABinaryIntTreeNode findMin() {
        if (left == null) {
            return this;
        }
        return left.findMin();
    }

    @Override
    public boolean contains(int value) {
        ABinaryIntTreeNode next;
        if (value > this.value) {
            next = right;
        } else if (value < this.value) {
            next = left;
        } else {
            return true;
        }
        return (next != null) ? next.contains(value) : false;
    }

    @Override
    public int size() {
        int size = 1;
        if (left != null) {
            size += left.size();
        }
        if (right != null) {
            size += right.size();
        }
        return size;
    }

    @Override
    public void setLeft(ABinaryIntTreeNode left) {
        this.left = left;
    }

    @Override
    public void setRight(ABinaryIntTreeNode right) {
        this.right = right;
    }
}
