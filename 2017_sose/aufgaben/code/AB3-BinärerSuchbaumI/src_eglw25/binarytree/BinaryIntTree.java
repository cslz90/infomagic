package binarytree;

public class BinaryIntTree extends ABinaryIntTree {

    private ABinaryIntTreeNode root;

    @Override
    public ABinaryIntTreeNode root() {
        return root;
    }

    @Override
    public void add(int value) {
        if (isEmpty()) {
            root = new BinaryIntTreeNode(value);
        } else {
            root.add(value);
        }
    }

    @Override
    public void remove(int value) {
        if (isEmpty()) {
            return;
        }
        if (root.value() == value && size() == 1) {
            root = null;
        } else {
            root.remove(null, value);
        }
    }

    @Override
    public boolean contains(int value) {
        if (isEmpty()) {
            return false;
        }
        return root.contains(value);
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public int size() {
        if (isEmpty()) {
            return 0;
        }
        return root.size();
    }
}
