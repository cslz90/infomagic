package binarytree;

public abstract class ABinaryIntTreeNode {

    /**
     * @return the value stored in this node.
     */
    public abstract int value();

    /**
     * @return the left subtree of this node.
     */
    public abstract ABinaryIntTreeNode left();

    /**
     * @return the right subtree of this node.
     */
    public abstract ABinaryIntTreeNode right();

    public abstract void setLeft(ABinaryIntTreeNode left);

    public abstract void setRight(ABinaryIntTreeNode right);

    public abstract void add(int value);

    public abstract void remove(ABinaryIntTreeNode parent, int value);

    public abstract boolean contains(int value);

    public abstract ABinaryIntTreeNode findMin();

    public abstract int size();

}