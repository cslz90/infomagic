package generator;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public final class Generator {
	/* If one of the students cracks this encryption, I'll buy him a pizza. */
	private static final String password = "5bIhZKZwkTh1lUMyOe2o8JfkDr7apk83"; /* top secret, as you can see :-) */
	private static final String salt = "The set of all subsets of the set of all problems of AuD is uncountable."; /* best salt ever */

	private Generator() {}

	public static void main(final String[] arguments) throws Throwable {
		final SecureRandom random = new SecureRandom();
		final String mySecretSeed = encode("" + random.nextLong());
		System.out.println(mySecretSeed);
	}

	public static final String encode(final String message) throws Throwable {
		final Key key = keyOf(password, salt);
		final Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		final byte[] encrypted = cipher.doFinal(message.getBytes(StandardCharsets.UTF_8));
		final BASE64Encoder encoder = new BASE64Encoder();
		return encoder.encode(encrypted);
	}

	public static final String decode(final String message) throws Throwable {
		final Key key = keyOf(password, salt);
		final BASE64Decoder decoder = new BASE64Decoder();
		final Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, key);
		final byte[] decrypted = cipher.doFinal(decoder.decodeBuffer(message));
		return new String(decrypted, StandardCharsets.UTF_8);
	}

	private static final Key keyOf(final String password, final String salt) throws Throwable {
		final SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
		final char[] pwBytes = password.toCharArray();
		final byte[] saltBytes = salt.getBytes(StandardCharsets.UTF_8);
		final KeySpec spec = new PBEKeySpec(pwBytes, saltBytes, 65536, 128);
		return new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
	}

}
