package checker;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Random;

import generator.Generator;

public final class Checker {

	private Checker() {}

	private static String matchArgument(final String[] arguments, final String prefix) {
		return Arrays.stream(arguments)
			.filter(argument -> argument.startsWith(prefix + "="))
			.map(argument -> argument.substring(prefix.length() + 1))
			.findFirst().orElseThrow(() -> new RuntimeException());
	}

	private static int parseStudentOutput(final String file) throws Throwable {
		final String content = new String(Files.readAllBytes(Paths.get(file)), StandardCharsets.UTF_8).trim();
		if (content.matches("[0-9]+")) {
			return Integer.parseInt(content);
		} else {
			throw new IllegalArgumentException("Bitte geben Sie eine Zahl zwischen 0 (inklusive) und 1024 (exklusive) an.");
		}
	}

	private static String parseInputFile(final String file) throws Throwable {
		final String content = new String(Files.readAllBytes(Paths.get(file)), StandardCharsets.UTF_8).trim();
		/* http://stackoverflow.com/a/5885097 */
		if (content.matches("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$")) {
			return content;
		} else {
			throw new IllegalArgumentException("Es ist ein interner Fehler aufgetreten.");
		}
	}

	public static void main(final String[] arguments) {
		try {
			if (arguments.length == 0) {
				System.exit(0);
			}

			final String inputFile = matchArgument(arguments, "input");
			final String studentOutputFile = matchArgument(arguments, "student-output");

			final String key = parseInputFile(inputFile);
			final int output = parseStudentOutput(studentOutputFile);

			final long seed = Long.parseLong(Generator.decode(key));

			final Random rnd = new Random(seed);
			final int number = rnd.nextInt(1024);

			if (number > output) {
				System.out.println("Leider ist Ihre Zahl falsch. Das von mir ausgedachte Passwort ist größer.");
				Thread.sleep(10000);
			} else if (number < output) {
				System.out.println("Leider ist Ihre Zahl falsch. Das von mir ausgedachte Passwort ist kleiner.");
				Thread.sleep(10000);
			}


		} catch (final Throwable t) {
			t.printStackTrace();
			if (t.getMessage() != null && t.getMessage().trim().length() > 0) {
				System.out.println(t.getMessage().trim());
			}
			System.exit(0);
		}
	}

}
