import java.util.*;

/**
 * Created by thomas on 07.04.17.
 */

public class MagicalPot_Aufgabe1 {
    private float currVolume, maxVolume;
    private Map ingredients;

    private void generateStock() {
        ingredients = new HashMap<String, Float>();
        ingredients.put("Wolfsmilch", 0.25f);
        ingredients.put("Krötenwurz", 2.0f);
        ingredients.put("Sonnenkraut", 0.1f);
        ingredients.put("Sesampulver", 3.0f);
        ingredients.put("Schnarchessenz", 0.5f);
        ingredients.put("Hasenpfote", 0.7f);
    }

    public MagicalPot_Aufgabe1 (float maxVolume){
        currVolume = 0;
        this.maxVolume = maxVolume;
        generateStock();
    }

    public void brew (int volumeWater, List<String> recipe){
        this.currVolume += volumeWater;
        float multiplicator = 1;
        for (String s : recipe) {
            multiplicator += ((float)ingredients.get(s));
        }
        currVolume *= multiplicator;
    }

    public boolean isOverrun (){
        return (this.currVolume>this.maxVolume);
    }
}


class Main {                                    //Zum Testen
    public static void main(String[] args) {

        MagicalPot_Aufgabe1 testPot = new MagicalPot_Aufgabe1(100);

        List testListe = new LinkedList<String>();
        testListe.add("Krötenwurz");
        testListe.add("Sesampulver");

        testPot.brew(20,testListe);
        System.out.println("Übergelaufen? "+testPot.isOverrun());
    }
}