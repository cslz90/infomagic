import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomas on 08.04.17.
 */
public class MagicalPot_Aufgabe2 {
    private float currVolume, maxVolume;
    private Stock stockOfIngredients;

    public MagicalPot_Aufgabe2(Stock stock, float maxVolume){
        this.currVolume = 0.0f;
        this.maxVolume = maxVolume;
        stockOfIngredients = stock;
    }

    public boolean isOverrun(){
        return (this.currVolume>this.maxVolume);
    }

    public MagicPotion brew (Recipe recipe, float water){
        this.currVolume += water;
        float multiplicator = 1.0f;
        MagicPotion result = new MagicPotion();

        List<String> listOfRecipeStrings = recipe.get();
        List<Ingredient> listOfStockIngredients = stockOfIngredients.get();
        for (String s:listOfRecipeStrings) {
            for (Ingredient i:listOfStockIngredients) {
                if (i.getName().equals(s)){
                    multiplicator += i.getGrowth();
                    result.add(i);
                }
            }
        }
        this.currVolume *= multiplicator;

        if (isOverrun()){
			throw new MagicalPotOverflowException();
        }

        else {
            return result;
        }
    }

}

class Stock{
    private List<Ingredient> listOfIngredients;

    public Stock(Ingredient ingredient){
        listOfIngredients = new ArrayList<>();
        listOfIngredients.add(ingredient);
    }

    public void add(Ingredient ingredient){
        listOfIngredients.add(ingredient);
    }

    public List get(){
        return listOfIngredients;
    }
}

class Ingredient{
    private String name;
    private float growth;

    public Ingredient(String nameOfIngredient, float growth){
        this.name = nameOfIngredient;
        this.growth = growth;
    }

    public String getName(){
        return this.name;
    }

    public float getGrowth(){
        return this.growth;
    }
}

class Recipe{
    private List<String> listOfIngredients;

    public Recipe(String nameOfIngredient){
        listOfIngredients = new ArrayList<>();
        listOfIngredients.add(nameOfIngredient);
    }

    public void add(String nameOfIngredient){
        listOfIngredients.add(nameOfIngredient);
    }

    public List get(){
        return listOfIngredients;
    }
}

class MagicPotion{
    private List<Ingredient> listOfBrewedIngredients;

    public MagicPotion(){
        listOfBrewedIngredients = new ArrayList<>();
    }

    public void add(Ingredient brewedIngredient){
        listOfBrewedIngredients.add(brewedIngredient);
    }

    public List get(){
        return listOfBrewedIngredients;
    }
}

class MagicalPotOverflowException extends RuntimeException{
}

class Test{
    public static void main(String[] args) {

        Ingredient wolfsmilch = new Ingredient("Wolfsmilch", 0.25f);
        Ingredient kroetenwurz = new Ingredient("Krötenwurz", 2.0f);
        Ingredient sonnenkraut = new Ingredient("Sonnenkraut", 0.1f);
        Ingredient sesampulver = new Ingredient("Sesampulver", 3.0f);
        Ingredient schnarchessenz = new Ingredient("Schnarchessenz", 0.5f);
        Ingredient hasenpfote = new Ingredient("Hasenpfote", 0.7f);

        Stock testStock = new Stock(wolfsmilch);
        testStock.add(kroetenwurz);
        testStock.add(sonnenkraut);
        testStock.add(sesampulver);
        testStock.add(schnarchessenz);
        testStock.add(hasenpfote);

        Recipe testRecipe = new Recipe("Sesampulver");
        testRecipe.add("Krötenwurz");

        MagicalPot_Aufgabe2 testPot = new MagicalPot_Aufgabe2(testStock, 100.0f);

        testPot.brew(testRecipe, 20.0f);
    }
}