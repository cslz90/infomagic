
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class GierigeZwergeTest {

    @Test
    public void noCoinsTest() {
        assertEquals(Arrays.asList(), GierigeZwerge.lassDenZwergWeinen(0));
    }

    @Test
    public void singleCoinsTest() {
        final int[] coinValues = new int[] { 50, 20, 10, 5, 2, 1 };

        for (final int coin : coinValues) {
            assertEquals(Arrays.asList(coin), GierigeZwerge.lassDenZwergWeinen(coin));
        }
    }

    @Test
    public void coins99Test() {
        assertEquals(Arrays.asList(50, 20, 20, 5, 2, 2), GierigeZwerge.lassDenZwergWeinen(99));
    }

    @Test
    public void coins3Test() {
        assertEquals(Arrays.asList(2, 1), GierigeZwerge.lassDenZwergWeinen(3));
    }

    @Test
    public void coins7Test() {
        assertEquals(Arrays.asList(5, 2), GierigeZwerge.lassDenZwergWeinen(7));
    }

    @Test
    public void coins11Test() {
        assertEquals(Arrays.asList(10, 1), GierigeZwerge.lassDenZwergWeinen(11));
    }

    @Test
    public void coins13Test() {
        assertEquals(Arrays.asList(10, 2, 1), GierigeZwerge.lassDenZwergWeinen(13));
    }

    @Test
    public void coins653Test() {
        assertEquals(Arrays.asList(50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 2, 1),
                GierigeZwerge.lassDenZwergWeinen(653));
    }

    @Test
    public void coins999Test() {
        assertEquals(Arrays.asList(50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50,
            50, 50, 50, 50, 50, 50, 20, 20, 5, 2, 2), GierigeZwerge.lassDenZwergWeinen(999));
    }

}
