package cycles;

import static org.junit.Assert.*;

import java.util.stream.IntStream;

import org.junit.Test;

public final class GraphTest {

	/* ***** cycles ***** */
	
	@Test
	public void test_cycle_1() {		
		final AbstractGraph<String> graph = new Graph<>();
		graph.addVertex("hello world");
		graph.connect("hello world", "hello world");
		assertTrue(graph.hasCycles());
	}
	
	@Test
	public void test_cycle_2() {		
		final AbstractGraph<Integer> graph = new Graph<>();
		IntStream.range(0, 100).forEach(graph::addVertex);
		IntStream.range(1, 100).forEach(i -> graph.connect(i - 1, i));
		graph.connect(99, 0);
		assertTrue(graph.hasCycles());
	}
	
	@Test
	public void test_cycle_3() {		
		final AbstractGraph<String> graph = new Graph<>();
		graph.addVertex("a");
		graph.addVertex("b");
		graph.addVertex("c");
		graph.connect("a", "b");
		graph.connect("b", "c");
		graph.connect("c", "a");
		assertTrue(graph.hasCycles());
	}
	
	@Test
	public void test_cycle_4() {		
		final AbstractGraph<String> graph = new Graph<>();
		IntStream.range(0, 26).forEach(c -> graph.addVertex("" + (char) (c + 'a')));
		IntStream.range(1, 26).forEach(c -> graph.connect("" + (char) ((c - 1) + 'a'), "" + (char) (c + 'a')));
		graph.connect("o", "w");
		graph.connect("o", "q");
		graph.connect("o", "o");
		assertTrue(graph.hasCycles());
	}
	
	/* ***** no cycles ***** */
	
	@Test
	public void test_no_cycle_1() {		
		final AbstractGraph<String> graph = new Graph<>();
		graph.addVertex("hello world");
		graph.addVertex("world hello");
		graph.connect("hello world", "world hello");
		assertFalse(graph.hasCycles());
	}
	
	@Test
	public void test_no_cycle_2() {		
		final AbstractGraph<Integer> graph = new Graph<>();
		IntStream.range(0, 100).forEach(graph::addVertex);
		IntStream.range(1, 100).forEach(i -> graph.connect(i - 1, i));
		assertFalse(graph.hasCycles());
	}
	
	@Test
	public void test_no_cycle_3() {		
		final AbstractGraph<Integer> graph = new Graph<>();
		graph.addVertex(0);
		assertFalse(graph.hasCycles());
	}
	
	@Test
	public void test_no_cycle_4() {		
		final AbstractGraph<Integer> graph = new Graph<>();
		assertFalse(graph.hasCycles());
	}
	
}
