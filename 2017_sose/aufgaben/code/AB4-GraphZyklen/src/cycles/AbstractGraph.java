package cycles;

import java.util.*;

/**
 * Simple undirected graph implementation
 * @param <V> The type of a vertex of this graph. The underlying class should implement 
 * the methods {@link Object#equals(Object)}, {@link Object#hashCode()} and {@link Object#toString()}.
 */
public abstract class AbstractGraph<V> {
	private final Set<V> vertices = new HashSet<>();
	private final Set<V> immutableView = Collections.unmodifiableSet(vertices);
	private final Map<V, Set<V>> edges = new HashMap<>();
	
	public AbstractGraph() {}
	
	/**
	 * Connects the two given vertices. Afterwards the following property holds:<br>
	 * {@code isConnected(a, b) == true && isConnected(b, a) == true}
	 * @param a The first vertex
	 * @param b The second vertex
	 */
	public final void connect(final V a, final V b) {
		if (!vertices.contains(a)) {
			throw new IllegalArgumentException(String.format("can not connect vertex '%s' with vertex '%s', '%s' is not a vertex of the graph!", a, b, a));
		}
		if (!vertices.contains(b)) {
			throw new IllegalArgumentException(String.format("can not connect vertex '%s' with vertex '%s', '%s' is not a vertex of the graph!", a, b, b));
		}
		edges.get(a).add(b);
		edges.get(b).add(a);
	}
	
	/**
	 * Adds the provided vertex to this graph
	 * @param vertex The vertex to add
	 */
	public final void addVertex(final V vertex) {
		vertices.add(vertex);
		edges.put(vertex, new HashSet<>());
	}
	
	/**
	 * @return All vertices of this graph
	 */
	public final Set<V> getVertices() {
		return immutableView;
	}
	
	/**
	 * Checks if this graph contains a cycle
	 * @return Whether or not this graph contains a cycle
	 */
	public abstract boolean hasCycles();

	public final Set<V> getAdjacent(final V vertex) {
		return Collections.unmodifiableSet(edges.get(vertex));
	}
	
}
