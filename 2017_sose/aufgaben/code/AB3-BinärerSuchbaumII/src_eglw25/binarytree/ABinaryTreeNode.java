package binarytree;

import java.util.Comparator;

public abstract class ABinaryTreeNode<T> {

    protected final Comparator<T> comparator;

    public ABinaryTreeNode(Comparator<T> comparator) {
        this.comparator = comparator;
    }

    /**
     * @return the value stored in this node.
     */
    public abstract T value();

    /**
     * @return the left subtree of this node.
     */
    public abstract ABinaryTreeNode<T> left();

    /**
     * @return the right subtree of this node.
     */
    public abstract ABinaryTreeNode<T> right();

    public abstract void setLeft(ABinaryTreeNode<T> left);

    public abstract void setRight(ABinaryTreeNode<T> right);

    public abstract void add(T value);

    public abstract void remove(ABinaryTreeNode<T> parent, T value);

    public abstract boolean contains(T value);

    public abstract ABinaryTreeNode<T> findMin();

    public abstract int size();

}