package binarytree;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by vertaljon on 14.06.17.
 */
public class BFIterator<E> implements Iterator<E> {

    private Queue<ABinaryTreeNode<E>> queue;

    public BFIterator(ABinaryTreeNode<E> root) {
        queue = new LinkedList<>();
        addIfNotNull(root);
    }

    @Override
    public boolean hasNext() {
        return !queue.isEmpty();
    }

    @Override
    public E next() {
        ABinaryTreeNode<E> next = queue.remove();
        addIfNotNull(next.left());
        addIfNotNull(next.right());
        return next.value();
    }

    private void addIfNotNull(ABinaryTreeNode<E> node) {
        if (node != null) {
            queue.add(node);
        }
    }

}
