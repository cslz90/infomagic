package binarytree;

import java.util.Comparator;
import java.util.Iterator;

public class BinaryTree<T> extends ABinaryTree<T> {

    private ABinaryTreeNode<T> root;

    public BinaryTree(Comparator<T> comparator) {
        super(comparator);
    }

    @Override
    public ABinaryTreeNode<T> root() {
        return root;
    }

    @Override
    public void add(T value) {
        if (isEmpty()) {
            root = new BinaryTreeNode(value, comparator);
        } else {
            root.add(value);
        }
    }

    @Override
    public void remove(T value) {
        if (isEmpty()) {
            return;
        }
        if (root.value() == value && size() == 1) {
            root = null;
        } else {
            root.remove(null, value);
        }
    }

    @Override
    public boolean contains(T value) {
        if (isEmpty()) {
            return false;
        }
        return root.contains(value);
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public int size() {
        if (isEmpty()) {
            return 0;
        }
        return root.size();
    }

    @Override
    public Iterator<T> breadthFirstIterator() {
        return new BFIterator<>(root);
    }

    @Override
    public Iterator<T> depthFirstIterator() {
        return new DFIterator<>(root);
    }
}
