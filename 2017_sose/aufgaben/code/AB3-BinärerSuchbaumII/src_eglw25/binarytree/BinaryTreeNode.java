package binarytree;

import java.util.Comparator;

public class BinaryTreeNode<T> extends ABinaryTreeNode<T> {

    private T value;

    private ABinaryTreeNode<T> right;

    private ABinaryTreeNode<T> left;

    public BinaryTreeNode(T value, Comparator<T> comparator) {
        super(comparator);
        this.value = value;
    }

    @Override
    public T value() {
        return value;
    }

    @Override
    public ABinaryTreeNode<T> left() {
        return left;
    }

    @Override
    public ABinaryTreeNode<T> right() {
        return right;
    }

    @Override
    public void add(T value) {
        if (comparator.compare(value, this.value) > 0) {
            right = addInternal(value, right);
        } else if (comparator.compare(value, this.value) < 0) {
            left = addInternal(value, left);
        }
    }

    private ABinaryTreeNode<T> addInternal(T value, ABinaryTreeNode<T> node) {
        if (node == null) {
            node = new BinaryTreeNode(value, comparator);
        } else {
            node.add(value);
        }
        return node;
    }

    @Override
    public void remove(ABinaryTreeNode<T> parent, T value) {
        if (comparator.compare(value, this.value) > 0) {
            if (right != null) {
                right.remove(this, value);
            }
        } else if (comparator.compare(value, this.value) < 0) {
            if (left != null) {
                left.remove(this, value);
            }
        } else {
            // value == this.value
            if (right == null && left == null) {
                if (parent != null) {
                    if (parent.left() == this) {
                        parent.setLeft(null);
                    } else {
                        parent.setRight(null);
                    }
                }
            } else if (right == null) {
                assignNode(left);
            } else if (left == null) {
                assignNode(right);
            } else {
                // both children are not null
                T rightMinValue = right.findMin().value();
                right.remove(this, rightMinValue);
                this.value = rightMinValue;
            }
        }
    }

    private void assignNode(ABinaryTreeNode<T> node) {
        value = node.value();
        right = node.right();
        left = node.left();
    }

    @Override
    public ABinaryTreeNode<T> findMin() {
        if (left == null) {
            return this;
        }
        return left.findMin();
    }

    @Override
    public boolean contains(T value) {
        ABinaryTreeNode<T> next;
        if (comparator.compare(value, this.value) > 0) {
            next = right;
        } else if (comparator.compare(value, this.value) < 0) {
            next = left;
        } else {
            return true;
        }
        return (next != null) ? next.contains(value) : false;
    }

    @Override
    public int size() {
        int size = 1;
        if (left != null) {
            size += left.size();
        }
        if (right != null) {
            size += right.size();
        }
        return size;
    }

    @Override
    public void setLeft(ABinaryTreeNode<T> left) {
        this.left = left;
    }

    @Override
    public void setRight(ABinaryTreeNode<T> right) {
        this.right = right;
    }
}
