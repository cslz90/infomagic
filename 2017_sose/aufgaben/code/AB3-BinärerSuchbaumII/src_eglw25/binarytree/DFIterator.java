package binarytree;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by vertaljon on 14.06.17.
 */
public class DFIterator<E> implements Iterator<E> {

    private Deque<ABinaryTreeNode<E>> stack;

    public DFIterator(ABinaryTreeNode<E> root) {
        stack = new LinkedList<>();
        addLeftChildren(root);
    }

    @Override
    public boolean hasNext() {
        return !stack.isEmpty();
    }

    @Override
    public E next() {
        ABinaryTreeNode<E> node = stack.pop();
        addLeftChildren(node.right());
        return node.value();
    }

    private void addLeftChildren(ABinaryTreeNode<E> node) {
        while (node != null) {
            stack.push(node);
            node = node.left();
        }
    }

}
