package labyrinth;

import java.util.List;

/**
 * Abstract class for any kind of labyrinths
 */
public abstract class Labyrinth {
	protected final LabyrinthTree tree;

	/**
	 * Create a new random labyrinth
	 */
	public Labyrinth() {
		tree = LabyrinthTreeFactory.factory()
			.maxBranches(4)
			.maxDepth(10)
			.minDepth(2)
			.crossroadProbability(1f)
			.build();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.tree.show();
	}

	/**
	 * Searches for the {@link LabyrinthTree.Exit} using a backtracking algorithm
	 * @return The list of tree nodes from the root node to the exit node (i.e. the path to the exit)
	 */
	public abstract List<LabyrinthTree> searchForExit();

}
