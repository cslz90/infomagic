package labyrinth;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Factory for random labyrinths
 */
public final class LabyrinthTreeFactory {
	private long seed = new Random().nextLong();
	private float crossroadProbability = 0.75f;
	private int maxBranches = 4;
	private int minDepth = 2;
	private int maxDepth = 10;

	private LabyrinthTreeFactory() {}

	/**
	 * Obtain a new factory for random labyrinths
	 * @return The factory
	 */
	public static final LabyrinthTreeFactory factory() {
		return new LabyrinthTreeFactory();
	}

	/**
	 * Sets the seed for the random generator
	 * @param seed The seed
	 * @return This factory
	 */
	public final LabyrinthTreeFactory seed(final long seed) {
		this.seed = seed;
		return this;
	}

	/**
	 * The maximal number of branches of a cross road
	 * @param maxBranches The maximal number of branches
	 * @return This factory
	 * @throws IllegalArgumentException If the maximal number of branches is less than 2
	 */
	public final LabyrinthTreeFactory maxBranches(final int maxBranches) {
		if (maxBranches < 2) {
			throw new IllegalArgumentException("branches < 2");
		}
		this.maxBranches = maxBranches;
		return this;
	}

	/**
	 * The minimal depth of the generated tree
	 * @param minDepth The minimal depth
	 * @return This factory
	 * @throws IllegalArgumentException If the minimal depth is greater than the maximal depth or smaller than 1
	 */
	public final LabyrinthTreeFactory minDepth(final int minDepth) {
		if (minDepth > maxDepth) {
			throw new IllegalArgumentException("minDepth > maxDepth");
		} else if (minDepth < 1) {
			throw new IllegalArgumentException("minDepth < 1");
		}
		this.minDepth = minDepth;
		return this;
	}

	/**
	 * The maximal depth of the generated tree
	 * @param maxDepth The maximal depth
	 * @return This factory
	 * @throws IllegalArgumentException If the maximal depth is less than the minimal depth
	 */
	public final LabyrinthTreeFactory maxDepth(final int maxDepth) {
		if (maxDepth < minDepth) {
			throw new IllegalArgumentException("maxDepth < minDepth");
		}
		this.maxDepth = maxDepth;
		return this;
	}

	/**
	 * Sets the initial probability of creating a new crossroad (this probability converges to 0)
	 * @param crossroadProbability The probability of creating a new crossroad
	 * @return This factory
	 * @throws IllegalArgumentException If the probability is not between 0 and 1
	 */
	public final LabyrinthTreeFactory crossroadProbability(final float crossroadProbability) {
		this.crossroadProbability = crossroadProbability;
		return this;
	}

	/**
	 * Builds a new random labyrinth based on the factory settings
	 * @return A new random generated labyrinth
	 */
	public final LabyrinthTree build() {
		return generate(new Random(seed), new LabyrinthTree.Exit(), this.crossroadProbability, 1);
	}

	private final LabyrinthTree generate(final Random random, final LabyrinthTree destination, final float probability, final int depth) {
		if (depth >= maxDepth || (depth >= minDepth && random.nextFloat() > probability)) {
			return destination;
		}

		final List<LabyrinthTree> children = new LinkedList<>();
		final LabyrinthTree crossroad = new LabyrinthTree.Crossroad(children);

		final int branches = random.nextInt(maxBranches - 1) + 2;
		final int dstIndex = random.nextInt(branches);
		IntStream.range(0, branches).<LabyrinthTree>mapToObj(i -> {
			final LabyrinthTree newDst = i == dstIndex ? destination : new LabyrinthTree.DeadEnd();
			return generate(random, newDst, Math.max(0f, probability - (probability / ((maxDepth + 1) - minDepth))), depth + 1);
		}).forEach(children::add);

		return crossroad;
	}

}
