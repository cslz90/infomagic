package labyrinth;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Common supertype of all kinds of labyrinth parts
 */
public interface LabyrinthTree {

	/**
	 * @return A list containing all children of this tree node (possibly empty)
	 */
	public List<LabyrinthTree> children();

	/**
	 * @return Whether or not this tree node is a crossroad
	 */
	public boolean isCrossroad();

	/**
	 * @return Whether or not this tree node is an exit
	 */
	public boolean isExit();

	/**
	 * @return Whether or not this tree node is a dead end
	 */
	public boolean isDeadEnd();

	/**
	 * Unsafe cast operation, succeeds when {@link LabyrinthTree#isCrossroad} returns true
	 * @return This tree node casted to {@link LabyrinthTree.Crossroad}
	 * @throws ClassCastException If this tree node is not of type {@link LabyrinthTree.Crossroad}
	 */
	public Crossroad asCrossroad() throws ClassCastException;

	/**
	 * Unsafe cast operation, succeeds when {@link LabyrinthTree#isExit} returns true
	 * @return This tree node casted to {@link LabyrinthTree.Exit}
	 * @throws ClassCastException If this tree node is not of type {@link LabyrinthTree.Exit}
	 */
	public Exit asExit() throws ClassCastException;

	/**
	 * Unsafe cast operation, succeeds when {@link LabyrinthTree#isDeadEnd} returns true
	 * @return This tree node casted to {@link LabyrinthTree.DeadEnd}
	 * @throws ClassCastException If this tree node is not of type {@link LabyrinthTree.DeadEnd}
	 */
	public DeadEnd asDeadEnd() throws ClassCastException;

	/**
	 * Generates a human readable string representation of this tree
	 * @return A human readable string representation
	 */
	public String show();

	/**
	 * Subtype for labyrinth parts that may have children
	 */
	public static final class Crossroad implements LabyrinthTree {
		private final List<LabyrinthTree> children;

		/**
		 * Creates a new cross road and remembers an immutable view
		 * of the provided children
		 * @param children The child nodes of this tree node
		 */
		public Crossroad(final List<LabyrinthTree> children) {
			this.children = Collections.unmodifiableList(children);
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#children()
		 */
		@Override
		public final List<LabyrinthTree> children() {
			return this.children;
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#isCrossroad()
		 */
		@Override
		public final boolean isCrossroad() {
			return true;
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#isExit()
		 */
		@Override
		public final boolean isExit() {
			return false;
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#isDeadEnd()
		 */
		@Override
		public final boolean isDeadEnd() {
			return false;
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#asCrossroad()
		 */
		@Override
		public final Crossroad asCrossroad() throws ClassCastException {
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#asExit()
		 */
		@Override
		public final Exit asExit() throws ClassCastException {
			throw new ClassCastException();
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#asDeadEnd()
		 */
		@Override
		public final DeadEnd asDeadEnd() throws ClassCastException {
			throw new ClassCastException();
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public final int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (this.children == null ? 0 : this.children.hashCode());
			return result;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public final boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof Crossroad)) {
				return false;
			}
			final Crossroad other = (Crossroad) obj;
			if (this.children == null) {
				if (other.children != null) {
					return false;
				}
			} else if (!this.children.equals(other.children)) {
				return false;
			}
			return true;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public final String toString() {
			return this.children.stream()
				.map(Object::toString)
				.collect(Collectors.joining(", ", "Crossroad(", ")"));
		}

		private final static String indent(final int indent) {
			return IntStream.range(0, indent)
				.mapToObj(i -> "|  ")
				.collect(Collectors.joining());
		}

		private final static String showHelper(final LabyrinthTree tree, final int indent) {
			final String ind = Crossroad.indent(indent);
			if (tree.isCrossroad()) {
				return tree.asCrossroad().children.stream()
					.map(d -> Crossroad.showHelper(d, indent + 1))
					.collect(Collectors.joining(
						",\n",
						ind + "Crossroad(\n",
						"\n" + ind + ")"
					));
			} else if (tree.isDeadEnd() || tree.isExit()) {
				return ind + tree.show();
			} else {
				throw new IllegalArgumentException();
			}
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#show()
		 */
		@Override
		public final String show() {
			return Crossroad.showHelper(this, 0);
		}

	}

	/**
	 * Subtype for labyrinth parts that mark an exit
	 */
	public static final class Exit implements LabyrinthTree {

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#children()
		 */
		@Override
		public final List<LabyrinthTree> children() {
			return Collections.emptyList();
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#isCrossroad()
		 */
		@Override
		public final boolean isCrossroad() {
			return false;
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#isExit()
		 */
		@Override
		public final boolean isExit() {
			return true;
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#isDeadEnd()
		 */
		@Override
		public final boolean isDeadEnd() {
			return false;
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#asCrossroad()
		 */
		@Override
		public final Crossroad asCrossroad() throws ClassCastException {
			throw new ClassCastException();
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#asExit()
		 */
		@Override
		public final Exit asExit() throws ClassCastException {
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#asDeadEnd()
		 */
		@Override
		public final DeadEnd asDeadEnd() throws ClassCastException {
			throw new ClassCastException();
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public final int hashCode() {
			return 37;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public final boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			return obj instanceof Exit;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public final String toString() {
			return "Exit()";
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#show()
		 */
		@Override
		public final String show() {
			return this.toString();
		}

	}

	/**
	 * Subtype for labyrinth parts that mark a dead end
	 */
	public static final class DeadEnd implements LabyrinthTree {

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#children()
		 */
		@Override
		public final List<LabyrinthTree> children() {
			return Collections.emptyList();
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#isCrossroad()
		 */
		@Override
		public final boolean isCrossroad() {
			return false;
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#isExit()
		 */
		@Override
		public final boolean isExit() {
			return false;
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#isDeadEnd()
		 */
		@Override
		public final boolean isDeadEnd() {
			return true;
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#asCrossroad()
		 */
		@Override
		public final Crossroad asCrossroad() throws ClassCastException {
			throw new ClassCastException();
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#asExit()
		 */
		@Override
		public final Exit asExit() throws ClassCastException {
			throw new ClassCastException();
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#asDeadEnd()
		 */
		@Override
		public final DeadEnd asDeadEnd() throws ClassCastException {
			return this;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public final int hashCode() {
			return 653;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public final boolean equals(final Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			return obj instanceof DeadEnd;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public final String toString() {
			return "DeadEnd()";
		}

		/*
		 * (non-Javadoc)
		 * @see labyrinth.LabyrinthTree#show()
		 */
		@Override
		public final String show() {
			return this.toString();
		}

	}

}
