import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;



/**
 * Created by alissia on 4/10/17.
 */
public class ZauberbeutelTest {
    int[] vals = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    String[] strs = new String[] {"abc", "bla","bar", "foo","demi","wallhalla", "xylophon"};


    List<String> strst = new Stack<String>();
    List<String> stral = new ArrayList<String>();
    List<Integer> intst = new Stack<Integer>();
    List<Integer> intal = new ArrayList<Integer>();
    Set<String> strhs = new HashSet<String>();
    Set<String> strts = new TreeSet<String>();
    Set<Integer> inths = new HashSet<Integer>();
    Set<Integer> intts = new TreeSet<Integer>();





    @Test
    public void maxTest(){
        for (int i:vals) {
            intal.add(new Integer(i));
            intst.add(new Integer(i));
            inths.add(new Integer(i));
            intts.add(new Integer(i));
        }
        for (String str: strs){
            stral.add(new String(str));
            strst.add(new String(str));
            strhs.add(new String(str));
            strts.add(new String(str));
        }
        assertEquals(new String("xylophon"),Zauberbeutel.max(stral));
        assertEquals(new String("xylophon"),Zauberbeutel.max(strst));
        assertEquals(new String("xylophon"),Zauberbeutel.max(strhs));
        assertEquals(new String("xylophon"),Zauberbeutel.max(strts));
        assertEquals(new Integer(15),Zauberbeutel.max(intst));
        assertEquals(new Integer(15),Zauberbeutel.max(intal));
        assertEquals(new Integer(15),Zauberbeutel.max(inths));
        assertEquals(new Integer(15),Zauberbeutel.max(intts));
    }

    @Test
    public void minTest(){
        for (int i:vals) {
            intal.add(new Integer(i));
            intst.add(new Integer(i));
            inths.add(new Integer(i));
            intts.add(new Integer(i));
        }
        for (String str: strs){
            stral.add(new String(str));
            strst.add(new String(str));
            strhs.add(new String(str));
            strts.add(new String(str));
        }
        assertEquals(new String("abc"),Zauberbeutel.min(stral));
        assertEquals(new String("abc"),Zauberbeutel.min(strst));
        assertEquals(new String("abc"),Zauberbeutel.min(strhs));
        assertEquals(new String("abc"),Zauberbeutel.min(strts));
        assertEquals(new Integer(1),Zauberbeutel.min(intst));
        assertEquals(new Integer(1),Zauberbeutel.min(intal));
        assertEquals(new Integer(1),Zauberbeutel.min(inths));
        assertEquals(new Integer(1),Zauberbeutel.min(intts));

    }
}
