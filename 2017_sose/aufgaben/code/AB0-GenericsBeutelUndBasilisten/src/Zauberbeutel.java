import java.util.Collection;

/**
 * Created by alissia on 4/7/17.
 */
public class Zauberbeutel {
    public static <T extends Comparable<T>> T max(Collection<T> thing){
    	return thing
    			.stream()
    			.max( (a,b) -> a.compareTo(b) )
    			.orElseThrow(() -> new IllegalArgumentException("There's no maximum value"));
    }
    public static <T extends Comparable<T>> T min(Collection<T> thing){
    	return thing
    			.stream()
    			.min( (a,b) -> a.compareTo(b) )
    			.orElseThrow(() -> new IllegalArgumentException("There's no minimum value"));
    }
}
