package basilistendiaet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Abspeckprogramm {

    public static <E> List<E> distinct(List<E> lst) {
        return new ArrayList<>(new HashSet<>(lst));
    }
}
