package tree;

import java.util.NoSuchElementException;
import java.util.stream.IntStream;

import org.junit.Test;

import tree.AddTree;
import tree.DivTree;
import tree.ExpTree;
import tree.MulTree;
import tree.NumTree;
import tree.SubTree;

import static org.junit.Assert.*;

public final class ExpTreeTest {

	private static int gaussianSum(int n) {
		return (n * n + n) / 2;
	}

	private static int factorial(int n) {
		if (n == 1) return 1;
		else return factorial(n - 1) * n;
	}

	/* AddTree */

	@Test(expected = NoSuchElementException.class)
	public final void add_test1() {
		final ExpTree tree = new AddTree();
		tree.evaluate();
	}

	@Test(expected = NoSuchElementException.class)
	public final void add_isomorph_test1() {
		final ExpTree tree = new AddTree();
		tree.isomorph(new NumTree(2));
	}

	@Test(expected = NoSuchElementException.class)
	public final void add_isomorph_test2() {
		final ExpTree tree = new NumTree(2);
		tree.isomorph(new AddTree());
	}

	@Test
	public final void add_test2() {
		final ExpTree tree = new AddTree();

		IntStream.range(0, 3).forEach(i -> {
			final ExpTree subTree = new AddTree();
			IntStream.range(1, 33).mapToObj(NumTree::new).forEach(subTree::appendChild);
			tree.appendChild(subTree);
		});

		assertEquals(gaussianSum(32) * 3, tree.evaluate());
	}

	@Test
	public final void add_test3() {
		final ExpTree tree = new AddTree();
		IntStream.range(0, 50).mapToObj(NumTree::new).forEach(tree::appendChild);
		int i = 0;
		for (ExpTree e : tree.children()) {
			assertEquals(i, e.evaluate());
			i++;
		}
	}

	/* SubTree */

	@Test(expected = NoSuchElementException.class)
	public final void sub_test1() {
		final ExpTree tree = new SubTree();
		tree.evaluate();
	}

	@Test(expected = NoSuchElementException.class)
	public final void sub_isomorph_test1() {
		final ExpTree tree = new SubTree();
		tree.isomorph(new NumTree(2));
	}

	@Test(expected = NoSuchElementException.class)
	public final void sub_isomorph_test2() {
		final ExpTree tree = new NumTree(2);
		tree.isomorph(new SubTree());
	}

	@Test
	public final void sub_test2() {
		final ExpTree tree = new SubTree();

		IntStream.range(0, 100).mapToObj(NumTree::new).forEach(tree::appendChild);

		int expected = 0;
		for (int i = 1; i < 100; i++) {
			expected -= i;
		}

		assertEquals(expected, tree.evaluate());
	}

	@Test
	public final void sub_test3() {
		final ExpTree tree = new SubTree();

		IntStream.range(0, 100).map(i -> 99 - i).mapToObj(NumTree::new).forEach(tree::appendChild);

		int expected = 99;
		for (int i = 98; i >= 0; i--) {
			expected -= i;
		}

		assertEquals(expected, tree.evaluate());
	}

	@Test
	public final void sub_test4() {
		final ExpTree tree = new SubTree();
		IntStream.range(0, 50).mapToObj(NumTree::new).forEach(tree::appendChild);
		int i = 0;
		for (ExpTree e : tree.children()) {
			assertEquals(i, e.evaluate());
			i++;
		}
	}

	/* MulTree */

	@Test(expected = NoSuchElementException.class)
	public final void mul_test1() {
		final ExpTree tree = new MulTree();
		tree.evaluate();
	}

	@Test(expected = NoSuchElementException.class)
	public final void mul_isomorph_test1() {
		final ExpTree tree = new MulTree();
		tree.isomorph(new NumTree(2));
	}

	@Test(expected = NoSuchElementException.class)
	public final void mul_isomorph_test2() {
		final ExpTree tree = new NumTree(2);
		tree.isomorph(new MulTree());
	}

	@Test
	public final void mul_test2() {
		final ExpTree tree = new MulTree();

		IntStream.range(0, 3).forEach(i -> {
			final ExpTree subTree = new MulTree();
			IntStream.range(1, 6).mapToObj(NumTree::new).forEach(subTree::appendChild);
			tree.appendChild(subTree);
		});

		assertEquals(factorial(5) * factorial(5) * factorial(5), tree.evaluate());
	}

	@Test
	public final void mul_test3() {
		final ExpTree tree = new MulTree();
		IntStream.range(0, 50).mapToObj(NumTree::new).forEach(tree::appendChild);
		int i = 0;
		for (ExpTree e : tree.children()) {
			assertEquals(i, e.evaluate());
			i++;
		}
	}

	/* DivTree */

	@Test(expected = NoSuchElementException.class)
	public final void div_test1() {
		final ExpTree tree = new DivTree();
		tree.evaluate();
	}

	@Test(expected = NoSuchElementException.class)
	public final void div_isomorph_test1() {
		final ExpTree tree = new DivTree();
		tree.isomorph(new NumTree(2));
	}

	@Test(expected = NoSuchElementException.class)
	public final void div_isomorph_test2() {
		final ExpTree tree = new NumTree(2);
		tree.isomorph(new DivTree());
	}

	@Test
	public final void div_test2() {
		final ExpTree tree = new DivTree();

		IntStream.range(1, 100).mapToObj(NumTree::new).forEach(tree::appendChild);

		int expected = 1;
		for (int i = 1; i < 100; i++) {
			expected /= i;
		}

		assertEquals(expected, tree.evaluate());
	}

	@Test
	public final void div_test3() {
		final ExpTree tree = new DivTree();

		IntStream.range(1, 100).map(i -> 100 - i).mapToObj(NumTree::new).forEach(tree::appendChild);

		int expected = 99;
		for (int i = 98; i > 0; i--) {
			expected /= i;
		}

		assertEquals(expected, tree.evaluate());
	}

	@Test
	public final void div_test4() {
		final ExpTree tree = new DivTree();
		IntStream.range(0, 50).mapToObj(NumTree::new).forEach(tree::appendChild);
		int i = 0;
		for (ExpTree e : tree.children()) {
			assertEquals(i, e.evaluate());
			i++;
		}
	}

	/* isomorphism check */

	@Test
	public final void isomorph1() {
		final ExpTree root1 = new DivTree();
		final ExpTree root2 = new AddTree();

		/* first tree */
		final ExpTree l1 = new MulTree();
		l1.appendChild(new NumTree(42));
		final ExpTree lr1 = new SubTree();
		lr1.appendChild(new NumTree(1337));
		lr1.appendChild(new NumTree(8888));
		l1.appendChild(lr1);

		final ExpTree r1 = new AddTree();
		final ExpTree rl1 = new SubTree();
		rl1.appendChild(new NumTree(Integer.MIN_VALUE));
		r1.appendChild(rl1);

		root1.appendChild(l1);
		root1.appendChild(r1);

		/* second tree */
		final ExpTree l2 = new AddTree();
		final ExpTree lr2 = new DivTree();
		lr2.appendChild(new NumTree(4321));
		lr2.appendChild(new NumTree(1234));
		l2.appendChild(lr2);
		l2.appendChild(new NumTree(9999));

		final ExpTree r2 = new MulTree();
		final ExpTree rl2 = new DivTree();
		rl2.appendChild(new NumTree(Integer.MAX_VALUE));
		r2.appendChild(rl2);

		root2.appendChild(r2);
		root2.appendChild(l2);

		assertTrue(root1.isomorph(root2) && root2.isomorph(root1));
	}

	@Test
	public final void isomorph2() {
		final ExpTree root1 = new NumTree(0xDEADC0DE);
		final ExpTree root2 = new NumTree(0xCAFEBABE);
		assertTrue(root1.isomorph(root2) && root2.isomorph(root1));
	}

	@Test
	public final void isomorph3() {
		final ExpTree root1 = new MulTree();
		final ExpTree root2 = new AddTree();

		root1.appendChild(new NumTree(0xDEADC0DE));
		root2.appendChild(new NumTree(0xCAFEBABE));

		assertTrue(root1.isomorph(root2) && root2.isomorph(root1));
	}

	@Test
	public final void notIsomorph1() {
		final ExpTree root1 = new DivTree();
		final ExpTree root2 = new AddTree();

		/* first tree */
		final ExpTree l1 = new MulTree();
		l1.appendChild(new NumTree(42));
		final ExpTree lr1 = new SubTree();
		lr1.appendChild(new NumTree(1337));
		lr1.appendChild(new NumTree(8888));
		l1.appendChild(lr1);

		final ExpTree r1 = new AddTree();
		final ExpTree rl1 = new SubTree();
		rl1.appendChild(new NumTree(Integer.MIN_VALUE));
		r1.appendChild(rl1);

		root1.appendChild(l1);
		root1.appendChild(r1);

		/* second tree */
		final ExpTree l2 = new AddTree();
		final ExpTree rl2 = new DivTree();
		rl2.appendChild(new NumTree(Integer.MAX_VALUE));
		l2.appendChild(rl2);
		l2.appendChild(new NumTree(9999));

		final ExpTree r2 = new MulTree();
		final ExpTree lr2 = new DivTree();
		lr2.appendChild(new NumTree(4321));
		lr2.appendChild(new NumTree(1234));
		r2.appendChild(lr2);

		root2.appendChild(r2);
		root2.appendChild(l2);

		assertTrue(!root1.isomorph(root2) && !root2.isomorph(root1));
	}

	@Test
	public final void notIsomorph2() {
		final ExpTree root1 = new NumTree(0xDEADC0DE);
		final ExpTree root2 = new MulTree();
		root2.appendChild(new NumTree(0xCAFEBABE));
		assertTrue(!root1.isomorph(root2) && !root2.isomorph(root1));
	}

	@Test
	public final void notIsomorph3() {
		final ExpTree root1 = new MulTree();
		final ExpTree root2 = new AddTree();

		root1.appendChild(new NumTree(0xDEADC0DE));
		root1.appendChild(new NumTree(0xCAFEBABE));
		root2.appendChild(new NumTree(0xCAFEBABE));

		assertTrue(!root1.isomorph(root2) && !root2.isomorph(root1));
	}

}
