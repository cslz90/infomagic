package tree;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

/* Musterlösung */
public final class NumTree implements ExpTree {
	private int value;

	public NumTree(int value) {
		this.value = value;
	}
	@Override
	public int evaluate() throws NoSuchElementException {
		return value;
	}

	@Override
	public List<ExpTree> children() {
		return new LinkedList<>();
	}

	@Override
	public void appendChild(ExpTree child) {}

	@Override
	public boolean isomorph(ExpTree other) throws NoSuchElementException {
		return new CanonicalNumber(this).equals(new CanonicalNumber(other));
	}

}
