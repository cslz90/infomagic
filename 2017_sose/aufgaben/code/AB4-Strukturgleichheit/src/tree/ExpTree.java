package tree;

import java.util.List;
import java.util.NoSuchElementException;

/* Musterlösung */
public interface ExpTree {

  public abstract int evaluate() throws NoSuchElementException;  /* evaluate the value of this tree */

  public abstract List<ExpTree> children(); /* return all children */

  public abstract void appendChild(ExpTree child); /* append child to this tree */

  public abstract boolean isomorph(ExpTree other) throws NoSuchElementException; /* check if `this` and `other` are isomorph */

}
