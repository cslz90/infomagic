package tree;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.sun.istack.internal.NotNull;

/**
 * Canonical number for rooted trees to detect isomorphism originally invented by Aho, Hopcroft and Ullman
 * @see http://www.imsc.res.in/~vikram/DiscreteMaths/2011/tree.pdf
 */
public final class CanonicalNumber implements Comparable<CanonicalNumber> {
	private final long[] bits; /* the internal representation of the bitset */
	private final int size; /* the actual count of bits which are in use */

	/**
	 * Assign each leaf node the bitstring `10` and each parent node the bitstring `1b1b2b3...bn0`
	 * where b1, b2, b3, ..., bn are the canonical numbers (numerically sorted) of its children.
	 * If the canonical numbers of two trees are equal, the two trees are isomorphic (this is because
	 * the canonical number captures the structure of the tree). Because a canonical number is
	 * always a bitstring, one can represent it as bitset which also allows fast comparisons.
	 */
	public CanonicalNumber(@NotNull final ExpTree tree) throws NoSuchElementException {
		assert tree != null;
		assert tree instanceof AddTree ||
			   tree instanceof SubTree ||
			   tree instanceof MulTree ||
			   tree instanceof DivTree ||
			   tree instanceof NumTree;

		if (tree instanceof AddTree) {
			final AddTree t = (AddTree) tree;
			if (t.children().isEmpty()) {
				throw new NoSuchElementException();
			}

			final List<CanonicalNumber> numbers = t.children().stream()
					.map(CanonicalNumber::new).collect(Collectors.toList());
			this.size = numbers.stream().map(n -> n.size).reduce(2, (a, b) -> a + b);
			this.bits = new long[(int) Math.ceil(this.size / 64f)];
			initializeBitstring(numbers.stream());

		} else if (tree instanceof SubTree) {
			final SubTree t = (SubTree) tree;
			if (t.children().isEmpty()) {
				throw new NoSuchElementException();
			}

			final List<CanonicalNumber> numbers = t.children().stream()
					.map(CanonicalNumber::new).collect(Collectors.toList());
			this.size = numbers.stream().map(n -> n.size).reduce(2, (a, b) -> a + b);
			this.bits = new long[(int) Math.ceil(this.size / 64f)];
			initializeBitstring(numbers.stream());

		} else if (tree instanceof MulTree) {
			final MulTree t = (MulTree) tree;
			if (t.children().isEmpty()) {
				throw new NoSuchElementException();
			}

			final List<CanonicalNumber> numbers = t.children().stream()
					.map(CanonicalNumber::new).collect(Collectors.toList());
			this.size = numbers.stream().map(n -> n.size).reduce(2, (a, b) -> a + b);
			this.bits = new long[(int) Math.ceil(this.size / 64f)];
			initializeBitstring(numbers.stream());

		} else if (tree instanceof DivTree) {
			final DivTree t = (DivTree) tree;
			if (t.children().isEmpty()) {
				throw new NoSuchElementException();
			}

			final List<CanonicalNumber> numbers = t.children().stream()
					.map(CanonicalNumber::new).collect(Collectors.toList());
			this.size = numbers.stream().map(n -> n.size).reduce(2, (a, b) -> a + b);
			this.bits = new long[(int) Math.ceil(this.size / 64f)];
			initializeBitstring(numbers.stream());

		} else if (tree instanceof NumTree) {
			this.size = 2;
			this.bits = new long[1];
			initializeBitstring(Stream.empty());
		} else {
			throw new RuntimeException("should not happen");
		}

	}

	private final void initializeBitstring(final Stream<CanonicalNumber> numbers) {
		/* the first bit is always 1 */
		setBit(0, true);
		/* the last bit is always a 0 */
		setBit(size - 1, false);
		/* for NumTree the bitstring should be '10' at this point */
		numbers /* for any other tree the center of the bitstring is set, according to its children */
			.sorted() /* sort them in ascending order (this captures the structure of the tree but ignores order of the children) */
			.reduce(1, (index, number) -> {
				/* set all b1, b2, b3, ..., bn according to the accumulated index `index` (starting at 1, because the first bit is already set) */
				IntStream.range(0, number.size).forEach(i -> this.setBit(index + i, number.getBit(i)));
				return index + number.size;
			}, (a, b) -> a + b /* not used, only for parallel streams */);
	}

	private final boolean getBit(final int index) {
		if (index < 0 || index >= size) {
			throw new ArrayIndexOutOfBoundsException();
		}
		return (this.bits[index / 64] & (1 << (index % 64))) != 0;
	}

	private final void setBit(final int index, final boolean value) {
		if (index < 0 || index >= size) {
			throw new ArrayIndexOutOfBoundsException();
		}
		if (value) {
			this.bits[index / 64] |= (1 << (index % 64));
		} else {
			this.bits[index / 64] &= ~(1 << (index % 64));
		}
	}

	@Override
	public boolean equals(final Object other) {
		if (other instanceof CanonicalNumber) {
			final CanonicalNumber num = (CanonicalNumber) other;
			return num.size == this.size && Arrays.equals(this.bits, num.bits);
		}
		return false;
	}

	@Override
	public String toString() {
		/* fancy printing of the bitstring */
		return IntStream.range(0, size)
				.mapToObj(i -> getBit(i) ? "1" : "0")
				.collect(Collectors.joining());
	}

	@Override
	public final int compareTo(CanonicalNumber o) {
		/* simply compare the internal long array, one can think of `this.bits` as a java.math.BigInteger */
		if (this.size > o.size) {
			return 1;
		} else if (this.size < o.size) {
			return -1;
		} else {
			for (int i = 0; i < this.bits.length; i++) {
				final int result = Long.compare(this.bits[i], o.bits[i]);
				if (result != 0) {
					return result;
				}
			}
			return 0;
		}
	}

}
