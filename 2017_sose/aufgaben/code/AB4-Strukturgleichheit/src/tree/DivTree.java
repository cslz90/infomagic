package tree;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/* Musterlösung */
public final class DivTree implements ExpTree {
	private final List<ExpTree> children = new ArrayList<>();

	@Override
	public int evaluate() throws NoSuchElementException {
		if (children.isEmpty()) {
			throw new NoSuchElementException();
		}
		int result = children.get(0).evaluate();
		for (int i = 1; i < children.size(); i++) {
			result /= children.get(i).evaluate();
		}
		return result;
	}

	@Override
	public List<ExpTree> children() {
		return children;
	}

	@Override
	public void appendChild(ExpTree child) {
		children.add(child);
	}

	@Override
	public boolean isomorph(ExpTree other) throws NoSuchElementException {
		return new CanonicalNumber(this).equals(new CanonicalNumber(other));
	}

}
