package tree;

import java.util.function.IntBinaryOperator;

public class DivTree extends AbstractOperationTree {

    @Override
    protected IntBinaryOperator getEvalOperator() {
        return (l, r) -> l / r;
    }
}