package tree;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

public class NumTree extends AbstractExpTree {

    private int value;

    public NumTree(int value) {
        this.value = value;
    }

    @Override
    public int evaluate() throws NoSuchElementException {
        return value;
    }

    @Override
    public List<ExpTree> children() {
        return new LinkedList<>();
    }

    @Override
    public void appendChild(ExpTree child) {
        throw new IllegalArgumentException("NumTree contains no children");
    }

    @Override
    public boolean isomorph(ExpTree other) throws NoSuchElementException {
        super.isomorph(other);
        return other.children().isEmpty();
    }

    @Override
    public boolean isEmptyAllowed() {
        return true;
    }
}
