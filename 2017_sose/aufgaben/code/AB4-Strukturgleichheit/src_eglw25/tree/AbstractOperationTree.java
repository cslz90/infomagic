package tree;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.IntBinaryOperator;

public abstract class AbstractOperationTree extends AbstractExpTree {

    private List<ExpTree> children = new LinkedList<>();

    protected abstract IntBinaryOperator getEvalOperator();

    @Override
    public int evaluate() throws NoSuchElementException {
        return children.stream().mapToInt(ExpTree::evaluate).reduce(getEvalOperator()).getAsInt();
    }

    @Override
    public List<ExpTree> children() {
        return children;
    }

    @Override
    public void appendChild(ExpTree child) {
        children.add(child);
    }

    @Override
    public boolean isomorph(ExpTree other) throws NoSuchElementException {
        super.isomorph(other);
        if (children.size() != other.children().size()) {
            return false;
        }
        List<ExpTree> otherChCopy = new LinkedList<>(other.children());
        for (ExpTree t1 : children) {
            Iterator<ExpTree> otherChIterator = otherChCopy.iterator();
            boolean isFound = false;
            while (otherChIterator.hasNext()) {
                if (t1.isomorph(otherChIterator.next())) {
                    otherChIterator.remove();
                    isFound = true;
                    break;
                }
            }
            if (!isFound) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isEmptyAllowed() {
        return false;
    }
}
