package tree;

import java.util.NoSuchElementException;

public abstract class AbstractExpTree implements ExpTree {

    @Override
    public boolean isomorph(ExpTree other) throws NoSuchElementException {
        checkEmptyAllowed(this);
        checkEmptyAllowed(other);
        return true;
    }

    private static void checkEmptyAllowed(ExpTree tree) {
        if (!tree.isEmptyAllowed() && tree.children().isEmpty()) {
            throw new NoSuchElementException();
        }
    }

}
