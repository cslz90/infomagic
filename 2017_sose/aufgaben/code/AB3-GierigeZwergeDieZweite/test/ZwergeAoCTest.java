import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ZwergeAoCTest {

	public static void main(String[] args) throws IOException {
		// Sicher gehen, dass wir ohne Parameter ohne Fehler beenden.
		// Das Eingabetest Plugin benutzt diese Information als Indiz, dass wir
		// die "Musterlösung" richtig geschrieben haben.

		try {

			if (args.length == 0) {
				System.exit(0);
			}

			String inputFile = null;
			String studentOutputFile = null;

			for (String arg : args) {
				if (arg.startsWith("input=")) {
					inputFile = arg.substring("input=".length());
				}

				if (arg.startsWith("student-output=")) {
					studentOutputFile = arg.substring("student-output=".length());
				}
			}

			String input = new String(Files.readAllBytes(Paths.get(inputFile)), StandardCharsets.UTF_8);

			final String regex = "\\d{1,}";

			int anzahlDerZwerge = getNumber(input, regex);
			if (anzahlDerZwerge < 1) {
				System.out.println("Der Generator Ihrer Aufgabe hat einen falschen Input produziert, bitte wenden Sie sich an die Tutoren.");
				System.exit(0);
			}
			int ergebnis = josephus(anzahlDerZwerge);

			int gotOutput = 0;

			try {
				gotOutput = Integer.parseInt(new String(Files.readAllBytes(Paths.get(studentOutputFile)), StandardCharsets.UTF_8).trim());

				if (ergebnis != gotOutput) {
					System.out.println("Ihr Ergebnis ist leider falsch!");
				}

			} catch (NumberFormatException e) {
				System.out.println("Abgabe muss eine Zahl sein!");
			}

		} catch (final Throwable t) {
			if (t.getMessage() != null && t.getMessage().trim().length() > 0) {
				System.out.println(t.getMessage().trim());
			}
			System.exit(0);
		}

	}

	private static int getNumber(String line, String regex) {

		final Pattern pattern = Pattern.compile(regex);
		final Matcher matcher = pattern.matcher(line);

		int number = 0;

		while (matcher.find()) {
			number = Integer.parseInt(matcher.group(0));
		}

		return number;
	}

	/* only works for n >= 1 */
	private static int josephus(int n) {
		if (n == 1) {
			return 1;
		}

		if ((n % 2) == 0) {
			return 2 * josephus(n / 2) - 1;
		} else {
			return 2 * josephus((n - 1) / 2) + 1;
		}
	}

}
