package generator;

import java.util.Random;

public final class IntGenerator {
	private static final Random random = new Random();
	private static final int maxZwerge = 1000; /* inclusively */
	private static final int minZwerge = 300; /* inclusively */

	private IntGenerator() {}

	public static void main(final String[] arguments) {
		final int anzahlZwerge = random.nextInt(maxZwerge - minZwerge + 1) + minZwerge;
		System.out.printf("Am Spiel sind %d Zwerge beteiligt.\n", anzahlZwerge);
	}

}
