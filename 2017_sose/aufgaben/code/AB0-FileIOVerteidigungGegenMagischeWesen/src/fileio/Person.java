package fileio;

public class Person {

    private String name;

    private int age;

    public Person(String name, int age) {
        setName(name);
        setAge(age);
    }

    public Person(String line) {
        String[] parts = line.split(", ");
        setName(parts[0]);
        setAge(Integer.parseInt(parts[1]));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("%s, %d\n", name, age);
    }
}
