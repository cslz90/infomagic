package fileio;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class RegexScanTest {

    private final String filePath = "Blatt0.txt";

    @Test
    public void readingTheFileShouldSucceed() throws IOException {
        assertNotEquals(null, RegexScan.readEntireFile(filePath));
    }

    @Test
    public void numarareMagischTest() {
        final String fileContents = RegexScan.readEntireFile(filePath);
        assertEquals(RegexScan.numerare(fileContents, "magisch"), 6);
    }

    @Test
    public void numarareZauberTest() {
        final String fileContents = RegexScan.readEntireFile(filePath);
        assertEquals(RegexScan.numerare(fileContents, "zauber"), 2);
    }

    @Test
    public void numarareLinksTest() {
        final String fileContents = RegexScan.readEntireFile(filePath);
        assertEquals(RegexScan.numerare(fileContents, "\\[.+\\]\\(.+\\)"), 3);
    }

    @Test
    public void numarareCodeTest() {
        final String fileContents = RegexScan.readEntireFile(filePath);
        assertEquals(RegexScan.numerare(fileContents, "`[^`]*`"), 14);
    }

}
