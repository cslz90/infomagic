package magicboxes;

public class MyDoublePointer<T, U> {

    private final U value;
    private final T first;
    private final T last;

    public MyDoublePointer(T first, T last, U value) {
        this.first = first;
        this.last  = last;
        this.value = value;
    }

}
