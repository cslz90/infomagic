package magicboxes;

public class MagicBoxes2 {

    private void fn2(final BoxVerification boxVerification) {
        final MyLinkedList<Integer> last = new MyLinkedList<>(5, null);
        final MyLinkedList<Integer> list = new MyLinkedList<>(1,
            new MyLinkedList<>(3, new MyLinkedList<>(6, last)));
        final MyDoublePointer<MyLinkedList<Integer>, String> box = new MyDoublePointer<>(list, last, "A");

        boxVerification.verify(box);
    }

    private void fn(final BoxVerification boxVerification) {
        fn2(boxVerification);
    }

    public void main(final BoxVerification boxVerification) {
        fn(boxVerification);
    }

}
