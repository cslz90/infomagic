package magicboxes;

public class MyLinkedList<T> {

    private final T value;
    private final MyLinkedList<T> next;

    public MyLinkedList(T value, MyLinkedList<T> next) {
        this.value = value;
        this.next = next;
    }
}
