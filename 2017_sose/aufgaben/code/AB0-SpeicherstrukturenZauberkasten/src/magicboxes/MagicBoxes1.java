package magicboxes;

public class MagicBoxes1 {

    private void fn2(final BoxVerification boxVerification) {
        boxVerification.verify(
                new MyLinkedList<>(5,
                        new MyLinkedList<>(6,
                                new MyLinkedList<>(10,
                                        new MyLinkedList<>(8,
                                                new MyLinkedList<>(9,
                                                        null)))))
        );
    }

    public void fn(final BoxVerification boxVerification) {
        fn2(boxVerification);
    }

    public void main(final BoxVerification boxVerification) {
        fn(boxVerification);
    }

}
