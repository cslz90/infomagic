package magicboxes;

public interface BoxVerification {
    void verify(Object box);
}
