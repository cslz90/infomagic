package magicboxes;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static magicboxes.CommonTestHelpers.*;

public class MagicBoxes2Test {

    private MagicBoxes2 boxes;

    @Before
    public void initialize() {
        boxes = new MagicBoxes2();
    }

    @Test
    public void testWasBoxVerificationCalled() {
        boxVerificationInvoked(boxes::main);
    }

    @Test
    public void testMemoryLayoutOfStackFrame() {
        verifyMemoryLayoutOfStackFrame(boxes::main);
    }

    @Test
    public void testMemoryLayoutOfHeap() {
        final BoxVerification boxVerification = (Object box) -> {
            try {
                InstanceGraphNode rootNode = InstanceGraph.build(box);

                final String boxPositionEmpty  = " [/] -> _ ";
                final String boxPositionA      = " [/] -> [A] -> _ ";
                final String boxPositionA1     = " [/] -> [A] -> [1] -> _ ";
                final String boxPositionA13    = " [/] -> [A] -> [1] -> [3] -> _ ";
                final String boxPositionA136   = " [/] -> [A] -> [1] -> [3] -> [6] -> _ ";

                final String boxPositionA5     = " [/] -> [A] -> [5] ";
                final String boxPositionA1365  = " [/] -> [A] -> [1] -> [3] -> [6] -> [5] ";

                final String boxPositionA5_    = " [/] -> [A] -> [5] -> _ ";
                final String boxPositionA1365_ = " [/] -> [A] -> [1] -> [3] -> [6] -> [5] -> _ ";

                final String invalidBoxFmt = "The %s box (%s) must have a member with value %s, but yours has not!";

                final String invalidBoxEmpty = String.format(invalidBoxFmt, "1st", boxPositionEmpty, "'A'");
                final String invalidBoxA     = String.format(invalidBoxFmt, "2nd", boxPositionA, "'1' or '5'");
                final String invalidBoxA1    = String.format(invalidBoxFmt, "3rd", boxPositionA1, 3);
                final String invalidBoxA13   = String.format(invalidBoxFmt, "3rd", boxPositionA13, 6);
                final String invalidBoxA136  = String.format(invalidBoxFmt, "4th", boxPositionA136, 5);

                try {
                    assertTrue(
                        forSomeOtherChild(rootNode, "A", invalidBoxEmpty, child1 ->
                            forSomeOtherChild(child1, 1, invalidBoxA, sibling2 ->
                                forSomeOtherChild(sibling2, 3, invalidBoxA1, sibling3 ->
                                    forSomeOtherChild(sibling3, 6, invalidBoxA13, sibling4 ->
                                        forSomeOtherChild(sibling4, 5, invalidBoxA136, sibling5 -> {

                                            assertTrue(String.format("The last box (%s) mustn't have any non-null members!", boxPositionA1365_),
                                                sibling5.children.isEmpty());

                                            return forSomeOtherChild(rootNode, "A", invalidBoxEmpty, achild2 ->
                                                forSomeOtherChild(achild2, 5, invalidBoxA, lastSibling -> {

                                                    assertTrue(String.format("The last box (%s) mustn't have any non-null members!", boxPositionA5_),
                                                        lastSibling.children.isEmpty());

                                                    assertTrue(
                                                        String.format("The boxes at (%s) and (%s) have to be the same!",
                                                            boxPositionA5, boxPositionA1365),
                                                        achild2.fieldValue == sibling4.fieldValue);
                                                    return true;
                                                })
                                            );

                                        })
                                    )
                                )
                            )
                        )
                    );
                } catch (BacktrackException bte) {
                    throw new AssertionError(bte.getMessage());
                }

            } catch (IllegalAccessException e) {
                fail(String.format("Can't verify memory layout of box '%s'!", box));
            }
        };

        boxes.main(boxVerification);
    }

}
