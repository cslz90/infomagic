package magicboxes;

import java.util.ArrayList;
import java.util.List;

/**
 * Class {@link magicboxes.InstanceGraphNode} represents a node of simple directed graph.
 * Each edge to a node describes the member-relation of an object to their value.
 */
class InstanceGraphNode {

    final InstanceGraphNode parent;
    final Object fieldValue;
    final List<InstanceGraphNode> children;

    InstanceGraphNode(InstanceGraphNode parent, Object fieldValue) {
        this.parent = parent;
        this.fieldValue = fieldValue;
        this.children = new ArrayList<>();
    }

}
