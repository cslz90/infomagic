package magicboxes;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.IdentityHashMap;
import java.util.Map;

/**
 * InstanceGraph builds a graph of your java-instances.
 */
class InstanceGraph {

    static InstanceGraphNode build(final Object object) throws IllegalAccessException {
        if (isPrimitive(object)) {
            return new InstanceGraphNode(null, null);
        } else {
            final Map<Object, InstanceGraphNode> seen = new IdentityHashMap<>();

            final InstanceGraphNode rootNode = new InstanceGraphNode(null, object);
            seen.put(object, rootNode);

            InstanceGraph.addAllChildren(seen, rootNode);

            return rootNode;
        }
    }

    private static void addAllChildren(Map<Object, InstanceGraphNode> seen, InstanceGraphNode parentNode) throws IllegalAccessException {
        if (parentNode.fieldValue == null || isPrimitive(parentNode.fieldValue)) {
            return;
        }

        final Class<?> instanceType = parentNode.fieldValue.getClass();
        if (instanceType.isArray()) {
            for (int i = 0; i < Array.getLength(parentNode.fieldValue); i++) {
                Object fieldInstance = Array.get(parentNode.fieldValue, i);
                InstanceGraph.addSingleChild(seen, parentNode, fieldInstance);
            }
        } else {
            for (Field field : instanceType.getDeclaredFields()) {
                if (!field.isAccessible()) field.setAccessible(true);
                Object fieldInstance = field.get(parentNode.fieldValue);
                InstanceGraph.addSingleChild(seen, parentNode, fieldInstance);
            }
        }
    }

    private static void addSingleChild(Map<Object, InstanceGraphNode> seen, InstanceGraphNode parentNode, Object fieldInstance) throws IllegalAccessException {
        if (isPrimitive(fieldInstance)) {
            InstanceGraph.addPrimitiveChild(parentNode, fieldInstance);
        } else {
            InstanceGraph.addCompoundChildren(seen, parentNode, fieldInstance);
        }
    }

    private static void addPrimitiveChild(InstanceGraphNode parentNode, Object fieldValue) {
        parentNode.children.add(new InstanceGraphNode(parentNode, fieldValue));
    }

    private static void addCompoundChildren(Map<Object, InstanceGraphNode> seen, InstanceGraphNode parentNode, Object fieldValue) throws IllegalAccessException {
        InstanceGraphNode node = seen.get(fieldValue);

        if (node == null) {
            node = new InstanceGraphNode(parentNode, fieldValue);
            seen.put(fieldValue, node);
            InstanceGraph.addAllChildren(seen, node);
        }

        parentNode.children.add(node);
    }

    private static boolean isPrimitive(Object value) {
        if (value == null) return false;
        final Class<?> clazz = value.getClass();

        return (clazz.isPrimitive()      ||
                clazz == Byte.class      ||
                clazz == Boolean.class   ||
                clazz == Character.class ||
                clazz == Double.class    ||
                clazz == Float.class     ||
                clazz == Short.class     ||
                clazz == Integer.class   ||
                clazz == Long.class      ||
                // strings are considered primitive here
                clazz == String.class);
    }

}
