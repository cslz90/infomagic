package magicboxes;

import org.junit.Before;
import org.junit.Test;

import static magicboxes.CommonTestHelpers.*;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class MagicBoxes1Test {

    private MagicBoxes1 boxes;

    @Before
    public void initialize() {
        boxes = new MagicBoxes1();
    }

    @Test
    public void testWasBoxVerificationCalled() {
        boxVerificationInvoked(boxes::main);
    }

    @Test
    public void testMemoryLayoutOfStackFrame() {
        verifyMemoryLayoutOfStackFrame(boxes::main);
    }

    @Test
    public void testMemoryLayoutOfHeap() {
        final BoxVerification boxVerification = (Object box) -> {
            try {
                InstanceGraphNode rootNode = InstanceGraph.build(box);

                final String boxPosition5    = " [/] -> _ ";
                final String boxPosition6    = " [/] -> [5] -> _ ";
                final String boxPosition10   = " [/] -> [5] -> [6] -> _ ";
                final String boxPosition8    = " [/] -> [5] -> [6] -> [10] -> _ ";
                final String boxPosition9    = " [/] -> [5] -> [6] -> [10] -> [8] -> _ ";
                final String boxPositionLast = " [/] -> [5] -> [6] -> [10] -> [8] -> [9] -> _ ";

                final String invalidBoxFmt = "The %s box (%s) must have a member with value '%s', but yours has not!";
                final String invalidBox_5  = String.format(invalidBoxFmt, "1st", boxPosition5, 5);
                final String invalidBox_6  = String.format(invalidBoxFmt, "2nd", boxPosition6, 6);
                final String invalidBox_10 = String.format(invalidBoxFmt, "3rd", boxPosition10, 10);
                final String invalidBox_8  = String.format(invalidBoxFmt, "4th", boxPosition8, 8);
                final String invalidBox_9  = String.format(invalidBoxFmt, "5th", boxPosition9, 9);

                try {
                    assertTrue(
                        forSomeOtherChild(rootNode, 5, invalidBox_5, siblingOf_5 ->
                            forSomeOtherChild(siblingOf_5, 6, invalidBox_6, siblingOf_6 ->
                                forSomeOtherChild(siblingOf_6, 10, invalidBox_10, siblingOf_10 ->
                                    forSomeOtherChild(siblingOf_10, 8, invalidBox_8, siblingOf_8 ->
                                        forSomeOtherChild(siblingOf_8, 9, invalidBox_9, siblingOf_9 -> {
                                            assertTrue(String.format("The last box (%s) mustn't have any non-null members!", boxPositionLast),
                                                siblingOf_9.children.isEmpty());
                                            return true;
                                        })
                                    )
                                )
                            )
                        )
                    );
                } catch (BacktrackException bte) {
                    throw new AssertionError(bte.getMessage());
                }

            } catch (IllegalAccessException e) {
                fail(String.format("Can't verify memory layout of box '%s'!", box));
            }
        };

        boxes.main(boxVerification);
    }

}
