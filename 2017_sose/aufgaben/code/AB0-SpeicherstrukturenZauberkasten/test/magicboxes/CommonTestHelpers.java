package magicboxes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

class CommonTestHelpers {

    static void boxVerificationInvoked(final Consumer<BoxVerification> boxVerificationConsumer) {
        final AtomicBoolean verifyCalled = new AtomicBoolean(false);

        final BoxVerification boxVerification = (Object box) -> verifyCalled.set(true);

        boxVerificationConsumer.accept(boxVerification);

        if (!verifyCalled.get()) {
            fail("BoxVerification.verify was not invoked! Please verify your magic boxes!");
        }
    }

    static void verifyMemoryLayoutOfStackFrame(final Consumer<BoxVerification> boxVerificationConsumer) {
        final Predicate<StackTraceElement> testContextPredicate = (stackTraceElement ->
            MagicBoxes1.class.getName().equals(stackTraceElement.getClassName()) ||
            MagicBoxes2.class.getName().equals(stackTraceElement.getClassName())
        );

        final BoxVerification boxVerification = (Object box) -> {
            final List<StackTraceElement> relevantStacktrace = Arrays
                .stream(Thread.currentThread().getStackTrace())
                .filter(testContextPredicate)
                .limit(3)
                .collect(Collectors.toList());

            final List<String> callChain = new ArrayList<>(3);

            switch (relevantStacktrace.size()) {
                case 0:
                    fail("Unexpected error! This will never happen!");
                    break;
                case 3: callChain.add(relevantStacktrace.get(2).getMethodName());
                case 2: callChain.add(relevantStacktrace.get(1).getMethodName());
                case 1: callChain.add(relevantStacktrace.get(0).getMethodName());
                default:
                    break;
            }

            final String actualCallChain = callChain.stream()
                .collect(Collectors.joining(" - "));

            final String invalidCallChainMessage = String.format(
                "The method call-chain should be 'main - fn - fn2', but yours is '%s'!", actualCallChain);

            if (callChain.size() != 3) {
                fail(invalidCallChainMessage);
            }

            assertEquals(invalidCallChainMessage, "fn2",  relevantStacktrace.get(0).getMethodName());
            assertEquals(invalidCallChainMessage, "fn",   relevantStacktrace.get(1).getMethodName());
            assertEquals(invalidCallChainMessage, "main", relevantStacktrace.get(2).getMethodName());
        };

        boxVerificationConsumer.accept(boxVerification);
    }

    private static class TheChildAndItsSiblings {
        InstanceGraphNode child = null;
        List<InstanceGraphNode> siblings = new ArrayList<>();
    }

    static class BacktrackException extends RuntimeException {
        BacktrackException(final String message) {
            super(message);
        }
    }

    static boolean forSomeOtherChild(InstanceGraphNode rootNode, Object value,
                                     String errorMessage, Predicate<InstanceGraphNode> fn) {
        TheChildAndItsSiblings childAndSiblings = verifyValueAndExtractSiblings(rootNode, value);

        if (childAndSiblings.child == null) {
            throw new BacktrackException(errorMessage);
        }

        boolean exists = false;
        BacktrackException first = null;

        for (InstanceGraphNode otherChild : childAndSiblings.siblings) {
            try {
                exists = exists || fn.test(otherChild);
            } catch (BacktrackException bte) {
                if (first == null) {
                    first = bte;
                }
            }
        }

        if (!exists && first != null) {
            throw first;
        }

        return exists;
    }

    private static TheChildAndItsSiblings verifyValueAndExtractSiblings(InstanceGraphNode node, Object value) {
        TheChildAndItsSiblings car = new TheChildAndItsSiblings();

        for (InstanceGraphNode child : node.children) {
            final String fieldValue    = Objects.toString(child.fieldValue);
            final String expectedValue = Objects.toString(value);

            if (fieldValue.equals(expectedValue)) {
                car.child = child;
            } else {
                car.siblings.add(child);
            }
        }

        return car;
    }

}
