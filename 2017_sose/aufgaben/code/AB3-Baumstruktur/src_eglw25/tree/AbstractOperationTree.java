package tree;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.IntBinaryOperator;

public abstract class AbstractOperationTree implements ExpTree {

    private List<ExpTree> children = new LinkedList<>();

    protected abstract IntBinaryOperator getEvalOperator();

    @Override
    public int evaluate() throws NoSuchElementException {
        return children.stream().mapToInt(ExpTree::evaluate).reduce(getEvalOperator()).getAsInt();
    }

    @Override
    public List<ExpTree> children() {
        return children;
    }

    @Override
    public void appendChild(ExpTree child) {
        children.add(child);
    }

}
