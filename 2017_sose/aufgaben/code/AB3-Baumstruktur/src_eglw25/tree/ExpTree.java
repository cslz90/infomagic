package tree;

import java.util.List;
import java.util.NoSuchElementException;

public interface ExpTree {

    int evaluate() throws NoSuchElementException;  /* evaluate the value of this tree */

    List<ExpTree> children(); /* return all children */

    void appendChild(ExpTree child); /* append child to this tree */

}
