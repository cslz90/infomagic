package test;

import java.util.NoSuchElementException;
import java.util.stream.IntStream;

import org.junit.Test;

import tree.AddTree;
import tree.DivTree;
import tree.ExpTree;
import tree.MulTree;
import tree.NumTree;
import tree.SubTree;

import static org.junit.Assert.*;

public final class ExpTreeTest {

	private static int gaussianSum(int n) {
		return (n * n + n) / 2;
	}

	private static int factorial(int n) {
		if (n == 1) return 1;
		else return factorial(n - 1) * n;
	}

	/* AddTree */

	@Test(expected = NoSuchElementException.class)
	public final void add_test1() {
		final ExpTree tree = new AddTree();
		tree.evaluate();
	}

	@Test
	public final void add_test2() {
		final ExpTree tree = new AddTree();

		IntStream.range(0, 3).forEach(i -> {
			final ExpTree subTree = new AddTree();
			IntStream.range(1, 33).mapToObj(NumTree::new).forEach(subTree::appendChild);
			tree.appendChild(subTree);
		});

		assertEquals(gaussianSum(32) * 3, tree.evaluate());
	}

	@Test
	public final void add_test3() {
		final ExpTree tree = new AddTree();
		IntStream.range(0, 50).mapToObj(NumTree::new).forEach(tree::appendChild);
		int i = 0;
		for (ExpTree e : tree.children()) {
			assertEquals(i, e.evaluate());
			i++;
		}
	}

	/* SubTree */

	@Test(expected = NoSuchElementException.class)
	public final void sub_test1() {
		final ExpTree tree = new SubTree();
		tree.evaluate();
	}

	@Test
	public final void sub_test2() {
		final ExpTree tree = new SubTree();

		IntStream.range(0, 100).mapToObj(NumTree::new).forEach(tree::appendChild);

		int expected = 0;
		for (int i = 1; i < 100; i++) {
			expected -= i;
		}

		assertEquals(expected, tree.evaluate());
	}

	@Test
	public final void sub_test3() {
		final ExpTree tree = new SubTree();

		IntStream.range(0, 100).map(i -> 99 - i).mapToObj(NumTree::new).forEach(tree::appendChild);

		int expected = 99;
		for (int i = 98; i >= 0; i--) {
			expected -= i;
		}

		assertEquals(expected, tree.evaluate());
	}

	@Test
	public final void sub_test4() {
		final ExpTree tree = new SubTree();
		IntStream.range(0, 50).mapToObj(NumTree::new).forEach(tree::appendChild);
		int i = 0;
		for (ExpTree e : tree.children()) {
			assertEquals(i, e.evaluate());
			i++;
		}
	}

	/* MulTree */

	@Test(expected = NoSuchElementException.class)
	public final void mul_test1() {
		final ExpTree tree = new MulTree();
		tree.evaluate();
	}

	@Test
	public final void mul_test2() {
		final ExpTree tree = new MulTree();

		IntStream.range(0, 3).forEach(i -> {
			final ExpTree subTree = new MulTree();
			IntStream.range(1, 6).mapToObj(NumTree::new).forEach(subTree::appendChild);
			tree.appendChild(subTree);
		});

		assertEquals(factorial(5) * factorial(5) * factorial(5), tree.evaluate());
	}

	@Test
	public final void mul_test3() {
		final ExpTree tree = new MulTree();
		IntStream.range(0, 50).mapToObj(NumTree::new).forEach(tree::appendChild);
		int i = 0;
		for (ExpTree e : tree.children()) {
			assertEquals(i, e.evaluate());
			i++;
		}
	}

	/* DivTree */

	@Test(expected = NoSuchElementException.class)
	public final void div_test1() {
		final ExpTree tree = new DivTree();
		tree.evaluate();
	}

	@Test
	public final void div_test2() {
		final ExpTree tree = new DivTree();

		IntStream.range(1, 100).mapToObj(NumTree::new).forEach(tree::appendChild);

		int expected = 1;
		for (int i = 1; i < 100; i++) {
			expected /= i;
		}

		assertEquals(expected, tree.evaluate());
	}

	@Test
	public final void div_test3() {
		final ExpTree tree = new DivTree();

		IntStream.range(1, 100).map(i -> 100 - i).mapToObj(NumTree::new).forEach(tree::appendChild);

		int expected = 99;
		for (int i = 98; i > 0; i--) {
			expected /= i;
		}

		assertEquals(expected, tree.evaluate());
	}

	@Test
	public final void div_test4() {
		final ExpTree tree = new DivTree();
		IntStream.range(0, 50).mapToObj(NumTree::new).forEach(tree::appendChild);
		int i = 0;
		for (ExpTree e : tree.children()) {
			assertEquals(i, e.evaluate());
			i++;
		}
	}

}
