package tree;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public final class MulTree implements ExpTree {
	private final List<ExpTree> children = new ArrayList<>();

	@Override
	public int evaluate() throws NoSuchElementException {
		if (children.isEmpty()) {
			throw new NoSuchElementException();
		}
		int result = children.get(0).evaluate();
		for (int i = 1; i < children.size(); i++) {
			result *= children.get(i).evaluate();
		}
		return result;
	}

	@Override
	public List<ExpTree> children() {
		return children;
	}

	@Override
	public void appendChild(ExpTree child) {
		children.add(child);
	}

}
