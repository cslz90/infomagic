package tree;

import java.util.List;
import java.util.NoSuchElementException;

public interface ExpTree {

  public abstract int evaluate() throws NoSuchElementException; /* evaluate (i.e. calculate) the value of this tree */

  public abstract List<ExpTree> children(); /* return all children */

  public abstract void appendChild(ExpTree child); /* append child to this tree */

}
