# Einleitung - OOP-Test

23:00 - 23:10 => 10 min


# Aufgabe 1 - Zauberkiste

```
algorithm cistamTeAperi(k: Zauberkiste)
    if k enthält Gegenstand then
        return k.extrahiere_Inhalt
    else
        cistamTeAperi(k.extrahiere_Inhalt)
```

19:46 - 19:50 => 4 min


# Aufgabe 2 - Reise in den Objektorient

## 2.1 Zaubern

19:50 - 20:33 => 43 min

## 2.2-2.6 Fragen

23:11 - 23:15 => 4 min


# Aufgabe 3 - Orkgrunzer

## Aufgabe 3.1 Implementierung

20:33 - 21:00 => 27 min

## Aufgabe 3.2 Fragen

23:27 - 23:30 => 3 min

Die Schlüssel müssen vergleichbar sein (`equals` korrekt implementieren), damit zu einem Schlüssel auch nur ein Wert existieren kann. Wenn die Default-Implementierung von `equals` verwendet wird, gelten nur identische Objekte als gleich. Es wäre also durchaus möglich, dass z.B. zwei Schlüsselobjekten mit völlig gleichen feldern jeweils ein anderer Wert zugeordnet wirt.

# Aufgabe 4 - LinkedList

21:00 - 21:37 => 37 min

# Aufgabe 5 - NovizenFilter

21:47 - 22:13 => 26 min

# Aufgabe 6 - Generic ArrayBasilist

22:13 - 22:24 => 11 min

# Aufgabe 7

23:15 - 23:16 => 1 min

# Aufgabe 8

## Aufgabe 8.1 Zauberbeitel

22:34 - 22:43 => 9 min

## Aufgabe 8.2 Generics Collections.copy

22:43 - 22:49 => 6 min

Wieso ist der generische Typ von dest `? super T` und nicht `T` ?
Wieso ist der generische Typ von src `? extends T`?

Antwort auf beide Fragen: Auf diese Art ist die Methode am flexibelsten bezüglich ihrer Argumente. Würde man den Typ `T` statt `? super T` verwenden, könnte man z.B. eine Integer-Liste nur in eine andere Integer-Liste kopieren, nicht aber z.B. in eine Object-Liste. Anders herum sorgt das `? extends T` dafür, dass der Elementtyp der Liste aus der Kopiert wird spezifischer als T sein darf.

Was bedeutet überhaupt `? super T` und `? extends T`?

`? super T` bedeutet "entweder T oder eine Basisklasse von T" und `? extends T` bedeutet "entweder T oder eine Subklasse".

## Aufgabe 8.3 Kovarianz und Invarianz

22:49 - 22:55 => 6 min

Kovarianz

Kovarianz bedeutet, dass Ausdrücke eines Typs mit Ausdrücken eines Subtyps von diesem kompatibel sind, dass man also ein Objekt einer Oberklasse mit Objekten einer Subklasse ersetzen kann. Beispiel:

`Number x = new Integer(10);`

Hier verhält sich die Variable x kovariant.


Invarianz

Invarianz bedeutet, dass ein Ausdruck weder Kovariant noch Kontravariant (Ersetzbarkeit durch Supertypen) ist. D.h. man muss genau den gleichen Typ beibehalten. Ein Beispiel hierfür sind generische Typen in Java.

`List<Number> lst = new ArrayList<Integer>();`

Das funktioniert nicht, weil der Generische Typ invariant ist. Statt `Number` muss hier auch der exakte Typ `Integer` verwendet werden (oder man arbeitet mit Wildcards).

## Aufgabe 8.4

22:55 - 22:58 => 3 min

Generische Typen wie `List<A>` sind invariant. Die angegebene Zuweisung funktioniert nicht. Wenn sie nämlich funktionieren würde, würde durch die folgende Zuweisung die Garantie verletzt, dass eine ArrayList vom Typ `Animal` immer nur `Animal`-Objekte enthalten kann:

```
List<Object> xs = new ArrayList<Animal>();
xs.add(new Object()); // müsste gehen, da der Compiler nur den statischen Typ sieht
```

## Aufgabe 8.5

Arrays sind Kovariant. Die angegebene Zuweisung funktioniert. Diese Definition war in Java nötig, weil die Sprache früher keine Generics kannte. Damit musste man um "generische" Kopier- oder Such-Methoden zu schreiben ein Integer-Array z.B. auch als Object-Array auffassen können.

22:58 - 22:59 => 1 min


=> Insgesamt für das ganze Aufgabenblatt 191 min => 3,2 Stunden
Normale Rechnung ist Dozentenzeit * 4 => 12,8 Stunden
Wir wollten 6 Stunden => Statt mit 70% besteht man mit 35%

# sonstige notizen

- mehrere Fehler, die Studierende bemerkt haben sind nicht gefixed
- LinkedList
    + Vorgabe enthält keinen Paketordner
    + Wenn die abgabe nicht kompiliert, behält firefox den filehandle offen
- NovizenFilter
    + code-vorgaben müssen unbedingt die richtige ordnerstruktur haben!
    + Dateinamen stimmen hinten und vorne nicht
- Zauberbeutel
    + Aufgabenstellung liefert keine Info über Klassenname