package linkedlist;

import java.util.function.Function;

public class Nil<A> implements ImmutableList<A> {

    @Override
    public ImmutableList<A> cons(A elem) {
        return new Cons<A>(elem);
    }

    @Override
    public ImmutableList<A> append(A elem) {
        return new Cons<A>(elem);
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public A getAt(int idx) throws IndexOutOfBoundsException {
        throw new IndexOutOfBoundsException("cannot access linkedlist.Nil");
    }

    @Override
    public <B> ImmutableList<B> map(Function<A, B> fn) {
        return new Nil<>();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Nil)) { return false; }
        return true; // all Nils are equal ;)
    }

    @Override
    public String toString() {
        return "Nil";
    }
}
