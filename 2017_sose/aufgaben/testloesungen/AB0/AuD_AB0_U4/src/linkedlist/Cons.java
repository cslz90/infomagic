package linkedlist;

import java.text.MessageFormat;
import java.util.function.Function;

/**
 * Notizen:
 * - Vorgabe enthält keinen Paketordner
 * - Wenn die abgabe nicht kompiliert, behält firefox den filehandle offen
 *
 * Start: 21:00
 * Ende: 21:37
 */
public class Cons<A> implements ImmutableList<A> {

    A head;
    ImmutableList<A> tail;

    public Cons(A head) {
        this(head, new Nil<>());
    }
    public Cons(A head, ImmutableList<A> tail) {
        this.head = head;
        this.tail = tail;
    }

    @Override
    public ImmutableList<A> cons(A elem) {
        return new Cons<A>(elem, this);
    }

    @Override
    public ImmutableList<A> append(A elem) {
        return new Cons<A>(head, tail.append(elem));
    }

    @Override
    public int size() {
        return 1 + tail.size();
    }

    @Override
    public A getAt(int idx) throws IndexOutOfBoundsException {
        if (idx == 0) { return head; }
        return tail.getAt(idx-1);
    }

    @Override
    public <B> ImmutableList<B> map(Function<A, B> fn) {
        return tail.map(fn).cons(fn.apply(head));
    }

    @Override
    public boolean equals(Object other) {
        if(!(other instanceof Cons)) { return false; }
        Cons<A> ocons = (Cons<A>) other;
        return head.equals(ocons.head) && tail.equals(ocons.tail);
    }

    @Override
    public String toString() {
        return MessageFormat.format("Cons({0}, {1})", head, tail);
    }
}
