package oop;

import java.text.MessageFormat;

/**
 * Created by Kylar on 24.04.2017.
 */
public class Professor extends Person {
    private final String title;

    public Professor(String name, String vorname, int alter, String title) {
        super(name, vorname, alter);
        this.title = title;
    }

    public String getTitel() {
        return title;
    }

    @Override
    public boolean equals(Object other) {
        if(! super.equals(other)) return false;
        if(! (other instanceof Professor)) return false;
        Professor op = (Professor)other;
        return title.equals(op.title);
    }

    @Override
    public String toString() {
        return MessageFormat.format(
                "{0} {1} {2} ({3})",
                title,
                getVorname(),
                getName(),
                getAlter());
    }
}
