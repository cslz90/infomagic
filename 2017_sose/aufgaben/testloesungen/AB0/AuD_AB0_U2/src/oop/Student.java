package oop;

import java.text.MessageFormat;

/**
 * Created by Kylar on 24.04.2017.
 */
public class Student extends Person {
    private final int mtknr;
    public Student(String name, String vorname, int alter, int mtknr) {
        super(name, vorname, alter);
        this.mtknr = mtknr;
    }

    public int getMatrikelnr() {
        return mtknr;
    }

    @Override
    public boolean equals(Object other) {
        if(! super.equals(other)) return false;
        if(! (other instanceof Student)) return false;
        Student os = (Student)other;
        return mtknr == os.mtknr;
    }

    @Override
    public String toString() {
        return MessageFormat.format(
                "{0} {1} ({2}, MTKNR {3,number,#})",
                getVorname(),
                getName(),
                getAlter(),
                mtknr
                );
    }
}
