package oop;

import java.text.MessageFormat;

/**
 * Created by Kylar on 24.04.2017.
 *
 * Start: 19:50
 * End: 20:33
 */
public class Person implements Comparable<Person> {

    public String getVorname() {
        return vorname;
    }

    public int getAlter() {
        return alter;
    }

    public String getName() {

        return name;
    }

    private final String name;
    private final String vorname;
    private final int alter;

    public Person(String name, String vorname, int alter) {
        this.name = name;
        this.vorname = vorname;
        this.alter = alter;
    }

    @Override
    public int compareTo(Person o) {
        int c =  name.compareTo(o.name);
        if (c != 0) {
            return c;
        }
        c = vorname.compareTo(o.vorname);
        if (c != 0) {
            return c;
        }
        return alter - o.alter;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Person)) return false;
        Person op = (Person) other;
        return name.equals(op.name)
                && vorname.equals(op.vorname)
                && alter == op.alter;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0} {1} ({2})", vorname, name, alter);
    }

    public static void main(String[] args) {
        Person p = new Person("foo", "bar", 10);
        System.out.println(p.toString());
    }
}
