import java.util.Collection;

/**
 * Start: 22:34
 * Ende: 22:43
 *
 * Notizen:
 * - Aufgabenstellung liefert keine Info über Klassenname
 * Created by Kylar on 24.04.2017.
 */
public class Zauberbeutel {
    public static <E extends Comparable> E min(Collection<E> col) {
        if (col.isEmpty()) return null;
        E mine = col.iterator().next();
        for(E x : col) {
            if (x.compareTo(mine) < 0) mine = x;
        }
        return mine;
    }
    public static <E extends Comparable> E max(Collection<E> col) {
        if (col.isEmpty()) return null;
        E maxe = col.iterator().next();
        for(E x : col) {
            if (x.compareTo(maxe) > 0) maxe = x;
        }
        return maxe;
    }
}
