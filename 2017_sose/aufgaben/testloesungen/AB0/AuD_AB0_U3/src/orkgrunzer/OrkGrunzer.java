package orkgrunzer;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Kylar on 24.04.2017.
 * Start: 20:33
 * Ende: 21:00
 */
public class OrkGrunzer {
    private static Map<Character, String> dict;
    static {
        dict = new HashMap<>();
        Pattern lp = Pattern.compile("\"([A-Z0-9\\.\\,\\- ])\" => \"([\\.\\- ]+)\"");
        try {
            Files.lines(Paths.get("morse-code.txt"), StandardCharsets.UTF_8).forEach(l -> {
                System.out.println(l);
                Matcher m = lp.matcher(l);
                if(m.find()) {
                    dict.put(m.group(1).charAt(0), m.group(2));
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String encode(String s) {
        return s.chars().mapToObj(c -> dict.get(Character.toUpperCase((char)c))).collect(Collectors.joining());
    }

    public static void main(String[] args) {
        System.out.println(dict);
        System.out.println(encode("AAB"));
    }
}
