import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertTrue;

import fileio.NovizenFilter;
import org.junit.Test;

public class NovizenFilterTest {

    private static final String OUT_FILE_NAME_TEST = "neue-personen_test.csv";
    private static final String OUT_FILE_NAME = "berechtigte_novizen.csv";

    @Test
    public void processTest() throws Exception {
        NovizenFilter.processList();
        List<String> generatedList = fileToList(OUT_FILE_NAME);
        List<String> testList = fileToList(OUT_FILE_NAME_TEST);
        for (String person : testList) {
            assertTrue("Must contain " + person, generatedList.contains(person));
            generatedList.remove(person);
        }
        assertTrue(generatedList.isEmpty());
    }

    private List<String> fileToList(String filePath) {
        List<String> list = new LinkedList<>();
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(filePath), StandardCharsets.UTF_8)) {
            String line;
            while ((line = reader.readLine()) != null) {
                list.add(line);
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
        return list;
    }

}
