package fileio;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Start: 21:47
 * Ende: 22:13
 * Notizen:
 * - code-vorgaben müssen unbedingt die richtige ordnerstruktur haben!
 * Created by Kylar on 24.04.2017.
 */
public class NovizenFilter {
    private class Person {
        String name;
        int age;
        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }
    }
    public static void processList() throws IOException {
        Pattern lp = Pattern.compile("\\w+, (\\d+)");
        List<String> lines = Files.lines(Paths.get("novizen.csv"), StandardCharsets.UTF_8).filter(l -> {
            Matcher m = lp.matcher(l);
            if (m.find()) return Integer.parseInt(m.group(1)) >= 18;
            return false;
        }).collect(Collectors.toList());
        Files.write(Paths.get("berechtigte_novizen.csv"), lines, StandardCharsets.UTF_8);
    }

    public static void main(String[] args) throws IOException {
        processList();
    }
}
