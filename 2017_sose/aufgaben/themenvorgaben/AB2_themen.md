# Aufgabenblatt 2

| Ausgabe | Abgabe | Themenkomplex                                                                          |
| ------- | ------ | -------------------------------------------------------------------------------------- |
| 11.5.   | 25.5.  | Teile und Herrsche, Dynamische Programmierung, Sortieralgorithmen, Binäre Suche        |

## Inhaltliche Voraussetzungen aus der Vorlesung

* Teile- und Herrsche am Beispiel von Feudalismus (Lehenswesen)
  - Gamifiziert: Beschwöre Helferlein, die Helferlein beschwören, die Helferlein beschwören, ... (vllt. einfach Schattenklone? ;P)
  - kein konkreter Code
* Dynamische Programmierung an einem Fibonacci-ähnlichen Algorithmus
  - Gamifiziert: Hive-Mind mit anderen beschwörbaren Viechern
  - kein konkreter Code
* Sortieralgorithmen: vermutlich selection, bubble und merge
  - Gamification: Basilisten mögen es, sortiert zu werden
  - nur Pseudocode auf den Folien, vllt Selection-sort auch in java?
* Binäre Suche ist auch besprochen, Beispiele werden dankend angenommen ;)
  - auch wieder nur Pseudocode

## Aufgabenideen

### Must-haves

* gerüst für Mergesort in java vorgeben, das ausimplementiert werden muss
  - vllt mit Teil-methoden, die man auch separat testen kann (um sicher zu gehen, dass es kein ganz anderer Algo ist)
* Freitextaufgabe: mit welcher Technik könnte man Problem X lösen?
  - es dürfen auch 2 oder 3 Probleme sein
  - kurze informelle Beschreibung der Lösungsidee
  - Techniken zur Auswahl:
    - Gier
    - brute force
    - backtracking
    - teile und herrsche
    - dynamische programmierung
* Irgendein Algorithmus (z.B. Levenshtein) als Pseudocode, Frage: Welche Technik wird hier verwendet
  - Antwort muss wieder kurz erläutert werden
  - idealerweise einen dynamic-programming oder teile-und-herrsche-algorithmus nehmen
  - vllt auch mehr als nur einen algo
* AoC-Aufgabe: Ich denke mir eine Zahl zwischen 0 und 1.000.000. Welche ist das?
  - Antwort von Dozentron: Die gesuchte Zahl ist größer/kleiner/korrekt.
    + beim Test noch ein Delay von 10s einbauen. ;P
  - Ziel des Ganzen: Binäre Suche von Hand machen und verstehen, warum man sie braucht. ;P
  - bei 1.000.000 ca. 20 Fragen nötig, vllt also doch etwas kleiner?


### Nice-to-haves (eher Bonus)

* einen Sortieralgorithmus in Java implementieren mit Listener
  - der Listener wird benachrichtigt, wenn Elemente verändert werden
  - => ermöglicht zu prüfen, ob der richtige Algo implementiert wurde
* sortierte ArrayList als Datenstruktur in Java bauen
  - bei `add` und `remove` (ohne Indices) muss Sortierung beibehalten werden
  - eigene `binarySearch`-Implementierung als teil der Klasse
    + auch wieder mit Listener, um Implementierung zu testen?
* AoC-Aufgabe: irgendeinen relativ einfachen Algorithmus auf Listen, der nicht in Standard-libraries enthalten ist
