# Aufgabenblatt 3

| Ausgabe | Abgabe | Themenkomplex                                              |
| ------- | ------ | ---------------------------------------------------------- |
| 25.5.   | 8.6.   | Bäume, Binäre Suchbäume, Heaps, Breadth-First, Depth-First |

## Inhaltliche Voraussetzungen

* Leider noch keine konkrete Planung vorhanden : /

## Aufgabenideen

### Must-haves

* Irgendeine AoC-Aufgabe, die mit Bäumen oder Listen gelöst werden kann.
    - Die Aufgabe beschreibt einen Algorithmus in Textform (kein Pseudocode).
    - Ein Minimalbeispiel mit korrekter Ausgabe wird mitgeliefert.
    - Die Ausgabe die auf Dozentron gefordert wird, muss etwas mit der Arbeitsweise des Algorithmus zu tun haben, damit sicher gestellt wird dass auch dieser Algorithmus implementiert wurde (Beispiel: Wieviele Elemente wurden im 10-ten durchlauf gefunden?)
    - Das Umsetzen von Text zu Code soll Teil der Aufgabenstellung sein.
    - Inspiration: Die Aufgaben sollen wirklich ziemlich genau dem Stil von Advent of Code entsprechen: http://adventofcode.com/
* Eine Klasseninvariante beweisen (z.B. von einer einfachen Baumstruktur, vllt. ein Suchbaum?)

### Nice-to-haves

* Eigene Baumstruktur in Java implementieren mit ein paar Algorithmen, die spezifisch für Bäume sind
* Werdet kreativ! Ihr habt sowieso mehr und bessere Ideen als ich. ;)
* Nochmal eine konkrete Schleifeninvariante beweisen als Bonusaufgabe. Der Algorithmus/die Schleife darf ruhig wieder sehr einfach sein.