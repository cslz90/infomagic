# Aufgabenblatt 1

| Ausgabe | Abgabe | Themenkomplex                                                                          |
| ------- | ------ | -------------------------------------------------------------------------------------- |
| 27.4.   | 11.5.  | Pseudocode, O-Notation, Laufzeit, Korrektheitsbeweise, Brute Force, Backtracking, Gier |

## Inhaltliche Voraussetzungen aus der Vorlesung

* Pseudocode-Notation ist vorgegeben
    - Zuweisung: `:=`
    - Vergleich: `=`
    - For-Schleife: `for i := 0, 1, ..., tmp.length() - 1`
    - If: `if x then ... else if y then ... else ...`
    - Einrückung hat semantische Bedeutung
    - Kopf: `algorithm name(param1: typ1, param2: typ2, ...)`
    - mehr folgt noch
* mathematische Definition der O-Notation ist bekannt
* Tabelle mit typischen Komplexitätsklassen und Anwendungsbeispiele in denen diese Auftreten ist bekannt
* Verschiedene Laufzeitanalyseverfahren sind besprochen
    - tatsächlich
    - durchschnittlich
    - worst-case
    - amortisiert
* Schleifeninvarianten und Klasseninvarianten sind besprochen
* Ein (einziger) Korrektheitsbeweis wurde geführt
* Wenn möglich werden in der Vorlesung die Basilisten aus Woche 1&2 als Codebeispiele für Beweise etc. verwendet (d.h. am Besten keine Aufgaben mehr dazu)
* Brute Force, Backtracking und Gier sind informal ohne konkrete Algorithmen Beschrieben
    - Brute Force: Passwort knacken
    - Backtracking: Weg durch Labyrinth finden
    - Gier: Wechselgeld richtig ausgeben

## Aufgabenideen

* Pseudocode für einfachen Algorithmus vorgegeben, Lösung in bel. Programmiersprache (AoC-Aufgabe).
    - Maximum und Minimum in einem Durchlauf finden?
    - Sechstgrößtes Element finden? Top 5?
* Recherche: Welche Komplexität haben Methoden von ArrayList und LinkedList?
    - Zusätzliche Frage: Wenn ich zum finden der Stelle bei `insert` in einer LinkedList O(n) brauche, was bringt es mir dann, dass das Einfügen selbst in O(1) geht?
* GenericArrayBasilist zu doppelt verketteter Liste ausbauen
* Multiple-Choice-Test zur O-Notation
    - Ist etwas, das in O(n) ist auch in O(n²) oder O(1)?
    - In welcher Komplexitätsklasse ist ein Algorithmus, der alle Permutationen einer Liste durchlaufen muss?
    - Ein Mathematiker führt den Beweis: P = NP, was würde das bedeuten?
    - ... (ca. 20 Fragen)
* Schleifeninvarianten gegeben, Frage: Gilt diese, oder gilt sie nicht? (Moodle-Test)
* Pseudocode gegeben, Laufzeit ermitteln
* Greedy-Algorithmus für [Activity selection problem](https://en.wikipedia.org/wiki/Activity_selection_problem)
    - entweder Algorithmus selbst implementieren, oder Fragen dazu beantworten
* Irgendein Brute-Force-Programm mit Listen in Java schreiben, das zur Lösung schon mal "merklich" Zeit braucht (> 5s)
* Verständnisfragen zu Backtracking allgemein

## Ausnahmen

Die folgenden Aufgabentypen sollten (noch) keine Pflichtaufgaben werden:

* einen Korrektheitsbeweis führen
* eine eigene Schleifen- oder Klasseninvariante aufstellen
* Fragen zu konkreten Backtracking-Algorithmen