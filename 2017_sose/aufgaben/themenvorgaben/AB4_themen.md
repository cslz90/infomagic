# Aufgabenblatt 4

| Ausgabe | Abgabe | Themenkomplex                                      |
| ------- | ------ | -------------------------------------------------- |
| 7.6.    | 21.6.  | Binäre Suchbäume, BFS, DFS, IDDFS, Graphen (light) |

## Inhaltliche Voraussetzungen

* Das gleiche wie bei AB3
* Zusätzlich eine grobe Einführung in Graphen (vermutlich ohne konkrete Algorithmen)
* Gamifiziert sind Graphen übrigens ein Graph (wie ein Rudel) von Knotlingen, die sich an ihren Kantakeln festhalten. So wie Tangela bei Pokemon. :wink:

### Must-Haves

> Anmerkung: Wir übernehmen die meisten Aufgaben direkt von AB3, da ich auch inhaltlich nicht viel weiter 

* [Strukturgleichheit](https://git.thm.de/cslz90/ad-cs/issues/142) -> 1h
* [Binärer Suchbaum II](https://git.thm.de/cslz90/ad-cs/issues/133) -> 2h
* Einfachen Java-Graph implementieren mit Methode `hasCycles()` -> 2h
* [Klasseninvariante](https://git.thm.de/cslz90/ad-cs/issues/130) -> 1h

### Nice-to-Haves

* Recherche-Aufgaben zu Graphen und Bäumen
    - Ballancierte Bäume?
    - Red-Black?
    - Heaps?
    - ...
* Recherche zu P und NP
    - Was heißt eigentlich "nichtdeterministisch" in "nichtdeterministisch polynomiell"?
    - Warum ist es das gleiche ob ich sage "mit einem nichtdeterministischen Programm/Automaten in polynomieller Zeit lösbar" und "eine Lösung kann mit einem deterministischen Programm/Automaten" in polynomieller Zeit auf ihre Korrektheit geprüft werden?
    - Was gibt es für andere Klassen außer P und NP? Was ist NP-hard? Was NP-Vollständig?
* Verstehensfragen (z.B. Multiple-Choice) zu Graphen und Bäumen
