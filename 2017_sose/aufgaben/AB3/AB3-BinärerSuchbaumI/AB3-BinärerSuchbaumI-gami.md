# Binärborkige Speichereichen I / Binärer Suchbaum I

*Da Sie die Forschung der Familie der Basilisten langsam langweilt, beschließen Sie Ihr Forschungsgebiet auf eine andere, spannende Datenkreaturfamilie, die der Speichereichen, zu verlagern.*

*Speichereichen sind beliebt und deswegen weit verbreitet. Sowohl im ObjektOrient als auch in Funktionlia können und werden Sie diese majestätischen Kreaturen in freier Wildbahn antreffen. Als angehender Infomagier wollen Sie diese Datenkreatur deswegen ausgiebig studieren.*

*Sie beginnen Ihr Studium der Speichereichen mit einer besonderen Untergattung: den binärborkigen Speichereichen.
Ein Spezifikum der binärborkigen Speichereichen ist, dass eine Knotenfrucht stets entweder zu einem Blütenblattknoten führt oder er sich in genau zwei weitere Äste mit neuen Knotenfrüchten aufspaltet.
Als Erstes betrachten Sie deswegen eine binärborkigen Speichereiche, in der Sie ausschließlich Integerlinge aufbewahren können.*

*Sie erproben den Umgang mit diesen Kreaturen in diesem [Feldversuch auf Dozentron](http://dozentron.mni.thm.de/submission_tasks/70).*
