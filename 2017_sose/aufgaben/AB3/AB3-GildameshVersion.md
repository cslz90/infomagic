## Binärborkige Speichereichen I (35 %)

*Da Sie die Forschung der Familie der Basilisten langsam langweilt, beschließen Sie Ihr Forschungsgebiet auf eine andere, spannende Datenkreaturfamilie, die der Speichereichen, zu verlagern.*

*Speichereichen sind beliebt und deswegen weit verbreitet. Sowohl im ObjektOrient als auch in Funktionlia können und werden Sie diese majestätischen Kreaturen in freier Wildbahn antreffen. Als angehender Infomagier wollen Sie diese Datenkreatur deswegen ausgiebig studieren.*

*Sie beginnen Ihr Studium der Speichereichen mit einer besonderen Untergattung: den binärborkigen Speichereichen.
Ein Spezifikum der binärborkigen Speichereichen ist, dass eine Knotenfrucht stets entweder zu einem Blütenblattknoten führt oder er sich in genau zwei weitere Äste mit neuen Knotenfrüchten aufspaltet.
Als Erstes betrachten Sie deswegen eine binärborkigen Speichereiche, in der Sie ausschließlich Integerlinge aufbewahren können.*

*Sie erproben den Umgang mit diesen Kreaturen in diesem [Feldversuch auf Dozentron](http://dozentron.mni.thm.de/submission_tasks/70).*

## Hieroglyphen aus dem Altlisp'schen Reich (40 %)

### Erklärung

*Im Rahmen der Altertumslehre (häufig auch Archäologie genannt) müssen Sie Untersuchungen zu magischen Ruinen anstellen, welche überall im Objektorient und in Funktionalia zu finden sind. Sie wissen bereits aus dem theoretischen Teil der Veranstaltung, dass das Reich, welches diese magischen Ruinen erbauen lies, Altlisp'sches Reich genannt wird. Damals, so scheint es, bildeten Funktionalia und der Objektorient noch einen Superkontinent. Dies erklärt auch, warum sich die Ruinen in beiden Ländern vorfinden lassen.*
*In jeder der Ruinen sind alte Schriften zu finden, die mit Hieroglyphen vergleichbar sind.*
*Man nennt diese Schriften auch Lisp. Schaut man sich diese genauer an, so sehen sie aus, wie eine Ansammlung von polyverästelten Speichereichen.*

*Auch zu polyverästelten Speichereichen haben Sie sich Notizen während der Veranstaltung gemacht.*
*Diese spalten sich im Gegensatz zu den binärborkigen Speichereichen nicht immer nur in zwei neue Knotenfrüchte auf, sondern in beliebig viele.*
*Leider sind die polyverästelten Speichereichen mit dem Altlisp'schen Reich untergegangen.*

*Während einer Ihrer Hausaufgaben haben Sie in einer Ruine nahe Ja´va die folgende Inschrift gefunden:*

```lisp
(-
  (* (/ 15 5) 20)
  (+ 1 2 3 4 5)
  3)
```

*Im Laufe Ihres Studiums der Magie haben Sie bereits viel Erfahrung im Züchten von Kreaturen erlangt.*
*Sie haben es sich nun zur Aufgabe gemacht, auch die anmutigen polyverästelten Speichereichen wieder auferstehen zu lassen.*
*Vielleicht, so erhoffen Sie es sich, gewinnen Sie dadurch neue Einsichten zum Altlisp'schen Reich.*
*Sie haben die Inschriften bereits oberflächlich dechiffriert und dabei folgende Struktur entdeckt:*

![alt text](https://homepages.thm.de/~cslz90/kurse/ad17/static/exp-tree.png)

### Aufgabe

*Lassen Sie die polyverästelte Speichereiche wiederauferstehen und nehmen Sie dabei das folgende Skelett zu Hilfe:*

```java
package tree;

import java.util.List;
import java.util.NoSuchElementException;

public interface ExpTree {

  public abstract int evaluate() throws NoSuchElementException;  /* evaluate the value of this tree */

  public abstract List<ExpTree> children(); /* return all children */

  public abstract void appendChild(ExpTree child); /* append child to this tree */

}
```

#### Ein kleiner Test

*Um Ihre neu gezüchtete polyverästelte Speichereiche auf ihre Gesundheit zu prüfen, können Sie folgenden Check mit ihr durchgehen.*

```java
public static void main(String[] args) throws NoSuchElementException {
  ExpTree sub = new SubTree();
  ExpTree add = new AddTree();
  ExpTree div = new DivTree();
  ExpTree mul = new MulTree();

  /* (/ 15 5) */
  div.appendChild(new NumTree(15));
  div.appendChild(new NumTree(5));

  /* (* (/ 15 5) 20) */
  mul.appendChild(div);
  mul.appendChild(new NumTree(20));

  /* (+ 1 2 3 4 5) */
  add.appendChild(new NumTree(1));
  add.appendChild(new NumTree(2));
  add.appendChild(new NumTree(3));
  add.appendChild(new NumTree(4));
  add.appendChild(new NumTree(5));

  /* (- (* (/ 15 5) 20) (+ 1 2 3 4 5) 3) */
  sub.appendChild(mul);
  sub.appendChild(add);
  sub.appendChild(new NumTree(3));

  if (sub.evaluate() != 42) {
    System.err.println("Ihrer polyverästelte Speichereiche scheint es nicht gut zu gehen, Sie sollten Ihre Züchtung noch einmal genauer untersuchen!");
  } else {
    System.out.println("Ihrer polyverästelte Speichereiche scheint es auf den ersten Blick sehr gut zu gehen, allerdings ersetzt so ein einfacher Check keine ausführliche Untersuchung!");
  }
}
```

[Abgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/74)

## Gierige Zwerge, die Zweite (25 %)

*Wie weithin bekannt ist, sind Zwerge sehr, sehr, sehr goldgierig. Aus diesem Grund läuft das "Wichteln" bei Zwergen ein wenig anders ab als üblicherweise. Beim normalen "Wichteln" wird ausgelost, wer wen beschenkt. Dann bekommt jeder Teilnehmer von jedem Teilnehmer ein Geschenk. Bei den Zwergen sind die Regeln ein wenig anders. Jeder Zwerg bringt zum "Wichteln" ein Geschenk in Form einer Goldmünze mit.*

*Dann läuft das Spiel wie folgt ab:*

*Die Zwergengemeinschaft setzt sich in einen Kreis. Der Zwerg der beginnt nimmt sich alle Münzen von seinem linken Nachbarn. Dieser Nachbar scheidet* ***sofort*** *aus, wird also übersprungen. Danach kommt der nächste Zwerg im Kreis an die Reihe und nimmt von seinem linken Nachbarn die Münzen. Dieser Nachbar scheidet danach wiederum* ***sofort*** *aus.*
*Es wird so lange im Kreis weitergegangen, bis nur noch ein Zwerg übrig ist.*

### Hinweise:

- *Der nächste Nachbar ist immer der Teilnehmer, welcher als Nächstes im Uhrzeigersinn folgt (links rum).*
- *Durch die Kreisformation gibt es immer einen nächsten Nachbarn.*
- *Die Aufgabe kann mit einer ***beliebigen Programmiersprache*** *gelöst werden.*
- *Die Anzahl der Zwerge ist immer größer als 0.*
- *Überlegen sie sich eine  geeignete Datenkreatur, um den Zwergenkreis darzustellen.*

### Aufgabe
*Sie bekommen von Dozentron Anzahl der teilnehmenden Zwerge mitgeteilt und müssen nun die Nummer des glücklichen Zwerges bestimmen. Geben Sie zusätzlich Ihren Zauberspruch ab.*

[Abgabe in Dozentron](http://dozentron.mni.thm.de/input_output_tasks/73)

### Beispiel

*Es sind fünf Zwerge am Spiel beteiligt (durchnummeriert von  `1` bis `5`):*

```
  1
5   2
 4 3
```

- *Zwerg `1` nimmt die Münze von Zwerg `2`.*
- *Zwerg `2` hat keine Münzen und fällt raus/wird übergangen.*
- *Zwerg `3` nimmt die Münzen von Zwerg `4`.*
- *Zwerg `4` hat keine Münzen und fällt raus/wird übergangen.*
- *Zwerg `5` nimmt die Münzen von Zwerg `1`.*
- *weder Zwerg `1` noch Zwerg `2` hat noch Münzen. Beide werden übersprungen.*
- *Zwerg `3` nimmt die Münzen von Zwerg `5`.*
- *Zwerg `3` gewinnt das Spiel.*
