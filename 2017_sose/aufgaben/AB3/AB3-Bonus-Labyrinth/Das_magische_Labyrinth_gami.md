# Die Reise durchs magische Labyrinth

*Jeder Novize an der Thaumaturgischen Hochschule muss sich im Laufe des Kurses der Algomantik der Prüfung des Labyrinthes stellen. Dabei wird dem Novizen die Aufgabe gestellt ein magisches Labyrinth, welches für jeden Novizen unterschiedlich aufgebaut ist, zu durchqueren.*

*Dadurch, dass das Labyrinth sehr groß ist, gab es in der Vergangenheit des öfteren Verluste unter den Novizen. Auf Grund der Größe des Labyrinths irrten diese tagelang umher ohne den Ausgang zu finden und mussten von den Infomagistern gerettet werden, bevor sie verhungert sind.*

*Um diese Probleme schon im Vorfeld zu vermeiden ist jeder Novize gezwungen einen Zauber in Form einer schwebenden Suchkugel zu entwickeln, welcher Ihm im den richtigen Weg durch das Labyrinth zeigt.*

*Der Zauber verfährt dabei auf folgende Weise:*

*Die Kugel schwebt das Labyrinth ab. An jeder Abzweigung biegt die Kugel zuerst in den linken Weg ab bis sie am Ende des Weges ankommt. Sollte die Kugel am Ziel ankommen, schwebt sie auf dem kürzesten Weg zum Anfang zurück. Kommt sie in eine Sackgasse, dreht sie um und schwebt bis zur letzten Abzweigung zurück und nimmt dann den rechten Weg. Hat sie schon beide Abzweigungen erledigt, geht sie zur vorletzten Abzweigung zurück und sucht von dort aus die bisher nicht besuchten Wege ab.*

### Hinweise:

- Das Labyrinth ist als Baumstruktur vorgegeben. Jeder Knoten ist vom Typ `Crossroad`. Jedes Blatt in diesem Knoten kann einen weiteren `Crossroad`, eine Sackgasse vom Typ `IsDeadEnd` oder den Ausgang vom Typ `IsExit` enthalten.
- Die Methode zur Suche des Ausgangs heißt `searchForExit`. Diese muss überschrieben werden (Siehe Code-Beispiel am Ende der Aufgabenstellung)
- Jeder Knoten und jedes Blatt besitzen einen HashCode. Dieser kann über die Methode `toHash` abgerufen werden.
- Die Blätter eines Knoten können über die Methode `children` abgerufen werden.
- Ihre Aufgabe ist es den Baum nach dem Ausgang zu durchsuchen und wenn Sie ihn gefunden haben den Weg vom Ausgang zum Start in Form einer Liste von HashCodes zurückzuliefern. Überlegen Sie sich eine Möglichkeit den Baum zu durchsuchen und den richtigen Weg zu finden.