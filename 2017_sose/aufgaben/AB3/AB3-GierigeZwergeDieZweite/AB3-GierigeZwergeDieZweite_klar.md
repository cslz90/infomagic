## Gierige Zwerge, die Zweite

Das Spiel stellt eine besondere Form des "Wichtelns" dar:

Jeder Teilnehmer bringt ein Geschenk mit.
Alle Teilnehmer setzen sich im Kreis hin und das Spiel läuft wie folgt ab:
Der erste Teilnehmer nimmt sich das Geschenk von seinem nächsten Nachbarn.
Dieser Nachbar scheidet **sofort** aus.
Danach kommt der nächste Teilnehmer, also der Dritte, an die Reihe und nimmt von seinem nächsten Nachbarn das Geschenk.
Auch dieser Nachbar scheidet **sofort** aus.

Dies wird so lange im Kreis wiederholt, bis nur noch ein Teilnehmer übrig ist.

### Hinweise:

- Jeder Teilnehmer, der an der Reihe ist, nimmt sich **alle** Münzen seines nächsten Nachbarn.
- Der nächste Nachbar ist immer der Teilnehmer, welcher als nächstes im Uhrzeigersinn folgt.
- Die Teilnehmer sitzen im Kreis, es gibt also **immer** einen nächsten Nachbarn.
- Teilnehmer ohne Münzen dürfen nicht mehr am Spiel teilnehmen.
  Das heißt, der Teilnehmer wird entweder aus dem Kreis entfernt oder wird einfach übersprungen.
- Die Aufgabe ist **programmiersprachenunabhängig**.
  Sie bekommen von Dozentron eine Zahl geliefert (Anzahl der Teilnehmer) und müssen nur die Nummer des Teilnehmers bestimmen, der am Ende alle Münzen erhält.
  Sie müssen aber trotzdem Ihre Lösung abgeben, damit wir nachvollziehen können, wie Sie auf die Lösung gekommen sind.
- Die Teilnehmeranzahl ist immer größer als 0.
- Überlegen Sie sich eine Datenstruktur, die einen Kreis abbilden kann.
- Die Nummer #1 der Teilnehmer befindet sich immer ganz oben im Kreis.

### Beispiel

Es sind fünf Teilnehmer am Spiel beteiligt (durchnummeriert von  `1` bis `5`):
```
  1
5   2
 4 3
```

- Teilnehmer `1` nimmt die Münze von Teilnehmer `2`.
- Teilnehmer `2` hat keine Münzen und fällt raus/wird übergangen.
- Teilnehmer `3` nimmt die Münzen von Teilnehmer `4`.
- Teilnehmer `4` hat keine Münzen und fällt raus/wird übergangen.
- Teilnehmer `5` nimmt die Münzen von Teilnehmer `1`.
- Weder Teilnehmer `1` noch Teilnehmer `2` hat noch Münzen. Beide werden übersprungen.
- Teilnehmer `3` nimmt die Münzen von Teilnehmer `5`.
- Teilnehmer `3` gewinnt das Spiel.
