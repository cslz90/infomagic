## Gierige Zwerge, die Zweite (25 %)

*Wie weithin bekannt ist, sind Zwerge sehr, sehr, sehr goldgierig. Aus diesem Grund läuft das "Wichteln" bei Zwergen ein wenig anders ab als üblicherweise. Beim normalen "Wichteln" wird ausgelost, wer wen beschenkt. Dann bekommt jeder Teilnehmer von jedem Teilnehmer ein Geschenk. Bei den Zwergen sind die Regeln ein wenig anders. Jeder Zwerg bringt zum "Wichteln" ein Geschenk in Form einer Goldmünze mit.*

*Dann läuft das Spiel wie folgt ab:*

*Die Zwergengemeinschaft setzt sich in einen Kreis. Der Zwerg der beginnt nimmt sich alle Münzen von seinem linken Nachbarn. Dieser Nachbar scheidet* ***sofort*** *aus, wird also übersprungen. Danach kommt der nächste Zwerg im Kreis an die Reihe und nimmt von seinem linken Nachbarn die Münzen. Dieser Nachbar scheidet danach wiederum* ***sofort*** *aus.*
*Es wird so lange im Kreis weitergegangen, bis nur noch ein Zwerg übrig ist.*

### Hinweise:

- *Der nächste Nachbar ist immer der Teilnehmer, welcher als Nächstes im Uhrzeigersinn folgt (links rum).*
- *Durch die Kreisformation gibt es immer einen nächsten Nachbarn.*
- *Die Aufgabe kann mit einer ***beliebigen Programmiersprache*** *gelöst werden.*
- *Die Anzahl der Zwerge ist immer größer als 0.*
- *Überlegen sie sich eine  geeignete Datenkreatur, um den Zwergenkreis darzustellen.*

### Aufgabe
*Sie bekommen von Dozentron Anzahl der teilnehmenden Zwerge mitgeteilt und müssen nun die Nummer des glücklichen Zwerges bestimmen. Geben Sie zusätzlich Ihren Zauberspruch ab.*

[Abgabe in Dozentron](http://dozentron.mni.thm.de/input_output_tasks/73)

### Beispiel

*Es sind fünf Zwerge am Spiel beteiligt (durchnummeriert von  `1` bis `5`):*

```
  1
5   2
 4 3
```

- *Zwerg `1` nimmt die Münze von Zwerg `2`.*
- *Zwerg `2` hat keine Münzen und fällt raus/wird übergangen.*
- *Zwerg `3` nimmt die Münzen von Zwerg `4`.*
- *Zwerg `4` hat keine Münzen und fällt raus/wird übergangen.*
- *Zwerg `5` nimmt die Münzen von Zwerg `1`.*
- *weder Zwerg `1` noch Zwerg `2` hat noch Münzen. Beide werden übersprungen.*
- *Zwerg `3` nimmt die Münzen von Zwerg `5`.*
- *Zwerg `3` gewinnt das Spiel.*
