# Expression-Tree

## Erklärung

Expression-Trees (Ausdrucksbäume) sind, wie der Name vermuten lässt, Baumstrukturen. Diese werden Ihnen in Ihrem Studium noch öfter begegnen. Beispielsweise werden diese im Compilerbau für die Repräsentierung von Ausdrücken in einer Programmiersprache verwendet. Sei folgendes, kleines Beispiel aus der Programmiersprache LISP gegeben:

```lisp
(-
  (* (/ 15 5) 20)
  (+ 1 2 3 4 5)
  3)
```

Dieser Gesamtausdruck kann in Form einer Baumstruktur dargestellt werden. Hierbei bilden Operatoren (bspw. `-`) Elternknoten und Operanden (bspw. `3`) Kindknoten. Der Ausdrucksbaum für diesen Gesamtausdruck sieht wie folgt aus.

![alt text](exp-tree.svg)

## Aufgabe

Ihre Aufgabe ist es nun, einen eigenen Datentyp für so einen Ausdrucksbaum in Java zu implementieren. Hierzu erhalten Sie folgendes Interface `ExpTree`:

```java
package tree;

import java.util.List;
import java.util.NoSuchElementException;

public interface ExpTree {

  public abstract int evaluate() throws NoSuchElementException;  /* evaluate the value of this tree */

  public abstract List<ExpTree> children(); /* return all children */

  public abstract void appendChild(ExpTree child); /* append child to this tree */

}
```

Ihre Implementierung soll auf fünf Klassen aufgeteilt werden. So soll es die Klassen `AddTree`, `SubTree`, `MulTree` und `DivTree` geben, die jeweils analog zum Namen die Operationen Addition, Subtraktion, Multiplikation und Division auf den Kindern ausführen, wenn die Funktion `evaluate` aufgerufen wird. Beachten Sie hierbei bitte die richtige Reihenfolge (von links nach rechts) in der Sie die Operationen ausführen (`3 - 5 - 7` liefert nicht das gleiche Ergebnis wie `7 - 5 - 3`). Sollte `evaluate` auf einer Instanz der oben genannten Klassen aufgerufen werden, ohne dass diese Kindknoten besitzt, dürfen Sie eine `NoSuchElementException` werfen.

Neben den vier Klassen für die Grundrechenarten, soll es noch eine weitere Klasse `NumTree` geben, die einen konstanten Ganzzahlenwert repräsentiert. Instanzen dieser Klasse sind Blätter.
Blätter können keine Kinder annehmen (die `appendChild` Funktion macht nichts) und sie geben immer eine leere Liste in der Funktion `children` zurück.
Wird eine Instanz dieser Klasse erzeugt, soll über den Konstruktor ein Wert vom Datentyp `int` angegeben werden. Dieser Wert ist auch gleichzeitig das Ergebnis der Funktion `evaluate`.

#### Ein kleiner Test

Um Ihre Implementierung zu testen, wird der folgende Java-Quellcode vorgegeben, der den oben aufgeführten Ausdrucksbaum zusammensetzt und evaluiert:

```java
public static void main(String[] args) throws NoSuchElementException {
  ExpTree sub = new SubTree();
  ExpTree add = new AddTree();
  ExpTree div = new DivTree();
  ExpTree mul = new MulTree();

  /* (/ 15 5) */
  div.appendChild(new NumTree(15));
  div.appendChild(new NumTree(5));

  /* (* (/ 15 5) 20) */
  mul.appendChild(div);
  mul.appendChild(new NumTree(20));

  /* (+ 1 2 3 4 5) */
  add.appendChild(new NumTree(1));
  add.appendChild(new NumTree(2));
  add.appendChild(new NumTree(3));
  add.appendChild(new NumTree(4));
  add.appendChild(new NumTree(5));

  /* (- (* (/ 15 5) 20) (+ 1 2 3 4 5) 3) */
  sub.appendChild(mul);
  sub.appendChild(add);
  sub.appendChild(new NumTree(3));

  if (sub.evaluate() != 42) {
    System.err.println("Test ist leider fehlgeschlagen!");
  } else {
    System.out.println("Test war erfolgreich!");
  }
}
```

## Abgabe

[Hier](http://dozentron.mni.thm.de/submission_tasks/74) ist der Link zur Abgabe in Dozentron.
