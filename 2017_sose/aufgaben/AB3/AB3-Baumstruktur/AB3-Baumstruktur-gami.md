# Hieroglyphen aus dem Altlisp'schen Reich

## Erklärung

*Im Rahmen der Altertumslehre (häufig auch Archäologie genannt) müssen Sie Untersuchungen zu magischen Ruinen anstellen, welche überall im Objektorient und in Funktionalia zu finden sind.*
*Sie wissen bereits aus dem theoretischen Teil der Veranstaltung, dass das Reich, welches diese magischen Ruinen erbauen lies, Altlisp'sches Reich genannt wird.*
*Damals, so scheint es, bildeten Funktionalia und der Objektorient noch einen Superkontinent.*
*Dies erklärt auch, warum sich die Ruinen in beiden Ländern vorfinden lassen.*
*In jeder der Ruinen sind alte Schriften zu finden, die mit Hieroglyphen vergleichbar sind.*
*Man nennt diese Schriften auch Lisp.*
*Schaut man sich diese genauer an, so sehen sie aus, wie eine Ansammlung von polyverästelten Speichereichen.*

*Auch zu polyverästelten Speichereichen haben Sie sich Notizen während der Veranstaltung gemacht.*
*Diese spalten sich im Gegensatz zu den binärborkigen Speichereichen nicht immer nur in zwei neue Knotenfrüchte auf, sondern in beliebig viele.*
*Leider sind die polyverästelten Speichereichen mit dem Altlisp'schen Reich untergegangen.*

*Während einer Ihrer Hausaufgaben haben Sie in einer Ruine nahe Ja´va die folgende Inschrift gefunden:*

```lisp
(-
  (* (/ 15 5) 20)
  (+ 1 2 3 4 5)
  3)
```

*Im Laufe Ihres Studiums der Magie haben Sie bereits viel Erfahrung im Züchten von Kreaturen erlangt.*
*Sie haben es sich nun zur Aufgabe gemacht, auch die anmutigen polyverästelten Speichereichen wieder auferstehen zu lassen.*
*Vielleicht, so erhoffen Sie es sich, gewinnen Sie dadurch neue Einsichten zum Altlisp'schen Reich.*
*Sie haben die Inschriften bereits oberflächlich dechiffriert und dabei folgende Struktur entdeckt:*

![alt text](exp-tree.svg)

## Aufgabe

*Lassen Sie die polyverästelte Speichereiche wiederauferstehen und nehmen Sie dabei das folgende Skelett zu Hilfe:*

```java
package tree;

import java.util.List;
import java.util.NoSuchElementException;

public interface ExpTree {

  public abstract int evaluate() throws NoSuchElementException;  /* evaluate the value of this tree */

  public abstract List<ExpTree> children(); /* return all children */

  public abstract void appendChild(ExpTree child); /* append child to this tree */

}
```

#### Ein kleiner Test

*Um Ihre neu gezüchtete polyverästelte Speichereiche auf ihre Gesundheit zu prüfen, können Sie folgenden Check mit ihr durchgehen.*

```java
public static void main(String[] args) throws NoSuchElementException {
  ExpTree sub = new SubTree();
  ExpTree add = new AddTree();
  ExpTree div = new DivTree();
  ExpTree mul = new MulTree();

  /* (/ 15 5) */
  div.appendChild(new NumTree(15));
  div.appendChild(new NumTree(5));

  /* (* (/ 15 5) 20) */
  mul.appendChild(div);
  mul.appendChild(new NumTree(20));

  /* (+ 1 2 3 4 5) */
  add.appendChild(new NumTree(1));
  add.appendChild(new NumTree(2));
  add.appendChild(new NumTree(3));
  add.appendChild(new NumTree(4));
  add.appendChild(new NumTree(5));

  /* (- (* (/ 15 5) 20) (+ 1 2 3 4 5) 3) */
  sub.appendChild(mul);
  sub.appendChild(add);
  sub.appendChild(new NumTree(3));

  if (sub.evaluate() != 42) {
    System.err.println("Ihrer polyverästelte Speichereiche scheint es nicht gut zu gehen, Sie sollten Ihre Züchtung noch einmal genauer untersuchen!");
  } else {
    System.out.println("Ihrer polyverästelte Speichereiche scheint es auf den ersten Blick sehr gut zu gehen, allerdings ersetzt so ein einfacher Check keine ausführliche Untersuchung!");
  }
}
```

## Abgabe

[Hier](http://dozentron.mni.thm.de/submission_tasks/74) ist der Link zur Abgabe in Dozentron.
