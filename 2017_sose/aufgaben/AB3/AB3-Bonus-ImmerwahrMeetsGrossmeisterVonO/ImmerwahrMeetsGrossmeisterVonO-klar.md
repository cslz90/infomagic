# Immerwahr meets Großmeister von O

## Aufgabe 1: Schleifeninvariante auf Korrektheit prüfen

Folgend erhalten Sie eine Schleifeninvariante und einen Algorithmus in Pseudocode. Bitte prüfen Sie die Schleifeninvariante auf Korrektheit.

Schleifeninvariante: prod_j = a[1] \cdot a[2] \cdot ... \cdot a[j] = \prod_{k = 1}^j a[k]

#### Pseudocode:

```
algorithm partialProd ( a[1..n]: int[]):
  p[1..n] (:= Array der Länge n)
  int i := 1
  while (i <= n)
    int prod := 1
    int j := 1
    while (j <= i)
      prod := prod · a[j]
      j := j + 1
    p[i] := prod
    i := i + 1
  return p
```

## Aufgabe 2: Groß-O-Einschätzung und Optimierung

### Aufgabe 2.1:
Bitte geben Sie zum o.g. Algorithmus die Laufzeit in Groß-O-Notation an.

### Aufgabe 2.2:
Bitte formulieren Sie den Algorithmus in Pseudocode so um, dass er eine Laufzeit von O(n) aufweist.

### Musterlösung:

## Aufgabe 1
I.A.: j = 1

prod<sub>1</sub> = 1 · a[1] = a[1]

I.S.: prod<sub>j</sub> = ∏<sup>j</sup><sub>k = 1</sub> a[k] = I.V.

prod<sub>j + 1</sub> = prod<sub>j</sub> · a[j + 1]

= ( ∏<sup>j</sup><sub>k = 1</sub> a[k] ) · a[j + 1]

= ∏<sup>j+1</sup><sub>k = 1</sub> a[k]

<sub>q.e.d.</sub> :blossom:

## Aufgabe 2
### Aufgabe 2.1:
O(n²)

### Aufgabe 2.2:
```
algorithm partialProd ( a[1..n]: int[]):
  p[1..n] (:= Array der Länge n)
  p[1] = a[1]
  int i := 2
  while (i <= n)
    p[i] := p[i - 1] · a[i]
    i := i + 1
  return p
```
