# Immerwahr meets Großmeister von O


*Sie haben sich in der Vergangenheit bereits auführlich mit der magischen Pflanzenkunde und den wunderhübschen Immerwahrs beschäftigt.
Da man bekanntlich Algomantik am besten durch Praktizieren lernen kann, bieten wir Ihnen mit dieser Aufgabe die Möglichkeit Ihre Kenntnisse durch Wiederholung zu verbessern.*

## Aufgabe 1: Immer wieder diese Immerwahrs

*Versetzen Sie sich erneut in die Rolle einer Immerwahr und sehen Sie sich den folgenden Zauberspruch genau an:*

*Form des Immerwahrs:* prod_j = a[1] \cdot a[2] \cdot ... \cdot a[j] = \prod_{k = 1}^j a[k]

#### Pseudobabel:

```
algorithm partialProd ( a[1..n]: int[]):
  p[1..n] (:= Array der Länge n)
  int i := 1
  while (i <= n)
    int prod := 1
    int j := 1
    while (j <= i)
      prod := prod · a[j]
      j := j + 1
    p[i] := prod
    i := i + 1
  return p
```

## Aufgabe 2: Schrumpf - O - mat

*Wie Sie sicherlich bereits erkannt haben, ist der oben genannte Pseudobabel nicht unbedingt optimal, bzw. die Großmeister von O würden Sie direkt dafür tadeln, da das doch noch deutlich schneller geht.*

*Um Ihr Ansehen zu retten, sollen Sie nun...*

### Aufgabe 2.1:
*den o.g. in Pseudobabel erklärten Algorithmus in Groß-O-Notation einschätzen und*

### Aufgabe 2.2:
*die Großmeister besänftigen, indem Sie den Pseudobabel so umformulieren, dass er eine Laufzeit von O(n) aufweist.*

### Musterlösung:

## Aufgabe 1
I.A.: j = 1

prod<sub>1</sub> = 1 · a[1] = a[1]

I.S.: prod<sub>j</sub> = ∏<sup>j</sup><sub>k = 1</sub> a[k] = I.V.

prod<sub>j + 1</sub> = prod<sub>j</sub> · a[j + 1]

= ( ∏<sup>j</sup><sub>k = 1</sub> a[k] ) · a[j + 1]

= ∏<sup>j+1</sup><sub>k = 1</sub> a[k]

<sub>q.e.d.</sub> :blossom:

## Aufgabe 2
### Aufgabe 2.1:
O(n²)

### Aufgabe 2.2:
```
algorithm partialProd ( a[1..n]: int[]):
  p[1..n] (:= Array der Länge n)
  p[1] = a[1]
  int i := 2
  while (i <= n)
    p[i] := p[i - 1] · a[i]
    i := i + 1
  return p
```

## Abgabe

*Die Abgabe der ersten und der zweiten Aufgabe geschieht über [Moodle](https://moodle.thm.de/mod/assign/view.php?id=166378).*
