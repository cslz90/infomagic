# Aufgabenblatt 3

|Nr | Thema                         | Gamifiziert                    | Dauer in min   | XP  | %   | Ref gamifiziert  |
|---| ---                           | ---                            | ---            | --- | --- |---               |
|01 | AoC                           | Gierige Zwerge, die Zweite     | 20-40          | ??  | 25  |
|03 | Baumstruktur implementieren   | Hieroglyphen aus dem Altlisp'schen Reich  | 120 | ??  | 40  |
|04 | Binärer Suchbaum I            | Binärborkige Speichereichen I       | 120-180   | ??  | 35  |
