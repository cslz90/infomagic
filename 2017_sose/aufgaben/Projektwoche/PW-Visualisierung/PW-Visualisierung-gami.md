# Turnamentum Imaginis / Visualisierung

Nur einmal im Semester, richten sich die Zwillingsplaneten *P* und *NP* für eine Woche in einer perfekten Linie zu unserer schönen Erde aus. Die Effizienzkomplexitätswellen, die bei diesem Schauspiel freigesetzt werden, lassen nicht nur seltene Immerwahrs im Dunkel erblühen, sondern zaubern auch bunte Lichter an den Himmel.

![Nordlichter](https://homepages.thm.de/~cslz90/kurse/ad17/static/northern-lights.png)

Da die meisten Infomagier sowieso nachtaktiv sind, veranstaltet die Thaumaturgische Hochschule für Magie in dieser Woche traditionsgemäß das *Turnamentum Imaginins* - ein Wettbewerb bei dem die verschiedenen Magiergilden und Häuser gegeneinander antreten, um zu bestimmen, wer mit den bunten Lichtern die schönsten algomantischen Formeln oder Datenkreaturen an den Nachthimmel zaubern kann.

Wenn Sie auch in diesem Semester an dem Wettbewerb teilnehmen wollen, dann lesen Sie sich die [Teilnahmeregeln](https://homepages.thm.de/~cslz90/kurse/ad17/static/PW-Visualisierung-klar.html) bitte genau durch.

[Abgabe in Moodle](https://moodle.thm.de/mod/assign/view.php?id=165874)