# Turnamentum Imaginis / Visualisierung

Freiwillige Bonusaufgabe für die Projektwoche

Algorithmen und Datenstrukturen, Sommersemester 2017

Christopher Schölzel

<!-- Erfahrungspunkte: 160 XP -->

## Aufgabenstellung

Für die Projektwoche dürfen Sie freiwillig in einer Gruppe von 2-10 Studierenden eine Visualisierung für eine Datenstruktur oder einen Algorithmus erstellen. Dabei sollen Sie schrittweise grafisch darstellen, wie die Operationen der Datenstruktur bzw. der ausgewählte Algorithmus funktioniert. Die Visualisierung soll es einem interessierten Informatikstudenten oder einer Informatikstudentin des ersten Semesters ermöglichen, die Funktionsweise des gezeigten Konzepts zu verstehen.

Ob die Visualisierung interaktiv ist oder nicht, ob sie in einer Animation abläuft oder die einzelnen Schritte selbst durch das Klicken durch eine Bildergalerie nachvollzogen werden können, ist dabei ganz Ihnen überlassen. Sie dürfen sowohl bereits besprochene Konzepte visualisieren wie auch noch nicht besprochene Algorithmen oder Datenstrukturen. In letzterem Fall sollten Sie aber zusätzlich einen erklärenden Text mitliefern, der beschreibt, worum es sich bei dem gezeigten Konzept überhaupt handelt und wofür man es verwendet.

Für dieses Projekt müssen Sie mit [Gitlab](https://git.thm.de) arbeiten. Dabei handelt es sich um eine Projektverwaltungsplattform, die das Versionsverwaltungssystem *git* verwendet. Die meisten von Ihnen werden diese Tools schon aus PIS kennen. Falls nicht, hat die [Hilfeseite](https://git.thm.de/help/gitlab-basics/README.md) von Gitlab alle Informationen, die Sie brauchen. Denken Sie bitte auf jeden Fall daran, dass wir ihre Arbeitsweise nur nachvollziehen können, wenn sie *viele kleine Commits* machen. Fügen Sie bitte unsere Tutorengruppe (@ad_2017_ss) in der Rolle des *Reporter* zu ihrem Repository hinzu, damit wir ihre Arbeit bewerten können.

## Formalia

* Die "Abgabe" auf Gildamesh erfolgt einzeln. Jedes Gruppenmitglied muss selbst den Abgabebutton klicken.
* Eine Gruppe darf auch mehrere Datenstrukturen/Algorithmen visualisieren.
* Sie dürfen beliebige Programmiersprachen und Bibliotheken verwenden. (s.u.)
* Die Gruppengröße ist beschränkt auf 2-10 Studierende.
* Die Verwendung von [git](https://git.thm.de) ist Pflicht, damit wir nachvollziehen können, wer wie viel an dem Projekt gearbeitet hat.
* Das Projekt sollte mindestens etwa 10 Stunden Arbeitszeit pro Student oder Studentin entsprechen. Projekte, die vermuten lassen, dass deutlich weniger Zeit und Sorgfalt investiert wurde, können aus Fairnessgründen nicht akzeptiert werden.
* Aus Fairnessgründen sind auf dem Turnamentum Imaginis nur Standard-Zauberstäbe erlaubt. Der verbesserte Zauberstab kann also nicht dazu verwendet werden, doppelte Erfahrungspunkte zu erhalten.
* Man darf nur bei einer Gruppe mitarbeiten.
* Thematisch ist es schön, wenn die Gruppen auch in Gildamesh einer Gilde entsprechen. Das ist aber nicht zwingend nötig. Was zählt sind die Mitglieder des Gitlab-Repositories.
* Die beste Visualisierung wird in der Vorlesung präsentiert und die Gruppe erhält einen zusätzlichen XP-Bonus. Ob Sie selbst die Präsentation übernehmen oder mich ihr Projekt vorstellen lassen können sie selbst entscheiden.
* Eine Visualisierung muss die *Funktionsweise* der Datenstruktur bzw. des Algorithmus zeigen, nicht nur das Ergebnis.
* Das Projekt muss eine selbst geschriebene Implementierung der Datenstruktur bzw. des Algorithmus enthalten.

## Unterlagen und Hilfestellung

### Listener-Pattern

Wenn Sie die "Innereien" einer Datenstruktur oder eines Algorithmus beobachten wollen, bietet es sich an, im Code das sogenannte *Listener-Pattern* zu verwenden. Dabei wird ein *Listener* an die zu inspizierenden Methoden übergeben, der dann innerhalb der Methode jedes Mal aufgerufen wird, wenn etwas Interessantes passiert.

In Java würde eine Fakultätsmethode mit Listener, der jeweils auf die Teilergebnisse reagiert, zum Beispiel wie folgt aussehen:

```java
public interface FakListener {
    void reportResult(int i, long res);
}
public long fak(int n, FakListener l) {
    long res = 1;
    for(int i=2; i <= n; i++) {
        res *= i;
        l.reportResult(i, res);
    }
    return res;
}
```

Ein Aufruf von `fak` könnte dann (in Java 8 Syntax) wie folgt aussehen: 

```java
fak(10, (i, res) -> System.out.println(i+": "+res))`.
```

> Anmerkung: Die Fakultät zählt natürlich nicht als Algorithmus, mit dem man diese Aufgabe bestehen kann - es sei denn natürlich Sie bauen mit ihrer Gruppe eine Visualisierung für sehr viele kleine Algorithmen und `fak` ist ein kleiner Teil des Gesamtprojektes.

### Beispiele

Ich habe für Sie ein paar Beispiele für die Visualisierung von Algorithmen und Datenstrukturen gesammelt, damit Sie eine Idee bekommen, wie so etwas aussehen könnte:

* Den Code für meine eigene [Visualisierung des 4-Damen-Problems](https://git.thm.de/cslz90/4queens) habe ich Ihnen auf Gitlab zur Verfügung gestellt.
* [15 Sorting Algorithms in 6 Minutes](https://www.youtube.com/watch?v=kPRA0W1kECg) ist ein sehr schönes Beispiel, das sowohl Bilder als auch Sound nutzt, um Sortieralgorithmen "erfahrbar" zu machen.
* [A* Visualisierung](https://www.youtube.com/watch?v=19h1g22hby8)
* [Quicksort](https://www.youtube.com/watch?v=aXXWXz5rF64) (Nein, sie müssen keine 3D-Roboter animieren. Ein paar einfache farbige Rechtecke würden den gleichen Effekt erzielen.)
* [Visualisierung des Genpools eines Genetischen Algorithmus](https://www.youtube.com/watch?v=_4Df9w1A-bA) (vermutlich mit *matplotlib*)
* [Groß-O als Jupyter Notebook](https://github.com/jmportilla/Python-for-Algorithms--Data-Structures--and-Interviews/blob/master/Algorithm%20Analysis%20and%20Big%20O/Big%20O%20Notation.ipynb)
* [visualgo.net](https://visualgo.net/en) schießt den Vogel ziemlich ab mit ihren Visualisierungen. So kompliziert muss es bei Ihnen nicht werden. :wink:
* [David Galles von der University of San Francisco](https://www.cs.usfca.edu/~galles/visualization/Algorithms.html) hat auch erklärende Visualisierungen zu vielen Algorithmen auf seiner Webseite.



### Sprachen und Bibliotheken zur Visualisierung

Sie sind in der Wahl der Programmiersprache und des Tools bzw. der Bibliothek für die Visualisierung völlig frei. Aus eigener Erfahrung kann ich Ihnen jedoch die folgenden Werkzeuge und Sprachen empfehlen:

* [BufferedImage](https://docs.oracle.com/javase/7/docs/api/java/awt/image/BufferedImage.html) ist eine Klasse der Java-API, die es erlaubt mit einem [Graphics2D](https://docs.oracle.com/javase/7/docs/api/java/awt/Graphics2D.html)-Objekt auf ein Bild zu zeichnen. Damit lassen sich sehr schnell aus Java-Code Bilder erzeugen.
* [JavaFX](https://en.wikipedia.org/wiki/JavaFX) ist ein modernes Toolkit für grafische Benutzeroberflächen in Java. Es enthält viele Klassen und Methoden, die bei der Erstellung von Bildern oder auch interaktiven Animationen nützlich sind.
* [Python](https://www.python.org/) bietet mit dem Paket [matplotlib](http://matplotlib.org/) eine exzellente Bibliothek für das Plotten von Daten, die auch Animationen beherrscht.
* [Project Jupyter](jupyter.org) erlaubt es sogenannte *Jupyter Notebooks* zu erstellen, mit denen man wunderbar Algorithmen erklären kann, da sie es erlauben, ausführbaren Code und formatierte Textpassagen in einem Dokument zu mischen. *Jupyter Notebooks* gibt es zum Beispiel für *Python*, *R* und *Julia*.
* [R](https://www.r-project.org/) ist eine Sprache für statistische Auswertungen. Sie hat unter anderem auch eine sehr ausgeklügelte Plotting-Bibliothek. Mit [Shiny](https://shiny.rstudio.com/) lassen sich außerdem diese Plots mit erschreckend wenig Aufwand als interaktive Webseiten darstellen.
* [Graphviz](http://www.graphviz.org/) ist ein sehr einfaches Konsolentool zur Darstellung von Bäumen und Graphen.
* [Processing](https://processing.org/) ist eine Sprache, die die gleiche Syntax wie Java verwendet, aber für interaktive grafische Simulationen entwickelt wurde, die auch mit [Processing.js](http://processingjs.org/) im Browser angezeigt werden können.
* [Tikz](http://www.texample.net/tikz/) ist eine Variante für die ganz Verrückten. :wink: Es handelt sich dabei um eine Bibliothek für LaTeX, mit der man ebenfalls Bilder, Bäume und Graphen zeichnen kann. Die Beispiele auf meinen Folien sind mit Tikz und dem Paket [Beamer](https://www.sharelatex.com/learn/Beamer) erstellt.