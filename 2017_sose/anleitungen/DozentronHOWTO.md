# Wie erstelle ich eine Abgabe in Dozentron?

:exclamation: :exclamation: Bitte beachten: :exclamation: :exclamation:
- Alle .java-Dateien müssen UTF-8 kodiert sein.
    Windows-Benutzer müssen besonders darauf achten


1. [dozentron.mni.thm.de](dozentron.mni.thm.de) öffnen (Nur im THM Netz verfügbar oder durch das VPN der THM).

2. Auf der Home-Seite das entsprechende Übungsblatt auswählen
   ![](img/dozentron-home.jpeg)

3. Wähle eine der beiden Aufgaben auf der rechten Seite

4. Titel, Text, Deadline etc. angeben

5. Der Quellcode für die lauffähige Musterlösung und den dazugehörigen Test sollte wie folgt strukturiert sein:

   1. Der Quellcode der Musterlösung kommt ins Unterverzeichnis `"src/packagename"`

   2. Der Quellcode der Tests kommt ins Unterverzeichnis `"test/packagename"`

   3. Der komplette Quellcode kann über den folgenden Konsolenbefehl unter Linux (Pfade müssen entsprechend angepasst werden) komplett kompiliert werden:

      `javac -cp /usr/share/java/junit4.jar:/usr/share/java/hamcrest-core.jar **/**/*.java`

6. Quellcode als *.jar-Archiv* hochladen, dabei ist folgendes beachten:
-  ​:exclamation: Bei Test **NUR** die **Test-Dateien** hochladen (Den Inhalts des Ordners `test` in ein *jar-Archiv* packen)

-  ❗ Bei Stub die **Musterlösung** hochladen (Den Inhalt des Ordners `src` in ein *jar-Archiv* packen)

-  ❗ Bei Aufgaben, in welcher eine Datei **eingelesen** werden soll, muss diese im Stammverzeichnis beider *jar-Archive* mitgeliefert werden. 

   ![](img/dozentron-task.jpeg)

7. Nachdem die Aufgabe erfolgreich in Dozentron eingefügt wurde:
- Aufgabe in Gildamesh verlinken
- Eventuelle Vorgaben in Moodle hochladen
