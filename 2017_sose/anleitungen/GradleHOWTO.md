# Quellcodeorganisation, -übersetzung, -paketierung

> :exclamation: :exclamation: :exclamation:
Empfohlen wird eine gradle version > 2.0 :exclamation:

## Organisation
Quellcode möglicher Musterlösungen landet im `aufgaben/entwurf/code` Ordner.
Dieser Ordner **ist ein** [gradle](https://gradle.org/) Projekt.
Er enthält verschiedene Subprojekte (für jedes Thema) in Unterordnern.

## Buildtool & Übersetzung
Der Quellcode ist als gradle Projekt organisiert.
Um ihn zu verwenden muss gradle installiert werden oder der [gradle wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html#sec:using_wrapper) wird verwendet.
Der Wrapper kann über die vorhandenen Skripte (`gradlew`,`gradlw.bat`) verwendet werden.
Der Wrapper kümmert sich um die Installation von gradle in der richtigen version.
Man folge dieser [Anleitung](https://gradle.org/install).
Die **allgemeine** Builddefinition steht in der `build.gradle` Datei im `code` Ordner.
Diese **gilt für alle** Subprojekte.
Subprojekte werden in der `settings.gradle` Datei registriert
(siehe auch [gradle doc](https://docs.gradle.org/current/userguide/intro_multi_project_builds.html)).

## Neue Projekte erstellen
Um ein neues Subprojekt zu erstellen gibt es einen gradle task:
```sh
$ gradle newProject -Ppname=[Projektname]
```
Dieser erstellt einen neuen Unterordner und registriert diesen als Subprojekt.
Quellcode wird in 2 Unterordnern verwaltet:
- `src` enthält die Quelldateien
- `test` enthält die Testdateien

## Common workflow
Wenn man eine Aufgabe erstellt hat und nun eine Musterlösung erstellen möchte, kann man wie
folgt vorgehen:

1. Neues Subprojekt für das Thema der Aufgabe erstellen (z.B.: `oop`)
  ```sh
  $ gradle newProject -Ppname=oop
  ```
  Danach gibt es im `code` Ordner einen neuen Unterordner `oop`.
  Dieser enthält 2 Unterordner `src` und `test`.
2. OPTIONAL: Projekt in IDE (Eclipse, IntelliJ) importieren
3. Quellcode der Musterlösung in `src` erstellen
4. Tests der Musterlösung in `test` erstellen
5. Tests mittels `gradle test` ausführen
6. Musterlösung und Tests für Dozentron paketieren:
  ```sh
  $ gradle packageSolution
  $ gradle packageTests
  ```
6. Resultierende Archive aus dem `build/libs` Ordner in Dozentron hochladen

## Befehlsreferenz
Alle Befehle werden wie folgt ausgeführt:
```sh
$ gradle [Befehl]
```

| Befehl | Beschreibung |
|---|---|
| tasks | Listet alle verfügbaren Tasks mit Beschreibung |
| projects | Listet alle Subprojekte |
| clean | Löscht den `build` Ordner |
| compileJava | Kompiliert die `main` java Dateien |
| test | Führt alle Tests aus |
| test --tests [TESTNAME] | Führt den Test mit dem Namen `TESTNAME` aus.  `TESTNAME` ist ein *full-qualified-Testname* |
| newProject -Ppname=[Name] | Erstellt ein neues Subprojekt mit dem Namen `Name` |
| packageSolution | Erstellt eine jar mit der Musterlösung und zugehörigen Tests. Diese ist passend für Dozentron´s `Stub`. |
| packageTests | Erstellt eine jar, die nur die Tests enthält. Diese ist passend für Dozentron´s `Test`. |
| dozentron | Führt `packageSolution` & `packageTests` aus |
