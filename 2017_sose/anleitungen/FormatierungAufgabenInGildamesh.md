# Formatierungsrichtlinie
## Warum?
 - Damit das Übertragen von Markdown oder in Gildamesh, Moodle und Dozentron einfach und schnell geht.
## Dateinamen:
- Gamifizierter Text kommt in "Arbeitsblattkennung"-"Thema"-"Aufgabentitel"-gam
- Klartext kommt in "Arbeitsblattkennung"-"Thema"-"Aufgabentitel"-klar
## Wie soll eine Aufgabe in Gildamesh formatiert sein:
 - gamifizierter und Klartext in getrennte Dateien.
 - gamifizierter Text kursiv: *ich bin gamifiziert*.
 - gamifizierter Text als Appetitmacher für Gildamesh
 - Klarer Aufgabentext (evt. auch gamifizierter, nur wenn es sinnvoll ist) für Moodle oder Dozentron
 - ist eine Aufgabe zu Teilen in Moodle und Dozentron eine  kleine gamifizierte Überleitung über dem Link (z.B: AB0 Aufgabe 2 -> der moodle-part)
 - beim Klartext die Unteraufgaben in einer eindeutigen Reihenfolge.
 - Code in '  ' ist erlaubt
 - kursiv zum Worte hervorheben ist nicht erlaubt
 - Überschriften
  - Titel h1 #
  - Aufgabentitel h2 ##
  - Unteraufgaben h3 ###

#################################
# generelle Blaupause

Titel: Aufgabenblatt X

in der Describtion:

## Inhaltsverzeichnis
* Aufgabentitel
* "
* "

## Aufgabe 1: Aufgabentitel
### Aufgabe 1.1
*Ich bin ein kursiver, gamifizierter Text*

[Aufgabe in Moodle/Dozentron](https://moodle.thm.de/mod/assign/view.php?id=159708)
-> Anmerkung: Moodlelink, oder Dozentron

### Aufgabe 1.2
*Ich bin ein kursiver, gamifizierter Text*


###################################

## Für Dozentron/Moodle:
### Aufgabe 1.1
lalalalalala ich bin Klartext und komme in Moodle oder Dozentron und beinhalte vielleicht Hinweise und Tipps
### Aufgabe 1.2
lalalalalala
