## Glossar der Algomantik und Datenkreaturen


### Lehre
- ***Adept***
    - Tutor
- ***Großmeister***
    - Professor
- ***Infomagier***
    - Programmier/Informatiker
- ***Infomagister***
    - Dozent der Vorlesung
- ***Novize***
    - Student
- ***Thaumaturgischen Hochschule der Magie***
    - Technische Hochschule Mittelhessen

### Datenstrukturen
- ***Array-Basilisten***
    - Array-Liste / ArrayList (java)
    - *Array-Basilisten sind schlangenähnliche Wesen, mit mehreren Köpfen und Mägen um Elemente wie Integerlinge oder Stringnattern zu fressen.*
- ***Basilist***
    - Liste als abstrakter Datentyp
- ***Baumhydra***
    - Datenstruktur Baum
- ***Datenkreatur***
    - Datenstruktur
- ***Graph von Knotlingen***
    - Graph
- ***Kantakeln***
    - Kanten innerhalb eines Graphen
    - *Knotlinge halten sich Mithilfe ihrer Kantakeln gegenseitig fest und bilden so ein Geflecht aus Knotlingen, auch bekannt als Graphen von Knotlingen.*
- ***Linked-Basilisten***
    - Verkettete Liste / LinkedList (java)
- ***Nil***
    - Nil (leere Liste / das Null-Element(java))
    - *ein Verketteter Basilist im Larvenstatium. Kann erst wenn es ausgewachsen ist, Elemente in seinen Magen aufzunehmen.*


### Datentypen
- ***Integerling***
    - Integer oder int
- ***Stringnatter***
    - String


### Algorithmik
- ***Ariadne, Herrin des Labyrinths*** :princess:
    - Sinnbild für Backtracking
    - *Prinzessin, welche gerne durch Labyrinthe wandert und die Zeit zurückspulen (temporales Backtracking) kann, wenn sie sich verirrt hat. Manche behaupten, sie hätte die Algomantik begründet.*
- ***Algomantik***
    - Algorithmik
- ***Bartleby der Schreiber*** :man:
    - Sinnbild für Teile und Herrsche
    - *Schreibtischmagier aus Ja'va und ein hohes Tier bei den Algomantikern. Hasst es Dinge selbst zu erledigen, liebt es aber zu delegieren.*
- ***Brute Force***
    - Brute Force
    - *Zaubertechnik, bei der jedes Problem in Standardgegner (siehe Brute) verwandelt um sie dann einer nach dem anderen zu besiegen.*
- ***Brute***
    - Ein Element, welches einen Arbeitsschritt entspricht. Z.B. ein Element einer Liste die durchsucht wird.
    - *Brutes stellen sich gerne in einer Reihe auf und sind doch Einzelkämpfer.*
- ***Die Immerwahrs***
    - Invariante
    - *Magische Blumen, welche Klassen- und/oder Schleifeninvarianten darstellt und verblüht, wenn die Invariante nicht mehr gültig ist.*
- ***Die Weisen von O***
    - O-Notation
    - *Gelehrte der Zeit, welche die Schreibweise von O erfunden haben.*
- ***Dolores*** :older_woman:
    - Sinnbild für Dynamische Programmierung
    - *Alte, weise Druidin, die Insekten verzaubert und somit klüger macht.
- ***Dynamische Programmierung***
    - Dynamische Programmierung
    - *Dolores :older_woman: Insekten besitzen ein Schwarmgedächnis, so wissen alle Insekten, was andere Insekten machen. Die Drohnen schwärmen gemeinsam aus und fliegen zu einem Problem, wenn das Problem gelöst ist, bleibt ein Insekt zurück. Wenn es ungelöste Teilprobleme gibt, wandert der Schwarm von einem Teilproblem zum nächsten und löst es. Irgendwann weiß der Schwarm jede Lösung von jedem Teilproblem und kann das komplette Problem lösen.*
- ***Gier***
    - Gier
    - *Greedy der Runenschmied nutzt seine Wünschelrute um Schätze zu finden. Die Rute zeigt immer auf den Schatz und an Abzweigungen läuft Greedy immer in Richtung des Schatzes. Ist dies nicht möglich bricht er ab.*
- ***Mana***
    - Speicherverbrauch
    - *Der Manaverbrauch steigt mit der Menge der Informationen, die der Zauber aktuell verarbeitet.*
- ***Orkschame Brutus*** :japanese_ogre:
    - Sinnbild für Brute Force
    - *Ein Orkischer Schamane aus grauer Vorzeit. Angeblich der Begründer der Algomantik*
- ***Pseudobabbel***
    - siehe *Universalhieroglyphen*
- ***Runenschmied Greedy*** :construction_worker:
    - Sinnbild für Gier
    - *Der zwergischer Runenschmied Greedy :construction_worker: kann Zauber auf Gegenstände übertragen, besitzt einer magische Wünschelrute und ist gutmütig, solange man nicht an sein Gold will.*
- ***Teile und Herrsche***
    - Teile und Herrsche
    - *Bartleby :man: delegiert seine Arbeit an mindesten zwei seiner untergebenen Schreibtischmagier. Diese delegieren die erhaltene Arbeit wiederum an zwei untergebene, das läuft solange bis es die Novizen erwischt. Die erarbeiteten Lösungen werden wiederum vom nächsthöheren Schreibtischmagier zusammen gefasst und zurück geschickt. Am Ende prahlt :man: mit dem vollständigen Ergebnis.*
- ***Temporales Backtracking***
    - Backtracking
    - *Ariadne :princess: untersucht jeden Winkel ihrer Umgebung, bei einem Problem (Sackgasse, Minotaurus, ...) wird die Zeit zurückgedreht, bis sie eine unerforschte Abzweigung findet.*
- ***Universalhieroglyphen***
    - Pseudocode
- ***Zauber***
    - Algorithmus oder Methode
- ***Zaubergesten***
    - Laufzeit
- ***Zauberspruch***
    - siehe *Zauber*

### Völker
- ***Elfen***
    - *Diese nahe zu perfekten Elfen leben bevorzugt auf Funktionlia. Hartnäckige Gerüchte besagten, dass es einige dicke Elfen gibt, nur hat sie noch niemand, noch lebender, gesehen.*
- ***Orks***
    - *Ein Volk das sich meistens nur in Grunzlauten verständigt, Magie nicht unbedingt schätzt und recht vergesslich ist.*
- ***Zwerge***
    - *Lange Bärte, Gold und Ehre sind diesem grummeligen, gierigen Volk das wichtigste.*


### Länder und Kontinente

- ***Funktionlia***
    - *Der westliche Nachbarkontinent des Objektorient, auf dem es keine Hintergedanken und Finten gibt.*
- ***Haskellien***
    - *Ein Land in Funktionalia. Die dort lebende Infomagier bevorzugen die funktionalen Pradikmen und sprechen Haskall.*
- ***Ja'va***
    - *Eine Halbinsel im ObjektOrient auf der Bürokratie als Volkssport gilt und Java gesprochen wird.*
- ***ObjektOrient***
    - *Ein Kontinent im Osten der Welt, auf dem alles als Objekt gilt*
