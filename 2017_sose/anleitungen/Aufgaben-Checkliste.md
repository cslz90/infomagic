# Checkliste: Aufgaben erstellen

## Aufgabe kreieren

* Das Thema ist bereits vorgegeben
   * bestensfalls ist die Aufgabe schon vorgegeben und muss nur ausgearbeitet werden
   * falls nicht, gibt es folgende Dinge zu überlegen bzw. tun:
      * Welche Fähigkeiten soll die Aufgabe vermitteln?
      * Welche Vorkenntnisse können vorausgesetzt werden?
      * Brainstorming zur Aufgabenidee
      * (ausführlicher ist es in Christophers Aufgabentutorial)
* Formulieren der Aufgabe
   * Gamifizierter Text: 
      * in eine Datei mit Namen "Arbeitsblattkennung"-"Thema"-"Aufgabentitel"-gam
      * in Ordner aufgaben/entwurf/ab-"Nummer"
      * soll später in Gildamesh als Appetitmacher dienen
      * gamifizierter Text kursiv: *ich bin gamifiziert* (sonst keine Worte kursiv setzen!)
      * Code in '  '
      * Überschriften
         * Titel h1 #
         * Aufgabentitel h2 ##
         * Unteraufgaben h3 ###
      * Platzhalter für Links lassen
      * bei mehrere Links muss über jedem Link ein Text stehen
   * Klartext:
      * in eine Datei mit Namen "Arbeitsblattkennung"-"Thema"-"Aufgabentitel"-klar
      * in Ordner aufgaben/entwurf/ab-"Nummer"
      * dient in Dozentron oder Moodle als klare Arbeitsanweisung
      * Muss deutlich machen, was exakt von den Studis verlangt ist (bei Progammieraufgaben muss sie alles beinhalten, was man zum erfolgreichen Abgeben braucht, etwa vorgegebene Namen etc.)
   * evtl. Musterlösung & JUnit-Tests erstellen
      * in Ordner aufgaben/muster/ab-"Nummer"/"Arbeitsblattkennung"-"Thema"-"Aufgabentitel"

## Aufgabe testen

* Texte untersuchen nach: 
   * Rechtschreibung & Grammatik
   * Korrektheit der Gamifizierungsmetaphern
   * Korrekte Formatierung (sodass Gildamesh keinen Murks macht)
   * Sie die zwei Texte so geschrieben, dass man sie jeweils einfach in die Tools copy-pasten kann?
   * Weiß der Student genau, was er zu tun hat?
* Musterlösung & Test:
   * Ist die Musterlösung korrekt?
   * Deckt der Test alle nötigen Testfälle ab?

## Aufgabe übertragen

* Übertrage die Klartextaufgabe in Dozentron / Moodle
* Speicher den Link in der gamifizierten Aufgabe

* Teste: Lässt sich eine Lösung abgeben?
* Ist die Bewertung (wenn eine automatische Bewertung stattfindet) korrekt?
