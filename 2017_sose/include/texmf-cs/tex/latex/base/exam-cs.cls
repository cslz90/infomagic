\def\fileversion{1.0}
\def\filedate{2015/12/08}

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{exam-cs}[\filedate\space Version \fileversion\space by
  Christopher Schölzel]

% documentclass based on article
\LoadClass[a4paper]{article}

\RequirePackage[utf8]{inputenc}
\RequirePackage{fancyhdr}
\RequirePackage[table]{xcolor}
\RequirePackage{graphicx}
\RequirePackage{amsmath}
\RequirePackage[ngerman]{babel}
\RequirePackage{ulem} % for \uline
\RequirePackage[framemethod=TikZ]{mdframed} %for frames with rounded corners
\RequirePackage{multicol} %for multicols environment
\RequirePackage{listings} %listings environments with syntax higlighting
\RequirePackage{parskip} %paragraphts w/o indent and with empty line in between
\RequirePackage[a4paper,inner=3cm,outer=3cm,top=3cm,bottom=3cm]{geometry} %to change geometry
\RequirePackage{lastpage} % for LastPage reference
\RequirePackage{caption} % allow line breaks in captions
\RequirePackage[hidelinks]{hyperref}
\RequirePackage[explicit]{titlesec} % for custom section titles (explicit to be able to give title with #1)
\RequirePackage{titling} % for \theauthor command

\renewcommand{\emph}[1]{\textit{#1}}
\newcommand{\term}[1]{\textit{#1}}
\newcommand{\clue}[1]{Hinweis: \textit{#1}}
\newcommand{\stress}[1]{\textbf{#1}}
\newcommand{\code}[1]{\textnormal{\lstinline|#1|}}
\newcommand{\enquote}[1]{\glqq{}#1\grqq{}}
\newcommand{\tabvspace}[1]{\parbox[t][#1][t]{0cm}{}}

% command for section image that can be changed to indicate type of exercise
% TODO switch to new style:
%\newcommand{\secimg}[1]{\gdef\insertsecimg{#1}}
\newcommand{\secimg}{}

% defines own title format which starts with the text "Aufgabe # --" and contains an optional section image
% \begin{section*} can still be used for normal sections
\newcommand{\secPoints}{0}
\titleformat{\section}
{\normalfont\large\bfseries}
{Aufgabe \thesection \hspace{1mm} -- #1 (\secPoints{} Punkte)}
{1mm}
{}

\titleformat{\subsection}
{\normalfont\normalsize\bfseries}
{\thesubsection}
{3mm}
{}

% TODO add global setting to hide or view solutions
% solution environment
\newenvironment{loesung}{
\begin{mdframed}[roundcorner=10pt,backgroundcolor=lightgreen]
\textcolor{red}{\bf Lösung}: 
}
{
\end{mdframed}
}

% clue environment
\newenvironment{hinweis}{
\begin{mdframed}[roundcorner=10pt,backgroundcolor=lightgray]
\it Hinweis: 
}
{
\end{mdframed}
}

\definecolor{THMgreen}{HTML}{80ba24}
\definecolor{lightblue}{rgb}{0.9,0.9,1}
\definecolor{lightgreen}{rgb}{0.9,1,0.9}
\definecolor{lightgray}{rgb}{0.95,0.95,0.95}

\definecolor{grey}{rgb}{0.4, 0.4, 0.4}
\definecolor{darkblue}{rgb}{0.43, 0.55, 0.85} % alter Farbwert, n
\definecolor{brightblue}{rgb}{0.16, 0.37, 1.0}
\definecolor{darkgreen}{rgb}{0.3, 0.62, 0.3}%  alter Farbwert, n
\definecolor{darkmagenta}{rgb}{0.68, 0.02, 0.58}


% sets logo
\newcommand{\logo}[1]{\gdef\insertlogo{#1}}
\logo{img/MNI-Logo.pdf}

% sets lecture name
\newcommand{\lecture}[1]{\gdef\thelecture{#1}}
\lecture{}

% hotfix for titlesec redefining \thetitle
\let\oldtitle\title
\gdef\mytitle{}
\renewcommand{\title}[1]{\oldtitle{#1}\gdef\mytitle{#1}}

\setlength{\headheight}{41pt}
\pagestyle{fancy}
% TODO allow to set logo heiht
\fancyhead[L]{\includegraphics[height=1cm]{\insertlogo}}
\fancyhead[R]{\theauthor\\\thelecture}
\fancyfoot[R]{\thepage / \pageref{LastPage}}
\fancyfoot[C]{}
\fancyfoot[L]{\mytitle}


\lstset{basicstyle=\small\ttfamily}
\lstset{literate={*}{{\char42}}1{-}{{\char45}}1} %makes * and - copy-pasteable
\lstset{frame=single}
\lstset{backgroundcolor=\color{white}}
\lstset{keepspaces=true}
\lstset{columns=flexible}
\lstset{inputencoding=utf8}

\lstset{literate=%
{Ö}{{\"O}}1
{Ä}{{\"A}}1
{Ü}{{\"U}}1
{ß}{{\ss}}2
{ü}{{\"u}}1
{ä}{{\"a}}1
{ö}{{\"o}}1
}

\renewcommand{\maketitle}{{\Large\textbf{\thetitle}}}

% changes number of points displayed for exercise sections
\newcommand{\points}[1]{\renewcommand{\secPoints}{#1}}

\newenvironment{overview}
{\begin{mdframed}[roundcorner=10pt,backgroundcolor=lightblue]}
{\end{mdframed}}

\newenvironment{criteria}
{\textbf{Bewertungskriterien:}\\\begin{tabular}{r l}}
{\end{tabular}}

\newcommand{\pointsFor}[2]{#1 & #2 \\}

\newcommand{\solutionSpace}[1]{\begin{mdframed}
\vspace{#1}
\end{mdframed}}

\newcommand{\lines}[1]{
	\vspace{\baselineskip}
	\begin{tikzpicture}
		\foreach \x in {1,...,#1} {
			\draw[line width=.5pt, loosely dotted] (0, {-(\x-1)*\baselineskip}) -- ++(\textwidth,0);
		}
	\end{tikzpicture}
}

\newenvironment{solutionspace}
{\begin{mdframed}}
{\end{mdframed}}

\newcommand{\makeInputFields}[1]{%
	\begin{tabular}{p{.4\textwidth} p{.4\textwidth}}
	Nachname: & Punkte: \hspace{1cm}/#1\\[5mm]
	Vorname: & Note:\\[5mm]
	Matrikelnummer: \\
	\end{tabular}\vspace{7mm}
}
