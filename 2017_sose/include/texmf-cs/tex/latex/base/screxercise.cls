\def\fileversion{1.0}
\def\filedate{2015/10/06}

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{screxercise}[\filedate\space Version \fileversion\space by
  Christopher Schölzel]

% documentclass based on coma script article
\LoadClass[a4paper, parskip=full, DIV=12]{scrartcl}

\RequirePackage[headsepline=.5pt:]{scrlayer-scrpage}
\RequirePackage[luatex, table, cmyk]{xcolor}
\RequirePackage{graphicx}
\RequirePackage{amsmath}
\RequirePackage[ngerman]{babel}
\RequirePackage{ulem} % for \uline
\RequirePackage[framemethod=TikZ]{mdframed} %for frames with rounded corners
\RequirePackage{tcolorbox} % for nice boxes
\tcbuselibrary{listings} % enable tcolorbox features for listings
\RequirePackage{multicol} %for multicols environment
\RequirePackage{listings} %listings environments with syntax higlighting
%\RequirePackage[a4paper,inner=3cm,outer=3cm,top=3cm,bottom=3cm]{geometry} %to change geometry
\RequirePackage{pageslts} % for LastPage reference
\RequirePackage{caption} % allow line breaks in captions
\RequirePackage[hidelinks]{hyperref}
\RequirePackage{titling} % for \theauthor command
\RequirePackage{lstautogobble} % for autogobble=true
\RequirePackage{fontspec} % for \setmonofont

\renewcommand{\emph}[1]{\textit{#1}}
\newcommand{\term}[1]{\textit{#1}}
\newcommand{\clue}[1]{Hinweis: \textit{#1}}
\newcommand{\stress}[1]{\textbf{#1}}
% code with rounded borders from Herr K. on Stackoverflow
% Link: http://tex.stackexchange.com/a/140175 
\newcommand\code[2][]{%
  \strut\tikz[baseline=(s.base)]{
    \node(s)[
      rounded corners,
      thick,
      fill=black!5,       % background color
      draw=black!20,      % border of box
      text=black,         % text color
      inner xsep =3pt,    % horizontal space between text and border
      inner ysep =0pt,    % vertical space between text and border
      text height=2ex,    % height of box
      text depth =0.7ex,  % depth of box
      outer sep  =0pt,    % outer margin
      #1                  % other options
    ]{\lstinline|#2|};
  }%
}\newcommand{\enquote}[1]{\glqq{}#1\grqq{}}
\newcommand{\tabvspace}[1]{\parbox[t][#1][t]{0cm}{}}

% command for section image that can be changed to indicate type of exercise
% TODO switch to new style:
%\newcommand{\secimg}[1]{\gdef\insertsecimg{#1}}
\newcommand{\secimg}{}

% defines own title format which starts with the text "Aufgabe # --" and contains an optional section image
% \begin{section*} can still be used for normal sections
\renewcommand{\sectionformat}{\secimg{}Aufgabe \thesection \hspace{1mm} -- }

\setkomafont{subsection}{\normalfont\normalsize\bfseries}

% TODO add global setting to hide or view solutions
% solution environment
\newenvironment{loesung}{
\begin{mdframed}[roundcorner=10pt,backgroundcolor=lightgreen]
\textcolor{red}{\bf Lösung}: 
}
{
\end{mdframed}
}

% clue environment
\newenvironment{hinweis}{
\begin{tcolorbox}[colback=black!5,colframe=black!20]
\textit{Hinweis:}
}
{
\end{tcolorbox}
}

\definecolor{THMgreen}{HTML}{80ba24}


% sets logo
\newcommand{\logo}[1]{\gdef\insertlogo{#1}}
\logo{img/MNI-Logo.pdf}

% sets lecture name
\newcommand{\lecture}[1]{\gdef\thelecture{#1}}
\lecture{}

\setkomafont{pagefoot}{\normalfont\normalcolor\small}
\setkomafont{pagehead}{\normalfont\normalcolor\small}
\setlength{\headheight}{1.2 cm}
\pagestyle{scrheadings}
% TODO allow to set logo heiht
\ohead{\includegraphics[height=1cm]{\insertlogo}}
\ihead{\theauthor\\\textbf{\thelecture}\vspace*{-1.8ex}}
\ifoot{\thepage / \pageref{LastPage}}
\cfoot{}
\ofoot{\thetitle}

\lstset{basicstyle=\small\ttfamily}
\lstset{literate={*}{{\char42}}1{-}{{\char45}}1} %makes * and - copy-pasteable
\lstset{backgroundcolor=\color{black!5}}
\lstset{keepspaces=true}
\lstset{columns=flexible}
\lstset{inputencoding=utf8}
\lstset{autogobble}

\lstset{literate=%
{Ö}{{\"O}}1
{Ä}{{\"A}}1
{Ü}{{\"U}}1
{ß}{{\ss}}2
{ü}{{\"u}}1
{ä}{{\"a}}1
{ö}{{\"o}}1
}

\newcommand{\tools}[1]{Relevante Werkzeuge: #1}
\newcommand{\function}[1]{\code[fill=blue!5, draw=blue!20]{#1}}
\newcommand{\variable}[1]{\code[fill=red!5, draw=red!20]{#1}}
\newcommand{\keyword}[1]{\code[fill=green!20!black!5, draw=green!20!black!20]{#1}}
\renewcommand{\maketitle}{{\Large\textbf{\thetitle}}}

% changes section image to warning sign
\newcommand{\hard}{\renewcommand{\secimg}{\hspace{-1.3em}\tikz[x=0.8em, y=0.8em] \draw[rounded corners=1, draw=red, very thick] (0,0)--(1,0)--(0.5,1)--cycle;\hspace{.5em}}}
% resets section image
\newcommand{\easy}{\renewcommand{\secimg}{}}

\newenvironment{overview}
{\begin{mdframed}[roundcorner=10pt,backgroundcolor=lightblue]}
{\end{mdframed}}

\newenvironment{criteria}
{\textbf{Bewertungskriterien:}\\\begin{tabular}{r l}}
{\end{tabular}}

\newcommand{\pointsFor}[2]{#1 & #2 \\}

\newcommand{\solutionSpace}[1]{\begin{mdframed}
\vspace{#1}
\end{mdframed}}

\newenvironment{solutionspace}
{\begin{mdframed}}
{\end{mdframed}}

\newcommand{\namefield}{%
	Name: \hspace{5cm} Matrikelnummer: \hspace{4cm}
}
