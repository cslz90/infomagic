\def\fileversion{1.0}
\def\filedate{2018/09/02}

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{scrhandout-cs}[\filedate\space Version \fileversion\space by
  Christopher Schölzel]

% documentclass based on coma script article
\LoadClass[a4paper, parskip=full]{scrartcl}

\RequirePackage{titling} % for \theauthor command
\RequirePackage[automark, headsepline]{scrlayer-scrpage}
\RequirePackage{pageslts} % for LastPage reference
\RequirePackage{fontspec} % for \setmonofont
\RequirePackage[hidelinks]{hyperref}
\RequirePackage[a4paper,inner=1cm,outer=1cm,top=2cm,bottom=2cm]{geometry} %to change geometry
\setmonofont{AnonymousPro.ttf}[BoldFont=AnonymousProB.ttf]

\renewcommand{\maketitle}{{\Large\textbf{\thetitle}}}

% sets logo
\newcommand{\logo}[1]{\gdef\insertlogo{#1}}
\logo{img/MNI-Logo.pdf}

% sets lecture name
\newcommand{\lecture}[1]{\gdef\thelecture{#1}}
\lecture{}


\setlength{\headheight}{41pt}
\pagestyle{scrheadings}
% TODO allow to set logo heiht
\ohead{\includegraphics[height=1cm]{\insertlogo}}
\chead{}
\ihead{\textbf{\thelecture}\\\theauthor}
\ifoot{\thepage / \pageref{LastPage}}
\cfoot{}
\ofoot{\thetitle}


% make minipage restore the parskip length
% source: https://tex.stackexchange.com/a/141123
\makeatletter
\newcommand{\@minipagerestore}{\setlength{\parskip}{\medskipamount}}
\makeatother