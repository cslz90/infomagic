# Algorithmen und Datenstrukturen
Christopher Schölzel

## Überblick

Dieses Repository enthält alle Unterlagen zu meiner Veranstaltung "Algorithmen und Datenstrukturen" an der Technischen Hochschule Mittelhessen.

Im Sommersemester 2017 habe ich diese Veranstaltung zum ersten Mal gehalten und mich dafür entschieden, ein umfangreiches Gamification-Konzept zu entwickeln und einzusetzen, um meine Studierenden zu mehr kontinuierlichem Lernen zu motivieren und den eher trockenen theoretischen Inhalt etwas aufzulockern.

Das Gamification-Konzept kam im Sommersemester 2017 und 2018 zum Einsatz.
Im Sommersemester 2019 habe ich die Gamification-Inhalte aus zeitlichen Gründen auf ein Minimum heruntergefahren.

Wie in \[[1]\] beschrieben hat dieser Ansatz dazu geführt, dass die Studierenden im Schnitt etwa acht Stunden pro Woche zusätzlich zur Präsenzzeit in die Veranstaltung investierten und die Bestehensquote der Klausur lag (bei einer laut Kolleg\*innen ausreichend schweren Klausur) bei 90% in 2017, 96% in 2018 und 92% in 2019.

Ich entlasse alle Unterlagen von mir und meinen Tutor\*innen in die Gemeinfreiheit.
Genauere Lizenzbedingungen für die Inhalte Dritter finden Sie in den README-Dateien in den Ordnern für das jeweilige Semester.

### Literatur

[1]: #schoelzel2018

\[<a name="schoelzel2018">1</a>\] C. Schölzel, [“Brutus der Orkschamane erklärt die Brute-Force-Methode: Gamification und E-Learning in der Veranstaltung 'Algorithmen und Datenstrukturen',”](http://ceur-ws.org/Vol-2250/WS_Pro_paper1.pdf) in Proceedings der Pre-Conference-Workshops der 16. E-Learning Fachtagung Informatik, Frankfurt, Germany, 2018, vol. 2250.


## Inhalt

Eine genauere Auflistung der Inhalte finden Sie in den README-Dateien des jeweiligen Semesters.

* [Unterlagen aus 2017](2017_sose/README.md)
* [Unterlagen aus 2018](2018_sose/README.md)
* [Unterlagen aus 2019](2019_sose/README.md)

## Dozentron und Gildamesh

Einige Inhalte referenzieren die E-Learning-Systeme Dozentron und Gildamesh.
Dabei handelt es sich um Webanwendungen, die von Studierenden in ihren Abschlussarbeiten an der THM entwickelt wurden.
Ich beschreibe hier im Folgenden nur ganz grob die Funktionalität der beiden Systeme.
Sobald diese Systeme ausreichend stabil sind, möchte ich sie auch gerne unter einer Open Source-Lizenz veröffentlichen.
Für genauere Informationen und einen eventuellen Einsatz der Systeme in Ihrer eigenen Veranstaltung können Sie mich gerne persönlich kontaktieren.

### Dozentron

Bei Dozentron handelt es sich um ein System zur automatischen Bewertung von Studierendenabgaben.
Studierende können auf dem Server die Aufgabenbeschreibung lesen und ihre Lösungen einreichen, um dann sofort ein Feedback anhand von automatisierten Tests zu erhalten.
Bis zur finalen Deadline können Aufgaben jederzeit nachgebessert und neu eingereicht werden.
Die abschließende Bewertung erfolgt durch Tutor\*innen, die ebenfalls schon die Ergebnisse der automatisierten Tests als Grundlage verwenden und sich daher mehr auf stilistische und semantische Fehler konzentrieren können.
Als Mitarbeiter\*in oder Student\*in der THM können Sie Dozentron [innerhalb des THM-Netzes erreichen](https://dozentron.mni.thm.de).

Dozentron beherrscht zur Zeit die folgenden Aufgabentypen:

* **Unittestaufgaben** sind Programmieraufgaben, die mit Unittests geprüft werden.
    Hier existieren Plugins für die Sprachen Java und Python.
* **Input/Output-Aufgaben** geben den Studierenden zusätzlich zum Aufgabentext eine zufällige Eingabe (z.B. eine Liste von Zahlen) in Form einer Textdatei, zu der die Studierenden dann die richtige Ausgabe (z.B. die fünftgrößte Zahl) ebenfalls in Textform angeben müssen.
    Die Studierenden müssen zusätzlich zum Lösungstext auch ihren Quellcode hochladen.
    Dieser wird aber im Gegensatz zu den Unittestaufgaben nicht direkt geprüft.
    Der Vorteil von diesem Aufgabentyp ist, dass die Studierenden Lösungen in jeder beliebigen Programmiersprache abgeben können.
    Inspiriert ist dieser Aufgabentyp von [Advent of Code](https://adventofcode.com/).
* **JShell-Aufgaben** sind ein Java-spezifischer Sonderfall der Unittestaufgaben.
    Hier muss kein vollständiges JAR-Archiv abgegeben werden, sondern der Code kann direkt in ein Textfeld eingegeben werden, so als würde man ihn auf der JShell tippen.
    Dieser Aufgabentyp wurde für diese Veranstaltung nicht verwendet, eignet sich aber besonders für Programmieranfänger, die noch nicht mit einer IDE arbeiten.

### Gildamesh

Gildamesh ist ein Verwaltungssystem für die Gamification-Elemente der Veranstaltung.
Hier werden Lebenspunkte, Erfahrungspunkte, Fähigkeiten und Aufgaben für die Studierenden gespeichert.
Zum Teil finden die Anwendung von Fähigkeiten (z.B. Heilung einer anderen Person) auch schon automatisiert statt.
