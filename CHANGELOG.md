# Change Log
Alle erwähnenswerten Änderungen für dieses Projekt werden in dieser Datei dokumentiert.

Das Format basiert auf [Keep a Changelog](http://keepachangelog.com/) 
und dieses Projekt folgt dem Prinzip des [Semantic Versioning](http://semver.org/).

## \[Unreleased\]

### Added

* fehlender Link zum DeLFI-Paper

### Changed

\[nothing\]

### Fixed

\[nothing\]


## \[3.0.0\]

### Added

* Unterlagen für Sommersemester 2017, 2018 und 2019

### Changed

\[nothing\]

### Fixed

\[nothing\]