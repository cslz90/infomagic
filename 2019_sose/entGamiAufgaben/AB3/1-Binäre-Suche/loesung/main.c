#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
    char *input = malloc(10 * sizeof(char));
    int min = 0;
    int max = 1024;
    int guess = max / 2;

    while (1) {
        printf("My next guess is %d\n", guess);
        printf("Commands : [<] less | [>] more | [=] match | [q] quit\n# ");
        if (fgets(input, 10, stdin) != NULL) {
            if (strcmp(input, ">\n") == 0) {
                min = guess;
                guess = min + (max - min) / 2;
            } else if (strcmp(input, "<\n") == 0) {
                max = guess;
                guess = min + (max - min) / 2;
            } else if (strcmp(input, "q\n") == 0 || strcmp(input, "=\n") == 0) {
                free(input);
                printf("Exiting with guess %d!\n", guess);
                return guess;
            }
        } else {
            free(input);
            printf("Error: unable to read input!");
            return -1;
        }
    }
}