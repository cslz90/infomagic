# Aufgabenblatt 3
## Pflichtaufgaben

|Nr | Thema        | Name           | Dauer in Sdt (für Studis)                |  Ref                                                            |
|---| ---          | ---            | ---                                      |  ---                                                            |
|01 | Binäre Suche | Binäre Suche   | Geschätzt:  1/6   Probegelöst in 2-5min. | [Link](2019_ss/entGamiAufgaben/AB3/1-Binäre-Suche/aufgabe.md)   |
|02 | Mergesort    | Mergesort      | Geschätzt:  1/2   Probegelöst in ca. 1h  | [Link](2019_ss/entGamiAufgaben/AB3/2-Mergesort/aufgabe.md)      |
|03 | Trie         | Recherche Trie | Geschätzt:  2                            | [Link](2019_ss/entGamiAufgaben/AB3/3-Recherche-Trie/aufgabe.md) |

## Bonusaufgaben

|Nr | Thema               | Name                                  | Dauer in Sdt (für Studis)                 | Ref                                                                                          |
|---| ---                 | ---                                   | ---                                       | ---                                                                                          |
|01 | Sortieren           | Permutationsort                       | Geschätzt:  2     Probegelöst in ca. 1,5h | [Link](2019_ss/entGamiAufgaben/AB3/Bonus/1-Permutationsort/aufgabe.md)                       |
|02 | Bäume               | Decision Tree                         | Geschätzt:  ??                            | ?                                                                                            |
|03 | Suchbäume           | Binärer Suchbaum I                    | Geschätzt:  ??                            | [Link](2019_ss/entGamiAufgaben/AB3/Bonus/3-Binärer-Suchbaum-I/aufgabe.md)                    |
|04 | Laziness            | Zip für Java-Streams                  | Geschätzt:  2h    Probegelöst in ca. 0,5  | [Link](2019_ss/entGamiAufgaben/AB3/Bonus/4-Zip-für-Java-Streams/aufgabe.md)                  |
|05 | Schleifeninvariante | Schleifeninvariante trifft O-Notation | Geschätzt:  ??                            | [Link](2019_ss/entGamiAufgaben/AB3/Bonus/5-Schleifeninvariante-trifft-O-Notation/aufgabe.md) |
|06 | Bäume               | Labyrinth                             | Geschätzt:  ??                            | ?                                                                                            |
|07 | Josephus-Problem    | Josephus-Problem                      | Geschätzt:  1h                            | [Link](2019_ss/entGamiAufgaben/AB3/Bonus/7-Josephus-Problem/aufgabe.md)                      |
|08 | Rekursion           | Reihenfolgen ohne Doppel-Null         | Geschätzt:  1h    Probegelöst in ca. 1h   | [Link](2019_ss/entGamiAufgaben/AB3/Bonus/8-Reihenfolgen-ohne-Doppel-Null/aufgabe.md)         |
