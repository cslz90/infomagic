# Korrekturempfehlungen

Sollte implementiert und funktionsfähig sein.

## Positive Faktoren
- Korrekte verwendung der Kapselungsprinzips in Tuple
- `zip` und `zipWith` lazy implementiert (`lazyZipTest` und `lazyZipWithTest` bestanden)

## Unit-Tests
- ca. 15% für `tupleTest` + `tupleEqualsTest`
- ca. 50% für `zipTest` + `zipLengthTest`
- ca. 35% für `zipWithTest` + `zipWithLengthTest`

**Bemerkung:** Die Tests `lazyZipTest` und `lazyZipWithTest` wurden nachträglich hinzugefügt und sollten nicht ausschlaggebend für das Bestehen der Aufgabe sein.