package changemaking;

import static changemaking.CoinChangeGenerator.generateShuffledLongs;

public class Level3Generator {
    public static void main(String[] args) {
        generateShuffledLongs(1000, 100_000, 11_287);
    }
}
