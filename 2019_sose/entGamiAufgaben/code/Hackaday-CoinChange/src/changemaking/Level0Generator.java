package changemaking;

import static changemaking.CoinChangeGenerator.generateShuffledLongs;

public class Level0Generator {
    public static void main(String[] args) {
        generateShuffledLongs(100, 7, 1);
    }
}
