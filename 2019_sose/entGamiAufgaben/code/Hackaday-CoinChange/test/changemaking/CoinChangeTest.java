package changemaking;

import de.thm.mni.aud.commons.AbstractInputOutputTest;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class CoinChangeTest extends AbstractInputOutputTest {

    private static long[] correctLengths;
    private static long[][] studentSolutions;

    public static final String STUDENT_INPUT_PROPERTY = "input";
    public static final String STUDENT_OUTPUT_PROPERTY = "student-output";

    private static List<String> studentInputLines;
    private static List<String> studentOutputLines;

    @BeforeClass
    public static void prepareClass() throws IOException {
        String studentInputFile = System.getProperty(STUDENT_INPUT_PROPERTY);
        String studentOutputFile = System.getProperty(STUDENT_OUTPUT_PROPERTY);
        if (studentInputFile == null) { studentInputFile = "exampleInput.txt"; }
        if (studentOutputFile == null) { studentOutputFile = "exampleOutput.txt"; }
        studentInputLines = Files.readAllLines(
                Paths.get(studentInputFile),
                StandardCharsets.UTF_8
        );
        studentOutputLines = Files.readAllLines(
                Paths.get(studentOutputFile),
                StandardCharsets.UTF_8
        );

        correctLengths = new long[studentInputLines.size()];
        studentSolutions = new long[studentOutputLines.size()][];
        for(int i = 0; i < correctLengths.length; i++) {
            long[] solution = CoinChangeSolver.solve(Long.parseLong(studentInputLines.get(i)));
            correctLengths[i] = solution.length;
        }
        for(int i = 0; i < studentOutputLines.size(); i++) {
            long[] solution = Arrays.stream(studentOutputLines.get(i).split(",")).mapToLong(Long::parseLong).toArray();
            studentSolutions[i] = solution;
        }
    }

    @Override
    public void prepare() {}

    @Test
    public void testCorrect() {
        for(int i = 0; i < studentSolutions.length; i++) {
            assertEquals("wrong solution at line "+(i+1),Long.parseLong(studentInputLines.get(i)), Arrays.stream(studentSolutions[i]).sum());
        }
    }

    @Test
    public void testOptimal() {
        for(int i = 0; i < studentSolutions.length; i++) {
            assertEquals("wrong solution length at line "+(i+1), correctLengths[i], studentSolutions[i].length);
        }
    }
}
