package fileio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class RegexScan {

    public static String readEntireFile(final String filePath) {
        try {
            return Files.readAllLines(Paths.get(filePath))
                    .stream()
                    .collect(Collectors.joining("\n"));
        } catch (IOException e) {
            return null;
        }
    }

    public static int numerare(String str, String pattern) {
        int counter = 0;
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(str);

        while (m.find()){
            counter++;
        }

        return counter;
    }


}
