package main;

public class myLowerCaseClassName {

	public static int MyStaticUpperCaseVariable = 0;
	public static final int MY_CONSTANT_VARIABLE_42 = 42;
	public static final int anotherConstantVariable = 1337;
	
	public void MyUpperCaseMethodName(int MyUpperCaseParameter)
	{
		int MyLocalUpperCaseVariable = 5;
		if (MyLocalUpperCaseVariable == 5) {
			int MyNestedVariable = 42;
			for (int I = 0; I < MyNestedVariable; I++) {
				MyStaticUpperCaseVariable += I; }
		}
	}
}
