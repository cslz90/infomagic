package morsecode;

import de.thm.mni.aud.commons.CodeAnalyser;
import org.junit.Test;
import sourcecodetester.SourceCodeTester;
import sourcecodetester.tests.*;
import sourcecodetester.util.Method;

public class SourceCodeTest extends CodeAnalyser {

  @Override
  public void setupTests(SourceCodeTester sct) {

    // A reference to the tested implementation is required to successfully compile on Dozentron
    try {
      MorseCode.encode("Hello World");
    } catch(Throwable t) {
      // prevents any exceptions or errors because the static code tests should do that
    }

    sct.createNewTestGroup("Class and Method Implementations", "The following tests are checking for correct class and method implementations.")
      .addTests(
        new ClassImplementations(
          "morsecode.MorseCode"
        ),
        new MethodImplementations(
          new Method("morsecode.MorseCode.encode()")
            .requireVisibilityPublic()
            .requireModifierStatic()
            .requireReturnType("String")
            .requireParameters("String")
        ),
        new MethodCallUsedInMethods(
          new Method("morsecode.MorseCode.getDictionary"),
          new Method("morsecode.MorseCode.encode()")
        )
      )
    ;
    // CODE QUALITY
    sct.createNewTestGroup("Code Quality", "The following tests are checking the overall quality of your source code.")
      .addTests(
        new ClassNamesStartWithCapitalLetter(),
        new MethodNamesStartWithLowerCaseLetter(),
        new VariableNamesStartWithLowerCaseLetter(),
        new ClosingBraceInOwnLine(),
        new OpeningBraceInSameLineWithStatement(),
        new LimitCharactersPerLine(150)
      )
    ;
  }

  @Test
  public void implementationsTest() {
    runTests("Class and Method Implementations");
  }

  @Test
  public void codeQualityTest() {
    runTests("Code Quality");
  }
  
}
