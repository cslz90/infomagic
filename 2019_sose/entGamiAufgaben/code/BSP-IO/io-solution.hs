import System.Environment (getArgs)

main = do
  args <- getArgs
  let number = read (head args) :: Integer
  putStrLn $ show $ number*10
