package hello;
import java.io.PrintStream;
import java.io.OutputStream;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class HelloWorldTest {
  private class MyPrintStream extends java.io.PrintStream {
    public MyPrintStream(OutputStream os) {
      super(os);
    }
    public void println(String s) {
      output = s;
      super.println(s);
    }
  }
  private String output;
  
  @Before
  public void rigSout() {
    System.setOut(new MyPrintStream(System.out));
  }

  @Test
  public void testEmpty() {
    // will always succeed (yay, you did not produce any compile errors!)
  }

  @Test
  public void testOutput() {
    HelloWorld.main(new String[0]);
    assertEquals("Hello World", output);
  }

  @Test
  public void failNoMatterWhat() {
    fail("Dies ist ein Test, was mit langen Zeilen und Zeilenumbrüchen in der Ausgabe von Dozentron passiert.\n\nUnd hier\n\tKommen\n\t\tmehr Zeilen.");
  }
}