## Korrekturschema:

### Kriterien fürs Nichtbestehen

* keine Quellenangabe
* keine vollständigen Sätze
* völlig unverständlicher Text
* über 800 Worte Text
* über 10 Quellen

### Punkteverteilung (12 gesamt)

* **1 Punkt** pro korrekt beantworteter Frage (**5 gesamt**)
* **2 Punkte** für verständliche Formulierungen.
* **2 Punkte** für das Akzeptable Grammatik/Rechtschreibung (muss nicht perfekt sein, aber auch nicht Kraut und Rüben)
* **2 Punkt** für verlässliche Quellen
* **1 Punkt** für das Einhalten der Quellengrenze


## Korrekturschema Ampel:

### Kriterien für Rot:

* keine Quellenangabe
* keine vollständigen Sätze
* völlig unverständlicher Text
* über 800 Worte Text
* über 10 Quellen
* mehr als 2 Frage(n) nicht bearbeitet
* Grundsätzlich schlecht

### Kriterien für Grün:

* Alle Fragen beantwortet
* Verständliche Formulierungen
* Akzeptable Grammatik/Rechtschreibung (muss nicht perfekt sein, aber auch nicht Kraut und Rüben)
* für verlässliche Quellen (Bonus, aber nicht notwendig für Grün, Anmerkung falls es fehlt)
* Einhalten der Quellengrenze

### Gelb:

* Alles was dazwischen ist
* Ausnahme: *eine* "nicht ganz so wichtige" Kriterie