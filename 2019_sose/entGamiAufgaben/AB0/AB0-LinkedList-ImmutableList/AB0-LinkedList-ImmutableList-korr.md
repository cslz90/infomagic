## Bewertungskriterien

## Aufgabe 1.1

### Kriterien für Grün

* Sie ist immutable
* Alle Methoden richtig implementiert
* Cons macht wirklich an das Anfang der Liste
* Wirft wirkliche eine IndexOutOfBounds Exception
* Die Speicherrepräsentation ist korrekt
* toString und equals überschrieben
* Alles ist Generisch implementiert

### Kriterien für Rot

* nicht jede Methode probiert 
* Kein Cons / Nil
* eine Array-Liste verwendet

## Aufgabe 1.2

### Kriterien für Grün

* Alles ist Generisch implementiert
* Sie funktioniert
* Die Signatur wird korrekt verwendet
* Sie ist immutable (wichtig!)

### Kriterien für Rot

* das mappen funktioniert garnicht
* Vorschläge?




