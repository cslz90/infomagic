# Bewertungsvorschläge



## Positive Faktoren:
	- Es wird eine interne Datenstruktur verwendet, um die Schlüssel-Wert Paare abzubilden
	- Gut und verständlich kommentierter Code
	- Verständliche und sinnvolle Variablennamen
	
## Weiche Kriterien: 
	- Die neue Schlüsselwerte werden berechnet und sortiert und der Ausgabeformat ist nur fehlerhaft
	- Daten werden von eine Datei eingelesen und nicht direkt als Datenstruktur "hardcoded"
	- Die Schlüssel werden in der umgekehrten reihenfolge sortiert -> Denkfehler
	
## Unit-Tests
	- 1% Luck
	
## Ungültig:
	

## Kriterien für Grün:

* Es ist kein Wirrwar - es ist gut erkennbar was gemacht wird. Hier evt Anmerkung in der Bewertung!
* Größtenteils Verständliche und sinnvolle Variablennamen 
* Sinnvolle Abbildung der Schlüssel-Wert-Paare ( Map etc, Array nur sehr bedingt)
* Bonus (eher grün, aber keine Vorraussetzung) Einlesen von einer Datei oder den String einer Methode als Parameter übergeben
* (aufsteigend) sortiert

## Anmerkung für Grün:

* Sofern die umgekehrte Reihenfolge besteht ist und sonst alles gut ist -> Grün
* Gleiche bei falscher Ausgabe weil es jemand nicht richtig gelesen hat, sofern der Rest passt

## Kriterien für Rot:

* ?? Eindeutig nicht verstanden?
* Ekelhafter Code