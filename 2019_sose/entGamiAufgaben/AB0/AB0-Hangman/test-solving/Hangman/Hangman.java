package hangman;

import java.util.*;

public class Hangman {

    private static List<Character> mostUsedSet = new ArrayList<>();
    private static Set<Character> characterSet = new TreeSet<>();

    static {
        for (int i = 0; i < 26; i++) characterSet.add((char) ('a' + i));
        Collections.addAll(mostUsedSet, 'e', 'n', 'i', 's', 'r', 'a', 't', 'd', 'h', 'u', 'l', 'c', 'g', 'm', 'o', 'b', 'w', 'f', 'k', 'z', 'p', 'v', 'j', 'y', 'x', 'q');
    }

    private Set<Character> wordSet;
    private Iterator<Character> famousSet;
    private String secretWord;
    private Integer secretSize;
    private Integer maxGuess;
    private Integer badGuess;
    private Random random;

    public Hangman() {
        wordSet = new TreeSet<>();
        famousSet = mostUsedSet.iterator();
        secretSize = 0;
        maxGuess = 0;
        badGuess = 0;
        random = new Random();
    }

    public Hangman(String secretWord, int maxGuess) {
        this();
        this.secretWord = secretWord;
        this.maxGuess = maxGuess;
        for (int i = 0; i < secretWord.length(); i++) wordSet.add(secretWord.charAt(i));
        this.secretSize = wordSet.size();
    }

    public String getWord() {
        return secretWord;
    }

    public boolean isPossibleToLoose() {
        return secretSize < (characterSet.size() - maxGuess);
    }

    public int getLineCount() {
        return badGuess;
    }

    public char playRandomly() {
        char guess = (char) ('a' + random.nextInt(26));
        if (wordSet.contains(guess)) wordSet.remove(guess);
        else badGuess++;
        return guess;
    }

    public char playStrategically() {
        char guess = famousSet.next();
        if (wordSet.contains(guess)) wordSet.remove(guess);
        else badGuess++;
        return guess;
    }

    public boolean isWin() {
        return badGuess <= maxGuess && wordSet.size() == 0 && secretWord.length() != 0;
    }

}
