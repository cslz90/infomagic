# Aufgabenblatt 0
## Pflichtaufgaben

|Nr | Thema                         | Name                | Typ      | Dauer in min   | XP  | Ref |
|---| ---                           | ---                 | ---      | ---            | --- | --- |
|01 | Generics                      | ArrayList           | Unittest | 30             |  5  | [Link](./../../../2019_ss/entGamiAufgaben/AB0/AB0-Generics-ArrayList/AB0-Generics-ArrayList.md) |
|02 | LinkedList                    | ImmutableList       | Unittest | 60             |  3  | [Link](./../../../2019_ss/entGamiAufgaben/AB0/AB0-LinkedList-ImmutableList/AB0-LinkedList-ImmutableList.md) |
|03 | Recherche                    | Generics       | Text | 30            |  ?  | [Link](./../../../2019_ss/entGamiAufgaben/AB0/AB0-Recherche-Generics/AB0-Recherche-Generics.md) |
|04 | Strings, ASCII                    | ASCII-Spielchen       | IO | 45-60            |  ?  | [Link](./../../../2019_ss/entGamiAufgaben/AB0/AB0-StringsASCII-ASCII-Spielchen/AB0-StringsASCII-ASCII-Spielchen.md) |



## Bonusaufgaben

| Thema                         | Name            | Typ      | Dauer in min | XP  | Ref |
| ---                           | ---             | ---      | ---          |---  | --- |
| Collections                   | UniqueList      | Unittest | 20       | 5   | [Link](./../../../2019_ss/entGamiAufgaben/AB0/AB0-Collections-UniqueList/AB0-Bonus-Collections-UniqueList.md) |
| Maps                          | Morse-Code      | Unittest | 180          | 45  | [Link](./../../../2019_ss/entGamiAufgaben/AB0/AB0-Collections-Morse-Code/AB0-Collections-Morse-Code.md) |
| OOP                           | OOP             | Unittest | 120          | 30  | [Link](./../../../2019_ss/entGamiAufgaben/AB0/AB0-OOP/AB0-Objektorientierung.md) |
| Allgemein                     | Hangman         | Unittest | 240          | 60  | [Link](./../../../2019_ss/entGamiAufgaben/AB0/AB0-Hangman/AB0-Hangman.md) |

## Bonusaufgaben (unfertig)

| Thema                         | Name                             | Typ      | Dauer in min   | XP  | Ref |
| ---                           | -------------------------------- | -------- | -------------- |--- | ----|
| File-IO                       | VerteidigungGegenMagischeWesen   | Unittest | 25             | 3   | [Link](./../../../2019_ss/aufgaben/AB0/AB0-FileIO-VerteidigungGegenMagischeWesen/AB0-FileIO-VerteidigungGegenMagischeWesen-klar.md)|
| Generics                      | BeutelUndBasilisten              | Unittest | 40             | 10  | [Link](./../../../2019_ss/aufgaben/AB0/AB0-Bonus-Generics-BeutelUndBasilisten/AB0-Bonus-Generics-BeutelUndBasilisten-klar.md) |
| Allgemein                     | MagicalPot                       | Unittest | ?             | ?   | [Link](./../../../2019_ss/aufgaben/AB0/AB0-Bonus-General-MagicalPot/AB0-Bonus-General-MagicalPot-klar.md) |
| Allgemein                     | ZipfschesGesetz                  | Unittest | ?             | ?   | [Link](./../../../2018_ss/aufgaben/AB0/AB0-Bonus-GeneralZipfschesGesetz/AB0-Bonus-GeneralZipfschesGesetz-klar.md) |
