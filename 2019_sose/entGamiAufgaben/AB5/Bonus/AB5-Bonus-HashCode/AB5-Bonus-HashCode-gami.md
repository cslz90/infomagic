## Azubi bei Weltenbauer Inc.

*Sie heuern als Azubi bei der Firma "Weltenbauer Inc." an.
Ihr Chef - selbstverständlich ein vielbeschäftigter Tabellenbasilist - zeigt leider die Tendenzen eines Schreibtischmagiers und delegiert einfach einen Teil seiner Arbeit an Sie, ohne Ihnen wirklich zu erklären, was zu tun ist.
Da Sie keine weiteren Untergebenen haben, müssen Sie in den sauren Apfel beißen und die Aufgaben tatsächlich lösen.*

*Aus dem Dokumentenstapel, den er Ihnen überreicht, können Sie sich aber zusammenreimen, dass Sie die Grundbausteine für ein paar alternative Realitäten erschaffen sollen. Genau genommen sind es drei an der Zahl:*

* *Eine Gruppe, die sich die **Rektangulatoren** nennt, hätte gerne eine Parallelwelt, in der alles aus rechteckigen Kacheln, sogenannten `Tile`s, besteht.
    Es soll verschiedene Regionen geben, zwischen denen man hin und her reisen kann, aber innerhalb dieser Regionen soll alles schön Flach sein und nur durch eine x- und y-Position definiert sein.
    Das ist besonders wichtig damit die **Quaderanten** - die natürlichen Fressfeinde der **Rektangulatoren** - nicht in diese Parallelwelt eindringen können.*
* *Ein ganz anderes Problem haben die **Punktinger**, die einen großen Teil der zweidimensionalen und der dreidimensionalen Welt breist haben.
    Diese wilden Krieger suchen nun nach neuen Abenteuern in einer eindimensionalen Welt, die Sie **Intervallhalla** nennen.
    Die einzigen Begrenzungen, die es dort gibt, sollen `Interval`s sein, die durch zwei Kommazahlen - eine untere und eine obere Grenze - gegeben sind.
    Selbst diese Grenzen soll es aber sowohl in geschlossener wie auch offener Form geben.*
* *Gerade als Sie dachten, nur Aufträge von völligen Spinnern vor sich zu haben, finden Sie die Nachricht von **Maena der Idealistin**, die sich einfach einen Neuanfang für diese verkommene Welt wünscht.
    Ihre alternative Realität soll eigentlich nach den gleichen Prinzipien aufgebaut sein wie unsere - lebende Wesen mit Genen aus vier unterschiedlichen Basen in einem Doppelstrang.
    Ihre Aufgabe ist die Definition der `Gene`s.*

*Für alle diese drei Aufträge müssen Sie das Standardverfahren von Weltenbauer Inc. durchführen: Erstellen Sie den jeweiligen Grundbaustein und legen Sie ein Zauberbuch dazu, das Zauber für die Erstellung (`constructio`), die textuelle Darstellung (`ad stringentio`), die Gleichheit (`equalitas`) und - für Ihren Chef - die Darstellung als Zahlencode (`hashiem`) beinhaltet.*

**Achtung:** *Denken Sie unbedingt daran, dass Tabellenbasilisten streng darauf achten, dass die Zauber `equalitas` und `hashiem` in der gemeinsamen Verwendung kein instabiles Verhalten zeigen.
Sie reagieren außerdem auch ganz allergisch, wenn bei der Verwendung von `hashiem` zu viele magische Kollisionen entstehen.*