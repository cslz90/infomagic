# Korekturschema:

* 30 Punkte aufgeteilt auf 3 Klassen => 10 Punkte pro Klasse
* Wichtig: Wenn der Test `t03_hashCodeFitsEquals` nicht bestanden wurde, gibt es auch keine Punkte für `t04_collisionsAcceptable` und `t05_collisionsOptimal`. In dem Fall besteht nämlich auch die default-Implementierung von `hashCode` die besagten Tests.
* Ansonsten gilt die folgende Punktverteilung:
  * 1 Punkte für `t01_toString`
  * 2 Punkte für `t02_equals`
  * 4 Punkte für `t03_hashCodeFitsEquals`
  * 2 Punkt für `t04_collisionsAcceptable`
  * 1 Punkt für `t05_collisionsOptimal`