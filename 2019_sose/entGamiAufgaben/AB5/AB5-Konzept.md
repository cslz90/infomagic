# Aufgabenblatt 5

## Pflichtaufgaben

|Nr | Thema                   | Name                    | Dauer in Sdt (für Studis) | Punkte Gildamesh  | Ref           |
|---| ---                     | ---                     | ---                       | ---               | ---           |
|01 | Wegfindung mit Dijkstra | AB5-Dijkstra-Wegfindung |  Geschätzt:  2h  Probegelöst: 2h          | ?                 |[Link](https://git.thm.de/cslz90/ad-cs/blob/master/2019_ss/entGamiAufgaben/AB5/AB5-Dijkstra-Wegfindung/AB5-Dijkstra-Wegfindung-klar.md) |

## Bonusaufgaben

| Thema   | Name                                                   | Dauer in Sdt (für Studis) | Punkte Gildamesh | Ref            |
| ---     | ---                                                    | ---                       | ---              | ---            |
|Graphen | AB5-Bonus-Graphen-SchatzgoblinsImLabyrinthDerSchmerzen |                           | ?                | |
|Graphen | AB5-Bonus-HashCode |             Geschätzt: 1h    Probegelöst ca. 1h          | ?                |[Link](https://git.thm.de/cslz90/ad-cs/blob/dev/2019_ss/entGamiAufgaben/AB5/Bonus/AB5-Bonus-HashCode/AB5-Bonus-HashCode-klar.md) |
