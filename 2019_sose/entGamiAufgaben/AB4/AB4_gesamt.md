# Rot-Schwarz-Bäume

## Vorwort

Diese Aufgabe ist eine Rechercheaufgabe wie aus Aufgabenblatt 0 und folgt daher den gleichen Regeln.

## Aufgabe 1.1

Recherchieren Sie den Aufbau von Rot-Schwarz-Bäumen. Beantworten Sie dabei in maximal 400 Worten Fließtext (mit vollständigen Sätzen ohne Stichpunkte!) die folgenden Fragen:

* Was sind die 5 Regeln, die einen Rot-Schwarz-Baum definieren?
* Welche Klasse aus der Java-API baut auf einem Rot-Schwarz-Baum auf?
* Welchen Vorteil bringt ein Rot-Schwarz-Baum gegenüber einer naiven Implementierung eines binären Suchbaums?
* Welche Eigenschaft muss ein Rot-Schwarz-Baum haben, der nur aus schwarzen Knoten besteht?
* Was ist die maximale Tiefe eines Rot-Schwarz-Baums mit n Knoten? Wie viele rote und wie viele schwarze Knoten hat der Pfad von der Wurzel zu einem Blatt auf der tiefsten Ebene?

**Hinweise:**

* Bitte achten Sie auf eine korrekte Rechtschreibung und Grammatik, auch diese fließt in die Bewertung ein.
* Bitte antworten Sie hier in Klartext.

## Aufgabe 1.2

Geben Sie die Quellen an, die Sie für Ihre Recherche verwendet haben. Hier reicht jeweils ein Link auf die Webseite oder der Titel des Buches, das Sie verwendet haben.
Es geht hier nicht darum herauszufinden, ob sie Formulierungen kopiert haben (was Sie natürlich nicht tun sollten), sondern darum, dass Sie zeigen, dass Sie in der Lage sind, geeignete Quellen zu finden und auszuwählen.

Bitte geben Sie auch nicht *alle* Quellen an, die Sie gelesen haben, sondern nur diejenigen, auf deren Inhalt Sie sich in Ihrem Text stützen.
Es sind maximal fünf Quellen erlaubt.


<div style="page-break-after: always;"></div>

# Zyklische Graphen

Graphen sind eine Standardstruktur der Informatik und werden Ihnen in Ihrer Karriere sehr häufig begegnen.

Ihre Aufgabe besteht darin in einem vorgegebenen Graphen (Kette) Zyklen zu finden. Für diese Aufgabenstellung sind gültige Graphen solche ohne Zyklen, ungültige enthalten mindestens einen Zyklus.

Um den Unterschied zwischen einem gültigen und ungültigen Graphen zu veranschaulichen, hier zwei bildliche Beispiele:

![Knotlinge](https://homepages.thm.de/~cslz90/kurse/ad17/static/Knotlinge.png)

Ungültige Zyklen sind rot markiert.

Erweitern Sie die Methode `hasCycles` so, dass sie ein "True" zurückgibt, falls ein Graph einen Zyklus enthält. Die Klasse `AbstractGraph<E>` ist in den Tests zum herunterladen enthalten

```java
import cycles.abstractGraph.AbstractGraph;
package cycles;
public final class Graph<E> extends AbstractGraph<E> {

	@Override
	public boolean hasCycles() {
		/* TODO: Aufgabe der Studenten */
		return false;
	}
}
```

<div style="page-break-after: always;"></div>

# Klasseninvariante

## Vorwort
Im Folgenden wird der Pseudocode zu einem Binärbaum gegeben.

```
# Definitions for the binary tree

structure BTree<E> { root: BNode<E>, size: int }

algorithm root(this: BTree<E>): BNode<E>
  return copy(this.root)

algorithm isEmpty(this: BTree<E>): boolean
  return this.root == None

algorithm size(this: BTree<E>): int
  return this.size

algorithm add(this: BTree<E>, value: E)
  if isEmpty(this) then
    this.root = singletonNode(value)
    size += 1
  else if addChild(this.root, value) then
    size += 1

# Definitions for nodes of the binary tree

structure BNode<E> { value: E, left: BNode<E>, right: BNode<E> }

algorithm value(this: BNode<E>): E
  return copy(this.value)

algorithm left(this: BNode<E>): BNode<E>
  return copy(this.left)

algorithm right(this: BNode<E>): BNode<E>
  return copy(this.right)

algorithm addChild(this: BNode<E>, value: E): boolean
  if value < value(this) then
    if left(this) = None then
      this.left = singletonNode(value)
      return true
    else
      return addChild(left(this), value)
  else if value > value(this) then
    if right(this) = None then
      this.right = singletonNode(value)
      return true
    else
      return addChild(right(this), value)
  else
    return false

# Helper definitions for creating an empty binary tree
# and a node for the binary tree with a value, but no child
# (i.e. the constructors)

algorithm emptyTree(): BTree<E>
  tree = new BTree<E>
  tree.root = None
  tree.size = 0
  return tree

algorithm singletonNode(value: E): BNode<E>
  node = new BNode<E>
  node.value = value
  node.left  = None
  node.right = None
  return node
```

## Aufgabe 3.1
Beantworten Sie die folgende Fragen: Kann der oben gegebene Binärbaum Elemente doppelt enthalten oder nicht? 
Erläutern Sie ebenfalls, an welchem Ausdruck bzw. an welcher Anweisung sich diese Eigenschaft feststellen lässt.

## Aufgabe 3.2
Sei `node` ein Knoten des Binärbaums `BTree`, dann ist `nodes(node)` die Menge aller Knoten (inklusive `node` selbst) in `node`.
nodes(node) ist definiert als:

![exercise-2](https://homepages.thm.de/~cslz90/kurse/ad18/static/AB4-Klasseninvariante-exercise-2.png)

Beweisen Sie, dass die Klasseninvarianten `1` und `2` stets für alle `BTree` gelten.

Sei `T` ein `BTree`, dann sind die Klasseninvarianten:

#### Klasseninvariante 1

![exercise-2-invariant-1](https://homepages.thm.de/~cslz90/kurse/ad18/static/AB4-Klasseninvariante-exercise-2-invariant-1.png)

**Beschreibung**: Die Anzahl der Werte, die `T` speichert, ist stets die Anzahl der Knoten im selben Baum.

#### Klasseninvariante 2

![exercise-2-invariant-2](https://homepages.thm.de/~cslz90/kurse/ad18/static/AB4-Klasseninvariante-exercise-2-invariant-2.png)

**Beschreibung**: Für alle Knoten `N` in einem beliebigen Binärbaum `T` gilt: Jeder Knoten `A` des linken Teilbaums hat einen kleineren Wert als `N` und jeder Knoten `B` des rechten Teilbaums hat einen größeren Wert als `N`.

### Aufgabe 3.2.1
Beweisen Sie, dass die Klasseninvarianten nach dem Aufruf des Konstruktors gelten. Da der in den Folien verwendete Pseudocode keine "richtigen" Konstruktoren hat, soll im Rahmen dieser Aufgabe die Prozedur `emptyTree()` als Konstruktor angesehen werden.

### Aufgabe 3.2.2
Beweisen Sie, dass die öffentlichen Prozeduren `root(BTree<E>)`, `isEmpty(BTree<E>)`, `size(BTree<E>)` und `add(BTree<E>, E)` nicht gegen die Klasseninvariante verstoßen. Sie dürfen davon ausgehen, dass die Invarianten vor dem Aufruf der Prozeduren gelten.



<div style="page-break-after: always;"></div>

# Binärer Suchbaum

## Aufgabe 1)

Realisieren Sie im Package `binarytree` einen binären Suchbaum für alle Datentypen, die vergleichbar sind.
Legen Sie dazu eine Klasse `BinaryTree` in besagtem Package an, die die folgende Schnittstelle implementiert:

```java
package binarytree;

import java.util.Comparator;

public abstract class ABinaryTree<T> {

  protected final Comparator<T> comparator;

  public ABinaryTree(Comparator<T> comparator) {
    this.comparator = comparator;
  }

  /**
   * @return the root node of this tree.
   */
  public abstract ABinaryTreeNode<T> root();

  /**
   * Add a value of type T to this binary tree.
   */
  public abstract void add(T value);

  /**
   * Remove a value of type T from this binary tree.
   */
  public abstract void remove(T value);

  /**
   * Check, if the value of type T exists in this tree.
   */
  public abstract boolean contains(T value);

  /**
   * @return true, if this tree is empty, false otherwise.
   */
  public abstract boolean isEmpty();

  /**
   * The number of elements in this tree.
   */
  public abstract int size();

}
```

Ein einzelner Knoten des `BinaryTree` wird von der Schnittstelle `ABinaryTreeNode` repräsentiert, von der Sie ebenfalls eine Realisierung anfertigen sollen.
Die Schnittstelle dafür wird im Folgenden aufgeführt:

```java
package binarytree;

public abstract class ABinaryTreeNode<T> {

  /**
   * @return the value stored in this node.
   */
  public abstract T value();

  /**
   * @return the left subtree of this node.
   */
  public abstract ABinaryTreeNode<T> left();

  /**
   * @return the right subtree of this node.
   */
  public abstract ABinaryTreeNode<T> right();

}
```

### Hinweise
* Dieser binäre Suchbaum soll keine Elemente doppelt enthalten.
* Nutzen Sie die bereitgestellten JUnit-Tests während Ihrer Arbeit.
* Es darf kein vorgefertigter Baum (der Java-API) verwendet werden.

## Hilfestellungen

Als Hilfestellungen werden im Folgenden für die Methoden `contains`, `add` und `remove` mögliche Implementierungen in Pseudocode vorgegeben.
Bitte beachten Sie, dass im Foliensatz 10 diese Algorithmen in ähnlicher Form vorgegeben wurden.
Die dort aufgeführten Algorithmen sind jedoch für binäre Suchbäume gedacht, die Elemente **mehrfach** enthalten können.

### Pseudocode für `contains` auf Basis von `bsearch`
```java
algorithm bsearch(haystack: BTNode<E>, needle: E)
  if haystack = None then
    return None
  if value(haystack) = needle then
    return haystack
  if value(haystack) > needle then
    return bsearch(right(haystack), needle)
  else
    return bsearch(left(haystack), needle)
```

```java
algorithm contains(haystack: BTNode<E>, needle: E)
  if bsearch(haystack, needle) = None then
    return false
  else
    return true
```

### Pseudocode für `add`
```java
algorithm sortedInsert(tree: BTNode<E>, el: E)
  next: BTNode<E>
  if el < value(tree) then
    next := left(tree)
  else if el > value(tree) then
    next := right(tree)

  if next = None then
    addChild(tree, el)
  else
    sortedInsert(next, el)
```

### Pseudocode für `remove`
```java
algorithm remove(tree: BTree<E>, el: E)
  tempRoot := new BTNode<E>(None, root(tree))
  res := sortedRemove(root(tree), tempRoot, el)
  tree.root := tempRoot.left
  tree.root = None
  return res

algorithm sortedRemove(node: BTNode<E>, parent: BTNode<E>, el: e)
  if value(node) < el then
    return sortedRemove(left(node), node, el)
  if value(node) > el then
    return sortedRemove(right(node), node, el)

  hasLeft := left(node) != None
  hasRight := right(node) != None
  if hasLeft and hasRight then
    minN, minP := findMin(right)
    node.value = minN.value
    sortedRemove(minN, minP, minN.value)
  if hasLeft:
    replace(node, parent, left(node))
  else:
    replace(node, parent, right(node))
  return true

algorithm findMin(node: BTNode<E>)
  if left(node) = None then
    return node
  return findMin(left(node))

algorithm replace(node: BTNode<E>, in: BTNode<E>, with: BTNode<E>)
  if left(in) = node then
    in.left = with
  else
    in.right = with
```

## Aufgabe 2)

Fügen Sie die beiden folgenden Methoden der abstrakten Klasse `ABinaryTree` hinzu und implementieren Sie sie.

```java
/**
 * Return an iterator, which traverses this tree in breadth-first-order.
 */
public abstract java.util.Iterator<T> breadthFirstIterator();

/**
 * Return an iterator, which traverses this tree in depth-first in-order.
 */
public abstract java.util.Iterator<T> depthFirstIterator();
```

### Hinweis
* Der in dieser Aufgabe zu implementierende Iterator muss lediglich die Operationen `hasNext()` und `next()` implementieren. Interessierte Studenten dürfen sich auch gerne an der `remove()`-Operation versuchen. Die Implementierung dieser Operation ist optional und wird deswegen nicht von den UnitTest überprüft.
