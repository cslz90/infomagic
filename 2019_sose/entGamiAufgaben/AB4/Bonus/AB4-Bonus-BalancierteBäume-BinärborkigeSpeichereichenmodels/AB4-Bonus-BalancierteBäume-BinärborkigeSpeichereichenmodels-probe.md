fun isBalanced(tree)
	if (tree.isLeaf)
	    return true
	if (abs(pathLength(tree.left) - pathlength(tree.right)) > 1)
	    return false
    return isBalanced(tree.left) && isBalanced(tree.right)

fun pathLength(tree)
    if (tree.isLeaf)
        return 1
    return 1 + max(pathLength(tree.left), pathLenght(tree.right))
