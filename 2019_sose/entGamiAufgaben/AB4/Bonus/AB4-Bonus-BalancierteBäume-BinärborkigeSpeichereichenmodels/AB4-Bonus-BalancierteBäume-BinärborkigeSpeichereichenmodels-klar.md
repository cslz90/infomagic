## Binärborkige Speichereichenmodels

In dieser Aufgabe sollen Sie einen Algorithmus als Pseudocode formulieren, der überprüft, ob ein Binärbaum ein balancierter Baum ist.

**Hinweise:**

- Unterscheidet sich an jedem Knoten die restliche Pfadlänge der beiden Teilbäume bis zum Blatt um höchstens eins, so ist ein Baum balanciert.
- Balancierte Bäume haben folgende Eigenschaften:  
    (muss so nicht im Algorithmus geprüft werden)
    - n Knoten insgesamt
    - Maximale Tiefe d
    - d <= c log n für irgendeine Konstante c
- Die Abgabe erfolgt als Klartextaufgabe.