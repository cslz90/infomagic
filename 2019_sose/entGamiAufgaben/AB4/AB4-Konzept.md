# Aufgabenblatt 4
## Pflichtaufgaben

|Nr | Thema                     | Name                                | Dauer in Sdt (für Studis)      | Punkte Gildamesh  | Ref |
|---| ---                       | ---                                 | ---                            |               --- | --- |
|01 | Rot-Schwarz-Bäume/Recherche         | AB4-Recherche-Rot-Schwarz-Bäume                   |  Geschätzt:  2h (Grob Probegelöst 1h)     |        	??|[Link](https://git.thm.de/cslz90/ad-cs/blob/dev/2019_ss/entGamiAufgaben/AB4/AB4-01-Recherche-Rot-Schwarz-B%C3%A4ume/AB4-Recherche-Rot-Schwarz-B%C3%A4ume-klar.md) |
|02 | Graphen-Algorithmus          | AB4-ZyklischerGraph(uf)                   | Geschätzt:  ?? Probegelöst in ca. 1h         |               ??  |[Link](https://git.thm.de/cslz90/ad-cs/blob/dev/2019_ss/entGamiAufgaben/AB4/AB4-02-ZyklischerGraph/AB4-GraphZyklen-klar.md) |
|03 | Klasseninvariante     | AB4-Klasseninvariante(uf) | Geschätzt:  ?? (Grob Probegelöst 1,5h)| ?? | [Link](https://git.thm.de/cslz90/ad-cs/blob/dev/2019_ss/entGamiAufgaben/AB4/AB4-03-Klasseninvariante/AB4-Klasseninvariante-klar.md) |
|04 | Binärbaum                 | Binäre Suchbaum                | ??                            | ??                |[Link](https://git.thm.de/cslz90/ad-cs/blob/dev/2019_ss/entGamiAufgaben/AB4/AB4-04-Bin%C3%A4rerSuchbaum/AB4-Bin%C3%A4rerSuchbaum-klar.md)|

## Bonusaufgaben

|Nr | Thema                     | Name                                 | Dauer in Sdt (für Studis)      | Punkte Gildamesh  | Ref |
|---| ---                       | ---                                  | ---                            |               --- | --- |
|01 | Balanciertheit            | Balancierter Binärbaum(uf)                | Geschätzt:  1h   Probegelöst: ca. 30 min|   ??    |[Link](https://git.thm.de/cslz90/ad-cs/blob/dev/2019_ss/entGamiAufgaben/AB4/Bonus/AB4-Bonus-BalancierteB%C3%A4ume-Bin%C3%A4rborkigeSpeichereichenmodels/AB4-Bonus-BalancierteB%C3%A4ume-Bin%C3%A4rborkigeSpeichereichenmodels-klar.md)  |
|02 | Traversierung             | AB4 - DFS-Iterator(uf)                    | Geschätzt:  2,5h Probegelöst ca. 30min|    ??     |[Link](https://git.thm.de/cslz90/ad-cs/blob/dev/2019_ss/entGamiAufgaben/AB4/Bonus/AB4-Bonus-Iteratoren-DFS/AB4-Bonus-Iteratoren-DFS-klar.md)  |
|03 | Traversierung             | AB4 - BFS-Iterator(uf)                    | Geschätzt:  2,5h              |  ??               |[Link](https://git.thm.de/cslz90/ad-cs/blob/dev/2019_ss/entGamiAufgaben/AB4/Bonus/AB4-Bonus-Iteratoren-BFS/AB4-Bonus-Iteratoren-BFS-klar.md) |
|04 | Traversierung             | AB4 - IDFS-Iterator(uf)                  | Geschätzt:  5:15 (3h) Probegelöst: 1,75h|    ??   |[Link](https://git.thm.de/cslz90/ad-cs/blob/dev/2019_ss/entGamiAufgaben/AB4/Bonus/AB4-Bonus-Iteratoren-IDFS/AB4-Bonus-Iteratoren-IDFS-klar.md) |
|05 | Binärbaum                 | Strukturgleicheit                     |       ??                      |   ??              |[Link](https://git.thm.de/cslz90/ad-cs/tree/dev/2019_ss/entGamiAufgaben/AB4/Bonus/AB4-Strukturgleichheit)|