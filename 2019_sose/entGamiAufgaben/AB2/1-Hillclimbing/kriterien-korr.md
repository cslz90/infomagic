# Bewertungsvorschläge


## Grün Faktoren:

  - Dozentron nimmt die Eingabe an.
  - Unit-Tests werden bestanden.
  - Gier ist im Code erkennbar.

## Gelb Kriterien:

 - Der SmallHill Test ist richtig, der Rest nicht. 
 - Hier entscheidet, ob Gier probiert wurde


## Unit-Tests
  - Wenn die hidden Tests fehlschlagen sollte genauer auf den Code geguckt werden. Möglicherweise ist es nur ein kleiner Flüchtigkeitsfehler.
 - Hierbei hilft der SmallHill-Test. 

## Rot:

  - Es wird keine Gier verwendet.
