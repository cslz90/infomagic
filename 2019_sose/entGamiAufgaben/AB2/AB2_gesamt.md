# Hillclimbing

Schreiben Sie eine Methode, die einen gierigen Algorithmus zum Hill Climbing implementiert: Gegeben ist eine 2D-Karte (2D-Array) mit Höhenlevels. Der Algorithmus startet an einem beliebigen Punkt auf dieser Karte und soll den "Gipfel" erklimmen. Der Algorithmus darf in jedem Schritt nur die vier benachbarten Felder betrachten (D-Nachbarschaft). In jedem Schritt muss der Algorithmus den höchsten Nachbarn wählen und sich auf diesen fortbewegen, wenn er höher ist als die aktuelle Position (wenn es auf diese Art nicht weitergeht ist der Algorithmus fertig).

Nutzen Sie einen einfachen Listener, um die Schritte des Algorithmus nachzuverfolgen. Dieser besteht hauptsächlich aus einer Callback-Methode, die in jedem Schritt gerufen wird.


#### Hinweise:
> - In der .jar mit den Unit-Tests finden Sie eine Methode, mit der Sie Karten generieren können.
> - Die Schnittstellen der zu implementierenden Klassen (siehe unten) sind mit Javadoc-Kommentaren versehen. Nutzen Sie diese als informelle Richtlinie.


Folgende Formalia sollen eingehalten werden:
- Implementieren Sie die Methode `climb(...)` aus der Klasse `Hillclimber` (siehe Codevorgaben).
- Implementieren Sie die Methoden `climbCallback(...)` und `getPath()` der Klasse `ClimbListener`.
- Für jeden Schritt den der Algorithmus geht muss genau einmal `ClimbListener.climbCallback(...)` aufgerufen werden.
- Wenn die `climb(...)` Methode durchgelaufen ist, muss die von `getPath()` zurückgegebene Liste alle durchlaufenen Koordinaten enthalten (inklusive Start- und Endpunkt).
- Die beiden zu implementierenden Klassen müssen im package `hillclimbing` liegen.
- Bei gleichwertigen benachbarten Feldern ist es Ihnen überlassen, welches Sie wählen. Ihre Entscheidung sollte nur konsistent sein.


Die Skelette der beiden zu implementierenden Klassen sehen so aus:

```java
public class Hillclimber {

    private int[][] hill;

    public Hillclimber(int[][] hill) {
        this.hill = hill;
    }

    /**
     * Implements a greedy algorithm to "climb" a 2d-array with height levels.
     *
     * @param start         the starting point
     * @param climbListener a listener whose climbCallback method is called for each position the climbing algorithm is on
     * @return the top of the hill
     */
    public Coordinate climb(Coordinate start, ClimbListener climbListener) {
        return null;
    }
}
```

```java
public class ClimbListener {

    /**
     * Returns all positions stored by the climbCallback method. The output is sorted chronologically by the time of insertion.
     *
     * @return the list of positions
     */
    public List<Coordinate> getPath() {
        return null;
    }

    /**
     * To be used in the hillclimbing exercise. This function must be called for each step the solving algorithm takes.
     * The argument is stored in an internal datastructure that can be read by using the getPath() method.
     *
     * @param newPosition the position to store
     */
    public void climbCallback(Coordinate newPosition) {
    }
}
```
Die Klasse `Coordinate` ist bereits implementiert, Sie müssen sie nur benutzen:

```java
package hillclimbing;

import java.util.Objects;

public class Coordinate {
    public final int x, y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return x == that.x &&
                y == that.y;
    }

    @Override
    public int hashCode() {

        return Objects.hash(x, y);
    }
}

```

<div style="page-break-after: always;"></div>
# Apothekerwaage

Schreiben sie ein Programm, das einen **Backtracking**-Algorithmus zum Ausgleichen einer Apothekerwaage implementiert. Gegeben ist ein Zielgewicht und eine unendliche Menge Gegengewichte, von denen jeweils beliebig viele benutzt werden dürfen. Die Gegengewichte setzen sich nach folgendem Schema zusammen:

> 0, 2, 7, 20, 70, 200, 700, 2000, ...

Sie sollen nacheinander auf die Waage gelegt werden, bis das Zielgewicht erreicht ist. Wird das Zielgewicht überschritten, muss das letzte Gewicht wieder heruntergenommen und ausgetauscht werden. Führt dies immer noch nicht zum Ziel, müssen zwei Schritte zurückgegangen werden. Finden Sie die **optimale** Kombination an Gewichten, d.h. die Anzahl der benutzten Gegengewichte soll minimal sein.

## Aufgabe:

Sie erhalten als Input eine beliebige Anzahl zufällig generierter, durch Zeilenumbrüche getrennter Gewichte.
Geben Sie im Output-Feld für jede Input-Zeile die optimale Kombination an oben genannten Gegengewichten an und schreiben Sie sie wie im Beispiel (siehe unten) gezeigt auf.

### Bitte beachten:
- Die Ausgabezeilen müssen der Reihenfolge der Eingabezeilen folgen.
- Innerhalb der Ausgabemengen sollte es keine Leerzeichen oder anderen Whitespace geben.
- Es dürfen keine Leerzeilen im Output stehen!
- Bitte reichen Sie Ihren Quellcode zur Korrektheitsprüfung ein.
- Ihr Programm muss Backtracking benutzen!
- Jedes Gegengewicht darf mehrmals verwendet werden.
- Der Generator wird keine Gewichte generieren, die sich nicht mithilfe von Gegengewichten aus dieser Menge lösen lassen.
- Sie müssen die **optimale** Kombination an Gewichten, d.h. die geringst mögliche Anzahl der benutzten Gegengewichte finden.

## Beispiel:

**Generierter Input**:

```  
80
141
0
777
```

**Ihr Output**:  

```
20,20,20,20
70,20,20,20,7,2,2
0
700,70,7
```


<div style="page-break-after: always;"></div>
# Master-Theorem

### Vorwort

Bei der folgenden Aufgabe handelt es sich um eine Rechercheaufgabe. Es gelten dieselben Regeln, wie bei der Rechercheaufgabe zur O-Notation auf Aufgabenblatt 1.

### Aufgabe 1.1

Recherchieren Sie das Master-Theorem (deutsch "Hauptsatz der Laufzeitfunktionen") zur Bestimmung der Effizienzklasse von rekursiven Funktionen.
Beantworten Sie dabei in maximal 700 Worten Fließtext (mit vollständigen Sätzen ohne Stichpunkte!) die folgenden Fragen:

* Wie lautet die Aussage des Master-Theorems? Versuchen Sie die mathematische Aussage in verständlicheren Worten zu erklären.
* Wozu wird das Master-Theorem verwendet?
* Was ist die Mastermethode? Wie hängt Sie mit dem Master-Theorem zusammen?
* Geben Sie ein Beispiel für die Anwendung des Master-Theorems auf den Mergesort-Algorithmus. Wie hilft das Theorem, um die Laufzeit von Mergesort zu bestimmen? Welchen Wert nehmen die darin verwendeten Konstanten an?

**Hinweise:**

* Bitte achten Sie auf eine korrekte Rechtschreibung und Grammatik, auch diese fließt in die Bewertung ein.
* Antworten Sie hier bitte in Klartext.

### Aufgabe 1.2

Geben Sie die Quellen an, die Sie für Ihre Recherche verwendet haben. Hier reicht jeweils ein Link auf die Webseite oder der Titel des Buches, das Sie verwendet haben.
Es geht hier nicht darum herauszufinden, ob sie Formulierungen kopiert haben (was Sie natürlich nicht tun sollten), sondern darum, dass Sie zeigen, dass Sie in der Lage sind, geeignete Quellen zu finden und auszuwählen.

Bitte geben Sie auch nicht *alle* Quellen an, die Sie gelesen haben, sondern nur diejenigen, auf deren Inhalt Sie sich in Ihrem Text stützen.
Es sind maximal fünf Quellen erlaubt.

<div style="page-break-after: always;"></div>
# Tribonati-Folge

## Aufgabe 1.1

Erstellen Sie eine Klasse `Tribonati` im package `tribonati` und implementieren Sie darin die Methode `static long tribDyn(int n, long[] brain)`. Die Methode `tribDyn()` soll das `n`-te Element folgender Zahlenfolge berechnen:

> 0, 1, 1, 2, 4, 7, 13, 24, ...

Verwenden Sie dafür die in der Vorlesung kennengelernte algorithmische Technik der **Dynamischen Programmierung**.

### Hinweise
* Achten Sie darauf, dass sich die Klasse im richtigen Package befindet.
* Benennen Sie die Methode genau wie in der Aufgabenstellung beschrieben und achten Sie auf korrekte Typen.

## Aufgabe 1.2

Beantworten Sie in eigenen Worten am Beispiel des in 1) bearbeiteten Algorithmus folgende Fragen:
* Warum führt Dynamische Programmierung hier zu einem schnelleren Algorithmus im Gegensatz zur Implementierung ohne Dynamische Programmierung? (Geben Sie ebenfalls die Laufzeit in der O-Notation an.)
* Wann ist es sinnvoll, Dynamische Programmierung einzusetzen?

### Hinweise
* Die zwei Fragen können in einem Text beantwortet werden.
* Ein Blick ins Vorlesungsskript kann hilfreich sein.
