# Bewertungsvorschläge

## Positive Faktoren
	- Matrizen wurden berechnet
	- Die Entfernungen stimmen
	- File IO ist implementiert und die Daten sind nicht "HARDCODED" :D

## Weiche Kriterien
	- Die Entfernungen sind um 1 niedriger (inkorrekter formulierung von Abbruchbedingungen)

## Ungültig:

  - Es wird kein Backtracking verwendet.
	- Es wird eine Bibliotheksfunktion verwendet (vorsicht z.B. bei Python).
