# Bewertungsvorschläge

## Positive Faktoren:

  - Dozentron nimmt die Eingabe an.
  - Unit-Tests werden bestanden.
  - Backtracking ist im Code erkennbar.

## Weiche Kriterien:


## Unit-Tests

  - Sollte es jemand schaffen, die hidden Tests zu bestehen, aber die sichtbaren nicht (unwahrscheinlich), sollte er trotzdem locker bestehen.

## Ungültig:

  - Es wird kein Backtracking verwendet.
  - Die hidden Tests werden nicht bestanden.
