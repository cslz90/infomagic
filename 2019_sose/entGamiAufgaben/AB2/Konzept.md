# Aufgabenblatt 2

## Pflichtaufgaben

| Nr  | Thema                     | Name            | Typ      | Dauer in min | Ref                                                              |
| --- | ---                       | ---             | ---      | ---          | ---                                                              |         
| 01  | Gier                      | Hillclimbing    | Unittest | 180          | [Link](2019_ss/entGamiAufgaben/AB2/1-Hillclimbing/aufgabe.md)    |         
| 02  | Backtracking              | Apothekerwaage  | IO       | 120?         | [Link](2019_ss/entGamiAufgaben/AB2/2-Apothekerwaage/aufgabe.md)  |         
| 03  | Teile und Herrsche        | Master-Theorem  | Text     | 120?         | [Link](2019_ss/entGamiAufgaben/AB2/3-Master-Theorem/aufgabe.md)  |
| 04  | Dynamische Programmierung | Tribonati-Folge | Unittest | 120?         | [Link](2019_ss/entGamiAufgaben/AB2/4-Tribonati-Folge/aufgabe.md) |

## Bonusaufgaben

| Nr  | Thema                     | Name                | Typ      | Dauer in Std. (gemessen)                        | Dauer in Sdt (für Studis) | Ref                                                                        |
| --- | ---                       | ---                 | ---      | ---                                             | ---                       | ---                                                                        |
| 01  | Backtracking              | Damenproblem        | Unittest | 2h (geschätzt vom Ersteller)                    | ???                       | [Link](2019_ss/entGamiAufgaben/AB2/Bonus/1-Damenproblem/aufgabe.md)        |
| 02  | Dynamische Programmierung | Levenshtein-Distanz | IO       | 2h (geschätzt vom Ersteller), Probegelöst in 2h | ???                       | [Link](2019_ss/entGamiAufgaben/AB2/Bonus/2-Levenshtein-Distanz/aufgabe.md) |