# Korrekturschema:

## Kriterien fürs Nichtbestehen

* keine Quellenangabe
* keine vollständigen Sätze
* völlig unverständlicher Text
* über 1400 Worte Text
* über 10 Quellen

## Punkteverteilung (100 gesamt)

* **15 Punkte** pro korrekt beantworteter Frage (**60 gesamt**)
* **20 Punkte** für fehlerfreien Text (-1 Punkt pro Fehler, die ersten 10 werden ignoriert)
* **20 Punkte** für verständliche Formulierungen
* **-20 Punkte** für das Überschreiten der Zeichengrenze
* **-20 Punkte** für unverlässliche/unpassende Quellen
* **-20 Punkte** für das Überschreiten der Quellengrenze


## Grün: 

 - Alle Fragen beantwortet (inlc. Teil-Fragen!)
 - Lesbarer Text, Grammatik / Rechtschreibfehler erst wenns krass wird.
 - Max 700 Wörter
 - Max 5 Quellen 
 - Gute Erklärungen - nicht nur "oberflächlich"
 - (kein Geeks-for-Geeks in den Quellen)

## Rot:

 - > 1400 Wörter
 - > 10 Quellen
 - Kein Verständnis erkennbar, nur Oberflächlich + rauskopiert/falsch.
