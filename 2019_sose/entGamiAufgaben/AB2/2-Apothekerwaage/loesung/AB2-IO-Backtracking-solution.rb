require 'set'

def weights127 zeros
  # start greedy (use largest weight)
  res = Set.new()
  sizes = [7, 2, 1]
  zeros.downto(0) do |z|
    sizes.each do |s|
      res << s * (10 ** z)
    end
  end
  return res
end

def find_weights127 target, brain: Hash.new(), weights: nil, solution: [], max_weights: Float::INFINITY
  return nil if target < 0 || solution.size >= max_weights
  return solution if target == 0
  weights = weights127(Math::log(target, 10).round(3).floor) if weights.nil?
  #b = brain[[target, weights]]
  #return solution + b unless b.nil?
  best_solution = nil
  min_weights = max_weights
  weights.each do |w|
    weights.delete(w)
    res = find_weights127(target - w, brain: brain, weights: weights.clone, solution: solution + [w], max_weights: min_weights)
    if !res.nil? && res.size < min_weights then
      min_weights = res.size
      best_solution = res
    end
  end
  #brain[[target, weights]] = best_solution[solution.size..best_solution.size] unless best_solution.nil?
  return best_solution
end

File.open("input.txt", "r:utf-8") do |f|
  f.each_line do |x| 
    puts find_weights127(x.to_i).join(",")
  end
end

multibrain = Hash.new()
1000.times do |i|
  w = find_weights127(i, brain: multibrain)
  ##puts "#{i} => #{w.inspect}" if w.nil?
end
puts find_weights127(140).inspect