import std.stdio: writeln;
import std.math: log, pow, lrint;
import std.algorithm: map, filter;
import std.container.dlist: DList;
import std.file: readText, write, append;
import std.conv: to;
import std.string: format, strip;
import std.typecons: Flag, Yes, No;
import std.range: join, split, array;
import std.random: uniform;
import std.datetime.stopwatch: benchmark, Duration, StopWatch, AutoStart;
import weight27.backtracking: backOpt27, Solution, Brain, DummyBrain, HashBrain, DynamicArrayBrain, NaiveTask, SortedTask, YesNoTask, CompressedYesNoTask;
import weight27.dynamic: dynamic27, D2ArrayBrain;
import weight27.bfs: bfs27;
import weigth27.iddfs: iddfs27;
import weight27.util: gen27;

void saveResults(string fname, int[][] res) {
  string formatWeights(int[] weights) {
    if (weights.length == 0) {
      return "0";
    } else {
      return weights.map!(x => x.to!string()).join(",");
    }
  }
  auto outString = res.map!(x => formatWeights(x)).join("\n");
  write(fname, outString);
}

Duration[string] speedCheckTarget(int target, int reps = 1000, int bfs_reps = 10, int noopt_reps = 10) {
  void bench_bfs() {
    bfs27(target);
  }
  void bench_iddfs() {
    iddfs27(target);
  }
  void bench_backOpt() {
    Brain b = new DynamicArrayBrain(target);
    backOpt27(new CompressedYesNoTask(target), b);
  }
  void bench_backOpt_onlyBB() {
    Brain b = new DummyBrain();
    backOpt27(new CompressedYesNoTask(target), b, Yes.useBB);
  }
  void bench_backOpt_onlyDP() {
    Brain b = new DynamicArrayBrain(target);
    backOpt27(new CompressedYesNoTask(target), b, No.useBB);
  }
  void bench_backOpt_noopt() {
    Brain b = new DummyBrain();
    backOpt27(new NaiveTask(target), b, No.useBB);
  }
  auto time_bfs = benchmark!(bench_bfs)(bfs_reps)[0];
  auto time_noopt = benchmark!(bench_backOpt_noopt)(noopt_reps)[0];
  auto times = benchmark!(
    bench_iddfs, bench_backOpt,
    bench_backOpt_onlyBB, bench_backOpt_onlyDP
  )(reps);
  return [
    "bfs": (bfs_reps == 0 ? Duration.zero : (time_bfs / bfs_reps)),
    "iddfs": times[0] / reps,
    "backOpt": times[1] / reps,
    "backOpt_onlyBB": times[2] / reps,
    "backOpt_onlyDP": times[3] / reps,
    "backOpt_noopt": (noopt_reps == 0 ? Duration.zero : (time_noopt / noopt_reps))
  ];
}

Duration[string] speedCheckRandom(int digits, int reps = 1000) {
  int magnitude = pow(10, digits-1);
  int start = magnitude;
  int end = 10 * magnitude;
  Brain sharedBrain = new HashBrain();
  void bench_bfs() {
    bfs27(uniform(start, end));
  }
  void bench_iddfs() {
    iddfs27(uniform(start, end));
  }
  void bench_backOpt() {
    backOpt27(uniform(start, end));
  }
  void bench_backOpt_shared() {
    backOpt27(new NaiveTask(uniform(start, end)), sharedBrain);
  }
  auto times = benchmark!(bench_bfs,bench_iddfs,bench_backOpt, bench_backOpt_shared)(reps);
  return [
    "bfs": times[0],
    "iddfs": times[1],
    "backOpt": times[2],
    "backOpt_shared": times[3]
  ];
}

void createBenchmark() {
  write("res/benchmark.csv", "input;bfs;iddfs;backOpt;backOpt_onlyBB;backOpt_onlyDP;backOpt_noopt;solution\n");
  for(int i = 7; i < 1000; i++) {
    writeln("Benchmark for input %d".format(i));
    Duration[string] speed;
    if (i < 80){
      speed = speedCheckTarget(i, 1000, 10, 10);
    } else if (i < 200) {
      speed = speedCheckTarget(i, 100, 1, 0);
    } else {
      speed = speedCheckTarget(i, 10, 1, 0);
    }
    auto solution_bfs = bfs27(i);
    auto solution_iddfs = iddfs27(i);
    auto solution_backOpt = backOpt27(i);
    if (solution_bfs.length != solution_iddfs.length || solution_bfs.length != solution_backOpt.length) {
      writeln("ERROR: differing solutions for %d: %s != %s != %s".format(
        i, solution_bfs, solution_iddfs, solution_backOpt
      ));
    }
    append(
      "res/benchmark.csv",
      "%d;%d;%d;%d;%d;%d;%d;%s\n".format(
        i,
        speed["bfs"].total!"nsecs",
        speed["iddfs"].total!"nsecs",
        speed["backOpt"].total!"nsecs",
        speed["backOpt_onlyBB"].total!"nsecs",
        speed["backOpt_onlyDP"].total!"nsecs",
        speed["backOpt_noopt"].total!"nsecs",
        solution_bfs
      )
    );
  }
}

void createSingleBenchmark(string fname, int[] function(long) testFunc) {
  write(fname, "input;nsecs;solution\n");
  int max_reps = 1_000;
  int max_nsecs = 100_000_000;
  long target = 0;
  // use prime stepsizes, sources: https://primes.utm.edu/lists/small/millions/, https://primes.utm.edu/lists/small/100000.txt
  long[] stepSizes = [1,13,107,1_171,11_287,127_931,1_233_259,15_485_863,122_949_829,982_451_653];
  int stepIndex = 0;
  while(target >= 0) { // stop at overflow, if we should ever reach it
    StopWatch timer = StopWatch(AutoStart.yes);
    int[] res = testFunc(target);
    timer.stop();
    long nsecs = timer.peek.total!"nsecs";
    writeln("%d msecs for %d".format(timer.peek.total!"msecs", target));
    if (nsecs > max_nsecs) break;
    // find suitable number of repetitions
    int reps = 0;
    for(int r = max_reps; r > 0; r /= 10) {
      if (nsecs * r < max_nsecs) {
        reps = r;
        break;
      }
    }
    // test 1000 numbers for each step size
    if (target / stepSizes[stepIndex] > 1000 && stepIndex < stepSizes.length) {
      stepIndex++;
      //writeln("increasing step size to %d, current #repetitions: %d".format(stepSizes[stepIndex], reps));
    }
    // now do the actual benchmark
    auto time = benchmark!(() => testFunc(target))(reps);
    append(fname, "%d;%d;%s\n".format(target, lrint(1.0 * time[0].total!"nsecs" / reps), res));
    target += stepSizes[stepIndex];
  }
}

void main()
{
	auto txt = readText("res/input.txt");
  auto inputs = txt.split("\n")
    .map!(x => strip(x))
    .filter!(x => x.length > 0)
    .map!(x => to!int(x));
  Brain brain;
  //foreach(int inp; inputs) {
  //  writeln(inp);
  //  //writeln(iddfs27(inp));
  //  writeln(backOpt27(inp, No.useDP, No.populateDP, No.useBB));
  //}
  
  auto res = inputs.map!((x) {Brain b = new DummyBrain(); return backOpt27(new CompressedYesNoTask(x), b, Yes.useBB);}).array;
  saveResults("res/output.txt", res);
  
  //writeln(speedCheckRandom(3, 10));

  //createBenchmark();

  //int target = 651294680;
  //Brain b = new DummyBrain(); //new DynamicArrayBrain(target);
  //int[] res = backOpt27(new CompressedYesNoTask(target), b, Yes.useBB);
  //writeln(res);
  
  //int[] function(long) f = (x) {
  //  Brain b = new DummyBrain();
  //  return backOpt27(new CompressedYesNoTask(cast(int)x), b, Yes.useBB);
  //};
  //createSingleBenchmark("res/bench_backOpt_CompressedYesNoTask_bb.csv", f);

  //createSingleBenchmark("res/bench_dynamic.csv", x => dynamic27(cast(int)x));

  //writeln(dynamic27(131_534_509));

  //backOpt27(2, brain);
  //backOpt27(81, brain);
  //backOpt27(250786, brain);
  //backOpt27(1630569, brain);
  //backOpt27(1244084, brain);
  //backOpt27(18033, brain);
  //backOpt27(406, brain);
  //backOpt27(2932044, brain);
  //writeln("foo");
  //writeln(brain[998064].weightsUsed[]);
  //writeln(backOpt27(998064, brain));
  
  //writeln(backOpt27(998064));
  //writeln(iddfs27(998064));
  // Example: 4932 => [2, 20, 70, 70, 70, 700, 2000, 2000]
  //writeln(bfs27(4932)); // needs 800 MB RAM
  //writeln(iddfs27(4932)); // needs 1.4 MB RAM 
  //writeln(backOpt27(4932)); // needs 2.5 MB RAM
}
