module weight27.dynamic;

import weight27.util: gen27;
import std.array: appender;
import std.stdio: writeln;
import std.string: format;

interface DynamicBrain {
  int[] opIndex(int weight);
  int opIndexAssign(int lastWeight, int i);
}

class D2ArrayBrain: DynamicBrain {
  byte[] ar;
  int[] counterWeights;
  byte[int] weightIndices;
  this(int weight) {
    ar.length = weight+1;
    counterWeights = gen27(weight);
    for(byte i = 0; i < counterWeights.length; i++) {
      weightIndices[counterWeights[i]] = i;
    }
  }
  int[] opIndex(int weight) {
    //writeln("get %d".format(weight));
    auto app = appender!(int[]);
    int i = weight;
    while(i > 0 && ar[i] >= 0) {
      int w = counterWeights[ar[i]];
      app ~= w;
      i -= w;
    }
    if (i > 0) return [];
    //writeln("result: %s".format(app.data));
    return app.data;
  }
  // todo seperate function just for solution length?
  int opIndexAssign(int lastWeight, int i) {
    //writeln("set %d to %d".format(i, lastWeight));
    if (lastWeight < 0) {
      ar[i] = -1;
    } else {
      ar[i] = weightIndices[lastWeight];
    }
    return lastWeight;
  }
}

int[] dynamic27(int target) {
  DynamicBrain b = new D2ArrayBrain(target);
  return dynamic27(target, b);
}

int[] dynamic27(int target, ref DynamicBrain brain) {
  return dynamic27(target, brain, gen27(target));
}

int[] dynamic27(int target, ref DynamicBrain brain, int[] counterWeights) {
  for(int current = 1; current <= target; current++) {
    //writeln("current: %d".format(current));
    int best = -1;
    int bestLength = int.max;
    foreach(int w; counterWeights) {
      if(current-w < 0) continue;
      // TODO: we can further reduce the number of candiates
      // idea: branch & bound - like?
      int sl = brain[current-w].length;
      if (sl < bestLength && sl > 0 || current == w) {
        best = w;
        bestLength = sl;
      }
    }
    brain[current] = best;
  }
  return brain[target];
}