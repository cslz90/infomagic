module weight27.bfs;

import weight27.util: gen27;
import std.container.slist: SList;
import std.container.dlist: DList;
import std.range: walkLength, array;

// TODO use task class

class Balance {
  int remaining;
  // TODO rename this to "weightsUsed" or "usedWeights"
  SList!int counterWeights;
  int weightCount;
  this(int remaining) {
    this(remaining, SList!int(), 0);
  }
  this(int remaining, SList!int counterWeights) {
    auto count = walkLength(counterWeights[]); // TODO is this correct?
    this(remaining, SList!int(), count);
  }
  this(int remaining, SList!int counterWeights, int weightCount) {
    this.remaining = remaining;
    this.counterWeights = counterWeights;
    this.weightCount = weightCount;
  }
  Balance addWeight(int w) {
    auto nextWeights = this.counterWeights.dup();
    nextWeights.insertFront(w);
    return new Balance(remaining - w, nextWeights, this.weightCount + 1);
  }
  int[] toArray() {
    return array(this.counterWeights[]);
  }
}

int[] bfs27(int inputWeight) {
  auto counterWeights = gen27(inputWeight);
  auto q = DList!Balance();
  q.insertFront(new Balance(inputWeight, SList!int()));
  bool[int] visited;
  while (!q.empty) {
    auto b = q.back;
    visited[b.remaining] = true;
    q.removeBack();
    //writeln(b.remaining);
    if (b.remaining == 0) {
      return b.toArray();
    }
    if (b.remaining < 0) {
      continue;
    }
    // for each counterWeight, put a new balance in the queue
    foreach (int w; counterWeights) {
      auto next = b.addWeight(w);
      if (! (next.remaining in visited)) {
        q.insertFront(next);
      }
    }
  }
  return null;
}