module weigth27.iddfs;

import weight27.util: gen27;
import std.typecons: Nullable;
import std.container.slist: SList;
import std.range: array;

// TODO use Task class

Nullable!(SList!int) dfs27(int inputWeight, int[] counterWeights, int maxDepth = int.max) {
  if (inputWeight == 0) {
    Nullable!(SList!int) res;
    res = SList!int();
    return res;
  }
  if (inputWeight < 0) {
    return Nullable!(SList!int)();
  }
  if (maxDepth <= 0) {
    return Nullable!(SList!int)();
  }
  foreach(int w; counterWeights) {
    auto res = dfs27(inputWeight - w, counterWeights, maxDepth - 1);
    if (!res.isNull) {
      res.get.insertFront(w);
      return res;
    }
  }
  return Nullable!(SList!int)();
}

int[] iddfs27(int inputWeight) {
  auto counterWeights = gen27(inputWeight);
  Nullable!(SList!int) res;
  int maxDepth = 0;
  while (res.isNull) {
    res = dfs27(inputWeight, counterWeights, maxDepth);
    maxDepth += 1;
  }
  if (res.isNull) {
    return null;
  } else {
    return array(res.get[]);
  }
}