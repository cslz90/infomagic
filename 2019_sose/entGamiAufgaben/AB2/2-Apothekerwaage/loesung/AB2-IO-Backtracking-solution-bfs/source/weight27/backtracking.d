module weight27.backtracking;

import weight27.util: gen27;
import std.typecons: Flag, Yes, No;
import std.container.slist: SList;
import std.range: walkLength, array, InputRange, InputRangeObject, inputRangeObject, take;
import std.stdio: writeln;
import std.string: format;
import std.algorithm: map;

class Solution {
  SList!int weightsUsed;
  int weightCount;
  bool isValid;
  this(bool isValid) {
    this(SList!int(), 0, isValid);
  }
  this(SList!int weightsUsed) {
    this(weightsUsed, walkLength(weightsUsed[]));
  }
  this(SList!int weightsUsed, int weightCount) {
    this(weightsUsed, weightCount, true);
  }
  this(SList!int weightsUsed, int weightCount, bool isValid) {
    this.weightsUsed = weightsUsed;
    this.weightCount = weightCount;
    this.isValid = isValid;
  }
  Solution addWeight(int w) {
    return addWeights([w]);
  }
  Solution addWeights(int[] weights) {
    auto nextWeights = this.weightsUsed.dup();
    foreach(int w; weights) {
      nextWeights.insertFront(w);  
    }
    return new Solution(nextWeights, this.weightCount + weights.length, true);
  }
  Solution removeWeight(int w) {
    auto nextWeights = this.weightsUsed.dup();
    nextWeights.linearRemoveElement(w);
    return new Solution(nextWeights, this.weightCount - 1, true);
  }
  int[] toArray() {
    return this.weightsUsed.array;
  }
}

class NotSupportedException: Exception {
  this() {
    super("opration not supported");
  }
}

interface Brain {
  auto ref Solution opIndex(int weight);
  ref Solution opIndexAssign(ref Solution v, int i);
  bool contains(int weight);
  final void populate(int task, ref Solution s) {
    if (!this.contains(task)) this[task] = s;
    foreach(int w; s.weightsUsed) {
      // if we remove one weight, the remaining weights must be the solution
      // for the remaining task, otherwise the base solution would not be optimal
      int newTask = task - w;
      if (this.contains(newTask)) continue;
      Solution newSolution = s.removeWeight(w);
      this.populate(newTask, newSolution);
    }
  }
}

class DummyBrain: Brain  {
  ref Solution opIndex(int weight) { throw new NotSupportedException(); }
  ref Solution opIndexAssign(ref Solution v, int weight) { return v; }
  bool contains(int weight) { return false; }
}

class HashBrain: Brain {
  Flag!"populate" pop;
  this(Flag!"populate" pop=Yes.populate) {
    this.pop = pop;
  }
  Solution[int] hash;
  auto ref Solution opIndex(int weight) {
    return hash[weight];
  }
  ref Solution opIndexAssign(ref Solution v, int weight) {
    hash[weight] = v;
    if (this.pop) this.populate(weight, v);
    return v;
  }
  bool contains(int weight) {
    auto pt = weight in hash;
    return pt !is null;
  }
}

class DynamicArrayBrain: Brain {
  int[] ar;
  Flag!"populate" pop;
  this(int targetWeight, Flag!"populate" pop=Yes.populate) {
    ar.length = targetWeight+1;
    ar[] = -1;
    ar[0] = 0;
    this.pop = pop;
  }
  auto ref Solution opIndex(int weight) {
    SList!int weightsUsed;
    int current = weight;
    while(ar[current] > 0) {
      weightsUsed.insertFront(current - ar[current]);
      current = ar[current];
    }
    return new Solution(weightsUsed);
  }
  ref Solution opIndexAssign(ref Solution v, int weight) {
    if (weight >= ar.length) {
      // should never be the case, if the brain was initialized correctly
      // but we need to increase the array length if we receive a higher weight
      int oldLength = ar.length;
      ar.length = weight + 1;
      ar[oldLength..$] = -1;
    }
    int remaining = weight;
    foreach(int w; v.weightsUsed) {
      ar[remaining] = remaining - w;
      remaining = remaining - w;
    }
    if (this.pop) this.populate(weight, v);
    return v;
  }
  bool contains(int weight) {
    return ar.length > weight && ar[weight] >= 0;
  }
}

class ChildTask {
  Task task;
  int[] weightsChosen;
  this(Task task, int[] weightsChosen) {
    this.task = task;
    this.weightsChosen = weightsChosen;
  }
}

abstract class Task {
  immutable int[] counterWeights;
  immutable int targetWeight;
  ChildTask[] childTasks();
  this(int targetWeight) {
    immutable int[] counterWeights = gen27(targetWeight).idup;
    this(targetWeight, counterWeights);
  }
  this(int targetWeight, immutable int[] counterWeights) {
    this.targetWeight = targetWeight;
    this.counterWeights = counterWeights;
  }
}

// just use the same counter weights at each level
class NaiveTask: Task {
  this(int targetWeight) {
    super(targetWeight);
  }
  this(int targetWeight, immutable int[] counterWeights) {
    super(targetWeight, counterWeights);
  }
  override ChildTask[] childTasks() {
    return counterWeights.map!(w => new ChildTask(new NaiveTask(targetWeight - w, counterWeights), [w] )).array;
  }
}

// do not allow non-sorted solutions
class SortedTask: Task {
  this(int targetWeight) {
    super(targetWeight);
  }
  this(int targetWeight, immutable int[] counterWeights) {
    super(targetWeight, counterWeights);
  }
  override ChildTask[] childTasks() {
    ChildTask[] res;
    res.length = counterWeights.length;
    for(int i = 0; i < counterWeights.length; i++) {
      int w = counterWeights[i];
      // only use counterWeights that are of the same or smaller size as the
      // weight that is added in this step => disallow unsorted solutions
      res[i] = new ChildTask(new SortedTask(targetWeight - w, counterWeights[i..$]), [w]);
    }
    return res;
  }
}

// either take a weight or don't
class YesNoTask: Task {
  this(int targetWeight) {
    super(targetWeight);
  }
  this(int targetWeight, immutable int[] counterWeights) {
    super(targetWeight, counterWeights);
  }
  override ChildTask[] childTasks() {
    if (counterWeights.length == 0) { return []; }
    return [
      // use the highest weight once and keep it in the counterWeights
      new ChildTask(
        new YesNoTask(targetWeight - counterWeights[0], counterWeights),
        [counterWeights[0]]
      ),
      // do not use the highest weight and remove it from counterWeights
      new ChildTask(
        new YesNoTask(targetWeight, counterWeights[1..$]),
        []
      )
    ];
  }
}

class CompressedYesNoTask: Task {
  // like YesNoTask, but take maximum amount at once
  this(int targetWeight) {
    super(targetWeight);
  }
  this(int targetWeight, immutable int[] counterWeights) {
    super(targetWeight, counterWeights);
  }
  override ChildTask[] childTasks() {
    if (counterWeights.length == 0) { return []; }
    ChildTask[] res;
    int w = counterWeights[0];
    int times = targetWeight / w;
    // TODO: 6 is enough
    // TODO: this means if targetWeight > 5 * sum(counterWeights) there is no longer an optimal solution int his branch
    if (times >= 6) {
      // if this solution was correct, there would have been a shorter solution
      // using w*10 as weight
      times = 0;
    }
    int[] weightsChosen;
    weightsChosen.length = times;
    weightsChosen[] = w;
    res.length = times + 1;
    for(int i = times; i > 0; i--) {
      res[times-i] = new ChildTask(
        new CompressedYesNoTask(targetWeight - i * w, counterWeights[1..$]),
        weightsChosen[0..i]
      );
    }
    res[$-1] = new ChildTask(
      new CompressedYesNoTask(targetWeight, counterWeights[1..$]),
      []
    );
    return res;
  }
}

// TODO count how many nodes have been visited

int[] backOpt27(
  int inputWeight,
  Flag!"useBB" useBB = Yes.useBB
) {
  Brain brain = new HashBrain();
  Task task = new NaiveTask(inputWeight);
  return backOpt27(task, brain, useBB);
}

int[] backOpt27(
  Task task,
  ref Brain brain,
  Flag!"useBB" useBB = Yes.useBB
) {
  return backOpt27rek(task, brain, useBB).toArray();
}

Solution backOpt27rek(
  Task task,
  ref Brain brain,
  Flag!"useBB" useBB = Yes.useBB,
  int bestFullCount = int.max,
  int weightCount = 0
) {
  if (task.targetWeight == 0) {
    return new Solution(true);
  }
  if (task.targetWeight < 0) {
    return new Solution(false);
  }
  if (useBB && weightCount >= bestFullCount) {
    return new Solution(false); // branch & bound
  }
  if (brain.contains(task.targetWeight)) {
    // dynamic programming
    //writeln(format("found %d in brain: %s", task.targetWeight, brain[task.targetWeight].weightsUsed[]));
    auto res = brain[task.targetWeight];
    if (useBB && res.weightCount + weightCount < bestFullCount) {
      return res;
    } else {
      return new Solution(false);
    }
  }
  auto best = new Solution(false);
  foreach(ChildTask t; task.childTasks()) {
    auto res = backOpt27rek(t.task, brain, useBB, bestFullCount, weightCount + t.weightsChosen.length);
    if (res.isValid && (!best.isValid || res.weightCount + t.weightsChosen.length < best.weightCount)) {
      best = res.addWeights(t.weightsChosen);
      auto fullCount = best.weightCount + weightCount;
      //writeln(format("found solution %s at level %d", best.weightsUsed.array, weightCount));
      if (useBB && fullCount < bestFullCount) {
        //writeln(format("%d < %d at level %d", fullCount, bestFullCount, weightCount));
        bestFullCount = fullCount;
      }
    }
  }
  //if (weightCount == 5) { writeln("%d: %s".format(task.targetWeight, best.weightsUsed.array)); }
  if (best.isValid) {
    //writeln(format("found solution of size %d for %d at depth %d with bestFullCount = %d", best.weightCount, task.targetWeight, weightCount, bestFullCount));
    brain[task.targetWeight] = best;
  }
  return best;
}