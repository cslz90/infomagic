module weight27.util;

import std.array: appender;
import std.algorithm.mutation: reverse;

int[] gen27(int weight) {
  int i = 0;
  int w = 2;
  int pot = 1;
  auto app = appender!(int[]);
  while (w <= weight) {
    app ~= w;
    i += 1;
    if (i % 2 == 0) pot *= 10;
    w = i % 2 == 0 ? 2 * pot : 7 * pot;
  }
  reverse(app.data);
  return app.data;
}