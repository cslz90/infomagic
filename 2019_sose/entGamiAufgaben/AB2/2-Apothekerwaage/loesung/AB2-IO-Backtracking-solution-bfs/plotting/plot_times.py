import matplotlib.pyplot as plt

import numpy as np


def read_benchmark(fname):
    with open(fname, "r", encoding="utf-8") as f:
        raw = [x.split(";") for x in f.readlines()]
        headers = [x.strip() for x in raw[0]]
        raw = raw[1:]
        res = {}
        for i, h in enumerate(headers[:-1]):
            res[h] = [int(x[i]) for x in raw]
        res[headers[-1]] = [eval(x[-1]) for x in raw]
        return res


def find_differences(dres, jres):
    ddata = read_benchmark(dres)
    jdata = read_benchmark(jres)
    inputs = np.array(ddata["input"])
    dlengths = np.array([len(x) for x in ddata["solution"]])
    jlengths = np.array([len(x) for x in jdata["result"]])
    combined = np.column_stack([inputs, dlengths, jlengths])
    differences = combined[combined[:, 1] != combined[:, 2]]
    print(differences)
    print(len(differences))


def plot_times(fname):
    data = read_benchmark(fname)
    inputs = data["input"]
    keys = [x for x in data.keys() if x not in ["solution", "input"]]
    for k in keys:
        plt.semilogy(inputs, data[k], label=k)
    plt.legend(loc="best")
    plt.show()


def plot_times_by_solution_length(fname):
    data = read_benchmark(fname)
    inputs = np.array(data["input"])
    lengths = np.array([len(x) for x in data["solution"]])
    xvals = np.unique(lengths)
    indices_by_length = [inputs[lengths == x] for x in xvals]
    keys = [x for x in data.keys() if x not in ["solution", "input", "backOpt_noopt"]]
    for k in keys:
        plt.semilogy(xvals, [np.mean(np.array(data[k])[idx-7]) for idx in indices_by_length], label=k)
    plt.legend(loc="best")
    plt.show()

if __name__ == "__main__":
    #find_differences("../res/benchmark.csv", "../res/deworetzki_results.csv")
    #print("done")
    plot_times("../res/benchmark.csv")
    plot_times_by_solution_length("../res/benchmark.csv")
