# Bewertungsvorschläge


## Grün:

  - Dozentron nimmt die Eingabe an.
  - Unit-Tests werden bestanden.
  - Backtracking ist im Code erkennbar.

## Gelb:

  - Es wird nicht die optimale Lösung gefunden (mehr Zahlen als notwendig). D.h. die sichtbaren Tests werden bestanden, die unsichtbaren aber nicht.
  - Es werden falsche Gewichte benutzt, bzw die Menge an Gewichten ist nicht (theoretisch) unendlich.
  - Falsche Reihenfolge der Ausgabe (schwer zu Testen)

## Unit-Tests

  - Siehe "Weiche Kriterien".

## Rot:

  - Es wird kein Backtracking verwendet.
