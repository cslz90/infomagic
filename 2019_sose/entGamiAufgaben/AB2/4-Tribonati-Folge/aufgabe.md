# Tribonati-Folge

## Aufgabe 1.1

Erstellen Sie eine Klasse `Tribonati` im package `tribonati` und implementieren Sie darin die Methode `static long tribDyn(int n, long[] brain)`. Die Methode `tribDyn()` soll das `n`-te Element folgender Zahlenfolge berechnen:

> 0, 1, 1, 2, 4, 7, 13, 24, ...

Verwenden Sie dafür die in der Vorlesung kennengelernte algorithmische Technik der **Dynamischen Programmierung**.

### Hinweise
* Achten Sie darauf, dass sich die Klasse im richtigen Package befindet.
* Benennen Sie die Methode genau wie in der Aufgabenstellung beschrieben und achten Sie auf korrekte Typen.

## Aufgabe 1.2

Beantworten Sie in eigenen Worten am Beispiel des in 1) bearbeiteten Algorithmus folgende Fragen:
* Warum führt Dynamische Programmierung hier zu einem schnelleren Algorithmus im Gegensatz zur Implementierung ohne Dynamische Programmierung? (Geben Sie ebenfalls die Laufzeit in der O-Notation an.)
* Wann ist es sinnvoll, Dynamische Programmierung einzusetzen?

### Hinweise
* Die zwei Fragen können in einem Text beantwortet werden.
* Ein Blick ins Vorlesungsskript kann hilfreich sein.
