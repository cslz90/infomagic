# Bewertungsschema: 

## Grün:

Größenteils durch Tests:

- Das richtige Element, nicht eins daneben
- Dynamische Programmierung wurde verwendet (Dyn-Test hierfür hilfreich)
- Das Brain wurde korrekt verwendet (Brain-Tests hilfreich)

- Zweite Aufgabe auf Moodle !komplett! beantwortet
- Nicht nur oberflächlich / rauskopiert, Verständnis erkennbar!
- Laufzeit in passender O-Notation (ohne kürzbare Faktoren)

## Gelb:

unter anderem:
- Brain nur falsch verwendet / 3 Variablen benutzt (Aufgabenstellung!)

## Rot:

- Moodle komplett weggelassen
- Nicht Dynamisch Programmiert
- kurze, oberflächliche Antworten ohne erkennbares Verständnis


# Generell:

- Es wird von beiden Aufgaben die schlechtere Bewertung genommen -> Eins Rot - Beides Rot
- Wenn die Textaufgabe in Dozentron im Code abgegeben wurde -> Rot, aber Feedback auf den Text.