## Musterlösung für die Aufgabe "Tribonati"

### Aufgabe 1.2
Die rekursiven Aufrufe teilen sich einen Lösungsspeicher, d.h. jede Teilaufgabe muss nur einmal gelöst werden.
Kommt eine Teilaufgabe zum zweiten Mal vor, so kann er im Lösungsspeicher nachschlagen. Wenn man die den Algorithmus
zur Berechnung des n-ten Elements der Tribonacci-Folge ohne Dynamische Programmierung implementiert, hat er eine
Laufzeit von O(3^n) durch verzweigte Rekurion. 

Da hier Teilprobleme öfter gelöst werden müssen, ist es sinnvoll, Dynamische Programmierung zu verwenden, 
denn dadurch sinkt die Laufzeit auf O(n).
