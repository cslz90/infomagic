# Aufgabenblatt 1
## Pflichtaufgaben

|Nr | Thema                         | Name                      | Typ      | Dauer in min   | XP  | Ref |
|---| ---                           | ---                       | -------- | ---            | --- | --- |
|01 | Recherche                     | Recherche-O-Notation      | Text     | 180           | 23  |  [Link](./../../../2019_ss/entGamiAufgaben/AB1/AB1-Recherche-O-Notation/AB1-Recherche-O-Notation.md)  |
|02 | Brute-Force                   | Bruteforce-Passwörter    | Unittest | 60             | 5   |  [Link](./../../../2019_ss/entGamiAufgaben/AB1/AB1-Bruteforce-passwords/AB1-Bruteforce-passwords.md)  |
|03 | Laufzeitklassen               | Min-Max-Top5              | IO       | 45             | 3   |  [Link](./../../../2019_ss/entGamiAufgaben/AB1/AB1-Laufzeitklassen-Min-Max-Top5/AB1-Laufzeitklassen-Min-Max-Top5.md)  |
|04 | Laufzeitanalyse               | Laufzeitanalyse           | Text     | 20?            | 2   |  [Link](./../../../2019_ss/entGamiAufgaben/AB1/AB1-Laufzeitanalyse/AB1-Laufzeitanalyse.md)  |
|05 | Schleifeninvariante           | Schleifeninvarianten      | Text     | 20?            | 5   |  [Link](./../../../2019_ss/entGamiAufgaben/AB1/AB1-Schleifeninvarianten/AB1-Schleifeninvarianten.md)  |

## Bonusaufgaben (fast fertig)

| Thema                         | Name                                                  | Dauer in min   | XP  | Ref |
| ---                           | ---                                                   | ---            | --- | --- |
| LinkedList                    | DoppeltVerketteterBasilist                        |            ?   | ?   |  [Link](./../../../2019_ss/entGamiAufgaben/AB1/AB1-Bonus-DoppeltVerketteterBasilist/AB1-DoppeltVerketteterBasilist-klar.md)  |
| Gier                          | GreedyAlgorithmus-ChangeMaking                    |            ?   | ?   |  [Link](./../../../2019_ss/entGamiAufgaben/AB1/AB1-GreedyAlgorithmus-ChangeMaking/AB1-GreedyAlgorithmus-ChangeMaking-klar.md)  |
| Abstrakte Datentypen          | Abstrakter Datentyp: Matrix                       | ?             | ?     | [Link](./../../2019_ss/entGamiAufgaben/AB1/AB1-Bonus-AbstrakterDatentyp-Matrix/AB1-Bonus-AbstrakterDatentyp-Matrix.md)