# Bewertungsvorschläge

## Positive Faktoren:
	- Dozentron nimmt die Eingabe an
	- Code existiert und sieht funktionsfähig aus und die Laufzeit stimmt
	- Korrekte fehlerbehandlung (try with resources etc.. bei IO)
	
## Weiche Kriterien:
	- Die Eingabe in Dozentron war mit '\n' oder sonstigen Zeichen und wurde deswegen nicht erkannt
	- Die Algorithmus sieht funktionsfähig aus, ist aber beim Parsen etwas schief gelaufen
	- Es wird zuerst das Maximum und dann das Minimum ausgegeben / Top5 wird umgekehrt ausgegeben -> Logischer fehler
	
## Unit-Tests
	- With a big grain of salt. -> Überprüfe die Texteingabe und den Code.
	
## Ungültig:
	- Wenn die suche nach den größten und kleinsten Zahlen durch vorgeschriebene Bibliotheks-Methoden erfolgt. (Teilaufgabe)
	- Wenn man ein Sortierverfahren verwendet, um den top5 zu bestimmen, das aber auch nicht selbst geschrieben worden ist. (Teilaufgabe)
