# Bewertungsvorschläge

## Grün: 

- Bei mindestens 3 von 4 die Laufzeit korrekt erkannt
- Erkennbar, dass das Prinzip jedes Algorithmus verstanden wurde
- Bonus: Aufgabenteil 4 gut verstanden
	
## Rot:

- Deutliche Fehler in zwei Aufgaben
- (Oder: Kleine Fehler in drei Aufgaben)
- Oberflächlich + keine Antwort gegeben
- Übliche Kriteren für Textaufgaben
- Erkennbar, dass es *nicht* verstanden wurde