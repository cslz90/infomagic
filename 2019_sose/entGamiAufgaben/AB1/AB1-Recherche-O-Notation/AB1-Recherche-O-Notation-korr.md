## Korrekturschema:

### Kriterien fürs Nichtbestehen

* keine Quellenangabe
* keine vollständigen Sätze
* völlig unverständlicher Text
* über 1400 Worte Text
* über 10 Quellen
* mehr als 3 der 7 Fragen nicht beantwortet

### Kriterien für Grün:

* Alle Fragen beantwortet (keine Ausgelassen)
* Aufgabenstellung vollständig beachtet, incl Grenzen
* Verständliche Formlierungen etc
* Verlässliche Quellen vorteilhaft
* Falls Tabelle: Ordentlich erklärt


### Punkteverteilung (14 gesamt) (alt)

* **1 Punkt** pro korrekt beantworteter Frage (**7 gesamt**)
* **2 Punkte** für verständliche Formulierungen.
* **2 Punkte** für das Einhalten der Zeichengrenze
* **2 Punkt** für verlässliche Quellen
* **1 Punkt** für das Einhalten der Quellengrenze