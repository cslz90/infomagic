# Bewertungsvorschläge

## Grün:

- Beide Fragen + Aufgaben richtig
- Sinnvolle Antwort
- Erkennbar, dass das Thema verstanden wurde
- Sinnvoller Beweis, selbst wenn kleine Fehler vorhanden sind

## Rot:

- Übliche Textaufgaben-Kriterien missachtet
- Speziell dazu: Oberflächliche Antwort, ohne ein Verständnis zu zeigen
- Offensichtliches Unverständnis
- Eine der Zwei Aufgaben nicht bearbeitet