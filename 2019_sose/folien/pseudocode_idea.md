type List[E]
  operation add(this, value: E)
  operation remove(this, index: int)
  operation insert(this, index: int, value: E)
  operation size(this): int

structure ArrayList[E] < List[E]
  components { size: int, content: array[E] }
  literal 'A[' values: E* ']' do
    this := ArrayList[E].new
    for v in values do
      this.add(v)
    return this
  operation add(this, value: E)
    blabla
  operation remove(this, index: int)
    blablabla
  operation insert(this, index: int, value: E)
    blablabla
  operation size(this): int
    return this.size

A[1,2,4,5]

type NodeList[E] = List[Node[E]]

structure ANodeList[E] = ArrayList[Node[E]]
  literal 'N[' nodes: Node[E]* ']'
    return A[nodes]

algorithm linearSearch(haystack: ArrayList[E], needle: E)
  for i in 0..haystack.size()-1 do
    if (haystack.get(i) = needle) return v
  return -1


## Alt

structure DWGraph<E, W> { nodes: List<DWGNode<E, W>> }
structure DWGNode<E, W> { value: E, edges: List<DWEdge<W>> }
structure DWEdge<W> { 
  source: int, // index of source node
  target: int, // index of target node
  weight: W
}

## Neu



structure DWGraph[E,W]
  content { nodes: List[DWGNode] }

structure DisjointSets { elements: List<DJNode> }
structure DJNode { 
    parent: DJNode, setID: int, children: List<DJNode> }

algorithm makeSet(this: DisjointSets, element: int)
    // it must hold that element = elements.size
    newTree := new DJNode(None, elements.size, new AList())
    add(this.elements, newTree)
algorithm findRoot(n: DJNode)
  while n.parent != None do
    n := n.parent
  return n
algorithm findSet(this: DisjointSets, element: int)
  return findRoot(this.elements[element]).setID
algorithm joinSets(first: int, second: int)
  add(findRoot(first).children, findRoot(second))
