# Definition für den Pseudocode aus der Veranstaltung "Algorithmen und Datenstrukturen"
Christopher Schölzel

## Vorwort

Der hier definierte Pseudocode ist eigentlich streng genommen kein *Pseudo*-code mehr.
Dazu folgt er bereits zu festen Regeln und enthält zu wenige "menschenverständliche" Textformulierungen.
Er entspricht bis auf ein paar Details der Definition einer eigenen Sprache, die irgendwo zwischen Python, Java und Pascal angesiedelt ist.

Ich verwende aber absichtlich so eine stark strukturierte Definition, damit die Übersetzung meines Pseudocodes in eine konkrete objektorientierte Programmiersprache Ihnen nicht zu schwer fällt.

### Abweichungen

Die hier geschriebene Definition kann naturgemäß nicht alle Fälle abdecken, die in der Vorlesung nötig werden können.
Daher behalte ich mir vor mit einer entsprechenden Ankündigung von der hier verwendeten Syntax abzuweichen.

## Primitive Typen

Mein Pseudocode kennt die folgenden primitiven Typen:

* `int`: Ganzzahl
* `real`: Fließkommazahl (reale Zahl)
  * wie gewohnt ist das Komma ein Punkt, z.B. `3.5`
* `boolean`: Wahrheitswert (`true` oder `false`)
* `char`: Einzelnes Zeichen
  * Literale werden in einfache Anführungszeichen gesetzt, z.B. `'a'`
* `String`: Zeichenkette
  * Literale werden in doppelte Anführungszeichen gesetzt, z.B. `"Hallo Welt"`
* `array<T>`: array mit Elementen vom Typ `T`
  * Erzeugung: `array<T>[size]`, wobei `size` ein `int`-Ausdruck ist
  * Indexierung: `a[i]` greift auf Index `i` (beginnend bei 0) in Array `a` zu
  * Länge ermitteln: `a.length()` liefert die Länge des Arrays `a`
  * Literal: `[1,2]` erzeugt ein int-Array mit den Werten `1` und `2`

## Einfache Anweisungen

In meinem Pseudocode steht `:=` für eine Zuweisung und `=` für einen Vergleich.
Als sonstige Operatoren verwende ich die folgenden gängigen Symbole:

* `+`: Addition
* `-`: Subtraktion
* `*`: Multiplikation
* `/`: Division (ganzzahlig bei `int`)
* `%`: Modulo
* `=`: Gleich
* `!=`: Ungleich
* `>`: Größer
* `>=`: Größer oder gleich
* `<`: Kleiner
* `<=`: Kleiner oder gleich
* `and`: logisches "und"
* `or`: logisches "oder"
* `not`: logisches "nicht"
* `is_a`: prüft auf Typkompatibilität (wie `instanceof` in Java)
* `&`: bitweises "und"
* `|`: bitweises "oder"
* `^`: bitweises exklusives "oder"
* `~`: bitweise Negation
* `<<`: bitshift nach links
* `>>`: bitshift nach rechts

Variablen werden als `name: Typ` definiert, oder `name := wert`, wobei im letzteren Fall der Typ der Variable dem Typ des übergebenen Wertes entspricht.

Beispiele:

* `foo: int` definiert eine Int-Variable namens `foo`.
* `bar := "Test"` definiert eine String-Variable namens `bar` mit dem Wert `"Test"`.

## Kontrollfluss

### if-Anweisung

Eine if-Anweisung hat die folgende Form:

```cs-pseudo
if Bedingung then
  Anweisungen für If-Fall
else
  Anweisungen für Else-Fall
```

Dabei ist die Einrückung (wie auch bei allen folgenden Sprachkonstrukten) Teil der Syntax.
Es gibt keine geschweiften Klammern.
Stattdessen umfasst ein Codeblock alle Codezeilen mit der gleichen Einrückung.

Für mehrarmige if-Anweisungen gibt es die Möglichkeit statt `else` wieder `else if Zweite Bedingung then` zu schreiben.

### for-Schleife

Eine for-Schleife hat die folgende Form:

```cs-pseudo
for i := v1, v2, ..., vn do
  Schleifenkörper
```

Dabei ist `v1, v2, ..., vn` eine Aufzählung der Werte, die die Schleifenvariable `i` annehmen soll.
Zum Beispiel wird eine Schleife mit der Aufzählung `0, 1, ..., 5` für die Werte `0`, `1`, `2`, `3`, `4` und `5` ausgeführt.
Eine Schleife mit `0, 2, 4, ..., 8` jedoch für die Werte `0`, `2`, `4`, `6` und `8`.

Bei der Aufzählung sollten natürlich so viele Werte genannt werden, wie nötig sind, um das Zählmuster zu erkennen.
Man könnte auch eine Schleife definieren, die über alle Primzahlen bis 127 iteriert mit `2, 3, 5, 7, 11, 13, ..., 127`.

Wie in Java gibt es die Befehle `continue` und `break` zum Sprung zum nächsten Durchlauf und dem vorzeitigen Beenden der Schleife.

### while-Schleife

Eine while-Schleife hat die folgende Form:

```cs-pseudo
while Bedingung do
  Schleifenkörper
```

### Foreach-Schleife

Oft möchte man über Elemente einer Datenstruktur iterieren.
Dazu gibt es in diesem Pseudocode wie auch in Java die Foreach-Schleife:

```cs-pseudo
for elementVar in structureVar do
  Schleifenkörper
```

Dabei wird der Wert der Variable `elementVar` in jedem Durchlauf auf den nächsten Wert aus der Struktur gesetzt, die in `structureVar` gespeichert ist.
Wie genau das "Iterieren" dabei funktioniert (welche Operationen verwendet werden) wird nicht angegeben, um den Pseudocode für neue "iterierbare" Datenstrukturen nicht unnötig mit zusätzlichen Operationen zu überfüllen.
Für `structureVar` sollten also nur Typen verwendet werden, bei denen das offensichtlich ist (`List<E>`, `array<E>`, `Iterator<E>`, ...).

## Algorithmen

Ein Algorithmus wird in diesem Pseudocode wie folgt definiert:

```cs-pseudo
algorithm name(p1: T1, p2: T2, ..., pn: TN): Rückgabetyp
  Anweisungen
```

Dabei sind `p1`, `p2` usw. bis `pn` die Namen der Parameter und `T1`, `T2`, ... `TN` deren Typen.
Die Angabe des Rückgabetyps kann wegfallen, wenn aus der Verwendung des `return`-Befehls (siehe unten) klar hervorgeht, was der Typ des zurückgegebenen Wertes ist.
Als besonderen Rückgabetyp gibt es `void`, wenn der Algorithmus nur seine Eingaben verändert und keinen neuen Wert zurückgibt.

Innerhalb der Anweisungen kann ein `return Wert` stehen.
Dieses `return` funktioniert wie in Java: Die Ausführung des Algorithmus wird beendet und der dahinter angegebene Wert wird als Ergebnis zurückgegeben.
Bei einem `void`-Algorithmus kann das `return` ganz entfallen oder ohne dahinterstehenden Wert verwendet werden, um den Algorithmus frühzeitig abzubrechen.

### Mehrere Rückgabewerte

In manchen Fällen ist es angenehmer, wenn ein Algorithmus zwei oder mehr Rückgabewerte hat.
Im Pseudocode können daher mehrere Rückgabetypen mit Komma getrennt vorkommen und auch hinter dem `return` können mehrere mit Komma getrennte Werte stehen.

Beispiel:

```cs-pseudo
algorithm divisionWithRest(a: int, b: int): int, int
  d := a / b
  r := a % b
  return a, b
```

Beim Aufruf eines solchen Algorithmus muss man das Ergebnis natürlich auch in mehreren Variablen speichern.
Auch hier können die Variablen mit Komma getrennt werden:

```cs-pseudo
x, y := divisionWithRest(10,3)
```

In Java müsste man eine eigene Tupel-Klasse implementieren, die die zwei Rückgabewerte als Variablen enthält.

## Operationen

Die oben genannten Algorithmen entsprechen in etwa den statischen Methoden in Java.
Nicht-statische Methoden nennen wir hier dagegen *Operationen*.
Die Syntax dafür weicht nur geringfügig von der eines Algorithmus ab:

```cs-pseudo
operation name(this: T0, p1: T1, p2: T2, ..., pn: TN): Rückgabetyp
  Anweisungen
```

Dabei ist der Name des ersten Parameters `this` immer fest und `T0` bestimmt den Datentyp oder die Datenstruktur für die die Operation definiert wird.

Operationen können in einem eingerückten Block in der Definition eines Datentyps oder einer Datenstruktur vorkommen.
In diesem Fall muss `T0` nicht angegeben werden, weil offensichtlich ist, welchen Typ `this` haben muss.
Sie können aber auch alleine stehen, dann muss `T0` angegeben werden.

## Datentypen

Datentypen entsprechen Interfaces in Java.
Sie definieren lediglich einen Namen für einen Typ und die Operationen, die mit ihm ausgeführt werden können.

Das Schlüsselwort zur Definition ist `type` und die Syntax entspricht dem folgenden Schema:

```cs-pseudo
type Name
  Operationsdefinitionen ohne Körper
```

Generische Typen funktionieren ähnlich wie in Java mit einem Typparameter:

```cs-pseudo
type Name<T>
  Operationsdefinitionen ohne Körper
```

Ein Beispiel einer Definition eines Listentyps könnte also so aussehen:

```cs-pseudo
type List<E>
  operation add(this, value: E)
  operation remove(this, index: int)
  operation insert(this, index: int, value: E)
  operation size(this): int
```

Um kürzere Namen für zusammengesetzte Typen zu definieren gibt es eine Kurzschreibweise, die einfach nur einen Alias für einen bestehenden Typ einführt:

```cs-pseudo
type NeuerName = AlterName
```

Das kann dann zum Beispiel wie folgt aussehen:

```cs-pseudo
type NodeList<E> = List<Node<E>>
```

Damit kann man jetzt überall wo `List<Node<E>>` als Typ benötigt wird stattdessen den Typ `NodeList<E>` verwenden.

### Typhierarchie

Natürlich kann es zu Typen auch Subtypen geben.
Die Definition eines Subtyps erfolgt mit dem Zusatz `< Supertyp` hinter dem Namen.

Beispiel:

```cs-pseudo
type FancyList<E> < List<E>
  operation map(this, f: Function<E,X>): FancyList<X>
```

Hier hätte der Typ `FancyList<E>` alle Operationen von `List<E>` und zusätzlich noch eine eigene Operation namens `map`.

Dabei gibt es keine Einschränkung, welcher Typ hinter `<` stehen kann.
Es ist also z.B. möglich mit `Sortable<E> < E` einen Typ zu definieren, bei dem sich die Superklasse anhand eines generischen Typparameters ändert.

Sollte es einmal nötig sein, auf eine Operation eines Supertyps zuzugreifen innerhalb einer gleichnamigen Operation des Subtyps, kann das Schlüsselwort `super` nach dem Schema `this.super.operatorName(Argumente)` verwendet werden.

## Datenstrukturen

Wenn eine `operation` in Java ein `interface` ist, dann ist eine Datenstruktur, die mit dem Schlüsselwort `structure` definiert wird, das Gegenstück zu `class`.

Die Syntax lautet wie folgt:

```cs-pseudo
structure Name
  components
    Variablendefinitionen
  Operationsdefinitionen mit Körper
```

Dabei stehen unter `components` eine Reihe von Variablen, die die Elemente darstellen aus denen die Datenstruktur zusammengesetzt ist.
Bei einer ArrayList könnte das wie folgt aussehen:

```cs-pseudo
structure ArrayList<E> < List<E>
  components
    size: int
    content: array<E>
  operation size(this): int
    return this.size
  ... (weitere Operationsdefinitionen)
```

Für `components` gibt es auch eine Kurzschreibweise.
Hier werden die Komponenten einfach in runden Klammern mit Kommata getrennt angegeben.

Beispiel:

* Langschreibweise:
    ```cs-pseudo
    components
      size: int
      content: array<E>
    ```
* Kurzschreibweise:
    ```cs-pseudo
    components (size: int, content: array<E>)
    ```

### Konstruktor

Eine besondere Operation, die nur bei einer `structure` und nicht bei einem `type` zu finden ist, ist der Konstruktor.

Die Syntax ist hier die gleiche, wie die Methode auch. Allerdings muss der Konstruktor immer `new` heißen und auch wenn kein Rückgabetyp angegeben wird, erzeugt er ein neues Objekt von dem Typ von `this`.
Die Anweisungen innerhalb des Konstruktors beinhalten nur den Code, der nötig ist, um die Variablen der Datenstruktur zu initialisieren - genau so wie bei Java-Konstruktoren.

Beispiel:

```cs-pseudo
operation new(this: ArrayList<E>, capacity: int)
  this.size := 0
  this.content := array<E>[capacity]
```

### Aufruf von Operationen

Operationen kann man zwar wie Algorithmen direkt aufrufen als `name(arg1, arg2, ...)`, wobei auch der Parameter `this` explizit als `arg1` angegeben wird.
Typischerweise findet man aber eher den Aufruf `arg1.name(arg2, arg3, ...)`, wobei dann `arg1` als `this` verwendet wird.

Einen Sonderfall bilden auch hier Konstruktoren.
Bei Ihnen erfolgt der Aufruf nach dem Schema `Datenstrukturname.new(arg1, arg2, ...)`.
Hier wird auch das `this` nicht explizit angegeben, da ja gerade ein neues Objekt erzeugt werden soll.

Beispiel:

```cs-pseudo
lst := ArrayList<Integer>.new(10)
lst.add(4)
x := lst.get(0)
```

## Operatorüberladung

Wenn man z.B. eine totale Ordnung allgemein definieren will mit dem folgenden Typ

```
type Sortable<E> < E
  operation leq(other: Sortable<E>): boolean
```

ist es natürlich einfacher, wenn man `a <= b` schreiben kann, statt immer wieder im Code den Ausdruck `a.leq(b)` zu wiederholen.

Deshalb habe ich in diesem Pseudocode ein Konstruktor zur Definition neuer und Überladung bestehender Operatoren eingefügt:

```
operator SyntaxMitPlatzhaltern with (p1: T1, p2: T2, ...): Rückgabetyp
  Anweisungen
```

Dabei kann `SyntaxMitPlatzhaltern` irgendeine Zeichenkette sein, die die Namen der Platzhalter `p1`, `p2` usw. enthält.
Wann immer im darauffolgenden Code eine Zeichenkette von der gegebenen Form auftaucht, bei der die Platzhalter die in der Definition angegebenen Typen haben, wird der Anweisungsblock ausgeführt.
Das Verhalten ist also ganz ähnlich wie bei der Definition einer Operation, nur dass die Syntax des Aufrufs frei wählbar ist.

Für `leq` würde das zum Beispiel so aussehen:

```
operator a <= b with (a: Sortable<E>, b: Sortable<E>): boolean
  return a.leq(b)
```

Damit wird jetzt immer, wenn im Code `x <= y` für irgendein `x` und `y` vom Typ `Sortable<E>` auftaucht, `x.leq(y)` aufgerufen.

Es sind auch andere Syntaxdefinitionen denkbar, wie der Zugriff über eckige Klammern auf eine Liste:

```
operator lst[idx] with (lst: List<E>, idx: int): E
  return lst.get(idx)
```

## Assert: Definitionsbereiche eingrenzen

Um einzugrenzen, für welche Argumente eine Operation oder ein Algorithmus definiert ist, gibt es in diesem Pseudocode das `assert`-Konstrukt.
Es stellt sicher, dass eine Bedingung gilt und bricht die Operation oder den Algorithmus ansonsten mit einer Fehlermeldung ab.
Die Syntax lautet wie folgt:

```cs-pseudo
assert Bedingung : Nachricht
```

Dabei ist `Bedingung` irgendein Ausdruck vom Typ `boolean` und `Nachricht` ein String.
Es gibt auch eine verkürzte Form, bei der die Nachricht nicht angegeben wird:

```cs-pseudo
assert Bedingung
```

## Beispiele für die Übersetzungen von Pseudocode nach Java

### Tupel

Pseudocode:

```cs-pseudo
type Tuple<A,B>
  operation swap(this): Tuple<B,A>
```

Entsprechung in Java:

```java
interface Tuple<A,B> {
  Tuple<B,A> swap();
}
```

Pseudocode:

```cs-pseudo
structure SimpleTuple<A,B>
  components
    a: A
    b: B
  operation new(this, a: A, b: B)
    this.a := a
    this.b := b
  operation swap(this)
    return SimpleTuple<B,A>.new(this.b, this.a)
```

Entsprechung in Java:

```java
public class SimpleTuple<A,B> implements Tuple<A,B> {
  public A a;
  public B b;
  public SimpleTuple(A a, B b) {
    this.a = a;
    this.b = b;
  }
  @Override
  public Tuple<B,A> swap() {
    return new Tuple<B,A>(b, a);
  }
}
```

*Hinweis:*

Natürlich sollte man Instanzvariablen meistens eher `private` setzen.
Für diese Veranstaltung interessieren uns aber die Innereien von Datenstrukturen und daher gibt es im Pseudocode keine Möglichkeit, den Zugriff zu verbieten.
Beim Übersetzen nach Java müssen Sie also eventuell nach eigenem Ermessen Getter und Setter hinzufügen.
