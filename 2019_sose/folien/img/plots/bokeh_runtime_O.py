import bokeh.plotting as bp
import bokeh.models as bm
import bokeh.models.widgets as bmw
import bokeh.embed as be
import bokeh.resources as br
import numpy as np


def write_autoload(fig, outname):
    js, tag = be.autoload_static(fig, br.CDN, outname+".js")
    with open(outname+".js", "w", encoding="utf-8") as f:
        f.write(js)
    with open(outname+".htm", "w", encoding="utf-8") as f:
        f.write("<!DOCTYPE html><html><head><meta charset=\"utf-8\">")
        f.write("</head><body>\n")
        f.write(tag)
        f.write("\n</body></html>")


xvals = np.arange(15)

runtimes = bm.ColumnDataSource(data=dict(
  t=["18n"] * len(xvals) + ["2n²"] * len(xvals),
  n=np.hstack((xvals, xvals)),
  y=np.hstack((18 * xvals, 2 * xvals * xvals)),
))
# print(runtimes.to_df())
runtimes_n = bm.CDSView(filters=[bm.GroupFilter("t", "18n")], source=runtimes)
runtimes_n2 = bm.CDSView(filters=[bm.GroupFilter("t", "2n²")], source=runtimes)
f = bp.figure(
  title="Quadratische Funktion überholt lineare Funktion",
  x_axis_label="n", y_axis_label="Laufzeit [Pz]",
  x_range=(0, 14), y_range=(0, 400),
  tooltips=[
    ("f(n)", "@t"),
    ("n", "@n{0}"),
    ("Pz", "@y{0}")
  ],
  tools="",
  width=400,
  height=300
)

f.line("n", "y", legend="18n", line_width=2, source=runtimes, view=runtimes_n)
f.line("n", "y", legend="2n²", color="red", line_width=2, source=runtimes, view=runtimes_n2)
n0 = bm.Span(
  location=9, dimension="height", line_color="black", line_dash="dashed",
  line_width=2
)
n0l = bm.Label(
  x=9, y=0, x_offset=10, y_offset=5, text="n₀",
  text_align="left", render_mode="css"
)
f.add_layout(n0)
f.add_layout(n0l)
f.legend.location = "top_left"

bp.show(f)
write_autoload(f, "runtime_O")
