import numpy as np
import matplotlib.pyplot as plt

def plot_functions(funcs, fname=None):
  n = np.arange(2, 1000, dtype=np.float64)
  for i in range(4):
    plt.subplot(2, 2, i+1)
    for f, l in funcs:
      plt.plot(n, f(n), label=l)
    plt.legend(loc="best")
    ymax = 4000
    ymin = 0
    xlog = i % 2 == 1
    ylog = i // 2 == 1
    if ylog:
      ymax *= 1e20
      ymin = 1
    if xlog:
      ymax *= 0.01
    plt.xlabel("n")
    plt.ylabel("Prozessorzyklen")
    if xlog:
      plt.xscale("log")
    if ylog:
      plt.yscale("log")
    plt.title("x %s / y %s" % ("log" if xlog else "lin", "log" if ylog else "lin"))
    plt.ylim(ymin, ymax)
    plt.xlim(2, n[-1])
  plt.tight_layout()
  if fname is not None:
    plt.savefig(fname)
  else:
    plt.show(fname)

if __name__ == "__main__":
  plot_functions(
    [
      (lambda x: 2 * np.log(x), "$2 \\log x$"),
      (lambda x: 3 * x, "$3 x$"),
      (lambda x: x ** 4, "$x^4$"),
      (lambda x: 1.1 ** x, "$5^x$")
    ],
    "logarithm_scales.pdf"
  )