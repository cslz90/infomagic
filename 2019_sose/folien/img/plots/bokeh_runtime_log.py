import bokeh.plotting as bp
import bokeh.models as bm
import bokeh.models.widgets as bmw
import bokeh.embed as be
import bokeh.resources as br
import numpy as np


def write_autoload(fig, outname):
    js, tag = be.autoload_static(fig, br.CDN, outname+".js")
    with open(outname+".js", "w", encoding="utf-8") as f:
        f.write(js)
    with open(outname+".htm", "w", encoding="utf-8") as f:
        f.write("<!DOCTYPE html><html><head><meta charset=\"utf-8\">")
        f.write("</head><body>\n")
        f.write(tag)
        f.write("\n</body></html>")


xvals = 1.5 ** np.arange(15)

runtimes = bm.ColumnDataSource(data=dict(
  t=["30n² + 194n"] * len(xvals) + ["n²"] * len(xvals) + ["2n³"] * len(xvals),
  n=np.hstack((xvals, xvals, xvals)),
  y=np.hstack((30 * xvals ** 2 + 194 * xvals, xvals ** 2, 2 * xvals ** 3)),
))
print(runtimes.to_df())
cvals = bm.ColumnDataSource(data=dict(
  t=["cn"] * len(xvals),
  n=xvals,
  y=100 * xvals ** 2,
  y0=np.ones(len(xvals))
))
f = bp.figure(
  title="Die Konstante c als Sicherheitsbereich",
  x_axis_label="n", y_axis_label="Laufzeit [Pz]",
  x_axis_type="log", y_axis_type="log",
  x_range=(xvals[0], xvals[-1]),
  y_range=(1, 10**8),
  tooltips=[
    ("f(n)", "@t"),
    ("n", "@n{0}"),
    ("Pz", "@y{0}")
  ],
  tools="",
  width=600,
  height=450
)
colors = ["darkorange", "firebrick", "deepskyblue"]

n0 = bm.Span(
  location=97/35, dimension="height", line_color="black", line_dash="dashed",
  line_width=2
)
f.add_layout(n0)
for t in sorted(set(runtimes.data["t"])):
    v = bm.CDSView(filters=[bm.GroupFilter("t", t)], source=runtimes)
    f.line("n", "y", legend=t, line_width=2, source=runtimes, view=v, line_color=colors.pop(0))
c = bm.Band(
  base="n", lower="y0", upper="y", dimension="height", source=cvals,
  level="underlay", line_width=1, line_color="black",
  fill_color="limegreen", fill_alpha=0.5, line_alpha=0.5
)
cline = f.line([100, 100], [10000, 1000000], line_dash="dashed", line_color="limegreen", line_width=2)
f.line([10, 10], [100, 10000], line_dash="dashed", line_color="limegreen", line_width=2)
# add empty patch to obtain a renderer that can be used for the legend
c_renderer = f.patch([], [], fill_color="limegreen", fill_alpha=0.5, line_alpha=0.5)
# add empty line to obtain renderer for span
n0_renderer = f.line([], [], line_color="black", line_dash="dashed", line_width=2)
f.legend[0].items.append(bm.LegendItem(label="≤ c·n²", renderers=[c_renderer]))
f.legend[0].items.append(bm.LegendItem(label="log c", renderers=[cline]))
f.legend[0].items.append(bm.LegendItem(label="n₀", renderers=[n0_renderer]))
f.add_layout(c)
f.legend.location = "top_left"

bp.show(f)
write_autoload(f, "runtime_log")
