(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("b9b39f16-9845-44c0-a8f1-86f7f185fcd9");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'b9b39f16-9845-44c0-a8f1-86f7f185fcd9' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-1.0.2.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-1.0.2.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-1.0.2.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-1.0.2.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"39bfe999-4f88-4597-8209-b645fd4f66dc":{"roots":{"references":[{"attributes":{"plot":null,"text":"Quadratische Funktion \\u00fcberholt lineare Funktion"},"id":"1006","type":"Title"},{"attributes":{"items":[{"id":"1040","type":"LegendItem"},{"id":"1052","type":"LegendItem"}],"location":"top_left","plot":{"id":"1007","subtype":"Figure","type":"Plot"}},"id":"1039","type":"Legend"},{"attributes":{"callback":null,"end":14},"id":"1009","type":"Range1d"},{"attributes":{"line_color":"red","line_width":2,"x":{"field":"n"},"y":{"field":"y"}},"id":"1042","type":"Line"},{"attributes":{"label":{"value":"18n"},"renderers":[{"id":"1033","type":"GlyphRenderer"}]},"id":"1040","type":"LegendItem"},{"attributes":{"filters":[{"id":"1002","type":"GroupFilter"}],"source":{"id":"1001","type":"ColumnDataSource"}},"id":"1003","type":"CDSView"},{"attributes":{},"id":"1013","type":"LinearScale"},{"attributes":{},"id":"1050","type":"Selection"},{"attributes":{},"id":"1015","type":"LinearScale"},{"attributes":{"data_source":{"id":"1001","type":"ColumnDataSource"},"glyph":{"id":"1042","type":"Line"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"1043","type":"Line"},"selection_glyph":null,"view":{"id":"1005","type":"CDSView"}},"id":"1044","type":"GlyphRenderer"},{"attributes":{"axis_label":"n","formatter":{"id":"1038","type":"BasicTickFormatter"},"plot":{"id":"1007","subtype":"Figure","type":"Plot"},"ticker":{"id":"1018","type":"BasicTicker"}},"id":"1017","type":"LinearAxis"},{"attributes":{},"id":"1051","type":"UnionRenderers"},{"attributes":{},"id":"1018","type":"BasicTicker"},{"attributes":{"label":{"value":"2n\\u00b2"},"renderers":[{"id":"1044","type":"GlyphRenderer"}]},"id":"1052","type":"LegendItem"},{"attributes":{"dimension":"height","line_dash":[6],"line_width":{"value":2},"location":9,"plot":{"id":"1007","subtype":"Figure","type":"Plot"}},"id":"1053","type":"Span"},{"attributes":{"callback":null,"end":400},"id":"1011","type":"Range1d"},{"attributes":{"plot":{"id":"1007","subtype":"Figure","type":"Plot"},"ticker":{"id":"1018","type":"BasicTicker"}},"id":"1021","type":"Grid"},{"attributes":{"axis_label":"Laufzeit [Pz]","formatter":{"id":"1036","type":"BasicTickFormatter"},"plot":{"id":"1007","subtype":"Figure","type":"Plot"},"ticker":{"id":"1023","type":"BasicTicker"}},"id":"1022","type":"LinearAxis"},{"attributes":{},"id":"1023","type":"BasicTicker"},{"attributes":{"dimension":1,"plot":{"id":"1007","subtype":"Figure","type":"Plot"},"ticker":{"id":"1023","type":"BasicTicker"}},"id":"1026","type":"Grid"},{"attributes":{"callback":null,"data":{"n":{"__ndarray__":"AAAAAAEAAAACAAAAAwAAAAQAAAAFAAAABgAAAAcAAAAIAAAACQAAAAoAAAALAAAADAAAAA0AAAAOAAAAAAAAAAEAAAACAAAAAwAAAAQAAAAFAAAABgAAAAcAAAAIAAAACQAAAAoAAAALAAAADAAAAA0AAAAOAAAA","dtype":"int32","shape":[30]},"t":["18n","18n","18n","18n","18n","18n","18n","18n","18n","18n","18n","18n","18n","18n","18n","2n\\u00b2","2n\\u00b2","2n\\u00b2","2n\\u00b2","2n\\u00b2","2n\\u00b2","2n\\u00b2","2n\\u00b2","2n\\u00b2","2n\\u00b2","2n\\u00b2","2n\\u00b2","2n\\u00b2","2n\\u00b2","2n\\u00b2"],"y":{"__ndarray__":"AAAAABIAAAAkAAAANgAAAEgAAABaAAAAbAAAAH4AAACQAAAAogAAALQAAADGAAAA2AAAAOoAAAD8AAAAAAAAAAIAAAAIAAAAEgAAACAAAAAyAAAASAAAAGIAAACAAAAAogAAAMgAAADyAAAAIAEAAFIBAACIAQAA","dtype":"int32","shape":[30]}},"selected":{"id":"1050","type":"Selection"},"selection_policy":{"id":"1051","type":"UnionRenderers"}},"id":"1001","type":"ColumnDataSource"},{"attributes":{"column_name":"t","group":"18n"},"id":"1002","type":"GroupFilter"},{"attributes":{"plot":{"id":"1007","subtype":"Figure","type":"Plot"},"render_mode":"css","text":"n\\u2080","x":9,"x_offset":10,"y":0,"y_offset":5},"id":"1054","type":"Label"},{"attributes":{"line_color":"#1f77b4","line_width":2,"x":{"field":"n"},"y":{"field":"y"}},"id":"1031","type":"Line"},{"attributes":{"callback":null,"tooltips":[["f(n)","@t"],["n","@n{0}"],["Pz","@y{0}"]]},"id":"1027","type":"HoverTool"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto","tools":[{"id":"1027","type":"HoverTool"}]},"id":"1028","type":"Toolbar"},{"attributes":{"column_name":"t","group":"2n\\u00b2"},"id":"1004","type":"GroupFilter"},{"attributes":{"line_alpha":0.1,"line_color":"#1f77b4","line_width":2,"x":{"field":"n"},"y":{"field":"y"}},"id":"1032","type":"Line"},{"attributes":{"data_source":{"id":"1001","type":"ColumnDataSource"},"glyph":{"id":"1031","type":"Line"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"1032","type":"Line"},"selection_glyph":null,"view":{"id":"1003","type":"CDSView"}},"id":"1033","type":"GlyphRenderer"},{"attributes":{"line_alpha":0.1,"line_color":"#1f77b4","line_width":2,"x":{"field":"n"},"y":{"field":"y"}},"id":"1043","type":"Line"},{"attributes":{"filters":[{"id":"1004","type":"GroupFilter"}],"source":{"id":"1001","type":"ColumnDataSource"}},"id":"1005","type":"CDSView"},{"attributes":{},"id":"1036","type":"BasicTickFormatter"},{"attributes":{},"id":"1038","type":"BasicTickFormatter"},{"attributes":{"below":[{"id":"1017","type":"LinearAxis"}],"left":[{"id":"1022","type":"LinearAxis"}],"plot_height":300,"plot_width":400,"renderers":[{"id":"1017","type":"LinearAxis"},{"id":"1021","type":"Grid"},{"id":"1022","type":"LinearAxis"},{"id":"1026","type":"Grid"},{"id":"1039","type":"Legend"},{"id":"1033","type":"GlyphRenderer"},{"id":"1044","type":"GlyphRenderer"},{"id":"1053","type":"Span"},{"id":"1054","type":"Label"}],"title":{"id":"1006","type":"Title"},"toolbar":{"id":"1028","type":"Toolbar"},"x_range":{"id":"1009","type":"Range1d"},"x_scale":{"id":"1013","type":"LinearScale"},"y_range":{"id":"1011","type":"Range1d"},"y_scale":{"id":"1015","type":"LinearScale"}},"id":"1007","subtype":"Figure","type":"Plot"}],"root_ids":["1007"]},"title":"Bokeh Application","version":"1.0.2"}}';
                  var render_items = [{"docid":"39bfe999-4f88-4597-8209-b645fd4f66dc","roots":{"1007":"b9b39f16-9845-44c0-a8f1-86f7f185fcd9"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing");
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-1.0.2.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-1.0.2.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-1.0.2.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-1.0.2.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-1.0.2.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-1.0.2.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();