(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("5ace92f9-b04c-419f-936f-3219cea9edd1");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '5ace92f9-b04c-419f-936f-3219cea9edd1' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-1.0.2.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-1.0.2.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-1.0.2.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-1.0.2.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"1234c930-951b-42f2-96ba-7f2d3e19b33c":{"roots":{"references":[{"attributes":{"label":{"value":"n\\u00b2"},"renderers":[{"id":"1060","type":"GlyphRenderer"}]},"id":"1068","type":"LegendItem"},{"attributes":{"callback":null,"data":{"x":[100,100],"y":[10000,1000000]},"selected":{"id":"1101","type":"Selection"},"selection_policy":{"id":"1102","type":"UnionRenderers"}},"id":"1071","type":"ColumnDataSource"},{"attributes":{},"id":"1103","type":"Selection"},{"attributes":{},"id":"1010","type":"LogScale"},{"attributes":{"line_color":"darkorange","line_width":2,"x":{"field":"n"},"y":{"field":"y"}},"id":"1031","type":"Line"},{"attributes":{"line_color":"limegreen","line_dash":[6],"line_width":2,"x":{"field":"x"},"y":{"field":"y"}},"id":"1072","type":"Line"},{"attributes":{"line_alpha":0.1,"line_color":"#1f77b4","line_width":2,"x":{"field":"n"},"y":{"field":"y"}},"id":"1032","type":"Line"},{"attributes":{"line_alpha":0.1,"line_color":"#1f77b4","line_dash":[6],"line_width":2,"x":{"field":"x"},"y":{"field":"y"}},"id":"1073","type":"Line"},{"attributes":{"data_source":{"id":"1001","type":"ColumnDataSource"},"glyph":{"id":"1031","type":"Line"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"1032","type":"Line"},"selection_glyph":null,"view":{"id":"1029","type":"CDSView"}},"id":"1033","type":"GlyphRenderer"},{"attributes":{"data_source":{"id":"1071","type":"ColumnDataSource"},"glyph":{"id":"1072","type":"Line"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"1073","type":"Line"},"selection_glyph":null,"view":{"id":"1075","type":"CDSView"}},"id":"1074","type":"GlyphRenderer"},{"attributes":{"ticker":null},"id":"1035","type":"LogTickFormatter"},{"attributes":{"source":{"id":"1071","type":"ColumnDataSource"}},"id":"1075","type":"CDSView"},{"attributes":{"plot":null,"text":"Die Konstante c als Sicherheitsbereich"},"id":"1003","type":"Title"},{"attributes":{"ticker":null},"id":"1037","type":"LogTickFormatter"},{"attributes":{"callback":null,"data":{"x":[10,10],"y":[100,10000]},"selected":{"id":"1103","type":"Selection"},"selection_policy":{"id":"1104","type":"UnionRenderers"}},"id":"1076","type":"ColumnDataSource"},{"attributes":{"line_color":"limegreen","line_dash":[6],"line_width":2,"x":{"field":"x"},"y":{"field":"y"}},"id":"1077","type":"Line"},{"attributes":{"filters":[{"id":"1041","type":"GroupFilter"}],"source":{"id":"1001","type":"ColumnDataSource"}},"id":"1042","type":"CDSView"},{"attributes":{"line_alpha":0.1,"line_color":"#1f77b4","line_dash":[6],"line_width":2,"x":{"field":"x"},"y":{"field":"y"}},"id":"1078","type":"Line"},{"attributes":{},"id":"1102","type":"UnionRenderers"},{"attributes":{"data_source":{"id":"1076","type":"ColumnDataSource"},"glyph":{"id":"1077","type":"Line"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"1078","type":"Line"},"selection_glyph":null,"view":{"id":"1080","type":"CDSView"}},"id":"1079","type":"GlyphRenderer"},{"attributes":{"line_color":"firebrick","line_width":2,"x":{"field":"n"},"y":{"field":"y"}},"id":"1044","type":"Line"},{"attributes":{"callback":null,"data":{"n":{"__ndarray__":"AAAAAAAA8D8AAAAAAAD4PwAAAAAAAAJAAAAAAAAAC0AAAAAAAEAUQAAAAAAAYB5AAAAAAADIJkAAAAAAABYxQAAAAAAAoTlAAAAAAMA4Q0AAAAAAINVMQAAAAADYn1VAAAAAAOI3YEAAAAAA01NoQAAAAEDePnJAAAAAAAAA8D8AAAAAAAD4PwAAAAAAAAJAAAAAAAAAC0AAAAAAAEAUQAAAAAAAYB5AAAAAAADIJkAAAAAAABYxQAAAAAAAoTlAAAAAAMA4Q0AAAAAAINVMQAAAAADYn1VAAAAAAOI3YEAAAAAA01NoQAAAAEDePnJAAAAAAAAA8D8AAAAAAAD4PwAAAAAAAAJAAAAAAAAAC0AAAAAAAEAUQAAAAAAAYB5AAAAAAADIJkAAAAAAABYxQAAAAAAAoTlAAAAAAMA4Q0AAAAAAINVMQAAAAADYn1VAAAAAAOI3YEAAAAAA01NoQAAAAEDePnJA","dtype":"float64","shape":[45]},"t":["30n\\u00b2 + 194n","30n\\u00b2 + 194n","30n\\u00b2 + 194n","30n\\u00b2 + 194n","30n\\u00b2 + 194n","30n\\u00b2 + 194n","30n\\u00b2 + 194n","30n\\u00b2 + 194n","30n\\u00b2 + 194n","30n\\u00b2 + 194n","30n\\u00b2 + 194n","30n\\u00b2 + 194n","30n\\u00b2 + 194n","30n\\u00b2 + 194n","30n\\u00b2 + 194n","n\\u00b2","n\\u00b2","n\\u00b2","n\\u00b2","n\\u00b2","n\\u00b2","n\\u00b2","n\\u00b2","n\\u00b2","n\\u00b2","n\\u00b2","n\\u00b2","n\\u00b2","n\\u00b2","n\\u00b2","2n\\u00b3","2n\\u00b3","2n\\u00b3","2n\\u00b3","2n\\u00b3","2n\\u00b3","2n\\u00b3","2n\\u00b3","2n\\u00b3","2n\\u00b3","2n\\u00b3","2n\\u00b3","2n\\u00b3","2n\\u00b3","2n\\u00b3"],"y":{"__ndarray__":"AAAAAAAAbEAAAAAAAGh2QAAAAAAAY4JAAAAAAMAjj0AAAAAA+FubQAAAAABHBqlAAAAA4CvWt0AAAABcRpTHQAAAgOdOGdhAAABwlFhK6UAAAP6CCxb7QADAXdiacg1BALyUTyQuIEGAU+/BKuoxQfA9e4gh8ENBAAAAAAAA8D8AAAAAAAACQAAAAAAAQBRAAAAAAADIJkAAAAAAAKE5QAAAAAAg1UxAAAAAAOI3YEAAAABA3j5yQAAAAAi6hoRAAAAASZEXl0AAACByg/qpQAAAZODTOb1AAEA4Lodw0EAASP8TmH7iQAAxfxZrzvRAAAAAAAAAAEAAAAAAAAAbQAAAAAAAyDZAAAAAAMA4U0AAAAAA4jdwQAAAAGBNXotAAAAASZEXp0AAAJiV4nvDQABAOC6HcOBAAOz+HeS9+0AgF095OGgXQYO7WqbvvzNBN45cNPKpUEH8L1y4yB5sQX3IjVv5uYdB","dtype":"float64","shape":[45]}},"selected":{"id":"1052","type":"Selection"},"selection_policy":{"id":"1053","type":"UnionRenderers"}},"id":"1001","type":"ColumnDataSource"},{"attributes":{"source":{"id":"1076","type":"ColumnDataSource"}},"id":"1080","type":"CDSView"},{"attributes":{"line_alpha":0.1,"line_color":"#1f77b4","line_width":2,"x":{"field":"n"},"y":{"field":"y"}},"id":"1045","type":"Line"},{"attributes":{"callback":null,"data":{"x":[],"y":[]},"selected":{"id":"1105","type":"Selection"},"selection_policy":{"id":"1106","type":"UnionRenderers"}},"id":"1081","type":"ColumnDataSource"},{"attributes":{"data_source":{"id":"1001","type":"ColumnDataSource"},"glyph":{"id":"1044","type":"Line"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"1045","type":"Line"},"selection_glyph":null,"view":{"id":"1042","type":"CDSView"}},"id":"1046","type":"GlyphRenderer"},{"attributes":{"fill_alpha":0.5,"fill_color":"limegreen","line_alpha":0.5,"line_color":"#1f77b4","x":{"field":"x"},"y":{"field":"y"}},"id":"1082","type":"Patch"},{"attributes":{},"id":"1052","type":"Selection"},{"attributes":{"fill_alpha":0.1,"fill_color":"#1f77b4","line_alpha":0.1,"line_color":"#1f77b4","x":{"field":"x"},"y":{"field":"y"}},"id":"1083","type":"Patch"},{"attributes":{},"id":"1053","type":"UnionRenderers"},{"attributes":{"data_source":{"id":"1081","type":"ColumnDataSource"},"glyph":{"id":"1082","type":"Patch"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"1083","type":"Patch"},"selection_glyph":null,"view":{"id":"1085","type":"CDSView"}},"id":"1084","type":"GlyphRenderer"},{"attributes":{"label":{"value":"30n\\u00b2 + 194n"},"renderers":[{"id":"1046","type":"GlyphRenderer"}]},"id":"1054","type":"LegendItem"},{"attributes":{"source":{"id":"1081","type":"ColumnDataSource"}},"id":"1085","type":"CDSView"},{"attributes":{"column_name":"t","group":"n\\u00b2"},"id":"1055","type":"GroupFilter"},{"attributes":{"callback":null,"data":{"x":[],"y":[]},"selected":{"id":"1107","type":"Selection"},"selection_policy":{"id":"1108","type":"UnionRenderers"}},"id":"1086","type":"ColumnDataSource"},{"attributes":{"filters":[{"id":"1055","type":"GroupFilter"}],"source":{"id":"1001","type":"ColumnDataSource"}},"id":"1056","type":"CDSView"},{"attributes":{"below":[{"id":"1014","type":"LogAxis"}],"left":[{"id":"1019","type":"LogAxis"}],"plot_height":450,"renderers":[{"id":"1014","type":"LogAxis"},{"id":"1018","type":"Grid"},{"id":"1019","type":"LogAxis"},{"id":"1023","type":"Grid"},{"id":"1027","type":"Span"},{"id":"1039","type":"Legend"},{"id":"1033","type":"GlyphRenderer"},{"id":"1046","type":"GlyphRenderer"},{"id":"1060","type":"GlyphRenderer"},{"id":"1074","type":"GlyphRenderer"},{"id":"1079","type":"GlyphRenderer"},{"id":"1084","type":"GlyphRenderer"},{"id":"1089","type":"GlyphRenderer"},{"id":"1069","type":"Band"}],"title":{"id":"1003","type":"Title"},"toolbar":{"id":"1025","type":"Toolbar"},"x_range":{"id":"1006","type":"Range1d"},"x_scale":{"id":"1010","type":"LogScale"},"y_range":{"id":"1008","type":"Range1d"},"y_scale":{"id":"1012","type":"LogScale"}},"id":"1004","subtype":"Figure","type":"Plot"},{"attributes":{"line_dash":[6],"line_width":2,"x":{"field":"x"},"y":{"field":"y"}},"id":"1087","type":"Line"},{"attributes":{},"id":"1101","type":"Selection"},{"attributes":{"dimension":"height","line_dash":[6],"line_width":{"value":2},"location":2.7714285714285714,"plot":{"id":"1004","subtype":"Figure","type":"Plot"}},"id":"1027","type":"Span"},{"attributes":{"line_alpha":0.1,"line_color":"#1f77b4","line_dash":[6],"line_width":2,"x":{"field":"x"},"y":{"field":"y"}},"id":"1088","type":"Line"},{"attributes":{"data_source":{"id":"1086","type":"ColumnDataSource"},"glyph":{"id":"1087","type":"Line"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"1088","type":"Line"},"selection_glyph":null,"view":{"id":"1090","type":"CDSView"}},"id":"1089","type":"GlyphRenderer"},{"attributes":{"label":{"value":"2n\\u00b3"},"renderers":[{"id":"1033","type":"GlyphRenderer"}]},"id":"1040","type":"LegendItem"},{"attributes":{"callback":null,"tooltips":[["f(n)","@t"],["n","@n{0}"],["Pz","@y{0}"]]},"id":"1024","type":"HoverTool"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto","tools":[{"id":"1024","type":"HoverTool"}]},"id":"1025","type":"Toolbar"},{"attributes":{"source":{"id":"1086","type":"ColumnDataSource"}},"id":"1090","type":"CDSView"},{"attributes":{"column_name":"t","group":"2n\\u00b3"},"id":"1028","type":"GroupFilter"},{"attributes":{"label":{"value":"\\u2264 c\\u00b7n\\u00b2"},"renderers":[{"id":"1084","type":"GlyphRenderer"}]},"id":"1091","type":"LegendItem"},{"attributes":{"label":{"value":"log c"},"renderers":[{"id":"1074","type":"GlyphRenderer"}]},"id":"1092","type":"LegendItem"},{"attributes":{"filters":[{"id":"1028","type":"GroupFilter"}],"source":{"id":"1001","type":"ColumnDataSource"}},"id":"1029","type":"CDSView"},{"attributes":{"column_name":"t","group":"30n\\u00b2 + 194n"},"id":"1041","type":"GroupFilter"},{"attributes":{"label":{"value":"n\\u2080"},"renderers":[{"id":"1089","type":"GlyphRenderer"}]},"id":"1093","type":"LegendItem"},{"attributes":{"line_color":"deepskyblue","line_width":2,"x":{"field":"n"},"y":{"field":"y"}},"id":"1058","type":"Line"},{"attributes":{"dimension":1,"plot":{"id":"1004","subtype":"Figure","type":"Plot"},"ticker":{"id":"1020","type":"LogTicker"}},"id":"1023","type":"Grid"},{"attributes":{"callback":null,"end":291.92926025390625,"start":1.0},"id":"1006","type":"Range1d"},{"attributes":{"items":[{"id":"1040","type":"LegendItem"},{"id":"1054","type":"LegendItem"},{"id":"1068","type":"LegendItem"},{"id":"1091","type":"LegendItem"},{"id":"1092","type":"LegendItem"},{"id":"1093","type":"LegendItem"}],"location":"top_left","plot":{"id":"1004","subtype":"Figure","type":"Plot"}},"id":"1039","type":"Legend"},{"attributes":{"line_alpha":0.1,"line_color":"#1f77b4","line_width":2,"x":{"field":"n"},"y":{"field":"y"}},"id":"1059","type":"Line"},{"attributes":{},"id":"1104","type":"UnionRenderers"},{"attributes":{"data_source":{"id":"1001","type":"ColumnDataSource"},"glyph":{"id":"1058","type":"Line"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"1059","type":"Line"},"selection_glyph":null,"view":{"id":"1056","type":"CDSView"}},"id":"1060","type":"GlyphRenderer"},{"attributes":{},"id":"1012","type":"LogScale"},{"attributes":{"callback":null,"data":{"n":{"__ndarray__":"AAAAAAAA8D8AAAAAAAD4PwAAAAAAAAJAAAAAAAAAC0AAAAAAAEAUQAAAAAAAYB5AAAAAAADIJkAAAAAAABYxQAAAAAAAoTlAAAAAAMA4Q0AAAAAAINVMQAAAAADYn1VAAAAAAOI3YEAAAAAA01NoQAAAAEDePnJA","dtype":"float64","shape":[15]},"t":["cn","cn","cn","cn","cn","cn","cn","cn","cn","cn","cn","cn","cn","cn","cn"],"y":{"__ndarray__":"AAAAAAAAWUAAAAAAACBsQAAAAAAApH9AAAAAAEDMkUAAAAAAyAWkQAAAAACBhrZAAAAAIFFXyUAAAABEO4LcQAAAQFZBCfBAAAAIgWkKAkEAACmxtksUQQAgToct1SZBAOQ3ONOvOUGA4D6fzeVMQUhek6lDQWBB","dtype":"float64","shape":[15]},"y0":{"__ndarray__":"AAAAAAAA8D8AAAAAAADwPwAAAAAAAPA/AAAAAAAA8D8AAAAAAADwPwAAAAAAAPA/AAAAAAAA8D8AAAAAAADwPwAAAAAAAPA/AAAAAAAA8D8AAAAAAADwPwAAAAAAAPA/AAAAAAAA8D8AAAAAAADwPwAAAAAAAPA/","dtype":"float64","shape":[15]}},"selected":{"id":"1109","type":"Selection"},"selection_policy":{"id":"1110","type":"UnionRenderers"}},"id":"1002","type":"ColumnDataSource"},{"attributes":{},"id":"1105","type":"Selection"},{"attributes":{"axis_label":"n","formatter":{"id":"1035","type":"LogTickFormatter"},"plot":{"id":"1004","subtype":"Figure","type":"Plot"},"ticker":{"id":"1015","type":"LogTicker"}},"id":"1014","type":"LogAxis"},{"attributes":{},"id":"1106","type":"UnionRenderers"},{"attributes":{"num_minor_ticks":10},"id":"1015","type":"LogTicker"},{"attributes":{},"id":"1107","type":"Selection"},{"attributes":{"base":{"field":"n","units":"data"},"fill_alpha":{"value":0.5},"fill_color":{"value":"limegreen"},"level":"underlay","line_alpha":{"value":0.5},"line_color":{"value":"black"},"lower":{"field":"y0","units":"data"},"plot":{"id":"1004","subtype":"Figure","type":"Plot"},"source":{"id":"1002","type":"ColumnDataSource"},"upper":{"field":"y","units":"data"}},"id":"1069","type":"Band"},{"attributes":{},"id":"1108","type":"UnionRenderers"},{"attributes":{"plot":{"id":"1004","subtype":"Figure","type":"Plot"},"ticker":{"id":"1015","type":"LogTicker"}},"id":"1018","type":"Grid"},{"attributes":{"callback":null,"end":100000000,"start":1},"id":"1008","type":"Range1d"},{"attributes":{},"id":"1109","type":"Selection"},{"attributes":{"axis_label":"Laufzeit [Pz]","formatter":{"id":"1037","type":"LogTickFormatter"},"plot":{"id":"1004","subtype":"Figure","type":"Plot"},"ticker":{"id":"1020","type":"LogTicker"}},"id":"1019","type":"LogAxis"},{"attributes":{},"id":"1110","type":"UnionRenderers"},{"attributes":{"num_minor_ticks":10},"id":"1020","type":"LogTicker"}],"root_ids":["1004"]},"title":"Bokeh Application","version":"1.0.2"}}';
                  var render_items = [{"docid":"1234c930-951b-42f2-96ba-7f2d3e19b33c","roots":{"1004":"5ace92f9-b04c-419f-936f-3219cea9edd1"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing");
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-1.0.2.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-1.0.2.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-1.0.2.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-1.0.2.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-1.0.2.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-1.0.2.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();