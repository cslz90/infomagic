# Literaturempfehlungen für AuD - Sommersemester 2019
Christopher Schölzel

## Einführung - Deutsch

1. M. Dietzfelbinger, K. Mehlhorn und P. Sanders, *Algorithmen und Datenstrukturen* Online-Ausg.. Deutschland, Berlin-Heidelberg: Springer, 2014.
    * [in der THM-Bibliothek als eBook](https://hds.hebis.de/thm/Record/HEB342961438) vorhanden

## Einführung - Englisch

1. R. Lafore, *Data Structures and Algorithms in Java*, 2. Ausgabe. England, London: Pearson Education, 2002.
    * ISBN: 978-0672324536
    * arbeitet mit älterer Java-Version
    * wird aber in internationalen Fachkreisen als "bestes Einsteigerwerk" bezeichnet
    * in der THM-Bibliothek vorhanden

## Nachschlagewerke

1. T.H. Cormen, C.E. Leiserson, R.L. Rivest und C. Stein, *Introduction to Algorithms*, 3. Auflage. Cambrige, MA: MIT Press, 2009.
    * ISBN: 9780262533058
    * Bekannt als "der CLRS"
    * *Das* Standardwerk für Algorithmen und Datenstrukturen
    * In der THM-Bibliothek vorhanden
2. T.H. Cormen, C.E. Leiserson, R.L. Rivest und C. Stein, *Algorithmen - Eine Einführung*, 4. Auflage. Deutschland, München: De Gruyter Oldenbourg, 2013.
    * deutsche Übersetzung des CLRS
    * ISBN: 978-3486748611
3. Steven S. Skiena, *The Algorithm Design Manual*, 2. Auflage. Deutschland, Berlin-Heidelberg: Springer, 2011.
    * ebenfalls ein sehr gutes Standardwerk
    * andere (praktischere?) Herangehensweise als CLRS
    * In der THM-Bibliothek vorhanden

---

## Nachhilfe zu OOP

1. M. Kofler, *Java - Der Grundkurs*, 2. aktualisierte Auflage. Deutschland, Bonn: Rheinwerk Computing, 2017.
    * ISBN: 978-3-8362-4581-4
    * kompakte Java-Grundlagen
    * verwendet für OOP
    * Die 1. Auflage reicht völlig und ist in der THM-Bibliothek zu finden
2. C. Ullenboom, *Java ist auch eine Insel*, 12. aktualisierte und überarbeitete Auflage. Deutschland: Bonn: Rheinwerk Computing, 2016.
    * ISBN: 978-3-8362-4119-9
    * Das "Standardwerk" für Java
    * Online einsehbar: http://openbook.rheinwerk-verlag.de/javainsel/
3. R. Reiter, *interactive-tutorials*, http://www.learnjavaonline.org/about
    * Interaktives online-Tutorial für Java
4. Codecademy, *Learn Java*, https://www.codecademy.com/learn/learn-java
    * Teilweise freier Online-Kurs zu Java
5. B. Kjell, *Java Tutorial interaktiv — Programmieren lernen mit Java*, übersetzt von Heinrich Gailer, http://www.gailer-net.de/tutorials/java/java-toc.html
    * Einmonatiger online verfügbarer Java-Kurs
6. D. Panjuta, *Java Tutorial – Programmieren lernen für Anfänger*, https://panjutorials.de/tutorials/java-tutorial-programmieren-lernen-fuer-anfaenger/
    * deutscher Online-Kurs mit Videos
