package de.thm.mni.aud

object LinkedList extends App {

  sealed trait LList[E]

  case class Nill[E]() extends LList[E]

  case class Cons[E](head: E, tail: LList[E]) extends LList[E]

  def size[E](lst: LList[E]): Int = lst match {
    case Nill() => 0
    case Cons(_, t) => 1 + size(t)
  }

  def add[E](lst: LList[E], value: E): LList[E] = lst match {
    case Nill() => Cons(value, Nill())
    case Cons(h, t) => Cons(h, add(t, value))
  }

  def get[E](lst: LList[E], idx: Int): Option[E] = (lst, idx) match {
    case (Nill(), _) => None
    case (Cons(h, _), 0) => Some(h)
    case (Cons(_, t), i) => get(t, i-1)
  }

  def set[E](lst: LList[E], idx: Int, value: E): Option[LList[E]] = (lst, idx) match {
    case (Nill(), _) => None
    case (Cons(_, t), 0) => Some(Cons(value, t))
    case (Cons(h, t), i) => set(t, i-1, value).map(x => Cons(h, x))
  }

  def remove[E](lst: LList[E], idx: Int): Option[LList[E]] = (lst, idx) match {
    case (Nill(), _) => None
    case (Cons(_, t), 0) => Some(t)
    case (Cons(h, t), i) => remove(t, i-1).map(x => Cons(h, x))
  }

  def insert[E](lst: LList[E], idx: Int, value: E): Option[LList[E]] = (lst, idx) match {
    case (Nill(), 0) => Some(Cons(value, Nill()))
    case (Nill(), _) => None
    case (Cons(_, _), 0) => Some(Cons(value, lst))
    case (Cons(h, t), i) => insert(t, i-1, value).map(x => Cons(h, x))
  }

  var x = add(add(Nill(), 10),7)
  println(x)
  x = insert(x, 1, 3).get
  println(x)
  x = remove(x, 2).get
  println(x)
  println(size(x))
}
