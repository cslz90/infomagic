data LinkedList a = Nil | Cons a (LinkedList a) 
  deriving (Show)

size Nil        = 0
size (Cons h t) = 1 + size t

add a Nil        = Cons a Nil
add a (Cons h t) = Cons h (add a t)

get idx Nil        = Nothing
get 0   (Cons h t) = Just h
get idx (Cons h t) = get (idx-1) t

set a idx Nil        = Nil
set a 0   (Cons h t) = Cons a t
set a idx (Cons h t) = Cons h (set a (idx-1) t)

remove idx Nil        = Nil
remove 0   (Cons h t) = t
remove idx (Cons h t) = Cons h (remove (idx-1) t)

insert a 0 Nil          = Cons a Nil
insert a idx Nil        = Nil
insert a 0   (Cons h t) = Cons a (Cons h t)
insert a idx (Cons h t) = Cons h (insert a (idx-1) t)

main = do
  let lst = Cons 1 (Cons 2 Nil) :: LinkedList Integer
  let lst2 = Cons 7 lst
  let lst3 = add 5 lst2
  let lst4 = insert 10 1 (remove 2 (set 0 3 lst3))
  let get0 = get 0
  putStrLn ("Lst3:      " ++ show lst3)
  putStrLn ("size Lst3: " ++ show (size lst3))
  putStrLn ("Lst3[2]:   " ++ show (get 2 lst3))
  putStrLn ("Lst3[0]:   " ++ show (get0 lst3))
  putStrLn ("Lst4:      " ++ show lst4)