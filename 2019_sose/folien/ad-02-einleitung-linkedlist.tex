\documentclass{beamer}

\usepackage[ngerman, english]{babel}
\usepackage[utf8]{inputenc}

\usetheme{csthm}
\usepackage{csjavalst}

\usetikzlibrary{positioning}

\title{Algorithmen und Datenstrukturen}
\subtitle{2 -- Verkettete Liste}
\date{8. April 2019}
\author{Christopher Schölzel}
\titlegraphic{\includegraphics[width=\paperwidth]{img/Globus_Nuernberg.JPG}}

\newcommand{\type}[1]{\textit{#1}}
\newcommand{\term}[1]{\textcolor{hl1}{#1}}

\makeatletter
\newcommand{\emoji}[1]{%
  \tikz[baseline=(s.base)]{
    \node (s)[inner sep=0pt, outer sep=0pt] {
      \includegraphics[height=1.5ex]{#1}
    }
  }%
}
\makeatother

\newcommand{\dragon}{\emoji{img/u1F409-dragonside.pdf}}
\newcommand{\book}{\emoji{img/u1F4D6-openbook.pdf}}

\begin{document}

\maketitle

\begin{frame}{\dragon{} Erinnerung: Array-Basilist}
  \includegraphics[width=\textwidth]{img/ArrayBasilist_1024.png}
\end{frame}

\begin{frame}{\dragon{} Verketteter Basilist}
  \includegraphics[width=\textwidth]{img/LinkedListMitMagie.png}

  \vfill

  \begin{block}{Definition: Verketteter Basilist (\type{Datenkreatur})}
    \term{Verkettete Basilisten} haben im Gegensatz zu Array-Basilisten \stress{mehrere Körper}.
    Um gegen die größeren Rivalen bestehen zu können, schließen sie sich zusammen, indem sie \stress{einander in den Schwanz beißen}.
    An der Spitze sitzt dabei immer der \stress{dominante Kopf}.
    Man sollte sehr darauf achten diesem zuerst Aufmerksamkeit zukommen zu lassen, da er ansonsten bissig wird.
  \end{block}
\end{frame}

\begin{frame}{\dragon{} Nil}
  \begin{center}
    \includegraphics[width=.3\textwidth]{img/Nil.png}
  \end{center}

  \vfill

  \begin{block}{Definition: Nil (\type{Verketteter Basilist})}
    Ein \term{Nil} ist ein verketteter Basilist im Larvenstadium.
    Es ist noch zu klein, um Dinge zu speichern, oder andere Basilisten an seinem Schwanz zu tragen.
    Damit es nicht trotzdem gebissen wird, hat es einen Abwehrstachel am Hinterteil.
    Nils findet man daher entweder \stress{alleine} oder \stress{ganz am Ende} von verketteten Basilisten.
  \end{block}
\end{frame}

\begin{frame}[fragile]{\book{} Verkettete Liste}
  \begin{block}{Definition: Verkettete Liste}
    Eine \term{Verkettete Liste} hinterlegt ihre Elemente \stress{einzeln} im Speicher.
    Zusätzlich zu dem \stress{Wert} des Elements wird auch ein \stress{Verweis} auf das nächste Element gespeichert.
    Anders als bei einer Array-Liste können die Daten \stress{beliebig im Speicher verteilt} liegen.
  \end{block}

  \vfill

  \begin{center}
    \begin{tikzpicture}[cell/.style={draw, outer sep=0pt, minimum width=20pt, minimum height=20pt}]
      \node[cell] (v1) at (0,0) {8};
      \node[cell, right=0pt of v1] (p1) {};

      \node[cell, right=20pt of p1] (v2) {2};
      \node[cell, right=0pt of v2] (p2) {};

      \node[cell, right=20pt of p2] (v3) {5};
      \node[cell, right=0pt of v3] (p3) {};

      \node[cell, fill=black, right=20pt of p3] (nil) {};

      \path[->, draw, very thick] (p1.center) -- (v2.west);
      \path[->, draw, very thick] (p2.center) -- (v3.west);
      \path[->, draw, very thick] (p3.center) -- (nil);
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}{\book{} Verkettete Liste mit Nil und Cons}
  Typische Implementierung einer Verketteten Liste:

  \vfill

  \begin{columns}[onlytextwidth, T]
    \begin{column}{.49\textwidth}
      \begin{block}{Datentyp \texttt{Nil}}
        \begin{itemize}
          \item entspricht einer \stress{leeren Liste}
          \item kein Wert
          \item keine Verweise
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{.49\textwidth}
      \begin{block}{Datentyp \texttt{Cons}}
        \begin{itemize}
          \item entspricht einem \stress{Listenelement}
          \item enthält Wert
          \item und Verweis
            \begin{itemize}
              \item auf ein \lstinline|Cons|, wenn die Liste noch weiter geht
              \item auf ein \lstinline|Nil|, wenn dieses \lstinline|Cons| das letzte ist
            \end{itemize}
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}

  \vfill\pause

  Vorteil: Keine leeren Verweise (\textit{null pointer})
\end{frame}

% \begin{frame}[t]{\dragon{} Reise nach Haskellien}
%   \begin{center}
%     \begin{tikzpicture}
%         \node[anchor=south west,inner sep=0] at (0,0) {
%           \includegraphics[width=.9\textwidth]{img/Karte_V1_5.jpg}
%         };
%         \path[blue,line width=1mm,->] (2.7,6) edge[bend right] (7.7,6.5);
%     \end{tikzpicture}
%   \end{center}
% \end{frame}


\lstset{language=Scala}
\lstset{
  morekeywords=[2]{Nill, Cons},
  keywordstyle=[2]\color{hl3}
}

\begin{frame}[fragile]{\book{} Verkettete Liste in Scala}
  \begin{center}
    \begin{minipage}{.7\textwidth}
      \begin{lstlisting}
        sealed trait LList[E]

        case class Nill[E]() extends LList[E]

        case class Cons[E](head: E, tail: LList[E])
          extends LList[E]
      \end{lstlisting}
    \end{minipage}
  \end{center}

  \pause
  \begin{itemize}
    \item definiere Datentyp \code{LList[E]}
      \begin{itemize}
        \item \code{E} ist ein Typparameter (z.B. \code{LList[Int]})
      \end{itemize}
    \item eine Liste ist entweder ein \code{Nill} oder ein \code{Cons}
    \item ein \code{Cons} besteht aus zwei Dingen
      \begin{itemize}
        \item ein Element vom Typ \code{E}
        \item eine weitere Liste vom Typ \code{LList[E]}
      \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}[fragile]{\book{} Umgang mit Listen in Scala}
  \begin{lstlisting}
    def size[E](lst: LList[E]): Int = lst match {
      case Nill() => 0
      case Cons(_, t) => 1 + size(t)
    }
  \end{lstlisting}

  \pause
  \begin{itemize}
    \item \code{size} ist eine Funktion, die eine \code{LList} übernimmt
    \item was die Funktion tut, hängt von deren Zusammensetzung ab
      \begin{itemize}
        \item die Länge eines \code{Nill} ist \code{0}
        \item die Länge eines \code{Cons} ist
          \begin{itemize}
            \item seine eigene Länge (\code{1})
            \item plus die Länge des Schwanzes (\code{size t})
          \end{itemize}
      \end{itemize}
    \pause
    \item das Prinzip der Entscheidung nach dem Typ der Argumente nennt man \term{pattern matching}
    \item in funktionalen Sprachen wie Scala sind \term{rekursive} Definitionen weit verbreitet
  \end{itemize}
\end{frame}

\begin{frame}<1-4>[fragile]{\book{} Verkettete Liste: Hinzufügen}
  \begin{center}
    \begin{minipage}{\textwidth}
      \begin{lstlisting}[style=cs-disabled, classoffset=1, keywordstyle=\color{hl3!50}]
        def size[E](lst: LList[E]): Int = lst match {
          case Nill() => 0
          case Cons(_, t) => 1 + size(t) }
      \end{lstlisting}

      \vspace{10mm}

      \begin{lstlisting}[style=uncover]
        def add[E](lst: LList[E], value: E): LList[E] = lst match {
          case Nill() => §Cons(value, Nill())§
          §§case Cons(h, t) =>§§ §§§Cons(h, add(t, value))§§§
        }
      \end{lstlisting}
    \end{minipage}
  \end{center}
\end{frame}

\begin{frame}[fragile]{\book{} Verkettete Liste: Zugriff}
  \begin{center}
    \begin{minipage}{\textwidth}
      \begin{lstlisting}[style=uncover]
        def get[E](lst: LList[E], idx: Int): Option[E] =
          (lst, idx) match {
            case (Nill(), _) => §None§
            §§case (Cons(h, _), 0) =>§§ §§§Some(h)§§§
            §§§§case (Cons(_, t), i) => get(t, i-1)§§§§
          }
      \end{lstlisting}
    \end{minipage}
  \end{center}

  \vfill

  \pause[6]
  \begin{itemize}
    \item \term{pattern matching} funktioniert mit allen Datentypen
    \item statt Exceptions gibt es in Scala den Rückgabetyp \code{Option[E]}
      \begin{itemize}
        \item entweder \code{None}
        \item oder \code{Some(x)} mit irgeneinem \code{x} vom Typ \code{E}
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\book{} Verkettete Liste: Set}
  \begin{lstlisting}
    def set[E](lst: LList[E], idx: Int, value: E): Option[LList[E]] = 
      (lst, idx) match {
        case (Nill(), _) => None
        case (Cons(_, t), 0) => Some(Cons(value, t))
        case (Cons(h, t), i) => 
          set(t, i-1, value).map(x => Cons(h, x))
  \end{lstlisting}

  \pause\vfill

  \begin{itemize}
    \item \code{map()} macht eine \code{Option[E]} zu einer \code{Option[T]}
    \item[$\Rightarrow$] man kann den Typ und den enthaltenen Wert verändern
    \item bei \code{None} wird aber immer noch \code{None} zurückgegeben
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\book{} Verkettete Liste: Remove}
  \begin{lstlisting}
    def remove[E](lst: LList[E], idx: Int): Option[LList[E]] =
      (lst, idx) match {
        case (Nill(), _) => None
        case (Cons(_, t), 0) => Some(t)
        case (Cons(h, t), i) => 
          remove(t, i-1).map(x => Cons(h, x))
    }
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{\book{} Verkettete Liste: Insert}
  \begin{lstlisting}
    def insert[E](lst: LList[E], idx: Int, value: E): Option[LList[E]]
    = (lst, idx) match {
      case (Nill(), 0) => Some(Cons(value, Nill()))
      case (Nill(), _) => None
      case (Cons(_, _), 0) => Some(Cons(value, lst))
      case (Cons(h, t), i) => 
        insert(t, i-1, value).map(x => Cons(h, x))
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile,t]{\book{} Verkettete Liste: Beispiel}
  \begin{lstlisting}
    var x = add(Cons(7, Nill()),5)
    println(x)
    x = insert(x, 1, 3).get
    println(x)
    x = remove(x, 2).get
    println(x)
    println(size(x))
  \end{lstlisting}

  \vfill

  \begin{itemize}
    \pause
    \item \strut\code{Cons(7, lst)} hängt eine \code{7} \stress{vorne} an \code{lst} an
    \pause
    \item \strut\code{add(lst, 5)} hängt eine \code{5} \stress{hinten} an \code{lst} an
    \pause
    \item \strut\code{get} greift auf den Inhalt einer \code{Option} zu (falls dieser existiert)
  \end{itemize}
\end{frame}

\begin{frame}[fragile,t]{\dragon{} Wer würde gewinnen?}

  \begin{tikzpicture}[x=.5\textwidth]
    \node[rotate=-35, xscale=-1] (linked) at (0,0) {\includegraphics[width=.5\textwidth]{img/LinkedList_nofeed.png}};
    \node[rotate=35] (array) at (1,0) {\includegraphics[width=.5\textwidth]{img/ArrayBasilist_1024.png}};
    \path[->, draw, very thick] (0.6,2) -- (0.4,0.5) -- (0.6, 1) -- (0.4, -0.5);
  \end{tikzpicture}

  \vfill

  \begin{itemize}
    \item Nach welchen Regeln läuft der Kampf?
    \begin{itemize}
      \item Vorschriften für unorganisierte Kampfhandlungen (Ja'va)?
      \item Codex Scaladria?
    \end{itemize}
    \item Was entscheidet den Kampf?
      \begin{itemize}
        \item Entscheidende Vor- oder Nachteile?
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Bildquellen}
  \begin{center}
    \includegraphics[width=.9\textwidth]{img/LinkedListMitMagie.png}
  \end{center}

  \begin{itemize}
    \item Emojis: Fxemoji, Mozilla Foundation, Lizenz: CC BY 4.0
    \item Titelbild: Christopher Schölzel
    \item Basilisten: Julia Jelitzki
    \item Sonstige Bilder: Pixabay, Lizenz: Public Domain
  \end{itemize}
\end{frame}

\end{document}
