# Klausur TODOs/Ansagen

- [ ] wer teilt welche Deckblätter aus?
- [ ] alle Zettel außer Spickzettel vom Tisch
- [ ] alle Studierendenausweise auf den Tisch
- [ ] wer sich nicht gesundheitlich in der Lage fühlt zu schreiben, soll sich melden.
- [ ] Ansage, dass Deckblatt getackert wird
- [ ] immer nur eine Person gleichz. auf Toilette => kurz melden
- [ ] Zeitansage bei 60/30/15 min verbleibend
- [ ] 15 min vor Schluss geht keiner mehr
- [ ] Austeilen und Einsammeln in gleicher Reihenfolge
- [ ] Tutoren: Melden, wer spickt (Matrikelnummer aufschreiben)
