## Anleitung Bewertung eintragen:

Dies ist der Weg die Bewertungen einzeln einzutragen. Ich finde, dass ist für unseren Stil am sinnvollsten. Die Abgaben über die gesamte Liste einzutragen ist auch möglich, aber schwerer zu suchen / unübersichtlicher

### 0: 
- *Wichtig* Das Ergebnis in der Bio-Cloud in dem jeweilige AB eintragen. Dies könnt ihr theoretisch alles auf einen Schlag machen
- https://bio-cloud.mni.thm.de/index.php/apps/files/?dir=/AuD%20SoSe19&fileid=6212

### 1:
- Die passende Aufgabe heraussuchen und auswählen
- Den Knopf "Bewertung" drücken

### 2:
- Es kommt eine Übersicht auf. Oben rechts in der Suchleiste die jeweilige Person aussuchen.
- Hier unter "Bewertung" das jeweilige Ergebnis eintragen
- Der Status des Bewertungsworkflows auf "fertig zur Freigabe" sofern alles klar ist, ansonsten "wird Überprüft"
- Zugeordneter Bewerter: ihr selbst
- Feedback ist selbsterklärend. Hierbei als Fließtext oder Stichpunktartig Hinweise oder Fehler auflisten. Am besten als letzten Satz noch einmal die Bewertung hinschreiben (Farbe)
- Speichern und nächster
