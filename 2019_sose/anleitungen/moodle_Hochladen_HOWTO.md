## Anleitung Aufgaben hochladen auf Moodle:

### 1: 
 - In dem jeweiligen Abschnitt "Material oder Aktivität anlegen" auswählen
 - "Aufgabe" auswählen

### 2: 
Aus Gitlab die .md Datei aussuchen und "raw"-öffnen (2-ter Knopf neben "Edit")

### 3: 
Mithilfe von einem Konverter (Pandoc etc) das .md in html umwandeln.

### 4: 
Über den "eckigen Pfeil nach unten" die Menüleiste umschalten, dann rechts unten "</>" auswählen.

### 5: 
Den Inhalt raus löschen und den konvertierten Inhalt einfügen

### 6: Folgende Einstellungen:
 - Abgabebeginn deaktivieren
- Fälligkeitsdatum passend auf 23:55
- Letzte Abgabemöglichkeit: 23:59
- Bewertung erinnern auschalten
- Unter Abgabetypen: Nur Dateiabgabe, nur PDF akzeptiert indem man ".pdf" eingibt
- Unter Feedback-Typen: Feedback als Kommentar
- Unter Bewertung: Typ Skala auswählen -> *Ampel* . Dann ebenfalls Bewertungsworkflow und Bewerterzuordnung auf "Ja"
- Unter "Weitere Einstellungen" die Verfügbarkeit auf "Verborgen" setzen