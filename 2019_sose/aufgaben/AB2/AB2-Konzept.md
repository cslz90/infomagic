# Aufgabenblatt 2
## Pflichtaufgaben

|Nr | Thema                     | Name                                              | Typ      | Dauer in min   | XP  | Ref |
|---| ---                       | ---                                               | -------- | ---            | --- | --- |
|01 | Gier   (UNFERTIG)                   | AB2-Gier-Hillclimbing         | Unittest |2h                   | 25                |  [Link](2018_ss/aufgaben/AB2/AB2-Hillclimbing/AB2-Hillclimbing-klar.md)                       |
|02 | Backtracking    (UNFERTIG)          | AB2-Backtracking            | IO |  Geschätzt: 2h   | 25                |  [Link](2018_ss/aufgaben/AB2/AB2-IO-Backtracking/AB2-IO-Backtracking-klar.md)                   |
|03 | Teile und Herrsche        | AB2-Recherche-Master-Theorem  | Text | Geschätzt: 2h    | 25                |  [Link](2018_ss/aufgaben/AB2/AB2-Recherche-Master-Theorem/AB2-Recherche-Master-Theorem-klar.md) |
|04 | Dynamische Programmierung | AB2-DP-Tribonati              | Unittest | Geschätzt: 2h    | 25                |  [Link](2018_ss/aufgaben/AB2/AB2-DP-Tribonati/AB2-DP-Tribonati-klar.md)                         |

## Bonusaufgaben

|Nr | Thema                         | Name                                              | Typ      | Dauer in min   | XP  | Ref |
|---| ---                           | ---                                               | -------- | ---            | --- | --- |
|01 | Backtracking                      | AB2-Bonus-Backtracking-ModerneOrkwaffen       | Unittest | Geschätzt: 2h| ??? | ???  |  [Link](2018_ss/aufgaben/AB2/AB2-Bonus-Backtracking-ModerneOrkwaffen/AB2-Bonus-Backtracking-ModerneOrkwaffen-klar.md) |
|02 | Dynamische Programmierung         | AB2-Bonus-Levenshtein-Tempelrätsel         | IO | Geschätzt: 2h | ??? | ???                |  [Link](2018_ss/aufgaben/AB2/AB2-Bonus-Levenshtein-Tempelraetsel/AB2-Bonus-Levenshtein-Tempelraetsel-klar.md)                         |
