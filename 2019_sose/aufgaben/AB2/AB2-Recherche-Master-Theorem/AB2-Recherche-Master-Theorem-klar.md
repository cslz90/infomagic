## Das Elderbuch

### Vorwort

Diese Aufgabe ist eine Rechercheaufgabe wie "Wilde Stammbaumwürmer" aus Aufgabenblatt 0 und folgt daher den gleichen Regeln.

### Aufgabe 1.1

Recherchieren Sie das Master-Theorem (deutsch "Hauptsatz der Laufzeitfunktionen") zur Bestimmung der Effizienzklasse von rekursiven Funktionen.
Beantworten Sie dabei in maximal 700 Worten Fließtext (mit vollständigen Sätzen ohne Stichpunkte!) die folgenden Fragen:

* Wie lautet die Aussage des Master-Theorems? Versuchen Sie die mathematische Aussage in verständlicheren Worten zu erklären.
* Wozu wird das Master-Theorem verwendet?
* Was ist die Mastermethode? Wie hängt Sie mit dem Master-Theorem zusammen?
* Geben Sie ein Beispiel für die Anwendung des Master-Theorems auf den Mergesort-Algorithmus. Wie hilft das Theorem, um die Laufzeit von Mergesort zu bestimmen? Welchen Wert nehmen die darin verwendeten Konstanten an?

**Hinweise:**

* Bitte achten Sie auf eine korrekte Rechtschreibung und Grammatik, auch diese fließt in die Bewertung ein.
* So spaßig die Gamifizierung auch ist, antworten Sie hier aus Gnade mit den Tutoren bitte in Klartext.

### Aufgabe 1.2

Geben Sie die Quellen an, die Sie für Ihre Recherche verwendet haben. Hier reicht jeweils ein Link auf die Webseite oder der Titel des Buches, das Sie verwendet haben.
Es geht hier nicht darum herauszufinden, ob sie Formulierungen kopiert haben (was Sie natürlich nicht tun sollten), sondern darum, dass Sie zeigen, dass Sie in der Lage sind, geeignete Quellen zu finden und auszuwählen.

Bitte geben Sie auch nicht *alle* Quellen an, die Sie gelesen haben, sondern nur diejenigen, auf deren Inhalt Sie sich in Ihrem Text stützen.
Es sind maximal fünf Quellen erlaubt.