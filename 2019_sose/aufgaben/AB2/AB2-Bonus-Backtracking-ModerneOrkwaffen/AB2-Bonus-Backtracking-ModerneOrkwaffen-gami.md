# Moderne Orkwaffen

*Einige Völker, die sich selbst als den Gipfel der Zivilisation ansehen, schauen häufig geringschätzend auf die Orks herab und belächeln ihre primitive Gesellschaft und Technologie. Doch so wild und unzivilisiert die Orks auch scheinen: Wenn es um die Entwicklung neuer Waffentechnologie geht, laufen sie jedem Elfengelehrten oder Zwergeningenieur den Rang ab.*

*Eine der neuesten Errungenschaften aus den orkischen Schmieden ist eine stationäre Waffe, die brennendes Öl auf große Distanz sternförmig in alle Richtungen verschießen kann. Die Auslassrohre für das Öl sind in 45°-Winkeln angeordnet. Die Waffe wird als Feuerspucker bezeichnet.*

*Die Orks wollen diese Waffe nun nutzen, um eine Schlucht zu verteidigen, über die Angreifer in ihr Land eindringen könnten. Dafür sollen möglichst viele der Feuerspucker so in der Schlucht platziert werden, dass sie den gesamten Bereich mit Feuer bedecken können. Bei der Anordnung muss natürlich bedacht werden, dass die Orks sich nicht gegenseitig mit Flammen besprühen.*

*Um die Platzierung der Feuerspucker planen zu können, wird vom zuständigen General der orkischen Garnison eine Karte angelegt, auf der das Gebiet in Quadranten eingeteilt ist. Ein Feuerspucker kann entsprechend des Winkels der Auslassrohre auf der Karte horizontal, vertikal und diagonal schießen.*

*Da ihre Kenntnisse in der Algomantik weithin bekannt sind, fällt ihnen die Aufgabe zu, eine Positionierung zu finden, in der alle geplanten Waffen aufgestellt werden können, ohne sich dabei gegenseitig zu behindern. Dazu erhalten sie die Karte des Orkgenerals und die Anzahl der zu positionierenden Feuerspucker.*

## Aufgabe:

*Schreiben Sie einen Zauber `public static List<Coordinate> place(int nOrcs, int nSize)`. Der Zauber erhält die Anzahl (`nOrcs`) an Orks mit Feuerspuckern und die Seitenlänge der quadratischen Karte (`nSize`). Der Zauber gibt eine Liste von `Coordinate`-Objekten (s.u.) zurück, die die Koordinaten aller platzierten Waffen enthält.*

*Schreiben sie zusätzlich den Hilfszauber `public static boolean canPlace(int[][] field, Coordinate position)`. Dieser Zauber erhält eine Karte als 2D-Array (`field`) und die Position, an der eine Waffe platziert werden soll (`position`). Sie gibt einen Wahrheitswert zurück, der besagt, ob sich die Waffe an der Stelle platzieren lässt. Auf der Karte gilt eine Position als besetzt, wenn sie auf der Karte mit einem Wert über `0` markiert ist.*

## Formalia:

- Die Methoden `place` und `canPlace` sind Teil der Klasse `OrcWeapons` im Package `orcweapons`.
- Nutzen Sie für Ihre Lösung Backtracking.
- Kann keine Lösung gefunden werden, gibt `place()` eine leere Liste zurück.
- Für die Spielfiguren (Damen, bzw. Orks) gelten die normalen Bewegungsregeln für Damen im Schach: Sie können sich beliebig weit horizontal, Vertikal und Diagonal auf dem Feld bewegen.

[Abgabe in Dozentron](https://dozentron.mni.thm.de/submission_tasks/39)

### Codevorgabe

Die Klasse `Coordinate` ist vorgegeben:

```java
package orcweapons;

import java.util.Objects;

public class Coordinate {
    public final int x, y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return x == that.x &&
                y == that.y;
    }

    @Override
    public int hashCode() {

        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return String.format("{%d, %d}", x, y);
    }
}
```
