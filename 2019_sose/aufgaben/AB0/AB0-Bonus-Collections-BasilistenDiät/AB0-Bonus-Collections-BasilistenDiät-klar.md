# Eine Diät für Basilisten

## Aufgabe 1: Programmierung
Schreiben Sie eine Funktion `List<A> distinct(List<A> lst)`, die mithilfe eines [Sets](http://docs.oracle.com/javase/7/docs/api/java/util/Set.html) eine neue Liste generiert, die keine Duplikate enthält.  

	package basilistendiät;

	public class Abspeckprogramm {

   		public static <E> List<E> distinct(List<E> lst) {
        	// code here
    	}
	}


## Aufgabe 2: Recherche

1. Was unterscheidet Sets von Listen?
2. Angenommen Sie fügen 5 Zahlen in der Reihenfolge 1,2,3,4,5 einem Set hinzu. In welcher Reihenfolge können die Werte entnommen werden?
3. Aus welchen Gründen müssen Elemente in einem TreeSet vergleichbar (comparable) sein? Was hat das mit der Datenstruktur zu tun?
>Werfen Sie einen Blick in die [Java API](http://docs.oracle.com/javase/7/docs/api/java/util/Set.html).
