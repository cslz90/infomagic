== Aufgabe 1: Ein Abspeckprogramm muss her
_Auf regelmäßigen Wanderungen ist Ihnen aufgefallen, dass die gefräßigen Basilisten ein
Vielfaches der nötigen Nahrung zu sich nehmen und zu Übergewicht tendieren.
Im Namen des Artenschutzes entwickeln sie einen Zauber `List<A> distinct(List<A> lst)`,
der mithilfe eines http://docs.oracle.com/javase/7/docs/api/java/util/Set.html[Sets] die
überfressenen Basilisten auf Diät setzt und dafür sorgt, dass sie sich nun normal ernähren._

_Hierbei handelt es sich um einen Zauber, der die Basilisten in eine Art "Boot Camp" mit Sets schickt,
damit diese das Essverhalten der Basilisten korrigieren. Sets sind sehr ernährungsbewusste Datenkreaturen
mit einer schlanken Figur. Sie nehmen nur maximal einen Happen von jeder für sie verträglichen Nahrung auf.
Dieses Verhalten sollen sie an die krankhaft übergewichtigen Basilisten im Boot Camp übertragen._

_Durch ihre elegante Figur und ihrem Hang zum Posieren sind Sets bis in die nicht-magische Welt berühmt.
Nicht um sonst heißen die Fotovisitenkarten, mit denen Models und Schauspieler erhoffen möglichen Arbeitgebern ihre Vorzüge zu präsentieren, Setcards._

==== Formalia:
- Implementieren Sie eine Klasse `Abspeckprogramm` im Package `basilistendiät`.
- Implementieren Sie folgende Methode:
```java
  public static <E> List<E> distinct(List<E> lst)
```

http://dozentron.mni.thm.de/submission_tasks/75[Abgabe in Dozentron]


== Aufgabe 2: Trendkost der Woche: Sets
_Allen Anschein nach, funktioniert dieser Diätzauber ganz wunderbar und schon nach einiger Zeit
rekeln sich die verschlankten Basilisten wieder in der Sonne. Aber warum eignet sich die temporäre Versorgung
durch Sets besonders, um für eine ausgewogene Ernährung bei den neuen Basilisten zu sorgen?_

_Nach Ihren erfolgreichem Zauber bitten einige Tierschützer, aus einer Pflegestation, um Ihre Mithilfe. Damit auch die in Gehegen gehaltenen Basilisten eine ausgewogene Ernährung erhalten, bitten Sie einige Sets die Mahlzeiten vorzubereiten.
Jedes Set erhält Integerlinge von 1 bis 5 in aufsteigender Reihenfolge.
In welcher Reihenfolge bekommen die Basilisten ihre Mahlzeiten von den Sets?_

_Um die Diät Ihres eigenen Basilisten zu regulieren zu können, sollten die vom Set enthaltenen Elemente vergleichbar sein._

  - Wieso?
  - Was hat das mit der Datenstruktur zu tun?

==== Formalia:
  1. Was unterscheidet Sets von Listen?
  2. Angenommen Sie fügen 5 Zahlen in der Reihenfolge 1,2,3,4,5 einem Set hinzu. In welcher Reihenfolge können die Werte entnommen werden?
  3. Aus welchen Gründen müssen Elemente in einem TreeSet vergleichbar (comparable) sein? Was hat das mit der Datenstruktur zu tun?

https://moodle.thm.de/mod/assign/view.php?id=166137[Aufgabe in Moodle]
