# Abbildungen Ork Grunzer

## Aufgabe 1
*Das Gleichstellungsbüro für anderweitige Fabelwesen bietet Hilfe bei der Kommunikation mit den neu-eingeschriebenen Orks.*
*Dafür wurde folgendes Pergament mit der Übersetzung von Grunz-Lauten in lexikografische Zeichen bereitgelegt:*  
(`morse-code.txt`)

*Entwickeln sie zunächst einen Zauber, der die Abbildung von lexikografischen Zeichen auf Grunzlaute aus dem Pergament in eine [Java Map](https://docs.oracle.com/javase/7/docs/api/java/util/Map.html) umwandelt (`public static Map<Character, String> getOrcDictionary(String filePath)`).*

*Da es sehr umständlich ist, jeden Grunzer einzeln zu übersetzen, empfiehlt der Sachbearbeiter des Gleichstellungsbüros für anderweitige Fabelwesen ihnen einen zweiten Zauber zu entwickeln, der ganze Texte auf einmal übersetzen kann. Dieser Zauber soll `public static String encode(String input)` heißen und `getOrcDictionary` zu Hilfe nehmen. *

### Hinweise:  
> - Schreiben Sie eine Klasse `OrkGrunzer` im Package `orkgrunzer`, welche die Methoden `getOrcDictionary` und `encode` enthält.  
> - Zum Parsing eignen sich reguläre Ausdrücke. Ein visueller Editor für reguläre Ausdrücke ist [hier](https://regex101.com/) zu finden.
> - Zwischen Gross- und Kleinschreibung wird nicht unterschieden.

[Abgabe in Dozentron](Link hier einfügen)

## Aufgabe 2
*Wie Sie sicherlich wissen, benötigt das magische Schriftstück "[Java Map](https://docs.oracle.com/javase/7/docs/api/java/util/Map.html)" sogenannte "Schlüssel" und "Werte".*
*Je nachdem, ob Sie eine `HashMap` oder eine `TreeMap` verwenden, gelten unterschiedliche Einschränkungen für ihre möglichen Schlüssel.*
* Welche Bedingung müssen Schlüssel einer HashMap erfüllen, welche die Schlüssel einer TreeMap?

### Hinweis
> *Eine Recherche in der magischen Bücherkollektion "Java API" kann hilfreich sein.*

[Freitextfeld in Gildamesh]()
