# Ork Grunzer

## Aufgabe 1
Gegeben sei die folgende Zuordnung von Buchstaben zu Morse-Code Darstellung. [(`morse-code.txt`)](https://gildamesh.mni.thm.de/uploads/course/component/output/file/file/172/morse-code.txt)

Lösen sie die Aufgabe in zwei Schritten, indem sie Zwei Methoden schreiben:
- `public static Map<Character, String> getOrcDictionary(String filePath)` generiert aus einer Datei eine [Java Map](https://docs.oracle.com/javase/7/docs/api/java/util/Map.html), die jeden Buchstaben auf seinen Morse-Code abbildet.
- `public static String encode(String input)` generiert mit Hilfe von `getOrcDictionary` aus einem String seine Repräsentation in Morse-Code.

Beide Methoden sind Teil der Klasse `OrkGrunzer` im Package `orkgrunzer`.

### Hinweise:
> - Zum Parsing eignen sich reguläre Ausdrücke. Ein visueller Editor für reguläre Ausdrücke ist zum Beispiel [regex101](https://regex101.com/).
> - Zwischen Gross- und Kleinschreibung wird nicht unterschieden.
> - Die Datei `morse-code.txt` muss bei der Abgabe nicht der Jar beigefügt werden. Es kann beim Zugriff davon ausgegangen werden, dass sich die Datei im gleichen Verzeichnis befindet. Es kann also direkt auf sie zugegriffen werden.

## Aufgabe 2
Je nach dem ob Sie eine `HashMap` oder eine `TreeMap` verwenden, gelten unterschiedliche Einschränkungen für ihre möglichen Schlüssel.
Welche Bedingung müssen Schlüssel einer HashMap erfüllen, welche die Schlüssel einer TreeMap?

[Abgabe in Gildamesh](https://gildamesh.mni.thm.de/courses/1/components/167)

### Hinweis:
> Ein Blick in die Java API kann hilfreich sein.
