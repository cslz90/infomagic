## Wilde Stammbaumwürmer

*Haben Sie schon einmal einem Basilisten eine Wurmkur verpasst?
Nein?
Seien sie froh!
Erstens sind die sogenannten "wilden Stammbaumwürmer", die diese Wesen befallen, ziemlich schwer zu fassen.
Zweitens ist es aber auch gar keine so gute Idee, sie zu entfernen, denn sie leben eigentlich in Symbiose mit ihrem Wirt.*

*Wilde Stammbaumwürmer erkennt man an ihrer charakteristischen Form. Sie haben einen zu einem Halbkreis gebogenen Schwanz und ein einzelnes Stielauge, wie die folgende Darstellung zeigt:* `?`

*Diese faszinierenden Kreaturen leben im Darm von Basilisten und anderen Datenkreaturen und erlauben ihnen, mit Nahrung umzugehen, die sonst unbekömmlich für sie wäre.
Die sogenannten `? super T`-Würmer erlauben das Ausgeben und Speichern der Nahrung in einer Vielzahl von Verstecken.
Die sogenannten `? extends T`-Würmer sorgen dagegen dafür, dass sehr spezifische und exotische Nahrung aufgenommen und verdaut werden kann.*

### Aufgabe 1.1

*Schreiben Sie einen Eintrag zu wilden Stammbaumwürmern in Ihr Bestiarium.
Wagen Sie sich dazu ins Netz der großen Google-Spinne oder in die verbotene Bibliothek der Thaumaturgischen Hochschule - je nach dem, was Ihnen weniger gefährlich erscheint.*

*Beantworten Sie dabei die folgenden Fragen:*

* *Was ist der Muggelname für wilde Stammbaumwürmer?*
* *Welche Worte würden Sie wählen, um einen Zauberspruch mit einem einfachen Stammbaumwurm (`?`) für Novizen verständlicher zu machen.
    Was würden Sie diesen zum Beispiel sagen, wenn Sie nach anderen Worten für `Collection<?>` suchen?*
* *Wie unterscheiden sich `? super T`-Würmer von `? extends T`-Würmern?*
* *Wozu verwendet man wilde Stammbaumwürmer in einer algomantischen Formel?*
* *Was ist der konkrete Vorteil der Stammbaumwürmer, die in der Formel `Collections.copy(List<? super T>, List<? extends T>)` aus dem Buch `java.util.Collections` vorkommen?
    Wie würde die Wirkung der Formel sich verändern, wenn man die Stammbaumwürmer entfernt?*

*Denken Sie daran, dass Bestiarien sehr bissig werden, wenn man einen Eintrag von mehr als 400 Worten schreibt oder keine vollständigen Sätze verwendet.*

> Hinweis: So spaßig die Gamifizierung auch ist, antworten Sie hier aus Gnade mit den Tutoren bitte in Klartext.

### Aufgabe 1.2

*Um ihren Mitnovizen zu helfen, genauere Studien anzustellen, sprechen Sie bitte einen kurzen Ortungszauber, um ihnen den Weg zu Ihren Informationsquellen zu weisen.
Da Novizen sich schnell von zu vielen Informationen verwirren lassen, beschränken Sie sich auf fünf davon.*
