# Objektorientierung

## Aufgabe 1
*Ein Novize aus Funktionalia hat von Ihrer Reise durch den Objektorient gehört und will mehr über diesen mysteriösen Ort hören.
Glücklicherweise haben sie Aufzeichnungen über die Einheimischen geführt, die sie ihm zeigen können. Auf diesen kann mit den Sprüchen  `equals`, `hashCode` und `toString` gearbeitet werden.*

>*Sie erinnern sich, einmal beim Essen im Speisesaal den hohen Magiern bei einer Unterhaltung gelauscht zu haben, die über den Spruch `hashCodes` und seine Komplexität  sprachen. Dabei war die Rede davon, dass Ja'va eigene Hilfszauber beherberge, die einem das Zaubern erleichtern könnten. So etwa der Spruch `Objects.hash`.*

### Formalia
- Implementieren sie die Klasse `Person` im Package `oop`.
- Diese Klasse soll die Attribute `name`, `vorname` und `alter` haben.
- Überschreiben sie die Methoden `equals`, `hashCode` und `toString`.

## Aufgabe 2
*Die Einwohner von Ja'va neigen dazu, sich häufig zu vergleichen. Um dies festzuhalten haben sie ihre Aufzeichnungen so gestaltet, dass die Einträge untereinander vergleichbar sind.*

### Formalia
Machen sie ihre Klasse zusätzlich [Comparable](https://docs.oracle.com/javase/9/docs/api/java/lang/Comparable.html).

## Aufgabe 3
*Fasziniert von Ihrer Erzählung bittet der Novize Sie, ihm auch noch etwas von den dortigen Novizen und Großmeistern zu erzählen. Sie haben in ihren Aufzeichnungen die entsprechenden Einträge jeweilse um eine Matrikelnummer bzw. einen Titel erweitert.*

### Formalia
Schreiben sie die beiden Klassen `Student` mit dem Attribut `int matrikelnummer` und `Professor` mit dem Attribut `String titel`, die von `Person` abgeleitet sind.

## Aufgabe 4
*Der Novize entdeckt in ihren Notizen an einigen Stellen das Schlüsselwort `final`. Er erinnert sich grob daran, schon einmal davon gelesen zu haben, aber kann es spontan nicht zuordnen. Helfen sie ihm auf die Sprünge, was dieses Schlüsselwort bedeutet.*

```java

    public final class Foo {}

```

## Aufgabe 5
*Im zweiten Beispiel handelt es sich um den `final`-Modifizierer vor einer Methode.*

*Was bewirkt der `final`-Modifizierer vor Methoden?*

```java

    public final void foo();

```

## Aufgabe 6
*Nun sind auch Sie ins Schwärmen geraten und erzählen ihm ohne weitere Aufforderung, wozu man den `static`-Modifizierer bei __geschachtelten__ Klassen verwendet und welchen Aufwand man betreiben muss, um ein Objekt aus einer geschachtelten Klasse zu erzeugen, die nicht mit `static` gekennzeichnet ist.*

```java

	  public class Foo {
	    static class Bar {
	    }
	  }

```

## Aufgabe 7
*Als Sie mit Ihrem Exkurs fertig sind, fragt er Sie noch zu einem weiteren Phänomen, das er während seiner Recherche entdeckt hat.
Für den Sinn von '...' in folgendem Beispiel konnte er keine gute Erklärung finden.*

```java

  	List<A> toList(A... args);

```

>Werfen Sie einen Blick in die [ObservableList](https://docs.oracle.com/javase/9/docs/api/javafx/collections/ObservableList.html), um einige Beispiele hierfür zu finden.

## Aufgabe 8
*Um ihm auch die Schattenseiten des Objektorients zu offenbaren, zeigen Sie ihm zum Abschluss noch das Problem der default Sichtbarkeit von Variablen in Ja'va anhand des folgenden Beispiels.*

```java

    public class X {
      int x;
    }

```
