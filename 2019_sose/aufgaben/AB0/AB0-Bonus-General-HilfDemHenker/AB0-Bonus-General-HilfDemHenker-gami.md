# Hilf dem Henker

## Einleitung

*Henker Hannibal, Sohn einer langen Scharfrichterdynastie, vollführt seinen Beruf in einer ganz besonderen Art und Weise. Da das Henkerdasein sehr eintönig sein kann, gibt er jedem Sträfling die Chance, gegen ihn in einem Ratespiel anzutreten. Dafür überlegt er sich für jeden Übeltäter ein Wort, welches dieser aber nicht weiß. Nur durch Raten der Buchstaben können die dem Tode Geweihten versuchen, das Wort herauszufinden. Gelingt ihnen dies mit einer vorgegebenen Anzahl an Fehlversuchen, lässt Hannibal sie laufen.*

## Aufgabe 1: Formel gegen Fieslinge

*"Das kann man doch gar nicht verlieren!", ruft ein Verbrecher selbstsicher. Er glaubt, dass es einen Trick gibt, das Spiel des Henkers immer zu gewinnen und versucht mit Hannibal zu verhandeln. Dieser erkennt die List des Gauners und beauftragt einen Zauberer, eine magische Formel für ihn zu entwickeln, mit der er vor jedem Spiel überprüfen kann, ob es möglich ist, zu verlieren.*

### Formalia
 - Implementieren Sie eine Klasse 'Hangman' im package 'hilfdemhenker' und folgende Methoden:

```java
  Hangman(String wordToGuess, int maxLines) { ... }

  public String getWord();

  public boolean isPossibleToLose();
```
Hierbei ist die Methode `getWord()` ein Getter für die entsprechenden Instanzvariablen. Die Methode `isPossibleToLose()` soll `true` zurückgeben, wenn ein Spiel verloren werden kann.

#### Beispiel:
* Zu erratenes Wort: "algomantik"
* Maximale Anzahl an Strichen: 10

Antwort: Dieses Spiel kann verloren werden.

#### Tipp:
Suchen Sie zunächst Beispiele, bei denen ein Spiel nicht verloren werden kann, formulieren Sie dazu eine Regel und versuchen Sie danach, diese in Java umzusetzen.

#### Anmerkungen:
* Achten Sie auf passende Datentypen, sofern diese nicht schon angegeben sind.
* Alle Instanzvariablen und zusätzlichen Methoden sollten auf `private` gesetzt werden.
* Wir nehmen an, dass das Wort nicht leer ist.
* Der Anfangsbuchstabe des gesuchten Wortes ist in unserer Variante nicht bekannt.
* Wir gehen davon aus, dass ein Buchstabe nicht doppelt geraten wird.
* Das Wort gilt erst als erraten, wenn alle Buchstaben erraten wurden.

## Aufgabe 2: Zufälliger Buchstabenzauber

*Die Schurken merken, dass es nicht leicht ist, Hannibal zu überlisten und überlegen sich Strategien, wie sie am besten gegen ihn gewinnen können. Ein Zauberer wird zum Tode verurteilt, da er im Glückspiel betrogen hat. Er hat einen Zauberspruch entwickelt, der es ihm ermöglicht, die Buchstaben zufällig zu raten. Das Spiel startet und bei jedem Rateversuch schwingt der Zauberer seinen Zauberstab und spricht den Zauberspruch, welcher ihm zufällig einen Buchstaben aus dem Alphabet offenbart. Ob dieser zufällige Zauber die beste Strategie ist, das Ratespiel zu gewinnen?*

### Formalia

Erweitern Sie dazu die in Aufgabe 1 erstellte Klasse `Hangman` um folgende Methoden:

```java
  int getLineCount();
  char playRandomly();
  boolean isWin();
```

Hierbei ist die Methode `getLineCount()` ein Getter, der die Anzahl der aktuellen Striche zurückgibt. Die Methode `playRandomly()` liefert bei jedem Aufruf einen Buchstaben, der zufällig aus dem Alphabet ausgewählt wird.
Zudem soll sie den internen Zustand der Klasse so verändern, dass mit der Methode `isWin()` dann überprüft werden kann, ob das Spiel gewonnen wurde oder nicht.

#### Anmerkungen:
* Buchstaben dürfen jetzt auch doppelt genannt werden.
* Es wird kein Unterschied zwischen Groß- und Kleinbuchstaben gemacht.
* Achten Sie darauf, dass das Striche-Limit nicht überschritten wird.

## Aufgabe 3: Weise Buchstabenwahl

*Hannibal hat viel zu tun, denn es kommt ein Gauner nach dem anderen, über den gerichtet werden soll. Diese werden jedoch immer raffinierter. Der Nächste hat ein Buch von einem Weisen gestohlen. Studien über die Sprache der Menschen, welche über Jahre hinweg geführt wurden, sind in dem Buch niedergeschrieben. Es enthält Wissen darüber, welche Buchstaben am häufigsten verwendet werden. Der Dieb hofft, mit dieser Hilfe strategisch gegen den Henker zu gewinnen.*
### Formalia
Ergänzen Sie die in Aufgabe 2 erweiterte Klasse `Hangman` um folgende Methoden:

```java
  char playStrategically();
```

Die Methode `playStrategically()` funktioniert entsprechend wie `playRandomly()`, nur dass Buchstaben strategisch geraten werden.

#### Bemerkung:
Die Methode `isWin()` sollte ein richtiges Ergebnis liefern, egal ob zufällig oder strategisch gespielt wird.

[Abgabe 1-3 in Dozentron, Link hier einfügen]()

## Aufgabe 4: Hannibal braucht deine Hilfe

*Der erfahrene Scharfrichter merkt, dass die Verbrecher einfallsreicher werden und dass es nicht mehr so einfach wird, schwierige Wörter zu finden.*
- *Was sind die besten Wörter, um gegen den zufälligen Buchstabenzauber anzukommen?*
- *Wie macht man eine weise Buchstabenwahl unnütz?*

[Freitextfeld in Gildamesh, Link hier einfügen]()
