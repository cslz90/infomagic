# Aufgabenblatt 0
## Pflichtaufgaben

|Nr | Thema                         | Name                             | Typ      | Dauer in min   | XP  | Ref |
|---| ---                           | ---                              | ---      | ---            | --- | --- |
|01 | ArrayList, Generics           | ArrayBasilist                    | Unittest | 45             | 5   | [Link](./../../../2019_ss/aufgaben/AB0/AB0-Generics-ArrayBasilist/AB0-Generics-ArrayBasilist-klar.md) |
|02 | LinkedList                    | ImmutableList                    | Unittest | 30             | 3   | [Link](./../../../2019_ss/aufgaben/AB0/AB0-LinkedList-ImmutableList/AB0-LinkedList-LinkedBasilist-klar.md) |
|03 | Recherche                     | WildeStammbaumwürmer             | Text     | ?              | ?   | [Link](./../../../2019_ss/aufgaben/AB0/AB0-Recherche-WildeStammbaumwürmer/AB0-Recherche-WildeStammbaumwürmer-klar.md) |
|04 | ASCII                         | KomischeZauberformel             | IO       | 45             | 5   | [Link](./../../../2019_ss/aufgaben/AB0/AB0-StringsASCII-KomischeZauberformel/AB0-StringsASCII-KomischeZauberformel-klar.md) |

## Bonufaufgaben

| Thema                         | Name                             | Typ      | Dauer in min   | XP  | Ref |
| ---                           | ---                              | -------- | ---            | --- | --- |
| Rekursion                     | Zauberkiste                      | Text     | 20             | 4   | [Link](./../../../2019_ss/aufgaben/AB0/AB0-Rekursion-Zauberkiste/AB0-Rekursion-Zauberkiste-klar.md) |
| Maps                          | OrkGrunzer                       | Unittest | 180            | 45  | [Link](./../../../2019_ss/aufgaben/AB0/AB0-Bonus-Collections-OrkGrunzer/AB0-Collections-OrkGrunzer-klar.md) |
| OOP                           | OOP                              | Unittest | 120            | 30  | [Link](./../../../2019_ss/aufgaben/AB0/AB0-Bonus-General-OOP/AB0-General-ObjektOrient-klar.md) |
| Allgemein                     | HilfDemHenker                    | Unittest | 240            | 60  | [Link](./../../../2019_ss/aufgaben/AB0/AB0-Bonus-General-HilfDemHenker/AB0-Bonus-General-HilfDemHenker-klar.md) |
| Rekursion                     | BonsaiHydrachen                  | Text     | 15             | 3   | [Link](./../../../2019_ss/aufgaben/AB0/AB0-Bonus-Rekursion-BonsaiHydrachen/AB0-Bonus-Rekursion-BonsaiHydrachen-klar.md) |

## Bonusaufgaben (unfertig)

| Thema                         | Name                             | Typ      | Dauer in min   | XP  | Ref |
| ---                           | -------------------------------- | -------- | -------------- |--- | ----|
| File-IO                       | VerteidigungGegenMagischeWesen   | Unittest | 25             | 3   | [Link](./../../../2019_ss/aufgaben/AB0/AB0-FileIO-VerteidigungGegenMagischeWesen/AB0-FileIO-VerteidigungGegenMagischeWesen-klar.md)|
| Collections                   | BasilistenDiät                   | Unittest | 20             | 5   | [Link](./../../../2019_ss/aufgaben/AB0/AB0-Bonus-Collections-BasilistenDiät/AB0-Bonus-Collections-BasilistenDiät-klar.md) |
| Generics                      | BeutelUndBasilisten              | Unittest | 40             | 10  | [Link](./../../../2019_ss/aufgaben/AB0/AB0-Bonus-Generics-BeutelUndBasilisten/AB0-Bonus-Generics-BeutelUndBasilisten-klar.md) |
| File-IO, Regex                | MagicalDryCounter                | Unittest | 45             | 11  | [Link](./../../../2019_ss/aufgaben/AB0/AB0-Bonus-MagicalDRYCounter/AB0-Bonus-MagicalDRYCounter-klar.md) |
| Allgemein                     | MagicalPot                       | Unittest | 15             | 3   | [Link](./../../../2019_ss/aufgaben/AB0/AB0-Bonus-General-MagicalPot/AB0-Bonus-General-MagicalPot-klar.md) |
