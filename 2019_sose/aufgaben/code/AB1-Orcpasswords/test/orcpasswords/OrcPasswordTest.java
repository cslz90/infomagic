package orcpasswords;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import de.thm.mni.aud.commons.CodeAnalyser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import sourcecodetester.SourceCodeTester;
import sourcecodetester.tests.*;
import sourcecodetester.util.Method;

public class OrcPasswordTest extends CodeAnalyser {

  private ArrayList<String> passwords;

  @Before
  public void init() {
    passwords = OrcPasswords.passwords();
  }

  @After
  public void cleanUp() {
    passwords.clear();
    passwords = null;
  }

  @Test
  public void thereAre7454981PasswordsTest() {
    assertEquals("There should be 7454981 passwords in the generated list!", 7454981, passwords.size());
  }

  @Test
  public void passwordsAreUniqueTest() {
    List<String> passwords = new ArrayList<>(this.passwords);
    Collections.sort(passwords);

    String last = null;
    for (final String password : passwords) {
      if (last == null) {
        last = password;
      } else if (last.equals(password)) {
        fail(String.format("Each password should exist exactly once in the generated list! Password '%s' is a duplicate!", password));
      } else {
        last = password;
      }
    }
  }

  @Test
  public void passwordsMatchExpectedShapeTest() {
    final Pattern pattern = Pattern.compile("grunz[a-zA-Z]{0,4}");

    for (final String password : passwords) {
      if (!pattern.matcher(password).matches()) {
        fail(String.format("Password '%s' doesn't match expected shape!", password));
      }
    }
  }

  @Override
  public void setupTests(SourceCodeTester sct) {

    // CLASS AND METHOD IMPLEMENTATIONS
    sct.createNewTestGroup("Class and Method Implementations", "The following tests are checking for correct class and method implementations.")
      .addTests(
        new ClassImplementations(
          "orcpasswords.OrcPasswords"
        ),
        new MethodImplementations(true,
          new Method("orcpasswords.OrcPasswords.passwords()")
            .requireVisibilityPublic()
            .requireModifierStatic()
            .requireReturnType("ArrayList<String>")
            .requireParametersNone()
        )
      )
    ;

    // CODE QUALITY
    sct.createNewTestGroup("Code Quality", "The following tests are checking the overall quality of your source code.")
      .addTests(
        new ClassNamesStartWithCapitalLetter(),
        new MethodNamesStartWithLowerCaseLetter(),
        new VariableNamesStartWithLowerCaseLetter(),
        new ClosingBraceInOwnLine(),
        new OpeningBraceInSameLineWithStatement(),
        //new LineIndentation(8),
//				new LimitStatementAmount(10, false, new Method("package.Class.method()")),
        new LimitCharactersPerLine(150)
      )
    ;

  }

  @Test
  public void implementationsTest() {
    runTests("Class and Method Implementations");
  }

  @Test
  public void codeQualityTest() {
    runTests("Code Quality");
  }

}
