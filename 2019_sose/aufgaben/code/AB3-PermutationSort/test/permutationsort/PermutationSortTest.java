package permutationsort;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.*;

import de.thm.mni.aud.commons.CodeAnalyser;
import org.junit.Test;
import sourcecodetester.SourceCodeTester;
import sourcecodetester.tests.*;
import sourcecodetester.util.Method;

public class PermutationSortTest extends CodeAnalyser {

    public static final int MIN_VALUE = -1000;
    public static final int MAX_VALUE = 1000;

    public static final int LONG_LIST_SIZE = 10;

    @Test
    public void listSize0Test() {
        testRandomList(0);
    }

    @Test
    public void listSize1Test() {
        testRandomList(1);
    }

    @Test
    public void listLongTest() {
        testRandomList(LONG_LIST_SIZE);
    }

    @Test
    public void testPermutations() {
      List<Integer> lst = new ArrayList<>();
      new Random().ints(6, MIN_VALUE, MAX_VALUE).forEach(lst::add);
      List<List<Integer>> perms = PermutationSort.permutations(lst);
      Set<List<Integer>> distinct = new HashSet<>(perms);
      assertEquals(distinct.size(), perms.size());
    }

    private void testRandomList(int size) {
        List<Integer> lst = new ArrayList<>();
        new Random().ints(size, MIN_VALUE, MAX_VALUE).forEach(i -> lst.add(i));

        List<Integer> correctList = new ArrayList<>(lst);
        Collections.sort(correctList);

        PermutationSort.permutationSort(lst);

        assertTrue("A list of size " + size + " was sorted incorrect", lst.equals(correctList));
    }

    @Override
    public void setupTests(SourceCodeTester sct) {

      // CLASS AND METHOD IMPLEMENTATIONS
      sct.createNewTestGroup("Class and Method Implementations", "The following tests are checking for correct class and method implementations.")
        .addTests(
          new ClassImplementations(
            "permutationsort.PermutationSort"
          ),
          new MethodImplementations(true,
            new Method("permutationsort.PermutationSort.permutations()")
              .requireVisibilityPublic()
              .requireModifierStatic()
              .requireReturnType("List<List<Integer>>")
              .requireParameters("List<Integer>"),
            new Method("permutationsort.PermutationSort.permutationSort()")
              .requireVisibilityPublic()
              .requireModifierStatic()
              .requireReturnType("void")
              .requireParameters("List<Integer>")
          )
        )
      ;

      // CODE QUALITY
      sct.createNewTestGroup("Code Quality", "The following tests are checking the overall quality of your source code.")
        .addTests(
          new ClassNamesStartWithCapitalLetter(),
          new MethodNamesStartWithLowerCaseLetter(),
          new VariableNamesStartWithLowerCaseLetter(),
          new ClosingBraceInOwnLine(),
          new OpeningBraceInSameLineWithStatement(),
  //				new LimitStatementAmount(10, false, new Method("package.Class.method()")),
          new LimitCharactersPerLine(150)
        )
      ;

      sct.createNewTestGroup("Limitations", "The following tests limit the usage of specific classes and method calls.")
			  .addTests(
				new BlacklistMethods(
					new Method("java.util.Arrays.sort()"),
          new Method("java.util.Collections.sort()")
				),
        new MethodCallUsedInMethods(
          new Method("permutationsort.PermutationSort.permutations()"),
          new Method("permutationsort.PermutationSort.permutationSort()")
        )
			)
		;
    }

    @Test
    public void implementationsTest() {
      runTests("Class and Method Implementations");
    }

    @Test
    public void codeQualityTest() {
      runTests("Code Quality");
    }

  	@Test
    public void limitationsTest() {
      runTests("Limitations");
    }
}
