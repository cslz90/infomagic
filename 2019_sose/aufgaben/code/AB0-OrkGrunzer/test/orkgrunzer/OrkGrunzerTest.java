package orkgrunzer;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class OrkGrunzerTest {


  @Test
  public void encodeTest() {
    final String TEST_INPUT1 = "The quick brown fox jumps over the lazy dog 0987654321-.,";
    final String TEST_OUTPUT1 = "-..... --.-..-..-.-.-.- -....-.---.---. ..-.----..- .---..---.--.... ---...-..-. -..... .-...---..-.-- -..-----. ---------.---..--...-.............-...--..---.-----....-.-.-.---..--";
    assertEquals(TEST_OUTPUT1, OrkGrunzer.encode(TEST_INPUT1));
  }

  @Test
  public void encodeFailTest() {
    final String TEST_INPUT2 = "The quick brown fox jumps over the lazy dog 0987654321-.,";
    final String TEST_OUTPUT2 = "-..... --.-..-.-.-.-.- -....-.---.---. ..-.----..- .---..---.--.... ---...-..-. -..... .-...---..-.-- -..-----. ---------.---..--...-.............-...--..---.-----....-.-.-.---..--";
    assertNotEquals(TEST_OUTPUT2, OrkGrunzer.encode(TEST_INPUT2));
  }

  @Test
  public void emptyTest() {
    assertEquals("", OrkGrunzer.encode(""));
  }
  
}
