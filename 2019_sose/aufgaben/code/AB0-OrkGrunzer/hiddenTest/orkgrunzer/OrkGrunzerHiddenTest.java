package orkgrunzer;

import de.thm.mni.aud.commons.CodeAnalyser;
import org.junit.Test;
import sourcecodetester.SourceCodeTester;
import sourcecodetester.tests.*;
import sourcecodetester.util.Method;

public class OrkGrunzerHiddenTest extends CodeAnalyser {

  @Override
  public void setupTests(SourceCodeTester sct) {
    sct.createNewTestGroup("Class and Method Implementations", "The following tests are checking for correct class and method implementations.")
      .addTests(
        new ClassImplementations(
          "orkgrunzer.OrkGrunzer"
        ),
        new MethodImplementations(
          new Method("orkgrunzer.OrkGrunzer.encode()")
            .requireVisibilityPublic()
            .requireModifierStatic()
            .requireReturnType("String")
            .requireParameters("String")
        ),
        new MethodCallUsedInMethods(
          new Method("orkgrunzer.OrkGrunzer.getOrcDictionary"),
          new Method("orkgrunzer.OrkGrunzer.encode()")
        )
      )
    ;
    // CODE QUALITY
    sct.createNewTestGroup("Code Quality", "The following tests are checking the overall quality of your source code.")
      .addTests(
        new ClassNamesStartWithCapitalLetter(),
        new MethodNamesStartWithLowerCaseLetter(),
        new VariableNamesStartWithLowerCaseLetter(),
        new ClosingBraceInOwnLine(),
        new OpeningBraceInSameLineWithStatement(),
        new LimitCharactersPerLine(150)
      )
    ;
  }

  @Test
  public void implementationsTest() {
    runTests("Class and Method Implementations");
  }

  @Test
  public void codeQualityTest() {
    runTests("Code Quality");
  }
  
}
