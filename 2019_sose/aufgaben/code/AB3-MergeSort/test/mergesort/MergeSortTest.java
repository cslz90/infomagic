package mergesort;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Random;

import de.thm.mni.aud.commons.CodeAnalyser;
import org.junit.Test;
import sourcecodetester.SourceCodeTester;
import sourcecodetester.tests.*;
import sourcecodetester.util.Method;

public class MergeSortTest extends CodeAnalyser {

  private static final int MIN_VALUE = -1000;
  private static final int MAX_VALUE = 1000;

  private static final int MEDIUM_ARRAY_LENGTH = 500 + (new Random()).nextInt(1000);
  private static final int LONG_ARRAY_LENGTH = 10000 + (new Random()).nextInt(10000);

  @Test
  public void arrayLength0Test() {
    testRandomArray(0);
  }

  @Test
  public void arrayLength1Test() {
    testRandomArray(1);
  }

  @Test
  public void arrayMediumTest() {
    testRandomArray(MEDIUM_ARRAY_LENGTH);
  }

  @Test
  public void arrayLongTest() {
    testRandomArray(LONG_ARRAY_LENGTH);
  }

  @Test
  public void mergeTest() {
    int[] array = new Random().ints(LONG_ARRAY_LENGTH, MIN_VALUE, MAX_VALUE).toArray();

    int fromIndex = array.length / 2;
    int toIndex = array.length;
    int middleIndex = (fromIndex + toIndex) / 2;

    int[] initialArray = Arrays.copyOf(array, array.length);
    int[] correctArray = Arrays.copyOf(array, array.length);
    Arrays.sort(correctArray, fromIndex, toIndex);
    Arrays.sort(array, fromIndex, middleIndex);
    Arrays.sort(array, middleIndex, toIndex);

    MergeSort.merge(array, fromIndex, middleIndex, toIndex);
    if (Arrays.equals(initialArray, array) && !Arrays.equals(initialArray, correctArray)) {
      fail("you did not change the array at all!");
    }

    //assertTrue("Merge function works incorrect", Arrays.equals(correctArray, array));
    assertArrayEquals("Merge function works incorrect", correctArray, array);
  }


  private void testRandomArray(int length) {
    int[] array = new Random().ints(length, MIN_VALUE, MAX_VALUE).toArray();

    int[] correctArray = Arrays.copyOf(array, length);
    Arrays.sort(correctArray);

    MergeSort.sort(array);

    assertArrayEquals("An array of size " + length + " was sorted incorrect", correctArray, array);
  }

  @Override
  public void setupTests(SourceCodeTester sct) {

    // CLASS AND METHOD IMPLEMENTATIONS
    sct.createNewTestGroup("Class and Method Implementations", "The following tests are checking for correct class and method implementations.")
      .addTests(
        new ClassImplementations(
          "mergesort.MergeSort"
        ),
        new MethodImplementations(true,
          new Method("mergesort.MergeSort.sort()")
            .requireVisibilityPublic()
            .requireModifierStatic()
            .requireReturnType("void")
            .requireParameters("int[]"),
          new Method("mergesort.MergeSort.merge()")
            .requireVisibilityPublic()
            .requireModifierStatic()
            .requireReturnType("void")
            .requireParameters("int[]", "int", "int", "int")
        )
      )
    ;

		sct.createNewTestGroup("Functionality", "The following tests are checking specific code functionalities.")
			.addTests(
				new MethodRecursiveImplementations(
					new Method("mergesort.MergeSort.sort()")
				),
					new MethodCallUsedInMethods(
					new Method("mergesort.MergeSort.merge()"),
					new Method("mergesort.MergeSort.sort()")
				)
			)
		;

    sct.createNewTestGroup("Code Quality", "The following tests are checking the overall quality of your source code.")
      .addTests(
        new ClassNamesStartWithCapitalLetter(),
        new MethodNamesStartWithLowerCaseLetter(),
        new VariableNamesStartWithLowerCaseLetter(),
        new ClosingBraceInOwnLine(),
        new OpeningBraceInSameLineWithStatement(),
        //new LineIndentation(8),
//				new LimitStatementAmount(10, false, new Method("package.Class.method()")),
        new LimitCharactersPerLine(150)
      )
    ;


		// LIMITATIONS
		sct.createNewTestGroup("Limitations", "The following tests limit the usage of specific classes and method calls.")
			.addTests(
        new BlacklistMethods(
          new Method("java.util.Arrays.sort()"),
          new Method("java.util.Collections.sort()")
        ),
        new MethodCallUsedInMethods(
          new Method("mergesort.MergeSort.merge()"),
          new Method("mergesort.MergeSort.sort()")
        )
			)
		;

  }

  @Test
  public void implementationsTest() {
    runTests("Class and Method Implementations");
  }

  @Test
  public void codeQualityTest() {
    runTests("Code Quality");
  }

	@Test
	public void limitationsTest() {
		runTests("Limitations");
	}

	@Test
	public void functionalityTest() {
		runTests("Functionality");
	}


}
