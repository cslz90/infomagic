package dijkstra;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Test;
import static org.junit.Assert.*;

public class DijkstraTest {

	@Test
	public void priorityQueueUpdateTest() {
		List<FlyingIsland> islands = IntStream.range(0, 4).mapToObj(FlyingIsland::new).collect(Collectors.toList());
		List<Bridge> bridges = List.of(
		        new Bridge(islands.get(0), islands.get(1), 1),
                new Bridge(islands.get(0), islands.get(2), 4),
                new Bridge(islands.get(0), islands.get(3), 5),
                new Bridge(islands.get(1), islands.get(3), 1),
                new Bridge(islands.get(3), islands.get(2), 1)
		);
		List<FlyingIsland> path0 = List.of(islands.get(0));
		List<FlyingIsland> path1 = List.of(islands.get(0), islands.get(1));
		List<FlyingIsland> path2 = List.of(islands.get(0), islands.get(1), islands.get(3), islands.get(2));
		List<FlyingIsland> path3 = List.of(islands.get(0), islands.get(1), islands.get(3));
		Dijkstra d = new Dijkstra(new Highlands(islands, bridges));
		d.execute(islands.get(0));
		assertEquals(path0, d.getShortestPath(islands.get(0), islands.get(0)));
        assertEquals(path1, d.getShortestPath(islands.get(0), islands.get(1)));
        assertEquals(path2, d.getShortestPath(islands.get(0), islands.get(2)));
        assertEquals(path3, d.getShortestPath(islands.get(0), islands.get(3)));
    }

	@Test
	public void smallHighlandsTest() {
		List<FlyingIsland> islands = new ArrayList<>();
		List<Bridge> bridges = new ArrayList<>();
		Highlands highlands;
		List<FlyingIsland> correctPath = new ArrayList<>();
		List<FlyingIsland> testedPath;
		
		for(int i = 0; i < 4; i++) islands.add(new FlyingIsland(i));
		
		bridges.add(new Bridge(islands.get(0), islands.get(1), 10));
		bridges.add(new Bridge(islands.get(1), islands.get(0), 10));
		bridges.add(new Bridge(islands.get(0), islands.get(2), 5));
		bridges.add(new Bridge(islands.get(2), islands.get(0), 5));
		bridges.add(new Bridge(islands.get(1), islands.get(3), 5));
		bridges.add(new Bridge(islands.get(3), islands.get(1), 5));
		bridges.add(new Bridge(islands.get(2), islands.get(3), 5));
		bridges.add(new Bridge(islands.get(3), islands.get(2), 5));
		
		highlands = new Highlands(islands, bridges);
		
		correctPath.add(islands.get(0));
		correctPath.add(islands.get(2));
		correctPath.add(islands.get(3));
		
		Dijkstra dijkstra = new Dijkstra(highlands);
		dijkstra.execute(islands.get(0));
		testedPath = dijkstra.getShortestPath(islands.get(0), islands.get(3));

		assertEquals("You haven't found the shortest path yet! Your path doesn't include the right number of islands!", correctPath.size(), testedPath.size());
		
		if(correctPath.size() == testedPath.size()) 
			for(int i = 0; i < correctPath.size(); i++) {
				assertEquals("You haven't found the shortest path yet! Island Nr. " + i + " is incorrect!", correctPath.get(i), testedPath.get(i));
			}
	}
	
	@Test
	public void mediumHighlandsTest() {
		List<FlyingIsland> islands = new ArrayList<>();
		List<Bridge> bridges = new ArrayList<>();
		Highlands highlands;
		List<FlyingIsland> correctPath = new ArrayList<>();
		List<FlyingIsland> testedPath;

		for (int i = 0; i < 7; i++) islands.add(new FlyingIsland(i));

		bridges.add(new Bridge(islands.get(0), islands.get(1), 10));
		bridges.add(new Bridge(islands.get(1), islands.get(0), 10));
		bridges.add(new Bridge(islands.get(0), islands.get(2), 5));
		bridges.add(new Bridge(islands.get(2), islands.get(0), 5));
		bridges.add(new Bridge(islands.get(1), islands.get(3), 4));
		bridges.add(new Bridge(islands.get(3), islands.get(1), 4));
		bridges.add(new Bridge(islands.get(1), islands.get(4), 19));
		bridges.add(new Bridge(islands.get(4), islands.get(1), 19));
		bridges.add(new Bridge(islands.get(3), islands.get(4), 8));
		bridges.add(new Bridge(islands.get(4), islands.get(3), 8));
		bridges.add(new Bridge(islands.get(3), islands.get(6), 13));
		bridges.add(new Bridge(islands.get(6), islands.get(3), 13));
		bridges.add(new Bridge(islands.get(4), islands.get(6), 4));
		bridges.add(new Bridge(islands.get(6), islands.get(4), 4));
		bridges.add(new Bridge(islands.get(2), islands.get(5), 20));
		bridges.add(new Bridge(islands.get(5), islands.get(2), 20));
		bridges.add(new Bridge(islands.get(5), islands.get(6), 3));
		bridges.add(new Bridge(islands.get(6), islands.get(5), 3));

		highlands = new Highlands(islands, bridges);

		correctPath.add(islands.get(0));
		correctPath.add(islands.get(1));
		correctPath.add(islands.get(3));
		correctPath.add(islands.get(4));
		correctPath.add(islands.get(6));

		Dijkstra dijkstra = new Dijkstra(highlands);
		dijkstra.execute(islands.get(0));
		testedPath = dijkstra.getShortestPath(islands.get(0), islands.get(6));

		assertEquals("You haven't found the shortest path yet! Your path doesn't include the right number of islands!", correctPath.size(), testedPath.size());

		if (correctPath.size() == testedPath.size())
			for (int i = 0; i < correctPath.size(); i++) {
				assertEquals("You haven't found the shortest path yet! Island Nr. " + i + " is incorrect!", correctPath.get(i), testedPath.get(i));
			}
	}

	@Test
	public void bigHighlandsTest() {
		List<FlyingIsland> islands = new ArrayList<>();
		List<Bridge> bridges = new ArrayList<>();
		Highlands highlands;
		List<FlyingIsland> correctPath = new ArrayList<>();
		List<FlyingIsland> testedPath;

		for (int i = 0; i < 13; i++) islands.add(new FlyingIsland(i));

		bridges.add(new Bridge(islands.get(0), islands.get(1), 2));
		bridges.add(new Bridge(islands.get(0), islands.get(9), 1));
		bridges.add(new Bridge(islands.get(0), islands.get(3), 11));
		bridges.add(new Bridge(islands.get(1), islands.get(0), 2));
		bridges.add(new Bridge(islands.get(1), islands.get(4), 4));
		bridges.add(new Bridge(islands.get(1), islands.get(2), 1));
		bridges.add(new Bridge(islands.get(2), islands.get(1), 1));
		bridges.add(new Bridge(islands.get(2), islands.get(10), 10));
		bridges.add(new Bridge(islands.get(2), islands.get(5), 5));
		bridges.add(new Bridge(islands.get(3), islands.get(0), 11));
		bridges.add(new Bridge(islands.get(3), islands.get(4), 7));
		bridges.add(new Bridge(islands.get(3), islands.get(6), 2));
		bridges.add(new Bridge(islands.get(4), islands.get(9), 1));
		bridges.add(new Bridge(islands.get(4), islands.get(1), 4));
		bridges.add(new Bridge(islands.get(4), islands.get(10), 3));
		bridges.add(new Bridge(islands.get(4), islands.get(5), 12));
		bridges.add(new Bridge(islands.get(4), islands.get(12), 8));
		bridges.add(new Bridge(islands.get(4), islands.get(7), 13));
		bridges.add(new Bridge(islands.get(4), islands.get(11), 9));
		bridges.add(new Bridge(islands.get(4), islands.get(3), 7));
		bridges.add(new Bridge(islands.get(5), islands.get(2), 5));
		bridges.add(new Bridge(islands.get(5), islands.get(4), 12));
		bridges.add(new Bridge(islands.get(5), islands.get(8), 20));
		bridges.add(new Bridge(islands.get(6), islands.get(3), 2));
		bridges.add(new Bridge(islands.get(6), islands.get(11), 1));
		bridges.add(new Bridge(islands.get(6), islands.get(7), 2));
		bridges.add(new Bridge(islands.get(7), islands.get(6), 2));
		bridges.add(new Bridge(islands.get(7), islands.get(4), 13));
		bridges.add(new Bridge(islands.get(7), islands.get(8), 3));
		bridges.add(new Bridge(islands.get(8), islands.get(7), 3));
		bridges.add(new Bridge(islands.get(8), islands.get(12), 10));
		bridges.add(new Bridge(islands.get(8), islands.get(5), 20));
		bridges.add(new Bridge(islands.get(9), islands.get(0), 1));
		bridges.add(new Bridge(islands.get(9), islands.get(4), 1));
		bridges.add(new Bridge(islands.get(10), islands.get(4), 3));
		bridges.add(new Bridge(islands.get(10), islands.get(2), 10));
		bridges.add(new Bridge(islands.get(11), islands.get(6), 1));
		bridges.add(new Bridge(islands.get(11), islands.get(4), 9));
		bridges.add(new Bridge(islands.get(12), islands.get(4), 8));
		bridges.add(new Bridge(islands.get(12), islands.get(8), 10));

		highlands = new Highlands(islands, bridges);

		correctPath.add(islands.get(0));
		correctPath.add(islands.get(9));
		correctPath.add(islands.get(4));
		correctPath.add(islands.get(3));
		correctPath.add(islands.get(6));
		correctPath.add(islands.get(7));
		correctPath.add(islands.get(8));

		Dijkstra dijkstra = new Dijkstra(highlands);
		dijkstra.execute(islands.get(0));
		testedPath = dijkstra.getShortestPath(islands.get(0), islands.get(8));

		assertEquals("You haven't found the shortest path yet! Your path doesn't include the right number of islands!", correctPath.size(), testedPath.size());

		if (correctPath.size() == testedPath.size())
			for (int i = 0; i < correctPath.size(); i++) {
				assertEquals("You haven't found the shortest path yet! Island Nr. " + i + " is incorrect!", correctPath.get(i), testedPath.get(i));
			}
	}

}
