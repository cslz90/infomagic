package dijkstra;
import java.util.List;

public class Highlands {

    private List<FlyingIsland> islands;
	  private List<Bridge> bridges;

    public Highlands(List<FlyingIsland> islands, List<Bridge> bridges) {
        this.islands = islands;
        this.bridges = bridges;
    }

    public List<FlyingIsland> getIslands() {
        return islands;
    }

    public List<Bridge> getBridges() {
        return bridges;
    }
}
