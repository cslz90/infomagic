package dijkstra;

public class Bridge {

    private final FlyingIsland src;
    private final FlyingIsland target;
    private final int distance;

    public Bridge(FlyingIsland src, FlyingIsland target, int distance) {
        this.src = src;
        this.target = target;
        this.distance = distance;
    }

    public FlyingIsland getSource() {
        return src;
    }

    public FlyingIsland getTarget() {
        return target;
    }

    public int getDistance() {
        return distance;
    }
}
