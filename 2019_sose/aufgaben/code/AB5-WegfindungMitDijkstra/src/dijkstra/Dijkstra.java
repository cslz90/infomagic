package dijkstra;
import java.util.*;

public class Dijkstra {

    private class DijkstraInfo {
        int distToStart;
        FlyingIsland predecessor;
        boolean visited;

        DijkstraInfo(int distToStart, FlyingIsland predecessor, boolean visited) {
            this.distToStart = distToStart;
            this.predecessor = predecessor;
            this.visited = visited;
        }
    }

    private List<FlyingIsland> islands;
    private List<Bridge> bridges;
    private Set<FlyingIsland> unvisitedIslands = new HashSet<>();
    private Map<FlyingIsland, DijkstraInfo> islandInfos = new HashMap<>();
    private Map<FlyingIsland, Integer> distances = new HashMap<>();

    Dijkstra(Highlands highlands) {
        islands = highlands.getIslands();
    	  bridges = highlands.getBridges();
    	  initInfo();
    }

    private void initInfo() {
        for(FlyingIsland node : islands) {
            islandInfos.put(node, new DijkstraInfo(Integer.MAX_VALUE, null, false));
        }
    }


    public void execute(FlyingIsland start) {
        distances.put(start, 0);
        unvisitedIslands.add(start);
        while (!unvisitedIslands.isEmpty()) {
            FlyingIsland current = getMinimum(unvisitedIslands);
            islandInfos.get(current).visited = true;
            unvisitedIslands.remove(current);
            for(Bridge edge : getBridgesFrom(current)) {
                if(islandInfos.get(edge.getTarget()).visited) continue;
                int oldDist = islandInfos.get(edge.getTarget()).distToStart;
                int newDist = islandInfos.get(current).distToStart + edge.getDistance();
                if(newDist < oldDist) {
                    distances.put(edge.getTarget(), newDist);
                    islandInfos.get(edge.getTarget()).distToStart = newDist;
                    islandInfos.get(edge.getTarget()).predecessor = current;
                    unvisitedIslands.add(edge.getTarget());
                }
            }
        }
    }

    private FlyingIsland getMinimum(Set<FlyingIsland> islands) {
        FlyingIsland minimum = null;
        for (FlyingIsland island : islands) {
            if (minimum == null) {
                minimum = island;
            }
            else if (getShortestDistance(island) < getShortestDistance(minimum)) {
                minimum = island;
            }
        }
        return minimum;
    }

    private int getShortestDistance(FlyingIsland target) {
        Integer d = distances.get(target);
        if (d == null) return Integer.MAX_VALUE;
        else return d;
    }

    private List<Bridge> getBridgesFrom(FlyingIsland node) {
        List<Bridge> adjacentBridges = new ArrayList<>();
        for (Bridge edge : bridges) {
            if (edge.getSource().equals(node)) {
                adjacentBridges.add(edge);
            }
        }
        return adjacentBridges;
    }

    public List<FlyingIsland> getShortestPath(FlyingIsland start, FlyingIsland target) {
        LinkedList<FlyingIsland> path = new LinkedList<>();
        path.addFirst(target);
        FlyingIsland current = target;
        while(current != start) {
            current = islandInfos.get(current).predecessor;
            path.addFirst(current);
        }
        return path;
    }

}
