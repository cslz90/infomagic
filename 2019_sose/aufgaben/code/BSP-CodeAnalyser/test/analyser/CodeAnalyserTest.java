package analyser;

import de.thm.mni.aud.commons.CodeAnalyser;
import org.junit.Test;
import sourcecodetester.SourceCodeTester;
import sourcecodetester.enums.Modifier;
import sourcecodetester.enums.Visibility;
import sourcecodetester.tests.*;
import sourcecodetester.util.Method;
import sourcecodetester.util.Parameter;

import java.util.EnumSet;

public class CodeAnalyserTest extends CodeAnalyser {
  
  @Override
  public void setupTests(SourceCodeTester sct) {
    // failing tests have been intentionally commented out so that these tests do not fail!
    
    sct.createNewTestGroup(
      "Class and Method Implementations",
      "The following tests check for correct implementation of classes and their methods."
    )
      .addTests(
        new ClassImplementations(
          "main.MyAwesomeClass",
          "main.Main",
          "myLowerCaseClassName"
        ),
        new InterfaceImplementations(
          "ExtendsTest"
        ),
        new MethodImplementations(
          new Method("main.myLowerCaseClassName.MyUpperCaseMethodName()")
            .requireVisibilityPublic()
            .requireReturnType("void")
            .requireParameters("int"),
          new Method("main.MyAwesomeClass.myRecursiveMethod()")
            .requireVisibilityPublic()
            .requireModifierStatic()
            .requireReturnType("int")
            .requireParameters("int"),
          new Method("main.MyAwesomeClass.myRecursiveMethod()")
            .requireVisibilityPublic()
            .requireModifierStatic()
            .requireReturnType("int")
            .requireParameters("int", "int"),
          new Method("main.MyAwesomeClass.test()")
            .requireVisibilityPublic()
            .requireReturnType("void")
            .requireParametersNone(),
          new Method("main.MyAwesomeClass.myNotRecursiveMethod()")
            .requireVisibilityPublic()
            .requireReturnType("void")
            .requireParametersNone(),
          new Method("main.MyAwesomeClass.randomMethod()")
            .requireVisibilityPublic()
            .requireReturnType("void")
            .requireParametersNone(),
          new Method("main.MyAwesomeClass.maxWrong()")
            .requireVisibilityPublic()
            .requireModifierStatic()
            .requireReturnType("E")
            .requireParameters("Collection<E>"),
          new Method("main.MyAwesomeClass.minWrong()")
            .requireVisibilityPublic()
            .requireModifierStatic()
            .requireReturnType("E")
            .requireParameters("Collection<E>"),
          new Method("main.MyAwesomeClass.min()")
            .requireVisibilityPublic()
            .requireModifierStatic()
            .requireReturnType("A")
            .requireParameters("Collection<? extends A>"),
          new Method("main.MyAwesomeClass.max()")
            .requireVisibilityPublic()
            .requireModifierStatic()
            .requireReturnType("A")
            .requireParameters("Collection<? extends A>"),
          new Method("main.MyAwesomeClass.compareTo()")
            .requireVisibilityPublic()
            .requireReturnType("int")
            .requireParameters("Integer"),
          new Method("main.MyAwesomeClass.RandomClass.compareTo()")
            .requireVisibilityPublic()
            .requireReturnType("int")
            .requireParameters("String"),
          new Method("main.Main.main()")
            .requireVisibilityPublic()
            .requireModifierStatic()
            .requireReturnType("void")
            .requireParameters("String[]")
        ),
        new MethodImplementations(
          new Method("main.Main.main()", Visibility.PUBLIC, "void", EnumSet.of(Modifier.STATIC), new Parameter("String[]", "args"))
        ),
        new MethodImplementations(
          new Method("MyAwesomeClass.myRecursiveMethod()", Visibility.PUBLIC, "int", EnumSet.of(Modifier.STATIC, Modifier.FINAL), new Parameter("int", "number")),
          new Method("MyAwesomeClass.myNotRecursiveMethod()"),
          new Method("MyAwesomeClass.randomMethod()", Visibility.ANY, null, EnumSet.of(Modifier.ANY), (Parameter[]) null), // equivalent similar to just checking the method name
          new Method("MyAwesomeClass.maxWrong()", Visibility.PUBLIC, "E", EnumSet.of(Modifier.STATIC), new Parameter("Collection<E>", "collection"))
        )/*,
        new ClassImplementations( // failing test
          "MyNotExistingClass"
          ),
        new ClassImplementations( // failing test
          "MyNotExistingClassOne",
          "MyNotExistingClassTwo"
        )*/
      );
    
    sct.createNewTestGroup(
      "Functionality",
      "The following tests check for correct functionality."
    )
      .addTests(
        /*new MethodRecursiveImplementations(new Method("MyAwesomeClass.myNotRecursiveMethod()")), // failing test
        new MethodRecursiveImplementations(new Method("MyAwesomeClass.myNotExistingRecursiveMethod()")), // failing test
        new MethodRecursiveImplementations(new Method("MyAwesomeClass.myNotRecursiveMethod()"), new Method("MyAwesomeClass.myNotExistingRecursiveMethod()")), // failing test
        new MethodRecursiveImplementations(new Method("MyAwesomeClass.myRecursiveMethod()", Visibility.PUBLIC, "int", EnumSet.of(Modifier.STATIC, Modifier.FINAL), new Parameter("int"), new Parameter("int"))), // failing test
        new MethodCallUsedInMethods(
            new Method("java.lang.Comparable.compareTo()", Visibility.PUBLIC, "int", EnumSet.of(Modifier.ANY), new Parameter("T")),
            new Method("MyAwesomeClass.min()"),
            new Method("MyAwesomeClass.max()")
          ),
        new MethodCallUsedInMethods( // failing test
          new Method("MyAwesomeClass.compareToFailing()"),
          new Method("min()"),
          new Method("max()"),
          new Method("median()")
          ),*/
        new MethodRecursiveImplementations(new Method("main.MyAwesomeClass.myRecursiveMethod()"))
      );

  	/*
  	sct.createNewTestGroup(
  			"Limitations",
  			"The following tests check the usage of specific classes and methods."
    )
  		.addTests(
        new BlacklistClasses(  // failing test
          java.util.Collections.class.getName(),
          java.util.ArrayList.class.getName()
        ),
        new WhitelistClasses(  // failing test
          java.util.Collection.class.getName(),
          java.util.Collections.class.getName()
        ),
        new BlacklistMethods(  // failing test
          new Method("java.lang.Math.random()"),
          new Method("java.lang.Math.max()"),
          new Method("java.lang.Math.min()"),
          new Method("java.util.Collections.max()"),
          new Method("java.util.Collections.min()")
        )
  		);
  		*/

  	/*
  	sct.createNewTestGroup(
  			"Java Documentation",
  			"The following tests check the Javadoc of methods."
  			)
  		.addTests(
			  new AllMethodsHaveJavadoc(), // failing test
        new PublicMethodsHaveJavadoc(false, false, false), // failing test
  			new MethodsHaveJavadoc( // failing test
  				new Method("myNotRecursiveMethod"),
  				new Method("main.MyAwesomeClass.myExceptionThrowingMethod", Visibility.PACKAGE, "void", new Parameter("boolean", "isTrue"))
  			)
  		);
    */
  	
  	/*
  	sct.createNewTestGroup(
  			"Code Quality",
  			"The following tests check the overall code quality."
    )
  		.addTests(
  			new ClassNamesStartWithCapitalLetter(), // failing test
  			new MethodNamesStartWithLowerCaseLetter(), // failing test
  			new VariableNamesStartWithLowerCaseLetter(), // failing test
        new LinesAreCorrectlyIndented(4), // failing test
        new ClosingBraceInOwnLine(), // failing test
        new OpeningBraceInSameLineWithStatement(), // failing test
//        new VariableDeclarationsHaveGenericParameter(), // failing test
        new MaxNumberOfCharactersInOneLine(60), // failing test
    		new MaxNumberOfStatementsInMethods(3, true, new Method("MyAwesomeClass.max()")), // failing test
        new MaxNumberOfStatementsInMethods(8, new Method("MyAwesomeClass.max()")) // failing test
  		);
  		*/

  	/*
  	sct.createNewTestGroup(
  			"Special Tests"
  	)
  		.addTests(
  			new Or( // failing test
    			new MethodCallUsedInMethods(
            new Method("MyAwesomeClass.funnyMethod()", Visibility.PUBLIC, "int", new Parameter("T")),
            new Method("min()"),
            new Method("max()")
        	),
				  new MethodsAreImplemented("MyAwesomeClass.myNotExistingRecursiveMethod()")
			  )
  		);
  		*/
    
  }
  
  
  @Test
  public void implementations() {
    runTests("Class and Method Implementations");
  }
  
  @Test
  public void functionality() {
    runTests("Functionality");
  }

//  @Test
//  public void limitations() {
//    runTests("Limitations");
//  }

//  @Test
//  public void javaDocumentation() {
//    runTests("Java Documentation");
//  }

//  @Test
//  public void codeQuality() {
//    runTests("Code Quality");
//  }

//  @Test
//  public void specialTests() {
//    runTests("Special Tests");
//  }


}
