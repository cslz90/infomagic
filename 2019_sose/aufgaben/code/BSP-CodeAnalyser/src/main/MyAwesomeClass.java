package main;

import java.io.IOException;
import java.rmi.server.ExportException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MyAwesomeClass implements Comparable<Integer> {
	
		private int a;
	private Collection<Integer> b;
		private java.util.ArrayList<Integer> arrayList;
		private Collection<?> test;
	
		public MyAwesomeClass() {
			new java.util.ArrayList<Integer>();
		java.util.ArrayList<Integer> array;
		a = 5;
		Math.random();
		Math.max(1, 2);
		Math.min(1, 2);
	}

	/**
	 * My epic recursive method for doing math.
	 * @param number Epic number used to do the cool maths.
	 * @return Returns another neat number created by maths.
	 */
	public static final int myRecursiveMethod(int number) {
		if (number > 1) {
			return myRecursiveMethod(number-1) + number;
		} else {
			return 0;
		}
	}

	/**
	 * Is
	 * This
	 * Long
	 * Enough?
	 * @pre Test
	 * @post
	 * @param numberA Test
	 * @param numberB
	 */
	public static final int myRecursiveMethod(int numberA, int numberB) {
		if (numberA > 1) {
			return myRecursiveMethod(numberA-1) + numberB;
		} else {
			return 0;
		}
	}
	
	public void test() {
		int i = 1;
		if (i==1) {
			i = 42;
		}
		
		if (i==1)
			i = 42;
		
		if (i==1) i = 42;
		
		
		if (i==1)
			i = 42;
		else if (i==2) 
			i = 42;
		else
			i = 42;
		
		if (i==1) {
			i = 42;
		} else
			i = 42;
		
		if (i==1) i = 42; else i = 1337;
		
		
		if (i==1) { i = 42; } else { i = 1337; }
		
		for (int j = 0; j < 42; j++)
			i++;
		
		while (i < 42)
			i++;
		
		List<Integer> list = new ArrayList<>();
		for (Integer integer : list)
			integer++;
		
	}
	
	/**
	 * 
	 */
	public void myNotRecursiveMethod() {
		int testVar = 10;
		myRecursiveMethod(testVar);
	}

	public void randomMethod() {
		myRecursiveMethod(2);
	}

	/**
	 * 
	 * @param isTrue This
	 * @throws IOException This is
	 */
	void myExceptionThrowingMethod(boolean isTrue) throws IOException {
		if (isTrue) {
			throw new IOException();
		} else {
			throw new ExportException("");
		}
	}
	
	public static <E extends Comparable> E maxWrong(Collection<E> collection) {
        return (E) java.util.Collections.max(collection);
    }

    public static <E extends Comparable> E minWrong(Collection<E> collection) {
        return (E) Collections.min(collection);
    }
    
    public static <A extends Comparable<? super A>> A min(Collection<? extends A> coll) {
    	myRecursiveMethod(1);
        A minObj = null;
        for(A obj : coll) {
            if(minObj != null) {
                if(obj.compareTo(minObj) < 0)
                    minObj = obj;
            }
            else
                minObj = obj;
        }

        return minObj;
    }

    /**
     * Awesome text right here.
     * @return Awesome text right here as well.
     */
    public static <A extends Comparable<? super A>> A max(Collection<? extends A> coll) {
    	myRecursiveMethod(1);
        A maxObj = null;
        for(A obj : coll) {
            if(maxObj != null) {
                if(obj.compareTo(maxObj) > 0)
                    maxObj = obj;
            }
            else
                maxObj = obj;
        }

        return maxObj;
    }

	@Override
	public int compareTo(Integer integer) {
		return 0;
	}
	
	
	public class RandomClass implements Comparable<String> {

		@Override
		public int compareTo(String string) {
			return 0;
		}
		
	}
}


