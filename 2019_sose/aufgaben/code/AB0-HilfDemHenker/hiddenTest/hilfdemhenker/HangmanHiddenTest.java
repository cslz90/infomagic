package hilfdemhenker;

import de.thm.mni.aud.commons.CodeAnalyser;
import org.junit.Test;
import sourcecodetester.SourceCodeTester;
import sourcecodetester.tests.*;
import sourcecodetester.util.Constructor;
import sourcecodetester.util.Method;

public class HangmanHiddenTest extends CodeAnalyser {

  @Override
  public void setupTests(SourceCodeTester sct) {

    // CLASS AND METHOD IMPLEMENTATIONS
    sct.createNewTestGroup("Class and Method Implementations", "The following tests are checking for correct class and method implementations.")
      .addTests(
        new ClassImplementations(
          "hilfdemhenker.Hangman"
        ),
        new ConstructorImplementations(
          new Constructor("hilfdemhenker.Hangman.Hangman()")
            .requireVisibilityPublic()
            .requireParameters("String", "int")
        ),
        new MethodImplementations(true,
          new Method("hilfdemhenker.Hangman.getWord()")
            .requireVisibilityPublic()
            .requireReturnType("String")
            .requireParametersNone(),
          new Method("hilfdemhenker.Hangman.isPossibleToLose()")
            .requireVisibilityPublic()
            .requireReturnType("boolean")
            .requireParametersNone(),
          new Method("hilfdemhenker.Hangman.getLineCount()")
            .requireVisibilityPublic()
            .requireReturnType("int")
            .requireParametersNone(),
          new Method("hilfdemhenker.Hangman.playRandomly()")
            .requireVisibilityPublic()
            .requireReturnType("char")
            .requireParametersNone(),
          new Method("hilfdemhenker.Hangman.isWin()")
            .requireVisibilityPublic()
            .requireReturnType("boolean")
            .requireParametersNone(),
          new Method("hilfdemhenker.Hangman.playStrategically()")
            .requireVisibilityPublic()
            .requireReturnType("char")
            .requireParametersNone()
          )
      )
    ;

    // CODE QUALITY
    sct.createNewTestGroup("Code Quality", "The following tests are checking the overall quality of your source code.")
      .addTests(
        new ClassNamesStartWithCapitalLetter(),
        new MethodNamesStartWithLowerCaseLetter(),
        new VariableNamesStartWithLowerCaseLetter(),
        new ClosingBraceInOwnLine(),
        new OpeningBraceInSameLineWithStatement(),
        //new LineIndentation(8),
        new LimitCharactersPerLine(150)
      );
  }

  @Test
  public void implementationsTest() { runTests("Class and Method Implementations");
  }

  @Test
  public void codeQualityTest() {
    runTests("Code Quality");
  }
}
