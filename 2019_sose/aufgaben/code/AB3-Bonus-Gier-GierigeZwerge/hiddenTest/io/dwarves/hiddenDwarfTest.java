package io.dwarves;

import de.thm.mni.aud.commons.AbstractInputOutputTest;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.fail;

public class hiddenDwarfTest extends AbstractInputOutputTest {

    private List<Integer> dwarves = new ArrayList<>();
    private int calculatedIndex, calculatedValue;

    @Before
    public void init() {
        studentInputLines.forEach(a -> dwarves.add(Integer.parseInt(a)));
        String[] splitted = studentOutputLines.get(0).split(";");
        calculatedIndex = Integer.parseInt(splitted[0]);
        calculatedValue = Integer.parseInt(splitted[1]);
    }

    private int[] getNeighbourIndexes(int index, List<Integer> dwarfList) {
        int[] out = new int[2];
        int left = (index > 0) ? index - 1 : dwarfList.size() - 1;
        int right = (index < dwarfList.size() - 1) ? index + 1 : 0;
        while (dwarfList.get(left) == null) {
            left = (left > 0) ? left - 1 : dwarfList.size() - 1;
        }
        while (dwarfList.get(right) == null) {
            right = (right < dwarfList.size() - 1) ? right + 1 : 0;
        }
        out[0] = left;
        out[1] = right;
        return out;
    }

    @Test
    public void testDwarves() {
        int activeDwarves = dwarves.size();
        int currentDwarfIndex = 1;
        while (activeDwarves > 1) {
            currentDwarfIndex = getNeighbourIndexes(currentDwarfIndex, dwarves)[0];
            int[] neighbours = getNeighbourIndexes(currentDwarfIndex, dwarves);
            int highest;
            if (dwarves.get(neighbours[0]) >= dwarves.get(neighbours[1])) {
                highest = neighbours[0];
            } else {
                highest = neighbours[1];
            }
            dwarves.set(currentDwarfIndex, dwarves.get(currentDwarfIndex) + dwarves.get(highest));
            dwarves.set(highest, null);
            activeDwarves -= 1;
        }
        //System.out.println(currentDwarfIndex);
        if (currentDwarfIndex != calculatedIndex) {
            fail(String.format("Calculated index (%d) is wrong", calculatedIndex));
        }
        if (dwarves.get(currentDwarfIndex) != calculatedValue) {
            fail(String.format("Calculated value (%d) is wrong", calculatedValue));
        }
    }
}
