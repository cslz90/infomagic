package changemaking;

import java.util.Random;

public class CoinChangeGenerator {
    public static void generateShuffledLongs(int n, long start, long step) {
        long[] ar = new long[n];
        for(int i = 0; i < n; i++) {
            ar[i] = i * step + start;
        }
        Random r = new Random();
        for(int i = 0; i < n; i++) {
            int ridx = r.nextInt(n);
            long tmp = ar[ridx];
            ar[ridx] = ar[i];
            ar[i] = tmp;
        }
        for(int i = 0; i < n; i++) {
            System.out.println(ar[i]);
        }
    }
}
