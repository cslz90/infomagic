package changemaking;

import static changemaking.CoinChangeGenerator.generateShuffledLongs;

public class Level6Generator {
    public static void main(String[] args) {
        generateShuffledLongs(1000, 10_000_000_000l, 982_451_653);
    }
}
