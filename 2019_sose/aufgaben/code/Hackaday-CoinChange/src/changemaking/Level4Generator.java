package changemaking;

import static changemaking.CoinChangeGenerator.generateShuffledLongs;

public class Level4Generator {
    public static void main(String[] args) {
        generateShuffledLongs(1000, 10_000_000, 1_233_259);
    }
}
