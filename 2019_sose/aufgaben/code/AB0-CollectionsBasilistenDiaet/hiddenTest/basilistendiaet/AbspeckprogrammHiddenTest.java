package basilistendiaet;


import org.junit.Test;
import de.thm.mni.aud.commons.CodeAnalyser;
import sourcecodetester.SourceCodeTester;
import sourcecodetester.tests.*;
import sourcecodetester.util.Method;

public class AbspeckprogrammHiddenTest extends CodeAnalyser {

  @Override
  public void setupTests(SourceCodeTester sct) {

    // CLASS AND METHOD IMPLEMENTATIONS
    sct.createNewTestGroup("Class and Method Implementations", "The following tests are checking for correct class and method implementations.")
      .addTests(
        new ClassImplementations(
          "basilistendiaet.Abspeckprogramm"
        ),
        new MethodImplementations(true,
          new Method("basilistendiaet.Abspeckprogramm.distinct()")
            .requireVisibilityPublic()
            .requireModifierStatic()
            .requireReturnType("List<E>")
            .requireParameters("List<E>")
        )
      )
    ;

    // CODE QUALITY
    sct.createNewTestGroup("Code Quality", "The following tests are checking the overall quality of your source code.")
      .addTests(
        new ClassNamesStartWithCapitalLetter(),
        new MethodNamesStartWithLowerCaseLetter(),
        new VariableNamesStartWithLowerCaseLetter(),
        new ClosingBraceInOwnLine(),
        new OpeningBraceInSameLineWithStatement(),
        new LimitCharactersPerLine(150)
      )
    ;
  }

  @Test
  public void implementationsTest() {
    runTests("Class and Method Implementations");
  }

  @Test
  public void codeQualityTest() {
    runTests("Code Quality");
  }
}
