#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Author: Birger Schulze

import sys
import distance


inFile=open(sys.argv[1], "r")
for line in inFile:
    splitted = line.upper().strip().split(";")
    #print("{a} {b} {dist}".format(a=splitted[0], b=splitted[1], dist=distance.levenshtein(splitted[0], splitted[1])))
    print(distance.levenshtein(splitted[0], splitted[1]))
