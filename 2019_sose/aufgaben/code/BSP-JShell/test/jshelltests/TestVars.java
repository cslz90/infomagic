package jshelltests;
import de.thm.mni.aud.commons.AbstractJShellTest;
import static org.junit.Assert.*;
import org.junit.Test;
import jdk.jshell.JShellException;
import java.io.IOException;

public class TestVars extends AbstractJShellTest {

    public TestVars()  throws IOException { super(); }

  @Test
  public void testVarFoo() {
    assertEquals("true",expressionResult("foo == 10"));
    assertEquals("int",varByName("foo").typeName());
  }

  @Test
  public void testVarBar() {
    assertEquals("3.5",shell.varValue(varByName("bar")));
    assertEquals("float",varByName("bar").typeName());
  }

  @Test
  public void testJShellExceptions() throws JShellException {
    assertNoJShellExceptions();
  }

  @Test
  public void testJShellRejected() {
    assertNoSnippetRejected();
  }
}
