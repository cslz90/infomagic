package tribonati;

import static org.junit.Assert.*;

import de.thm.mni.aud.commons.CodeAnalyser;
import org.junit.Test;
import sourcecodetester.SourceCodeTester;
import sourcecodetester.tests.*;
import sourcecodetester.util.Method;

public class TribonatiTest extends CodeAnalyser {

  @Test
  public void tribTest() {
    assertEquals(0, Tribonati.tribDyn(0, new int[1]));
    assertEquals(1, Tribonati.tribDyn(1, new int[2]));
    assertEquals(1, Tribonati.tribDyn(2, new int[3]));
    assertEquals(2, Tribonati.tribDyn(3, new int[4]));
    assertEquals(24, Tribonati.tribDyn(7, new int[8]));
    assertEquals(197466473, Tribonati.tribDyn(50, new int[51]));
  }

  @Test(timeout = 100)
  public void dynamicProgrammingTest() {
    Tribonati.tribDyn(5000, new int[5001]);
  }

  @Override
  public void setupTests(SourceCodeTester sct) {

    // CLASS AND METHOD IMPLEMENTATIONS
    sct.createNewTestGroup("Class and Method Implementations", "The following tests are checking for correct class and method implementations.")
      .addTests(
        new ClassImplementations(
          "tribonati.Tribonati"
        ),
        new MethodImplementations(true,
          new Method("tribonati.Tribonati.tribDyn()")
            .requireVisibilityPublic()
            .requireModifierStatic()
            .requireReturnType("int")
            .requireParameters("int", "int[]")
        )
      )
    ;

    // CODE QUALITY
    sct.createNewTestGroup("Code Quality", "The following tests are checking the overall quality of your source code.")
      .addTests(
        new ClassNamesStartWithCapitalLetter(),
        new MethodNamesStartWithLowerCaseLetter(),
        new VariableNamesStartWithLowerCaseLetter(),
        new ClosingBraceInOwnLine(),
        new OpeningBraceInSameLineWithStatement(),
        new LimitCharactersPerLine(150)
      )
    ;


    sct.createNewTestGroup("Functionality", "The following tests are checking specific code functionalities.")
      .addTests(
        new MethodRecursiveImplementations(
          new Method("tribonati.Tribonati.tribDyn()")
        )
      )
    ;

  }

  @Test
  public void implementationsTest() {
    runTests("Class and Method Implementations");
  }

  @Test
  public void codeQualityTest() {
    runTests("Code Quality");
  }

	@Test
	public void functionalityTest() {
		runTests("Functionality");
	}

}
