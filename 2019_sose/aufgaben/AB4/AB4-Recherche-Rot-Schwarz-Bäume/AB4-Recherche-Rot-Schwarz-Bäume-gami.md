## Kommunistenkirschen

*Sie dringen immer weiter in den Hain der Speichereichen ein und treffen dabei auf immer obskurere Vertreter dieser Gattung, wie die Kommunistenkirsche.*

*Diese strengen binärborkigen Gehölze glauben fest daran, dass sie die Last ihrer Früchte gleichmäßig auf alle Äste verteilen müssen.
Dazu teilen sie diese in die schwarzen Marx-Kirschen und die roten Lenin-Kirschen auf, wobei Äste mit Lenin-Kirschen etwas länger sein dürfen.*

### Aufgabe 1.1

*Schreiben Sie einen Eintrag über die Kommunistenkirsche in Ihr Herbarium.
Wagen Sie sich auf der Suche nach dem Buch ins Netz der großen Google-Spinne oder in die verbotene Bibliothek der Thaumaturgischen Hochschule - je nach dem, was Ihnen weniger gefährlich erscheint.*

*Beantworten Sie dabei die folgenden Fragen:*

* *Was sind die 5 Regeln des kommunistenkirschigen Manifests?*
* *Die Bürokraten von Ja'va pflanzen gerne Kommunistenkirschen. Unter welchem Namen tauchen diese in der Allgemeinen Pingeligen Inventarliste (API) auf?*
* *Wie hilft die politische Einstellung der Kommunistenkirsche ihr gegenüber ihren unpolitischen nativen Verwandten unter den binärborkigen Speichereichen.*
* *Welche Eigenschaft muss eine Kommunistenkirsche haben, wenn sie nur aus schwarzen Marx-Kirschen besteht?*
* *Wie hoch kann eine Kommunistenkirsche mit n Früchten höchstens werden? Wie viele Marx- und wieviele Lenin-Kirschen hat der Pfad von der Wurzel zu einem Blatt auf der höchsten Ebene?*

**Hinweise:**

* *Denken Sie daran, dass Herbarien genauso bissig werden wie Bestiarien, wenn man einen Eintrag von mehr als 400 Worten schreibt, Rechtschreibfehler einbaut, oder keine vollständigen Sätze verwendet.*

### Aufgabe 1.2

*Um ihren Mitnovizen zu helfen, genauere Studien anzustellen, sprechen Sie bitte einen kurzen Ortungszauber, um ihnen den Weg zu Ihren Informationsquellen zu weisen.
Da Novizen sich schnell von zu vielen Informationen verwirren lassen, beschränken Sie sich auf fünf davon.*