# Musterlösung zur Rechercheaufgabe "Kommunistenkirschen"

Ein Rot-Schwarz-Baum ist ein binärer Suchbaum, der die folgednen Eigenschaften erfüllt:

1. Jeder Knoten ist entweder rot oder schwarz.
2. Die Wurzel ist schwarz.
3. Kinder von roten Knoten sind schwarz.
4. Alle Blätter sind leer (NIL) und schwarz.
5. Alle Pfade von einem Knoten zu einem von dessen Blattknoten haben die gleiche Anzahl von schwarzen Knoten.

Rot-Schwarz-Bäume sind so balanciert, dass ihre Tiefe - und mit auch die Laufzeit der binären Suche in einem solchen Baum - in O(log n) liegt.
Sie werden auch zum Beispiel in der Standardbibliothek von Java in der Implementierung der `TreeMap` verwendet.

Ein Rot-Schwarz-Baum, der nur aus schwarzen Knoten besteht, wäre perfekt balanciert.
Das ist aber natürlich nur für vollständige Bäume (n = 2^x) möglich, da es in einem unvollständigen Baum immer einige Blätter geben muss, die um einen Knoten näher an der Wurzel liegen als das tiefste Blatt im Baum.

Die Einführung von roten Knoten erlaubt daher etwas Flexibilität, die durch die Regel 3 wieder eingeschränkt wird.
Im Extremfall kann ein Rot-Schwarz-Baum mit n Knoten eine Tiefe von 2 * log_2(n + 1) haben, wobei dann im Pfad zum tiefsten Blatt immer abwechselnd ein roter und ein schwarzer Knoten auftaucht.
Insgesamt wären in diesem Pfad also log_2(n + 1) schwarze und log_2(n + 1) rote Knoten und es könnte einen kürzeren Pfad zu einem anderen Blatt geben, der nur ausschließlich aus log_2(n + 1) schwarzen Knoten besteht.
Damit ist die Unbalanciertheit des Rot-Schwarz-Baums also auf einen Faktor von 2 beschränkt.