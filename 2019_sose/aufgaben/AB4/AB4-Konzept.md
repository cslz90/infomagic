# Aufgabenblatt 4
## Pflichtaufgaben

|Nr | Thema                     | Name                                | Dauer in Sdt (für Studis)      | Punkte Gildamesh  | Ref |
|---| ---                       | ---                                 | ---                            |               --- | --- |
|01 | Rot-Schwarz-Bäume/Recherche         | AB4-Recherche-Rot-Schwarz-Bäume                   |  Geschätzt:  2h (Grob Probegelöst 1h)     |        	??|[Link](2018_ss/aufgaben/AB4/AB4-Recherche-Rot-Schwarz-Bäume/AB4-Recherche-Rot-Schwarz-Bäume-klar.md) |
|02 | Binäre Suchbäume          | AB4 - Mergesort                     | Geschätzt:  ?? Probegelöst in ca. 1h         |               ??  |?? |
|03 | Klasseninvariante     | AB4-Klasseninvariante | Geschätzt:  ?? (Grob Probegelöst 1,5h)| ?? | ?? |

## Bonusaufgaben

|Nr | Thema                     | Name                                 | Dauer in Sdt (für Studis)      | Punkte Gildamesh  | Ref |
|---| ---                       | ---                                  | ---                            |               --- | --- |
|01 | Binärbaum              	| AB4-BinärerSuchbaumII                |                   |                ?? |  ?  |
|02 | Balanciertheit                     |Balancierungstest als Pseudocode                     | Geschätzt:  1h   Probegelöst: ca. 30 min     |                ?? |  ?  |
|03 | bäume                 | AB4-Strukturgleichheit            | Geschätzt:  ??                 |                ?? |  ?  |
|04 | Traversierung                 | AB4 - DFS-Iterator          | Geschätzt:  2,5h Probegelöst ca. 30min                |                ?? |  ?  |
|05 | Traversierung      | AB4 - BFS-Iterator  | Geschätzt:  2,5h                 |                ?? |  ?  |
|06 | Traversierung                     | AB4 - IDDFS-Iterator                    | Geschätzt:  5:15 (3h) Probegelöst: 1,75h  |                ?? |  ?  |
