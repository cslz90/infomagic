# Binärborkige Speichereichen I / Binärer Suchbaum I

*Da Sie die Forschung der Familie der Basilisten langsam langweilt, beschließen Sie Ihr Forschungsgebiet auf eine andere, spannende Datenkreaturfamilie, die der Speichereichen, zu verlagern.*

*Speichereichen sind beliebt und deswegen weit verbreitet. Sowohl im ObjektOrient als auch in Funktionlia können und werden Sie diese majestätischen Kreaturen in freier Wildbahn antreffen. Als angehender Infomagier wollen Sie diese Datenkreatur deswegen ausgiebig studieren.*

*Sie beginnen Ihr Studium der Speichereichen mit einer besonderen Untergattung: den binärborkigen Speichereichen.
Ein Spezifikum der binärborkigen Speichereichen ist, dass eine Knotenfrucht stets entweder zu einem Blütenblattknoten führt oder er sich in genau zwei weitere Äste mit neuen Knotenfrüchten aufspaltet.
Als Erstes betrachten Sie deswegen eine binärborkigen Speichereiche, in der Sie ausschließlich Integerlinge aufbewahren können.*

*Sie erproben den Umgang mit diesen Kreaturen in diesem [Feldversuch auf Dozentron](http://dozentron.mni.thm.de/submission_tasks/70).*

## Aufgabenstellung

Realisieren Sie im Package `binarytree` einen binären Suchbaum.
Legen Sie dazu eine Klasse `BinaryIntTree` in besagtem Package an, die die folgende Schnittstelle implementiert:

```java
package binarytree;

public abstract class ABinaryIntTree {

  /**
   * @return the root node of this tree.
   */
  public abstract ABinaryIntTreeNode root();

  /**
   * Add an integer value into this binary tree.
   */
  public abstract void add(int value);

  /**
   * Check, if the integer value exists in this tree.
   */
  public abstract boolean contains(int value);

  /**
   * @return true, if this tree is empty, false otherwise.
   */
  public abstract boolean isEmpty();

  /**
   * @return the number of elements in this tree.
   */
  public abstract int size();

}
```

Ein einzelner Knoten des `BinaryIntTree` wird von der Schnittstelle `ABinaryIntTreeNode` repräsentiert, von der Sie ebenfalls eine Realisierung anfertigen sollen (die Details der Implementierung sind ihnen überlassen).
Die Schnittstelle dafür wird im Folgenden aufgeführt:

```java
package binarytree;

public abstract class ABinaryIntTreeNode {

  /**
   * @return the value stored in this node.
   */
  public abstract int value();

  /**
   * @return the left subtree of this node.
   */
  public abstract ABinaryIntTreeNode left();

  /**
   * @return the right subtree of this node.
   */
  public abstract ABinaryIntTreeNode right();

}
```

### Hinweise
* Dieser binäre Suchbaum soll keine Elemente doppelt enthalten.
* Nutzen Sie die bereitgestellten JUnit-Tests während Ihrer Arbeit.

### Hilfestellungen

Als Hilfestellungen werden im Folgenden für die Methoden `contains` und `add` mögliche Implementierungen in Pseudocode vorgegeben.
Bitte beachten Sie, dass im Foliensatz 10 diese Algorithmen in ähnlicher Form vorgegeben wurden.
Die dort aufgeführten Algorithmen sind jedoch für binäre Suchbäume gedacht, die Elemente mehrfach enthalten können.

#### Pseudocode für `contains` auf Basis von `bsearch`
```java
algorithm bsearch(haystack: BTNode<E>, needle: E)
  if haystack = None then
    return None
  if value(haystack) = needle then
    return haystack
  if value(haystack) > needle then
    return bsearch(left(haystack), needle)
  else
    return bsearch(right(haystack), needle)
```

```java
algorithm contains(haystack: BTNode<E>, needle: E)
  if bsearch(haystack, needle) = None then
    return false
  else
    return true
```

#### Pseudocode für `add`
```java
algorithm sortedInsert(tree: BTNode<E>, el: E)
  next: BTNode<E>
  if el < value(tree) then
    next := left(tree)
  else if el > value(tree) then
    next := right(tree)

  if next = None then
    addChild(tree, el)
  else
    sortedInsert(next, el)
```
