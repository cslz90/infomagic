# Korrekturempfehlungen

## Positive Faktoren
- Die Hinweise wurden beabsichtigt und eingehalten!
"Dieser binäre Suchbaum soll keine Elemente doppelt enthalten."
(Unit Test!)
- Die algorithmen "bsearch" und "sortedInsert" wurden zur Umsetzung verwendet und sind korrekt implementiert.
- Abstrakte Vorgabe wurde um eine Methode "addChild()" für Nodes erweitert.

## Weiche Kriterien
- Die Methode "contains" funktioniert ordnungsgemäß und mithilfe von "bsearch". und nur "addChild" wurde nicht korrekt umgesetzt.
- Size stimmt nicht überein. Die Wurzel des Baues wurde nicht mitgezählt.
