## Binärborkige Speichereichenmodels

*Bei dem jährlich stattfindenden Schönheitswettbewerb für Baumhydren im großen Speichereichenwald entschieden die Punktrichter, darunter Tiefentauben und Breitenhörnchen, bisher immer nur nach ihrer persönlichen Einschätzung, wer am Ende gewinnt. Letztes Jahr beschwerten sich viele zuschauende Zwergengilden, dass es nicht sein könne, dass wunderschön gleichmäßig und balanciert gewachsene binärborkige Speichereichen gegen hässliche Kommunistenkirschen verlieren. Deshalb soll dieses Jahr eine algomantische Formel in Universalhieroglyphen als Bewertungsmaßstab angewendet werden. Sie wurden als angehender Infomagier damit beauftragt, diese Universalhieroglyphen zu schreiben.*

*Anhand der Formel soll einfacher entschieden werden können, ob eine Baumhydra den Kriterien einer schönen balancierten binärborkigen Speichereiche entspricht:*

*Unterscheidet sich an jeder Verzweigung die restliche Pfadlänge der beiden Teilbäume bis zum Blatt um höchstens eins, so ist eine Baumhydra schön gleichmäßig gewachsen und gilt als balanciert.*

*Weiterhin haben balancierte Baumhydren folgende Eigenschaften:*  
*(muss so nicht in der Formel geprüft werden)*
- *n Verzweigungen insgesamt*
- *Maximale Höhe d*
- *d <= c log n für irgendeine Konstante c*

**Hinweise:**

- Die Abgabe erfolgt als Klartextaufgabe.
