package zipper;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Stream;

public class Zipper {

    static <A, B> Stream<Tuple<A, B>> zip(Stream<A> stream1, Stream<B> stream2) {
        Iterator<A> first = stream1.iterator();
        Iterator<B> second = stream2.iterator();
        List<Tuple<A, B>> output = new LinkedList<>();
        while (first.hasNext() && second.hasNext()) output.add(new Tuple<>(first.next(), second.next()));
        return output.stream();
    }

    static <A, B, C> Stream<C> zipWith(Stream<A> stream1, Stream<B> stream2, BiFunction<A, B, C> biFn) {
        return zip(stream1, stream2).map(tuple -> biFn.apply(tuple._1(), tuple._2()));
    }

}
