#include <iostream>

int f(int a, int b) {
    if (a - b - 1 > 0) return 0;
    else if (a == 0 || b == 0) return 1;
    else return f(a - 1, b - 1) + f(a, b - 1);
}

int main(int argc, int argv[]) {
    std::cout << f(21, 29) << std::endl;
    return 0;
}