## Mergesort

*Wie Sie inzwischen bestimmt wissen, sind Basilisten sehr ordentliche Datenkreaturen und lassen sich liebend gerne sortieren. Eine recht effiziente Form dies zu bewirken, ist der Zauber "generis confundantur", den man umgangssprachlich auch als "MergeSort" bezeichnet.*

*Bei diesem Zauber werden die Basilisten erst vorsichtig aufgetrennt, um sie nachher in entsprechender Reihenfolge wieder zusammenzuführen.*

*Da ein einzelner Kopf eines Basilisten in Panik verfallen würde, wenn man ihn vom Rest trennen würde, versucht man immer möglichst gleichmäßig aufzuteilen. Zudem lässt man üblicherweise ca. zwei Köpfe beisammen, um die Kreaturen nicht zu sehr zu stressen.*

*Um unnötigen Stress für Novizen und Basilisten zu vermeiden, wird dieser Zauber erst einmal auf Arrays geübt. Diese sind vor allem den ArrayBasilisten sehr ähnlich, jedoch sind sie von der Handhabung viel einfacher.*

*Aus der Thaumaturgischen Hochschule der Magie haben Sie den groben Aufbau eines Merge-Zaubers erfahren. Nun liegt es an Ihnen diesen Zauber zu perfektionieren, damit Sie eines Tages auch auf das Sortieren von ArrayBasilisten mit "generis confundantur" umsteigen können. Bedenken Sie hierbei, dass es sich um einen Spruch "Rekursio" handelt.*

### Formalia

Implementieren sie die Klasse `MergeSort` im Package `mergesort`. Darin sollen sich die folgenden Methoden befinden:
- `public static void sort(int[] arr)`
- `static void merge(int[] arr, int fromIndex, int middleIndex, int toIndex)`

**Getestet werden zwei Methoden**:
* die Methode `sort`, die ein `int`-Array übernehmen und sortieren muss
* die Methode `merge`, die zwei sortierte `arr`-Subarrays von `fromIndex` bis `middleIndex` und von `middleIndex` bis `toIndex` verschmilzt.

**Hinweise**:
* In den Vorlesungsfolien werden Sie auch weitere Informationen zu "Teile und Herrsche" und "MergeSort" finden.  
* Beachten Sie, dass untere Grenzen immer inklusive und obere Grenzen immer exklusive gegeben sind. Ein Beispiel zur Methode `sort`: Das erste Element ist `arr[fromIndex]`, das letzte Element ist `arr[toIndex - 1]`.
* Bei der Implementierung der Methode `merge` kann ein temporäres Array zur Hilfe genommen werden. 

[Link zur Abgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/67)
