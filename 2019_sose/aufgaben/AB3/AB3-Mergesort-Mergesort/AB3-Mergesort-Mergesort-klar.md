## MergeSort

Implementieren Sie Mergesort um ein Array zu sortieren. Dieser Algorithmus arbeitet nach dem Prinzip "Teile und Herrsche":

Er zerlegt das Array in zwei Teile, sortiert sie rekursiv und verschmilzt am Ende die Teile wieder (engl. merge).

Implementieren sie die Klasse `MergeSort` im Package `mergesort`. Darin sollen sich die folgenden Methoden befinden:
- `public static void sort(int[] arr)`
- `static void merge(int[] arr, int fromIndex, int middleIndex, int toIndex)`

**Getestet werden zwei Methoden**:
* die Methode `sort`, die ein `int`-Array übernehmen und sortieren muss
* die Methode `merge`, die zwei sortierte `arr`-Subarrays von `fromIndex` bis `middleIndex` und von `middleIndex` bis `toIndex` verschmilzt.

**Hinweise**:
* In den Vorlesungsfolien werden Sie auch weitere Informationen zu "Teile und Herrsche" und "MergeSort" finden.  
* Beachten Sie, dass untere Grenzen immer inklusive und obere Grenzen immer exklusive gegeben sind. Ein Beispiel zur Methode `sort`: Das erste Element ist `arr[fromIndex]`, das letzte Element ist `arr[toIndex - 1]`.
* Bei der Implementierung der Methode `merge` kann ein temporäres Array zur Hilfe genommen werden. 