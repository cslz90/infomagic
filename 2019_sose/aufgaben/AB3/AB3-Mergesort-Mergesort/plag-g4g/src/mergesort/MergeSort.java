package mergesort;

import java.util.Arrays;

/* Java program for Merge Sort */
class MergeSort
{
    public static void sort(int[] arr) {
        sort(arr, 0, arr.length);
    }

    private static int counter = 0;


    static int getRecursionCounter() { // gibt die Anzahl der rekursiven Aufrufe zurück
        return counter;
    }

    static void resetRecursionCounter() { // setzt den Rekursionszähler wieder auf 0
        counter = 0;
    }

    // Merges two subarrays of arr[].
    // First subarray is arr[l..m]
    // Second subarray is arr[m+1..r]
    static void merge(int arr[], int l, int m, int r)
    {
        //System.out.println(""+l+" "+m+" "+r);

        // Find sizes of two subarrays to be merged
        int n1 = m - l;
        int n2 = r - m;

        /* Create temp arrays */
        int L[] = new int [n1];
        int R[] = new int [n2];

        /*Copy data to temp arrays*/
        for (int i=0; i<n1; ++i)
            L[i] = arr[l + i];
        for (int j=0; j<n2; ++j)
            R[j] = arr[m + j];


        /* Merge the temp arrays */

        // Initial indexes of first and second subarrays
        int i = 0, j = 0;

        // Initial index of merged subarry array
        int k = l;
        while (i < n1 && j < n2)
        {
            if (L[i] <= R[j])
            {
                arr[k] = L[i];
                i++;
            }
            else
            {
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        /* Copy remaining elements of L[] if any */
        while (i < n1)
        {
            arr[k] = L[i];
            i++;
            k++;
        }

        /* Copy remaining elements of R[] if any */
        while (j < n2)
        {
            arr[k] = R[j];
            j++;
            k++;
        }
    }

    // Main function that sorts arr[l..r] using
    // merge()
    static void sort(int arr[], int l, int r)
    {
        //System.out.println("Sort "+l+"-"+r);
        counter++;
        if (l < r - 1)
        {
            // Find the middle point
            int m = (l+r)/2;

            // Sort first and second halves
            sort(arr, l, m);
            sort(arr , m, r);

            // Merge the sorted halves
            merge(arr, l, m, r);
        }
        //System.out.println("Sorted ("+l+"-"+r+")"+ Arrays.toString(Arrays.copyOfRange(arr, l, r)));
    }

    /* A utility function to print array of size n */
    static void printArray(int arr[])
    {
        int n = arr.length;
        for (int i=0; i<n; ++i)
            System.out.print(arr[i] + " ");
        System.out.println();
    }

    // Driver method
    public static void main(String args[])
    {
        int arr[] = {12, 11, 13, 5, 6, 7, 8, 5, 19};

        System.out.println("Given Array");
        printArray(arr);

        MergeSort ob = new MergeSort();
        ob.sort(arr, 0, arr.length);

        System.out.println("\nSorted array");
        printArray(arr);
    }
}
/* This code is contributed by Rajat Mishra */