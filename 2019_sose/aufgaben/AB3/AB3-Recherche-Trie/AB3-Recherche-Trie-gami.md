## Präfixpappeln

*Im Hain der Speichereichen wachsen die verschiedensten Baumsorten.
Eine besonders interessante Gattung ist die Präfixpappel.*

*Diese oft riesig großen Gewächse haben die Angewohnheit, dass sie jedes Wort, was man sagen möchte, sofort vervollständigen wollen.
Daher sind sie auch bei Novizen beliebt, die Einträge für ihr Herbarium oder ihr Bestiarium verfassen wollen.*

> Hinweis: Eine Präfixpappel heißt außerhalb unserer Gamification "Trie".

### Aufgabe 1.1

*Genau dies sollen Sie jetzt tun: Schreiben Sie einen Eintrag über die Präfixpappel in Ihr Herbarium.
Wagen Sie sich auf der Suche nach dem Buch ins Netz der großen Google-Spinne oder in die verbotene Bibliothek der Thaumaturgischen Hochschule - je nach dem, was Ihnen weniger gefährlich erscheint.
Man munkelt, dass die Schriften des großen Knutherich hier sehr hilfreich sein können.*

*Beantworten Sie dabei die folgenden Fragen:*

* *Zu welcher Familie von Datenkreaturen gehört die Präfixpappel?*
* *Finden Sie eine Präfixpappel, die die folgenden Worte gespeichert hat und fügen Sie eine Skizze von dieser in Ihr Herbarium ein. Benutzen Sie dazu eine einzelne zusätzliche Seite aus Pergament der Forschung (PDF), Jungen Pergament-Gräsern (JPG) oder Pergamentum non Gratum (PNG).*
  * bla
  * bar
  * blubb
  * foo
* *Wie würde man in dieser Präfixpappel nach dem Wort "bla" suchen?*
* *Wie würde man in dieser Präfixpappel nach dem Wort "ba" suchen?*
* *Wie stehen die Weisen von O zu Präfixpappeln? In welche Effizienzklasse würden sie die Suche nach einem Wort der Länge m in einer Präfixpappel stecken, die insgesamt n Worte enthält?*

**Bonus:** *Die Beantwortung der folgenden Frage ist nicht nötig, um Ihren Herbariumseintrag zu vervollständigen, aber Sie könnten den Goldgorgonen und seine Adepten damit gewogen stimmen.*

* Können Sie die Weisen von O dazu bringen, eine Bewertung der Suche in Präfixpappeln abzugeben, die nicht von der Wortlänge m abhängt? Betrachten Sie dabei eine Präfixpappel, die alle möglichen Worte (aus den Buchstaben a bis z) der Länge m speichert.

**Hinweise:**

* *Denken Sie daran, dass Herbarien genauso bissig werden wie Bestiarien, wenn man einen Eintrag von mehr als 400 Worten schreibt, Rechtschreibfehler einbaut, oder keine vollständigen Sätze verwendet.*

### Aufgabe 1.2

*Um ihren Mitnovizen zu helfen, genauere Studien anzustellen, sprechen Sie bitte einen kurzen Ortungszauber, um ihnen den Weg zu Ihren Informationsquellen zu weisen.
Da Novizen sich schnell von zu vielen Informationen verwirren lassen, beschränken Sie sich auf fünf davon.*
