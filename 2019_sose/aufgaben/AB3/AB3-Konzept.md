# Aufgabenblatt 3
## Pflichtaufgaben

|Nr | Thema                     | Name                                | Dauer in Sdt (für Studis)      | Punkte Gildamesh  | Ref |
|---| ---                       | ---                                 | ---                            |               --- | --- |
|01 | Binäre Suche              | AB3 - BinäreSuche                   | Geschätzt:  1/6   Probegelöst in 2-5min.                 |                2 |[Link](2018_ss/aufgaben/AB3/AB3-Recherche-Trie/AB3-Recherche-Trie-klar.md)|
|02 | Mergesort                 | AB3 - Mergesort                     | Geschätzt:  1/2   Probegelöst in ca. 1h               |                59 |[Link](2018_ss/aufgaben/AB3/AB3-Mergesort-Mergesort/AB3-Mergesort-Mergesort-klar.md)|
|03 | Recherche / Bäume         | AB3 - Recherche-Trie                | Geschätzt:  2              |                39 |  [Link](2018_ss/aufgaben/AB3/AB3-BinäreSuche-BinärSuche/AB3-BinäreSuche-BinärSuche-klar.md)  |


## Bonusaufgaben

|Nr | Thema                     | Name                                 | Dauer in Sdt (für Studis)      | Punkte Gildamesh  | Ref |
|---| ---                       | ---                                  | ---                            |               --- | --- |
|01 | Sortieren                 | Permutation sort                     | Geschätzt:  2     Probegelöst in ca. 1,5h          |                ?? |  ?  |
|02 | Bäume                     | Decision Tree                        | Geschätzt:  ??                 |                ?? |  ?  |
|03 | Suchbäume                 | AB3 - BinärerSuchbaumI               | Geschätzt:  ??                 |                ?? |  ?  |
|04 | Laziness                  | AB3 - Zip für Java-Streams           | Geschätzt:  2h    Probegelöst in ca. 0,5           |                ?? |  ?  |
|05 | Schleifeninvariante       | AB3 - ImmerwahrMeetsGrossmeisterVonO | Geschätzt:  ??                 |                ?? |  ?  |
|06 | Bäume                     | AB3 - Labyrinth                      | Geschätzt:  ??                 |                ?? |  ?  |
|07 | Josephus-Problem          | AB3 - GierigeZwergeDieZweite         | Geschätzt:  1h                 |                ?? |  ?  |
|08 | Rekursion                 | AB3 - BonsaiHydrachen                | Geschätzt:  1h    Probegelöst in ca. 1h            |                ?? |  ?  |
