## Combinatoris sortere
### Dauer: 2h

Implementieren Sie den Sortieralgorithmus "Permutation Sort" aus der Unteraufgabe `combinatoris sortere`, der Aufgabe  `Laufzeitanalyse-MagischeSelbsteinschätzung`, von `AB1`.  
Dem Algorithmus soll eine Liste aus Zahlen übergeben werden. Daraufhin überprüft er jede Permutation der Liste, ob
sie aufsteigend sortiert ist und gibt die entsprechende Permutation zurück.

### Hinweise

Der Sortieralgorithmus muss in der Klasse `PermutationSort` im Package `permutationsort` implementiert werden und aus Zwei Methoden bestehen:

```java
public static void permutationSort(List<Integer> lst)
public static List<List<Integer>> permutations(List<Integer> lst)
```

Dabei generiert `permutations` alle möglichen Permutationen für eine gegebene Liste.

Dieser Pseudocode dient Ihnen als Hilfe, sie müssen sich aber nicht danach richten:

```
algorithm isSorted(list: List<Integer>)
  for i := 1, 2, ..., list.size - 1
    if list.get(i-1) > list.get(i) then
      return false
  return true
```

```
algorithm permutationSort(list: List<Integer>)
  for permutation from permutations(list)
    if isSorted(permut) then
      list := permut
```

```
algorithm permutations(list: List<Integer>): List<List<Integer>>
  List<List<Integer>> lst := ArrayList<List<Integer>>.new()
  if lst.size <= 1 then
    permutations.add(lst)
  else
    List<Integer> list := ArrayList<Integer>.new(lst)
    int last := list.get(list.size-1)
    List<Integer> rest := list.fromTo(0, list.size-1)
    for permut from permutations(rest)
      for i := 0, 1, ..., permut.size - 1
        List<Integer> permutation = ArrayList<>.new()
        permutation.add(permut)
        permutation.add(i, last)
        permutations.add(permutation)
  return permutations
```

#### Achtung:
Da der Algorithmus `permutations` hier im Pseudocode rekursiv ist, kommt man bei größeren Listen (ca. mehr als 10 Elemente) schnell an die Grenzen, da es dann entsprechend viele Permutationen gibt: Bei n Elementen n!. Bei großen Listen ist dann schnell der Arbeitsspeicher überfordert. Aber auch iterative Varianten brauchen relativ viel Rechenzeit.

Bspw. werden für eine Liste mit 7 Elementen schon 7! = 5040 Permutationen erzeugt!
