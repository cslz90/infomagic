## Combinatoris sortere
### Dauer: 2h

*Sie haben inzwischen schon zahlreiche Basilisten auf verschiedenste Art und Weise sortiert, doch in den Künsten der Algomantik gibt es immer wieder neue raffinierte Sprüche, die Ihnen das Leben erleichtern können. Hier sollen Sie eine neue Möglichkeit erproben, diese ordentlichen Datenkreaturen zu ordnen.*  

*Auf dem letzten Magierkongress wurde von dem Zauber "combinatoris sortere" berichtet, den einige Novizen auch als "Permutationsort" bezeichnen:*  
*Bei diesem Zauber werden Basilisten liebevoll zerteilt und in allen möglichen Anordnugen (Permutationen) wieder zusammengefügt, bis sie aufsteigend sortiert sind.*

*Im Detail heißt das, dass der ursprünglich vorderste Kopf einmal an jeder möglichen Stelle zwischen die anderen Köpfe und an das Ende gesetzt wird.*
*Mit den anderen Köpfen verfährt man dann mit demselben Schema.*

*Bei jeder möglichen Anordnung prüft der Zauber zusätzlich, ob die aufsteigende Sortierung geglückt ist.*

*Sie sollen diesen Spruch nun für zahlenfressende ArrayBasilisten üben. Schreiben Sie dazu die Formel wie im Folgenden beschrieben nieder:*

*Der Zauber muss, zusammen mit einem Hilfszauber, in dem `package permutationsort` gepackt sein, in der Sammlung `PermutationSort` stehen. Sie müssen wie folgt beginnen:*

```java
public static void permutationSort(List<Integer> lst)
public static List<List<Integer>> permutations(List<Integer> lst)
```

*Auf dem Magierkongress wurde folgende grobe Orientierung gegeben:*

```
algorithm isSorted(list: List<Integer>)
  for i := 1, 2, ..., list.size - 1
    if list.get(i-1) > list.get(i) then
      return false
  return true
```

```
algorithm permutationSort(list: List<Integer>)
  for permutation from permutations(list)
    if isSorted(permut) then
      list := permut
```

```
algorithm permutations(list: List<Integer>): List<List<Integer>>
  List<List<Integer>> lst := ArrayList<List<Integer>>.new()
  if lst.size <= 1 then
    permutations.add(lst)
  else
    List<Integer> list := ArrayList<Integer>.new(lst)
    int last := list.get(list.size-1)
    List<Integer> rest := list.fromTo(0, list.size-1)
    for permut from permutations(rest)
      for i := 0, 1, ..., permut.size - 1
        List<Integer> permutation = ArrayList<>.new()
        permutation.add(permut)
        permutation.add(i, last)
        permutations.add(permutation)
  return permutations
```

_**Achtung:**_

- *Da einem größeren Basilisten schneller schwindelig wird, wenn ständig seine Köpfe neu angeordnet werden, müssen Sie beim Anwenden dieses Zaubers viel Geduld mitbringen. Je größer der Basilist, desto länger dauert es.*

_**Hinweis:**_

- *Die obigen Universalhieroglyphen sollen Ihnen nur helfen. Sie müssen die algomantische Formel nicht genauso schreiben.*
