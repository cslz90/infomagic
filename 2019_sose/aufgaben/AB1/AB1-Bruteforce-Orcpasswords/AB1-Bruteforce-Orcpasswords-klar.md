# Kennwörter der Orks

- Bewertungsschema: Abgabe auf Dozentron, Prüfung durch UnitTests

Erzeugen Sie eine ArrayList mit allen möglichen Wörtern mit den folgenden Eigenschaften:
1. Jedes Wort fängt mit "grunz" an.
2. Jedes Wort hat eine Länge von maximal 9 Zeichen.
3. Jedes Wort besteht nur aus den Buchstaben 'a' bis 'z' und 'A' bis 'Z'.

##### Hinweis
> Chars werden in Java intern als Zahlenwerte dargestellt. Können sie dies nutzen, um darüber zu iterieren?

Folgende Formalia sollen eingehalten werden:
- Erstellen Sie eine Klasse `OrcPasswords` in dem package `orcpasswords`.
- Implementieren Sie in der Klasse `OrcPasswords` eine Methode mit der Signatur `public static ArrayList<String> passwords()`. Diese soll alle möglichen Wörter nach oben beschriebenen Regeln erzeugen.

[Gamifizierter Text auf Gildamesh](https://gildamesh.mni.thm.de/courses/1/components/301)
