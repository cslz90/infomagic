# Bewertungsvorschläge

## Positive Faktoren:
	- Rekursion mit Abbruchbedingung
	- Kreative Erstellung vom Alphabet String -> chars oder mit Streams :'D
	- Kombinationen kommen in der Liste vor und sind von der Aufbau richtig (unique, shape) 
	- Gut und verständlich kommentierter Code (Verwendung von Annotationen)
	- Verständliche und sinnvolle Variablennamen
	
## Weiche Kriterien:
	- Abbruchbedingung Stimmt nicht -> Anzahl der Kennwörter passt nicht
	- Einträge kommen doppelt in der Liste vor -> logischer Fehler
	- Shape passt nicht komplett -> Charset wurde nicht richtig erzeugt (Buchstabe fehlt oder ähnliches)
	- Es gibt statt 7454981 Passwörter 7454980 -> Anfangswort wurde nicht mitgezählt
	- Die Kennwörter wurden ohne den Anfang "grunz" generiert
	
## Unit-Tests
	- passwordsAreUnique -> 35%
	- passwordsMatchExpectedShape -> 35%
	- thereAre7454981Passwords -> 30%
	
## Ungültig:
	- Wenn ein Bibliothek für die generierung der "Passwörter" eingesetzt wird und man selber nichts Programmiert hat.
