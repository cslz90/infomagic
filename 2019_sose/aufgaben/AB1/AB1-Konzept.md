# Aufgabenblatt 1
## Pflichtaufgaben

|Nr | Thema                         | Name                                              | Typ      | Dauer in min   | XP  | Ref |
|---| ---                           | ---                                               | -------- | ---            | --- | --- |
|01 | Recherche                     | Recherche-Klein-o                                 | Text     | 180?           | 23  |  [Link](./../../../2019_ss/aufgaben/AB1/AB1-Recherche-Klein-o/AB1-Recherche-Klein-o-klar.md)  |
|02 | Brute-Force                   | BruteforceOrcpasswords                            | Unittest | 60             | 5   |  [Link](./../../../2019_ss/aufgaben/AB1/AB1-BruteForce-OrcPasswords/AB1-Bruteforce-Orcpasswords-klar.md)  |
|03 | Laufzeitklassen               | SozialeUngleichheit                               | IO       | 45             | 3   |  [Link](./../../../2019_ss/aufgaben/AB1/AB1-Laufzeitklassen-SozialeUngleichheit/AB1-Laufzeitklassen-SozialeUngleichheit-klar.md)  |
|04 | Laufzeitanalyse               | Laufzeitanalyse-MagischeSelbsteinschätzung        | Text     | 20?            | 2   |  [Link](./../../../2019_ss/aufgaben/AB1/AB1-Laufzeianalyse-MagischeSelbsteinschätzung/AB1-Laufzeianalyse-MagischeSelbsteinschätzung-klar.md)  |
|05 | Schleifeninvariante           | Schleifeninvarianten-DieImmerwahrs                | Text     | 20?            | 5   |  [Link](./../../../2019_ss/aufgaben/AB1/AB1-Schleifeninvarianten-DieImmerwahrs/AB1-Schleifeninvarianten-DieImmerwahrs-klar.md)  |

## Bonusaufgaben (fast fertig)

| Thema                         | Name                                                  | Dauer in min   | XP  | Ref |
| ---                           | ---                                                   | ---            | --- | --- |
| LinkedList                    | DoppeltVerketteterBasilist                            |            ?   | ?   |  [Link](./../../../2017_ss/aufgaben/AB1/AB1-GreedyAlgorithmusGierigeZwerge/AB1-GreedyAlgorithmusGierigeZwerge-klar.md)  |
| Gier                          | GreedyAlgorithmusGierigeZwerge                        |            ?   | ?   |  [Link](./../../../2017_ss/aufgaben/AB1/AB1-DoppeltVerketteterBasilist/AB1-DoppeltVerketteterBasilist-klar.md)  |
