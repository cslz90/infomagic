#### Magische Naturkunde: "Die Immerwahrs"

*Ihnen und vielen anderen Novizen sind wahrscheinlich schon aufgefallen, dass die Tage wieder länger werden. Wenn Sie früh morgens zum ersten Block höchst übermüdet an die Thaumaturgische Hochschule für Magie wandern, können Sie mittlerweile im Hellen und ganz ohne künstliches Licht ans Ziel gelangen.
Mit geschultem Novizenblick kann man aber noch einige andere Veränderungen in der Umwelt ausmachen. Dem aufmerksamen Beobachter verschlägt die Farbenpracht der Jahreszeit schier den Atem. Vor den Toren der Thaumaturgischen Hochschule  erstrahlt ein reinstes Blütenmeer.  
Das war nicht immer so: Erst kürzlich während der letzten Klausurphase konnten wir einen rapiden Rückgang an blühenden Immerwahrs verzeichnen.  
Viele Forscher und Großmeister sind sich über die genauen Gründe dieser kleinen, zyklischen Naturkatastrophe im Unklaren, aber sehr zuversichtlich, dass sich der Bestand in nächster Zeit wieder erholen wird.  
Eine Besonderheit dieser Pflanze ist nämlich, dass sie im Regelfall zweimal im Jahr blüht - "zufällig" immer dann, wenn an unserer Thaumaturgischen Hochschule für Magie die Vorlesung Algomantik und Datenkreaturen gehalten wird.  
Grund hierfür sind mit hoher Wahrscheinlichkeit die symbiotisch-magischen Eigenschaften der Pflanze: Wenn sich eine Immerwahrblume in der Nähe eines Magiers befindet und dieser einen korrekten, störungsfreien Zauberspruch vollführt, erblüht sie in schönster Form und Farbe. Falsche Gestenwiederholungen beispielsweise sind jedoch Gift für diese empfindlichen, kleinen Pflänzchen, denn wie ihr Name schon sagt, können sie nur in der Nähe von Zaubern leben, in denen sie immer wahr sind. Ist das nahelebende Immerwahr allerdings einmal am Anfang oder Ende einer Schleifenhaften Gestenwiederholung nicht wahr, muss es leider vor Scham eingehen. Der Botanomagiker in Ihnen möchte den armen Immerwahrs nun hoffentlich dieses Ende ersparen.*

*Darum soll in dieser Aufgabe Ihre Empathie zu dieser Pflanze geschult werden. Versetzen Sie sich bitte in die Rolle der Pflanze und sehen Sie sich den folgenden Zauberspruch genau an:*

*Form des Immerwahrs:* `q * b + r = a`

```
algorithm lassMichBlühen (a, b ∈ ℕ):
  q := 0
  r := a
  while r >= b
    q = q + 1
    r = r - b
  return r
```  

##### Aufgabe 1: Der Zauber

*Sie als kleines Immerwahr müssen erst einmal herausfinden, ob der Zauberspruch der für Sie richtige ist. Was bewirkt er?*

##### Aufgabe 2: Der Beweis

*Zeigen Sie, ob Sie blühen dürfen oder nicht.*

__Hinweise:__
> - *Um zu beweisen, dass sie tatsächlich blühen dürfen, sollten Sie Ihre Korrektheit vor der Ersten, vor der i-ten (i-1) sowie zur i-ten Gestenwiederholung prüfen.*

[Abgabe auf Gildamesh]()
