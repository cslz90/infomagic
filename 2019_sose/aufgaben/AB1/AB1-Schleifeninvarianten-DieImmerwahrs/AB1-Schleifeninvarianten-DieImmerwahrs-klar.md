#### Magische Naturkunde: Die Immerwahrs

Thema der Aufgabe sind Schleifeninvarianten und ihre Geltungsbereiche.
Sehen Sie sich den folgenden Pseudocode zum Algorithmus `lassMichBlühen` genau an:

```
algorithm lassMichBlühen (a, b ∈ ℕ ):
  q := 0
  r := a
  while r >= b
    q = q + 1
    r = r - b
  return r

```

Schleifeninvariante: `q * b + r = a`

##### Aufgabe 1: Der Zauber

Was macht der Algorithmus?
Warum ist die Schleifeninvariante sinnvoll?

##### Aufgabe 2: Der Beweis
Beweisen Sie, dass die folgende Schleifeninvariante für den Pseudocodealgorithmus `lassMichBlühen` gilt.

__Hinweis:__
> Prüfen Sie die Gültigkeit der Schleifeninvariante vor dem Ersten, vor dem i-ten (i-1) sowie zum i-ten Schleifendurchlauf.
