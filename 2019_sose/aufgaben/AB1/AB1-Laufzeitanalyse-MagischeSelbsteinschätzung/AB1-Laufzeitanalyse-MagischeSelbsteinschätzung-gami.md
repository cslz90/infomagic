# Magische Selbsteinschätzung

*Jeder Zauber verlangt eine Anzahl von bestimmten Gesten, für die der Magier ein gewisses Maß an Konzentration benötigt.
Das trifft sowohl auf den unerfahrenen Novizen, den erfahrenen Infomagister und sogar auf den Goldgorgonen zu.
Je länger ein Zauber wirkt, desto mehr muss sich der Magier konzentrieren.
Ist ein Magier der mentalen Belastung beim Wirken eines Zaubers nicht gewachsen, verlässt ihn das Bewusstsein. Mit etwas Ruhe und einem Stück Schokolade kann der Magier sich zwar erholen, eine solche Anstrengung zehrt dennoch an den Nerven.*

*Bevor man solch anstrengende Zauber wirkt, ist es also ratsam einzuschätzen, ob man den abverlangten Anforderungen gewachsen ist.*

*Um die Fähigkeit zu erlangen, die Komplexität eines beliebigen Zaubers einschätzen zu können, muss der Novize diese Fertigkeit anhand einfacher Zauber erproben. Mit genug Übung wird er dann in der Lage sein, auch komplexe Zaubersprüche zu meistern. Ziehen Sie dafür die Weisen von O zurate und erklären Sie kurz deren Entscheidung.*

*Ziemlich einfache Zaubersprüche sind zum Beispiel:*
1. *"sit numerus primis": Dieser Spruch ermöglicht es dem Magier herauszufinden, ob eine unmagische (natürliche) Zahl eine Primzahl ist.*

```
algorithm isPrime(n: int):
  if n < 2 then
    return false

  if n == 2 then
    return true

  for i := 2, 3, .. n - 1
    // i ist ein Teiler von n
    if n % i == 0 then
      return false

  return true
```
2. *"omnia primis": Der Zauberspruch findet alle Primzahlen in einem gewünschten Bereich der unmagischen (natürlichen) Zahlen.*

```
algorithm primesInRange(start: int, end: int):
  result = []
  for i := start, ..., end
    if isPrime(i) then
      result.add(i)
  return result
```

3. *"numerus quadratum": Der Spruch zum Quadrieren einer unmagischen (natürlichen) Zahl.*

```
algorithm square(n: int):
  return n * n
```

4. *"combinatoris sortere": Ein Zauberspruch zum Sortieren eines ArrayBasilisten, der unmagische (natürlichen) Zahlen gefressen hat.*
```
algorithm isSorted(list: [int])
  for i := 1, ..., list.length() - 1
    if list.get(i - 1) > list.get(i) then
      return false

  return true
```
```
algorithm permutationSort(list: [int])
  for each permut from permutations(list)
    if isSorted(permut) then
      return permut
```
Wobei es eine Funktion `permutations(list: [int]): [[int]]` gibt, die alle Permutationen einer Liste erzeugt. Zur Vereinfachung soll angenommen werden, dass die Komplexität des Aufrufs von `permutations(list)` O(1) ist.

*An diesen Zaubern sollen die Novizen abschätzen, ob sie der Dauer und der Anstrengung, die auf sie beim Wirken dieser mächtigen Zauber zukommt, gewachsen sind. Geben sie die benötigten Gesten für jeden Zauber in O-Notation an und begründen sie ihre Antwort in 2-3 Sätzen.*
