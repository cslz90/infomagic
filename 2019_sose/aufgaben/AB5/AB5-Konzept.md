# Aufgabenblatt 5

## Pflichtaufgaben

|Nr | Thema                   | Name                    | Dauer in Sdt (für Studis) | Punkte Gildamesh  | Ref           |
|---| ---                     | ---                     | ---                       | ---               | ---           |
|01 | Wegfindung mit Dijkstra | AB5-Dijkstra-Wegfindung |  Geschätzt:  2h  Probegelöst: 2h          | ?                 | Link einfügen |

## Bonusaufgaben

| Thema   | Name                                                   | Dauer in Sdt (für Studis) | Punkte Gildamesh | Ref            |
| ---     | ---                                                    | ---                       | ---              | ---            |
|Graphen | AB5-Bonus-Graphen-SchatzgoblinsImLabyrinthDerSchmerzen |                           | ?                |  Link einfügen |
|Graphen | AB5-Bonus-HashCode |             Geschätzt: 1h    Probegelöst ca. 1h          | ?                |  Link einfügen |
