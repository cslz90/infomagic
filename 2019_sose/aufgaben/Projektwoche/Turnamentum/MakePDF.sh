cat PW-Visualisierung-gami.md > Turnamentum_Imaginis.md
cat PW-Visualisierung-klar.md >> Turnamentum_Imaginis.md

pandoc Turnamentum_Imaginis.md -s -H css/markdown_pandoc.css -f markdown_github-hard_line_breaks --metadata pagetitle="Turnamentum Imaginis" -t html5 -o Turnamentum_Imaginis.pdf

rm Turnamentum_Imaginis.md
