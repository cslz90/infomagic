cat PW-Visualisierung-gami.md > Turnamentum_Imaginis.md
cat PW-Visualisierung-klar.md >> Turnamentum_Imaginis.md

pandoc Turnamentum_Imaginis.md -s -H css/markdown_pandoc.css -f markdown_github-hard_line_breaks --metadata pagetitle="Turnamentum Imaginis" -t html -o turnamentum-organization/Turnamentum_Imaginis.html
pandoc Turnamentum_Imaginis.md -s -H css/markdown_pandoc.css -f markdown_github-hard_line_breaks --metadata pagetitle="Turnamentum Imaginis" -t html5 -o turnamentum-organization/Turnamentum_Imaginis.pdf


mv -f Turnamentum_Imaginis.md turnamentum-organization/README.md
cp -r img turnamentum-organization
cd turnamentum-organization
git checkout master
git pull
git add .
git commit -m "Publishes new version of organisational documents."
git push

cd ..
