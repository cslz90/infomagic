### 📖 Spielregeln

Für die Projektwoche dürfen Sie freiwillig in einer Gruppe von 2-10 Studierenden eine Visualisierung für eine Datenstruktur oder einen Algorithmus erstellen. Dabei sollen Sie schrittweise grafisch darstellen, wie die Operationen der Datenstruktur bzw. der ausgewählte Algorithmus funktioniert. Die Visualisierung soll es einem interessierten Informatikstudenten oder einer Informatikstudentin des ersten Semesters ermöglichen, die Funktionsweise des gezeigten Konzepts zu verstehen.

Ob die Visualisierung interaktiv ist oder nicht, ob sie in einer Animation abläuft oder die einzelnen Schritte selbst durch das Klicken durch eine Bildergalerie nachvollzogen werden können, ist dabei ganz Ihnen überlassen. Sie dürfen sowohl bereits besprochene Konzepte visualisieren wie auch noch nicht besprochene Algorithmen oder Datenstrukturen. In letzterem Fall sollten Sie aber zusätzlich einen erklärenden Text mitliefern, der beschreibt, worum es sich bei dem gezeigten Konzept überhaupt handelt und wofür man es verwendet.

Die Anwesenheit in der Projektwoche ist dabei zwar empfohlen, aber keine Voraussetzung.
Jede Gruppe von Teilnehmern, welche die Projektwoche erfolgreich beendet (d.h. eine funktionierende Visualisierung vorweisen kann), erhält 3% Bonus für die Klausur. Ausnahme sind Sortieralgorithmen. Da diese mit wesentlich weniger Aufwand zu visualisieren sind, können wir darauf nur 2% Bonus geben.
Die beste Visualisierung wird in der Vorlesung präsentiert und die Gruppe erhält einen zusätzlichen Bonus Prozent in der Klausur. Ob Sie selbst die Präsentation übernehmen oder mich ihr Projekt vorstellen lassen können sie selbst entscheiden.

Für dieses Projekt müssen Sie mit [Gitlab](https://git.thm.de) arbeiten. Dabei handelt es sich um eine Projektverwaltungsplattform, die das Versionsverwaltungssystem *git* verwendet. Die meisten von Ihnen werden diese Tools schon aus PIS kennen. Falls nicht, hat die [Hilfeseite](https://git.thm.de/help/gitlab-basics/README.md) von Gitlab alle Informationen, die Sie brauchen. Denken Sie bitte auf jeden Fall daran, dass wir ihre Arbeitsweise nur nachvollziehen können, wenn sie *viele kleine Commits* machen.

Um eine "saubere" Struktur der Git-Repositories zu gewährleisten werden wir nachdem sie sich für das Turnamentum angemeldet haben ein Repository für sie erstellen und all ihre Teammitglieder in das Repository einfügen.

Damit dies reibungslos klappt muss einer von Ihnen sich per Mail bei mir unter [Felix.Muenscher@■■■■.■■](Felix.Muenscher@■■■■.■■) anmelden. Dazu schreiben sie mir bitte eine Mail mit dem Betreff `AuD Turnamentum`, die die Namen und Nutzerkennung aller Team-Mitglieder, sowie einen frei wählbaren Team-Namen, nach dem ihr Repository benannt wird, enthält. Bitte achten sie darauf, dass sich jedes Teammitglied sich zuvor mindesten einmal in [Gitlab](https://git.thm.de) eingeloggt hat, damit bereits ein User erstellt wurde.

#### Formalia

* Die "Abgabe" komplett auf `Git` erfolgt einzeln. In der `README.md` muss vermerkt sein, welche Bibliotheken verwendet werden, und wie das Programm compiled und gestartet werden muss.
* Eine Gruppe darf auch mehrere Datenstrukturen/Algorithmen visualisieren.
* Sie dürfen beliebige Programmiersprachen und Bibliotheken verwenden. (s.u.)
* Die Gruppengröße ist beschränkt auf 2-10 Studierende.
* Die Verwendung von [git](https://git.thm.de) ist Pflicht, damit wir nachvollziehen können, wer wie viel an dem Projekt gearbeitet hat.
  * Dazu zählt auch das regelmäßige Committen. Ein einziger großer Commit zählt nicht als arbeiten mit Git.
* Tragen sie in die README.md Datei in ihrem Repository die Namen aller Studenten der Gruppe ein.
* Das Projekt sollte mindestens etwa 10 Stunden Arbeitszeit pro Student oder Studentin entsprechen. Projekte, die vermuten lassen, dass deutlich weniger Zeit und Sorgfalt investiert wurde, können aus Fairnessgründen nicht akzeptiert werden.
* Man darf nur bei einer Gruppe mitarbeiten.
* Teilnehmer, welche die Projektwoche erfolgreich beenden, erhalten 3% Bonus für die Klausur.
  * Ausnahme sind Sortieralgorithmen. Da diese mit wesentlich weniger aufwändig zu visualisieren sind, können wir darauf nur 2% Bonus geben.
* Die beste Visualisierung wird in der Vorlesung präsentiert und die Gruppe erhält einen zusätzlichen Bonusprozent in der Klausur. Ob Sie selbst die Präsentation übernehmen oder mich ihr Projekt vorstellen lassen können sie selbst entscheiden.
* Eine Visualisierung muss die *Funktionsweise* der Datenstruktur bzw. des Algorithmus zeigen, nicht nur das Ergebnis.
* Das Projekt muss eine selbst geschriebene Implementierung der Datenstruktur bzw. des Algorithmus enthalten.

#### Unterlagen und Hilfestellung

##### Listener-Pattern

Wenn Sie die "Innereien" einer Datenstruktur oder eines Algorithmus beobachten wollen, bietet es sich an, im Code das sogenannte *Listener-Pattern* zu verwenden. Dabei wird ein *Listener* an die zu inspizierenden Methoden übergeben, der dann innerhalb der Methode jedes Mal aufgerufen wird, wenn etwas Interessantes passiert.

In Java würde eine Fakultätsmethode mit Listener, der jeweils auf die Teilergebnisse reagiert, zum Beispiel wie folgt aussehen:

```java
public interface FakListener {
    void reportResult(int i, long res);
}
public long fak(int n, FakListener l) {
    long res = 1;
    for(int i=2; i <= n; i++) {
        res *= i;
        l.reportResult(i, res);
    }
    return res;
}
```

Ein Aufruf von `fak` könnte dann (in Lambda Syntax) wie folgt aussehen:

```java
fak(10, (i, res) -> System.out.println(i+": "+res))`.
```

> Anmerkung: Die Fakultät zählt natürlich nicht als Algorithmus, mit dem man diese Aufgabe bestehen kann - es sei denn natürlich Sie bauen mit ihrer Gruppe eine Visualisierung für sehr viele kleine Algorithmen und `fak` ist ein kleiner Teil des Gesamtprojektes.

**Beispiele**

Ich habe für Sie ein paar Beispiele für die Visualisierung von Algorithmen und Datenstrukturen gesammelt, damit Sie eine Idee bekommen, wie so etwas aussehen könnte:

* Christopher Schölzels Code zur [Visualisierung des 4-Damen-Problems](https://git.thm.de/cslz90/4queens) auf Gitlab zur Verfügung gestellt.
* [15 Sorting Algorithms in 6 Minutes](https://www.youtube.com/watch?v=kPRA0W1kECg) ist ein sehr schönes Beispiel, das sowohl Bilder als auch Sound nutzt, um Sortieralgorithmen "erfahrbar" zu machen.
* [A* Visualisierung](https://www.youtube.com/watch?v=19h1g22hby8)
* [Quicksort](https://www.youtube.com/watch?v=aXXWXz5rF64) (Nein, sie müssen keine 3D-Roboter animieren. Ein paar einfache farbige Rechtecke würden den gleichen Effekt erzielen.)
* [Visualisierung des Genpools eines Genetischen Algorithmus](https://www.youtube.com/watch?v=_4Df9w1A-bA) (vermutlich mit *matplotlib*)
* [visualgo.net](https://visualgo.net/en) schießt den Vogel ziemlich ab mit ihren Visualisierungen. So kompliziert muss es bei Ihnen nicht werden. 😉

##### Sprachen und Bibliotheken zur Visualisierung

Sie sind in der Wahl der Programmiersprache und des Tools bzw. der Bibliothek für die Visualisierung völlig frei. Aus eigener Erfahrung kann ich Ihnen jedoch die folgenden Werkzeuge und Sprachen empfehlen:

* [JavaFX](https://en.wikipedia.org/wiki/JavaFX) ist ein modernes Toolkit für grafische Benutzeroberflächen in Java. Es enthält viele Klassen und Methoden, die bei der Erstellung von Bildern oder auch interaktiven Animationen nützlich sind.
* [Python](https://www.python.org/) bietet mit dem Paket [matplotlib](http://matplotlib.org/) eine exzellente Bibliothek für das Plotten von Daten, die auch Animationen beherrscht.
* [Project Jupyter](jupyter.org) erlaubt es sogenannte *Jupyter Notebooks* zu erstellen, mit denen man wunderbar Algorithmen erklären kann, da sie es erlauben, ausführbaren Code und formatierte Textpassagen in einem Dokument zu mischen. *Jupyter Notebooks* gibt es zum Beispiel für *Python*, *R*, *Julia* und sogar neuerdings [für Java](https://github.com/SpencerPark/IJava).
* [R](https://www.r-project.org/) ist eine Sprache für statistische Auswertungen. Sie hat unter anderem auch eine sehr ausgeklügelte Plotting-Bibliothek. Mit [Shiny](https://shiny.rstudio.com/) lassen sich außerdem diese Plots mit erschreckend wenig Aufwand als interaktive Webseiten darstellen.
* [Graphviz](http://www.graphviz.org/) ist ein sehr einfaches Konsolentool zur Darstellung von Bäumen und Graphen.
* [Processing](https://processing.org/) ist eine Sprache, die die gleiche Syntax wie Java verwendet, aber für interaktive grafische Simulationen entwickelt wurde.
* [Tikz](http://www.texample.net/tikz/) ist eine Variante für die ganz Verrückten. 😉 Es handelt sich dabei um eine Bibliothek für LaTeX, mit der man ebenfalls Bilder, Bäume und Graphen zeichnen kann. Die Beispiele auf Herrn Schölzels Folien sind mit Tikz und dem Paket [Beamer](https://www.sharelatex.com/learn/Beamer) erstellt.


P.S.: Aus Fairnessgründen sind auf dem Turnamentum Imaginis nur Standard-Zauberstäbe erlaubt. Der verbesserte Zauberstab kann also nicht dazu verwendet werden.
