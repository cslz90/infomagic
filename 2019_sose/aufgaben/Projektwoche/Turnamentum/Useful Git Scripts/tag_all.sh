#!/bin/sh
#Okd and Okay: ls | xargs -I{} git -C {} pull 
echo "Tagging all repos with Tag: $1";
echo "And message: $2"
echo "In 5 Seconds...";
echo "Ctrl + C to abort";
sleep 5;
for i in */.git; 
do (
	echo "Repository: $i"; 
	cd $i/..; 
	git pull; 
	git tag -a $1 -m "$2";
	git push --tags;
	echo "<=======================>" ); 
done

echo "Done";
