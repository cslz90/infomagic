# Turnamentum Imaginis / Visualisierung

Freiwillige Bonusaufgabe für die Projektwoche

Algorithmen und Datenstrukturen, Sommersemester 2019

Felix Münscher

<!-- Klausurpunkte: 3% -->

### 🐉 Vorwort:
 *Nur einmal im Semester, richten sich die Zwillingsplaneten __P__ und __NP__ für eine Woche in einer perfekten Linie zu unserer schönen Erde aus. Die Effizienzkomplexitätswellen, die bei diesem Schauspiel freigesetzt werden, lassen nicht nur seltene Immerwahrs im Dunkel erblühen, sondern zaubern auch bunte Lichter an den Himmel.*

![Nordlichter](img/northern-lights.png)

*Da die meisten Infomagier sowieso nachtaktiv sind, veranstaltet die Thaumaturgische Hochschule für Magie in dieser Woche traditionsgemäß das __Turnamentum Imaginins__ - ein Wettbewerb bei dem die verschiedenen Magiergilden und Häuser gegeneinander antreten, um zu bestimmen, wer mit den bunten Lichtern die schönsten algomantischen Formeln oder Datenkreaturen an den Nachthimmel zaubern kann.*

*Wenn Sie auch in diesem Semester an dem Wettbewerb teilnehmen wollen, dann lesen Sie sich die folgenden Teilnahmeregeln bitte genau durch.*
