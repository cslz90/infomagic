# AuD - Ehrenkodex

Der AuD-Ehrenkodex stellt Regeln für aufrichtiges akademisches Verhalten für Studierende und Hochschullehrer auf. Er ist dem [Vorbild der Uni Stanford](https://web.stanford.edu/class/archive/cs/cs106b/cs106b.1164//handouts/honor-code.pdf) nachempfunden.

## Der Ehrenkodex

1. Wir, die AuD-Studierenden, verpflichten uns individuell und als Gruppe:
    1. Übungsaufgaben und Prüfungen selbstständig zu lösen - denn auf dem Weg zu echtem Verständnis gibt es keine Abkürzungen.
    2. anderen Studierenden nur zu helfen, wenn es dem *Ver*stehen dient und nicht allein dem *Be*stehen - denn ein Schein für etwas, das man nicht beherrscht, hilft niemandem etwas.
    3. keine fremden Leistungen als eigene darzustellen - denn das schadet auch dem ursprünglichen Autor.
    4. erhaltene Hilfe bei Übungsaufgaben offen darzulegen - denn Zitate sind keine Plagiate und würdigen stattdessen das Original.
2. Die Dozent\*innen und Tutor\*innen der Veranstaltung verpflichten sich,
    1. Verstöße gegen den Ehrenkodex zu verfolgen, damit diejenigen, die sich an ihn halten, nicht im Nachteil sind.
    2. den Studierenden zu vertrauen und keine unnötig restriktiven Maßnahmen (wie z.B. das Lösen von Aufgaben unter Aufsicht) anzuwenden.
    3. keine Aufgaben und Verfahren zu verwenden, die dazu verleiten, gegen den Ehrenkodex zu verstoßen.

## Regeln für AuD

### Vorwort

All diese Regeln beziehen sich auf Aufgaben, die in Einzelarbeit gelöst werden sollen und deren Frist noch nicht abgelaufen ist. Bei Gruppenaufgaben gelten die Regeln entsprechend für die Gruppe, so wie sie für den Einzelnen formuliert sind. Nach Ablauf der Einreichungsfrist, können Sie selbstverständlich Ihre Lösungen vergleichen.

### Regel 1: Teilen Sie keine Lösungen mit Kommiliton\*innen

Schreiben Sie Ihre Lösung an einem eigenen Rechner und lassen Sie niemanden in den Code oder Text schauen. Verschicken Sie keine Lösungen per Mail oder sozialen Netzwerken und fragen Sie niemanden nach deren Lösung. Das gilt auch, wenn nur Teile einer Lösung betroffen sind.

### Regel 2: Schauen Sie nicht auf den Code von anderen

Fertigen Code zu verstehen und anzupassen ist leicht, aber nur, wer eigenen Code schreibt, lernt dabei etwas. Schicken Sie zum Beispiel auch niemanden Ihren Code, um zu fragen, wo der Fehler herkommt, den Sie gerade haben und fragen Sie jemanden, dem Sie helfen wollen, nicht nach dem problematischen Codestück.

### Regel 3: Schauen Sie nicht auf fertige Lösungen im Internet

Für Lösungen aus dem Internet gilt das gleiche wie für Lösungen anderer Kommiliton*innen. Am Besten vermeiden Sie Verstöße gegen den Ehrenkodex, indem Sie gar nicht erst nach einer Lösung googeln.
In der Regel lassen sich alle Aufgaben, die nicht explizit als Recherche gekennzeichnet sind, auch mit den Informationen aus der Vorlesung und der Java-API lösen.
Wenn Sie dennoch zu einem Thema mehr Informationen brauchen, dann suchen Sie nach dem allgemeinen Thema, das Ihnen Schwierigkeiten bereitet und nicht nach der direkten Aufgabenstellung (zum Beispiel "Java generics" statt "Java generic ArrayList").

### Regel 4: Geben Sie die Quellen für alle Teile einer Lösung an, die nicht von Ihnen selbst stammen

In der Informatik ist es üblich, für kleinere Teillösungen Code von anderen zu verwenden oder Codeschnippsel aus dem Internet zu kopieren. Dies geschieht aber immer unter einer genauen Angabe der Quelle. Bei Übungsaufgaben in diesem Modul sollte idealerweise gar kein Code kopiert sein, weil es eben darum geht, zu lernen, wie Sie die Probleme *selbst* lösen können. Ist Ihnen das an einer kleinen Stelle aber partout nicht möglich, dann geben Sie den Namen und/oder Link zu dem Original, bei dem Sie sich bedienen mussten, an. Eine solche Lösung kann unter Umständen als nicht Bestanden gewertet werden, wenn nicht genug Eigenleistung erkennbar ist. Sie führt aber in keinem Fall zu einer Verletzung des Ehrenkodex.

Hier ein Beispiel für ein Code-Zitat in LaTeX:
```tex
% patch \begin{frame} to reset the footline extra material
% Author: Matthew Leingang
% Source: http://tex.stackexchange.com/questions/5491/how-do-i-insert-text-into-the-footline-of-a-specific-slide-in-beamer
\makeatletter
\let\beamer@original@frame=\frame
\def\frame{\gdef\insertextrafoot{}\beamer@original@frame}
\makeatother
```

### Regel 5: Geben Sie *langfristige* Hilfe, auch wenn es weh tut

Wie schon erwähnt: Es hilft niemandem, einen Schein zu bekommen für etwas, das er oder sie nicht verstanden hat.
Sollte ein\*e Kommiliton\*in sich hilfesuchend an Sie wenden, überlegen Sie, was dieser Person *wirklich* weiterhelfen würde.
Meistens ist das vermutlich ein gemeinsames Durchsprechen der Aufgabe und eine Hilfestellung beim Erarbeiten einer eigenen Lösung.
Was aber, wenn dafür keine Zeit ist, oder auch das nicht für das Verstehen reicht?

Ist es dann kollegial, eine funktionierende Lösung weiterzureichen?
Oder würde es der Person vielleicht viel mehr helfen, zu hören, dass es in Ordnung ist, wenn ein Studium mal ein oder zwei Semester länger dauert, weil man ein Modul wiederholen muss?
Oder ist vielleicht sogar die wichtigste Frage, ob das Informatikstudium diejenige\*n wirklich glücklich macht?
Wenn es in einer *grundlegenden* Informatikveranstaltung hapert, ist das vielleicht ein Zeichen dafür, dass Informatik nicht das richtige für die Person ist.
Vielleicht gehen an solchen Personen großartige Germanist\*innen, Fachinformatiker\*innen oder Florist\*innen verloren, wenn Sie nicht einmal sensibel nachhaken.

## Ahndung und Erkennung von Verstößen

### Hinweis: Ihr Code wird automatisch auf Plagiate geprüft

In diesem Kurs werden Ihre Codeabgaben automatisch auf Plagiate geprüft.
Dabei handelt es sich um eine strukturelle Prüfung, die auch Variablenumbenennungen und ähnliche kleine Änderungen durchschaut.
Wenn die Software uns auffällige Übereinstimmungen meldet, werden wir den Fall noch einmal persönlich analysieren und Sie gegebenenfalls zu einem Plagiatsgespräch einladen.
Das gilt selbstverständlich nicht in den Fällen, in denen eine Übereinstimmung daher stammt, dass beide Studierenden sich an entsprechende Vorlagen aus der Vorlesung oder der Aufgabenstellung gehalten haben.

### Plagiatsgespräch

Ein Plagiatsgespräch dient dazu in unklaren Fällen festzustellen, ob eine Lösung ein Plagiat ist und wer der ursprüngliche Autor ist. Das Gespräch findet immer mit dem Dozenten und mindestens einem Tutor statt und wird schriftlich dokumentiert.

### Ahndung von Verstößen

Ein Verstoß gegen den Ehrenkodex führt in aller Regel zum sofortigen und unwiderruflichen Nichtbestehen des dazugehörigen Aufgabenblattes. Außerdem wird ein Verweis in der Studierendenakte hinterlegt, um wiederholte Verstöße erkennen zu können.
Solche wiederholten Verstöße werden dann strenger geahndet und können in Extremfällen bis zur Zwangsexmatrikulation führen. Mehr dazu finden Sie auch im [Plagiatskompass der THM](https://www.thm.de/site/images/stories/PA/Download/Plagiatskompass.pdf).

## Schlusswort: Zusammenarbeit ist erwünscht!

Bei diesen Regeln kann der Eindruck aufkommen, Zusammenarbeit bei Übungsaufgaben wäre generell unerwünscht. Dem ist absolut nicht so. Sie sollen definitiv zusammen über die Aufgaben und auch Ihre Lösungen reden - nur eben nicht, indem Sie sich einfach gegenseitig Ihre Lösungen zeigen, sondern indem Sie über die damit verbundenen Konzepte, mögliche Sonderfälle oder allgemeine Designentscheidungen bei der Codestruktur diskutieren.
Auch Fehlergründe lassen sich sehr gut durch systematisches Nachfragen finden, ohne dafür in den Code schauen zu müssen. ("Fehlt irgendwo ein Semikolon?", "Bei welchem Index beginnt die Schleife, bei welchem endet sie?", ...)

Wenn Sie dennoch jemanden brauchen, der einen Blick in Ihren Code wirft, können Sie sich außerdem jederzeit an die Tutoren in der Übung und im Tutorium oder auch an den MNI-Helpdesk wenden.