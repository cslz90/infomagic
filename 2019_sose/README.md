# Algorithmen und Datenstrukturen 2019
Christopher Schölzel

## Überblick

In diesem Verzeichnis finden Sie alle Unterlagen, die ich für das Modul "Algorithmen und Datenstrukturen" zusammengestellt habe.

* `anleitungen`: Anleitungen für die Tutor*innen zum Erstellen von Aufgaben
* `aufgaben`: zum Teil zwischen den Semestern überarbeitete Versionen der Aufgaben aus 2018 mit Gamification-Elementen (nicht für dieses Semester verwendet)
* `entGamiAufgaben`: Aufgabentexte für Moodle oder unser Hauseigenes e-Learning-System Dozentron (ohne Gamification im Aufgabentext)
* `entGamiAufgaben/code`: Unittests und Musterlösungen für die Programmieraufgaben in `aufgaben`.
* `folien`: LaTeX-Folien für die Veranstaltung
* `klausur`: Klausur und Probeklausuren als LaTeX-Dokument
* `konzept`: Konzeptdateien für die Planung der Veranstaltung
* `uebungen`: Protokoll der Übungsstunden für die Studierenden
* `generated`: Fertige PDFs der Folien
* `include/texmf-cs`: Eigene LaTeX-Templates und -Makros, die zum Übersetzen der Folien benötigt werden
* `ad-cs/2019_ss/uebungen/konzept/2019-04_11_15-Plagiate/AuD_Honor_Code.md`: "Ehrenkodex" nach Vorbild der Uni Stanford, auf den sich Studierende und Dozent geeinigt haben, um Plagiate zu vermeiden

Bitte beachten Sie, dass nicht alle Aufgaben in `entGamiAufgaben` fertiggestellt sind und daher auch nicht zu allen Aufgaben entsprechender Code in `entGamiAufgaben/code` existiert.
Der Ordner `aufgaben` wurde in diesem Semester gar nicht aktiv verwendet, enthält aber leicht aktualisierte Versionen der gamifizierten Aufgaben aus dem Vorsemester, die bei einer weiteren gamifizierten Version der Veranstaltung Anwendung finden könnten.

Welche Aufgaben als Pflicht und welche als Bonus eingeplant waren sehen Sie in der jeweiligen Konzeptdatei `entGamiAufgaben/ABX/ABX-Konzept.md`.

## Autor\*innen

Die Inhalte in dem Ordner `aufgaben` (und zum Teil auch in `anleitungen`) wurden größtenteils von meinen Tutor\*innen erstellt. In diesem Semester waren das die folgenden Personen:

* Anna-Lena Kobiela
* Björn Pfarr
* Clara Ifland
* Felix Brübach
* Felix Münscher
* Frederic Schwieren
* Gian Saß
* Kai Bastian Badde
* Marius Grebe
* Niels Korschinsky
* Pia-Doreen Ritzke
* Sebastian Engel

Viele Inhalte wurden außerdem vom Vorsemester übernommen und verbessert. Die Autor\*innen der Originale finden Sie jeweils in der README-Datei des dazugehörigen Semesters.

## Übersetzung der Inhalte

### LaTeX-Dokumente

Die LaTeX-Dokumente in diesem Ordner habe ich zu beginn des Semesters noch mit Pdflatex geschrieben, später dann aber mit Lualatex.
Ich empfehle einfach für alle Dokumente Lualatex zu verwenden.
Unabhängig von dem verwendeten Befehl ist es nötig, die LaTeX-Dateien in `includes/texmf-cs` einzubinden.
Jede LaTeX-Distribution bietet Möglichkeiten, solche lokalen texmf-Verzeichnisse einzubinden.
Wer sich damit aber nicht herumschlagen möchte, kann einfach die Umgebungsvariable `TEXINPUTS` wie folgt verwenden:

```bash
BASEDIR=../include/texmf-cs/tex/latex
export TEXINPUTS=.:$BASEDIR/base:$BASEDIR/listings:$BASEDIR/beamer/custom/themes/csthm:
lualatex -interaction=nonstopmode NAME_DER_TEX_DATEI
```

Das Beispiel ist für ein Linux-System geschrieben.
Unter Windows lässt sich natürlich das gleiche mit einem Bash-Script erreichen.

Um die Arbeit für meine Tutor\*innen zu erleichtern habe ich auch bereits ein Script in `folien/lualatex-texinputs.sh` erstellt, das genau diese Aufgabe übernimmt, so dass man einfach den Befehl `./lualatex-texinputs DATEINAME` verwenden kann.

### Markdown-Dokumente

Die Markdown-Dokumente (Endung `.md`), die den Studierenden zugänglich sein sollten wurden mit [Pandoc](https://pandoc.org/) in statische HTML-Dokumente übersetzt.
Dazu muss man den folgenden Befehl in dem Ordner, in dem sich auch diese Datei befindet, ausführen:

```
pandoc -s --mathjax -H konzept/css/markdown_css_pandoc.html -f markdown_github-hard_line_breaks+tex_math_dollars -t html NAME.md -o NAME.html
```

Dabei steht `NAME` für den Namen der Markdown-Datei (ohne Endung), die übersetzt werden soll.

## Lizenzen

Alle Bilder, Quellcodes, Texte und andere Materialien, die von mir oder meinen Tutor\*innen für dieses Modul erstellt wurden entlasse ich im Rahmen der [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)-Lizenz in die Gemeinfreiheit.

Dies gilt für alle Dateien in dem Ordner in dem sich diese Datei befindet sowie den entsprechenden Unterordnern.
Dateien, die Werke von Dritten enthalten und unter einer anderen Lizenz stehen sind im folgenden aufgelistet.
Wenn LaTeX-Dateien Code von Dritten enthalten, ist das innerhalb der Datei gekennzeichnet.

### Pixabay-Bilder: [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)

Einige Bilder auf den Folien stammen von [Pixabay](https://pixabay.com/de/) und wurden bereits unter der CC0 veröffentlicht.
Da diese damit gleich behandelt werden können wie meine Werke und die meiner Tutor\*innen sind sie nicht im einzelnen aufgelistet.

### Illustrationen von Julia Jelitzki: [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)

Die handgezeichneten Basilisten, Speichereichen und Knotlinge stammen aus der Feder meiner Tutorin Julia Jelitzki, die diese unter die Lizenz CC-BY 4.0 gestellt hat. Dazu zählen die folgenden Bilder:

* `folien/img/ArrayBasilist_*.*`
* `folien/img/AdjacencyMatrix.png`
* `folien/img/binary_tree.pdf`
* `folien/img/DisjointSet.pdf`
* `folien/img/Graph.png`
* `folien/img/GraphDirected.png`
* `folien/img/ìddfs4.pdf`
* `folien/img/Kruskal.pdf`
* `folien/img/LinkedList*.*`
* `folien/img/RunzligeRanunkula2.png`
* `folien/img/Schlafbaum.pdf`
* `folien/img/Sorted_Array.jpg`
* `aufgaben/AB4/AB4-GraphZyklen/Knotlinge.png`
* `klausur/img/scapegoatTransparent.png`
* `klausur/img/SingeltonTransparentSm.png`
* `folien/img/1Washater_1024.png`
* `folien/img/2rechnen_1024.png`
* `folien/img/4hütchen_1024.png`
* `folien/img/BubbleSort.png`
* `folien/img/hashlistpng_1024.png`
* `folien/img/Knotlingspost.png`
* `folien/img/LinkedNeu_2048.jpg`
* `folien/img/Ranunkula_1024.png`
* `folien/img/Ranunkula_1024_binary.png`
* `folien/img/scapegoat*.*`
* `folien/img/Setzling2.png`
* `folien/img/stream_*.jpg`
* `ArrayBasilist_1024_shadow.png`

### Fxemoji: [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)

Alle Bilder mit dem Namensschema `uXXXXX-name.pdf` stammen von [Fxemoji](https://github.com/mozilla/fxemoji).

### Twitter Emoji: [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)

The Bird emoji `folien/img/twemoji_1f426_bird.pdf` stammt von [Twemoji](https://twemoji.twitter.com/content/twemoji-twitter/en.html).

### Andere Bilder

* `folien/img/Karte_V1_5.jpg` und `konzept/Karte_V1_5.jpg`: (c) [WorldAnvil](https://www.worldanvil.com)
* `folien/img/Christopher_gamified.png`: (c) [Face Your Manga](https://www.faceyourmanga.com/)
* `folien/img/419px-2kg_Gewicht.jpg`: [CC-By-sa 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en), [LoKiLeCh, Wikimedia](https://commons.wikimedia.org/wiki/File:2kg_Gewicht_freigeschnitten.jpg).
* `folien/img/snake-tree.jpg`: (c) [Strangehistory.net](http://www.strangehistory.net/2015/10/11/the-snake-tree/)
* `folien/img/moodle-logo.pdf`: (c) [Moodle Pty Ltd](https://moodle.org/mod/page/view.php?id=8338&forceview=1)
* `include/texmf-cs/tex/latex/base/img/MNI-logo.pdf`: (c) [Technische Hochschule Mittelhessen](https://www.thm.de)
* `folien/img/Bairal.png`: Screenshot aus [Lineage II](https://www.lineage2.com/), (c) NCSOFT
* `folien/img/Parsertongue.png`: (c) Ryan Sawyer ([@EightballArt](https://twitter.com/EightballArt/status/515195030546690048)) (für webs.com)
* `folien/img/THM_gamified.pdf`: THM-Logo (c) [Technische Hochschule Mittelhessen](https://www.thm.de)
* `folien/img/Aminoacids_table.pdf`: [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) (Mouagip, Wikimedia)

## Danksagungen

Das Projekt wäre in dieser Größe nicht ohne weitere Unterstützung möglich gewesen.

Die Finanzierung und Qualifikation der Tutor\*innen und die didaktische Beratung lief über das Projekt Klasse in der Masse (KIM) unter der Leitung von Gisa von Marcard an unserem Zentrum für kooperatives Lehren und Lernen (ZekoLL).
Hier möchte ich insbesondere Marion Heiser, Meike Hölscher, Victoria Ribel-Sencan und Katrin Weigand danken.

Für die restliche Finanzierung der Tutor\*innen bedanke ich mich beim Dekanat des Fachbereichs MNI, insbesondere dem Studiendekan Prof. Dworschak.

Auch für die Inhalte der Vorlesung habe ich mich frei an den mentalen und zeitlichen Ressourcen meiner Kolleg\*innen bedient.
Hier möchte ich Prof. Letschert, Prof. Gogol-Döring, Prof. Dominik und Prof. Just hervorheben.
Außerdem bedanke ich mich auch bei allen Studierenden und Mitarbeitern, die während der Veranstaltungsplanung in meinem Büro waren und nicht schnell genug vor meinen Fragen weggelaufen sind.

In diesem Semester habe ich es zum ersten Mal geschafft, wirklich nur 50 Prozent meiner Arbeitstage in die Veranstaltung zu investieren und in der restlichen Zeit meine Promotion voranzutreiben.
Dafür dass das möglich war, ohne dass die Qualität der Veranstaltung darunter gelitten hat, mache ich maßgeblich die Geduld, Beratung und Unterstützung meiner Lebensgefährtin Annina Hofferberth verantwortlich, die mir auch bei der Veranstaltungsplanung geholfen hat.

Last but not least möchte ich auch wieder meinen Studierenden danken, die mit ihrem Engagement und ihrem umfangreichen und unermüdlichen konstruktiven Feedback maßgeblich zum Erfolg der Veranstaltung beigetragen und mir viele gute Ansätze für weitere Verbesserungen gegeben haben.
