Betreff: Globales Kuriosum: Stakk-Overflow

*Das Feedback der Novizen war in der letzten Veranstaltung so umfangreich, ehrlich und konstruktiv, dass der Stakk dieses nicht mehr völlig fassen konnte und dadurch einen Riss bekommen hat.*

*In Fachkreisen nennt man dieses Phänomen einen "Stakk-Overflow".
Dabei wird ein Teil der mystischen Energien frei, mit denen der Stakk vor Urzeiten geschmiedet wurde.
Diese manifestieren sich grundsätzlich bei allen Novizen im Umkreis in spontanen Geistesblitzen im Wert von einem Erfahrungspunkt.*

📖 Klartext: Als Dank für Ihr umfangreiches Feedback bekommt der gesamte Kurs einen XP gutgeschrieben.