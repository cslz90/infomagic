# Vorlesung

2 Blöcke pro Woche

Themen:

* Einleitung
    - ArrayList in Java
    - LinkedList in Haskell (+ Optional in Java?)
    - O-Notation
* Algor. Techniken
    - Brute Force
    - Gier
        + **neu: Wünschelrute weist zum tiefsten Punkt, wo man zu buddeln beginnt**
    - Backtracking
    - Teile und herrsche
    - Dynamische Programmierung
* Datenstrukturen
    - Sequentielle Datentypen
        + Stack
        + Queue
        + Liste
        + Iterator
        + Realisierungen als ArrayList oder LinkedList
        + Sortieralgorithmen (Selection, Bubble, Merge)
    - **neu: Streams**
    - Bäume
        + Menge
        + Baum
        + Traversierung (DFS, BFS, IDDFS, pre, in, post)
        + Binäre Suche
        + Suchbaum
        + Suchbaum als Array
        + Heap
        + **neu: keine Spielbäume, kein Minimax**
        + **neu: Scapegoat-Tree als beispiel für balancierte Binärbäume**
    - Abbildungen
        + Abbildung
        + Hashtabelle
    - Graphen
        + Dijkstra
        + Prioritätswarteschlangen
        + Kruskal
        + Disjunkte Mengen

## Pseudocode

**neu: Pseudocode bekommt einen eher objektorientierten Stil**

* `type` und `structure` mit Vererbung (`acts_as` und `is_a`)
* Algorithmus in einem `type` oder `structure` heißt `operation`

# Hausübungen

2 Hausübungen aufgeteilt auf 6 Arbeitsblätter

* AB0: OOP Auffrischung
    - **neu: HelloWorld-Abgabe auf Dozentron in Woche 1**
* AB1: Einleitung (O-Notation!)
* AB2: Algor. Techniken
* AB3: Stack, Queue, Liste, Iterator
* AB4: Bäume
* AB5: Abbildungen, Graphen

# Übung

1 Block pro Woche

Abwechselnd:

* Vorbereitung auf nächstes Aufgabenblatt
* Besprechung des vorherigen Aufgabenblatts

**neu: Plagiatskurs in der ersten Übung**

* Wann wird Zusammenarbeit zum Plagiat?
* Welche Folgen hat ein Plagiat?
* Beispiele aus dem vorherigen Semester
* Möglichkeiten, um Plagiate zu vermeiden
    - Zitat im Code
    - Absprache über Strategie, nicht über konkreten Code

# Tutorium

**neu: Das gab es vorher überhaupt nicht**

1 Block pro Woche

Ziel: Grundlagen einhämmern, bis es (nicht mehr) weh tut

Besetzung: Zwei Tutoren a 20 Monatsstunden

Voraussetzung:

Ein AB mit > 0% aber < 70% abgegeben
oder kleinen Skill dafür gekauft

* Soll Verbindlichkeit fördern
* Reduktion auf diejenigen, die es wirklich brauchen/wollen
* Belohnt auch fehlgeschlagene Versuche

Inhalte:

* Nochmaliges Erklären/Wiederholen der Vorlesungsinhalte
* Gemeinsames Lösen von Beispielaufgaben
* Unterstützung bei Aufgabenblättern
* Richtlinie: Der Vorlesung immer 1-2 Wochen hinterherhinken
    - angepasst an Bedürfnis der Besucher
    - notfalls auch in Woche 6 nochmal die O-Notation erklären

# Gamification-System

## Grundlagen

10 HP, -4 HP pro Aufgabenblatt
**neu: Evtl. durch verspätete erneute Abgabe 2 HP wieder zurückgewinnen?**

XP werden zu Bonuspunkten verrechnet

Skills können für XP gekauft werden

## Zusatzaufgaben

Alte Aufgaben werden beibehalten, neue werden hinzugefügt

**neu: Mehr Übungen, die:**

* Recherche statt Programmieren verlangen
* Weiterführende Konzepte beibringen (z.B. AVL-Trees)

**neu: keine XP mehr für Meldungen, höchstens als spezielles Event**

**neu: Aufgaben für Gilden und Parties**

**neu: Turnamentum Imaginis mit Infografik/Poster?**

## Crowdsourcing

Ziel: Aufgabenideen von Studierenden sammeln.

**neu: Konkrete Ideen pro Woche beisteuern, damit Studis nicht so in der Luft hängen (z.B. für implementierbare Algos)**
**neu: Mit Fabian konkret über Ideen reden, wie man das anregen kann**
    * Fabian hat Vertreter

## Skills

Weitestgehend beibehalten

**neu: Eventuell flachere Hierarchie**
**neu: Eventuell Spaßskills entkoppeln/reduzieren**

**neu: Spickzettel ist an Gesamt-XP des Kurses gekoppelt und gilt für alle**
**neu: Hard-Mode-Skills (z.B.: wandle HP in XP um oder Skills die HP kosten)**
**neu: Easy-Mode-Skills (z.B.: AB schon bei 35% bestanden, aber nur 50% XP, oder HP durch Aufgabe zurückerhalten)**
**neu: XP fürs erste Anmelden auf Gildamesh**
**neu: XP-Spende (sehr gute Studis dürfen XP an Kommilitonen abgeben)**
**neu: Skills werden erst nach der Bewertung von AB1 eingeführt**

## Story

**neu: World-Events erzählen eine Geschichte**

* Abhängig davon, wie die Vorlesung/Übung/Etc. läuft

# Tutorenaufwand

* 2 * 20h für Tutorium (Felix, Markus?, Gianna?)
    - Felix (20)
    - Gianna (20)
* 1 * 40h für Qualitätssicherung (Markus?)
    - Markus (20)
    - Sebastian S. (20)
* 1 * 20h fürs Probelösen
    - Laszlo (20)
* 2 * 40h+ für Programmierung an Dozentron (Nico + Pascal?)
    - Pascal (40)
* 2 * 40h+ für Programmierung an Gildamesh (Marcel + Gianna, Felix?)
    - Felix (30+)
    - Sebastian S. (20)
    - Gianna (10)
    - Laszlo (10)
* 4 * 30h für Abgabenkorrekturen (Sebastian Engel, Vincent, Laszlo, Tobias)
    - Kristina (30)
    - Elias (30)
    - Sebastian E. (20)
    - Sebastian W. (30)
* 2 * 40h für neue Aufgaben (Dominik Fahlenberg, Sebastian Sommer)
    - Tim (20)
    - Dominik (80)
* 1 * 30h für Illustrationen + Texte überarbeiten (Julia)
    - Julia (32)

# Punktesystem

* Fleißige Studenten arbeiten 8 h/Woche
  - Modulhandbuch sieht 90 Stunden Arbeit zu Hause vor
  - Semester hat 13 Wochen, A&D fällt aber in 3 davon aus
  - => zwischen 90/10 = 9 u. 90/13 ~= 7 Stunden Arbeit pro Woche
* Das Aufgabenblatt soll pro Woche maximal 4h kosten, also insgesamt 8h
  - ein fleißiger Student sollte maximal die Hälfte seiner Zeit am Aufgabenblatt sitzen
* Für freiwillige Aufgaben sind 4h pro Woche vorgesehen
* Eine freiwillige Aufgabe gibt ein XP pro 4 min Arbeit
* Ein Aufgabenblatt gibt 64 XP
  - ca. ein XP pro 8 min Arbeit
  - freiwillige Aufgaben sollen sich mehr lohnen als Pflichtaufgaben
* Ein fleißiger Student erhält also pro Woche ca. 90 XP
  - 32 XP von einer Hälfte des Aufgabenblattes
  - 60 XP von 4 * 60 min / 4 min/XP freiwilligen Aufgaben
* Alle 64 XP steigt man ein Level auf
  - wer nur das Minimum macht (Aufgabenblätter), steigt alle zwei Wochen auf
  - wer alles macht, steigt jede Woche 1,5 Level auf
* Level 14 ist das voraussichtliche "Maximallevel"
  - 90 XP * 10 Wochen / 64 XP 

# Kurslevel

Gesamt-XP im letzten Jahr: 58244 XP für 185 auf Gildamesh gemeldete Studenten
=> Etwa 317 XP pro Student im Schnitt => Level 5

Dieses Semester erwarte ich etwas mehr XP im Durchschnitt. Wir haben zur Zeit 196 gemeldete Studenten in Gildamesh.
Annahme: 400 XP pro Studi sind machbar => DIN A4 gibt es bei 400 XP

=> Grob geschätzt 200 * 400 = 80000 XP auf max. Level
=> Level 12 bei 6400 XP pro Level

|Level|XP|Belohnung|
|---|---|---|
|1|6400| Post-It des Herumtreibers (DIN A9 Spickzettel)|
|2|12800| Visitenkarte des Herumtreibers (DIN A8 Spickzettel)|
|3|19200| Karteikarte des Herumtreibers (DIN A7 Spickzettel)|
|4|25600| Holz-Zeitumkehrer (+ 1 min Bearbeitungszeit) |
|5|32000| Flyer des Herumtreibers (DIN A6 Spickzettel)|
|6|38400| Bronze-Zeitumkehrer (+ 2 min Bearbeitungszeit) |
|7|44800| Silber-Zeitumkehrer (+ 3 min Bearbeitungszeit) |
|8|51200| Notizzettel des Herumtreibers (DIN A5 Spickzettel)|
|9|57600| Gold-Zeitumkehrer (+ 4 min Bearbeitungszeit) |
|10|64000| Platin-Zeitumkehrer (+ 5 min Bearbeitungszeit) |
|11|70400| Diamant-Zeitumkehrer (+ 6 min Bearbeitungszeit) |
|12|76800| Karte des Herumtreibers (DIN A4 Spickzettel)|
