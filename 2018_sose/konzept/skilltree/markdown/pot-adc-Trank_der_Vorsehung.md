# Trank der Vorsehung

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Der Dozent muss definitiv eine Klausurfrage zu dem gewählten Foliensatz stellen.

Geben Sie bitte beim Erlernen dieser Fähigkeit die Nummer des Foliensatzes an, von dem Sie auf jeden Fall eine Aufgabe in der Klausur haben möchten.

---

* Voraussetzung: Trank der Klausurvisionen
* Kosten: 160 XP
* Cooldown: passiv/365 Tage
* Anwendungen nötig: 30
