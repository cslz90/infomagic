# Mantel des Medicus

Jedes Mal, wenn Sie einen ihrer Kommilitonen mit einer anderen Fähigkeit heilen, können Sie diese Fähigkeit verwenden, um selbst ein paar zusätzliche XP zu erhalten.

Geben Sie bei der Anwendung dieser Fähigkeit bitte den Namen der Heilfähigkeit an, für deren Anwendung Sie XP bekommen wollen.

---

* Kosten: 64 XP
* Cooldown: 7 Tage
* Effekte
  * Nachrichteneffekt: Name der Heilfähigkeit
  * \+ 10 XP