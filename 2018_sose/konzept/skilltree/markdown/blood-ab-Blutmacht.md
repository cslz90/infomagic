# Blutmacht

*Sie entfachen die verborgene Macht in ihrem Blut und verstärken damit Ihre bereits erlernten Fähigkeiten.*

Jede Anwendung dieser Fähigkeit verdoppelt die Effekte der Anwendung einer anderen Fähigkeit.

Geben Sie bei der Aktivierung den Namen der Fähigkeit, deren Effekte sie verdoppeln wollen an.

---

* Kosten: 256 XP
* Cooldown: 14 Tage
* Effekte:
  * Nachrichteneffekt: Name der anderen Fähigkeit