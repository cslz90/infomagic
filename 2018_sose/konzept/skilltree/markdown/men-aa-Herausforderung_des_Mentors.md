# Herausforderung des Mentors

Sie können einem Kommilitonen (das musss nicht ihr Schützling sein) herausfordern, eine der Bonusaufgaben aus dem Kurs zu lösen.
Sie bezahlen für die Herausforderung mit Erfahrungspunkten, die Ihr Kommilitone dann zusätzlich angerechnet bekommt, sollte er die Aufgabe tatsächlich lösen.

Geben Sie bei der Anwendung dieser Fähigkeit bitte den Namen des Kommilitonen und den Namen der Aufgabe an, die Sie ihm stellen wollen.

**Achtung:** Die XP, die Sie bezahlen können auch ihr Level senken. Dieser Skill ist nur für Studierende gedacht, die zu viel XP gesammelt haben und diese weitergeben wollen.

---

* Kosten: 128 XP
* Cooldown: 0 Tage
* Effekte:
  * \- 20 XP
  * Nachrichteneffekt: Name von Kommilitone und Aufgabe
