# Beschwörung eines Gamification-Djinn

Erlaubt es, einen Vorschlag für einen Skill, eine Aufgabe oder eine sonstige Änderung des Gamification-Systems zu stellen. Wenn es keine triftigen Gründe gibt, den Wunsch abzulehnen findet er seinen Weg in die Veranstaltung.
Außerdem gibt es eine kleine Belohnung in Form von XP für jeden guten Vorschlag.

---

* Voraussetzung: Charismatische Aura
* Kosten: 64 XP
* Cooldown: 3 Tage
* Effekte:
  * \+ 10 XP
  * Nachrichteneffekt
