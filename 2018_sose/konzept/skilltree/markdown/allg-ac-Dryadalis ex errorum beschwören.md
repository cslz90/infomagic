# Dryadalis ex errorum beschwören

Beschwört einen hilfreichen Elf, der gefundene Fehler bei genauer Beschreibung weg zaubert. Dann teilt er die Belohnung des Infomagisters mit dem Finder.
Damit der Elf nicht zu lange mit einem Fehler beschäftigt ist, und schnell seine Arbeit erledigen kann ist es jedoch wichtig, dass die Beschwörung genauen Regeln folgt.

Hast du einen Fehler in den Folien oder den Aufgaben gefunden, so reicht es zur Beschwörung einfach den Fehler direkt hier zu beschreiben.

Hast du jedoch einen Fehler an Gildamesh gefunden, so stelle bitte ein Issue in Gitlab und schreibe die Nummer des Issues hier in das Textfeld. Ohne die Nummer kann der Elf dich nicht wiederfinden um die XP mit dir zu teilen.
Für Dozentron gilt grundsätzlich das gleiche, jedoch muss dass Issue im Dozentron Repository gestellt werden.

---

Parent: Aura des Enthusiasmus

Effekte:

* \+2 XP
* Nachrichteneffekt