# Festmahl herbeirufen

Der Studierende kann mitten in der Vorlesung oder Übung beliebig viel Essen oder Getränke erscheinen lassen und natürlich auch verzehren.
Ohne diese Fähigkeit ist nur eine einzige Trinkflasche erlaubt.

* Kosten: 32 XP
* Cooldown: 14 Tage
* Effekt: keiner, muss aber akzeptiert werden