# Conditio Humana

*Infomagier sind in der Regel ein sehr scheues Völkchen.
Selbst unter ihresgleichen geraten Sie nur in Gespräche, wenn es unbedingt sein muss und bevorzugen lieber magische Kommunikationsmittel.*

*Es geht jedoch die Sage um von einer besonderen Sorte von Infomagiern, der "Sozialmagischen Schar" (kurz SMS).
Diese hat den alten Zauber "Conditio Humana" wiederentdeckt und erhält damit ungeahnte Einsicht in die Fragen des zwischenmenschlichen Zusammenseins.*

*Als eine Nebenwirkung dieser Fähigkeit, können Sie auch genügend Verständnis in Infomagiern wecken, dass Sie deren Aufgaben nach der Tradition ihrer Heimat Typoscriptia lösen dürfen und sich nicht den strengen Regeln von Ja'va unterwerfen müssen.*

Als Student oder Studentin des Studiengangs "Social Media Systems" (und nur als solche/r!) dürfen Sie die Unittest-Aufgaben, die sonst mit der Sprache Java gelöst werden müssen, auch mit Typescript lösen.

Ihre Lösung laden Sie dazu bitte nicht in Dozentron hoch, sondern reichen sie als Nachricht bei der Aktivierung dieses Skills ein. (Das Nachrichtensystem akzeptiert auch Dateianhänge.)
Ihre Bewertung und Erfahrungspunkte erhalten Sie dann wie gewohnt in der eigentlichen Aufgabe in Gildamesh.

---

Effekte:

* nur Texteffekt, keine XP