# Wahrheitsserum

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Der Dozent muss eine Klausuraufgabe bekannt geben.

---

* Voraussetzung: Trank der Klausurvisionen
* Kosten: 256 XP
* Cooldown: passiv/365 Tage
* Anwendungen nötig: 20
