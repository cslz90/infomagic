# Beschwörung eines großen Djinn

Beim Erlernen dieses Skills kann ein Thema für die letzte Woche der Vorlesung vorgeschlagen werden. Wenn genug Vorschläge zusammen kommen, wird die letzte Woche aus diesen Wunschinhalten bestehen.
Außerdem wird jeder Vorschlag mit ein paar zusätzlichen XP belohnt.

---

* Voraussetzung: Beschwörung eines Gamification-Djinn
* Kosten: 64 XP
* Cooldown: Passiv
* Effekte
  * Nachrichteneffekt
  * \+ 15 XP
