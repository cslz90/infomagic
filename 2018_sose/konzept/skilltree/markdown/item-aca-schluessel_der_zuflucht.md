# Schlüssel der Zuflucht

Sie erhalten Zugang zur geheimen Zuflucht, die Schutz vor den Schrecken von Klausuchur bietet.

Sie dürfen an einer Vorbereitungsstunde für die Klausur teilnehmen, in der der Dozent mit Ihnen Aufgaben von dem Typ übt, wie sie in der Klausur vorkommen könnten.

Diese Fähigkeit wirkt erst, wenn Sie mindestens drei Trankfähigkeiten unterstützt haben.

---

* Voraussetung: Schlüssel der Hilfsbereitschaft
* Kosten: 64 XP
* Cooldown: passiv
