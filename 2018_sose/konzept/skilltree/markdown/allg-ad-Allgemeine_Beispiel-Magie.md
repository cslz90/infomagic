# Allgemeine Beispiel-Magie

*Wenn Infomagier zu wenig zu tun haben, wenden sie gerne die sogenannte Allgemeine Beispiel-Magie (kurz ABM) an.
Damit materialisieren sie die Gedankenströme anderer Infomagier auf der Welt in einer konkreten Aufgabe, die sie dann lösen können.*

Schlagen Sie eine neue Unteraufgabe für eine der Aufgaben aus dem Bereich "Freie Aufgaben" vor.
Wir werden diese dann mit einer passenden XP-Belohnung versehen und sie Ihnen und allen anderen Studierenden zur Verfügung stellen.

---

Parent: Aura des Enthusiasmus

Effekte:

* \+ 3 XP
* Nachrichteneffekt zur Benennung der Aufgabe
* Wird nicht automatisch akzeptiert