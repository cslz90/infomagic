# Kleine Heilung anderer

Da Sie als echter Infomagier natürlich eine soziale Ader haben, lernen Sie als erstes, die Wunden ihrer Mitnovizen zu versorgen.
Im Kampf gegen den Drachen Klausuchur wird es die Kraft des ganzen Kurses brauchen.

---

Parent: Aura des Enthusiasmus

Effekte:

* Zielt auf andere Studierende (target selectable)
* +1 HP
* Cooldown: 4 Wochen

Kosten: 10 XP