# Fluch der Logorrhö

Der Dozent muss am Anfang der nächsten Vorlesung einen Text von bis zu 140 Zeichen, den der Anwender vorgibt, unkommentiert vorlesen. Bei zu vulgären oder diskriminierenden Texten kann er den Fluch jedoch abschütteln.

---

* Kosten: 32 XP
* Cooldown: 14 Tage
* Effekt: Nachrichteneffekt