# Seelenheilung

*Seelenmagie ist das Gegenstück zur Blutmagie: Sie ist nicht der Weg zu legendärer Macht, aber dafür erlaubt sie dem Anwender selbst die schlimmsten Gefahren wohlbehalten zu überstehen.*

*Das wichtigste für Seelenmagier ist, immer einen Plan B zu haben, mit dem sie das Schlimmste abwenden können.
Daher haben sie eine Fähigkeit entwickelt, mit der sie einen Teil ihrer magischen Kraft dauerhaft opfern können, um damit aber jede Verletzung heilen zu können.*

**Achtung:** Diese Fähigkeit *kostet* sie Erfahrungspunkte.
Dadurch kann auch ihre Stufe sinken. Dafür erhalten Sie aber wertvolle Lebenspunkte zurück.

---

* Kosten: 64 XP
* Cooldown: 7 Tage
* Effekte:
  * \- 64 XP
  * \+ 1 HP