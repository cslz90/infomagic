# Pheromone des Probeklausurdrachen

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

*Probeklausurdrachen sind kleinere Exemplare der gefährlichen Klausurdrachen.
Da sie von ihren größeren Geschwistern oft geärgert und gehänselt werden, tun sie sich sehr gerne mit Novizen zusammen, die gegen diese antreten wollen und helfen ihnen bei ihrem Training.*

**Wirkung:** Lockt einen kleinen Probeklausurdrachen an, der sich für ein Training für den Kampf gegen Klausuchur zur Verfügung stellt.

---

* Voraussetzung: Pheromone des Musterlösungskobolds
* Kosten: 128 XP
* Cooldown: 14 Tage
* Anwendungen nötig: 30