# Blutpakt

Sie schließen einen Blutpakt mit einem Kommilitonen. Dieser erhält für eine seiner Abgaben von einer freiwilligen Aufgabe zusätzlich noch eimmal 50% der ursprünglichen XP-Belohnung.

Die XP werden manuell von einem Tutor vergeben. Sie müssen bei der Anwendung der Fähigkeit aber den Kommilitonen angeben auf den sie wirkt und den Namen der Aufgabe, deren Belohnung sie erhöhen wollen.

---

* Kosten: 128 XP
* Cooldown: 0 Tage
* Effekte:
  * -1 HP
  * Nachrichteneffekt: Name des Kommilitionen und der Aufgabe