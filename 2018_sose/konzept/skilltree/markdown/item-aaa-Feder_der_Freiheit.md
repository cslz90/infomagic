# Feder der Freiheit

Die Feder der Freiheit erlaubt es jedes Detail einer einzigen Aufgabe auf einem Aufgabenblatt umzuschreiben. Die Aufgabe ist dadurch nur für den Anwender dieser Fähigkeit geändert. Der Rest des Kurses muss die Aufgabe ganz normal bearbeiten.

Diese Fähigkeit gilt für die oberste Ebene der Nummerierung, also z.B. für Aufgabe 3, nicht nur Aufgabe 3.1.
Sie dürfen sich damit die Aufgabe natürlich leichter machen, aber sie darf dadurch nicht völlig trivial werden.

Geben Sie bei der Anwendung dieser Fähigkeit den Namen der Aufgabe und die gewünschte Änderung als Text an.

---

* Voraussetzung: Feder der Sprachen
* Kosten: 128 XP
* Cooldown: 3 Wochen
* Texteingabe: Aufgabennummer und Änderung