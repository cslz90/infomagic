# Gorgonenbann

*Ihre Bezauberungsfähigkeiten sind so stark geworden, dass Sie selbst den Goldgorgonen höchstpersönlich in ihren Bann ziehen können - zumindest lange genug, dass er Ihnen eine Frage zu einer Aufgabe beantwortet, um Ihnen bei deren Lösung zu helfen.*

---

* Vorgängerskill: Bezauberung
* Kosten: 128 XP
* Effekte: Nachrichteneffekt
* Cooldown: 10 Tage