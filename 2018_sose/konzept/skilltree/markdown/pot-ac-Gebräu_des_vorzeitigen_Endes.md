# Gebräu des vorzeitigen Endes

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Das Raum-Zeit-Gefüge bricht auseinander und die Vorlesung endet eine Woche früher.

---

* Voraussetzung: Trank des Bonbonregens
* Kosten: 160 XP
* Cooldown: passiv
* Anwendungen nötig: 30