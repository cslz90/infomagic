# Trank der Redseligkeit

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Der Infomagister muss zu Beginn der nächsten Vorlesung für 15 Minuten das Thema wiederholen, das bei der Anwendung dieser Fähigkeit am häufigsten genannt wurde.

---

* Voraussetzung: Trank des Bonbonregens
* Kosten: 128 XP
* Cooldown: 14 Tage
* Texteingabe: zu beantwortende Frage
* Anwendungen nötig: 15