# Blutheilung

*Wenn ein Infomagier seine Energie aufgebraucht hat, kann er immer noch mit der Kraft seines eigenen Blutes zaubern.
Diese Fähigkeit ist eigentlich verboten, weil sie sehr gefährlich ist und den Anwender sogar töten kann, aber im Angesicht des Schreckens, den Klausuchur verbreitet, erlaubt Ihnen der Goldgorgone diese geheimen Techniken zu erlernen.*

*Der erste Schritt auf dem Pfad der Blutmagie ist die Fähigkeit, andere zu heilen auf Kosten der eigenen Lebenskraft.*

**Achtung:** Unsterbliche dürfen diesen Skill nicht erlernen.

---

* Kosten: 64 XP
* Cooldown: 7 Tage
* Effekte:
  * -2 HP
  * +1 HP beim Ziel