# Patronus

*Sie haben gelernt, einen Schutzgeist zu beschwören, der Ihnen hilft, die Gefahren auf ihrer Reise zu bestehen.*

Beim Erlernen dieser Fähigkeit müssen Sie einen Kommilitonen als Mentor auswählen.
Dieser Kommilitone muss die Fähigkeit "Ätherform" besitzen und bei deren Erlernen ebenfalls ihren Namen angeben.

Sie benötigen zum Bestehen eines Aufgabenblattes nur noch 60% der Punkte statt 70%.

---

* Kosten: 96 XP
* Cooldown: passiv
* Effekte:
  * Nachrichteneffekt: Name des Mentors