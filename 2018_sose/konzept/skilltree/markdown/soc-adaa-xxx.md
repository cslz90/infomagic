# ?❔❓

Oh, da sind Sie wohl auf einen Fehler im System gestoßen. Diese Fähigkeit ist noch nicht fertig erstellt. Denken Sie sich eine beliebige Wirkungsweise dieser Fähigkeit aus. Der Infomagister muss der Fähigkeit zustimmen, bevor sie aktiviert werden kann.

---

* Voraussetzung: Beschwörung eines großen Djinn
* Kosten: 265 XP
* Cooldown: 7 Tage
* Effekt:
  * Nachrichteneffekt: Wirkungsweise des neuen Skills
