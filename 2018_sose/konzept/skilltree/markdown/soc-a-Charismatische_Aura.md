# Charismatische Aura

*Sie haben gelernt ihre Magie zu nutzen, um ihre eigene charismatische Ausstrahlung zu verstärken - so sehr, dass Sie einen Adepten dazu bringen können ein Stichwort fallenzulassen oder einen Ortungszauber zu sprechen, der ihnen den richtigen Weg weist, um eine ihrer Aufgaben zu bestehen.*

Stellen Sie eine Frage zu einer Pflicht- oder Bonusaufgabe. Die Tutoren werden Ihnen mit einem Stichtwort bzw. einem kurzen Satz oder einen Link bzw. Literaturhinweis antworten, mit dem Sie die Aufgabe leichter lösen können.

---

* Kosten: 64 XP
* Effekte: Nachrichteneffekt
* Cooldown: 7 Tage