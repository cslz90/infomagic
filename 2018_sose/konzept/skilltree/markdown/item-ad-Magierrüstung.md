# Magierrüstung

Beschwört eine magische Rüstung herauf, die den Schaden von verärgerten Datenkreaturen und verpatzten algomantischen Zauberformeln mindert.
Statt 4 HP ziehen missglückte Aufgabenblätter jetzt nur noch 3 HP ab.

Der Effekt wird als Heilung realisiert.
Daher müssen Sie diese Fähigkeit selbst anwenden, wenn Sie ein Aufgabenblatt nicht bestanden haben.

---

* Voraussetzung: Verbesserter Zauberstab
* Kosten: 256 XP
* Cooldown: 10 Tage
* Effekt:
  * \+1 HP