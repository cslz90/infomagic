# Pheromone des Musterlösungskobolds

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Lockt einen Musterlösungskobold an, der eine Musterlösung für ein bereits abgelaufenes Aufgabenblatt erstellt und dem Kurs zur Verfügung stellt.

Geben Sie bei der Anwendung dieser Fähigkeit bitte an, für welches Aufgabenblatt Sie eine Musterlösung haben möchten.

---

* Voraussetzung: Trank des Bonbonregens
* Kosten: 128 XP
* Cooldown: 14 Tage
* Anwendungen nötig: 20