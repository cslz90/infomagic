# Seelenalchemie

*Auch unter Seelemnagiern gibt es sehr talentierte Alchemisten, die einen Teil ihrer magischen Kraft opfern können um Tränke in kürzester Zeit mit weniger Zutaten zu brauen.*

Für jede Anwendung dieser Fähigkeit könnnen Sie einen Trank im Kurs um eine Einheit füllen.

Geben Sie den Namen des Tranks bitte bei der Anwendung der Fähigkeit an.

---

* Kosten: 64 XP
* Cooldown: 3 Tage
* Effekte:
  * \- 15 XP
  * Nachrichteneffekt: Name des Tranks