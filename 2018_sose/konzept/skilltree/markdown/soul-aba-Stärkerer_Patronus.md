# Stärkerer Patronus

Das Band mit ihrem Patronus verstärkt sich.

Sie brauchen jetzt nur noch 50% der Punkte um ein Aufgabenblatt zu bestehen und Ihr Mentor bekommt Ihre vollen Erfahrungspunkte von Aufgabenblättern zusätzlich angerechnet statt wie bisher 50%.

---

* Kosten: 128 XP
* Cooldown: passiv