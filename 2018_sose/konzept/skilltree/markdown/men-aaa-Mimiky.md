# Mimikry

Durch ihre soziale Ader haben Sie ihre Kommilitonen so gut kennengelernt, dass Sie deren Fähigkeiten nachahmen können.
Diese Fähigkeit kann nur einmal aktiviert werden, gewährt dann aber dauerhaft irgendeinen andere Fähigkeit aus dem gesamten Baum.

Geben Sie den Namen der Fähigkeit, die Sie lernen wollen beim Erlernen dieser Fähigkeit an.

---

* Kosten: 256 XP
* Cooldown: passiv
* Texteingabe: Name der Fähigkeit