# Bezauberung

*Sie haben gelernt ihre charismatische Aura nicht nur zu verstärken sondern bewusst zu nutzen, um den Geist ihrer Mitmagier gewogen zu stimmen.
Statt kurzen Stichworten können Sie nun einen Adepten so bezaubern, dass er Ihnen einen umfangreicheren Hinweis in mehreren vollständigen Sätzen gibt.*

---

* Kosten: 64 XP
* Effekte: Nachrichteneffekt
* Cooldown: 7 Tage