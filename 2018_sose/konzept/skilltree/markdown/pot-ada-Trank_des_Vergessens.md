# Trank des Vergessens

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Der Dozent vergisst das Thema eines einzelnen Foliensatzes und kann in der Klausur keine Aufgaben dazu stellen.

Geben Sie beim Erlernen dieser Fähigkeit die Nummer des Foliensatzes an, der nicht für die Klausur relevant sein soll.

**Achtung:** Es ist nicht möglich, Themen aus der Klausur zu streichen, die auf mehreren Foliensätzen vorkommen (z.B. Laufzeitanalyse in O-Notation).

---

* Voraussetzung: Trank der Klausurvisionen
* Kosten: 160 XP
* Cooldown: passiv
* Anwendungen nötig: 30
* Texteingabe: zu vergessendes Thema / Nummer des Foliensatzes
