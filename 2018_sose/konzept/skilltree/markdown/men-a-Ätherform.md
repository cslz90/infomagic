# Ätherform

*Sie haben gelernt, eine körperlose Gestalt anzunehmen, mit der sie für einen ihrer Mitnovizen als Schutzgeist fungieren können.
Dadurch profitieren Sie natürlich auch von den Erfahrungen ihres Schützlings.*

Bei der Aktivierung dieser Fähigkeit müssen Sie einen Kommilitonen als Schützling auswählen.
Dieser Kommilitone muss die Fähigkeit "Patronus" besitzen und bei deren Aktivierung ebenfalls ihren Namen angeben.

Immer wenn Ihr Schützling ein Aufgabenblatt besteht, bekommen sie 50% von seinen Erfahrungspunkten zusätzlich angerechnet.

---

* Kosten: 96 XP
* Cooldown: passiv/365 Tage
* Effekte:
  * \+ 0 HP für Schützling