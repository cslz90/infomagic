# Schlüssel der Hilfsbereitschaft

Öffnet die Türe zum Büro des Infomagisters und stimmt diesen für mindestens 15 Minuten so wohlwollend, dass er nahezu jede Frage beantwortet.

Geben Sie bei der Anwendung der Fähigkeit bitte einen gewünschten Termin für das Gespräch an.

---

* Voraussetzung: Verbesserter Zauberstab
* Kosten: 64 XP
* Cooldown: 2 Wochen
* Texteingabe: Terminvorschläge