# Verbesserter Zauberstab

Wertet den eigenen Zauberstab durch einen Kern aus Einhornhaar, Drachenblut oder Greifenfedern auf.
Erhalte zusätzliche XP bei der Abgabe der nächsten freiwilligen Aufgabe, die mindestens 20 XP Wert ist.

---

* Kosten: 64 XP
* Cooldown: 14 Tage
* Effekte:
  * \+ 20 XP
  * Nachrichteneffekt: Aufgabe