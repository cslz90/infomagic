# Fluch der Albernheit

Der Goldgorgone muss irgendein albernes Kleidungsstück in der nächsten Vorlesung oder Übung tragen. Das Kleidungsstück muss natürlich von dem Anwender der Fähigkeit gestellt werden.

Warnen Sie den Goldgorgonen bitte mit einer Nachricht vor. 😉

---

* Kosten: 128 XP
* Cooldown: 21 Tage
* Effekte:
  * Nachrichteneffekt: Beschreibung des Kleidungsstücks
