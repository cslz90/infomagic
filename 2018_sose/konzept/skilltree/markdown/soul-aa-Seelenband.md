# Seelenband

*Seelenmagier sind dafür bekannt, sich zu größeren Gruppen zusammenzuschließen und dadurch noch zäher zu werden.*

Sie können diese Fähigkeit jedesmal anwenden, wenn Sie einen Kommilitonen mit einer anderen Fähigkeit geheilt haben.
Sie bekommen dadurch selbst auch einen Lebenspunkt zurück.

Geben Sie bei der Anwendung dieser Fähigkeit bitte den Namen der Heilfähigkeit an, die Sie verwendet haben.

---

* Kosten: 192 XP
* Cooldown: 14 Tage
* Effekte:
  * \+ 1 HP
  * Nachrichteneffekt: Name der Fähigkeit