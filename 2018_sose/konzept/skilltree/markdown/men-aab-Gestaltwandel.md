# Gestaltwandel

Der Anwender dieser Fähigkeit kann eine Lösung eines Kommilitonen als eigene Lösung für eine Aufgabe einreichen. Der Kommilitone erhält natürlich trotzdem auch selbst noch die Punkte dafür.

Geben Sie bei der Anwendung der Fähigkeit bitte den Namen des Kommilitonen und den Namen der Aufgabe an.

---

* Voraussetzung: Herausforderung des Mentors
* Kosten: 256 XP
* Cooldown: 14 Tage
* Texteingabe: Name des Kommilitonen + Name der Aufgabe
