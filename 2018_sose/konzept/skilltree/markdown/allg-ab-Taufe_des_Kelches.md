# Taufe des Kelches

Der Kelch, in dem Ihr Feedback zur Veranstaltung gesammelt wird, braucht einen neuen Namen.
Nachdem der *Kelch der Kritik* für zu negativ (wenn auch immer noch besser als der *Apfelkorb der Abscheulichkeiten*, der ab und zu als Ersatz zum Einsatz kam) befunden wurde, steht der Infomagier ratlos vor dem unbenannten durchsichtigen Gefäß.

Helfen Sie ihm, indem Sie diesen Taufzauber verwenden und einen Vorschlag einreichen.
Die Fünf Vorschläge, die dem wahren Wesen des Gefäßes am nächsten kommen, werden mit einer besonderen Einsicht (in Form von XP) belohnt.

---

Parent: Aura des Enthusiasmus

Effekte:

* \+ 10 XP
* Nachrichteneffekt
* Wird nicht automatisch akzeptiert
* Cooldown: "unendlich"