# Blutfeuer

*Sie entzünden ein Feuer in ihrem eigenen Herzen und machen sich dadurch stärker, aber auch verwundbarer.*

Aufgabenblätter geben Ihnen die doppelte Erfahrung, fügen beim Nichtbestehen aber auch doppelte Schadenspunkte zu.

Damit die XP ausgeteilt werden, müssen Sie nach der Abgabe des Aufgabenblattes diese Fähigkeit anwenden.

---

* Kosten: 192 XP
* Cooldown: 10 Tage
* Effekte:
  * \+ 64 XP