# Seelenruhe

*Seelenmagier brauchen viel Zeit für ihre Zauber.
Manchmal bedeutet das, dass sie die Fristen ihrer Vorgesetzten verpassen.
Deshalb haben Sie eine Fähigkeit entwickelt, den Vorgesetzten in die gleiche "Seelenruhe" zu versetzen, in der sie sich selbst befinden.*

Sie dürfen jedes Arbeitsblatt, das Sie noch nicht bestanden haben, nachträglich noch einmal einreichen.
Sie erhalten dafür keine Erfahrungspunkte mehr, aber sie bekommen zwei der verlorenen vier Lebenspunkte zurück (falls ihre Abgabe genug Punkte erreicht).

Reichen Sie Ihre Abgabe bitte komplett als Zip-Datei mit der Anwendung dieses Skills ein.

---

* Kosten: 64 XP
* Cooldown: 7 Tage
* Effekte:
  * \+ 2 HP
  * Nachrichteneffekt: Abgabe