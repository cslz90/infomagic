# Blutalchemie

*Sie haben gelernt mit ihrem Blut auch Tränke zu verstärken.*

Jede Anwendung dieser Fähigkeit erhöht den Füllstand eines Trankes um zwei Einheiten.

Geben Sie bei der Anwendung dieser Fähigkeit den Namen des Tranks an, dessen Füllstand Sie erhöhen wollen.

**Achtung:** Es ist derzeit technisch noch nicht möglich, diese Fähigkeit zu verwenden. Daher haben wir sie "gesperrt" indem wir das benötigte Level zum Lernen erhöht haben. Sobald die technischen Probleme gelöst sind, wird das wieder rückgängig gemacht.

---

* Kosten: 128 XP
* Cooldown: 0 Tage
* Effekte:
  * \- 1 HP
  * Nachrichteneffekt: Name des Tranks