# Gedankenkontrolle

*Durch ihre Studien in der Bezauberung anderer Mitmagier haben Sie ein so gutes Verständnis für deren Gedankengänge erreicht, dass Sie sie nun direkt kontrollieren können.
Sie können diese Kräfte Nutzen, um einen Adepten dazu zu bringen, einen Fehler in ihren Zauberformeln zu finden.
Da die Gedankenkontrolle aber nicht lange anhält, müssen Sie den Fehler schon auf höchstens 20 Zeilen ihrer Formel eingrenzen.*

Geben Sie 20 Zeilen Code in das Nachrichtenfeld ein.
Ein Tutor wird sich 15 Minuten Zeit nehmen, um zu versuchen diesen Code zu verbessern und Fehler zu beheben.

Hinweis: Denken Sie daran, dass ihre Eingabe mit [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) formatiert wird. Setzen Sie also bitte ein ` ``` ` vor und hinter den Code, damit dieser für die Tutoren korrekt angezeigt wird.

---

* Kosten: 128 XP
* Effekte: Nachrichteneffekt
* Cooldown: 7 Tage