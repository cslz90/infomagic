# Temporalschlüssel des Tutankhtorium

*So manch ein Novize würde gerne die Zeit zurückdrehen - und vielleicht auch gleich noch etwas ausdehen - um sich die bewegten Ansprachen des Goldgorgonen noch einmal in einer Geschwindigkeit anzuhören, bei der einem das Hirn nicht sofort aus dem Schädel springen will.*
*Glücklicherweise gibt es einen Gegenstand aus dem Reich des antiken Königs Tutankhtorium, der genau das ermöglicht: Der Temporalschlüssel.*

*Mit einem Temporalschlüssel kann man nicht nur in der Zeit sondern auch zwischen Dimensionen wechseln, so dass man die Lehren des Goldgorgonen noch einmal aus dem Munde einer alternativen Version seiner selbst hörten kann, um dann mit neuem Wissen gestärkt wieder in die Realität zurückzukehren.*

Diese Fähigkeit erlaubt es Ihnen an dem Tutorium teilzunehmen, das von den Tutoren begleitend zur Vorlesung angeboten wird.
In diesem werden die Inhalte der Vorlesung noch einmal in einem langsameren Tempo wiederholt.
Es dient dazu, dass Sie ihre Schwächen ausbessern können.
Idealerweise führt das dazu, dass Sie wieder mit der Vorlesung Schritt halten können, wenn sie vorher Schwierigkeiten damit hatten.

Selbst wenn es in diesem Semester aber mit der Klausurzulassung oder der Klausur selbst knapp werden sollte, erlaubt Ihnen das Tutorium mit einem soliden Grundwissen in das nächste Semester zu gehen, statt von jedem Thema nur ein kleines Bisschen mitzunehmen, weil Sie irgendwo am Anfang etwas wichtiges verpasst haben.

Die [Termine des Tutoriums](https://moodle.thm.de/mod/choice/view.php?id=192416) finden Sie in Moodle.

---

Passiver Skill

Kosten: 5 XP