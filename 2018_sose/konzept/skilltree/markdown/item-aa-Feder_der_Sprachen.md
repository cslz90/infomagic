# Feder der Sprachen

Sie erhalten die Fähigkeit, von jeder Sprache in jede andere zu übersetzen. Dadurch können Sie eine Aufgabe (freiwillig oder Aufgabenblatt) in jeder beliebigen Programmiersprache abgeben statt in der geforderten Sprache.

Die Abgabe muss dadurch aber als Zip-Datei über diese Fähigkeit erfolgen.

---

* Voraussetzung: Verbesserter Zauberstab
* Kosten: 128 XP
* Cooldown: 10 Tage
* Effekte
  * Dateieffekt: Abgabe