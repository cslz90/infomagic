# Trank des Bonbonregens

*Trank: Jede Anwendung erhöht den Füllstand für den gesamten Kurs um 1. Ist der maximale Füllstand erreicht, entfaltet der Trank seine Wirkung.*

**Wirkung:** Der Dozent muss in der nächsten Stunde an jeden, der eine Frage stellt oder beantwortet, Süßigkeiten verteilen.

---

* Kosten: 64 XP
* Cooldown: 7 Tagen
* Anwendungen nötig: 10