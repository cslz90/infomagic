# Konzept

* *Skillpunkte (SP)* existieren als Begriff in Gildamesh und sind eigentlich sinnvoll. Sollten auch ihren Weg in die Spielanleitung finden.

# Folien

* Doch kein Haskell (aber was dann?)
  * Oder java zusätzlich zu Haskell?
* Typische Probleme?
  * Suchen
  * Sortieren
  * Optimieren

Gemeinsamer Base-File

# Pseudocode

`A < B` => `A is_a B`

`x != None` => `x exists` bzw. `exists x`

```
operator lst[idx] with (lst: List<E>, idx: int): E
  return lst.get(idx)

operator x <= y with (x: Sortable<E>, y: Sortable<E>): boolean
  return x.leq(y)
```

```
for i from x to_including y [step z] do
  ...
for i from x to_excluding y [step z] do
  ...
```

Beim Binärbaum `hasLeft()` und `hasRight()`