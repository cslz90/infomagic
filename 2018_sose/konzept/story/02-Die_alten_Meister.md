# Akt 2 - Die alten Meister 👹👷👸

Die Novizen trainierten die alten Künste des Objektorients Tag und Nacht, während er sie mit Unversalhieroglyphen und den Lehren der Weisen von O vertraut machte.
Jedoch hatte der Goldgorgone nicht daran gedacht, dass die Novizen der Sozialmagischen Schar (kurz SMS) noch nicht mit den strengen Formalismen der Ja'vanesen vertraut waren, deren magischer Übungspuppen er verwendete.
Natürlich hatte er in seiner Güte ein Einsehen mit ihnen und seine Adepten konnnten auch glücklicherweise auf die schnelle noch ein paar gebrauchte typoscriptianische DeclareDummies im Nexus des t-Bay auftreiben, die sich mit der Kunst der ["Conditio Humana"](https://gildamesh.mni.thm.de/courses/1/skills) der SMSler bearbeiten ließen.

Natürlich waren die vorbildlichen Novizen trotzdem daran interessiert, die Wege der Ja'vanesen zu erlernen und einige besonders wagemutige äußerten sogar Interesse an einem Auslandsaufenthalt in Haskellien.
Also stieg der Goldgorgone in die verbotene Bibliothek herab und brachte seinen Schützlingen die [Iridiumgeprägte Spezielle BijektionsNummer](https://homepages.thm.de/~cslz90/kurse/ad18/static/ad-00-literaturempfehlungen.html) der Folianten, die für sie am interessantesten sein würden.

Trotz allem Eifers würden die Novizen nicht endlos Zeit für ihr [Training](https://gildamesh.mni.thm.de/courses/1/components/16) haben.
Schon zur Geisterstunde am kommenden Tag des Donners würde sich der Drache erneut erheben, um zu versuchen die schwachen Novizen von der Gruppe zu trennen - so hatte es der Kalendarische Donnervogel des Goldgorgonen prophezeit.

Um dabei so wenig wie möglich Novizen zu verlieren, führte der Goldgorgone sie in die [Lehre der Universalhieroglyphen](https://homepages.thm.de/~cslz90/kurse/ad18/static/ad-00-pseudocode-definition.html) ein und bannte die [besprochenen Übungstechniken](https://homepages.thm.de/~cslz90/kurse/ad18/static/2018-04-13_16-Plagiate-AB0.html) noch einmal in einen kleinen handlichen elektronischen Folianten.

Die besonders engagierten Novizen, die ihre magischen Übungen bereits frühzeitig erledigen konnte, ermutigte er außerdem, in der Wildnis mit Hilfe [allgemeiner Beispiel-Magie](https://gildamesh.mni.thm.de/courses/1/skills) auf eigene Faust nach [neuen Zaubern, Kreaturen und Waffen](https://gildamesh.mni.thm.de/courses/1/components/173) zu suchen, die im Kampf gegen Klausuchur nützlich sein könnten.

Sie sollten jedoch früh genug zurückkehren, damit Sie nun gemeinsam den Pfad der alten Meister beschreiten könnten, auf dem sie in gemeinsamer Meditation den Segen der ersten und größten Algomantiker für ihre Queste einholen würden.

Da es ab nun um alles ging, bat der Goldgorgone seine Schützlinge auch, jeglichen Fehler von ihm oder seinen Adepten von jetzt an schnell mit der Beschwörung eines [Dryadalis ex errorum](https://gildamesh.mni.thm.de/courses/1/skills) zu beseitigen.

So zogen Sie nun also los und ließen langsam aber sicher die warme sichere Heimat hinter sich, um sich auf ihr großes Abenteuer zu begeben.

---

📖 TLDR in Klartext:

* **SMS-Studierende** dürfen ihre Abgaben für Unittest-Aufgaben (das sind auf Aufgabenblatt 0 die Aufgaben 1 und 2) in Typescript verfassen. Dazu brauchen sie den (kostenlosten) Skill ["Conditio Humana"](https://gildamesh.mni.thm.de/courses/1/skills).
* Es gibt zusätzliche [Literaturvorschläge zu Haskell und OOP-Grundlagen](https://homepages.thm.de/~cslz90/kurse/ad18/static/ad-00-literaturempfehlungen.html).
* Die Deadline für das [erste Aufgabenblatt](https://gildamesh.mni.thm.de/courses/1/components/16) läuft am Donnerstag (d.h. für Sie, wenn Sie das lesen, vermutlich "heute") kurz vor Mitternacht aus. Sie müssen dieses mit mindestens 70% der Gesamtpunktzahl bestehen, sonst verlieren Sie 4 Lebenspunkte.
* Zusätzlich zu den Folien ist jetzt auch ein [Protokoll der ersten Übung](https://homepages.thm.de/~cslz90/kurse/ad18/static/2018-04-13_16-Plagiate-AB0.html) auf Moodle zu finden.
* Sie finden in Moodle einen Link zu der [Definition meines Pseudocodes](https://homepages.thm.de/~cslz90/kurse/ad18/static/ad-00-pseudocode-definition.html).
* Es gibt viele neue [freie Aufgaben](https://gildamesh.mni.thm.de/courses/1/components/173) auf Gildamesh, die Sie dazu ermutigen sollen, sich selbstständig mit den Themen der Veranstaltung zu beschäftigen. Reichen Sie dazu bitte gerne jederzeit neue Ideen mit dem Skill ["Allgemeine Beispiel-Magie"](https://gildamesh.mni.thm.de/courses/1/skills) ein.
* Als nächstes werden wir in der Vorlesung grundlegende algorithmische Techniken besprechen.
* **Fehler** in Folien, Aufgabenstellungen oder an den Systemen Gildamesh und Dozentron sollten ab jetzt mit dem (kostenlosen) Skill ["Dryadalis ex errorum beschwören"](https://gildamesh.mni.thm.de/courses/1/skills) gemeldet werden.