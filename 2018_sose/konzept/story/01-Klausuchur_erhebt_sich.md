# Akt 1 - Klausuchur erwacht 🐉

Auf dem *schiefen Berg* stieg Rauch auf und flüchtende Mönche aus dem dortigen Kloster berichteten, dass ein waschechter Drache namens *Klausuchur* den Berg für sich beansprucht hatte. 🐉

Als wäre das nicht genug, tauchten immer mehr Meldungen über vermisste Novizen an der Thaumaturgischen Hochschule für Magie auf.
Man sagte dahiner stecke der Drache, der sie in die *Minen von Gogol'dur* entführe, wo sie für ihn arbeiten müssten. 👷🔨

Wie so oft wollte von diesem Problem aber niemand der Zuständigen wirklich etwas wissen.

> "Probleme mit Drachen gab es an der THM mindestens seit einem Jahrhundert nicht mehr.
> Mittelhessen ist eine absolut drachenfreie Zone."
>
> -- Offizielle Aussage des Amts für magische Wesen

> "Die Novizen sind halt zu doof! Wer sich von Drachen entführen lässt, ist ja wohl selber schuld!"
>
> -- Ein unbekannter Adept

> "Das ist doch nur ein verzweifelter Versuch dieser nervigen Theoretiker, sich wichtig zu machen.
> Wer muss schon heutzutage noch wirklich diese ganzen Datenkreaturen kennen?
> Wenn wir den Novizen nur beibringen, wie sie sich elegant durch das wirklich wichtige Weltennetz bewegen, kann auch kein Drache sie fangen."
>
> -- Ein Infomagier aus der Schule der Netzweber

Nur einem einzigen Infomagier, den man wegen seiner goldgelockten Haare und dem bisweilen versteinernden strengen Blick auch den *Goldgorgonen* nannte, ließen die Berichte keine Ruhe.
Er wendete sich an die alten Meister Gonagalletschert und Dumble-Döring, um sie um Rat zu bitten, aber auch die beiden hatten einfach zu viele andere Probleme mit dem technomagischen Volk der Scalarianer und Parallelwelten, die nur mit einem klobigen magischen Helm zu sehen sind.
Trotzdem brachten sie ihn auf eine wichtige Idee:

> "Goldgorgone, die Lösung deiner Probleme liegt direkt vor dir!
> Unterschätze niemals die Kraft eines motivierten Novizen.
> Warum alleine gegen den Drachen ziehen, wenn du eine ganze Hundertschaft von ihnen anführen kannst?
> Es liegt an dir, sie für diesen Kampf vorzubereiten."
>
> -- Gonagalletschert, der Weiße

Und so versammelte der Goldgorgone die fähigsten Adepten der Thaumaturgischen Hochschule für sein gewagtes Unterfangen.
In nur einem halben Jahr mussten sie die Novizenschar so gut ausbilden, dass sie es mit vereinten Kräften mit Klausuchur aufnehmen konnten.
Keine leichte Aufgabe, aber eine, die die Adepten und der Goldgorgone sofort mit Eifer aufnahmen.

Sie überlegten sich, was am wichtigsten für die Novizen sein würde:

* Ihre Stärke lag in ihrer Anzahl und der Goldgorgone war für die Novizen verantwortlich, also sollten Sie verhindern, dass irgendjemand unterwegs auf der Strecke blieb.
    Dazu brachten Sie zuerst allen Studierenden, die dies wollten, einen [einfachen Heilzauber](https://gildamesh.mni.thm.de/courses/1/skills) bei.
* Naturgemäß hatten die Novizen nicht so viel Kampferfahrung und hatten bisher nur mit einem `JShell`-Zauberstab gearbeitet, der zwar für einfache Übungszauber gut geeignet war, aber nicht die gleiche Macht entfalten konnte wie der bei professionellen Infomagiern gebräuchliche `IntelliJ-2018` ✨.
    Also mussten Sie die Novizen [in der Bedienung dieses mächtigen Werkzeugs unterweisen](https://moodle.thm.de/mod/choice/view.php?id=192416).
* Wenn die Novizen nun die richtigen Werkzeuge in der Hand hatten, war der nächste Schritt natürlich, sie auf einen [kleinen Übungskurs](https://dozentron.mni.thm.de/groups/6) zu schicken, auf dem sie für den ersten Ernstfall trainieren konnten.

Der Goldgorgone war zuversichtlich: Seine Novizen und sein Orden aus Adepten wirkten motiviert und ehrgeizig und der Trainingsplan klang gut.
Wenn das alles so bleiben sollte, bis sie den schiefen Berg erreichten, dann sollte sich Klausuchur lieber warm einpacken.

---

📖 TLDR in Klartext:

* Es gibt einen [neuen Skill in Gildamesh](https://gildamesh.mni.thm.de/courses/1/skills), mit dem Sie Ihre Kommilitonen heilen können.
* Denken Sie daran, sich für das [Tutorium](https://moodle.thm.de/mod/choice/view.php?id=192416) am Montag im 2. Block oder am Donnerstag im 3. Block einzutragen, wenn Sie Hilfe mit IntelliJ und Dozentron bekommen möchten.
* Sie können sich auf Dozentron an [zwei kleinen Testaufgaben](https://dozentron.mni.thm.de/groups/6) austoben, um sich mit dem System vertraut zu machen.