# Akt 3 - Durch die Basihara 🏜

Nachdem der Goldgorgone und seine Novizen sich den Segen von Brutus, Greedy, Ariadne, Bartleby und Dolores geholt hatten, war es Zeit geworden, den Weg zu Klausuchurs Hort auf dem schiefen Berg einzuschlagen.

Das erste Hindernis stellte dabei die Basihara dar - eine unwirtliche Wüste und die Heimat von wilden Basilisten. 🐍
Dort gerieten sie in einen Hinterhalt der totalen Ordner - Schergen von Klausuchur, die Sie gnadenlos auf dem Weg immer weiter nach hinten sortierten, so dass sie einfach nicht vom Fleck kamen.

In der sengenden Hitze spielten die Sinne der Novizen ihnen üble Streiche, so dass einige sich schon kurz vor dem Tod wägten, obwohl sie eigentlich noch kerngesund waren.
Mit Schrecken stellte der Goldgorgone fest, dass die Novizen diesen Täuschungen hilflos ausgeliefert waren, da die Trainingsbewertungen der Adepten, die sie darauf hätten vorbereiten können, nicht rechtzeitig fertig geworden waren.

Es gab nur eine Möglichkeit: Sie mussten der Basihara und den totalen Ordnern so schnell wie möglich entkommen, um sich zu sammeln.
So lehrte er die Novizen, das Vorgehen der totalen Ordner genau zu beobachten und sie ersannen gemeinsam einen Plan.
Ein Teil der Novizen lenkte die Ordner ab, indem er ihre Transitivität, ihre Antisymmetrie und ganz besonders ihre Totalität in Frage stellte.
Währenddessen besorgte der Goldgorgone eine große Menge magischer Kisten mit Rollen, in denen er die anderen Novizen versteckte, so dass nur ihr Kopf herausschaute und sie wie ein zu sortierender Basilist wirkten.
Als die Ordner sich dann daran machten, diesen vermeintlichen Basilisten zu sortieren, schlüpften die Novizen, die das Ablenkungsmanöver geleitet hatten, schnell in die übrigen Kisten, da sie wussten, dass die Ordner sie am Ende wieder mit dem Trupp des Goldgorgonen zusammenmergen würden.

So rollten sie dann als getarnte sortierte Datenkreatur an den Ordnern vorbei und konnten endlich ihre Reise fortsetzen.
Gleich als sie außer Sichtreichweite waren, wies der Goldgorgone die Adepten an, die fehlenden Trainingsbewertungen nachzureichen, den Novizen einen Selbstanalysezauber zur vermeidung von weiteren Verwirrungen beizubringen, und die Kommunikation mit ihnen auf einen sicheren Kanal zu beschränken.
Es konnten nur die alten klapprigen Moodleflöten gewesen sein, die Klausuchur ihre Position verraten hatten.

Daraufhin trat Sir Felix Kentravyon mutig aus den Reihen der Adepten hervor und schlug vor, dass die Novizen all ihre Nachrichten ab sofort seinem Raben anvertrauen sollten, der auf den (zugegebenermaßen seltsamen) Namen felix.muenscher@■■■■.■■ hörte.

Dem Goldgorgonen fiel außerdem auf, dass die Tragetaschen ihrer Gildamesh-Ochsen, die sie als Lastentiere mitgenommen hatten, gar nicht genug Platz hatten, um alle Notizen der Novizen aufzunehmen, was sicherlich auch zu der misslichen Lage beigetragen hatte.
Schnell wies er einen seiner Adepten an, die Taschen mit einem Zauber zu erweitern, so dass nun auch Übungsnotizen platz darin fanden.

Durch all diese Maßnahmen blieben sie tatsächlich zunächst von weiterem Unheil verschont.
Am Abend übernachteten sie an einer Oase, wo ein paar vereinzelte Immerwahrs blühten. 🌴
Sie wussten zwar, dass dort Klausuchurs tödliche Plagiadile auf der Lauer liegen würden, aber sie brauchten das frische Wasser und der Goldgorgone hatte ihnen schon zu Beginn ihrer Reise erklärt, dass diese Wesen nur solche Novizen verschlangen, die Wissen vortäuschten, was sie eigentlich gar nicht besaßen. 🐊
Zum Stolz des Goldgorgonen gab es niemanden, der dieser Versuchung verfallen war und sie näherten sich immer mehr dem Ort an dem die Basihara in den Hain der Speichereichen überging.

Statt direkt auf die ersten Bäume zuzuhalten, die sie in der Ferne sahen, bog der Goldgorgone aber noch einmal nach Norden ab, wo sich an den weitesten Ausläufern des Taurusgebirges eine Höhle befand, in der die unterirdischen Ströme der Bequemlichkeit zu finden waren. 🌄

Sein Ziel war es, dort zu verschnaufen und den Novizen genug Zeit zu geben, ihre Fähigkeiten zu schulen.
Jeder bekam einen dicken Folianten mit mächten - und teilweise sogar streng genommen verbotenen - Formeln in die Hand gedrückt, während der Goldgorgone selbst den Wissensstand des gesamten Kurses ermittelte, um vorherzusagen, wie weit sie gemeinsam schon vorangekommen waren.
Die Adepten hatten unterdessen wieder ein Trainingscamp errichtet und neue Übungspuppen aufgestellt, um mit den Novizen ihre Fähigkeiten zu trainieren und sicherzustellen, dass sie die Lehren der alten Meister auch richtig verstanden hatten. 📚

Mit dem ersten Ergebnis seiner Bestandsaufnahme schien der Goldgorgone nach ein paar Tagen so zufrieden, dass er bestimmte, dass das nächste Trainingscamp noch etwas warten könnte und auch nicht so groß sein müsste.

---

📖 TLDR in Klartext:

* Die Bewertung von [Arbeitsblatt 0](https://gildamesh.mni.thm.de/courses/1/components/16) ist jetzt endlich vollständig abgeschlossen. Wenn Ihnen Punkte fehlen, sollten Sie auch einen Kommentar von den Tutoren erhalten haben, warum das so ist.
* Sie können in Gildamesh nun eine "Timeline" ihrer Lebenspunkte und Erfahrungspunkte sehen. Dort ist ganz genau aufgelistet aus welchen Gründen ihre Punkte wann verändert wurden. Sollten Ihnen dort Fehler auffallen, melden Sie diese bitte umgehend per Mail an meinen Tutoren Felix Münscher (felix.muenscher@■■■■.■■).
* Es gab im Kurs zwar eine ganz leichte Mehrheit dafür, das Moodle-Forum zu behalten, ich habe mich jetzt aber doch dafür entschieden, das Gildamesh-Forum beizubehalten und das Moodle-Forum (vorerst) zu sperren. Der Grund dafür ist einfach der, dass wir gerne das Gildamesh-Forum noch etwas testen und weiterentwickeln wollen.
* Ich bitte Sie, alle inhaltlichen Fragen zu den Aufgaben und deren Bewertung, die Sie nicht ins Forum schreiben möchten, direkt an meine Tutoren zu stellen. Am Besten dafür geeignet ist der ["Nachrichtenhub" von Gildamesh](https://gildamesh.mni.thm.de/courses/1/conversations), in dem Sie einfach mit dem großen Roten Stiftsymbol eine neue Nachricht an das "Course team" schicken können. Diese ist dann sofort für alle Tutoren sichtbar. Wenn Sie aber eine E-Mail bevorzugen ist mein Tutor Felix Münscher (felix.muenscher@■■■■.■■) dafür ihr Ansprechpartner.
* Wir haben bisher keine Plagiate gefunden. Das freut mich sehr! 😄
* Leider haben wir es versäumt, eine Abgabe für Mitschriften zu den Übungen einzureichen. In Zukunft werden diese in die Abgabe zur Mitschrift bzw. dem Stundenprotokoll für die Montagsvorlesung integriert.
* Der [Skilltree](https://gildamesh.mni.thm.de/courses/1/skills) ist jetzt so weit wie möglich freigeschaltet. Es kann aber natürlich noch Änderungen geben - insbesondere, was das Balancing angeht.
* Die [Spielregeln](https://homepages.thm.de/~cslz90/kurse/ad18/static/Spielregeln.html#kursstufe) enthalten jetzt eine Tabelle für die Belohnungen, die Sie über das "Kurslevel" für die Klausur erlangen können.
* Sie bekommen für das Aufgabenblatt 2 eine Woche mehr Zeit. Dafür wird das Aufgabenblatt 3 kürzer und muss innerhalb einer Woche abgegeben werden.