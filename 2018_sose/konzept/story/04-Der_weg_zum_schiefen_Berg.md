# Akt 4 - Der Weg zum schiefen Berg 🗻

Die Verschnaufpause an den Strömen der Bequemlichkeit war nun schon einige Zeit her.
Die Lücke in dieser Erzählung rührt daher, dass der Chronist so sehr vom Lambada-Ausdruckstanz der Stromnymphen 💃 bezaubert war, dass er in einen Reduce-Strom gefallen ist, wo er dann in einen Basilisten verpackt wurde.
Dieser kroch der Gruppe dann vollgefressen hinterher und gab seinen Mageninhalt erst vor kurzem wieder Preis.
Da der Leser aber hier nicht mit Ausschweifungen über die fraglichen Sicherheitsbestimmungen und den mangelnden Komfort in den heutigen Basilistenmägen gelangweilt werden soll, kommen wir jetzt wieder zur eigentlichen Geschichte.

Nachdem also die Truppen des Goldgorgonen die Ströme der Bequemlichkeit verlassen hatten, machten sie sich in das dichte Unterholz des Walds der Speichereichen auf. 🌳
Mit Hilfe der freundlichen Tiefentauben und Breitenhörnchen, die dort lebten, fanden sie schnell ihren Weg und nachdem der Goldgorgone den Novizen auch die laaangsaaame Spraaacheee deeer biiinääärbooorkiiigeeen Speeeiiicheeereeeiiicheeen beigebracht hatte, verrieten diese Geschöpfe sogar die ein oder andere Abkürzung.
Die einzige Gefahr, der sie unterwegs begegneten, war ein übereifriger Sündenbock, der - geweckt von den teilweise etwas abenteuerlichen Pflanzarbeiten der Novizen - den gesamten Wald rebalancieren wollte.
Als diese dem kleinen Tierchen jedoch erklären, dass Amortis - einer der Weisen von O - ihn dann in eine ganz schlechte Effizienzklasse stecken würde, ließ er von seinem Vorhaben ab und die Novizen luden ihn sogar ein, dem Turnamentum Imaginins beizuwohnen. 🐑

Dieses wurde auf einer Lichtung abgehalten und alle Wesen des Waldes bewunderten gemeinsam mit den Infomagiern das bunte Farbenspiel, das ein paar besonders engagierte Novizen in den Nachthimmel zauberten.

Die Idylle währte jedoch nicht lange, denn schon am nächsten Tag kamen sie in einen Teil des Waldes, der von einer Käferplage befallen war.
Die kleinen Biester hatten es ganz besonders auf die Mitschriften der Novizen und deren Zauberbücher abgesehen und machten einen geregelten Übungsablauf fast unmöglich.

So führte der Goldgorgone seine Schaar auf dem schnellsten Wege aus dem Wald hinaus in die Highlands von Ja'va.
Dor überzeugte er gemeinsam mit den Novizen einen Graphen von Knotlingen dazu, ihnen zu helfen, einen Weg über die schwebenden Inseln zu finden, indem sie diese an deren Bündnis mit den legendären Infomagiern Dijkstra und Kruskal erinnerten.

****

📖 TLDR in Klartext:

* Dieser Post hat keine inhaltliche Relevanz für den Kurs. Er führt nur die gamifizierte Story fort.