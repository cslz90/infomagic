# Epilog - Klausuchurs Vermächtnis 🐲

Das Zwiehorn hatte keine Chance gegen die vereinte Macht, die die Novizen auf ihrem Weg zum schiefen Berg gesammelt hatten.
Schon nach kurzer Zeit löste es sich mit zornigem Wiehern und Schnauben in zerfasernde Schatten auf.

Der Weg war frei und die Novizen mehr als motiviert.
Sie brauten solche Unmengen an Stärkungstränken, dass der Goldgorgone ihnen Einhalt gebieten musste, weil sonst die magischen Kessel explodiert wären.
Dann machten Sie sich auf den Weg zur Spitze des Berges, um Klausuchur zu stellen.

Der Kampf tobte so heftig, dass der ganze Berg erzitterte.
Der Drache spieh Feuer, das seltsamerweise eine verästelte Form mit einigen kreisrunden freien Flächen auf dem Boden hinterließ, in die die Novizen sich in der richtigen Reihenfolge aufstellen mussten, um die Flammen wieder zu löschen.
Er verwandelte die algomantischen Formeln in ihren Zauberbüchern in Hash-Codes um, die sie erst wieder richtig sortieren mussten, um ihre Zauber sprechen zu können.
An einem Punkt des Kampfes sprengte er sogar mit roher magischer Kraft die ganze Bergkuppe, um sie in eine Reihe fliegender Inseln wie in den Highlands von Ja'va zu verwandeln, die aber zur Verwirrung der Novizen viel mehr Brückenverbindungen hatten als dort.

Manch ein Novize ahnte bei diesen seltsamen Angriffen schon, dass etwas nicht stimmte.
Warum machte der Drache nicht etwas... drachigeres, wie sie einfach zu verschlingen, oder mit einem weniger obskuren Feuerzauber zu rösten?
Und tatsächlich - nachdem die Novizen auch den Schwarm von doppelt verketteten Basilisten, den Klausuchur heraufbeschworen hatte, durch das Zerpflücken der Immerwahrs auf ihren Körpern in eine schwere Identitätskrise gestürzt hatten, begann der Drache einfach herzhaft zu lachen, so als würde er sich königlich amüsieren.

> "Chrrchrrchrr, Goldgorgone, da habt ihr eine tapfere Schaar ausgebildet. Es tut mir fast Leid, dass ich euch so hinter's Licht führen musste."

Der Angesprochene, der sich bei dem Kampf im Hintergrund gehalten hatte, um einigen verwirrten und gestürzten Novizen wieder aufzuhelfen, schien nicht wirklich überrascht, knirschte aber trotzdem mit den Zähnen.

> "Ihr habt uns die ganze Zeit nur auf die Probe gestellt, oder? Das war alles ein Test. Wofür?"

Rauchkringel bildeten sich an den Nüstern des Drachen, als er wieder amüsiert schnaubte.

> "Ihr habt es doch selbst gesehen. Eine Krankheit hat dieses Land und eure hohe Schule befallen.
> Früher hätte niemand es zugelassen, dass ich dieses Territorium beanspruche.
> Wäre auch nur ein Novize verschwunden, hätte es nur den kleinsten Hinweis auf ein Problem gegeben, wären Schwärme von Magistern, Adepten und Novizen ausgeströmt und hätten sich jedem Schrecken gestellt.
> Heute reicht es aus, wenn ich ein paar griechische Symbole in die Mauern des Klosters ritze und sie verkriechen sich alle vor Angst.
> Denkt ihr, dass das Zufall ist?
>
> Nein, da sind dunkle Mächte am Werk, lieber Infomagister.
> Vor etwa fünfzig Jahren wurde eure thaumaturgische Hochschule gegründet, um Dhuum'ait und Ignor Anzia, die größten Feinde der Menschheit, zu bannen.
> Für eine Weile hat das funktioniert, aber jetzt sind sie wieder erwacht.
> Ihr erstes Ziel war natürlich die Hochschule selbst.
> Sie hassen jede Form der Mathemantik und höherer Theorie im allgemeinen, weil sie wissen, dass sie einem wachen klaren Geist nichts anhaben können.
> Daher haben sie die Schule mit einem schleichenden Fluch belegt, der die Infomagister ihre Anforderungen senken lässt und Novizen dazu bringt, sich tunlichst von schweren Aufgaben fernzuhalten.
>
> Der einzige Weg, diesen Fluch zu brechen, besteht in schweißtreibenden Übungen in der Algomantik und der Auseinandersetzung und dem Anfreunden mit den gefählichsten und mächtigsten Datenkreaturen.
> Ich habe gehofft, dass es noch welche unter euch gibt, die sich einer echten Bedrohung stellen würden.
> Bei Drachen fragt niemand zweimal nach, warum sie die Welt beherrschen oder Jungfrauen fressen wollen - die nebenbei bemerkt nicht besser schmecken als trockenes Knäckebrot und habt ihr einmal darüber nachgedacht, wieviel Verwaltungsaufwand mit so einer Weltherrschaft einhergeht?
> Aber ich schweife ab.
> Daher habe ich mich jedenfalls entschieden, den Bösen zu spielen und euch genug zu schikanieren, damit ihr diese Reise unternehmt und daran wachst.
> Und seht, wie weit ihr gekommen seid!
> Der da drüben konnte am Anfang nicht einmal das richtige Ende seines Zauberstabs finden und jetzt schreibt er seitenweise algomantische Formeln ohne Probleme.
> Und damit ist er nicht alleine.
> Jeder von euch ist über seine Grenzen hinaus gegangen und stärker als jemals zuvor.
> Ich sage ihr seid bereit, dem wirklichen Feind entgegenzutreten und auch andere von dem Fluch der Dhuum'ait zu befreien."

Der Goldgorgone betrachtete den Drachen lange und prüfend und antwortete schließlich noch nicht ganz überzeugt.

> Aber was ist mit den Minen von Gogol'dur?
> Was ist mit den Novizen, die es nicht geschafft haben - die ihr entführt habt?

Der Drache grinste nur und entblößte dabei schneeweiße Zähne, die so groß wie der Unterarm eines ausgewachsenen Mannes waren.

> Wollt ihr sie sehen?

Und so führte er die Gruppe tatsächlich in die Minen von Gogol'dur - die sich als nichts anderes entpuppten als eine prächtige unterirdische Akademie, in der der Großmeister Dumble-Döring sich darauf vobrereitete, einen neuen Schwung von Novizen für den Kampf um das Wissen der Menschheit bereit zu machen.
Da waren auch all diejenigen, die mit ihnen losgezogen waren, aber den Weg zum Schiefen Berg oder den Kampf gegen Klausuchur selbst nicht überstanden hatten.
Sie übten fleißig, um beim nächsten Mal die Prüfungen des Drachen zu bestehen.
Sogar die Mönche des Klosters waren gar nicht vertrieben worden, sondern hatten in den unterirdischen Hallen schon Getränke zur geistigen Stärkung für die geschundenen Novizen vorbereitet, die sie auch freizügig ausschenkten.

Einerseits war es verwirrend in Klausuchur plötzlich keinen Gegner mehr, sondern einen (zugegebenermaßen unorthodoxen) Verbündeten zu sehen, aber andererseits machten seine Argumente Sinn.
Ihnen allen waren die unheilvollen Veränderungen aufgefallen - verdorrende Immerwahrs, verwahrloste Speichereichen, verkümmerte kleine Basilisten, die sich in Steinspalten verstecken mussten.
All das hatten sie auch vorher schon an der Thaumaturgischen Hochschule gesehen, hatten dem aber keine größere Bedeutung zugemessen.
Es war klar, dass es nur eines zu tun gab: Die Lehren des Drachen und des Goldgorgonen zu verinnerlichen und sich in einen neuen noch viel größeren Kampf zu stürzen.

---

📖 TLDR in Klartext:

* Dieser Post dient hauptsächlich dazu, die Geschichte, die ich mit den Rundmails begonnen habe, zu Ende zu führen. Ich hoffe Sie hatten auch daran Ihren Spaß.
* Die Klausur ist fertig bewertet und ich habe die Ergebnisse im Prüfungssystem eingetragen. Um nicht zu sehr zu verwirren, schicke ich dazu aber gleich noch eine E-Mail hinterher, die nur die wichtigen Infos im Klartext enthält. 😉
* Ich hoffe sie behalten zumindest ein paar der Dinge, die Sie in dieser Veranstaltung gelernt haben. Vielleicht haben ein paar von Ihnen ja sogar genug Spaß an den Themen gehabt, um später freiwillig vertiefende Theoriemodule zu hören. Die THM und auch die Welt braucht Informatiker, die keine Angst vor griechischen Buchstaben haben. 😉