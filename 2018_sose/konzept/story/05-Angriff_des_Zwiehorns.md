# Akt 5 - Der Angriff des Zwiehorns 🦄

Mit der Hilfe der Knotlinge überwanden die Novizen des Goldgorgonen mühelos die schwebenden Inseln der Highlands von Ja'va und endlich erhaschten sie einen Blick auf den majestätischen schiefen Berg und das Kloster darauf.

![Kloster in den Bergen](img\the-tigers-nest-171377_1920.jpg)

Doch kaum hatten sie wieder festen Boden unter den Füßen, krochen Schatten um den Berghang herum und es ertönt ein gespenstisches Wiehern.

"Ein Zwiehorn!" hörte man einen der Adepten rufen, kurz bevor ein pechschwarzer Hengst mit roten Augen und zwei spitzen Hörnern die Klostertreppen hinunter auf sie zugallopierte.

![Zwiehorn](img\zwiehorn.svg)

Entsetzte Schreie wurden laut und die Novizen stoben auseinander, während die Schatten nach ihnen griffen und in ihren Geist eindrangen.
Der Goldgorgone versucht sich der Höllenkreatur entgegenzustellen und leistet sich ein Blickduell mit ihr.
Seine Augen begannen golden zu glühen, während die der Kreatur in unheilvollem Blutrot funkelten.
Jeder, der sich zwischen die beiden gestellt hätte, wäre bestimmt sofort tot umgekippt.
Aber dann war es mit einem Augenblick vorbei und die Schultern des Goldgorgonen sackten herab, sein Blick wirkte nun matt und gebrochen.

Das Zwiehorn hatte seine grausige Fähigkeit eingesetzt, Zwietracht in den Herzen der Menschen zu streuen und den Goldgorgonen überzeugt, dass sein unterfangen Zwecklos war.
Auch den Adepten und Novizen ging es nicht viel besser.
Manche beschimpften sich gegenseitig, andere hockten apatisch in einer Ecke, weil sie die Schuld für alle Fehler, die auf der Reise hierhin passiert waren, plötzlich bei sich suchten.

Es gab nur eines, was dagegen helfen konnte: [Ehrliches Feedback](https://moodle.thm.de/mod/feedback/view.php?id=201873).
Alle Novizen würden sich zusammenreißen müssen, und noch einmal mit vereinten Kräften den `Evaluatio`-Zauber sprechen, so wie es ihnen der Goldgorgone mit dem Stakk beigebracht hatte.
Wenn Ihnen das gelänge, würden sie sicher gestärkt und um viele Erfahrungen reicher aus der Situation hinausgehen.
Wenn nicht, ... wer weiß, was für Schrecken Klausuchur dann noch auf sie loslassen könnte.
Womöglich würde er sie sogar an seine fleischfressenden nachtblühenden Immerfalschs verfüttern - den pervertierten Abbildern der Immerwahrs.

---

📖 TLDR in Klartext:

* Ich bitte Sie recht herzlich, an der Aktionsforschung für die Veranstaltung teilzunehmen. Ich habe dazu ein [anonymes Feedbackformular in Moodle](https://moodle.thm.de/mod/feedback/view.php?id=201873) eröffnet.
* Wenn bis zum 1.7. insgesamt mindestens 50 Antworten zusammenkommen, erhält der gesamte Kurs (d.h. jeder lebendige Kursteilnehmer) pro Antwort einen zusätzlichen XP. Bedenken Sie, dass das vor allem die Kursstufe noch einmal enorm steigern kann.
* Wenn bis zum 1.7. insgesamt weniger als 30 Antworten zusammenkommen, wird es in der Klausur nicht nur eine, sondern zwei Fragen zu Invarianten geben.
* Auch wenn Sie den Kurs nicht bis zum Ende besucht haben würde ich mich trotzdem sehr über ihr Feedback und ihre Verbesserungsvorschläge freuen. Insbesondere würde es mich natürlich interessieren, was Sie dazu bewogen hat, den Kurs abzubrechen und ob ich etwas hätte tun können, um das zu verhindern.