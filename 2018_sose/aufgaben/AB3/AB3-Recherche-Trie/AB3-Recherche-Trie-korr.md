# Korrekturschema:

## Kriterien fürs Nichtbestehen

* keine Quellenangabe
* keine vollständigen Sätze
* völlig unverständlicher Text
* über 800 Worte Text
* über 10 Quellen

## Punkteverteilung (100 gesamt)

* **12 Punkte** pro korrekt beantworteter Frage (**60 gesamt**)
* **20 Punkte** für fehlerfreien Text (-1 Punkt pro Fehler, die ersten 10 werden ignoriert)
* **20 Punkte** für verständliche Formulierungen
* **-20 Punkte** für das Überschreiten der Zeichengrenze
* **-20 Punkte** für unverlässliche/unpassende Quellen
* **-20 Punkte** für das Überschreiten der Quellengrenze