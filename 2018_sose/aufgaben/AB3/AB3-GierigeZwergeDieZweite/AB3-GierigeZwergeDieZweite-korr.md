# Bewertungsvorschläge

## Positive Faktoren:

  - Dozentron nimmt die Eingabe an.
  - Unit-Tests werden bestanden.
  - "Simulation" ist im Code erkennbar.

## Weiche Kriterien:

  - Es wird der richtige Zwerg bestimmt, aber nicht der richtige Wert. M.m.n. sinnvoll wären hier vll. 25% Abzug.
