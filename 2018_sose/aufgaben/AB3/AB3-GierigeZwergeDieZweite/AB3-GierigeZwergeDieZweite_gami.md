## Gierige Zwerge, die Zweite (25 %)

*Wie weithin bekannt ist, sind Zwerge sehr, sehr, sehr goldgierig. Aus diesem Grund läuft das "Wichteln" bei Zwergen ein wenig anders ab als üblicherweise. Beim normalen "Wichteln" wird ausgelost, wer wen beschenkt. Dann bekommt jeder Teilnehmer von dem Teilnehmer, der ihn gelost hat, ein Geschenk. Bei den Zwergen sind die Regeln ein wenig anders. Jeder Zwerg bringt zum "Wichteln" ein Geschenk in Form einer beliebigen Anzahl an Goldmünzen mit.*

*Dann läuft das Spiel wie folgt ab:*

*Die Zwergengemeinschaft setzt sich in einen Kreis. Der Zwerg der beginnt sucht sich den Nachbar aus, der die meisten Münzen hat und nimmt ihm diese ab. Dieser Nachbar scheidet sofort aus, wird also in Zukunft übersprungen. Danach geht es linksherum weiter und der nächste Zwerg im Kreis kommt an die Reihe. Dieser sucht sich wieder seinen reichsten Nachbarn aus und nimmt ihm die Münzen ab. Dieser Nachbar scheidet danach ebenfalls sofort aus.*
*Auf diese Art geht es immer weiter, bis nur noch ein Zwerg übrig ist.*

### Aufgabe

*Entwickeln sie einen Zauber, der das Zwergenwichteln simuliert. Dazu erhalten sie von Dozentron eine beliebige Anzahl Zeilen. In jeder Zeile steht eine Zahl, die die Anzahl Münzen eines Zwerges beschreibt.*

*Ihr Zauber soll nach der obigen Beschreibung bestimmen, welcher Zwerg am Ende übrig bleibt und wieviele Münzen er hat. Die Zwerge werden über die dazugehörigen Zeilennummern identifiziert.*

Geben sie die Zeilennummer und das erbeute Gold durch ein Semikolon getrennt an.

### Hinweise:

- Die Zeilen werden mit `0` beginnend nummeriert.
- Die Zwerge werden über ihre Zeilennummern identifiziert, d.h. Teilnehmer 0 ist der Teilnehmer, der mit der Anzahl Münzen in Zeile 0 beginnt.
- Die Teilnehmer (*die Zwerge*) sitzen im Kreis, es gibt also **immer** einen nächsten Nachbarn. Der letzte Teilnehmer (letzte Zeile der Eingabe) ist ein Nachbar des ersten Teilnehmers.
- "Der nächste Teilnehmer auf der linken Seite" ist hierbei der Teilnehmer mit der nächst geringeren Nummer.
- Der linke Teilnehmer von Teilnehmer 0 ist der Teilnehmer mit der höchsten Nummer.
- Ausgeschiedene Teilnehmer verlassen sofort den Kreis, sie werden also nicht weiter beachtet.
- Es gelten die üblichen Bedingungen für IO-Aufgaben.
- Die Teilnehmeranzahl ist immer größer als 0.
- Überlegen Sie sich eine Datenstruktur, die einen Kreis abbilden kann.

### Beispiel

*Fünf Zwerge nehmen am Wichteln teil:*

```
4
2
8
0
7
```
*Zwerg Nummer 0 beginnt. Seine Nachbarn sind die Zwerge Nummer 1 und 4. Zwerg 4 hat mehr Münzen, also gehen diese in den Besitz von Zwerg 0 über, der nun 11 Münzen besitzt*

```
11
2
8
0
X (Die Zeile ist hier mit X markiert, um zu zeigen, dass der Teilnehmer Ausgeschieden ist. Die Zeilennummern sollen aber konsistent bleiben.)
```

*Als nächstes wäre jetzt der Zwerg mit der nächst geringeren Nummer an der Reihe. Da zuletzt Zwerg 0 an der Reihe war geht es im Kreis zu Zwerg 4 weiter. Dieser ist allerdings schon ausgestiegen, also ist Zwerg 3 an der Reihe. Seine Nachbarn Sind die Zwerge 0 und 2. Der Erstgenannte hat mehr Münzen, also werden sie ihm abgenommen.*

```
X
2
8
11
X
```

*Nach dem selben Schema geht es dann weiter zu Zwerg 2:*

```
X
2
19
X
X
```

*Und zuletzt zu Zwerg 1:*

```
X
21
X
X
X
```

*Damit ist Zwerg 1 als letztes übrig und hat 21 Münzen.*

Damit lautet ihre Ausgabe:

```
1;21
```
