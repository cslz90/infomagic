package zipper;

public class Tuple<A, B> {

    private A val1;
    private B val2;

    public Tuple(A val1, B val2) {
        this.val1 = val1;
        this.val2 = val2;
    }

    public A _1() {
        return val1;
    }

    public B _2() {
        return val2;
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof Tuple &&
                val1.equals(((Tuple) other).val1)
                && val2.equals(((Tuple) other).val2);
    }


}
