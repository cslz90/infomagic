# Faule Orks im Röhrenrutschen-Wasserpark

### Die Olympischen Magiespiele 

*Die sonst so faulen Orks aus Ja'va sitzen am liebsten zu Hause in ihren Sesseln und schauen sich die Olympischen Magiespiele in ihren magischen Kristallkugeln an. Die beliebteste Sportart direkt nach dem Besenfliegen-Wettrennen ist das Zweier-Reif-Röhrenrutschenrennen. Diese Diziplin findet bisher aber nur in Haskellien und Scalandrien statt, da dort die einzigen Wasserparks sind. Die einzelnen Nationen des Zauberreichs treten immer in Zweier-Teams an. Die Strecke ist geformt wie ein `Y`. Am Anfang starten die zwei Mitglieder aus einem Team jeweils in einem Reif an ihrer Startposition. Bei der Mündung der zwei Streckenabschnitte befindet sich eine Röhre, in der die zwei Teammitglieder von ihren _zwei_ Reifen zusammen in _einen_ Reifen steigen müssen. Das erfordert sehr viel Koordination und wenn die Teilnehmer dies nicht bis zum Ende der ersten Röhre geschafft haben, sind sie disqualifiziert. Bei den diesjährigen treten wie immer die stärksten Zweierteams an. Auch Ja'va ist vertreten mit den Startnummern 5 und 6.*

*Zu Beginn sieht man zwei Listen. Eine enthält die Startnummern der einen Teampartner, die zweite Liste enthält die der anderen.*

```
{1,3,5,7}
{2,4,6,8}
```

*Nach dem Rutschen durch die erste Röhre ergibt sich eine Liste ergeben, die aus den Teampaarungen besteht.*

```
{(1,2),(3,4),(5,6),(7,8)}
```

*Ist die erste Röhre überwunden, ist es nicht mehr weit bis zum Ziel. Die Teams müssen jetzt nur noch durch eine letzte Röhre fahren. Diese ist jedoch verzaubert und kann mit den Teams allerlei anstellen, denn die Möglichkeiten der Zauberei sind grenzenlos. In diesem Beispiel wird der Zauber "adde numerare" angewandt, der die Startnummern der beiden Teammitglieder zusammenrechnet und der Teilnehmer mit der entsprechenden neuen Nummer mit dem Team im Reif ausgetauscht wird.*

*"adde numerare":*

```
(a, b) => a + b
```

*Nach dem Passieren der zweiten Röhre ergibt sich die folgende Liste:*

```
{3,7,11,15}
```

*Welches Team nun die beste Zeit hinlegt, gewinnt die Disziplin Zweier-Reif-Röhrenrutschenrennen.*

### Ein Wasserpark für Ja'va

*In diesem Jahr ist es zum ersten Mal so weit, dass zwei Ja'vanesen die Goldmedaille in dieser Disziplin holen. Die Begeisterung ist groß und die sonst so faulen Orks nehmen sich vor auch in Ja'va einen Wasserpark mit einer Röhrenrutsche zu errichten, damit ihre Idole aus dem einheimischen Team für die nächsten Olympischen Magiespiele auch in Ja'va trainieren kann und nicht immer die weite Reise bis nach Haskellien auf sich nehmen muss. Außerdem bietet ein solcher Wasserpark Spaß für die ganze Familie! So packen alle Orks mit an, um dieses Projekt auf die Beine zu stellen.*

*Da es in Ja'va keine Doppelreifen gibt, müssen erst welche hergestellt werden. Dabei müssen ein paar Hinweise des Herstellers `Tuple<A, B>` beachtet werden:*  
  
  * ein Konstruktor mit den Parametern `A val1` und `B val2`
  * zwei Getter-Methoden `A _1()` und `B _2()`
  * Die Methode `equals()` soll überschrieben werden. Zwei Tupel sind dann gleich, wenn `val1` von Tupel A mit `val1` von Tupel B übereinstimmt. Das gleiche gilt für `val2`.

*Nun geht es an die Arbeit. Das schwierigste sind die zwei Röhren, deshalb entwickeln die Orks folgende Zauber, die ihnen die Konstruktion dafür erleichtern soll:*  
  
```java
class Zipper {
  static <A, B> Stream<Tuple<A, B>> zip(Stream<A> stream1, Stream<B> stream2) { ... }
  static <A, B, C> Stream<C> zipWith(Stream<A> stream1, Stream<B> stream2, BiFunction<A, B, C> biFn) { ... }
}
```

Implementieren Sie die Methoden `zip` und `zipWith` aus der Klasse `Zipper`.

**Vorgehensweise:**

Die Methoden `zip` und `zipWith` sollen **lazy** implementiert werden. Um das zu erreichen benötigt man einige Klassen und Methoden, die Ihnen eventuell in Ihrem Studium noch nicht über den Weg gelaufen sind. Deshalb gibt es hier eine kleine Hilfestellung, um die Aufgabe dennoch bewältigen zu können:

1. Gegeben: zwei Streams, Ziel: zwei Iteratoren
2. Gegeben: zwei Iteratoren, Ziel: ein Iterator
3. Gegeben: Iterator, Ziel: Spliterator (Es gibt eine Klasse namens `Spliterators`. Diese besitzt eine Methode, die einen Iterator in einen Spliterator umwandeln kann. Dazu werden aber noch zwei weitere Argumente benötigt. Sie können hier zur Vereinfachung `Long.MAX_VALUE` für `size` und `0` für `characteristics` angeben)
4. Gegeben: Spliterator, Ziel: Stream (Es gibt eine Klasse namens `StreamSupport`. Diese besitzt eine Methode, die einen Spliterator in einen Stream umwandeln kann. Dazu wird aber noch ein weiteres Argument benötigt. Sie können hier zur Vereinfachung `false` für `parallel` angeben)

Die Klassen `Tuple` und `Zipper` gehören in ein gemeinsames Package namens `zipper`.

#### Hinweise

* Achten Sie darauf, dass sich die Klassen im richtigen Package befinden.
* Benennen Sie die Methoden genau wie in der Aufgabenstellung beschrieben und achten Sie auf korrekte Typen.
* Bei der Methode `zip` und `zipWith` kann Ihnen die oben beschriebene Vorgehensweise helfen. Nähere Infos zu den dort verwendeten Prinzipien im Vorlesungsskript oder in der Java-Dokumentation zu den entsprechenden Klassen.
* Sind die Streams unterschiedlich lang, zählt die Länge des kürzeren. Der Rest wird verworfen.
* Bei `zipWith` ist es nicht nötig eine komplett neue Methode zu implementieren. Hier kann `zip` verwendet werden.
* Was ist eine BiFunction? Sie haben bereits die funktionale Schnittstelle Function in Arbeitsblatt 0 Aufgabe 2.2 kennengelernt. BiFunction funktioniert dementsprechend mit dem Unterschied, dass ein Argument mehr übergeben wird. Für zusätzliche Infos können Sie auch hier in die [Java-Dokumentation](https://docs.oracle.com/javase/8/docs/api/java/util/function/BiFunction.html) schauen.

**Beispiel:**  

```java
BiFunction<String, Integer, String> buyFood = new BiFunction<>() {
    @Override
    String apply(String food, Integer count) {
        return food + ": " + count;
    }
}

```

[Abgabe in Dozentron](Link hier einfügen)