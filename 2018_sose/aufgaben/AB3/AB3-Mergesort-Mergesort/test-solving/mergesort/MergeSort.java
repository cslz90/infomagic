package mergesort;

public class MergeSort {

    private static int counter = 0;

    public static void sort(int[] arr) {
        sort(arr, 0, arr.length);
    }

    private static void sort(int[] arr, int from, int to) {
        counter++;
        if (to - from > 1) {
            sort(arr, from, (from + to) / 2);
            sort(arr, (from + to) / 2, to);
            merge(arr, from, (from + to) / 2, to);
        }
    }

    static void merge(int[] arr, int from, int middle, int to) {
        int[] temp = new int[to - from];
        int write = 0;
        int left = from;
        int right = middle;
        while (write < temp.length) {
            temp[write++] = (left < middle && (!(right < to) || arr[left] <= arr[right])) ? arr[left++] : arr[right++];
        }
        System.arraycopy(temp, 0, arr, from, temp.length);
    }

    static int getRecursionCounter() {
        return counter;
    }

    static void resetRecursionCounter() {
        counter = 0;
    }

}