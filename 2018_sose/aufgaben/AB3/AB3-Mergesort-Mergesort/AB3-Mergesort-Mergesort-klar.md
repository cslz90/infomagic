## MergeSort

Implementieren Sie Mergesort um ein Array zu sortieren. Dieser Algorithmus arbeitet nach dem Prinzip "Teile und Herrsche":

Er zerlegt das Array in zwei Teile, sortiert sie rekursiv und verschmilzt am Ende die Teile wieder (engl. merge).

Der folgende Code kann als Hilfe dienen:

```java
package mergesort;

public class MergeSort {

    public static void sort(int[] arr) {
        sort(arr, 0, arr.length);
    }

    // fromIndex inclusive, toIndex exclusive
    private static void sort(int[] arr, int fromIndex, int toIndex) {
        // ...
    }

    // first subarray: fromIndex inclusive, middleIndex exclusive
    // second subarray: middleIndex inclusive, toIndex exclusive
    static void merge(int[] arr, int fromIndex, int middleIndex, int toIndex) {
        // ...
    }

}
```

Zählen Sie außerdem alle rekursiven Aufrufe von `sort` und Implementieren Sie hierfür folgende Methoden ebenfalls in der Klasse `MergeSort`:

```java

    static int getRecursionCounter() { // gibt die Anzahl der rekursiven Aufrufe zurück

    }

    static void resetRecursionCounter() { // setzt den Rekursionszähler wieder auf 0

    }

```

**Getestet werden vier Methoden**:
* die Methode `sort`, die ein `int`-Array übernehmen und sortieren muss
* die Methode `merge`, die zwei sortierte `arr`-Subarrays von `fromIndex` bis `middleIndex` und von `middleIndex` bis `toIndex` verschmilzt.
* die Methode `getRecursionCounter`, die die Anzahl der rekursiven Aufrufe von `sort` mitzählt.
* die Methode `resetRecursionCounter`, die den Rekursionszähler wieder auf 0 setzt.

**Hinweise**:
* In den Vorlesungsfolien werden Sie auch weitere Informationen zu "Teile und Herrsche" und "MergeSort" finden.  
* Beachten Sie, dass untere Grenzen immer inklusive und obere Grenzen immer exklusive gegeben sind. Ein Beispiel zur Methode `sort`: Das erste Element ist `arr[fromIndex]`, das letzte Element ist `arr[toIndex - 1]`.
* Bei der Implementierung der Methode `merge` kann ein temporäres Array zur Hilfe genommen werden. 