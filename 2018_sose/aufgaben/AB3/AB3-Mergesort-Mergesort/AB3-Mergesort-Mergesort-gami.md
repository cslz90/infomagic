## Mergesort

*Wie Sie inzwischen bestimmt wissen, sind Basilisten sehr ordentliche Datenkreaturen und lassen sich liebend gerne sortieren. Eine recht effiziente Form dies zu bewirken, ist der Zauber "generis confundantur", den man umgangssprachlich auch als "MergeSort" bezeichnet.*

*Bei diesem Zauber werden die Basilisten erst vorsichtig aufgetrennt, um sie nachher in entsprechender Reihenfolge wieder zusammenzuführen.*

*Da ein einzelner Kopf eines Basilisten in Panik verfallen würde, wenn man ihn vom Rest trennen würde, versucht man immer möglichst gleichmäßig aufzuteilen. Zudem lässt man üblicherweise ca. zwei Köpfe beisammen, um die Kreaturen nicht zu sehr zu stressen.*

*Um unnötigen Stress für Novizen und Basilisten zu vermeiden, wird dieser Zauber erst einmal auf Arrays geübt. Diese sind vor allem den ArrayBasilisten sehr ähnlich, jedoch sind sie von der Handhabung viel einfacher.*

*Aus der Thaumaturgischen Hochschule der Magie haben Sie den groben Aufbau eines Merge-Zaubers erfahren. Nun liegt es an Ihnen diesen Zauber zu perfektionieren, damit Sie eines Tages auch auf das Sortieren von ArrayBasilisten mit "generis confundantur" umsteigen können. Bedenken Sie hierbei, dass es sich um einen Spruch "Rekursio" handelt.*

```java
package mergesort;

public class MergeSort {

    public static void sort(int[] arr) {
        sort(arr, 0, arr.length);
    }

    // fromIndex inclusive, toIndex exclusive
    private static void sort(int[] arr, int fromIndex, int toIndex) {
        //
    }

    // first subarray: fromIndex inclusive, middleIndex exclusive
    // second subarray: middleIndex inclusive, toIndex exclusive
    static void merge(int[] arr, int fromIndex, int middleIndex, int toIndex) {
        //
    }

}
```

*Desweiteren sollen Sie mitzählen, wie häufig ihr Spruch "Rekursio" gewirkt wird. Da Ihr Kopf allerdings Platz braucht, um die tollen Lehren dieses Kurses aufnehmen zu können, verschieben Sie diesen geistigen Aufwand auf zwei weitere Zaubersprüche:*

```java

    static int getRecursionCounter() { // gibt die Anzahl der rekursiven Aufrufe zurück

    }

    static void resetRecursionCounter() { // setzt den Zähler wieder auf 0

    }

```

[Link zur Abgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/67)
