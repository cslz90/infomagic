package mergesort;

import java.util.Arrays;

// source: http://www.java2novice.com/java-sorting-algorithms/merge-sort/
public class MergeSort {
    static private int[] array;
    static private int[] tempMergArr;
    static private int length;
    static private int counter;

    public static void main(String a[]){

        int[] inputArr = {45,23,11,89,77,98,4,28,65,43};
        MergeSort mms = new MergeSort();
        mms.sort(inputArr);
        for(int i:inputArr){
            System.out.print(i);
            System.out.print(" ");
        }
    }

    public static void resetRecursionCounter() {
        counter = 0;
    }

    public static int getRecursionCounter() {
        return counter;
    }

    static public void sort(int inputArr[]) {
        array = inputArr;
        length = inputArr.length;
        tempMergArr = new int[length];
        doMergeSort(0, length);
    }

    static private void doMergeSort(int lowerIndex, int higherIndex) {
        counter++;
        //System.out.println("Sort "+lowerIndex+"-"+higherIndex);
        if (lowerIndex < higherIndex - 1) {
            int middle = lowerIndex + (higherIndex - lowerIndex) / 2;
            // Below step sorts the left side of the array
            doMergeSort(lowerIndex, middle);
            // Below step sorts the right side of the array
            doMergeSort(middle, higherIndex);
            // Now merge both sides
            merge(array, lowerIndex, middle, higherIndex);
        }
        //System.out.println("Sorted ("+lowerIndex+"-"+higherIndex+") "+Arrays.toString(Arrays.copyOfRange(array, lowerIndex, higherIndex)));
    }

    public static void merge(int[] array, int lowerIndex, int middle, int higherIndex) {

        for (int i = lowerIndex; i < higherIndex; i++) {
            tempMergArr[i] = array[i];
        }
        int i = lowerIndex;
        int j = middle;
        int k = lowerIndex;
        while (i < middle && j < higherIndex) {
            if (tempMergArr[i] <= tempMergArr[j]) {
                array[k] = tempMergArr[i];
                i++;
            } else {
                array[k] = tempMergArr[j];
                j++;
            }
            k++;
        }
        while (i < middle) {
            array[k] = tempMergArr[i];
            k++;
            i++;
        }

    }
}
