# Binäre Suche

*Seit Ihrem letzten Besuch im Dorf Ihres befreundeten Orks, haben die Orks an ihren kryptografischen Systemen gearbeitet. Da diese sich lange Passwörter, wie beispielsweise "grunz", nicht merken können, haben sie diese nun durch Zahlen ersetzt. Die Wachen wechseln das Passwort täglich. Das jeweils neue Passwort wird von ihnen jeden Morgen aus dem Zahlenbereich 0 (inklusive) bis 1024 (exklusive) ausgewürfelt. Leider ist den Wachen nicht bewusst, dass ihr kryptografisches System eine Sicherheitslücke hat: Sie haben vergessen festzulegen, dass man Fremden gegenüber immer nur mit "Ja" oder "Nein" antworten sollte. So kommt es nicht selten vor, dass eine Wache mit "Ja", "Nein, mein Passwort ist kleiner" oder "Nein, mein Passwort ist größer" antwortet.*

*Die Orks haben vergessen, Sie über diese Änderungen aufzuklären. Um dennoch Ihren befreundeten Ork besuchen zu können, müssen Sie den Wachen wie gewohnt die Passwörter aufsagen. Keine Sorge: Wie gehabt, vergessen diese schnell wieder, dass Sie bereits ein Passwort genannt haben. Allerdings können Sie nicht mehr einfach alle Passwörter aufzählen. Da die Wachen das Passwort aus Sicherheitsgründen täglich ändern, vergessen sie es regelmäßig selbst. Um es nachzuschlagen muss eine der Wachen zur Garnison ans andere Ende des Dorfes laufen. Würden Sie alle möglichen 1024 Passwörter aufzählen, wäre ein ganzer Tag vergangen. Somit wäre auch das Passwort wieder ein anderes.*

*Ihr befreundeter Ork hat, genau wie die Wachen, Probleme, sich die Passwörter zu merken. Sein schlechtes Gedächtnis macht er aber mit Einfallsreichtum wieder wett. So kommt es, dass er eine Strategie entwickelt hat, mit der man möglichst schnell an das Passwort kommt.*

*Er ist ziemlich schnell im Kopfrechnen und kann sich gut merken, dass das Passwort zwischen 0 und 1024 (exklusive) liegt. Um mit jedem Schritt näher ans Ziel zu kommen, fragt er erst, ob die Zahl der mittlere Wert aus der Menge ist. Je nach Antwort der Wachen kommt er entweder durch und weiß, dass er nochmal die Hälfte der letzten dazu gerechneten Zahl hinzufügen muss (Passwort > Zahl) oder die Hälfte dieser Zahl abziehen muss. In anderen Worten: immer den Mittelwert zwischen seiner ermittelten Ober- und Untergrenze.*

*So könnte er z.B. 512 + 256 - 128 + 64 rechnen um sich an die Zahl 704 anzunähern.*

[Abgabe in Dozentron](http://dozentron.mni.thm.de/input_output_tasks/66)
