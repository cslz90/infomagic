# Binäre Suche

Ihre Aufgabe ist es, eine rein zufällig ausgewählte Zahl zwischen 0 (inklusive) und 1024 (exklusive) herauszufinden. Nach jeder Abgabe wird Ihnen mitgeteilt, ob die gesuchte Zahl größer oder kleiner ist als die von Ihnen angegebene. Ist Ihre Zahl die zufällig ausgewählte, haben Sie die Aufgabe bestanden. Wenden Sie hierfür die Strategie der "Binären Suche" an.

Den verlinkten Input dürfen Sie ignorieren. Dieser ist für Sie irrelevant und nur ein Schönheitsfehler von Dozentron.

Auch ein Quellcode muss nicht abgegeben werden.
