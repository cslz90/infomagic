# Die Höhlentour der Zwerge

*Um ihre Gier nach Reichtümern stillen zu können, dringen waghalsige Zwergenkundschafter immer tiefer in unterirdische Höhlen vor. Dort suchen sie nach Vorkommen von Edelmetallen wie Gold oder Mithril. Leider sind diese Abenteuer nicht ungefährlich: In der Tiefe lauern verschiedenste Kreaturen, die Zwerge zum fressen gern haben.*

*Natürlich wissen sich die Zwerge zu wehren und sie sind nicht dafür bekannt Feiglinge zu sein. Außerdem überwiegt der Rausch des Goldes häufig die Vorsicht. Zwerge verfügen über einen zusätlichen Sinn, der sie Edelmetalle schon auf große Distanzen wahrnehmen lässt. So können sie es schon am Eingang einer Höhle regelrecht in der Luft spüren, ob sich in der Tiefe lohnenswerte Vorkommen finden. Ist der Sinneseindruck zu stark, können Zwerge in einen Rausch fallen, der sie fast alle Gefahren vergessen lässt.*

*Die begehrteste Beute sind Drachenschätze. Leider ist es Gleichzeitig auch die gefährlichste. Und so berauschend dieser Schatz auch sein mag: Nur die mutigsten Zwerge wagen sich in eine Höhle, an deren Eingang Drachenspuren zu erkennen sind. Dagegen werden kleine Kreaturen wie Goblins kaum einen Zwerg vom Betreten einer Höhle abhalten.*

*Zwerge sind allerdings nicht nur goldgierige Abenteuer, sondern auch leidenschaftlich Chronisten und beizeiten fasziniert von der Algomantik. Sie sind ein Gehilfe des berühmten Gelehrten Hortswerk. Dieser hat ihnen aufgetragen eine gigantische Sammlung an Aufzeichnungen über vergangene Höhlenerkundungen auszuwerten und eine Möglichkeit zu finden, Voraussagen für kommende Abenteuer zu treffen.*

*In den alten Schriften ist für jede vergangene Erkundung detailliert festgehalten, wie hoch die Erwartungen bezüglich der zu erwartenden Schätze und der möglichen Gefahren beim betreten der Höhle waren. Außerdem gibt es ausführliche Beschreibung zu den jeweiligen Kundschaftern. Insbesondere Relevant für die ihnen übertragene Aufgabe ist der Mut der einzelnen Zwerge.*

*Die Menge an Daten, die sie zusammengetragen haben ist gigantisch es scheint aussichtslos sie ohne Hilfe auszuwerten. Deshalb entscheiden sie sich einen Zauber zu entwickeln, der ihnen helfen soll. Dieser Zauber arbeitet mit einem Satz Daten aus vergangenen Tagen und Informationen zu einer neuen Höhle und dem Expeditionstrupp. Anhand dessen soll vorausgesagt werden, ob die Zwerge vor Ort die Erkundung für lohnenswert erachten.*
