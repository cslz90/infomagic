# Linked List
### Dauer: ca. 30 Min. (3 XP)

Erinnern Sie sich an ihre Notizen zu Funktionen für diese Aufgabe.

```haskell
-- a custom list type
-- either empty (Nil) or an element and the rest of the list (Cons element tail)
data Lst a = Nil | Cons a (Lst a)
```

> Hinweis: `--` leitet in Haskell einen Kommentar ein


## Aufgabe 1.1
Implementieren Sie die oben stehende Haskell Variante einer verketteten Liste in Java.
Die Liste sollte sich im Package `linkedlist` befinden und dem folgenden Interface genügen:

```java
public interface ImmutableList<A> {
	/** Prepends elem infront of this list */
	ImmutableList<A> cons(A elem);
	/** Appends elem at the end of this list. */
	ImmutableList<A> append(A elem);
	/** Returns the size of the list. */
	int size();
	/** Returns the element at the index idx. */
	A getAt(int idx) throws IndexOutOfBoundsException;
}
```

**Hinweise**
> - Definieren Sie 2 Subklassen, die das Interface `ImmutableList` implementieren. Einmal die leere Liste `Nil` und die nicht-leere Liste `Cons`.
> - Die Liste sollte im Speicher wie folgt aussehen: `[5]->[3]->[10]->Nil`. (Die Zahlen sind Beispielwerte. Elemente, Elementtypen und Längen der Liste sind variabel zu halten.)
> - Beachten Sie, dass auch die Methoden `toString` und `equals` überschrieben werden müssen.  

*Beispiel:* Wenn im Speicher `[5]->[3]->Nil` steht, sollte `toString` folgendes zurückgeben: `Cons(5, Cons(3, Nil))`

## Aufgabe 1.2
Erweitern Sie das Interface `ImmutableList` um eine `map()` Methode:

```java
	<B> ImmutableList<B> map(Function<A,B> fn);
```

`map` wendet auf jedes Element der Liste die gegebene Funktion an.
Sie könnte wie folgt verwendet werden:

```java
ImmutableList<Integer> lst = ...;
//functional (Java 8) style
ImmutableList<Integer> doubled = lst.map(i -> i*2);
//anonymous class alternative
ImmutableList<Integer> doubled = lst.map(new Function<Integer,Integer>(){
	@Override Integer apply(Integer i) {
		return i*2;
	}
});
```

**Hinweis**
> Der Ausdruck `i -> i*2` ist ein [Closure/Lambda](https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html).
Also eine anonyme Funktion, die der Methode `map` übergeben wird.


<!-- TODO: Wir müssen hier beim nächsten Mal darauf hinweisen, dass die Liste immutable sein muss und was das bedeutet. -->