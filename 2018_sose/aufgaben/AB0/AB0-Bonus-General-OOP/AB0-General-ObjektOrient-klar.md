# Objektorientierung
### Dauer: ca. 120 Min. (30 XP)

## Aufgabe 1
Definieren Sie im Package `oop` eine Klasse Person mit den Feldern `name`, `vorname` und `alter`. Überschreiben Sie die Methoden `equals`, `hashCode` und `toString`.

>In der Regel ist es keine gute Idee, selbst einen HashCode zu errechnen. Java bietet für eigene hashCode-Implementierungen Hilfsmethoden, wie z.B. `Objects.hash` an.

## Aufgabe 2
Machen Sie die Klasse Person vergleichbar, in dem Sie [Comparable](https://docs.oracle.com/javase/9/docs/api/java/lang/Comparable.html) implementieren. Überlegen Sie sich, anhand welcher
  Eigenschaft eine Person verglichen werden kann.

## Aufgabe 3
Erstellen Sie 2 Subklassen von Person: `Student` und `Professor`.
Ein Student hat zusätzlich eine **Matrikelnummer** und ein Professor einen **Titel**.

## Aufgabe 4
Was bewirkt der `final`-Modifizierer vor Klassen?

```java

    public final class Foo {}

```

## Aufgabe 5
Im zweiten Beispiel handelt es sich um den `final`-Modifizierer vor einer Methode.

Was bewirkt der `final`-Modifizierer vor Methoden?

```java

    public final void foo();

```

## Aufgabe 6
Was bewirkt der `static`-Modifizierer vor **geschachtelten** Klassen?
  Wie würden Instanzen von `Bar` erzeugt werden, wenn das `static` fehlt?

```java

	  public class Foo {
	    static class Bar {
	    }
	  }

```

## Aufgabe 7
Was bewirkt '...' hinter dem generischen Datentypen `A`?

```java

 	List<A> toList(A... args);

```

>Werfen Sie einen Blick in die [ObservableList](https://docs.oracle.com/javase/9/docs/api/javafx/collections/ObservableList.html), um einige Beispiele hierfür zu finden.

## Aufgabe 8
Welche Sichtbarkeit hat die Variable `x` im folgenden Beispiel?  
Welche Probleme können auftreten?

```java

    public class X {
      int x;
    }

```
