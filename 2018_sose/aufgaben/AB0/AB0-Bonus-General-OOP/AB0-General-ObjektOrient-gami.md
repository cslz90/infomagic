# Objektorientierung
### Dauer: ca. 120 Min. (30 XP)

## Aufgabe 1
*Ein Novize aus Funktionalia hat von Ihrer Reise durch den Objektorient gehört und will mehr über diesen, für ihn fremden, Ort hören.
Um ihm zu erklären wie die Einheimischen sind, zeigen Sie ihm die Java-Klasse Person im Paket `oop`, die `name`, `vorname` und `alter` enthält. Auf dieser kann mit den Sprüchen  `equals`, `hashCode` und `toString` gearbeitet werden.*

>*Sie erinnern sich, einmal beim Essen im Speisesaal den hohen Magiern bei einer Unterhaltung gelauscht zu haben, die über den Spruch `hashCodes` und seine Komplexität  sprachen. Dabei war die Rede davon, dass Ja'va eigene Hilfszauber beherberge, die einem das Zaubern erleichtern könnten. So etwa der Spruch `Objects.hash`.*

## Aufgabe 2
*Um zu verdeutlichen wie sehr sich die Einwohner von Ja'va aneinander messen, machen Sie ihre Klasse zusätzlich [Comparable](https://docs.oracle.com/javase/9/docs/api/java/lang/Comparable.html).*

## Aufgabe 3
*Fasziniert von Ihrer Erzählung bittet der Novize Sie, ihm auch noch etwas von den dortigen Studenten und Professoren zu erzählen.
Erstellen sie hierzu zwei weitere Klassen die von `Person` abgeleitet sind.
Ein `Student` hat zusätzlich eine __Matrikelnummer__ und ein `Professor` einen __Titel__.*

## Aufgabe 4
*Beeindruckt von Ihrem Wissen fragt er nun, ob Sie ihm einige Verwendungen von `final` erläutern können, auf die er gestoßen ist während er selbst über den Objektorient nachgelesen hatte.
Im ersten Fall handelt es sich um den `final`-Modifizierer vor einer Klasse.
Was bewirkt dieser?*

```java

    public final class Foo {}

```

## Aufgabe 5
*Im zweiten Beispiel handelt es sich um den `final`-Modifizierer vor einer Methode.*

*Was bewirkt der `final`-Modifizierer vor Methoden?*

```java

    public final void foo();

```

## Aufgabe 6
*Nun sind auch Sie ins Schwärmen geraten und erzählen ihm ohne weitere Aufforderung, wozu man den `static`-Modifizierer bei __geschachtelten__ Klassen verwendet und welchen Aufwand man betreiben muss, um ein Objekt aus einer geschachtelten Klasse zu erzeugen, die nicht mit `static` gekennzeichnet ist.*

```java

	  public class Foo {
	    static class Bar {
	    }
	  }

```

## Aufgabe 7
*Als Sie mit Ihrem Exkurs fertig sind, fragt er Sie noch zu einem weiteren Phänomen, das er während seiner Recherche entdeckt hat.
Für den Sinn von '...' in folgendem Beispiel konnte er keine gute Erklärung finden.*

```java

  	List<A> toList(A... args);

```

>Werfen Sie einen Blick in die [ObservableList](https://docs.oracle.com/javase/9/docs/api/javafx/collections/ObservableList.html), um einige Beispiele hierfür zu finden.

## Aufgabe 8
*Um ihm auch die Schattenseiten des Objektorients zu offenbaren, zeigen Sie ihm zum Abschluss noch das Problem der default Sichtbarkeit von Variablen in Ja'va anhand des folgenden Beispiels.*

```java

    public class X {
      int x;
    }

```
