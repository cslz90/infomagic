# [Bonus] Bonsai-Hydrachen
Dauer: ca. 15 Min. (3 XP) ---> viel zu wenig

#### Fütterung der Bonsai-Hydrachen

*Um auch nicht magiebegabten Wesen einen Einblick in unsere magische Welt zu gewähren, fertigen Novizen der Thaumaturgischen Hochschule der Magie kleine Souvenirboxen, in denen kleine Baumhydren (sogenannte Bonsai-Hydrachen) leben, an. Diese Wesen sind besonders handzahm und umgänglich, erfordern jedoch eine strenge Ernährung.*

*Bonsai-Hydrachen ernähren sich von magischen Einsen und Nullen. Dabei dürfen sie jedoch niemals nach Mitternacht gefüttert werden oder 2 Nullen hintereinander fressen.*
*Da die Erklärung, weshalb ein Bonsai-Hydrache keine zwei Nullen fressen darf, den Verstand eines nichtmagischen Wesens übersteigt, muss eine magische, einfache Lösung für alle nichtmagischen Besitzer der Hydrachen gefunden werden.Diese muss aus einer gegebenen Menge magischer Einsen und Nullen die Anzahl der möglichen gesunden Kombinationen berechnen.*

*So weiß das nichtmagische Wesen,dass die Fütterung unbedenklich ist.Um auch im Objektorient verkauft werden zu können, muss die Box gemäß MJN (magische Ja'va Norm) rekursiv aufgebaut sein.*

__Hinweise:__

>  *Zeichnen Sie einen Essensplan für die Bonsai-Hydrachen, in dem jede Mahlzeit einer magischen 0 oder 1 entspricht (jedes Menü stellt eine Kombination der angegebenen Nullen und Einsen dar).*
>*Viele Novizen der Thaumaturgischen Hochschule der Magie bevorzügen es ihren Essensplan so zu zeichnen, dass er dem Aufbau einer binärborkigen Speichereiche gleicht, damit der Plan übersichtlicher wird.*
*Was stellen dann die Früchte, deren Weg von der Wurzel `a` Null-Äste und `b` Eins-Äste enthält, dar?*
> *Betrachten wir den Essensplan einer der Adepten: `a = 1, b = 2`, die Länge der gesuchten gesunden Kombinationen ist also `3`.*
>*An der ersten Stelle der Kombination kann entweder eine magische Null oder Eins stehen.*
>*Der Rest der Kombination hat die Länge `2` und kann als ein neuer Essensplan mit den Parametern `a = 0, b = 2` bzw. `a = 1, b = 1` betrachtet werden.*
>*Auf diese Weise wird der Essensplan in zwei einfachere Pläne zerlegt, die man durch den Spruch Rekursio erhalten kann.*


[Abgabe in Dozentron](Link hier einfügen)
