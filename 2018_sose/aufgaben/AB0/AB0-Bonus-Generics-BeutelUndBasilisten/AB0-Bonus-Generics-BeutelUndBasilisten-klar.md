#[Bonus] Generics
### Dauer: ca. 40 Min (10 XP)

## Aufgabe 1
Definieren Sie eine generische Funktion `max`, die das Maximum einer beliebigen Kollektion von vergleichbaren (comparable) Elementen liefert.

## Aufgabe 2
Definieren Sie eine generische Funktion `min`, die das Minimum einer beliebigen Kollektion von vergleichbaren (comparable) Elementen liefert.


## Aufgabe 3
Die Funktion [Collections.copy](https://docs.oracle.com/javase/7/docs/api/java/util/Collections.html#copy(java.util.List,%20java.util.List))
    hat die folgende Signatur:

  		public static <T> void copy(List<? super T> dest,List<? extends T> src)

  * Wieso ist der generische Typ von `dest` `? super T` und nicht `T` ?
  * Wieso ist der generische Typ von `src` `? extends T`?
  * Was bedeutet überhaupt `? super T` und `? extends T`?

## Aufgabe 4

Was ist **Kovarianz** und **Invarianz**?

## Aufgabe 5
Sind generische Typen `List<A>` *Kovariant* oder *Invariant*? Wieso sind sie es?
  Ist die Zuweisung `List<Object> xs = new ArrayList<Animal>()` valide?

## Aufgabe 6
Verhalten sich Arrays genauso? `Object[] objs = new Integer[5];`
