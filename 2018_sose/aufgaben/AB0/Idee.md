#Auswahl der Aufgaben
##Vorwort
**Update: 27.03.2018, 11:45**  
Es ist textlich (fast) alles Konform mit unseren Vorgaben.  

Die Aufgabe *ArrayBasilist* benötigt noch weitere Veränderung, da sie  
noch Verweise auf die Folien enthält und sie Folienunabhängig werden sollte.

**Update: 21.03.2018, 01:03Uhr**  
Im folgenden eine Liste der Aufgaben für das Arbeitsblatt 0, die man für das nächste Semester verwenden könnte.  
Sie sind sortiert nach Pflicht und Bonusaufgaben. 
 
Leider entsprechen viele von ihnen nicht unseren Vorgaben, weshalb eine Nachbearbeitung notwendig ist.  
Auch die Zeitangaben werden ebenfalls noch überdacht.  

Auf dem Repo befinden sich alle Aufgaben, die bisher von mir angepasst wurden und werden im Folgenden mit dem Text **"Überarbeitet"** markiert.  

##Pflicht:
###Collections

OrkGrunzer **(Überarbeitet)**

###General  

OOP **(Überarbeitet)**

###Generics

ArrayBasilist **(Muss noch Folienunabhängig geschrieben werden!)**

###FileIO

VerteidigungGegenMagischeWesen  **(Überarbeitet)**

###LinkedList

ImmutableList **(Überarbeitet)**

###Regex

Besenfliegen **(Überarbeitet)**

###Rekursion

Zauberkiste **(Überarbeitet)**

##Bonus

###Collections

Basilistendiät **(Überarbeitet)**

###General  

MagicalPot **(Überarbeitet)**

###Generics

BeutelUndBasilisten **(Überarbeitet)** 

###FileIO

MagicalDRYCounter 

###LinkedList

ImmutableList **(Überarbeitet)**

###Regex

MagicalDRYCounter

###Rekursion

BonsaiDrachen **(Überarbeitet)**