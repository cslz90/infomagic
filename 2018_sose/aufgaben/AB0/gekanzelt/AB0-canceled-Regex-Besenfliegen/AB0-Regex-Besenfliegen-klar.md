# Reguläre Ausdrücke (Regex)
### Dauer: ca. 10Min (1 XP)

Gegeben sei der folgende reguläre Ausdruck in Java-Syntax:

  	([A-Z]{1,3})-([A-Z]{1,2})-(\\d{2,4})

Welche der folgenden Zeichenketten ist valide?

	  test
	  test-blup-930
	  B-ML-930
	  VB-DE-20
	  VB-DE-2056798
	  GI-LI-2004
	  GI-T-2005


