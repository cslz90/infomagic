# File IO
## Dauer: ca 25 Min. (3 XP)

Lesen Sie die folgende strukturierte Datei (`personen.csv`) in eine Java Liste ein:

	Nicolarius, 22
	Alissiara, 19
	Denneris, 17
	Christopherus, 30
	Anhara, 18
	Alcaperus, 7
	Prunak, 80

Definieren Sie eine geeignete Klasse, die die Felder der `personen.csv` Datei enthält.

Filtern Sie alle Personen heraus, die jünger als 18 sind.  

Schreiben Sie die resultierende Liste in eine neue Datei (`neue-personen.csv`).
> Hinweis: Achten Sie auf das Encoding (UTF-8) der Datei
