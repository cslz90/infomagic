[Bonus] Aufgabe File IO:    (geschätzte Dauer: 45 min. => 11 XP)

#### Wie viel Magie ist erträglich?

Bitte lesen Sie die bereitgestellte [File auf Glidamesh]() als FileInputStream in Java ein.
Verwenden Sie reguläre Ausdrücke, um den Text nach :
 * Worten und Wortverbindungen mit "magisch" und "zauber"
 * allen Code-Schnipseln (keine ganzen Blöcke)
 * Links
 
  zu suchen.

Erstellen Sie hierzu eine Klasse mit dem Namen `RegexScan` im package `fileio`.
Entwickeln Sie in der Klasse eine Prozedur mit `readEntireFile`, die den gesamten Inhalt der Datei mit dem übergebenen Dateipfad `filePath` einliest und als String zurückgibt.
Entwerfen sie eine zweite Prozedur (ebenfalls in der Klasse `RegexScan`) mit dem Namen `numerare`, die im übergebenen String alle Vorkommnisse des ebenfalls übergebenen Musters `pattern` sucht und diese zählt.

>Hinweise:
- Das Pattern `pattern`, das der Prozedur `numerare` gegeben wird, ist ein valider Regex.
- Stellen Sie Ihr Charset für das Einlesen der Datei auf UTF-8!
- Wenn beim Lesen der Datei eine Ausnahme (Exception) auftritt, soll die Prozedur `readEntireFile` `null` zurückgeben.
- Bitte nicht wundern: Der einzulesende Text ist aus den Aufgabenstellungen zusammengesetzt.
