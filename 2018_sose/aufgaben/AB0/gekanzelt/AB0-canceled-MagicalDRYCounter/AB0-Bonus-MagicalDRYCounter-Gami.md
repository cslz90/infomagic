[Bonus] Aufgabe File IO:	(geschätzte Dauer: 45 min. => 11 XP)

#### Wie viel Magie ist erträglich?

*Der technische Fortschritt hält auch in unserer magischen Hochschule Einzug. Jeder Novize läuft heute mit einem scheinbar magischen flachen Quader in der Hand über den Campus und zeigt seine übermäßige Zuneigung durch viel Berührung und Blickkontakt.
Uns wurde nach Rücksprache mit vertrauenswürdigen Vertretern der Thaumaturgischen Hochschule für Magie mitgeteilt, dass es diese Geräte auch bei ihnen gibt, und dass diese ganz ohne Magie funktionieren. Die Novizen haben dadurch die Möglichkeit, neben der Pflege ihrer sozialen Kontakte und dem Betrachten von Katzenvideos, ebenso wichtige Informationen für ihr Studium zu recherchieren.
Dies geht auf diese moderne Weise sehr schnell, da die Novizen anhand von Schlagwörtern im Text suchen können, ob sich das gewählte Schriftstück oder die gefundene Passage als Quelle der Erkenntnis eignet.*

*Weil unsere magische Hochschule stets bemüht ist, dass gerade unsere Informagie-Novizen modernste magische Technologien und Wirkungsweisen von Grund auf verstehen, bitten wir Sie, als ersten Feldversuch den Text, den wir Ihnen in Form einer zusätzlichen Datei zur Verfügung stellen, einzulesen. Durchsuchen Sie mit Hilfe von regulären Ausdrücken nach folgenden Besonderheiten im Text:*

* *"magisch"-Wortkomposition*
* *"zauber"-Wortkomposition*
* *magische Tunnel* (Links)
* *Code-Schnipsel* (keine ganzen Blöcke)

*Entwickeln Sie einen Zauberspruch `numerare`, welcher ein Pattern aus Stringnattern, sowie den zu durchsuchenden Text übernimmt und die Häufigkeit der gesuchten Besonderheiten ermittelt. Zudem sollten einige Formalia ja'vajanischen Beamten während des Lösens der Aufgabe betachtet werden:*

 * *die Aufgabe sollte innerhalb eines Paketes `fileio` mit der Klasse `RegexScan` gelöst werden*
 * *implementieren Sie innerhalb ihrer Klasse den Zauberspruch `readEntireFile`, welche den Beispieltext  für ihren Zauberstab lesbar aufarbeitet und einließt*

>Hinweis zur Aufgabe:
- Bitte stellen Sie, beim Einlesen, Ihr CharSet auf UTF-8
- Der textverarbeitende Zauberspruch `readEntireFile` sollte bei etwaigen Komplikationen `null` zurückgeben
- Lassen Sie sich nicht beirren: Der einzulesende Text ist aus den Aufgabenstellungen zusammengesetzt.

[File auf Glidamesh]()
[Abgabe in Dozentron, link noch einfügen]()
