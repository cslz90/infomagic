# Regex Besenfliegen
### Dauer: ca. 10Min (1 XP)

*Auf der Thaumaturgischen Hochschule der Magie lernen sie natürlich auch einen Besen zu fliegen, dem offiziell anerkannten Transportmittel eines jeden Magiers und folglich auch Novizen. Zwar bietet das Reisen durch die Lüfte durchaus seine Reize, doch ist gerade bei Flügen über Java eine Zulassung erforderlich. Für den Erwerb einer solchen Zulassung benötigen sie eine genormte Identifikationsplakette nach folgendem Muster.*


  	([A-Z]{1,3})-([A-Z]{1,2})-(\\d{2,4})

*Ein eher zwielichtiger Goblin bietet ihnen folgende Identifikationsplaketten zu einem verlockend niedrigen Preis an. Sind diese in Java zulässig?*

	  test
	  test-blup-930
	  B-ML-930
	  VB-DE-20
	  VB-DE-2056798
	  GI-LI-2004
	  GI-T-2005


