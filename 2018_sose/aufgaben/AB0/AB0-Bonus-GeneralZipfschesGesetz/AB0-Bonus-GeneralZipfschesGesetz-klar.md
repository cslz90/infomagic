[file io / regex / collections / generics] [2 Stunden]

## Zipfsches Gesetz

Thema der Aufgabe ist [das Zipfsche Gesetz](https://en.wikipedia.org/wiki/Zipf's_law) und seine Implementierung. Das Gesetz kann folgendermaßen beschrieben werden:  Wenn die Elemente einer Menge – beispielsweise die Wörter eines Textes – nach ihrer Häufigkeit geordnet werden, ist die Wahrscheinlichkeit ihres Auftretens umgekehrt proportional zur Position innerhalb der Reihenfolge.
In anderen Worten kommt das häufigste Wort doppelt so oft im betrachteten Text vor wie das Zweithäufigste. Das zweithäufigste Wort dagegen tritt dreimal häufiger auf als das dritthäufigste Wort usw.

### Zucht und Ordnung
Lesen Sie die Wörter aus einer beliebigen Datei mit dem deutschen Text in eine geeignete Java Collection ein.
Nutzen Sie reguläre Ausdrücke um Sonderzeichen und kurze Wörter (length < 4) zu ignorieren.
Sortieren Sie die Wörter nach ihrer Häufigkeit. Implementieren Sie hierzu den Algorithmus `BubbleSort`.

### Die Prüfung
Überprüfen Sie das Zipfsche Gesetz.
Wie hoch ist die Abweichung (in %) bei Ihrem Text?
Wie viele Wörter gibt es in dem Schriftstück insgesamt und wie viele Wörter muss ein sprachfremder Leser mindestens lernen, um 50 % des Textes zu verstehen?

### Mehr als nur Worte
Dieses Gesetz gilt nicht nur in der Linguistik, sondern auch in anderen Bereichen.
Zum Beispiel kann man versuchen vorherzusagen, wie sich die Zahl der Einwohner in deutschen Städten verändern wird.
Dazu lesen Sie die folgende Datei ein:
```
Stadt / Land, Kürzel  Einwohner
Frankfurt am Main / Hessen, HE  732.688
Berlin / Berlin, BE	3.520.031
Stuttgart / Baden-Württemberg, BW 623.738
Düsseldorf / Nordrhein-Westfalen, NW  535.753
Leipzig / Sachsen, SN 560.472
Dortmund / Nordrhein-Westfalen, NW  586.181
Essen / Nordrhein-Westfalen, NW 582.624
Hamburg / Hamburg, HH	1.787.408
München / Bayern, BY	1.450.381
Köln / Nordrhein-Westfalen, NW	1.060.582
```
Das bestehende Programm für Wörter soll möglichst wenig geändert werden.

