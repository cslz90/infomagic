[file io / regex / collections / generics] [2 Stunden]

## Zipfsches Gesetz
*Im Rahmen des bürokratischen Umbruchs innerhalb des Statistischen Ordens, bittet Java die Thaumaturgischen Hochschule der Magie als neutrale Instanz um Hilfe eines der neuen Gesetze auf magische Art zu belegen.*

*[Das Zipfsche Gesetz](https://en.wikipedia.org/wiki/Zipf's_law) besagt: Wenn die Elemente einer Menge – beispielsweise die Wörter eines Textes – nach ihrer Häufigkeit geordnet werden, ist die Wahrscheinlichkeit ihres Auftretens umgekehrt proportional zur Position innerhalb der Reihenfolge.
In anderen Worten kommt das häufigste Wort doppelt so oft im betrachteten Text vor wie das zweithäufigste Wort. Das zweithäufigste Wort dagegen tritt dreimal häufiger auf als das dritthäufigste Wort usw.*

### Zucht und Ordnung

*Um diese Gesetzmäßigkeit zu beweisen, sollen Sie einen Spruch kreieren, der ein beliebiges in deutscher Sprache verfasstes, versiegeltes Stück Pergament in eine geeignete Datenkreatur nach Java Norm einließt.
Nutzen Sie die RegEx-Norm, um Sonderzeichen oder kurze Worte ( length < 4) zu ignorieren. Sortieren Sie anschließend die Wörter nach ihrer Häufigkeit. Implementieren Sie hierzu das anglomatische Verfahren `BubbleSort`, um die Bürokraten in Java zu beeindrucken.*

### Die Prüfung
*Um wie viel Prozent hätte sich der Statistischen Orden mit seinem Gesetz bei ihrem Text verschätzt? Wie viele Wörter gibt es gesamt in dem Schriftstück und wie viele Wörter muss ein orkischer Student mindestens lernen, um 50 % des Textes zu verstehen?*

### Mehr als nur Worte
*Dieses Gesetz soll laut dem Statistischen Orden nicht nur in der Linguistik, sondern auch in anderen Bereichen gelten.
Zum Beispiel versucht der Orden seit Jahren die Populationsveränderungen unserer magischen Bevölkerung vorherzusagen. Ihre Aufgabe soll auch dieses Mal die Validierung dieser Aussage sein.
Lesen Sie bitte hierzu das folgende Pergament ein:*
```
Stadt / Land, Kürzel  Einwohner
Java/ Objektorient, JA		9.007.346
.Net / Objektorient, NT	8.000.000
Haskell/ Funktionalia, HA	72
Lisp / Funktionalia, LS	46.750.359
```
*Versuchen Sie ihren bestehenden Spruch möglichst wenig zu verändern.*

