package orkgrunzer;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OrkGrunzer {

    private static HashMap<Character, String> dictionary = new HashMap<>();

    private static void init() {
        if (dictionary.size() != 0) return;
        Pattern pattern = Pattern.compile("\\s*\\\"(.)\\\"\\s*=>\\s*\\\"(.+)\\\"\\s*,*\\s*");
        Matcher matcher;
        try {
            List<String> lines = Files.readAllLines(Paths.get("./morse-code.txt"), StandardCharsets.UTF_8);
            for (String line : lines) {
                matcher = pattern.matcher(line);
                if (matcher.matches()) {
                    dictionary.put(matcher.group(1).charAt(0), matcher.group(2));
                }
            }
        } catch (IOException e) {
            System.err.println("Could not read file!");
        }
    }

    public static String encode(String input) {
        init();
        StringBuilder sb = new StringBuilder();
        for (char c : input.toUpperCase().toCharArray()) {
            sb.append(dictionary.get(c));
        }
        return sb.toString();
    }


}
