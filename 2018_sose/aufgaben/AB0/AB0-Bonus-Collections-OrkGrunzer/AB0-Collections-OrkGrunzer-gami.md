# Abbildungen Ork Grunzer
### Dauer: ca. 3 Stunde (45 XP)

## Aufgabe 1
*Das Gleichstellungsbüro für anderweitige Fabelwesen bietet Hilfe bei der Kommunikation mit den neu-eingeschriebenen Orks.*
*Dafür wurde folgendes Pergament mit der Übersetzung von Grunz-Lauten in lexikografische Zeichen bereitgelegt:*  
(`morse-code.txt`)

*Parsen Sie dieses Pergament und generieren Sie eine [Java Map](https://docs.oracle.com/javase/7/docs/api/java/util/Map.html), die jedes lexikografische Zeichen auf seine zugehörige Grunzfolge abbildet: `Character => String` - `Map<Character,String>()`.*
*Da das ständige Herumtragen des Pergaments recht unhandlich ist und wohl kein Ork die Geduld aufbringt, jedes Wort erneut zu grunzen, bis Sie es verstanden haben, empfiehlt Ihnen der Sachbearbeiter des Gleichstellungsbüros für anderweitige Fabelwesen, einen Zauberspruch `endcode` zu entwickeln, der Ihr gesprochenes Wort in Grunzen übersetzt und umgekehrt.*

### Hinweise:  
> - Schreiben Sie eine Klasse `OrkGrunzer` im Package `orkgrunzer`, welche die Methode `encode` enthält.  
> - Zum Parsing eignen sich reguläre Ausdrücke. Ein visueller Editor für reguläre Ausdrücke ist [hier](https://regex101.com/) zu finden.
> - Zwischen Gross- und Kleinschreibung wird nicht unterschieden.

[Abgabe in Dozentron](Link hier einfügen)

## Aufgabe 2
*Wie Sie sicherlich wissen, benötigt das magische Schriftstück "[Java Map](https://docs.oracle.com/javase/7/docs/api/java/util/Map.html)" sogenannte "Schlüssel" und "Werte".*
*Je nachdem, ob Sie eine `HashMap` oder eine `TreeMap` verwenden, gelten unterschiedliche Einschränkungen für ihre möglichen Schlüssel.*
* Welche Bedingung müssen Schlüssel einer HashMap erfüllen, welche die Schlüssel einer TreeMap?

### Hinweis
> *Eine Recherche in der magischen Bücherkollektion "Java API" kann hilfreich sein.*

[Freitextfeld in Gildamesh]()
