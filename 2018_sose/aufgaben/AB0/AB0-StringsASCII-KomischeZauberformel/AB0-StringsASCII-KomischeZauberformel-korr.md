# Bewertungsvorschläge

## Positive Faktoren:
	- Es wird eine interne Datenstruktur verwendet, um die Schlüssel-Wert Paare abzubilden
	- Gut und verständlich kommentierter Code
	- Verständliche und sinnvolle Variablennamen
	
## Weiche Kriterien:
	- Die neue Schlüsselwerte werden berechnet und sortiert und der Ausgabeformat ist nur fehlerhaft
	- Daten werden von eine Datei eingelesen und nicht direkt als Datenstruktur "hardcoded"
	- Die Schlüssel werden in der umgekehrten reihenfolge sortiert -> Denkfehler
	
## Unit-Tests
	- 1% Luck
	
## Ungültig:
	
