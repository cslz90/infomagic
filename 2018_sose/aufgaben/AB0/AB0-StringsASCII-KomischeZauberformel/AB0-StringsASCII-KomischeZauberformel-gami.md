Dauer: 45 Minuten

## Komische Zauberformel

*Beim Aufräumen im Keller hat der berühmte Magier Magnus Magicus ein dickes Buch und ein altes Pergament von seinem Urgroßvater gefunden. Das Schriftstück trägt die Aufschrift "Zugangsformel für Zauberenzyklopädie". Magnus fällt ein, dass ihm sein Urgroßvater einmal von der Zauberenzyklopädie (das dicke Buch) erzählt hat. Dieses Buch könne man nur öffnen, wenn man vorher die magischen Zeichen der Formel aufsagt.
Die Formel sei verschlüsselt in einem Dokument festgehalten.*

*Jetzt, wo Magnus Magicus das Buch und das Dokument zu dessen Zugang gefunden hat, versucht er die magische Formel auf dem Pergament zu entschlüsseln. Dafür muss er anhand einer zweispaltigen Tabelle, deren linke Spalte Zahlen und deren rechte Spalte Schriftzeichen beinhaltet, die Formel zusammenbauen. Die Zahlen stehen für die wiederum verschlüsselte Position der Schriftzeichen in der Zauberformel.
Jede Tabellenzeile beinhaltet also ein Schlüssel-Wert-Paar aus verschlüsselter Position (Schlüssel) und Zeichen (Wert).*

*Um die verschlüsselten Positionen zu entschlüsseln, muss zuerst jeweils die Zahl mit dem ASCII-Wert des zugehörigen Schriftzeichens addiert werden. Dann müssen diese Werte noch aufsteigend sortiert werden. Die Position der Werte in der sortierten Reihenfolge bestimmt dann die endgültige Position in der Zauberformel.*

*Beispiel:*

*linke Spalte* | *rechte Spalte*
---------------|----------------
*42*           |*g*
*7*            |*a*
*63*           |*e*
*10*           |*M*
*50*           |*i*

*ASCII-Wert von g => 103*

*Position von g => 42 + 103 = 145*

*ASCII-Wert von a => 97*

*Position von a= 7 + 97 = 104*

*ASCII-Wert von e => 101*

*Position von e => 63 + 101 = 164*

*ASCII-Wert von M => 77*

*Position von M => 10 + 77 = 87*

*ASCII-Wert von i => 105*

*Position von i => 50 + 105 = 155*

*aufsteigend sortiert: 87, 104, 145, 155, 164*

*Daraus ergibt sich folgende komische Zauberformel: "Magie"*

*Bitte helfen Sie dem neugierigen Magier Magnus Magicus, zu vorgegebener Tabelle die korrekte Zauberformel nach den Regeln des Pergaments zu bestimmen.*