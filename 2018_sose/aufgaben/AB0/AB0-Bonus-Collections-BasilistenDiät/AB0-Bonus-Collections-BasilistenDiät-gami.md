# [Bonus] Eine Diät für Basilisten
### Dauer: ca. 20 min (5 XP)
## Aufgabe 1: Ein Abspeckprogramm muss her
*Auf regelmäßigen Wanderungen ist Ihnen aufgefallen, dass die gefräßigen Basilisten ein Vielfaches der nötigen Nahrung zu sich nehmen und zu Übergewicht tendieren.
Im Name des Artenschutzes entwickeln sie einen Zauber `List<A> distinct(List<A> lst)`, der mit Hilfe eines [Sets](http://docs.oracle.com/javase/7/docs/api/java/util/Set.html) aus überfressenen Basilisten, abgespeckte Tiere macht und zu viel verzehrtes Futter entfernt, sodass nur noch einfache Nahrung vorliegt.*

*Hierbei handelt es sich um einen Zauber, der die Basilisten in eine Art "Boot Camp" mit Sets schickt, damit diese das Essverhalten der Basilisten korrigieren. Sets sind sehr ernährungsbewusste Datenkreaturen mit einer schlanken Figur. Sie nehmen nur maximal einen Happen von jeder für sie verträglichen Nahrung auf, dieses Verhalten sollen sie an die krankhaft übergewichtigen Basilisten im Boot Camp übertragen.*

*Durch ihre elegante Figur und ihrem Hang zum posieren sind Sets bis in die nicht-magische Welt berühmt. Nicht um sonst heißen die Fotovisitenkarten, mit denen Models und Schauspieler erhoffen möglichen Arbeitgebern ihre Vorzüge zu präsentieren, Setcards.*


[Abgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/75)


## Aufgabe 2: Trendkost der Woche: Sets
*Allen Anschein nach, funktioniert dieser Diätzauber ganz wunderbar und schon nach einiger Zeit räkeln sich die verschlankten Basilisten wieder in der Sonne. Aber warum eignet sich die temporäre Versorgung durch Sets besonders, um für eine ausgewogene Ernährung bei den neuen Basilisten zu sorgen?*

*Nach Ihren erfolgreichem Zauber bitten einige Tierschützer, aus einer Pflegestation, um Ihre Mithilfe. Damit auch die in Gehegen gehaltenen Basilisten eine ausgewogene Ernährung erhalten, bitten Sie einige Sets die Mahlzeiten vorzubereiten.
Jedes Set erhält Integerlinge von 1 bis 5 in aufsteigender Reihenfolge.
In welcher Reihenfolge bekommen die Basilisten ihre Mahlzeiten von den Sets?*

*Um die Diät Ihres eigenen Basilisten zu regulieren zu können, sollten die vom Set enthaltenen Elemente vergleichbar sein.*
- *Wieso?*
- *Was hat das mit der Datenstruktur zu tun?*

[Aufgabe in Moodle](https://moodle.thm.de/mod/assign/view.php?id=166137)
