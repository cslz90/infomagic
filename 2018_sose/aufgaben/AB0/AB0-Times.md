|Aufgabe								|Stunden  |
| -										| - 	  |
|AB0-Bonus-Collections-OrkGrunzer 		|1,5      |
|AB0-Generics-ArrayList					|1,5      |
|AB0-ImmutableList-LinkedList 			|2,5      |
|AB0-Bonus-General-HilfDemHenker  		|4        |
|AB0-StringsASCII-KomischeZauberformel  |3        |
|AB0-Bonus-General-OOP					|1 	      |
|AB0-Recherche-WildeStammbaumw�rmer		|0,5	  |