# Hilf dem Henker
### Dauer: 240 Min (60 XP)

## Einleitung

*Henker Hannibal, Sohn einer langen Scharfrichterdynastie, vollführt seinen Beruf in einer ganz besonderen Art und Weise. Da das Henkerdasein sehr eintönig sein kann, gibt er jedem Sträfling die Chance, gegen ihn in einem Ratespiel anzutreten. Dafür überlegt er sich für jeden Übeltäter ein Wort, welches dieser aber nicht weiß. Nur durch Raten der Buchstaben können die dem Tode Geweihten versuchen, das Wort herauszufinden. Gelingt ihnen dies mit einer vorgegebenen Anzahl an Fehlversuchen, lässt Hannibal sie laufen.*

## Aufgabe 1: Formel gegen Fieslinge

*"Das kann man doch gar nicht verlieren!" ruft ein Verbrecher selbstsicher. Er glaubt, dass es einen Trick gibt, das Spiel des Henkers immer zu gewinnen und versucht mit Hannibal zu verhandeln. Dieser erkennt die List des Gauners und beauftragt einen Zauberer, eine magische Formel für ihn zu entwickeln, mit der er vor jedem Spiel überprüfen kann, ob es möglich ist, zu verlieren.*

## Aufgabe 2: Zufälliger Buchstabenzauber

*Die Schurken merken, dass es nicht leicht ist, Hannibal zu überlisten und überlegen sich Strategien, wie sie am besten gegen ihn gewinnen können. Ein Zauberer wird zum Tode verurteilt, da er im Glückspiel betrogen hat. Er hat einen Zauberspruch entwickelt, der es ihm ermöglicht, die Buchstaben zufällig zu raten. Das Spiel startet und bei jedem Rateversuch schwingt der Zauberer seinen Zauberstab und spricht den Zauberspruch, welcher ihm zufällig einen Buchstaben aus dem Alphabet offenbart. Ob dieser zufällige Zauber die beste Strategie ist, das Ratespiel zu gewinnen?*

## Aufgabe 3: Weise Buchstabenwahl

*Hannibal hat viel zu tun, denn es kommt ein Gauner nach dem anderen, über den gerichtet werden soll. Diese werden jedoch immer raffinierter. Der nächste hat ein Buch von einem Weisen gestohlen. Studien über die Sprache der Menschen, welche über Jahre hinweg geführt wurden, sind in dem Buch niedergeschrieben. Es enthält Wissen darüber, welche Buchstaben am häufigsten verwendet werden. Der Dieb hofft mit dieser Hilfe strategisch gegen den Henker zu gewinnen.*

[Abgabe 1-3 in Dozentron, Link hier einfügen]()

## Aufgabe 4: Hannibal braucht deine Hilfe

*Der erfahrene Scharfrichter merkt, dass die Verbrecher einfallsreicher werden und dass es nicht mehr so einfach wird, schwierige Wörter zu finden.*
- *Was sind die besten Wörter, um gegen den zufälligen Buchstabenzauber anzukommen?*
- *Wie macht man eine weise Buchstabenwahl unnütz?*

[Freitextfeld in Gildamesh, Link hier einfügen]()
