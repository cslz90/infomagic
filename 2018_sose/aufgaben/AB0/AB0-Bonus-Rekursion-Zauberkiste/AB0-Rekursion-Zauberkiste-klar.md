# Zauberkiste:
### Dauer: ca. 20Min (4 XP)

*** Aufgabe im Klartext ***
Gegeben ist eine Datenstruktur von ineinander verschachtelten Objekten.
Das letzte Objekt dieser Kette dient der Informationsspeicherung.
Formulieren Sie einen Algorithmus in Pseudocode, der diese Struktur rekursiv durchläuft und die Information ausließt.

Als Lösungshilfe soll hierbei ein iterativer Pseudocode gelten:  

    aperire! (Zauberkiste){
      äußere_Zauberkiste = Zauberkiste
      while (äußere_Zauberkiste enthält nicht den Gegenstand){
                innere_Zauberkiste = äußere_Zauberkiste.extrahiere_Inhalt
                äußere_Zauberkiste = innere_Zauberkiste
      }
      nimm Gegenstand
}

