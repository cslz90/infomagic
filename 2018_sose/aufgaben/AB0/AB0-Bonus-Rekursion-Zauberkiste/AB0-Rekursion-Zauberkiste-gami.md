# Zauberkiste:
### Dauer: ca. 20Min (4 XP)

*Jeder von Ihnen verfügt über Gegenstände, die ihm persönlich sehr wichtig sind und die er nicht unbedingt mit anderen teilen möchte.
Zu diesem Zweck gibt es magische Zauberkisten.
Diese magischen Zauberkisten bestehen aus vielen ineinander verschachtelten Zauberkisten, die mit einem Zauberspruch verschlossen sind.
Sofern Sie über den nötigen Zauberspruch verfügen, können Sie die äußere Zauberkiste öffnen und die innen-liegende Zauberkiste herausholen.
Diesen Vorgang müssen Sie dann so häufig wiederholen, bis Sie irgendwann aus der innersten Zauberkiste Ihren Gegenstand entnehmen können.*

 >*Hierbei handelt es sich um ein vereinfachtes Modell, bei dem sich alle verschachtelten Zauberkisten mit dem gleichen Zauberspruch öffnen lassen. Da die Zauberkästen verschachtelt sind, wirkt der Zauberspruch immer nur auf die äußere Zauberkiste.*

*Weil es sich bei dieser Aufgabe um eine Wiederholung handelt und Sie bereits bewiesen haben, dass Sie diese Inhalte verstanden haben, geben wir Ihnen noch sinnvolle Ratschläge, damit Sie sich möglichst schnell mit den neuen wirklich spannenden Aufgaben zur Vorlesung beschäftigen können:*

- Zauberspruch zum Öffnen jeder Zauberkiste lautet: *„aperire!“*

*Ihre **Aufgabe** soll nun sein, dass Sie einen rekursiven abstrakten Zauberspruch (Pseudocode) formulieren, mit dem Sie alle Zauberkisten öffnen, bis Sie an den Gegenstand gelangt sind.
Als Orientierungshilfe erhalten Sie hier einen iterativen abstrakten Zauberspruch:*

    aperire! (Zauberkiste){
          äußere_Zauberkiste = Zauberkiste
          while (äußere_Zauberkiste enthält nicht den Gegenstand){
                	innere_Zauberkiste = äußere_Zauberkiste.extrahiere_Inhalt
                    äußere_Zauberkiste = innere_Zauberkiste
          }
          nimm Gegenstand
    }

