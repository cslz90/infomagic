# Aufgabenblatt 0
## Pflichtaufgaben

|Nr | Thema                         | Name                             | Dauer in min   | XP  | %   | Ref |
|---| ---                           | ---                              | ---            | --- | --- | --- |
|01 | ArrayList, Generics           | ArrayBasilist                    | 45             | 5   | --- | [Link](./2018_ss/aufgaben/AB0/AB0-Generics-ArrayBasilist/AB0-Generics-ArrayBasilist-klar.md) |
|02 | File-IO                       | VerteidigungGegenMagischeWesen   | 25             | 3   | --- | [Link](./2018_ss/aufgaben/AB0/AB0-FileIO-VerteidigungGegenMagischeWesen/AB0-FileIO-VerteidigungGegenMagischeWesen-klar.md)|
|03 | LinkedList                    | ImmutableList                    | 30             | 3   | --- | [Link](./2018_ss/aufgaben/AB0/AB0-LinkedList-ImmutableList/AB0-LinkedList-LinkedBasilist-klar.md) |
|04 | Rekursion                     | Zauberkiste                      | 20             | 4   | --- | [Link](./2018_ss/aufgaben/AB0/AB0-Rekursion-Zauberkiste/AB0-Rekursion-Zauberkiste-klar.md) |
|05 | ASCII                         | KomischeZauberformel             | 45             | 5   | --- | [Link](./2018_ss/aufgaben/AB0/AB0-StringsASCII-KomischeZauberformel/AB0-StringsASCII-KomischeZauberformel-klar.md) |
|06 | Recherche                     | WildeStammbaumwürmer             | ?              | ?   | --- | [Link](./2018_ss/aufgaben/AB0/AB0-Recherche-WildeStammbaumwürmer/AB0-Recherche-WildeStammbaumwürmer-klar.md) |

## Bonufaufgaben

|Nr | Thema                         | Name                             | Dauer in min   | XP  | %   | Ref |
|---| ---                           | ---                              | ---            | --- | --- | --- |
|01 | Collections                   | BasilistenDiät                   | 20             | 5   | --- | [Link](./2018_ss/aufgaben/AB0/AB0-Bonus-Collections-BasilistenDiät/AB0-Bonus-Collections-BasilistenDiät-klar.md) |
|02 | Allgemein                     | MagicalPot                       | 15             | 3   | --- | [Link](./2018_ss/aufgaben/AB0/AB0-Bonus-General-MagicalPot/AB0-Bonus-General-MagicalPot-klar.md) |
|03 | Generics                      | BeutelUndBasilisten              | 40             | 10  | --- | [Link](./2018_ss/aufgaben/AB0/AB0-Bonus-Generics-BeutelUndBasilisten/AB0-Bonus-Generics-BeutelUndBasilisten-klar.md) |
|04 | File-IO, Regex                | MagicalDryCounter                | 45             | 11  | --- | [Link](./2018_ss/aufgaben/AB0/AB0-Bonus-MagicalDRYCounter/AB0-Bonus-MagicalDRYCounter-klar.md) |
|05 | Rekursion                     | BonsaiHydrachen                  | 15             | 3   | --- | [Link](./2018_ss/aufgaben/AB0/AB0-Bonus-Rekursion-BonsaiHydrachen/AB0-Bonus-Rekursion-BonsaiHydrachen-klar.md) |
|06 | Maps                          | OrkGrunzer                       | 180            | 45   | --- | [Link](./2018_ss/aufgaben/AB0/AB0-Bonus-Collections-OrkGrunzer/AB0-Collections-OrkGrunzer-klar.md) |
|07 | OOP                           | OOP                              | 120             | 30   | --- | [Link](./2018_ss/aufgaben/AB0/AB0-Bonus-General-OOP/AB0-General-ObjektOrient-klar.md) |
|08 | Allgemein                     | HilfDemHenker                    | 240          | 60  | --- | [Link](./2018_ss/aufgaben/AB0/AB0-Bonus-General-HilfDemHenker/AB0-Bonus-General-HilfDemHenker-klar.md) |
