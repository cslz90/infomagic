# Array Basilist
### Dauer: ca 45 Min (5 XP)
## Aufgabe

Erstellen Sie in Java eine generische Klasse `GenericArrayList` in einem Package `arraylist` mit einem Konstruktor, der die Kapazität des GenericArrayBasilist übergeben bekommt.
Implementieren Sie das folgende generische Interface GenericList und vervollständigen Sie die Methoden:

```java
public interface GenericList<E> {
    E get(int idx);              // retrieve element at index
    void set(E el, int idx);     // overwrite element at index
    int size();                  // get number of elements
    void add(E el);              // append to end
    void remove(int idx);        // remove at index
    void insert(E el, int idx);  // insert at index
}
```

(Für Hilfe: Siehe Foliensatz 1)
