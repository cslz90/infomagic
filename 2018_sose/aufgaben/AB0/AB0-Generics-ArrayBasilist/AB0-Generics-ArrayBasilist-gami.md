### Dauer: ca 45 Min (5 XP)
## Aufgabe 1: Array Basilisten

*Im Rahmen ihrer wunderbar-magischen ersten Vorlesung im Hörsaal 20 1/36, durften Sie bereits im Streichelzoo der Datenkreaturen Ihren ersten Kontakt mit dem Array-Basilisten herstellen.
Wie bei vielen anderen seltsamen Geschöpfen auf unserem zauberhaften Planeten gilt auch für den Array-Basilisten, dass er nicht einfach von Anfang an in der Form existierte, wie Sie ihn heute kennen.
Seine Entwicklung ist allerdings weniger magisch, sondern eher evolutionär zu erklären.
Man spricht hier von “Informatischem Darwinismus”, oder auch „form follows function“.
Die spezielle Gattung des Array-Basilisten aus der Vorlesung hat sich mit der Zeit und durch Nahrungsvorkommen in seinem Umfeld auf “Doubles” als Nahrungsquelle spezialisiert und kann sich nun nur noch von diesen ernähren.*

*Aus historischen Überlieferungen der beiden Großmeister Burbetschert und Dumble-Döring wissen wir, dass vorher ein Array-Basilist existiert haben muss, der diese Spezifizierung noch nicht aufwies.
Man spricht hier vom sogenannten Generic-Array-Basilist.
Dieser hatte die Veranlagung sich von allen Nahrungsquellen zu ernähren, wenn er sich jedoch für seine favorisierte Nahrung entschieden hat, blieb er auch dabei.*

### Aufgabe 1.1

*Wie bei engagierten Forschern üblich, vergessen Sie auch manchmal etwas.
In unserem Fall ist es die vollständige Implementierung des Generic-Array-Basilisten, dessen fossile Überreste im ersten Foliensatz angesprochen, aber nicht zu Ende geführt wurden.
Ihre Aufgabe bis zur ersten Zauberübung wird sein, den im Javaorient heimischen Generic-Array-Basilisten zu rekonstruieren. Er sollte folgender Rolle genüge tuen:*
```java
public interface GenericList<E> {
    E get(int idx);              // retrieve element at index
    void set(E el, int idx);     // overwrite element at index
    int size();                  // get number of elements
    void add(E el);              // append to end
    void remove(int idx);        // remove at index
    void insert(E el, int idx);  // insert at index
}
```
*Zudem soll ihr Basilist seine Magenkapazität in seiner Beschwörungsformel beinhalten.*

>*Namensähnlichkeiten zu Mitgliedern der Technischen Hochschule Mittelhessen sind rein zufällig!*
