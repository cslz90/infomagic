# Die Pilgerreise der kurzsichtigen Mönche

### Dauer: ca. 45 min

*In einer kleinen Siedlung am Ufer der Lohn steht ein Altehrwürdiges Kloster, dessen Gründervater vor allem für die Entwicklung moderner Düngemittel bekannt ist. Eine Gruppe Mönche aus diesem Kloster möchten eine Pilgerreise zum Gipfel des schiefen Berges unternehmen, der in den Randgebieten des Taurusgebirges liegt.*

*Leider sind die Mönche schon alt und ihre Sicht durch das lange Studium der antiken Schriften getrübt. Daher können sie nur wenige Meter weit sehen. So passierte es, dass sie sich auf dem Weg verliefen und in einer abgelegenen Taverne nach dem Weg fragen mussten. Die einzige Hilfe, die sie erhielten kam von einem betrunkenen Zwerg, der ihnen folgende Wegbeschreibung gab:*

*"Startet im Wald der Vieldenkenden, wo selbst die Bäume tiefsinnige Gedanken bilden und folgt von dort aus dem Pfad den Berg hinauf. Nehmt immer die steilste Steigung. Geht nur weiter, wenn vor euch auch wirklich eine Steigung liegt. Findet ihr keine weitere Erhebung mehr so seid ihr am Ziel."*

*Zur Hilfe besitzen die Mönche noch ein mächtiges Werkzeug: Eine von Greedy dem Runenschmied mit der Rune der Fernsicht verzauberte Wasserwaage. Diese erlaubt es den Mönchen zu erkennen, ob das Gelände auf eine Distanz, die die Sichtweite der Mönche übersteigt, ansteigt.*

*Derart gewappnet machen sich die Mönche auf die Reise. Sie prüfen mit ihrer magischen Wasserwaage die Steigung in alle vier Himmelsrichtungen und entscheiden der steilsten Steigung zu folgen. Dieses Vorgehen wiederholen sie nach kurzen, regelmäßigen Wegstücken bis sie schließlich den Gipfel erreichen.*

*Um ihren Weg für spätere Generationen festzuhalten haben die Mönche einen Schreiber im Tal zurückgelassen. Da die Mönche dieses Ordens Meisterschaft in der hohen Kunst des Jodelns erlangt haben nutzen sie dies zur Kommunikation im Gebirge. Auf jedem Wegstück jodeln sie ihre aktuelle Position, die der Schreiber umgehend protokolliert.*

*Entwickeln Sie einen gierigen Zauber, der einem Reisenden auf diese Art den Weg auf jeden Berg hinauf führen kann.*

#### Hinweise:
> - In der .jar mit den Unit-Tests finden Sie eine Methode, mit der Sie Karten generieren können.
> - Die Schnittstellen der zu implementierenden Klassen (siehe unten) sind mit Javadoc-Kommentaren versehen. Nutzen Sie diese als informelle Richtlinie.


Folgende Formalia sollen eingehalten werden:
- Implementieren Sie die Methode `climb(...)` aus der Klasse `Hillclimber` (siehe Codevorgaben).
- Implementieren Sie die Methoden `climbCallback(...)` und `getPath()` der Klasse `ClimbListener`.
- Für jeden Schritt den der Algorithmus geht muss genau einmal `ClimbListener.climbCallback(...)` aufgerufen werden.
- Wenn die `climb(...)` Methode durchgelaufen ist, muss die von `getPath()` zurückgegebene Liste alle durchlaufenen Koordinaten enthalten (inklusive Start- und Endpunkt).
- Die beiden zu implementierenden Klassen müssen im package `hillclimbing` liegen.

[Abgabe in Dozentron](https://dozentron.mni.thm.de/submission_tasks/28)

Die Skelette der beiden zu implementierenden Klassen sehen so aus:

```java
public class Hillclimber {

    private int[][] hill;

    public Hillclimber(int[][] hill) {
        this.hill = hill;
    }

    /**
     * Implements a greedy algorithm to "climb" a 2d-array with height levels.
     *
     * @param start         the starting point
     * @param climbListener a listener whose climbCallback method is called for each position the climbing algorithm is on
     * @return the top of the hill
     */
    public Coordinate climb(Coordinate start, ClimbListener climbListener) {
        return null;
    }
}
```

```java
public class ClimbListener {

    /**
     * Returns all positions stored by the climbCallback method. The output is sorted chronologically by the time of insertion.
     *
     * @return the list of positions
     */
    public List<Coordinate> getPath() {
        return null;
    }

    /**
     * To be used in the hillclimbing exercise. This function must be called for each step the solving algorithm takes.
     * The argument is stored in an internal datastructure that can be read by using the getPath() method.
     *
     * @param newPosition the position to store
     */
    public void climbCallback(Coordinate newPosition) {
    }
}
```
Die Klasse `Coordinate` ist bereits implementiert, Sie müssen sie nur benutzen:

```java
package hillclimbing;

import java.util.Objects;

public class Coordinate {
    public final int x, y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return x == that.x &&
                y == that.y;
    }

    @Override
    public int hashCode() {

        return Objects.hash(x, y);
    }
}

```
