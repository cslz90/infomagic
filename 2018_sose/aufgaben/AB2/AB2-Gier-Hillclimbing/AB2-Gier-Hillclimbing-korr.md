# Bewertungsvorschläge


## Positive Faktoren:

  - Dozentron nimmt die Eingabe an.
  - Unit-Tests werden bestanden.
  - Gier ist im Code erkennbar.

## Weiche Kriterien:


## Unit-Tests
  - Wenn die hidden Tests fehlschlagen sollte genauer auf den Code geguckt werden. Möglicherweise ist es nur ein kleiner Flüchtigkeitsfehler.

## Ungültig:

  - Es wird keine Gier verwendet.
