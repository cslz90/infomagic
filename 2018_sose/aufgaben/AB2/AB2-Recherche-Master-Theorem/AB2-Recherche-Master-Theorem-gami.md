## Das Elderbuch

*Die drei großen Erzinfomagier Jon "You know everything!" Bentley, Dorothea vom Haken, und Saxophon-James wurden so oft von jungen Novizen genervt, die sich darüber stritten, in welcher Effizienzklasse ihre rekursiven Zauberformeln steckten, dass sie sich zusammenschlossen, um das Problem ein für alle Male zu lösen.
Sie entwickelten das Elderbuch - ein sprechendes Buch, das den Novizen in fast allen Fällen die Antworten auf ihre Fragen geben konnte.*

*Das Buch wurde allerdings mit der Zeit die ständige Fragerei auch leid und bekam daher eine zynische Ader, die es macnhmal dazu brachte, die Novizen lieber mit einem Zauber in den Wahnsinn zu treiben, statt sie zu erleuchten.*

### Aufgabe 1.1

*Suchen Sie das Elderbuch und studieren Sie seine Weisheiten.
Versuchen Sie eine Abschrift der Erkenntnisse darin zu verfassen, die man lesen kann, ohne die grumpige Stimmung des Elderbuchs fürchten zu müssen.
Wagen Sie sich auf der Suche nach dem Buch ins Netz der großen Google-Spinne oder in die verbotene Bibliothek der Thaumaturgischen Hochschule - je nach dem, was Ihnen weniger gefährlich erscheint.*

*Beantworten Sie dabei die folgenden Fragen:*

* *Nach welchem Schema entscheidet das Elderbuch, in welcher Effizienzklasse ein rekursiver Zauber sich befindet?*
* *Wie sollte man mit dem Elderbuch umgehen und warum sollte man seine Weisheit suchen?*
* *Was ist die Eldermethode - von Muggeln auch Master-Methode genannt? Wie hängt Sie mit den Aussagen des Buches zusammen?*
* *Fragen Sie das Elderbuch nach der Laufzeit des Mergesort-Algorithmus und schreiben Sie sich seine Antwort genau auf. Die gelegentlichen Beleidigungen, die das Buch ausspuckt, könnten Sie dabei auslassen.*

**Hinweise:**

* *Das Elderbuch hasst nichts so sehr wie Rechtschreib- oder Grammatikfehler. Machen Sie es bitte nicht wütend.*
* *Denken Sie daran, dass die Weisen nichts so sehr hassen wie Geschwafel und daher Abhandlungen über sie, die mehr als 700 Worte umfassen, automatisch mit einem Fluch belegen werden.*

### Aufgabe 1.2

*Um ihren Mitnovizen zu helfen, genauere Studien anzustellen, sprechen Sie bitte einen kurzen Ortungszauber, um ihnen den Weg zu Ihren Informationsquellen zu weisen.
Da Novizen sich schnell von zu vielen Informationen verwirren lassen, beschränken Sie sich auf fünf davon.*
