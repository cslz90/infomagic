# Tribonati
### Dauer: ca. 50 min

*Die Legende um Tribonati besagt, dass ein seit Jahrhunderten bestehender Geheimbund von Magiern versuche, den Stamm der Druiden zu zerstören. Diese geheime Organsiation bezeichne sich auch als Orden der Tribonaten und ihr tatsächliches Motiv sei die ideologische Annäherung der Druiden an die Magie Insectum. Dabei handle es sich um die Verzauberung von Insekten, sodass diese klüger werden. Es gäbe schon seit Urzeiten immer wieder Vereinigungen, die diese Art von Magie unterdrücken wollen.*

*Die Jahre vergehen, in denen es ruhig geworden ist um den Tribonatenorden, doch nach einigen ungewöhnlichen Todesfällen von Druiden kommt der Verdacht auf, dass böse Mächte hier ihre Finger im Spiel haben. Jeder der toten Druiden hat eine Zahl auf der Brust eingebrannt, umschlossen von einem Dreieck. Betrachtet man die Todeszeitpunkte und setzt die Zahlen in eine Reihenfolge, erhält man folgendes Fragment, welches womöglich eine Verschlüsselung oder ähnliches darstellt.*

*0, 1, 1, 2, 4, 7, 13, 24, ...*

## Aufgabe 1.1

*Der alten weisen Druidin Dolores kommt diese Folge von Zahlen sehr bekannt vor und sie findet heraus, dass diese Zahlenfolge in alten Büchern auch als Tribonacci bezeichnet wird und stößt auf einen Hinweis, der alles auf einen Angriff der Tribonaten hindeuten lässt. Die Tribonacci-Folge kann unendlich lange fortgeführt werden, was darauf schließen lässt, dass das Opfer mit der Zahl 24 nicht das letzte gewesen ist. Um den Gegnern einen Schritt voraus zu sein, versucht Dolores einen Zauber zu finden, um die nächsten Elemente der Folge herauszufinden. Natürlich verwendet sie dazu ihre Lieblingstechnik: Dynamische Drohnen.*

*Entwickeln Sie einen Zauber, der ein bestimmtes Element der Folge berechnet und gehen Sie dabei vor wie Dolores und verwenden Sie die Technik __Dynamische Drohnen__.*

Erstellen Sie dafür eine Klasse `Tribonati` im package `tribonati` und implementieren Sie darin die Methode `static int tribDyn(int n, int[] brain)`.
Die Methode `tribDyn()` soll das `n`-te Element oben genannten Zahlenfolge berechnen.

### Hinweise
* Achten Sie darauf, dass sich die Klasse im richtigen Package befindet.
* Benennen Sie die Methode genau wie in der Aufgabenstellung beschrieben und achten Sie auf korrekte Typen.

(Abgabe Aufgabe 1.1 in Dozentron)[Link hier einfügen]

## Aufgabe 1.2

*Mithilfe des von Dolores gefundenen Zaubers kommt sie den Tribonaten auf die Spur und es kommt zu einem erbitterten Kampf zwischen den Magiern des Tribunatenordens und dem Stamm der Druiden. Schließlich können die Druiden mithilfe der dynamischen Drohnen siegen. Mit dieser Technik kamen sie gegen den Spruch Rekursio an, den die Tribonaten verwendeten. Doch warum ist das so?*

- *Warum führt die Technik Dynamische Drohnen hier zu einem geringeren Zaubergestenverbrauch im Gegensatz zum Spruch Rekursio. (Ziehen Sie auch die Weisen von O zu Rate.)*
- *Wann ist es sinnvoll die Technik Dynamische Drohnen einzusetzen?*

### Hinweise
* Die zwei Fragen können in einem Text beantwortet werden.
* Ein Blick ins Vorlesungsskript kann hilfreich sein.