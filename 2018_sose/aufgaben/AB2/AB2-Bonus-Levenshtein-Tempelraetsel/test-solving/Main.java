import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        try {
            List<String> distances = Files.readAllLines(Paths.get("input"), Charset.forName("UTF-8"))
                    .stream()
                    .map(line -> line.split(";"))
                    .map(words -> distance(words[0].toCharArray(), words[1].toCharArray()) + "")
                    .collect(Collectors.toList());
            Files.write(Paths.get("output"), distances, Charset.forName("UTF-8"));
        } catch (IOException ioe) {
            System.err.println("File operation failed!");
        }
    }

    private static int distance(char[] first, char[] second) {
        int sub;
        int m = first.length;
        int n = second.length;
        int[][] matrix = new int[m + 1][n + 1];
        for (int i = 1; i <= m; i++) matrix[i][0] = i;
        for (int j = 1; j <= n; j++) matrix[0][j] = j;
        for (int j = 1; j <= n; j++) {
            for (int i = 1; i <= m; i++) {
                sub = first[i - 1] == second[j - 1] ? 0 : 1;
                matrix[i][j] = min(matrix[i - 1][j] + 1,
                        matrix[i][j - 1] + 1,
                        matrix[i - 1][j - 1] + sub);
            }
        }
        //printMatrix(matrix, new String(first), new String(second));
        return matrix[m][n];
    }

    private static int min(int a, int b, int c) {
        return Integer.min(a, Integer.min(b, c));
    }

    private static void printMatrix(int[][] matrix, String first, String second) {
        System.out.println("Matrix for " + first + " vs " + second);
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j]);
            }
            System.out.println();
        }
    }

}
