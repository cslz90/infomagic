# Tempelrätsel

*Große Teile der Halbinsel Ja'va sind von dichtem und undurchsichtigem Dschungel bedeckt. Inmitten dieser durchwucherten Wildnis finden sich zahlreiche Relikte aus vergangenen Tagen. Darunter magische Artefakte umwoben mit antiken Zaubern, die seit Jahrhunderten in Vergessenheit geraten sind.*

*Eine Gruppe Elfenmagier begibt sich auf die Suche nach besonderen Relikten. Sie besitzen eine alte Karte vom Oaka-Stamm, den Ureinwohnern von Ja'va. Diese Karte führt sie in einen alten Tempel. In einem Seitengang des Gewölbes finden sie eine Reihe an Worten in die Wände gemeißelt, die scheinbar keinen Zusammenhang zueinander haben. Die Worte sind Zeilenweise angeordnet, mit jeweils Zwei Worten pro Zeile.*

*Am Ende des Ganges befindet sich eine Tür. Als sie diese zu öffnen versuchen, ertönt eine tiefe, durchdringende Stimme, die sie zur Nennung einer Lösung auffordert.*

*Auf der Karte ist eine bis dato verwirrende Botschaft zu finden:*  
*"Seht die Entfernung der ersten beiden Worte. Seht außerdem die Entfernung der nächsten beiden Worte. Fahrt fort, bis ihr alle Entfernungen kennt."*

*Der letzte Hinweis bringt einen der Elfen auf eine Idee: Am Stein Levens lebte ein alter Mönch, der ihm einen Zauber lehrte, mit dessen Hilfe sich eine Art Distanz zwischen Wörtern bestimmen lässt. So vermutet er, dass die Lösung eine Aneinanderreihung von Zahlen ist, die sich aus den Distanzen zwischen den Wörtern bildet.*

*Leider hat er die Zauberformel zur Berechnung dieser Distanzen nicht in seinem Taschendictionarium. Helfen sie ihm den Zauber zu rekonstruieren.*


### Aufgabe:

*Die Worte sind zeilenweise angeordnet, wobei in jeder Zeile zwei durch Semikola getrennte Wörter stehen. Entwickeln sie einen Zauber mit der gleichen Wirkungsweise des Mönches von Stein Levens. Ermitteln sie mithilfe ihres Zaubers für jede Zeile die Distanz zwischen den beiden Wörtern.*

### Hinweise:

- Es gelten die üblichen Vorgaben für IO-Aufgaben.
- Groß- und Kleinschreibung werden nicht beachtet (d.h. "Hallo" und "hallo" sind gleich).
- Es gibt nur Buchstaben aus dem ASCII-Zeichensatz und keine Sonderzeichen.
- Die Nutzung von Vorgefertigten Implementationen der Levenshtein-Distanz (z.B. Bibliotheksfunktionen) ist natürlich nicht gestattet.
- Die Ausgabezeilen haben die gleiche Reihenfolge, wie die Eingabezeilen.

### Beispiel:

Input:

```
Hallo;Holla
Hallo;hallo
Welt;Elt
Levenshtein;Meilenstein
Algomantik;Algorithmik
```

Output:

```
2
0
1
4
5
```
