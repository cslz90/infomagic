# Aufgabenblatt 2
## Pflichtaufgaben

|Nr | Thema                     | Name                          | Dauer in Std. (gemessen) | Dauer in Sdt (für Studis)               | Punkte Gildamesh  | Ref                                                                                             |
|---| ---                       | ---                           | ---                      | ---                                     | ---               | ---                                                                                             |
|01 | Gier                      | AB2-Gier-Hillclimbing         | 1,5h                     | Geschätzt: 2h, Berechnet: 1,5h * 2 = 3h | 25                |  [Link](2018_ss/aufgaben/AB2/AB2-Hillclimbing/AB2-Hillclimbing-klar.md)                         |
|02 | Backtracking              | AB2-Backtracking-?            |                          | Geschätzt: 2h                           | 25                |  [Link](2018_ss/aufgaben/AB2/AB2-IO-Backtracking/AB2-IO-Backtracking-klar.md)                   |
|03 | Teile und Herrsche        | AB2-Recherche-Master-Theorem  |                          | Geschätzt: 2h                           | 25                |  [Link](2018_ss/aufgaben/AB2/AB2-Recherche-Master-Theorem/AB2-Recherche-Master-Theorem-klar.md) |
|04 | Dynamische Programmierung | AB2-DP-Tribonati              |                          | Geschätzt: 2h                           | 25                |  [Link](2018_ss/aufgaben/AB2/AB2-DP-Tribonati/AB2-DP-Tribonati-klar.md)                         |

## Bonusaufgaben

|Nr | Thema                     | Name                          | Dauer in Std. (gemessen) | Dauer in Sdt (für Studis)               | Punkte Gildamesh  | Ref                                                                                             |
|---| ---                       | ---                           | ---                      | ---                                     | ---               | ---                                                                                             |
|01 | Backtracking                      | AB2-Bonus-Backtracking-ModerneOrkwaffen         | 2h (geschätzt vom Ersteller)                     | ??? | ???                |  [Link](2018_ss/aufgaben/AB2/AB2-Bonus-Backtracking-ModerneOrkwaffen/AB2-Bonus-Backtracking-ModerneOrkwaffen-klar.md)                         |
|02 | Dynamische Programmierung                      | AB2-Bonus-Levenshtein-Tempelrätsel         | 2h (geschätzt vom Ersteller), Probegelöst in 2h | ??? | ???                |  [Link](2018_ss/aufgaben/AB2/AB2-Bonus-Levenshtein-Tempelraetsel/AB2-Bonus-Levenshtein-Tempelraetsel-klar.md)                         |
