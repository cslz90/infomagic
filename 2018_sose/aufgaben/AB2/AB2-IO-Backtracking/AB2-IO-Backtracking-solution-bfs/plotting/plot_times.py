import matplotlib.pyplot as plt

import numpy as np


def fit_line(xvals, yvals):
    coeffs, res, _, _, _ = np.polyfit(xvals, yvals, 1, full=True)
    return coeffs, 1 - (res[0]/np.var(yvals)/len(yvals))


def autoreg(xvals, yvals):
    xvals = np.asarray(xvals)
    yvals = np.asarray(yvals)
    mask = np.where(np.logical_and(xvals > 0, yvals > 0))
    xvals = xvals[mask]
    yvals = yvals[mask]
    versions = [
        ("log", np.log(xvals), yvals),
        ("poly", np.log(xvals), np.log(yvals)),
        ("exp", xvals, np.log(yvals))
    ]
    fits = [(name, fit_line(x, y)) for name, x, y in versions]
    # poly -> fit_line -> coeffs -> intercept
    fits[1][1][0][1] = np.exp(fits[1][1][0][1])
    # exp -> fit_line -> coeffs -> slope
    fits[2][1][0][0] = np.exp(fits[2][1][0][0])
    # exp -> fit_line -> coeffs -> intercept
    fits[2][1][0][1] = np.exp(fits[2][1][0][1])
    # print([x[1][1] for x in fits])
    best = max(fits, key=lambda x: x[1][1])
    best_name, (best_coeffs, best_r2) = best
    return (best_name, best_coeffs, best_r2)


def coeffs_to_str(ftype, coeffs):
    if (ftype == "log"):
        return "%f log x + %f" % tuple(coeffs)
    if (ftype == "poly"):
        return "%f x^%f" % tuple(coeffs[::-1])
    if (ftype == "exp"):
        return "%f %f^x" % tuple(coeffs[::-1])
    return "unknown function type '%s'" % ftype


def coeffs_to_func(ftype, coeffs):
    if (ftype == "log"):
        return lambda x: coeffs[0] * np.log(x) + coeffs[1]
    if (ftype == "poly"):
        return lambda x: coeffs[1] * np.power(x, coeffs[0])
    if (ftype == "exp"):
        return lambda x: coeffs[1] * np.power(coeffs[0], x)
    raise RuntimeError("unknown ftype %s" % ftype)


def read_benchmark(fname):
    with open(fname, "r", encoding="utf-8") as f:
        raw = [x.split(";") for x in f.readlines()]
        headers = [x.strip() for x in raw[0]]
        raw = raw[1:]
        res = {}
        for i, h in enumerate(headers[:-1]):
            res[h] = [int(x[i]) for x in raw]
        res[headers[-1]] = [eval(x[-1]) for x in raw]
        return res


def find_differences(dres, jres):
    ddata = read_benchmark(dres)
    jdata = read_benchmark(jres)
    inputs = np.array(ddata["input"])
    dlengths = np.array([len(x) for x in ddata["solution"]])
    jlengths = np.array([len(x) for x in jdata["result"]])
    combined = np.column_stack([inputs, dlengths, jlengths])
    differences = combined[combined[:, 1] != combined[:, 2]]
    print(differences)
    print(len(differences))


def check_data(data, labels):
    errors = []
    for i in range(max([len(d["input"]) for d in data])):
        compare = [
            (d["input"][i], d["solution"][i], l) for d, l in zip(data, labels)
            if i < len(d["input"])
        ]
        wrong_sums = [l for i, s, l in compare if sum(s) != i]
        wrong_inputs = [l for i, s, l in compare if i != compare[0][0]]
        wrong_solutions = [
            l for i, s, l in compare
            if not np.alltrue(s == compare[0][1])
        ]
        errors.extend([
            "wrong sum %d for method %s at input %d" % (sum(s), l, i)
            for i, s, l in compare if sum(s) != i and i not in [1, 3, 5]
        ])
        errors.extend([
            "wrong input %d for method %s" % (i, l)
            for i, s, l in compare if i != compare[0][0]
        ])
        errors.extend([
            "solution %s not optimal for method %s at input %d" % (s, l, i)
            for i, s, l in compare
            if not np.alltrue(np.sort(s) == np.sort(compare[0][1]))
        ])
    if len(errors) > 0:
        raise RuntimeError("\n".join(errors))


def plot_times(fname):
    data = read_benchmark(fname)
    inputs = data["input"]
    keys = [x for x in data.keys() if x not in ["solution", "input"]]
    for k in keys:
        plt.semilogy(inputs, data[k], label=k)
    plt.legend(loc="best")
    plt.show()


def plot_times_multi(fnames):
    data = [read_benchmark(fn) for fn in fnames]
    check_data(data, fnames)
    inputs = data[0]["input"]
    sig = 5
    l = 21
    gfilter = 1 / np.sqrt(2 * np.pi * sig**2) * np.exp(- (np.arange(l) - l/2)**2 / (2 * sig**2))
    gfilter /= sum(gfilter)
    plt.subplot(211)
    colors = [
        "#ff5a4d",
        "#02ce4e",
        "#6711c6",
        "#bab500",
        "#0048b2",
        "#ff9f25",
        "#fe7aff",
        "#df6300",
        "#ff3dae",
        "#d60044"
    ]
    for (i, (d, fn)) in enumerate(zip(data, fnames)):
        plt.loglog(d["input"], d["nsecs"], alpha=0.5, color=colors[i])
    for (i, (d, fn)) in enumerate(zip(data, fnames)):
        smoothed = np.exp(np.convolve(np.log(d["nsecs"]), gfilter, mode='valid'))
        plt.loglog(d["input"][l//2-1:-l//2], smoothed, color=colors[i], label=fn, alpha=0.8)
        reg = autoreg(d["input"], d["nsecs"])
        plt.loglog(d["input"], coeffs_to_func(*reg[:-1])(d["input"]), "--", color=colors[i], label=coeffs_to_str(*reg[:-1]))
    plt.legend(loc="best")
    plt.subplot(212)
    for (i, (d, fn)) in enumerate(zip(data, fnames)):
        plt.loglog(d["input"], d["calls"], alpha=0.5, color=colors[i])
    for (i, (d, fn)) in enumerate(zip(data, fnames)):
        smoothed = np.exp(np.convolve(np.log(d["calls"]), gfilter, mode='valid'))
        plt.loglog(d["input"][l//2-1:-l//2], smoothed, label=fn, color=colors[i], alpha=0.8)
        reg = autoreg(d["input"], d["calls"])
        plt.loglog(d["input"], coeffs_to_func(*reg[:-1])(d["input"]), "--", color=colors[i], label=coeffs_to_str(*reg[:-1]))
    plt.legend(loc="best")
    plt.show()


def plot_times_by_solution_length(fname):
    data = read_benchmark(fname)
    inputs = np.array(data["input"])
    lengths = np.array([len(x) for x in data["solution"]])
    xvals = np.unique(lengths)
    indices_by_length = [inputs[lengths == x] for x in xvals]
    keys = [x for x in data.keys() if x not in ["solution", "input", "backOpt_noopt"]]
    for k in keys:
        plt.semilogy(xvals, [np.mean(np.array(data[k])[idx-7]) for idx in indices_by_length], label=k)
    plt.legend(loc="best")
    plt.show()


def plot_times_by_solution_length_multi(fnames):
    data = [read_benchmark(fn) for fn in fnames]
    max_data = np.argmax([len(d["input"]) for d in data])
    inputs = np.array(data[max_data]["input"])
    lengths = np.array([len(x) for x in data[max_data]["solution"]])
    xvals = np.unique(lengths)
    indices_by_length = [lengths == x for x in xvals]
    plt.subplot(211)
    for d, l in zip(data, fnames):
        n = len(d["input"])
        max_len = np.max(lengths[:n])
        plt.semilogy(
            xvals[:n],
            [np.median(np.asarray(d["nsecs"])[idx[:n]]) for idx in indices_by_length],
            label=l
        )
    plt.legend(loc="best")
    plt.subplot(212)
    for d, l in zip(data, fnames):
        n = len(d["input"])
        max_len = np.max(lengths[:n])
        plt.semilogy(
            xvals[:n],
            [np.median(np.asarray(d["calls"])[idx[:n]]) for idx in indices_by_length],
            label=l
        )
    plt.legend(loc="best")
    plt.show()


if __name__ == "__main__":
    benchmarks = []
    benchmarks.append("../res/bench_backOpt_onlyBBc.csv")
    benchmarks.append("../res/bench_backOpt.csv")
    benchmarks.append("../res/bench_backOptc.csv")
    benchmarks.append("../res/bench_backOpt_noopt.csv")
    benchmarks.append("../res/bench_backOpt_onlyBB.csv")
    benchmarks.append("../res/bench_backOpt_onlyDP.csv")
    benchmarks.append("../res/bench_bfs.csv")
    benchmarks.append("../res/bench_dynamic.csv")
    benchmarks.append("../res/bench_iddfs.csv")
    # find_differences("../res/benchmark.csv", "../res/deworetzki_results.csv")
    # print("done")
    # plot_times("../res/benchmark.csv")
    # plot_times_by_solution_length("../res/benchmark.csv")
    plot_times_multi(benchmarks)
    # plot_times_by_solution_length_multi(benchmarks)
    x = np.arange(1, 1000)
    print(autoreg(x, 4 * x**3))
    print(autoreg(x, 2 * np.log(x) + 9))
    print(autoreg(x, 3.5 * 1.1**x))
    for fn in benchmarks:
        print(fn)
        data = read_benchmark(fn)
        name, coeffs, r2 = autoreg(data["input"], data["nsecs"])
        print(name, coeffs, r2)
        print(coeffs_to_str(name, coeffs))
