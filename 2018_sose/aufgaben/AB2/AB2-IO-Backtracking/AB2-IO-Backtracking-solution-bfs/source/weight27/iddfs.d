module weigth27.iddfs;

import weight27.util: gen27, callCount, Task, ChildTask, BinaryTask;
import std.typecons: Nullable;
import std.container.slist: SList;
import std.range: array;

Nullable!(long[]) dfs27(Task task, int maxDepth = int.max) {
  if (task.targetWeight == 0) {
    return Nullable!(long[])([]);
  }
  if (task.targetWeight < 0) {
    return Nullable!(long[])();
  }
  if (maxDepth <= 0) {
    return Nullable!(long[])();
  }
  callCount++;
  foreach(ChildTask c; task.childTasks()) {
    auto res = dfs27(c.task, maxDepth - 1);
    if (!res.isNull) {
      return Nullable!(long[])(c.weightsChosen ~ res.get);
    }
  }
  return Nullable!(long[])();
}

long[] iddfs27(long target) {
  return iddfs27(new BinaryTask(target));
}

long[] iddfs27(Task task) {
  if (task.targetWeight == 1 || task.targetWeight == 3 || task.targetWeight == 5) {
    // required, because otherwise we would run into infinite loop later
    return [];
  }
  Nullable!(long[]) res;
  int maxDepth = 0;
  while (res.isNull) {
    res = dfs27(task, maxDepth);
    maxDepth += 1;
  }
  if (res.isNull) {
    return null;
  } else {
    return array(res.get[]);
  }
}