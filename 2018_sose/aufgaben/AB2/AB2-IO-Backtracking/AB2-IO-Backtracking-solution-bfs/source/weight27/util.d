module weight27.util;

import std.array: appender;
import std.algorithm.mutation: reverse;
import std.string: format;
import std.algorithm: min, map;
import std.range: array;

class ArraySizeOverflowException: Exception {
  this(long n, string file = __FILE__, size_t line = __LINE__) {
    super(format("cannot store %d elements in an array", n), file, line);
  }
}

class ChildTask {
  Task task;
  long[] weightsChosen;
  this(Task task, long[] weightsChosen) {
    this.task = task;
    this.weightsChosen = weightsChosen;
  }
}

abstract class Task {
  immutable long[] counterWeights;
  immutable long targetWeight;
  immutable bool isComplete;
  ChildTask[] childTasks();
  this(long targetWeight) {
    immutable long[] counterWeights = gen27(targetWeight).idup;
    this(targetWeight, counterWeights, true);
  }
  this(long targetWeight, immutable long[] counterWeights, bool isComplete) {
    this.targetWeight = targetWeight;
    this.counterWeights = counterWeights;
    this.isComplete = isComplete;
  }
}

// just use the same counter weights at each level
class NaiveTask: Task {
  this(long targetWeight) {
    super(targetWeight);
  }
  this(long targetWeight, immutable long[] counterWeights) {
    super(targetWeight, counterWeights, true);
  }
  override ChildTask[] childTasks() {
    return counterWeights.map!(w => new ChildTask(new NaiveTask(targetWeight - w, counterWeights), [w] )).array;
  }
}

// do not allow non-sorted solutions
class SortedTask: Task {
  this(long targetWeight) {
    super(targetWeight);
  }
  this(long targetWeight, immutable long[] counterWeights) {
    super(targetWeight, counterWeights, true);
  }
  override ChildTask[] childTasks() {
    ChildTask[] res;
    res.length = counterWeights.length;
    for(int i = 0; i < counterWeights.length; i++) {
      long w = counterWeights[i];
      // only use counterWeights that are of the same or smaller size as the
      // weight that is added in this step => disallow unsorted solutions
      res[i] = new ChildTask(new SortedTask(targetWeight - w, counterWeights[i..$]), [w]);
    }
    return res;
  }
}

// either take a weight or don't
class YesNoTask: Task {
  this(long targetWeight) {
    super(targetWeight);
  }
  this(long targetWeight, immutable long[] counterWeights) {
    super(targetWeight, counterWeights, true);
  }
  override ChildTask[] childTasks() {
    if (counterWeights.length == 0) { return []; }
    return [
      // use the highest weight once and keep it in the counterWeights
      new ChildTask(
        new YesNoTask(targetWeight - counterWeights[0], counterWeights),
        [counterWeights[0]]
      ),
      // do not use the highest weight and remove it from counterWeights
      new ChildTask(
        new YesNoTask(targetWeight, counterWeights[1..$]),
        []
      )
    ];
  }
}

class CompressedYesNoTask: Task {
  // like YesNoTask, but take maximum amount at once
  this(long targetWeight) {
    super(targetWeight);
  }
  this(long targetWeight, immutable long[] counterWeights, bool isComplete) {
    super(targetWeight, counterWeights, isComplete);
  }
  override ChildTask[] childTasks() {
    if (counterWeights.length == 0) { return []; }
    ChildTask[] res;
    long w = counterWeights[0];
    if (targetWeight >= 10 * w) {
      // if we skipped two weights, we cannot get an optimal solution anymore
      // see BinaryTask for a proof
      return [];
    }
    int times = cast(int) (targetWeight / w);
    long[] weightsChosen;
    weightsChosen.length = times;
    weightsChosen[] = w;
    res.length = times + 1;
    for(int i = times; i >= 0; i--) {
      long newTarget = targetWeight - i * w;
      // task cannot be considered complete anymore if we skip ahead multiple
      // steps and could have missed a solution using fewer instances of
      // counterWeights[0]
      bool complete = isComplete && (i == 1 || newTarget < counterWeights[0]);
      res[times-i] = new ChildTask(
        new CompressedYesNoTask(newTarget, counterWeights[1..$], complete),
        weightsChosen[0..i]
      );
    }
    return res;
  }
}

immutable(long[]) reduceWeights(long target, immutable long[] counterWeights) {
  if (counterWeights[$-1] > target) return [];
  int skip = 0;
  while(counterWeights[skip] > target) { skip++; }
  return counterWeights[skip..$];
}

class BinaryTask: Task {
  // only allows binary choice taking the highest current weight or the
  // second highest

  /*
  Proof that this is correct for weights w_i =  2, 7, 20, 70, 200, 700, ... :

  Lemma 1: Solutions that use 7 or more instances of the same weight are not
    optimal.

  Proof of Lemma 1:
    If you take 7 times 2 * 10^x, you could have taken 2 times 7 * 10^x.
    If you take 7 times 7 * 10^x, you could have taken
      2 times 2 * 10^(x+1) + 1 times 7 * 10^x + 1 times 2 * 10^x.

  Lemma 2: If weight w_i fits into the target weight, the next smaller weight
    w_(i-1) fits at least least 2.8 times into the target weight.

  TODO: Lemma 2 and 3 can be replaced by proof using sum of remaining weights

  Proof of Lemma 2:
    If w_i = 2 * 10^x < t then w_(i-1) = 7 * 10^(x-1) and
    2.8 * w_(i-1) = 1.96 * 10^x < 2 * 10^x < t.
    If w_i = 7 * 10^x < t then w_(i-1) = 2 * 10^x and
    2.8 * w_(i-1) = 5.6 * 10^x < 7 * 10^x < t.

  Lemma 3: It is not possible to avoid using one instance of a weight 7 times
    if you skip the two largest weights w_i and w_(i-1) that fit into your
    target weight.

  Proof of Lemma 3:
    Since w_(i-2) = 10 * w_i for any i and t > w_i we have
    t > 10 * w_(i-2). To avoid using too many weights, we use the maximum
    allowed number of 6 times w_(i-2). This leaves us with
    t > 6 * w_(i-2) + 4 * w_(i-2) and the second summand can only be balanced
    with weights smaller than w_(i-3). From Lemma 2 we know that
    4 * w_(i-2) < 4 * 2.8 * w_(i-3) = 11.2 * w_(i-3). Since we never have made
    any restrictions for i in Lemma 2 or 3 we can conclude that
    t > 6 * w_(i-2) + 6 * w_(i-3) + 6 * w_(i-4) + ... + 6 * 2 and this means
    that using 6 instances of all remaining weights will not be enough.
    Any solution that we find has to use at least one weight at least 7 times.

  Theorem: It is not possible to find an optimal solution if you do not use
    the largest and the second largest weight that fit in your target weight.

  Proof:
    Trivial from combination of Lemma 2 and 3.
  */

  this(long targetWeight) {
    super(targetWeight);
  }
  this(long targetWeight, immutable long[] counterWeights) {
    super(targetWeight, reduceWeights(targetWeight, counterWeights), true);
  }
  override ChildTask[] childTasks() {
    if (counterWeights.length == 0) { return []; }
    ChildTask[] res;
    res.length = min(counterWeights.length, 2);
    for(int i = 0; i < res.length; i++) {
      res[i] = new ChildTask(
        new BinaryTask(targetWeight - counterWeights[i], counterWeights),
        [counterWeights[i]]
      );
    }
    return res;
  }
}

long[] gen27(long weight) {
  long i = 0;
  long w = 2;
  long pot = 1;
  auto app = appender!(long[]);
  while (w <= weight) {
    app ~= w;
    i += 1;
    if (i % 2 == 0) pot *= 10;
    w = i % 2 == 0 ? 2 * pot : 7 * pot;
  }
  reverse(app.data);
  return app.data;
}

int[] gen27i(int weight) {
  int i = 0;
  int w = 2;
  int pot = 1;
  auto app = appender!(int[]);
  while (w <= weight) {
    app ~= w;
    i += 1;
    if (i % 2 == 0) pot *= 10;
    w = i % 2 == 0 ? 2 * pot : 7 * pot;
  }
  reverse(app.data);
  return app.data;
}

// global variable to count recursive calls
long callCount = 0;
