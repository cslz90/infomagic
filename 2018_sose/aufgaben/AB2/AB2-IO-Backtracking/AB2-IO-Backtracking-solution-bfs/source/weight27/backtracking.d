module weight27.backtracking;

import weight27.util: gen27, ArraySizeOverflowException, callCount, Task, ChildTask, NaiveTask;
import std.typecons: Flag, Yes, No;
import std.container.slist: SList;
import std.range: walkLength, array, InputRange, InputRangeObject, inputRangeObject, take;
import std.stdio: writeln;
import std.string: format;

class Solution {
  SList!long weightsUsed;
  size_t weightCount;
  bool isValid;
  this(bool isValid) {
    this(SList!long(), isValid ? 0 : size_t.max, isValid);
  }
  this(SList!long weightsUsed) {
    this(weightsUsed, walkLength(weightsUsed[]));
  }
  this(SList!long weightsUsed, size_t weightCount) {
    this(weightsUsed, weightCount, true);
  }
  this(SList!long weightsUsed, size_t weightCount, bool isValid) {
    this.weightsUsed = weightsUsed;
    this.weightCount = weightCount;
    this.isValid = isValid;
  }
  Solution addWeight(long w) {
    return addWeights([w]);
  }
  Solution addWeights(long[] weights) {
    auto nextWeights = this.weightsUsed.dup();
    foreach(long w; weights) {
      nextWeights.insertFront(w);  
    }
    return new Solution(nextWeights, this.weightCount + weights.length, true);
  }
  Solution removeWeight(long w) {
    auto nextWeights = this.weightsUsed.dup();
    nextWeights.linearRemoveElement(w);
    return new Solution(nextWeights, this.weightCount - 1, true);
  }
  long[] toArray() {
    return this.weightsUsed.array;
  }
}

class NotSupportedException: Exception {
  this() {
    super("opration not supported");
  }
}

interface Brain {
  Solution opIndex(long weight);
  Solution opIndexAssign(Solution v, long i);
  bool contains(long weight);
  final void populate(long task, Solution s) {
    if (!this.contains(task)) this[task] = s;
    foreach(long w; s.weightsUsed) {
      // if we remove one weight, the remaining weights must be the solution
      // for the remaining task, otherwise the base solution would not be optimal
      long newTask = task - w;
      if (this.contains(newTask)) continue;
      Solution newSolution = s.removeWeight(w);
      this.populate(newTask, newSolution);
    }
  }
}

class DummyBrain: Brain  {
  Solution opIndex(long weight) { throw new NotSupportedException(); }
  Solution opIndexAssign(Solution v, long weight) { return v; }
  bool contains(long weight) { return false; }
}

class HashBrain: Brain {
  Flag!"populate" pop;
  this(Flag!"populate" pop=Yes.populate) {
    this.pop = pop;
  }
  Solution[long] hash;
  Solution opIndex(long weight) {
    return hash[weight];
  }
  Solution opIndexAssign(Solution v, long weight) {
    hash[weight] = v;
    if (this.pop) this.populate(weight, v);
    return v;
  }
  bool contains(long weight) {
    auto pt = weight in hash;
    return pt !is null;
  }
}

class DynamicArrayBrain: Brain {
  long[] ar;
  Flag!"populate" pop;
  this(long targetWeight, Flag!"populate" pop=Yes.populate) {
    if (targetWeight + 1 > size_t.max) {
      throw new ArraySizeOverflowException(targetWeight + 1);
    }
    ar.length = cast(size_t) targetWeight+1;
    ar[] = -1;
    ar[0] = 0;
    this.pop = pop;
  }
  Solution opIndex(long weight) {
    SList!long weightsUsed;
    long current = weight;
    while (current > 0) {
      weightsUsed.insertFront(current - ar[cast(size_t) current]);
      current = ar[cast(size_t) current];
    }
    Solution s = new Solution(weightsUsed);
    return s;
  }
  Solution opIndexAssign(Solution v, long weight) {
    //if (weight >= ar.length) {
    //  // should never be the case, if the brain was initialized correctly
    //  // but we need to increase the array length if we receive a higher weight
    //  size_t oldLength = ar.length;
    //  ar.length = weight + 1;
    //  ar[oldLength..$] = -1;
    //}
    long remaining = weight;
    foreach(long w; v.weightsUsed) {
      ar[cast(size_t) remaining] = remaining - w;
      remaining = remaining - w;
    }
    if (this.pop) this.populate(weight, v);
    return v;
  }
  bool contains(long weight) {
    return ar.length > weight && ar[cast(size_t) weight] >= 0;
  }
}

long[] backOpt27(
  long inputWeight,
  Flag!"useBB" useBB = Yes.useBB
) {
  Brain brain = new HashBrain();
  Task task = new NaiveTask(inputWeight);
  return backOpt27(task, brain, useBB);
}

long[] backOpt27(
  Task task,
  ref Brain brain,
  Flag!"useBB" useBB = Yes.useBB
) {
  return backOpt27rek(task, brain, useBB).toArray();
}

Solution backOpt27rek(
  Task task,
  ref Brain brain,
  Flag!"useBB" useBB = Yes.useBB,
  size_t bestFullCount = size_t.max,
  size_t weightCount = 0
) {
  if (task.targetWeight == 0) {
    return new Solution(true);
  }
  if (task.targetWeight < 0) {
    return new Solution(false);
  }
  if (useBB && weightCount >= bestFullCount) {
    return new Solution(false); // branch & bound
  }
  if (brain.contains(task.targetWeight)) {
    // dynamic programming
    // writeln(format("+found %d in brain: %s", task.targetWeight, brain[task.targetWeight].toArray()));
    auto res = brain[task.targetWeight];
    if (useBB && res.weightCount + weightCount >= bestFullCount) {
      //writeln(format(
      //  "-ignoring brain solution, because %d >= %d",
      //  res.weightCount + weightCount, bestFullCount
      //));
      // do not consider solutions that bring no improvement
      return new Solution(false);
    }
    return res;
  }
  callCount++;
  auto best = new Solution(false);
  bool bestComplete = false;
  foreach(ChildTask t; task.childTasks()) {
    auto res = backOpt27rek(t.task, brain, useBB, bestFullCount, weightCount + t.weightsChosen.length);
    bool better;
    if (useBB) {
      // if we use branch and bound we can only consider solutions that improve
      // bestFullCount, because otherwise there could have been better
      // solutions that were not investigated because *all* solutions would be
      // too long
      better = res.weightCount + t.weightsChosen.length + weightCount < bestFullCount;
    } else {
      better = res.weightCount + t.weightsChosen.length < best.weightCount;
    }
    if (res.isValid && better) {
      best = res.addWeights(t.weightsChosen);
      // do not consider solution for DP if recursive call could have but did
      // not explore solution using larger weights which are not present in
      // child tasks
      bestComplete = t.task.isComplete;
      auto fullCount = best.weightCount + weightCount;
      // writeln(format("found solution %s at level %d (%s)", best.weightsUsed.array, weightCount, bestComplete));
      if (useBB) {
        // writeln(format("%d < %d at level %d", fullCount, bestFullCount, weightCount));
        bestFullCount = fullCount;
      }
    }
  }
  //if (weightCount == 5) { writeln("%d: %s".format(task.targetWeight, best.weightsUsed.array)); }
  if (best.isValid && bestComplete) {
    // writeln(format("found solution of size %d for %d at depth %d with bestFullCount = %d", best.weightCount, task.targetWeight, weightCount, bestFullCount));
    brain[task.targetWeight] = best;
  }
  return best;
}