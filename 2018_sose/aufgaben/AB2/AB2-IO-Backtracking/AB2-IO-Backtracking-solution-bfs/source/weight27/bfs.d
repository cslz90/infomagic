module weight27.bfs;

import weight27.util: gen27i, callCount, Task, ChildTask, BinaryTask;
import std.container.slist: SList;
import std.container.dlist: DList;
import std.range: walkLength, array;
import std.stdio: writeln;
import std.string: format;

interface Balance {
  Balance[] children();
  long[] toArray();
  bool isSolution();
  bool solvable();
  long id();
  final Balance[] childrenFor(Task task) {
    ChildTask[] tasks = task.childTasks();
    Balance[] children;
    children.length = tasks.length;
    for(int i = 0; i < tasks.length; i++) {
      children[i] = new BalanceNode(this, tasks[i]);
    }
    return children;
  }
}

class BalanceRoot: Balance {
  Task remaining;
  this(Task rootTask) {
    this.remaining = rootTask;
  }
  Balance[] children() {
    return childrenFor(remaining);
  }
  long[] toArray() {
    return [];
  }
  bool isSolution() { return remaining.targetWeight == 0; }
  bool solvable() { return true; }
  long id() { return remaining.targetWeight; }
}

class BalanceNode: Balance {
  ChildTask remaining;
  Balance parent;
  this(Balance parent, ChildTask remaining) {
    this.parent = parent;
    this.remaining = remaining;
  }
  Balance[] children() {
    return childrenFor(remaining.task);
  }
  long[] toArray() {
    long[] parentAr = this.parent.toArray();
    return parentAr ~ remaining.weightsChosen;
  }
  bool isSolution() { return remaining.task.targetWeight == 0; }
  bool solvable() { return remaining.task.targetWeight >= 0; }
  long id() { return remaining.task.targetWeight; }
}

long[] bfs27(long target) {
  return bfs27(new BinaryTask(target));
}

long[] bfs27(Task task) {
  auto q = DList!Balance();
  q.insertFront(new BalanceRoot(task));
  bool[long] visited;
  while (!q.empty) {
    auto b = q.back;
    visited[b.id] = true;
    q.removeBack();
    if (b.isSolution()) {
      return b.toArray();
    }
    if (!b.solvable()) {
      continue;
    }
    callCount++;
    foreach (Balance next; b.children()) {
      if (! (next.id in visited)) {
        q.insertFront(next);
      }
    }
  }
  return null;
}