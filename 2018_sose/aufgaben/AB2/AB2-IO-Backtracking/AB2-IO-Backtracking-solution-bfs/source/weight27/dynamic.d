module weight27.dynamic;

import weight27.util: gen27, callCount, Task, ChildTask, BinaryTask, ArraySizeOverflowException;
import std.array: appender;
import std.stdio: writeln;
import std.string: format;

interface DynamicBrain {
  long[] opIndex(long weight);
  long opIndexAssign(long lastWeight, long i);
  size_t solutionLength(long weight);
}

class D2ArrayBrain: DynamicBrain {
  byte[] ar;
  long[] counterWeights;
  byte[long] weightIndices;
  this(long weight) {
    if (weight > size_t.max) {
      throw new ArraySizeOverflowException(weight);
    }
    ar.length = cast(size_t)(weight+1);
    counterWeights = gen27(weight);
    for(byte i = 0; i < counterWeights.length; i++) {
      weightIndices[counterWeights[i]] = i;
    }
  }
  long[] opIndex(long weight) {
    //writeln("get %d".format(weight));
    auto app = appender!(long[]);
    size_t i = cast(size_t) weight;
    while(i > 0 && ar[i] >= 0) {
      long w = counterWeights[ar[i]];
      app ~= w;
      i -= w;
    }
    if (i > 0) return [];
    //writeln("result: %s".format(app.data));
    return app.data;
  }
  size_t solutionLength(long weight) {
    size_t len = 0;
    size_t i = cast(size_t) weight;
    while(i > 0 && ar[i] >= 0) {
      i -= counterWeights[ar[i]];
      len++;
    }
    return i > 0 ? size_t.max : len;
  }
  long opIndexAssign(long lastWeight, long i) {
    //writeln("set %d to %d".format(i, lastWeight));
    if (lastWeight < 0) {
      ar[cast(size_t) i] = -1;
    } else {
      ar[cast(size_t) i] = weightIndices[lastWeight];
    }
    return lastWeight;
  }
}

long[] dynamic27(long target) {
  DynamicBrain b = new D2ArrayBrain(target);
  return dynamic27(target, x => new BinaryTask(x), b);
}

long[] dynamic27(long target, Task delegate(long target) genTask, DynamicBrain brain) {
  for(long current = 1; current <= target; current++) {
    //writeln("current: %d".format(current));
    long best = -1;
    size_t bestLength = size_t.max;
    foreach(ChildTask c; genTask(current).childTasks()) {
      callCount++;
      size_t sl = brain.solutionLength(c.task.targetWeight) + c.weightsChosen.length;
      if (sl < bestLength && sl > 0) {
        best = c.weightsChosen[0];
        bestLength = sl;
      }
    }
    brain[current] = best;
  }
  return brain[target];
}