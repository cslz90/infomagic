# Bewertungsvorschläge


## Positive Faktoren:

  - Dozentron nimmt die Eingabe an.
  - Unit-Tests werden bestanden.
  - Backtracking ist im Code erkennbar.

## Weiche Kriterien:

  - Es wird nicht die optimale Lösung gefunden (mehr Zahlen als notwendig). D.h. die sichtbaren Tests werden bestanden, die unsichtbaren aber nicht.

## Unit-Tests

  - Siehe "Weiche Kriterien".

## Ungültig:

  - Es wird kein Backtracking verwendet.
