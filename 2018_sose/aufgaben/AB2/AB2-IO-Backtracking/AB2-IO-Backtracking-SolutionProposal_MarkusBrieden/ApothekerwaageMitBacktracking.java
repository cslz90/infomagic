import java.util.Random;

public class ApothekerwaageMitBacktracking {
    private static int[] gewichte = {2,7,20,70,200,700,2000,7000,20000};

    public static void main(String[] args) {
        Random ran = new Random();
        //int randomGewicht = ran.nextInt(100);
        System.out.println("Das gegebene Gewicht war: " + 4745);
        System.out.println(anzahlDerGewichte(4745));
    }

    public static String anzahlDerGewichte(int verbleibendesGewicht){
        boolean[] alreadyTried = {false, false, false, false, false, false, false, false, false};
        if(verbleibendesGewicht == 0){
            return "";
        }
        for(int i = 8; i >= 0; i--){
            if(gewichte[i] <= verbleibendesGewicht){
                if(alreadyTried[i]){
                    continue;
                }
                String rest = anzahlDerGewichte(verbleibendesGewicht-gewichte[i]);
                if(rest != null){
                    return "" + gewichte[i] + (rest == "" ? "" : (", " + rest));
                }
                else{
                    alreadyTried[i] = true;
                }
            }
        }
        return null;
    }


}
