
import java.util.Random;

class Apothekerwaage{
    static int[] gewichte = {2,7,20,70,200,700,2000,7000,20000};
    public static void main(String[] args) {
        //Random ran = new Random();
        //int randomGewicht = ran.nextInt(50_000);
        System.out.println("Das gegebene Gewicht war: " + 4745);
        System.out.println(anzahlDerGewichte(4745));
    }

    static String anzahlDerGewichte(int gewicht){
        int[] benutzteGewichte = {0, 0, 0, 0, 0, 0, 0, 0, 0};
        int jetzigesGewicht = 0;

        for(int i = 8; i >= 0; i--){
            if(gewicht-jetzigesGewicht >= gewichte[i]){
                benutzteGewichte[i] += 1;
                jetzigesGewicht += gewichte[i];
                i = i + 1;
            }
        }
        return benutzteGewichteToString(benutzteGewichte, gewicht);
    }

    static String benutzteGewichteToString(int[] benutzteGewichte, int gewicht){
        int gesamtGewicht = 0;
        for(int i = 0; i < benutzteGewichte.length ; i++){
            gesamtGewicht += (gewichte[i] * benutzteGewichte[i]);
        }
        if(gesamtGewicht != gewicht){
            return null;
        }

        String out = "";
        for (int i = benutzteGewichte.length - 1; i >= 0; i--) {
            for (int j = 0; j < benutzteGewichte[i]; j++) {
                out += gewichte[i] + ", ";
            }
        }
        return (out.length() >= 2) ? out.substring(0, out.length() - 2) : out;
    }
}