# Orkwippe

*Die Region um Murberg ist ein friedliches Fleckchen Erde, fernab jeder Zivilisation. Dorthin hat es eine Orkfamilie verschlagen, die genug von den ewigen Auseinandersetzungen zwischen Menschen und Elfen hat. Das Familienoberhaupt nimmt sich täglich nach Feierabend immer etwas Zeit, um mit seinen zahlreichen Kindern zu spielen.*

*Die Kleinen haben sich diesmal etwas besonderes einfallen lassen: Mit dem Stamm eines umgestürzten Baumes und ein paar Felsbrocken haben sie eine große Wippe gebaut und möchten sie gemeinsam mit ihrem Vater ausprobieren. Natürlich ist ein ausgewachsener Ork viel schwerer als seine Kinder und so muss er mehrere von ihnen auf die andere Seite der Wippe setzen. Glücklicherweise hat er sehr viel Nachwuchs, denn in dieser abgelegenen Gegend gibt es häufig nicht viel zu tun.*

*In seinem Perfektionismus versucht der Orkvater genau die Kinder auf die Wippe zu setzen, die das genaue Gegengewicht zu ihm bilden. Normalerweise können Orks Magie nicht ausstehen, aber die frische Landluft macht ihn empfänglich für Neues. Da er nicht mehr Plündern und Brandschatzen muss, hat er genug Zeit, hin und wieder die Akademie von Murberg zu besuchen. Dort hat er einige Grundzüge der Algomantik beim Großmeister L'tschat gelernt.*

*Jetzt möchte unser Ork versuchen seine neu gewonnenen Kenntnisse anzuwenden und einen Zauber entwickeln, der ihm hilft, seine Kinder auf der Wippe anzuordnen. Sein Zauber ermittelt zunächst das Gewicht für eine beliebige Anordnung der Kinder. Wird damit nicht das Gewicht des Vaters erreicht, muss in der Berechnung das letzte Kind ausgetauscht werden. Findet sich auf diese Weise auch keine Lösung, muss der Zauber einen Schritt zurück gehen und das Kind austauschen, das an vorletzter Stelle war. Auf diese Art geht der Zauber vor, bis er das Zielgewicht erreicht hat.*

### Aufgabe:

*Entwickeln sie einen Zauber, der für eine beliebige Menge an Orkkindern unter Angabe deren Gewichts die Kombination ermittelt, mit der das Gewicht des Orkvaters erreicht werden kann.*

## Formalia:

Sie erhalten als Input eine beliebige Anzahl zufällig generierter, durch Zeilenumbrüche getrennter Gewichte.
Geben Sie im Output-Feld für jede Input-Zeile die optimale Kombination an oben genannten Gegengewichten an und schreiben Sie sie wie im Beispiel (siehe unten) gezeigt auf.

Die Gegengewichte setzen sich nach folgendem Schema zusammen:

> 0, 2, 7, 20, 70, 200, 700, 2000, ...

### Bitte beachten:
- Die Ausgabezeilen müssen der Reihenfolge der Eingabezeilen folgen.
- Innerhalb der Ausgabemengen sollte es keine Leerzeichen oder anderen Whitespace geben.
- Es dürfen keine Leerzeilen im Output stehen!
- Bitte reichen Sie Ihren Quellcode zur Korrektheitsprüfung ein.
- Ihr Programm muss Backtracking benutzen!
- Jedes Gegengewicht darf mehrmals verwendet werden.
- Der Generator wird keine Gewichte generieren, die sich nicht mithilfe von Gegengewichten aus dieser Menge lösen lassen.
- Sie müssen die **optimale** Kombination an Gewichten, d.h. die geringst mögliche Anzahl der benutzten Gegengewichte finden.

## Beispiel:

**Generierter Input**:

```  
80
141
0
777
```

**Ihr Output**:  

```
20,20,20,20
70,20,20,20,7,2,2
0
700,70,7
```
