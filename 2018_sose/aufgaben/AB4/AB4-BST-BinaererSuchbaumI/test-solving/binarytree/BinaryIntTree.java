package binarytree;

public class BinaryIntTree extends ABinaryIntTree {

    ABinaryIntTreeNode root;
    int size;

    @Override
    public ABinaryIntTreeNode root() {
        return root;
    }

    @Override
    public void add(int value) {
        if (root == null) {
            root = new BinaryIntTreeNode(value);
            size++;
        } else if (!contains(value)) {
            sinsert(root, value);
            size++;
        }
    }

    private void sinsert(ABinaryIntTreeNode tree, int el) {
        ABinaryIntTreeNode next = null;
        if (el < tree.value()) next = tree.left();
        else if (el > tree.value()) next = tree.right();
        if (next == null) tree.add(new BinaryIntTreeNode(el));
        else sinsert(next, el);
    }

    private ABinaryIntTreeNode bsearch(ABinaryIntTreeNode haystack, int needle) {
        if (haystack == null) return null;
        else if (haystack.value() == needle) return haystack;
        else if (haystack.value() > needle) return bsearch(haystack.left(), needle);
        else return bsearch(haystack.right(), needle);
    }

    @Override
    public boolean contains(int value) {
        return bsearch(root, value) != null;
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public int size() {
        return size;
    }
}
