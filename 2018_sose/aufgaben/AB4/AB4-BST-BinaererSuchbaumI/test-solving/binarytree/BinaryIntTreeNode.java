package binarytree;

public class BinaryIntTreeNode extends ABinaryIntTreeNode {

    ABinaryIntTreeNode left;
    ABinaryIntTreeNode right;
    int value;

    BinaryIntTreeNode(int value) {
        this.value = value;
    }

    @Override
    public int value() {
        return value;
    }

    @Override
    public void add(ABinaryIntTreeNode child) {
        if (child.value() < value) left = child;
        else right = child;
    }

    @Override
    public ABinaryIntTreeNode left() {
        return left;
    }

    @Override
    public ABinaryIntTreeNode right() {
        return right;
    }
}
