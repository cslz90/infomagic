# Musterlösung zur Pseudocodeaufgabe "Binärborkige Speichereichenmodels"

Ein möglicher Pseudocode:

```
algorithm isBalanced(tree: Node<E>)
    if tree == NULL then
        return true

    int heightLeft := getHeigth(tree.left)
    int heightRight := getHeigth(tree.right)
    
    int diff := Math.abs(heightLeft - heightRight)
    if diff <= 1 and isBalanced(tree.left) and isBalanced(tree.right) then
        return true
    
    return false


algorithm getHeight(node: Node<E>): int
    if (node == null)
        return 0

    int heightLeft := getHeigth(tree.left)
    int heightRight := getHeigth(tree.right)

    return 1 + Math.max(heightLeft, heightRight)
```