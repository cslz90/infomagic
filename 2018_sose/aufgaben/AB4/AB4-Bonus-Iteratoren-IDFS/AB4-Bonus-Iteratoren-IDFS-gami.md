# Der Wettkampf der Iteratoren - Die Rückkehr der Tiefentaube

*__Was bisher geschah ...__*

*Das alljährliche Frühlingsfest von Ja'va im Wald der tausend Speichereichen neigt sich dem Ende zu. Viele Gläser Zaubertrank wurden getrunken und durch die Abendsonne, welche über den Wipfeln der Speichereichen langsam verschwindet, ist der Wald in ein warmes rot getaucht. Die Spannung hat ihren Höhepunkt erreicht, denn die zwei Teilnehmer am Wettkampf der Iteratoren, Gilbert der Tiefsinnige und Igor der Breite, sind bereits mit ihren dressierten Tieren angetreten und konnten der Katzenprinzessin Justina eine ihrer begehrten Eulerschen Knotenfrüchte vom Baum pflücken. Nun ist es an der Zeit, dass sich Justina die Quantenverschränkende entscheidet, wer die bessere Leistung erbracht hat und um ihre Hand anhalten darf.*

### Einleitung

*Doch was ist das? Am Horizont erscheint eine Gestalt. Nur ihre Umrisse sind zu erkennen: Langes Haar Haar bewegt sich im Wind und auf der Schulter sitzend, erkennt man einen Vogel. Die Gestalt kommt langsam näher und es handelt sich um die Iteratorin Brunhilde die Schreitende. Zusammen mit ihrer Tiefentaube möchte sie sich auch der Herausforderung stellen und am Wettkampf teilnehmen. Auch ihr Tier ist dressiert und hört auf die Befehle Amplius und Proxime, doch um zu vermeiden, dass sich ihre Tiefentaube in den unendlichen Höhen der Speichereiche verirrt, hat Brunhilde ihre Taube an einer Leine befestigt, um sie jeder Zeit zurückholen zu können.*

*Brunhilde die Schreitende verfolgt die gleiche Taktik wie Gilbert der Tiefsinnige, nur dass Sie ihre Tiefentaube immer nur bis zu einer bestimmten Ebene des Baumes fliegen lässt und sie dann zu sich zurückzieht, um Bericht erstattet zu bekommen. So kann Sie die Speichereiche kontrolliert absuchen und findet schließlich auch die Eulersche Knotenfrucht.*

### Aufgabe 1

*Entwickeln sie die Zauber, mit der die angebundene Tiefentaube dressiert wurde. Diese sind:*
- *Amplius?* (`public boolean hasNext()`)  
- *Proxime!* (`public E next()`)
*Die beiden Zauber sind Teil der Rolle des Iterators.*

*Achten sie darauf, dass die angebundene Tiefentaube ihrem Namen alle Ehre macht.*

### Aufgabe 2

*Überlegen sie sich, warum der Einsatz einer auf diese Art dressierten angebundenen Tiefentaube besonders beeindruckend sein könnte. Vor allem im Vergleich zum Einsatz einer Tiefentaube, die nicht angeleint ist, oder eines Breitenhörnchen.*

#### Formalia:

- Schreiben sie eine Klasse `public class IDDFSIterator<E> implements Iterator<E>`, die einen IDDFS-Iterator implementiert.
- Der Iterator soll über einen Baum iterieren, der das Interface `TreeInterface<E>` implementiert.    
  - Sowohl das Interface, als auch eine implementierende Klasse `Tree<E>` finden sie in den Codevorgaben. Ihr Iterator sollte nur die Methoden des Interfaces benutzen (das wird in den Tests geprüft).
  - Dort finden sie außerdem ein "Skelett" für die Iteratorklasse (`IDDFSIterator`). Nutzen sie dieses.
- Innerhalb des Iterators dürfen sie zur Zwischenspeicherung von Knoten nur eine `Deque<E>` und eine Referenz auf einen Kindindex, sowie einen Tiefenzähler benutzen.
  - Außerdem dürfen sie von der `Deque` nur die Methoden `addFirst(e)`, `removeFirst()` und `peekFirst()` benutzen. Damit hat sie das Verhalten eines Stacks (die Klasse `Stack` darf nicht verwendet werden, da sie als veraltet gilt).
  - Speichern sie bei der Erstellung des Iterators nicht alle Knoten in der `Deque`. Ihr Iterator muss bei jedem Aufruf von `next()` schrittweise vorgehen.

#### Hinweise:

  >- Überlegen sie sich, was sie genau in der `Deque` speichern. Evtl. ist es sinnvoll, eine "Datenklasse" zu erstellen, die zusätzliche Informationen zu einem Knoten enthält.

#### Codevorgabe:

TreeInterface.java
``` Java
package iterators;

import java.util.List;

public interface TreeInterface<E> extends Iterable<E> {

    /**
     * @return the node's content
     */
    public E getContent();

    /**
     * @return a list of the node's children
     */
    public List<TreeInterface<E>> getChildren();

    /**
     * Adds a node to this node's children.
     *
     * @param child the node to add
     */
    public void addChild(TreeInterface<E> child);

    /**
     * Creates a node from a value and adds it to this node's children.
     * This function returns this node to make chaining possible.
     *
     * @param child the value to create the node from
     * @return this node
     */
    public TreeInterface<E> addChild(E child);
}
```

Skelett von IDDFSIterator.java. Vervollständigen sie dieses.
``` Java
package iterators;

public class IDDFSIterator<E> implements Iterator<E> {

  public IDDFSIterator(TreeInterface<E> tree) {
  }

  @Override
  public boolean hasNext() {
  }

  @Override
  public E next() {
  }
}
```

Tree.java
``` Java
package iterators;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Tree<E> implements TreeInterface<E> {
    private final E content;
    private List<TreeInterface<E>> children = new LinkedList<>();

    public Tree(E content) {
        this.content = content;
    }

    public E getContent() {
        return content;
    }

    @Override
    public List<TreeInterface<E>> getChildren() {
        return children;
    }

    @Override
    public void addChild(TreeInterface<E> child) {
        children.add(child);
    }

    @Override
    public TreeInterface<E> addChild(E child) {
        TreeInterface<E> tree = new Tree<>(child);
        addChild(tree);
        return this;
    }

    @Override
    public Iterator<E> iterator() {
        return new IDDFSIterator<E>(this);
    }
}
```
