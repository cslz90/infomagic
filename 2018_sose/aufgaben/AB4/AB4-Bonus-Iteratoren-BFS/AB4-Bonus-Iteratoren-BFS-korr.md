# Bewertungsvorschläge

Wichtig: Entgegen der Aufgabenstellung gibt es keine Textabgabe!

## Positive Faktoren:

  - Dozentron nimmt die Eingabe an.
  - Unit-Tests werden bestanden.
  - BFS ist im Code erkennbar.
  - Die Vorgaben wurden eingehalten.

## Weiche Kriterien:

  - Da es sehr viele Vorgaben waren, würde ich ein Auge zu drücken, falls ein Student nicht alle perfekt eingehalten hat. Wichtig ist dabei eigentlich nur, dass der Student nicht direkt am Anfang den gesamten Baum in eine Queue o.ä. packt und danach nur noch draus liest.

## Unit-Tests

  - Wenn nur die sichtbaren Tests bestanden werden, sollte ein Student noch gerade so eben bestehen können. Dann aber nur, wenn er alle formalen Vorgaben erfüllt.
