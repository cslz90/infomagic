# Der Wettkampf der Iteratoren - Der Angriff des Breitenhörnchens

*__Was bisher geschah ...__*

*Das alljährliche Frühlingsfest von Ja'va im Wald der tausend Speichereichen ist in vollem Gange. Drei Großrömische Iteratoren wollen um die Hand die Hand der Cousine des Kaisers anhalten. Die Katzenprinzessin Justina die Quantenverschränkende verlangt von jedem Teilnehmer, ihr auf möglichst beeindruckende Art eine Eulersche Knotenfrucht von einer besonders hohen Speichereiche zu pflücken.*

*Als Erster Herausforderer hat sich Gilbert der Tiefsinnige an die Aufgabe gewagt. Seine Tiefentaube hat sich bis in die höchsten Wipfel gewagt und eine Frucht gefunden, die Gilbert der Prinzessin überreichen konnte.*

### Einleitung

*Der zweite Streiter ist Igor der Breite. Er bringt ein speziell dressiertes Breitenhörnchen an den Start. Dieses ist besonders gut darin, zwischen Ästen hin und her zu springen. Es beginnt an der Wurzel, springt dann auf die erste Ebene von Astgabelungen. Dort bewegt es sich auf der gleichen Ebene von Astgabelung zu Astgabelung, bis es keine weiteren gibt. Erst dann springt es auf die nächste Ebene. Auf jeder Gabelung hält Das Breitenhörnchen inne, um auf Befehle von ihrem Herrchen zu warten. Wie auch die Tiefentaube beherrscht es die Befehle Amplius? und Proxime!*

*Das Tier kann dem Iterator durch eine antrainierte Folge der für Breitenhörnchen üblichen Knacklaute mitteilen, welche Knotenfrucht sich an der aktuellen Astgabelung befindet. Wenn es sich nicht um die gewünschte Eulersche Knotenfrucht handelt, gibt der Iterator den Befehl weiter zu springen.*

### Aufgabe 1

*Entwickeln sie die Zauber, mit der das Breitenhörnchen dressiert wurde. Diese sind:*
- *Amplius?* (`public boolean hasNext()`)  
- *Proxime!* (`public E next()`)
*Die beiden Zauber sind Teil der Rolle des Iterators.*

*Achten sie darauf, dass das Breitenhörnchen seinem Namen alle Ehre macht und dass es auf jeder Astgabelung Halt macht, um auf Befehle zu warten.*

### Aufgabe 2

*Überlegen sie sich, warum der Einsatz eines auf diese Art dressierten Breitenhörnchens besonders beeidruckend sein könnte, vor allem im Vergleich zum einsatz einer Tiefentaube.*

#### Formalia:

- Schreiben sie eine Klasse `public class BFSIterator<E> implements Iterator<E>`, die einen BFS-Iterator implementiert.
- Der Iterator soll über einen Baum iterieren, der das Interface `TreeInterface<E>` implementiert.    
  - Sowohl das Interface, als auch eine implementierende Klasse `Tree<E>` finden sie in den Codevorgaben. Ihr Iterator sollte nur die Methoden des Interfaces benutzen (das wird in den Tests geprüft).
  - Dort finden sie außerdem ein "Skelett" für die Iteratorklasse (`BFSIterator`). Nutzen sie dieses.
- - Innerhalb des Iterators dürfen sie zur Zwischenspeicherung von Knoten nur eine `Queue<E>`
  - Sie dürfen nur die Methoden `offer(e)`, `poll()` und `peek()` der `Queue` verwenden.
  - Speichern sie bei der Erstellung des Iterators nicht alle Knoten in der `Queue`. Ihr Iterator muss bei jedem Aufruf von `next()` schrittweise vorgehen.

#### Hinweise:

  >- Überlegen sie sich, was sie genau in der `Queue` speichern. Evtl. ist es sinnvoll, eine "Datenklasse" zu erstellen, die zusätzliche Informationen zu einem Knoten enthält.

#### Codevorgabe:

TreeInterface.java
``` Java
package iterators;

import java.util.List;

public interface TreeInterface<E> extends Iterable<E> {

    /**
     * @return the node's content
     */
    public E getContent();

    /**
     * @return a list of the node's children
     */
    public List<TreeInterface<E>> getChildren();

    /**
     * Adds a node to this node's children.
     *
     * @param child the node to add
     */
    public void addChild(TreeInterface<E> child);

    /**
     * Creates a node from a value and adds it to this node's children.
     * This function returns this node to make chaining possible.
     *
     * @param child the value to create the node from
     * @return this node
     */
    public TreeInterface<E> addChild(E child);
}
```

Skelett von BFSIterator.java. Vervollständigen sie dieses.
``` Java
package iterators;

public class BFSIterator<E> implements Iterator<E> {

    public BFSIterator(TreeInterface<E> tree) {
    }

    @Override
    public boolean hasNext() {
    }

    @Override
    public E next() {
    }
}
```

Tree.java
``` Java
package iterators;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Tree<E> implements TreeInterface<E> {
    private final E content;
    private List<TreeInterface<E>> children = new LinkedList<>();

    public Tree(E content) {
        this.content = content;
    }

    public E getContent() {
        return content;
    }

    @Override
    public List<TreeInterface<E>> getChildren() {
        return children;
    }

    @Override
    public void addChild(TreeInterface<E> child) {
        children.add(child);
    }

    @Override
    public TreeInterface<E> addChild(E child) {
        TreeInterface<E> tree = new Tree<>(child);
        addChild(tree);
        return this;
    }

    @Override
    public Iterator<E> iterator() {
        return new BFSIterator<E>(this);
    }
}
```
