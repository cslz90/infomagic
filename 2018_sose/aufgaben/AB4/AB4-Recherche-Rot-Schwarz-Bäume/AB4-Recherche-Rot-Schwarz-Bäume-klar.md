## Kommunistenkirschen

[//]: # (Recherche: 20:14 - 20:36)
[//]: # (Schreiben: 20:45 - 21:02)
[//]: # (=> insges. 18 + 17 = 35 min => 2h für Studis)

### Vorwort

Diese Aufgabe ist eine Rechercheaufgabe wie "Wilde Stammbaumwürmer" aus Aufgabenblatt 0 und folgt daher den gleichen Regeln.

### Aufgabe 1.1

Recherchieren Sie den Aufbau von Rot-Schwarz-Bäumen. Beantworten Sie dabei in maximal 400 Worten Fließtext (mit vollständigen Sätzen ohne Stichpunkte!) die folgenden Fragen:

* Was sind die 5 Regeln, die einen Rot-Schwarz-Baum definieren?
* Welche Klasse aus der Java-API baut auf einem Rot-Schwarz-Baum auf?
* Welchen Vorteil bringt ein Rot-Schwarz-Baum gegenüber einer naiven Implementierung eines binären Suchbaums?
* Welche Eigenschaft muss ein Rot-Schwarz-Baum haben, der nur aus schwarzen Knoten besteht?
* Was ist die maximale Tiefe eines Rot-Schwarz-Baums mit n Knoten? Wie viele rote und wie viele schwarze Knoten hat der Pfad von der Wurzel zu einem Blatt auf der tiefsten Ebene?

**Hinweise:**

* Bitte achten Sie auf eine korrekte Rechtschreibung und Grammatik, auch diese fließt in die Bewertung ein.
* So spaßig die Gamifizierung auch ist, antworten Sie hier aus Gnade mit den Tutoren bitte in Klartext.

### Aufgabe 1.2

Geben Sie die Quellen an, die Sie für Ihre Recherche verwendet haben. Hier reicht jeweils ein Link auf die Webseite oder der Titel des Buches, das Sie verwendet haben.
Es geht hier nicht darum herauszufinden, ob sie Formulierungen kopiert haben (was Sie natürlich nicht tun sollten), sondern darum, dass Sie zeigen, dass Sie in der Lage sind, geeignete Quellen zu finden und auszuwählen.

Bitte geben Sie auch nicht *alle* Quellen an, die Sie gelesen haben, sondern nur diejenigen, auf deren Inhalt Sie sich in Ihrem Text stützen.
Es sind maximal fünf Quellen erlaubt.

Quellen:

* T.H. Cormen, C.E. Leiserson, R.L. Rivest und C. Stein, *Introduction to Algorithms*, 3. Ausgabe. Cambrige, MA: MIT Press, 2009.
* https://docs.oracle.com/javase/10/docs/api/java/util/TreeMap.html
  * Interessante Anekdote: "red black tree java" hat als Suchbegriff nicht funktioniert, "red black tree java API" aber schon. 😉
* https://stackoverflow.com/questions/6406658/is-a-tree-with-all-black-nodes-a-red-black-tree ( nur um meine eigene Antwort zu Frage 4 kurz zu bestätigen )