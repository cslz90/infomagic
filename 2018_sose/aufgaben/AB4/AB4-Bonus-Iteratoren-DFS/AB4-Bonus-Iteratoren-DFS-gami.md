# Der Wettkampf der Iteratoren - Höhenflug der Tiefentaube

### Einleitung

*Es findet das alljährliche Frühlingsfest von Ja'va im Wald der tausend Speichereichen statt. Dieses Jahr veranstaltet der Großrömische Kaiser im Rahmen der Feierlichkeiten ein Turnier. Dem Sieger ist es gestattet, um die Hand der Cousine des Kaisers, Prinzessin Justina die Quantenverschränkende, anzuhalten. Im finalen Wettstreit geht es darum, die Prinzessin zu beeindrucken. Sie ist auch bekannt als die Katzenprinzessin, da sie in Tiere aller Art, insbesondere Katzen, vernarrt ist. Aus diesem Grund haben die Finalisten auf magische Weise dressierte Tiere zum Wettkampf mitgebracht.*

*Prinzessin Justina hat beschlossen, dass die Bewerber ihr eine besondere Frucht von der höchsten Speichereiche des Waldes pflücken sollen. Die Eulersche Knotenfrucht wächst dort nur alle 2,71828 Jahrhunderte in kleinen Mengen. Aufgrund der Höhe des Baumes ist die Frucht vom Boden aus nicht einsehbar. Außerdem hat sich das Astwerk dieser Speichereiche durch  die Jahrtausende über hunderte Meter weit ausgedehnt, sodass die Suche eine echte Herausforderung ist.*

*Nur ein Großrömischer Iterator darf an diesem Turnier teilnehmen. Deswegen haben alle Teilnehmer ihre Tiere so dressiert, dass sie auf die Befehle "Amplius" und "Proxime" hören.*

*Der erste Streiter ist Gilbert der Tiefsinnige. Seine Tiefentaube ist weithin dafür bekannt, die Äste eines jeden Baumes besonders schnell in der Höhe absuchen zu können.
Die Taube beginnt stets an der Wurzel des Baumes und fliegt auf Befehl ihres Meisters einen Ast bis zu seiner nächsten Gabelung hinauf. Dort angekommen erteilt der Iterator ihr den Befehl, ihm durch antrainierte Gurrlaute mitzuteilen, welche Frucht an dieser Stelle wächst. Handelt es sich nicht um die Eulersche Knotenfrucht, erhält sie den Befehl weiter zu suchen. Daraufhin fliegt die Taube zur nächst höheren Verzweigung und verharrt dort wieder. Ist sie am Ende eines Astes angekommen, fliegt sie zurück zur letzten Gabelung.*

### Aufgabe 1

*Entwickeln sie die Zauber, mit der die Tiefentaube dressiert wurde. Diese sind:*
- *Amplius?* (`public boolean hasNext()`)  
- *Proxime!* (`public E next()`)
*Die beiden Zauber sind Teil der Rolle des Iterators.*

*Achten sie darauf, dass die Tiefentaube ihrem Namen alle Ehre macht und dass sie auf jeder Astgabelung Halt macht, um auf Befehle zu warten.*

[Abgabe Aufgabe 1 in Dozentron](https://dozentron.mni.thm.de/submission_tasks/58)

### Aufgabe 2

*Überlegen sie sich, warum der Einsatz einer auf diese Art dressierten Tiefentaube besonders beeindruckend sein könnte, vor allem im Vergleich zum Einsatz eines Breitenhörnchens.*

#### Formalia:

- Schreiben sie eine Klasse `public class DFSIterator<E> implements Iterator<E>`, die einen DFS-Iterator implementiert.
- Der Iterator soll über einen Baum iterieren, der das Interface `TreeInterface<E>` implementiert.    
  - Sowohl das Interface, als auch eine implementierende Klasse `Tree<E>` finden sie in den Codevorgaben. Ihr Iterator sollte nur die Methoden des Interfaces benutzen (das wird in den Tests geprüft).
  - Dort finden sie außerdem ein "Skelett" für die Iteratorklasse (`DFSIterator`). Nutzen sie dieses.
- Innerhalb des Iterators dürfen sie zur Zwischenspeicherung von Knoten nur eine `Deque<E>` und eine Referenz auf einen Kindindex benutzen.
  - Außerdem dürfen sie von der `Deque` nur die Methoden `addFirst(e)`, `removeFirst()` und `peekFirst()` benutzen. Damit hat sie das Verhalten eines Stacks (die Klasse `Stack` darf nicht verwendet werden, da sie als veraltet gilt).
  - Speichern sie bei der Erstellung des Iterators nicht alle Knoten in der `Deque`. Ihr Iterator muss bei jedem Aufruf von `next()` schrittweise vorgehen.


#### Hinweise:

>- Überlegen sie sich, was sie genau in der `Deque` speichern. Evtl. ist es sinnvoll, eine "Datenklasse" zu erstellen, die zusätzliche Informationen zu einem Knoten enthält.

#### Codevorgabe:

TreeInterface.java
``` Java
package iterators;

import java.util.List;

public interface TreeInterface<E> extends Iterable<E> {

    /**
     * @return the node's content
     */
    public E getContent();

    /**
     * @return a list of the node's children
     */
    public List<TreeInterface<E>> getChildren();

    /**
     * Adds a node to this node's children.
     *
     * @param child the node to add
     */
    public void addChild(TreeInterface<E> child);

    /**
     * Creates a node from a value and adds it to this node's children.
     * This function returns this node to make chaining possible.
     *
     * @param child the value to create the node from
     * @return this node
     */
    public TreeInterface<E> addChild(E child);
}
```

Skelett von DFSIterator.java. Vervollständigen sie dieses.
``` Java
package iterators;

public class DFSIterator<E> implements Iterator<E> {

    public DFSIterator(TreeInterface<E> tree) {
    }

    @Override
    public boolean hasNext() {
    }

    @Override
    public E next() {
    }
}
```

Tree.java
``` Java
package iterators;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Tree<E> implements TreeInterface<E> {
    private final E content;
    private List<TreeInterface<E>> children = new LinkedList<>();

    public Tree(E content) {
        this.content = content;
    }

    public E getContent() {
        return content;
    }

    @Override
    public List<TreeInterface<E>> getChildren() {
        return children;
    }

    @Override
    public void addChild(TreeInterface<E> child) {
        children.add(child);
    }

    @Override
    public TreeInterface<E> addChild(E child) {
        TreeInterface<E> tree = new Tree<>(child);
        addChild(tree);
        return this;
    }

    @Override
    public Iterator<E> iterator() {
        return new DFSIterator<E>(this);
    }
}
```
