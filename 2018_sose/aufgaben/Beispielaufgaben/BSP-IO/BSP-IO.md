# Beispielaufgabe für Input/Output-Aufgaben

## Aufgabe

Schreiben Sie ein Programm in einer beliebigen Programmiersprache, das die Zahl in ihrem Input mit zehn multipliziert.

Beispiele

|Input|Output|
|---|---|
|64|640|
|1|10|
|13|130|

## Struktur der Abgabe

Geben Sie die Ausgabe des Programms hier als Lösung ab und laden Sie zusätzlich den Quellcode ihrer Lösung hoch.

* Die erwartete Ausgabe ist tatsächlich nur die Zahl, die als Ergebnis für Ihren Input herauskommen muss.
* Der Quellcode kann beliebig strukturiert sein.
  * Wenn Ihre Lösung nur aus einer Datei besteht, laden Sie einfach diese hoch.
  * Wenn Ihre Lösung aus mehreren Dateien besteht, verpacken Sie diese in ein Zip-Archiv und laden dieses hoch.

## Tests

Es gibt für diese Aufgabe drei Tests:

* `testCorrectLineCount` prüft nur ob Ihre Ausgabe auch wirklich nur aus einer Zeile besteht.
* `testIsNumber` prüft, ob es sich bei Ihrer Ausgabe wirklich um eine Zahl handelt.
* `testMultiplyByTen` prüft, ob die Zahl tatsächlich der mit zehn multiplizierte Input ist.

Eine generelle Anleitung für I/O-Aufgaben finden Sie auch (bald), wenn Sie auf den Button "Hilfe" neben dem Abgabebutton klicken.

## Musterlösung

Natürlich können Lösungen für diese Aufgabe sehr unterschiedlich aussehen.
Wir haben als Beispiel eine [Musterlösung in der Sprache Haskell](https://homepages.thm.de/~cslz90/kurse/ad18/static/io-solution.hs) für Sie bereitgesteltl.