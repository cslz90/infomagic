# Beispielaufgabe für Unittests

## Aufgabe

Schreiben Sie eine Klasse `HelloWorld` in einem Paket `hello`.
Wenn die Main-Methode dieser Klasse aufgerufen wird, soll die folgende Ausgabe auf der Konsole erscheinen:

```
Hello World
```

## Struktur der Abgabe

Verpacken Sie ihre Lösung als JAR-Archiv und laden Sie sie hier hoch.
Das Archiv sollte die folgende Struktur haben:

* 📂 `hello`
  * 📄 `HelloWorld.java`
  * 📄 `HelloWorld.class`

Eine Anleitung zum Erzeugen von JAR-Archiven finden Sie auch, wenn sie auf den Button "Hilfe" neben dem Abgabebutton klicken.

## Tests

Diese Aufgabe hat drei Tests, deren Ergebnis Ihnen bei ihrer Abgabe angezeigt wird:

* `testEmpty` wird immer bestanden.
* `testOutput` wird nur bestanden, wenn die Ausgabe tatsächlich exakt `Hello World` lautet.
    Wenn Sie alles richtig gemacht haben, sollte ihr Code diesen Test bestehen.
* `failNoMatterWhat` kann nicht bestanden werden.
    Der Test dient nur dazu, Ihnen zu zeigen, wie die Fehlermeldungen von Dozentron aussehen.

Zum lokalen Testen können Sie die Unittests auch mit dem Button "Download Tests" herunterladen.
In anderen Aufgaben kann es aber auch Unittests geben, die nicht zum Download angeboten werden (weil sie einen Teil der Lösung enthalten).

## Musterlösung

Wenn Sie Probleme mit dem Hochladen haben, können Sie sich die [Musterlösung](https://homepages.thm.de/~cslz90/kurse/ad18/static/BSP-Unittest-solution.jar) herunterladen und Sie mit ihrer Abgabe vergleichen (z.B. mit [7zip](https://www.7-zip.org/)).

*Hinweis*: Die Musterlösung enthält noch einige zusätzliche Dateien, die nicht zwingend in ihrer Lösung enthalten sein müssen.
Zum Beispiel sind dort auch die Unittests selbst noch einmal enthalten.
Wichtig sind nur die weiter oben genannten zwei Dateien `HelloWorld.java` und `HelloWorld.class`.