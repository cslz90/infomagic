package dijkstra;

import java.util.Objects;

public class FlyingIsland {

    private final int id;

    public FlyingIsland(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlyingIsland that = (FlyingIsland) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String toString() {
        return "("+id+")";
    }

}
