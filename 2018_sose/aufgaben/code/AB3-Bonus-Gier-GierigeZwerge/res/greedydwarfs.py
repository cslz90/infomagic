#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Author: Birger Schulze

import sys

def getNeighbourIndexes(dwarfList, index):
    lower = index - 1
    if lower < 0:
        lower = len(dwarfList) - 1
    higher = index + 1
    if higher >= len(dwarfList):
        higher = 0
    while dwarfList[lower] == None:
        lower -= 1
        if lower < 0:
            lower = len(dwarfList) - 1
    while dwarfList[higher] == None:
        higher += 1
        if higher > len(dwarfList) - 1:
            higher = 0
    return lower, higher
            
    

dwarfs = []
inFile=open(sys.argv[1], "r")
for line in inFile:
    dwarfs.append(int(line))
activeDwarves = len(dwarfs)
currentDwarfIndex = 1
#print(dwarfs)
while (activeDwarves > 1):
    currentDwarfIndex, bla = getNeighbourIndexes(dwarfs, currentDwarfIndex)
    #print("Active dwarf: {d}".format(d=currentDwarfIndex))
    lower, higher = getNeighbourIndexes(dwarfs, currentDwarfIndex)
    #print("Neighbours: {a} {b}".format(a=lower, b=higher))
    highest = -1
    if dwarfs[lower] >= dwarfs[higher]:
        highest = lower
    else:
        highest = higher
    #print("Highest neighbour: {i} {n}".format(i=highest, n=dwarfs[highest]))
    dwarfs[currentDwarfIndex] += dwarfs[highest]
    dwarfs[highest] = None
    activeDwarves -= 1
    #print(dwarfs)
#print(dwarfs)
print("{a};{b}".format(a=currentDwarfIndex, b=dwarfs[currentDwarfIndex]))
