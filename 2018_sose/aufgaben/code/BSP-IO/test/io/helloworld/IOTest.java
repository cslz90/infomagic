package io.helloworld;

import de.thm.mni.aud.commons.AbstractInputOutputTest;
import org.junit.Assert;
import org.junit.Test;

public class IOTest extends AbstractInputOutputTest {

    @Test
    public void testCorrectLineCount() {
        int nOut = studentOutputLines.size();
        if (1 != nOut) {
            Assert.fail("Die Ausgabe sollte genau eine Zeile haben. Sie haben "+nOut+" Zeilen angegeben.");
        }
    }

    @Test
    public void testIsNumber() {
        try {
            Integer.parseInt(studentOutputLines.get(0));
        } catch (NumberFormatException nfe) {
            Assert.fail("Die Ausgabe muss eine Zahl sein!");
        }
    }

    @Test
    public void testMultiplyByTen() {
        //the student should multiply the given number by 10
        int expectedNumber = Integer.parseInt(studentInputLines.get(0))*10;
        int actualNumber = Integer.parseInt(studentOutputLines.get(0));
        //System.out.println(expectedNumber);
        //System.out.println(actualNumber);
        if(actualNumber > expectedNumber)
            Assert.fail("the number should be smaller");
        else if (actualNumber < expectedNumber)
            Assert.fail("the number should be larger");
    }
}
