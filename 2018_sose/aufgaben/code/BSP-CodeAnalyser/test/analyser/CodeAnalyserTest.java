package analyser;

import de.thm.mni.aud.commons.CodeAnalyser;
import org.junit.Test;
import sourcecodetester.SourceCodeTester;
import sourcecodetester.enums.Modifier;
import sourcecodetester.enums.Visibility;
import sourcecodetester.parser.javaparser.tests.AllMethodsHaveJavadoc;
import sourcecodetester.parser.javaparser.tests.BlacklistClasses;
import sourcecodetester.parser.javaparser.tests.BlacklistMethods;
import sourcecodetester.parser.javaparser.tests.ClassNamesStartWithCapitalLetter;
import sourcecodetester.parser.javaparser.tests.ClassesAreImplemented;
import sourcecodetester.parser.javaparser.tests.LinesAreCorrectlyIndented;
import sourcecodetester.parser.javaparser.tests.MaxNumberOfStatementsInMethods;
import sourcecodetester.parser.javaparser.tests.MethodCallUsedInMethods;
import sourcecodetester.parser.javaparser.tests.MethodNamesStartWithLowerCaseLetter;
import sourcecodetester.parser.javaparser.tests.MethodsAreImplemented;
import sourcecodetester.parser.javaparser.tests.MethodsAreRecursive;
import sourcecodetester.parser.javaparser.tests.MethodsHaveJavadoc;
import sourcecodetester.parser.javaparser.tests.PublicMethodsHaveJavadoc;
import sourcecodetester.parser.javaparser.tests.VariableNamesStartWithLowerCaseLetter;
import sourcecodetester.parser.javaparser.tests.WhitelistClasses;
import sourcecodetester.parser.string.tests.ClosingBraceInOwnLine;
import sourcecodetester.parser.string.tests.MaxNumberOfCharactersInOneLine;
import sourcecodetester.parser.string.tests.OpeningBraceInSameLineWithStatement;
import sourcecodetester.parser.universal.tests.Or;
import sourcecodetester.util.Method;
import sourcecodetester.util.Parameter;

import java.util.EnumSet;

public class CodeAnalyserTest extends CodeAnalyser {

  @Override
  public void setupTests(SourceCodeTester sct) {
    // failing tests have been intentionally commented out so that these tests do not fail!

  	sct.createNewTestGroup(
  			"Class and Method Implementations",
  			"The following tests check for correct implementation of classes and their methods."
    )
      .addTests(
  			new ClassesAreImplemented(
  				"main.MyAwesomeClass",
  				"main.Main",
  				"myLowerCaseClassName",
  				"myGenericClass",
  				"ExtendsTest"
  			),
        new MethodsAreImplemented(
          "main.MyAwesomeClass.myNotRecursiveMethod()",
          "main.MyAwesomeClass.myRecursiveMethod()"
        ),
        new MethodsAreImplemented(
          new Method("main.Main.main()", Visibility.PUBLIC, "void", EnumSet.of(Modifier.STATIC), new Parameter("String[]", "args"))
        ),
        new MethodsAreImplemented(
          new Method("MyAwesomeClass.myRecursiveMethod()", Visibility.PUBLIC, "int", EnumSet.of(Modifier.STATIC, Modifier.FINAL), new Parameter("int", "number")),
          new Method("MyAwesomeClass.myNotRecursiveMethod()"),
          new Method("MyAwesomeClass.randomMethod()", Visibility.ANY, null, EnumSet.of(Modifier.ANY), (Parameter[]) null), // equivalent similar to just checking the method name
          new Method("MyAwesomeClass.maxWrong()", Visibility.PUBLIC, "E", EnumSet.of(Modifier.STATIC), new Parameter("Collection<E>", "collection"))
        )/*,
        new ClassesAreImplemented( // failing test
          "MyNotExistingClass"
          ),
        new ClassesAreImplemented( // failing test
          "MyNotExistingClassOne",
          "MyNotExistingClassTwo"
        )*/
  		);

  	sct.createNewTestGroup(
  			"Functionality",
  			"The following tests check for correct functionality."
    )
      .addTests(
        /*new MethodsAreRecursive(new Method("MyAwesomeClass.myNotRecursiveMethod()")), // failing test
        new MethodsAreRecursive(new Method("MyAwesomeClass.myNotExistingRecursiveMethod()")), // failing test
        new MethodsAreRecursive(new Method("MyAwesomeClass.myNotRecursiveMethod()"), new Method("MyAwesomeClass.myNotExistingRecursiveMethod()")), // failing test
        new MethodsAreRecursive(new Method("MyAwesomeClass.myRecursiveMethod()", Visibility.PUBLIC, "int", EnumSet.of(Modifier.STATIC, Modifier.FINAL), new Parameter("int"), new Parameter("int"))), // failing test
        new MethodCallUsedInMethods(
            new Method("java.lang.Comparable.compareTo()", Visibility.PUBLIC, "int", EnumSet.of(Modifier.ANY), new Parameter("T")),
            new Method("MyAwesomeClass.min()"),
            new Method("MyAwesomeClass.max()")
          ),
        new MethodCallUsedInMethods( // failing test
          new Method("MyAwesomeClass.compareToFailing()"),
          new Method("min()"),
          new Method("max()"),
          new Method("median()")
          ),*/
        new MethodsAreRecursive(new Method("main.MyAwesomeClass.myRecursiveMethod()"))
  		);

  	/*
  	sct.createNewTestGroup(
  			"Limitations",
  			"The following tests check the usage of specific classes and methods."
    )
  		.addTests(
        new BlacklistClasses(  // failing test
          java.util.Collections.class.getName(),
          java.util.ArrayList.class.getName()
        ),
        new WhitelistClasses(  // failing test
          java.util.Collection.class.getName(),
          java.util.Collections.class.getName()
        ),
        new BlacklistMethods(  // failing test
          new Method("java.lang.Math.random()"),
          new Method("java.lang.Math.max()"),
          new Method("java.lang.Math.min()"),
          new Method("java.util.Collections.max()"),
          new Method("java.util.Collections.min()")
        )
  		);
  		*/

  	/*
  	sct.createNewTestGroup(
  			"Java Documentation",
  			"The following tests check the Javadoc of methods."
  			)
  		.addTests(
			  new AllMethodsHaveJavadoc(), // failing test
        new PublicMethodsHaveJavadoc(false, false, false), // failing test
  			new MethodsHaveJavadoc( // failing test
  				new Method("myNotRecursiveMethod"),
  				new Method("main.MyAwesomeClass.myExceptionThrowingMethod", Visibility.PACKAGE, "void", new Parameter("boolean", "isTrue"))
  			)
  		);
    */
  	
  	/*
  	sct.createNewTestGroup(
  			"Code Quality",
  			"The following tests check the overall code quality."
    )
  		.addTests(
  			new ClassNamesStartWithCapitalLetter(), // failing test
  			new MethodNamesStartWithLowerCaseLetter(), // failing test
  			new VariableNamesStartWithLowerCaseLetter(), // failing test
        new LinesAreCorrectlyIndented(4), // failing test
        new ClosingBraceInOwnLine(), // failing test
        new OpeningBraceInSameLineWithStatement(), // failing test
//        new VariableDeclarationsHaveGenericParameter(), // failing test
        new MaxNumberOfCharactersInOneLine(60), // failing test
    		new MaxNumberOfStatementsInMethods(3, true, new Method("MyAwesomeClass.max()")), // failing test
        new MaxNumberOfStatementsInMethods(8, new Method("MyAwesomeClass.max()")) // failing test
  		);
  		*/

  	/*
  	sct.createNewTestGroup(
  			"Special Tests"
  	)
  		.addTests(
  			new Or( // failing test
    			new MethodCallUsedInMethods(
            new Method("MyAwesomeClass.funnyMethod()", Visibility.PUBLIC, "int", new Parameter("T")),
            new Method("min()"),
            new Method("max()")
        	),
				  new MethodsAreImplemented("MyAwesomeClass.myNotExistingRecursiveMethod()")
			  )
  		);
  		*/

  }


  @Test
  public void implementations() {
    runTests("Class and Method Implementations");
  }

  @Test
  public void functionality() {
    runTests("Functionality");
  }

  
//  @Test
//  public void limitations() {
//    runTests("Limitations");
//  }

//  @Test
//  public void javaDocumentation() {
//    runTests("Java Documentation");
//  }
  
//  @Test
//  public void codeQuality() {
//    runTests("Code Quality");
//  }
  
//  @Test
//  public void specialTests() {
//    runTests("Special Tests");
//  }


}
