package orkgrunzer;

import static org.junit.Assert.*;

import de.thm.mni.aud.commons.AbstractFileIOTest;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class OrkGrunzerTest extends AbstractFileIOTest {

    public OrkGrunzerTest() {
        super("morse-code.txt");
    }

    @Test
    public void encodeTest() throws Exception {
        final String TEST_INPUT1 = "The quick brown fox jumps over the lazy dog 0987654321-.,";
        final String TEST_OUTPUT1 = "-..... --.-..-..-.-.-.- -....-.---.---. ..-.----..- .---..---.--.... ---...-..-. -..... .-...---..-.-- -..-----. ---------.---..--...-.............-...--..---.-----....-.-.-.---..--";
        assertEquals(TEST_OUTPUT1, OrkGrunzer.encode(TEST_INPUT1));
    }

}
