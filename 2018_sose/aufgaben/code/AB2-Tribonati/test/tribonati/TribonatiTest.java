package tribonati;

import static org.junit.Assert.*;
import org.junit.Test;

public class TribonatiTest {

  @Test
  public void tribTest() {
    assertEquals(0, Tribonati.tribDyn(0, new int[1]));
    assertEquals(1, Tribonati.tribDyn(1, new int[2]));
    assertEquals(1, Tribonati.tribDyn(2, new int[3]));
    assertEquals(2, Tribonati.tribDyn(3, new int[4]));
    assertEquals(24, Tribonati.tribDyn(7, new int[8]));
    assertEquals(197466473, Tribonati.tribDyn(50, new int[51]));
  }

  @Test(timeout = 100)
  public void dynamicProgrammingTest() {
    Tribonati.tribDyn(5000, new int[5001]);
  }



}
