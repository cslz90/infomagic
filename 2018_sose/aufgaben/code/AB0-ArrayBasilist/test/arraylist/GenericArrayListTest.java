package arraylist;

import de.thm.mni.aud.commons.CodeAnalyser;
import org.junit.Test;
import static org.junit.Assert.*;


import java.util.EnumSet;
import sourcecodetester.SourceCodeTester;
import sourcecodetester.enums.Modifier;
import sourcecodetester.enums.Visibility;
import sourcecodetester.parser.javaparser.tests.*;
import sourcecodetester.parser.string.tests.*;
import sourcecodetester.parser.universal.tests.*;
import sourcecodetester.util.Method;
import sourcecodetester.util.Parameter;
import sourcecodetester.util.TestGroup;

public class GenericArrayListTest extends CodeAnalyser {
	

  @Override
  public void setupTests(SourceCodeTester sct) {

    sct.createNewTestGroup("Class Implementations")
      .addTests(
        new ClassesAreImplemented("arraylist.GenericArrayList"),
        new MethodsAreImplemented(
          new Method("arraylist.GenericArrayList.get()"),
          new Method("arraylist.GenericArrayList.set()"),
          new Method("arraylist.GenericArrayList.size()"),
          new Method("arraylist.GenericArrayList.add()"),
          new Method("arraylist.GenericArrayList.remove()"),
          new Method("arraylist.GenericArrayList.insert()")
        ),
        new MethodsAreImplemented(
          new Method("arraylist.GenericArrayList.get()", Visibility.ANY, "E", EnumSet.of(Modifier.NONE), new Parameter("int", "idx")),
          new Method("arraylist.GenericArrayList.set()", Visibility.ANY, "void", EnumSet.of(Modifier.NONE), new Parameter("E", "el"), new Parameter("int", "idx")),
          new Method("arraylist.GenericArrayList.size()", Visibility.ANY, "int", EnumSet.of(Modifier.NONE), NO_PARAMS),
          new Method("arraylist.GenericArrayList.add()", Visibility.ANY, "void", EnumSet.of(Modifier.NONE), new Parameter("E", "el")),
          new Method("arraylist.GenericArrayList.remove()", Visibility.ANY, "void", EnumSet.of(Modifier.NONE), new Parameter("int", "idx")),
          new Method("arraylist.GenericArrayList.insert()", Visibility.ANY, "void", EnumSet.of(Modifier.NONE), new Parameter("E", "el"), new Parameter("int", "idx"))
        ).setName("Method Implementations (exact)")
      )
    ;

    sct.createNewTestGroup("Code Quality")
      .addTests(
        new ClosingBraceInOwnLine(),
        new OpeningBraceInSameLineWithStatement(),
        new LinesAreCorrectlyIndented(8),
        new MaxNumberOfCharactersInOneLine(60)
      )
    ;
  }
  


  @Test
  public void codequality() {
    runTests("Code Quality");
  }

  @Test
  public void classImplementations() {
    runTests("Class Implementations");
  }

  



	
  @Test
  public void sizeTest() {
    GenericList<Integer> xs = new GenericArrayList<>(5);
    for(int i=5;i<10; i++)
      xs.add(i);
    
    assertEquals(5, xs.size());
  }
  
  @Test
  public void sizeTest2() {
	  GenericList<Integer> xs = new GenericArrayList<>(5);
	  assertEquals(0, xs.size());
  }
  
  @Test
  public void sizeTest3() {
	  GenericList<Integer> xs = new GenericArrayList<>(5);
	  xs.add(2);
	  xs.insert(5,0);
	  assertEquals(2, xs.size());
  }
  
  @Test
  public void getTest() {
    GenericList<Integer> xs = new GenericArrayList<>(5);
    for(int i=5;i<10; i++)
      xs.add(i);
    
    for(int i=0; i<5; i++)
      assertEquals(i+5, (int) xs.get(i));
  }
  
  @Test
  public void increaseCapacityTest() {
    GenericList<Integer> xs = new GenericArrayList<>(5);
    for(int i=5;i<10; i++)
      xs.add(i);
    
    xs.add(10);
    xs.add(20);
    assertTrue("capacity should get increased", xs.size() >= 7);
    assertEquals(20, (int) xs.get(6));
    assertEquals(10, (int) xs.get(5));
  }
  
  @Test
  public void removeTest() {
    GenericList<Integer> xs = new GenericArrayList<>(5);
    for(int i=5;i<10; i++)
      xs.add(i);
    
    xs.remove(4);
    xs.remove(2);
    assertEquals(3, xs.size());
    assertEquals(8, (int) xs.get(2));
    assertEquals(5, (int) xs.get(0));
  }
  
  @Test
  public void insertTest() {
    GenericList<Integer> xs = new GenericArrayList<>(5);
    for(int i=5;i<10; i++)
      xs.add(i);
    
    assertEquals(3+5, (int) xs.get(3));
    xs.insert(20, 3);
    assertEquals(20, (int) xs.get(3));
    assertEquals(3+5, (int) xs.get(4));
  }
}
