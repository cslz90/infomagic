package changemaking;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CoinChangeSolver {
    public static class Counter {
        long count = 0;
        public void inc() {
            count++;
        }
    }
    public static long[] weights27(long target) {
        int i = 0;
        long w = 2;
        long pot = 1;
        List<Long> lst = new ArrayList<Long>();
        while (w <= target) {
            lst.add(w);
            i += 1;
            if (i % 2 == 0) pot *= 10;
            w = i % 2 == 0 ? 2 * pot : 7 * pot;
        }
        Collections.reverse(lst);
        return lst.stream().mapToLong(x -> x).toArray();
    }
    public static long[] solveGreedy(long targetValue, long[] weights, int startIdx) {
        List<Long> lst = new ArrayList<Long>();
        long remaining = targetValue;
        int i = startIdx;
        if (targetValue >= weights[i] * 6) return null;
        while(remaining > 0) {
            if(remaining < weights[i]) i++;
            if(i >= weights.length) break;
            lst.add(weights[i]);
            remaining -= weights[i];
        }
        return remaining == 0 ? lst.stream().mapToLong(x->x).toArray() : null;
    }

    public static long[] solve(long targetValue) {
        return solve(targetValue, new Counter());
    }
    public static long[] solve(long targetValue, Counter c) {
        return solve(targetValue, weights27(targetValue), 0, Integer.MAX_VALUE, 0, c).stream().mapToLong(x -> x).toArray();
    }
    public static List<Long> solve(long targetValue, long[] weights, int largestIdx, int bestFullCount, int currentCount, Counter c) {
        c.inc();
        if (targetValue == 0) return new ArrayList<Long>();
        if (targetValue < 0) return null;
        if (currentCount >= bestFullCount) return null;
        if (largestIdx >= weights.length) return null;
        //long[] greedy = solveGreedy(targetValue, weights, largestIdx);
        //if (greedy != null) return Arrays.stream(greedy).boxed().collect(Collectors.toList());
        List<Long> best = null;
        long w = weights[largestIdx];
        long times = targetValue / w;
        // if we use 7 x 2, we could have used 2 x 7
        // if we use 7 x 7, we could have used 1 x 2 + 1 x 7 + 2 x 20
        if (times > 6) return null; // TODO is this really OK?
        for(int t = (int)times; t >= 0; t--) {
            List<Long> res = solve(targetValue - t * w, weights, largestIdx + 1, bestFullCount, currentCount + t, c);
            if (res != null && res.size() + t + currentCount < bestFullCount) {
                for(int i = 0; i < t; i++) res.add(w);
                best = res;
                bestFullCount = res.size() + currentCount;
            }
        }
        return best;
    }

    private static void benchmark() {
        Counter c = new Counter();
        long t = System.nanoTime();
        System.out.println(Arrays.toString(solve(6903688607814215303l, c)));
        System.out.println(System.nanoTime() - t);
        System.out.println(c.count);
    }

    private static void writeOutput() throws IOException {
        Files.write(
                Paths.get("res/exampleOutput.txt"),
                Files.lines(Paths.get("res/exampleInput.txt"))
                        .map(Long::parseLong)
                        .map(x -> solve(x))
                        .map(x -> Arrays.stream(x).mapToObj(y -> Long.toString(y)).collect(Collectors.joining(",")))
                        .collect(Collectors.toList())
        );
    }

    public static void main(String[] args) throws IOException {
        benchmark();
        //writeOutput();
    }
}
