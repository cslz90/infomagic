package changemaking;

import static changemaking.CoinChangeGenerator.generateShuffledLongs;

public class Level2Generator {
    public static void main(String[] args) {
        generateShuffledLongs(1000, 1000, 107);
    }
}
