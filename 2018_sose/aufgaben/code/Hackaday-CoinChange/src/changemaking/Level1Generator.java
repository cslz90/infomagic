package changemaking;

import static changemaking.CoinChangeGenerator.generateShuffledLongs;

public class Level1Generator {
    public static void main(String[] args) {
        generateShuffledLongs(1000, 7, 1);
    }
}
