package changemaking;

import static changemaking.CoinChangeGenerator.generateShuffledLongs;

public class Level5Generator {
    public static void main(String[] args) {
        generateShuffledLongs(100, 1_000_000_000, 122_949_829);
    }
}
