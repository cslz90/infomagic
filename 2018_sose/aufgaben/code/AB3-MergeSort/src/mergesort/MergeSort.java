package mergesort;

public class MergeSort {

	private static int recursionCounter;
	
    public static void sort(int[] arr) {
        sort(arr, 0, arr.length);
    }

    // fromIndex inclusive, toIndex exclusive
    private static void sort(int[] arr, int fromIndex, int toIndex) {
    	increaseRecursionCounter();
        if (fromIndex >= toIndex - 1) {
            return;
        }
        int middleIndex = (fromIndex + toIndex) / 2;
        sort(arr, fromIndex, middleIndex);
        sort(arr, middleIndex, toIndex);
        merge(arr, fromIndex, middleIndex, toIndex);
    }

    // first subarray: fromIndex inclusive, middleIndex exclusive
    // second subarray: middleIndex inclusive, toIndex exclusive
    static void merge(int[] arr, int fromIndex, int middleIndex, int toIndex) {
        int[] tempArr = new int[toIndex - fromIndex];
        for (int i = 0; i < tempArr.length; i++) {
            tempArr[i] = arr[fromIndex + i];
        }

        int tempLeftIndex = 0;
        int tempMiddleIndex = middleIndex - fromIndex;
        int tempRightIndex = tempMiddleIndex;

        for (int i = fromIndex; i < toIndex; i++) {
            if (tempRightIndex >= tempArr.length
                    || (tempLeftIndex < tempMiddleIndex && tempArr[tempLeftIndex] < tempArr[tempRightIndex])) {
                arr[i] = tempArr[tempLeftIndex];
                tempLeftIndex++;
            } else {
                arr[i] = tempArr[tempRightIndex];
                tempRightIndex++;
            }
        }
        
    }

    static int getRecursionCounter() {
    	return recursionCounter;
    }
    
    static void increaseRecursionCounter() {
    	recursionCounter++;
    }
    
    static void resetRecursionCounter() {
    	recursionCounter = 0;
    }
}