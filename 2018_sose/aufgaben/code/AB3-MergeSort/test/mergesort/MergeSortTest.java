package mergesort;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Random;

import org.junit.Test;

public class MergeSortTest {

    public static final int MIN_VALUE = -1000;
    public static final int MAX_VALUE = 1000;

    public static final int LONG_ARRAY_LENGTH = 1000;

    @Test
    public void arrayLength0Test() {
        testRandomArray(0);
    }

    @Test
    public void arrayLength1Test() {
        testRandomArray(1);
    }

    @Test
    public void arrayLongTest() {
        testRandomArray(LONG_ARRAY_LENGTH);
    }

    @Test
    public void mergeTest() {
        int[] array = new Random().ints(LONG_ARRAY_LENGTH, MIN_VALUE, MAX_VALUE).toArray();

        int fromIndex = array.length / 2;
        int toIndex = array.length;
        int middleIndex = (fromIndex + toIndex) / 2;

        int[] initialArray = Arrays.copyOf(array, array.length);
        int[] correctArray = Arrays.copyOf(array, array.length);
        Arrays.sort(correctArray, fromIndex, toIndex);
        Arrays.sort(array, fromIndex, middleIndex);
        Arrays.sort(array, middleIndex, toIndex);

        MergeSort.merge(array, fromIndex, middleIndex, toIndex);
        if (Arrays.equals(initialArray, array) && !Arrays.equals(initialArray, correctArray)) {
            fail("you did not change the array at all!");
        }

        //assertTrue("Merge function works incorrect", Arrays.equals(correctArray, array));
        assertArrayEquals("Merge function works incorrect", correctArray, array);
    }

    @Test
    public void recursionCountTest() {
    	MergeSort.resetRecursionCounter();
    	int[] array = new Random().ints(0, MIN_VALUE, MAX_VALUE).toArray();

        int[] correctArray = Arrays.copyOf(array, 0);
        Arrays.sort(correctArray);

        MergeSort.sort(array);
    	int counter = MergeSort.getRecursionCounter();
    	assertTrue("Recursive counting failed!", counter == 1);
    }
    
    @Test
    public void recursionCountTest2() {
    	MergeSort.resetRecursionCounter();
    	int[] array = new Random().ints(1, MIN_VALUE, MAX_VALUE).toArray();

        int[] correctArray = Arrays.copyOf(array, 1);
        Arrays.sort(correctArray);

        MergeSort.sort(array);
    	int counter = MergeSort.getRecursionCounter();
    	assertTrue("Recursive counting failed!", counter == 1);
    }
    
    @Test
    public void recursionCountTest3() {
    	MergeSort.resetRecursionCounter();
    	int[] array = new Random().ints(2, MIN_VALUE, MAX_VALUE).toArray();

        int[] correctArray = Arrays.copyOf(array, 2);
        Arrays.sort(correctArray);

        MergeSort.sort(array);
    	int counter = MergeSort.getRecursionCounter();
    	assertTrue("Recursive counting failed!", counter == 3);
    }
    
    @Test
    public void recursionCountTest4() {
    	MergeSort.resetRecursionCounter();
    	int[] array = new Random().ints(LONG_ARRAY_LENGTH, MIN_VALUE, MAX_VALUE).toArray();

        int[] correctArray = Arrays.copyOf(array, LONG_ARRAY_LENGTH);
        Arrays.sort(correctArray);

        MergeSort.sort(array);
    	int counter = MergeSort.getRecursionCounter();
    	assertTrue("Recursive counting failed!", counter == 1999);
    }
    

    private void testRandomArray(int length) {
        int[] array = new Random().ints(length, MIN_VALUE, MAX_VALUE).toArray();

        int[] correctArray = Arrays.copyOf(array, length);
        Arrays.sort(correctArray);

        MergeSort.sort(array);

        assertTrue("An array of size " + length + " was sorted incorrect", Arrays.equals(correctArray, array));
    }

}