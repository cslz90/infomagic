package de.thm.mni.aud.commons;

import static org.junit.Assert.*;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;
import java.io.IOException;
import org.junit.*;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.*;

public abstract class AbstractInputOutputTest {

    public static final String STUDENT_INPUT_PROPERTY = "input";
    public static final String STUDENT_OUTPUT_PROPERTY = "student-output";

    protected List<String> studentInputLines;
    protected List<String> studentOutputLines;

    @Before
    public void prepare() throws IOException {
        String studentInputFile = System.getProperty(STUDENT_INPUT_PROPERTY);
        String studentOutputFile = System.getProperty(STUDENT_OUTPUT_PROPERTY);

        if (studentInputFile == null) { studentInputFile = "exampleInput.txt"; }
        if (studentOutputFile == null) { studentOutputFile = "exampleOutput.txt"; }
        studentInputLines = Files.readAllLines(
                Paths.get(studentInputFile),
                StandardCharsets.UTF_8
        );
        studentOutputLines = Files.readAllLines(
                Paths.get(studentOutputFile),
                StandardCharsets.UTF_8
        );
        assertThat(studentInputLines, is(not(empty())));
        assertThat(studentOutputLines, is(not(empty())));
    }
}
