package de.thm.mni.aud.commons;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.BeforeClass;
import jdk.jshell.JShell;
import jdk.jshell.Snippet;
import jdk.jshell.SnippetEvent;
import jdk.jshell.VarSnippet;
import jdk.jshell.MethodSnippet;
import jdk.jshell.JShellException;
import jdk.jshell.SourceCodeAnalysis;
import static org.junit.Assert.*;

public abstract class AbstractJShellTest {

  protected static String studentCode;
  protected JShell shell;
  protected List<SnippetEvent> events;


    public AbstractJShellTest() throws IOException {
        assertNotNull(System.getProperty("jshellInput"));
        studentCode = new String(
            Files.readAllBytes(Paths.get(System.getProperty("jshellInput"))),
            StandardCharsets.UTF_8
            );
    }

  public VarSnippet varByName(String name) {
    Optional<VarSnippet> opt = shell.variables().filter(x -> x.name().equals(name)).findFirst();
    if (!opt.isPresent()) fail("There is no variable named "+name);
    return opt.get();
  }

  public MethodSnippet methodByName(String name) {
    Optional<MethodSnippet> opt = shell.methods().filter(x -> x.name().equals(name)).findFirst();
    if (!opt.isPresent()) fail("There is no method named "+name);
    return opt.get();
  }

  public String expressionResult(String exp) {
    List<SnippetEvent> events = shell.eval(exp);
    return events.get(events.size()-1).value();
  }

  @Before
  public void createJShell() {
    shell = JShell.create();
    events = new ArrayList<SnippetEvent>();
    //JShell.Subscription s = shell.onSnippetEvent(events::add);
    SourceCodeAnalysis anal = shell.sourceCodeAnalysis();
    SourceCodeAnalysis.CompletionInfo info;
    String src = studentCode;
    do {
      info = anal.analyzeCompletion(src);
      List<SnippetEvent> snippetEvents = shell.eval(info.source());
      events.addAll(snippetEvents);
      src = info.remaining();
      //System.out.println(events.size());
    } while (src.length() > 0);
    //shell.unsubscribe(s);
  }

  public abstract void testJShellExceptions() throws JShellException;

  public void assertNoJShellExceptions() throws JShellException {
    Optional<SnippetEvent> exEvent = events.stream()
      .filter(x -> x.exception() != null)
      .limit(1)
      .findFirst();
    if (exEvent.isPresent()) throw exEvent.get().exception();
  }

  public abstract void testJShellRejected();

  public void assertNoSnippetRejected() {
    events.stream().filter(e -> e.status() == Snippet.Status.REJECTED)
      .map(e -> shell.diagnostics(e.snippet())).limit(1)
      .forEach(x -> fail(x.toString()));
  }

  @After
  public void destroyJShell() {
    shell.stop();
  }
}
