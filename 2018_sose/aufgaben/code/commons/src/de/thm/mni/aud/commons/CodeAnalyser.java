package de.thm.mni.aud.commons;

import org.junit.Assert;

import sourcecodetester.SourceCodeTester;
import sourcecodetester.util.TestGroupResult;
import sourcecodetester.util.Parameter;

import java.io.File;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/** A TestSuite for running a 'SourceCodeTester'.
 * <p>
 * Implement your tests in the 'setupTests' method by adding test groups and test to the SourceCodeTester object.
 * <p>
 * Use the method 'runTests(String)' in your @Test methods to run all the tests of a specific test group. If at least one test failed, a CodeAnalyserException will be thrown containing the feedback text. Assertions are thrown when the student jar name is not provided or the jar file can not be found/accessed.
 * <p>
 * The student jar name is provided via the system property 'student-jar'.
 */
public abstract class CodeAnalyser {

  public final Parameter[] NO_PARAMS = {};
  public boolean showCodeSnippets = true;

  private String jarPath;
  private SourceCodeTester sct = null;
  private Map<String, String> results = null;

  /**
   * Using this constructor, the path/name to the jar file will be obtain through the 'student-jar' (or 'user.dir') property.
   */
  public CodeAnalyser() {
    jarPath = System.getProperty("student-jar");

    if (jarPath == null) {
      jarPath = System.getProperty("user.dir") + File.separator + "src";
      System.out.println("[INFO]: manually setting student-jar property to: "+jarPath);
    }
  }

  /**
   * Using this constructor, the path/name to the jar file can be set manually.
   */
  public CodeAnalyser(String jarPath) {
    this.jarPath = jarPath;
  }


  /**
   * The tests and test groups are supposed to be added to the provided SourceCodeTester object.
   * The names of the test groups should be unique.
   */
  public abstract void setupTests(SourceCodeTester sct);


  /**
   * Runs the tests of the test group with the provided name.
   * Throws an CodeAnalyserException exception, if at least one of the tests were not passed.
   * @param testGroupName The name of the test group which tests should be run.
   */
  public void runTests(String testGroupName) {
    checkPreRequirements();

    // setup tests
    if (sct == null) {
      sct = new SourceCodeTester();
      setupTests(sct);
    }

    // run tests
    if (results == null) {
      runAllTests();
    }

    // throw exception and return feedback text if at least one test failed.
    if (results.containsKey(testGroupName)) {
      String feedback = results.get(testGroupName);
      if (!feedback.isEmpty()) {
        throw new CodeAnalyserException("TRIM START" + feedback + "TRIM END");
      }
    } else {
      Assert.assertTrue("The testgroup with name '" + testGroupName + "' does not exist! If you are a student and reading this, please notify your teacher about it.", false);
    }

  }

  /**
   * Checks basic jar file requirements.
   */
  private void checkPreRequirements() {
    Assert.assertNotNull("Path/name to the student jar file must be set as 'student-jar' property!", jarPath);
    Path p = Paths.get(jarPath);
    Assert.assertTrue(String.format("The jar '%s' doesn't exist or isn't readable!", jarPath), Files.exists(p) && Files.isReadable(p));
  }

  /**
   * Runs all the tests and saves the results of the test groups for later access.
   */
  private void runAllTests() {
    ArrayList<TestGroupResult> testGroupResults = sct.runTestsObjects(jarPath);
    results = new HashMap<>();

    for (TestGroupResult testGroupResult : testGroupResults) {
      String testGroupName = testGroupResult.getTestGroup().getName();
      if (testGroupResult.getFailedTests().size() > 0) { // if at least one test failed
        results.put(testGroupName, testGroupResult.transformToString(showCodeSnippets)); // generate and save feedback text
      } else { // all tests passed
        results.put(testGroupName, ""); // empty feedback string
      }
    }
  }

}
