# Change-Making 27

Schreiben sie ein Programm, das einen möglichst effizienten Algorithmus für das [Change-making problem](https://en.wikipedia.org/wiki/Change-making_problem) mit den folgenden Münzgrößen implementiert:

```
2, 7, 20, 70, 200, 700, 2000, 7000, ...
```

## Change-making problem

Das Change-making problem beschreibt die Aufgabe, einen vorgegebenen Betrag an Wechselgeld mit der kleinstmöglichen Anzahl an Münzen zusammenzustellen.
Dabei geht man davon aus, dass von jeder erlaubten Münzengröße unendlich viele Münzen vorhanden sind.
Wenn man - wie bei den meisten echten Währungen üblich - die "Münzgrößen" 1, 2, 5, 10, 20, 50, 100, 200, 500, ... usw. wählt, lässt sich das mit Gier optimal lösen: Man nimmt jeweils immer die größte Münze hinzu, so dass der gesuchte Wert noch nicht überschritten wird. Die korrekte Lösung für `74` ist dann z.B. `50 + 20 + 2 + 2`.

Wir nehmen aber absichtlich Münzgrößen bei denen das nicht geht.
Für die Münzgrößen 2, 7, 20, 70, 200, 700, ... usw. würde Gier z.B. für die `80` die Lösung `70 + 2 + 2 + 2 + 2 + 2` finden.
Die kürzeste Lösung ist aber `20 + 20 + 20 + 20`.

## Struktur der Aufgabe

Sie erhalten als Input eine beliebige Anzahl zufällig generierter, durch Zeilenumbrüche getrennter Gewichte.
Geben Sie im Output-Feld für jede Input-Zeile die optimale Kombination an oben genannten Gegengewichten an und schreiben Sie sie wie im Beispiel (siehe unten) gezeigt auf.

## Beispiel

### Generierter Input

```  
80
141
0
777
```

### Ihr Output

```
20,20,20,20
70,20,20,20,7,2,2

700,70,7
```

## Bitte beachten:

- Die Ausgabezeilen müssen der Reihenfolge der Eingabezeilen folgen.
- Innerhalb der Ausgabemengen sollte es keine Leerzeichen oder anderen Whitespace geben.
- Jedes Gegengewicht darf mehrmals verwendet werden.
- Die Zahlen `1`, `3` und `5` lassen sich mit diesen Münzgrößen nicht darstellen. Diese Zahlen werden daher nicht in ihrem Input vorkommen.
- Sie müssen die **optimale** Kombination an Gewichten, d.h. die geringst mögliche Anzahl der benutzten Gegengewichte finden.

## Weitere Level

Diese Aufgabe stellt den niedrigsten Level der Herausforderung dar.
Die Lösungen für die Zahlen von 7 bis 107 sollten Sie selbst mit einfachen Algorithmen leicht berechnen können.
Sie werden aber feststellen, dass bei größeren Zahlen das Problem schnell deutlich schwieriger wird.
Probieren Sie aus, wie weit Sie kommen. Level 5 und 6 reichen sogar bis in den Bereich von 64-Bit Integern (in Java `long`) hinein.