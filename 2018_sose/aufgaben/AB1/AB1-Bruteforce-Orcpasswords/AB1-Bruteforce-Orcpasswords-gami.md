# Kennwörter der Orks
### Dauer: ca. 40 min (5 XP)

*Aufgrund des großen Krieges zwischen den magischen Wesen in der Vergangenheit stehen gerade die älteren, konservativ eingestellten Orks den Freundschaften mit Magiern eher misstrauisch gegenüber.
Nur wirklich gute Freunde und vertrauenswürdige Magier werden in die Dörfer der Orks eingeladen.
In einer heiligen Zeremonie wird den menschlichen Gästen das Kennwort genannt, mit dem sie als willkommene Gäste gelten.*

*Sie wollen einen befreundeten Ork in seinem Dorf besuchen, haben aber das Kennwort vergessen.
Da das Vergessen des heiligen Kennwortes als Vertrauensbruch gilt, wollen Sie sich einen Zauber überlegen, mit dem Sie das vergessene Kennwort wiederfinden können.
Da Orks sehr vergesslich sind, können Sie den Wachen des Orkdorfes alle Kennwörter nach und nach nennen, ohne dass diese merken, dass etwas nicht stimmt.*

*Sie konnten bereits in der Bibliothek der Thaumaturgischen Hochschule für Magie recherchieren und folgende Informationen über die Kennwörter sammeln:*
1. Jedes Kennwort fängt mit "grunz" an.
2. Jedes Kennwort hat eine Länge von maximal 9 Zeichen.
3. Jedes Kennwort besteht nur aus den Buchstaben 'a' bis 'z' und 'A' bis 'Z'.

##### Hinweis
> - *In einem dieser Werke haben Sie den Hinweis bekommen, dass man das `Iteraria-Konstrukt` auch an __Zeichen__ anwenden kann.  
Wie könnte Ihnen das von Nutzen sein?*

*Entwickeln Sie einen Zauber, der alle möglichen Kennwörter erzeugt und sie in einer ArrayList abspeichert.*

*Der Zauber könnte (muss aber nicht) aus den folgenden, einfacheren Teilzaubern bestehen:*
- Nenne alle Kennwörter der Länge 6.
  - Das ist trivial: Kombiniere "grunz" mit jedem möglichen Buchstaben. Also: "grunza", ..., "grunzz", "grunzA", ..., "grunzZ".
- Wiederhole solange, bis alle möglichen Kennwörter genannt sind:
  - Erweitere jedes bis hierhin genannte Kennwort um einen Buchstaben.
- Nenne das Kennwort "grunz" selbst.

[Abgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/19)
