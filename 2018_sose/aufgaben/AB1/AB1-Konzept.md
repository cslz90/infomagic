# Aufgabenblatt 1
## Pflichtaufgaben

|Nr | Thema                         | Name                                                  | Dauer in min   | XP  | %   | Ref |
|---| ---                           | ---                                                   | ---            | --- | --- | --- |
|01 | Recherche                     | AB1-Recherche-Klein-o                                             | Geschätzt: 3h   | 23   |  ?  |  [Link](2018_ss/aufgaben/AB1/AB1-Recherche-Klein-o/AB1-Recherche-Klein-o-klar.md)  |
|02 | Brute-Force                   | AB1-BruteforceOrcpasswords                            | 60 Min, Geschätzt: 40 Min | 5   |  ?  |  [Link](2018_ss/aufgaben/AB1/AB1-BruteForce-OrcPasswords/AB1-Bruteforce-Orcpasswords-klar)  |
|03 | Laufzeitklassen               | AB1-SozialeUngleichheit                               | 45 Min, Geschätzt: 30 Min   | ?   |  ?  |[Link](2018_ss/aufgaben/AB1/AB1-Laufzeitklassen-SozialeUngleichheit/AB1-Laufzeitklassen-SozialeUngleichheit-klar.md)  |
|04 | Laufzeitanalyse               | AB1-LaufzeitanalyseMagischeSelbsteinschätzung         | Geschätzt: 20 Min   | ?   |  ?  |  ?  |
|05 | Schleifeninvariante           | AB1-SchleifeninvariantenDieImmerwahrs                 | Geschätzt: 45 Min | ?   |  ?  |  [Link](2018_ss/aufgaben/AB1/AB1-Schleifeninvarianten-DieImmerwahrs/AB1-Schleifeninvarianten-DieImmerwahrs-klar.md)  |

## Bonusaufgaben

|Nr | Thema                         | Name                                                  | Dauer in min   | XP  | %   | Ref |
|---| ---                           | ---                                                   | ---            | --- | --- | --- |
|01 | LinkedList                    | AB1-DoppeltVerketteterBasilist                        | Geschätzt: ?   | ?   |  ?  |  ?  |
|02 | LinkedList, Laufzeitklassen   | *Beispiel für schnelle LinkedList*                    | Geschätzt: ?   | ?   |  ?  |  ?  |
|03 | O-Notation                    | *Fragen zu O-Notation*                                | Geschätzt: ?   | ?   |  ?  |  ?  |
|04 | Gier                          | AB1-GreedyAlgorithmusGierigeZwerge                    | Geschätzt: ?   | ?   |  ?  |  ?  |
|05 | Laufzeitklassen               | *Wettbewerb für schnellsten Algorithmus*              | Geschätzt: ?   | ?   |  ?  |  ?  |
|06 | Klasseninvariante             | *Beweis einer sinnvollen Klasseninvariante*           | Geschätzt: ?   | ?   |  ?  |  ?  |
|07 | Schleifeninvariante           | *Ähnlich wie die Pflichtaufgabe zum zusätzlichen Üben*| Geschätzt: ?   | ?   |  ?  |  ?  |
