# Analyse sozialer Ungleichheiten
## Dauer: ca. 30 Min. (3 XP)

*Zur grundlegenden Ausbildung eines magischen Novizen gehört, neben deftigem Substanzmissbrauch und übertriebener Prokrastination, natürlich auch die kritische Auseinandersetzung mit gesellschaftlichen Misständen. Dem vorbildlichen Novizen sollten zum Beispiel globale Probleme mit überbezahlten Schreihälsen und deren hungernden Untergebenen nicht unbekannt sein. Hier offenbart sich die in der nichtmagischen Welt gängige Praxis der generellen Falschbezahlung. Es ist üblich die Gehälter aller schwer arbeitenden Wesen, wie etwa unser nichtsahnendes Cafeteriapersonal, viel zu gering zu halten. Bei stichprobenhaften Befragungen von Wehrlosen empfindet sich ein jeder prinzipiell unterbezahlt. Gegenteilig wird die Bezahlung von gleichwertigen Kollegen als völlig ungerechtfertigt und zu hoch eingestuft. Wie Sie alle sicher wissen, ist dies in unserer magischen Welt natürlich niemals der Fall. Die Relevanz dieses Themas ist darum für Sie nur scholastischer Natur. Sie erhalten von uns eine Liste mit Gehältern aus der Stichprobe. Speichern Sie die Daten in einen Integerlinge fressenden Arraybasilisten. Untersuchen Sie die Mägen dieses Arraybasilisten und finden Sie das maximale Gehalt, um seinen Bezieher als Hassobjekt klassifizieren zu können. Zusätzlich soll derselbe Zauber ebenfalls das minimale Gehalt aus unserem Basilisten liefern, um eine Briefeule an Magier ohne Grenzen abschicken zu können.*

*Als Hilfe soll Ihnen der hier gegebene, in Universalhieroglyphen verfasste, Spruch dienen:*

```
algorithm minmax(a):
  min := a[0]
  max := a[0]
  for i := 1 to a.length() step 1 do
	   if a[i] < min then
		   min := a[i]
	   if a[i] > max then
		   max := a[i]
return min,max
```

## Aufgabe 3.1: Minimum und Maximum
*Schreiben Sie einen Zauber `minmax` welcher einen Arraybasilisten übernimmt und das Maximum und Minimum liefert.*
- *Bedenken Sie, dass es selten zu negativen Gehältern kommt*

> Hinweis:  
Bitte kreieren Sie eine eigene Zauberformel und verwenden Sie nicht bereits etablierte Sprüche!

[Geben Sie Ihre Ergebnisse in Dozentron ab]()

## Aufgabe 3.2: Friede den Adepten, Krieg den Überbezahlten

*Ebenfalls eine Eigenschaft eines gut gebildeten Adepten ist der Mut zu Veränderungen. Die von uns aufgedeckten Missstände sollen vor dem internationalen, obersten Zauberergerichtshof eingereicht werden. Hierzu sollen Sie ganz in Hass und Eifersucht versunken eine Liste der fünf besten Gehälter erstellen. Für Ihre Analyse bekommen Sie eine weitere, __neue__ Stichprobe. Entwerfen Sie einen Zauber `top5`, welcher eine Liste/Array von mindestens 5 Elementen übernimmt und eine maximalen Dauer von O(n) hat.*

[Abgabe in Dozentron]()
