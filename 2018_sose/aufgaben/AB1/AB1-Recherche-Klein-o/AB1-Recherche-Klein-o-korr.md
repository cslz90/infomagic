## Korrekturschema:

### Kriterien fürs Nichtbestehen

* keine Quellenangabe
* keine vollständigen Sätze
* völlig unverständlicher Text
* über 1400 Worte Text
* über 10 Quellen

### Punkteverteilung (14 gesamt)

* **1 Punkt** pro korrekt beantworteter Frage (**7 gesamt**)
* **2 Punkte** für verständliche Formulierungen.
* **2 Punkte** für das Einhalten der Zeichengrenze
* **2 Punkt** für verlässliche Quellen
* **1 Punkt** für das Einhalten der Quellengrenze