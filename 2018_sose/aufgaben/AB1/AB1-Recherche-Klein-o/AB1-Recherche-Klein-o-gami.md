## Die Weisen von o, Ω, ω und Θ

*Der Orden der Weisen von O geht auf die ersten Weisen Edmundus Lan'dau und Pauleas den Flussherren zurück.
Sie waren sehr fleißig und mit ihren Schülern zusammen gründeten sie noch viele weitere Orden, die aber viel komplizierter und vor allem komplizierter zu schreiben waren, als "Die Weisen von O".*

*Es lohnt sich aber natürlich als angehender Infomagier, auch die anderen Orden zu kennen.
Die Weisen von o, die Weisen von Ω, die Weisen von ω und die Weisen von Θ sind alle unterschiedlich pessimistisch oder optimistisch ihrem Untersuchungsgegenstand gegenüber eingestellt, aber ihr Rat ist in jedem Fall Gold wert.*

*Insbesondere den Codex der Weisen von o sollten sie sich anschauen.
Sie werden bemerken, dass die anderen Codices im Wesentlichen aus Abschriften und Umkehrungen der Codices der Weisen von Groß-O und Klein-o bestehen.*

### Aufgabe 1.1

*Machen Sie sich mit den Umgangsformen der verschiedenen Orden von Weisen vertraut und schreiben Sie ihre Erfahrungen nieder. Wagen Sie sich dazu ins Netz der großen Google-Spinne oder in die verbotene Bibliothek der Thaumaturgischen Hochschule - je nach dem, was Ihnen weniger gefährlich erscheint.*

*Beantworten Sie dabei die folgenden Fragen:*

* *Wie nennen Muggel die Buchstaben O, o, Ω, ω und Θ im Namen der Orden?*
* *Wie genau ist der Codex der Weisen von o aufgebaut?*
* *Können Sie den Codex in normalsterbliche Worte übersetzen?*
* *Wie unterscheiden sich die Codices der Weisen von o und der Weisen von O?*
* *Kann es sein, dass die anderen Codices im Wesentlichen aus Abschriften und Umkehrungen der Codices der Weisen von Groß-O und Klein-o bestehen?*
* *Wie lauten die Codices der Weisen von Ω, ω und Θ in normalsterblichen Worten?*
* *Die Weisen von Θ behaupten, dass viele Aussagen, die über die Urteile der Weisen von O getroffen werden, eigentlich damit ihre eigenen Urteile meinen. Können Sie ein Beispiel finden, wo eine solche Verwechslung zu Verwirrung führen könnte?*

**Hinweise:**

* *Denken Sie daran, dass die Weisen nichts so sehr hassen wie Geschwafel und daher Abhandlungen über sie, die mehr als 700 Worte umfassen, automatisch mit einem Fluch belegen werden.*
* *Der Codex für die Weisen von Ω ist in mehreren Varianten überliefert. Halten Sie sich an die Überlieferung von Donaldinius dem Knutherich.*
* *Da die Weisen klare Strukturen sehr mögen, dürfen Sie bei der Auflistung der Codices ausnahmsweise eine Liste bzw "Tabelle" verwenden.
    Listen ohne zusätzliche Erklärungen sind den Weisen dagegen auch wieder verhasst und werden mit Flüchen belegt.*

### Aufgabe 1.2

*Um ihren Mitnovizen zu helfen, genauere Studien anzustellen, sprechen Sie bitte einen kurzen Ortungszauber, um ihnen den Weg zu Ihren Informationsquellen zu weisen.
Da Novizen sich schnell von zu vielen Informationen verwirren lassen, beschränken Sie sich auf fünf davon.*
