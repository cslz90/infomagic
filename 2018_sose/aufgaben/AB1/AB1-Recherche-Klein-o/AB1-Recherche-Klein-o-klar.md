## Die Weisen von o, Ω, ω und Θ

### Vorwort

Diese Aufgabe ist eine Rechercheaufgabe wie "Wilde Stammbaumwürmer" aus Aufgabenblatt 0 und folgt daher den gleichen Regeln.

## Aufgabe 1.1

Recherchieren Sie die Notationen für asymptotische Laufzeiten, die es außer der Groß-O-Notation noch gibt.
Beantworten Sie dabei in maximal 700 Worten Fließtext (mit vollständigen Sätzen ohne Stichpunkte!) die folgenden Fragen:

* Wie nennt man die Oberklasse der O-, o-, Ω-, ω- und Θ- Notationen?
* Wie lautet die mathematische Definition der o-Notation?
* Was bedeutet diese Definition umgangssprachlich ausgedrückt?
* Wie unterscheidet sich diese von der O-Notation?
* Wie lassen sich Ω-, ω- und Θ-Notation mit Hilfe der O- und o-Notation definieren?
* Wie kann man O-, o-, Ω-, ω- und Θ- Notation jeweils umgangssprachlich ausdrücken?
* Wir sagen oft umgangssprachlich, dass ein Algorithmus in O(n²) ist, meinen damit aber meistens eher, dass er in Θ(n²) ist. Was ist der Unterschied? Können Sie einen Fall angeben, in dem die Aussage "der Algorithmus ist in O(n²)" zwar korrekt, aber irreführend ist?

**Hinweise:**

* Für die Ω-Notation existieren zwei Definitionen. Verwenden Sie bitte die von Donald Knuth.
* So spaßig die Gamifizierung auch ist, antworten Sie hier aus Gnade mit den Tutoren bitte in Klartext.
* Sie dürfen in Ihrer Antwort die Erklärungen und Unterschiede der Notationen auch in Form einer Liste bzw. "Tabelle" angeben. Die Liste sollte aber nicht für sich alleine stehen, sondern noch einmal im Fließtext erklärt werden.

## Aufgabe 1.2

Geben Sie die Quellen an, die Sie für Ihre Recherche verwendet haben. Hier reicht jeweils ein Link auf die Webseite oder der Titel des Buches, das Sie verwendet haben.
Es geht hier nicht darum herauszufinden, ob sie Formulierungen kopiert haben (was Sie natürlich nicht tun sollten), sondern darum, dass Sie zeigen, dass Sie in der Lage sind, geeignete Quellen zu finden und auszuwählen.

Bitte geben Sie auch nicht *alle* Quellen an, die Sie gelesen haben, sondern nur diejenigen, auf deren Inhalt Sie sich in Ihrem Text stützen.
Es sind maximal fünf Quellen erlaubt.