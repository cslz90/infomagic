# Into the Highlands

### Ein atemberaubender Anblick

*Nachdem der Goldgorgone und seine Novizen den Hain der Speichereichen durchquert hatten, war es bis zum schiefen Berg, wo der Drache Klausuchur lauerte, nicht mehr weit. Trotz Verschnaufpause blieb den Novizen der Atem weg, bei dem was sie vor sich erblickten. Vor ihnen erstreckten sich die atemberaubenden und legendären Highlands von Ja'va. Im leichten Schimmer der Morgensonne ragte der schiefe Berg in der Ferne aus der Gebirgslandschaft hervor. Dem Ziel so nah waren sie dennoch am weiteren Fortschreiten gehindert, denn vor ihren Füßen ging es tausende Meter nach unten. Am Boden sah man klein die Assoziativen Basilisten und Knotlinge in allen Farben, die um sie herumsprangen. Ein Passieren war jedoch unmöglich und der einzige Weg waren die schwebenden Felsen und Inseln, die vor den Novizen lagen. Sie waren durch Brücken miteinander verbunden. Doch die Zeit drängte. Sie mussten den Berg erreichen, bevor die Sonne aufgeht, denn dann wacht Klausuchur auf aus seinem Schlaf. Es musste also der schnellste Weg über die Inseln gefunden werden, um in einer möglichst guten Ausgangslage dem Drachen gegenüberzustehen.*

### Der kürzeste Weg über die Highlands

*Aus dem morgendlichen Nebel erschien plötzlich ein Mann mit langem weißen Bart. Er stellte sich den Novizen als Zauberer Edsgerius Wybe vor und erzählte ihnen, dass er den mächtigen Zauber des Dijkstras beherrsche. Ehrfürchtig schauten sie den Zauberer an und auch die Adepten hatten Respekt vor seiner Macht. Wie eingefroren standen sie da, nur der Goldgorgone trat nach vorne und bat Edsgerius Wybe, seinen Novizen mehr über diesen Zauber zu berichten. Und das tat er. Er sagte ihnen, dass der Zauber der Knotlingspost nachempfunden sei und dass man mit ihm den kürzesten Weg zu einem Ziel finden könne. Nachdem die Novizen ihn von ihrem Plan erzählt hatten, den Drachen Klausuchur zu besiegen, entschloss sich Edsgerius, ihnen den Zauber beizubringen.*

*Das Wichtigste, bevor man losziehe, sei aber, sich einen Überblick über die Highlands zu verschaffen. Dafür gab Edsgerius Wybe den Novizen eine Karte, die er aufgezeichnet hatte, während er die fliegenden Inseln ausgekundschaftet hatte. Es war zu erkennen, dass alle Inseln durch Brücken miteinander verbunden waren. Es gab keine Inselgruppen oder einzelne Inseln, die getrennt von den anderen schwebten. Außerdem konnte man sehen, dass die Entfernungen von einer Insel zu anderen unterschiedlich lang waren.* 

*In der Legende der Karte waren ein paar magische Formeln aufgeschrieben:*

* `FlyingIsland`: Repräsentiert einen Knoten und hat eine ID, um die Knoten vergleichbar zu machen.
* `Bridge`: Repräsentiert eine Kante und hat einen Startknoten `src` und eien Zielknoten `target` sowie ein Gewicht `distance`. Auf alle Felder kann man über Getter-Methoden zugreifen.
* `Highlands`: Repräsentiert den Graphen und besitzt eine Liste an Knoten `islands` und eine Liste an Kanten `bridges`, die über Getter-Methoden zu erhalten sind.

*Um den Zauber des Dijkstras zu erlenen, waren folgende Zaubersprüche unerlässlich:*

```java
  class Dijkstra {

  private class DijkstraInfo {
    int distToStart;
    FlyingIsland predecessor;
    boolean visited;

    DijkstraInfo(int distToStart, FlyingIsland predecessor, boolean visited) {
        this.distToStart = distToStart;
        this.predecessor = predecessor;
        this.visited = visited;
    }
  }
  
  private List<FlyingIsland> islands;
  private List<Bridge> bridges;
  private Map<FlyingIsland, DijkstraInfo> islandInfos = new HashMap<>();
  // ...

  Dijkstra(Highlands highlands) {
    islands = highlands.getIslands();
    bridges = highlands.getBridges();
    // ...
  }

  void execute(FlyingIsland start) { 
    // TODO: implement
  }

  List<FlyingIsland> getShortestPath(FlyingIsland start, FlyingIsland target) {
    // TODO: implement
  }

}
```

#### Hinweise

* Achten Sie darauf, dass sich die Klassen im richtigen Package befinden.
* Benennen Sie die Methoden genau wie in der Aufgabenstellung beschrieben und achten Sie auf korrekte Typen.
* Die oben genannten Klassen erhalten Sie beim Download der Tests über Dozentron.
* Nutzen Sie das Ihnen gegebene Code-Gerüst. Sie können dieses aber beliebig erweitern.
* Es kann helfen, sich zusätzlich zu den oben genannten Methoden noch Hilfsmethoden zu schreiben.
* Es hilft, zusätzlich zu den oben genannten Datenstrukturen noch weitere zu verwenden. Wählen Sie diese sinnvoll aus.
* Nähere Infos dazu wie der Algorithmus von Dijkstra funktioniert und wie ein Graph aufgebaut ist, finden Sie im Vorlesungsskript.