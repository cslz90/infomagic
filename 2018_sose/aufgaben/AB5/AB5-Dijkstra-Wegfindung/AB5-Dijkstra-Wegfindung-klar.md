# Wegfindung mit Dijkstra

### Aufgabe

Gegeben ist ein zusammenhängender gewichteter Graph.
Ihre Aufgabe ist es, mithilfe des Algorithmus von Dijkstra den kürzesten Pfad von einem Startknoten zu einem Zielknoten zu finden. Die Aufgabe soll in Java programmiert werden. Ihnen werden die folgenden Klassen zur Verfügung gestellt:

* `FlyingIsland`: Repräsentiert einen Knoten und hat eine ID, um die Knoten vergleichbar zu machen.
* `Bridge`: Repräsentiert eine Kante und hat einen Startknoten `src` und eien Zielknoten `target` sowie ein Gewicht `distance`. Auf alle Felder kann man über Getter-Methoden zugreifen.
* `Highlands`: Repräsentiert den Graphen und besitzt eine Liste an Knoten `islands` und eine Liste an Kanten `bridges`, die über Getter-Methoden zu erhalten sind.

Erstellen Sie eine Klasse namens `Dijkstra` anhand des vorgefertigten Code-Gerüsts:

```java
class Dijkstra {

  private class DijkstraInfo {
    int distToStart;
    FlyingIsland predecessor;
    boolean visited;

    DijkstraInfo(int distToStart, FlyingIsland predecessor, boolean visited) {
        this.distToStart = distToStart;
        this.predecessor = predecessor;
        this.visited = visited;
    }
  }
  
  private List<FlyingIsland> islands;
  private List<Bridge> bridges;
  private Map<FlyingIsland, DijkstraInfo> islandInfos = new HashMap<>();
  // ...

  Dijkstra(Highlands highlands) {
    islands = highlands.getIslands();
    bridges = highlands.getBridges();
    // ...
  }

  void execute(FlyingIsland start) { 
    // TODO: implement
  }

  List<FlyingIsland> getShortestPath(FlyingIsland start, FlyingIsland target) {
    // TODO: implement
  }

}
```

In `execute` soll der Algorithmus von Dijkstra angewendet werden und der kürzeste Pfad von der Startinsel `start` zu jeder anderen bestimmt werden. Speichern Sie sich die Informationen, die Sie dadurch erhalten, sinnvoll in der Datenstruktur `islandInfos` intern ab. Die Methode `getShortestPath` soll nun den kürzesten Pfad vom Knoten `start` bis zum Knoten `target` ermitteln. Zurückgegeben wird eine Liste aller Knoten, die dabei besucht worden sind.

Fügen Sie **alle** genannten Klassen in einem Package namens `dijkstra` zusammen.

#### Hinweise

* Achten Sie darauf, dass sich die Klassen im richtigen Package befinden.
* Benennen Sie die Methoden genau wie in der Aufgabenstellung beschrieben und achten Sie auf korrekte Typen.
* Die oben genannten Klassen erhalten Sie beim Download der Tests über Dozentron.
* Nutzen Sie das Ihnen gegebene Code-Gerüst. Sie können dieses aber beliebig erweitern.
* Es kann helfen, sich zusätzlich zu den oben genannten Methoden noch Hilfsmethoden zu schreiben.
* Es hilft, zusätzlich zu den oben genannten Datenstrukturen noch weitere zu verwenden. Wählen Sie diese sinnvoll aus.
* Nähere Infos dazu wie der Algorithmus von Dijkstra funktioniert und wie ein Graph aufgebaut ist, finden Sie im Vorlesungsskript.