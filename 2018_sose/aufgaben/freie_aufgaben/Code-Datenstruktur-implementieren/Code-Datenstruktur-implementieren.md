*Nicht nur neue Zauber sind reizvoll, sondern Sie werden in der Wildnis auch auf Datenkreaturen treffen, die Sie mit ihrer bizarren Schönheit in blankes Staunen versetzen.
Wagen Sie sich daran mit Hilfe Ihres Bestiariums eine Beschwörungsformel für eines dieser Wesen zu entwickeln!*

Implementieren Sie die folgenden Datenstrukturen in einer **beliebigen Programmiersprache** (echter ausführbarer Code, kein Pseudocode).
Liefern Sie dazu auch unbedingt eine vollständige Dokumentation, wie die Datenstruktur funktioniert und wie ihr Code zu verwenden ist.

Die Abgabe erfolgt direkt in der jeweiligen Aufgabe (bei mehreren Dateien als Zip-Archiv) und nicht über Dozentron.

---

## ASCIIString (25 XP)

Implementieren Sie eine Datenstruktur `ASCIIString`, die einen `String` auf basis eines Byte-Arrays implementiert.
Geben Sie ihrer Datenstruktur mindestens drei sinnvolle Operationen.

## Bit-Set (30 XP)

Implementieren Sie eine Datenstruktur `LetterBitSet`, die die Zeichen `a`, `b`, `c`, usw. bis `z` in einer Menge speichern kann.
Die Information, ob ein Zeichen in der Menge ist oder nicht soll in einem einzelnen `int` gespeichert werden.
Ihre Datenstruktur muss mindestens die folgenden Operationen unterstützen:

* `contains` prüft, ob ein Buchstabe in der Menge enthalten ist
* `add` fügt einen Buchstaben der Menge hinzu
* `remove` löscht einen Buchstaben aus der Menge
* `intersect` bildet die Schnittmenge von zwei `LetterBitSet`s
* `union` bildet die Vereinigung von zwei `LetterBitSet`s

## Remove für LinkedList (10 XP)

Sie haben die LinkedList im Aufgabenblatt 0 schon implementiert, allerdings wurde im Interface keine remove-Methode beschrieben. Entwickeln Sie nun eine Methode `remove` für ihre eigene LinkedList.

*Danke an Bilal Karim Reffas*