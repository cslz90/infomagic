*Wer den berühmten Goldgorgonen kennt, weiß, er ist auch berüchtigt: Der* ***Confusio Professorii*** *Fluch hat ihn schon sehr früh getroffen. Deswegen ist es sehr hilfreich in seinen Veranstaltungen, Buch zu führen, um den Gedankensprüngen und begeisterten Anekdoten folgen zu können.*

Machen Sie sich während der Vorlesung Notizen und laden Sie diese hier hoch.
Die Notizen dürfen gerne in Stichworten verfasst und Abfotografiert sein, sollten aber natürlich noch lesbar sein. 😉