*Nur wer wagt, der auch gewinnt! Ein mutiger Novize geht nicht nur ausgetrampelte Pfade, sondern betritt Neuland! Versuchen Sie sich an der vollständigen Entwicklung eines neuen, noch nicht im Kurs besprochenen Zaubers!*

Implementieren Sie die folgenden Algorithmen in einer **beliebigen Programmiersprache** (echter ausführbarer Code, kein Pseudocode).
Liefern Sie dazu auch unbedingt eine vollständige Dokumentation, was der Algorithmus tut und wie ihr Code zu verwenden ist.

Die Abgabe erfolgt direkt in der jeweiligen Aufgabe (bei mehreren Dateien als Zip-Archiv) und nicht über Dozentron.

---

## Filter-Operation für LinkedList (15 XP)

Implementieren Sie eine Operation `filter` für die Datenstruktur `LinkedList`.

Parameter:

* `pred` ein Prädikat-Objekt mit der Operation `pred.evaluate(x)`, die für jedes Listenelement `x` entweder `true` oder `false` zurückgibt

Rückgabewert:

* Eine neue Liste, die nur diejenigen Elemente `x` enthält, für die `pred.evaluate(x)` den Wert `true` ergibt

Beispiel: [filter-Methode von Java-Streams](https://docs.oracle.com/javase/9/docs/api/java/util/stream/Stream.html#filter-java.util.function.Predicate-)

## Maximum, Median und Minimum für ArrayList (10 XP)

Implementieren Sie die Operationen `max`, `median` und `min`, die das Maximum, den Median und das Minimum einer `ArrayList` bestimmen.

## flatten-Methode für Arrays (20 XP)

Entwickeln Sie eine Funktion in einer beliebigen Programmiersprache, welches ein mehrdimensionales Array *flattet*. Es soll also zum Beispiel aus einem [[[1]],[[2]],[[3]],[[3],[4],[2],[2,3,4]],[[43]]] ein [1,2,3,3,4,2,2,3,4,43] erstellt werden.

*Danke an Bilal Karim Reffas*

## Zweierpotenzen-Sequenz (20 XP)

Finden Sie in einem Array von Zahlen die längste Sequenz von aufeinanderfolgenden Zweierpotenzen.

In [1,2,5,6,7,2,4,8,1] wäre das zum Beispiel [2,4,8], in [1,2,3,8,4,16,5,6,7,8] dagegen [1,2].

## "Hello World!" Brute Force (15 XP)

Schreiben Sie eine Methode findResult(), in der Klasse BruteHello, die keinen Rückgabewert hat.
Als gegeben ist der String "Hello World!" anzunehmen. Ihre Methode soll die einzelnen Werte der ASCII-Tabelle durchlaufen und diese auf der Konsole anzeigen, wenn ein Buchstabenmatch
gefunden wurde (z. B. ein großes 'H'), soll dieses auf der Konsole stehen bleiben.
Neben dem 'H' sollen jetzt alle ASCII-Zeichen bis zum nächsten Match nacheinander gezeigt
werden. Der Vorgang soll solange andauern bis "Hello World!" auf der Konsole zu sehen ist.

Hinweis:

Die Methode soll nur einen einzeiligen Konsolenoutput haben der sich "aktualisiert",
also sind Leerzeilen nicht erlaubt.

*Danke an Alex Ruhl*

## Zaubersammlung

Sie wollen alle Zauber der Welt lernen.
Dazu müssen Sie sich natürlich Zauberbücher kaufen, in denen diese enthalten sind.
Jedes Zauberbuch hat einen Preis in Golddublonen und beinhaltet eine Reihe von Zaubern (der einfachheit halber nummerieren wir diese).
Finden Sie aus einer vorgegebenen Reihe solcher Bücher eine Auswahl, die möglichst wenig kostet und jeden möglichen Zauber abdeckt.

Beispiel für Bücher:

1. Zauber 1,2 und 7 für 10 Dublonen
2. Zauber 5,3,2,1 und 8 für 40 Dublonen
3. Zauber 6,1,5 und 0 für 30 Dublonen
4. Zauber 4 für 5 Dublonen
5. Zauber 9 und 7 für 10 Dublonen

In diesem Fall wäre eine günstige Zusammenstellung die Bücher 2,3,5 und 4 zu kaufen und Buch 1 stehen zu lassen.

## N-Bonacci (20 XP)

Die Tribonacci-Folge und die altbekannte Fibonacci-Folge folgen alle einem Muster. Aber bei zwei oder drei Summanden hört es ja noch lange nicht auf!

Die Aufgabe ist es, eine Methode `bonacci` mit zwei Parametern `n` und `k` zu schreiben, welche `k` Folgenglieder der `n`-Bonacci Folge ausgibt.

Der Parameter `n` gibt dabei an, wie viele der vorherigen Folgenglieder jeweils aufaddiert werden müssen. Bei Fibonacci wäre `n = 2`, bei Tribonacci `n = 3`.

**Beispiel:** 

`bonacci(4, 11)`

`n = 4`: die vier vorherigen Folgenglieder müssen aufsummiert werden
`k = 11`: bedeutet die ersten 11 Folgenglieder sind auszugeben

Der Algorithmus würde also folgende Ausgabe produzieren:  
`0, 0, 0, 1, 1, 2, 4, 8, 15, 29, 56`

**Hinweis:**

Die ersten `n-1` Ausgaben sind immer 0 und die `n`-te Ausgabe ist eine 1, wie beim Beispiel von `n = 4` zu sehen ist.

*Danke an Niklas Deworetzki*

## Negative ungerade zahlen (5 XP)

Implementieren Sie eine Funktion `negativeOddSum(k)` zur Berechnung der Summe aller negativen ungeraden ganzen Zahlen, die größer oder gleich einer Zahl `k` sind. Die Zahl k soll der Funktion als Parameter übergeben werden. Wenn eine Zahl als Parameter übergeben wird, die nicht kleiner als 0 ist, dann gibt die Funktion eine aussagekräftige Fehlermeldung aus und beendet das Programm, das sie aufgerufen hat.

**Beispiel:** 

`negativeOddSum(8)`

`k = 8`: `(-7) + (-5) + (-3) + (-1) = -16` 

*Danke an Bilal Karim Reffas*
