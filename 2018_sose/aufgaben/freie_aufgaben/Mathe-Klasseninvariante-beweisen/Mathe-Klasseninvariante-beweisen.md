*Im Objektorient gibt es eine besonders interessante Form der Immerwahrs.
Diese zarten Pflänzchen sind nicht auf algomantische Formeln sondern auf Datenkreaturen abgestimmt.
Sie halten sich mit ihren Wurzeln an deren Körper fest und leben in Symbiose mit ihrem Wirt.
Sie gedeihen nur, wenn die Datenkreatur sich immer korrekt verhält, garantieren dann aber auch, dass die Daten im Inneren der Kreatur ihr nie Probleme machen.*

Beweisen Sie die Gültigkeit der folgenden Klasseninvarianten.

---

Beispiele:

## ArrayList: 0 <= size <= content.length (30 XP)

## LinkedList: tail != null (30 XP)
