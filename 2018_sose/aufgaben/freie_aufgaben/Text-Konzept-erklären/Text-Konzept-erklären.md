*Die Enzyklopedia Algomantika galt stets als* ***das*** *Nachschlagewerk eines Infomagiers. Als jedoch Wolfur der Blaue versehentlich die uralte Bibliothek niederbrannte, ist es bedauerlicherweise zerstört worden. Helfen Sie mit, das unschätzbare Wissen wieder zusammenzutragen!*

Schreiben Sie eine kurze(!) Erklärung zu den unten genannten Konzepten in eigenen Worten und vollständigen Sätzen.

---

## Java: Kovarianz, Kontravarianz, Invarianz (30 XP)

## Java: Lambda-Ausdrücke (20 XP)

## Java: Default-Methoden (10 XP)

## Java: Functional Interfaces (10 XP)

## Multiple Vererbung (20 XP)

## Tail-call optimization (30 XP)

## NP-schwer (20 XP)

## Unentscheidbar (15 XP)

## Hoare-Kalkül (35 XP)

## Branch-and-bound-Algorithmen (20 XP)

## Randomisierter Algorithmus (20 XP)

## Sweep-Verfahren (20 XP)