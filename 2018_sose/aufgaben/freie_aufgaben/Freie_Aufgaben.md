*Ein echter Infomagier schreitet nicht nur auf ausgetreten Wegen, sondern wagt sich selbst in die Wildnis, um neue algomantische Formeln zu erlernen und sagenumwobene Datenkreaturen zu entdecken.
Natürlich gelten außerhalb der Mauern der Thaumaturgischen Hochschule rauere Gesetze und Sie werden sich nicht so sehr auf die Unterstützung ihres Infomagisters verlassen können, aber dafür winken dort auch große Freiheit, Abenteuer und unschätzbares Wissen.*

Die folgenden Aufgaben sollen Ihnen einen Anreiz bieten, sich im Selbststudium mit den Themen der Veranstaltung zu beschäftigen und ein wenig über den Tellerrand zu blicken.

Sie bekommen hier keine fertigen Aufgabenstellungen, sondern nur eine grobe Idee.
Dafür sind Sie (mit Hilfe des Skills "Allgemeine Beispiel-Magie") völlig frei, eigene Ideen einzureichen.
Wenn es irgendein Thema gibt, was Sie besonders interessiert, fühlen Sie sich frei, es als Aufgabe vorzuschlagen!