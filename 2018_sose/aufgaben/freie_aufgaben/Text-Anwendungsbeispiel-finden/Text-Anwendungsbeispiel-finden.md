*Ein Zauber ist nur zu etwas nütze, wenn man auch weiß, wann und in welcher Situation man ihn anwenden kann. Sie haben bereits einen Fundus an Zaubern in ihrem magischen Folianten. Wie und wann kann man sie eigentlich anwenden?*

Erstellen Sie zu den unten genannten Problemfällen eine Lösungsskizze, die (in Worten) beschreibt, wie man mit einem Algorithmus bzw. einer Datenstruktur aus der Vorlesung das jeweilige Problem lösen kann.

Sie dürfen auch Pseudocode-Stücke in ihre Erklärung einfügen, sollten aber noch ein paar Worte zur Erklärung der Strategie mit dazu schreiben.

---

## LinkedList: Einfacher Stackmaschinen-Interpreter (15 XP)

Problem: Schreiben Sie einen einfachen Interpreter, der die folgenden Befehle für eine Stackmaschine ausführen kann:

* `pushc x` legt den Wert `x` oben auf den Stack
* `dup` dupliziert das oberste Element des Stacks
* `add`, `sub`, `mul`, `div` ersetzen die obersten beiden Elemente durch das Ergebnis einer Addition, Subtraktion, Multiplikation bzw. Division dieser beiden Werte.

## ArrayList: Run-length encoding (10 XP)

Problem: Kodieren Sie einen DNA-String (z.B. `AGCTTTAGAAACGT`) mit Hilfe eines [Run-length encoding](https://en.wikipedia.org/wiki/Run-length_encoding).

## Stack: Verschachtelte Klammern (15 XP)

Problem: Finden Sie heraus ob ein String korrekt geklammert ist. Erlaubt sind runde `()`, eckige `[]` und geschweifte `{}` Klammern.

Beispiele:

* `{ foo foo [ bar [ mopp ]] yodl }` ist korrekt geklammert
* `foo [ bar [ baz ]` hier fehlt eine schließende Klammer
* `{ wango [ tango } mango ]` hier stimmt die Anzahl der klammern, aber die Reihenfolge ist falsch