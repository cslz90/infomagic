*Die Weisen von O sind seit je und her schwer damit beschäftigt, die Streitereien zwischen Infomagiern zu schlichten, wessen Zauber nun in der besseren Effizienzklasse liegt.
Sie suchen daher immer nach willigen und fähigen Aushilfskräften, die diese Fragen für sie klären.
Im Gegenzug lassen sie die Novizen, die sich der mühsamen Aufgabe annehmen, an ihrem unerschöpflichen Wissen teilhaben.*

Finden Sie eine möglichst enge Schranke für die Laufzeit der unten genannten Algorithmen und beweisen sie diese (d.h. argumentieren sie mathematisch, warum diese Schranke gilt).

---

## Operationen von `ArrayList` (20 XP)

Bestimmen Sie die Effizienzklasse für die Operationen der Datenstruktur ArrayList aus der Vorlesung.

## Operationen von `LinkedList` (20 XP)

Bestimmen Sie die Effizienzklasse für die Operationen der Datenstruktur LinkedList aus der Vorlesung.

## Eigene Lösung von AB0 - Aufgabe 4 (15 XP)

Bestimmen Sie die Effizienzklasse deiner eigenen Lösung von Aufgabe 4 aus AB0.
