# Algorithmus beschreiben

*Nur wer wagt, der auch gewinnt! Ein mutiger Novize geht nicht nur ausgetrampelte Pfade, sondern betritt Neuland! Versuchen Sie sich an der Skizzierung eines neuen, noch nicht im Kurs besprochenen Zaubers!*

Schreiben Sie einen Pseudocode zu einem **nicht** in der Veranstaltung besprochenen Algorithmus. Dies muss in eigenen Worten und nach der Pseudocode-Konvention der Vorlesung erfolgen.

---

## Filter-Operation für LinkedList (15 XP)

Beschreiben Sie eine Operation `filter` für die Datenstruktur `LinkedList`.

Parameter:

* `pred` ein Prädikat-Objekt mit der Operation `pred.evaluate(x)`, die für jedes Listenelement `x` entweder `true` oder `false` zurückgibt

Rückgabewert:

* Eine neue Liste, die nur diejenigen Elemente `x` enthält, für die `pred.evaluate(x)` den Wert `true` ergibt

Beispiel: [filter-Methode von Java-Streams](https://docs.oracle.com/javase/9/docs/api/java/util/stream/Stream.html#filter-java.util.function.Predicate-)

## Maximum, Median und Minimum für ArrayList (15 XP)

Beschreiben Sie die Operationen `max`, `median` und `min`, die das Maximum, den Median und das Minimum einer `ArrayList` bestimmen.