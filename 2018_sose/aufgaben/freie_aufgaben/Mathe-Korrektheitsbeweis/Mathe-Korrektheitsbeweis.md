*Wenn ein Immerwahr seine volle Blüte erreicht hat, kann es sich mit anderen Logikpflänzchen zu einem mächtigen Wahrheitswurzelwerk verbinden.
Infomagier suchen fieberhaft nach Orten an denen solche Gebilde wachsen, weil man dort Zauber erlernen kann, die niemals fehlschlagen.
Wenige wissen jedoch, dass es gar nicht so viel Aufwand braucht, um sich selbst ein Wahrheitswurzelwerk zu züchten.
Beweisen Sie doch ihren grünen Daumen und versuchen sich einmal daran!*

Beweisen Sie die Korrektheit der folgenden Algorithmen (d.h. argumentieren sie mathematisch, warum diese Algorithmen für alle *gültigen* Eingaben korrekte Ausgaben liefern).

---

Beispiele:

## Operationen von `ArrayList` (35 XP)

Beweisen Sie für mindestens drei Operationen der Datenstruktur `ArrayList` aus der Vorlesung, dass diese korrekt sind.

## Operationen von `LinkedList` (35 XP)

Beweisen Sie für mindestens drei Operationen der Datenstruktur `LinkedList` aus der Vorlesung, dass diese korrekt sind.

## Eigene Lösung von AB0 - Aufgabe 4 (30 XP)

Beweisen Sie die Korrektheit ihrer eigenen Lösung von Aufgabe 4 auf Aufgabenblatt 0.

## Iterative Fibonacci-Folge (25 XP)

Beweisen Sie die Korrektheit der Methode `fibonacciIterative` in diesem [Gist zur Fibonacci-Folge in Java](https://gist.github.com/meghakrishnamurthy/331bd9addab3dbb1b6a23802b1c6845e).