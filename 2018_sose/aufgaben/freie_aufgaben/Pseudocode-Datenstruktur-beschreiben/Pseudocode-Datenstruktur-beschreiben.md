# Datenstruktur beschreiben

*Nicht nur neue Zauber sind reizvoll, sondern Sie werden in der Wildnis auch auf Datenkreaturen treffen, die Sie mit ihrer bizarren Schönheit in blankes Staunen versetzen.
Wagen Sie sich in die Außenwelt und füttern Sie ihr Bestiarium mit einer Beschreibung einer solchen Kreatur.
Aber vorsicht: Nicht zu viel schreiben, sonst wird das Bestiarium bissig.*

Beschreiben Sie eine **nicht** in der Veranstaltung besprochene Datenstruktur als Pseudocode.
Dies muss in eigenen Worten und nach der Pseudocode-Konvention der Vorlesung erfolgen.

---

## ASCIIString (30 XP)

Beschreiben Sie eine Datenstruktur `ASCIIString`, die einen `String` auf basis eines Byte-Arrays implementiert.
Geben Sie ihrer Datenstruktur mindestens drei sinnvolle Operationen.

## Bit-Set (35 XP)

Beschreiben Sie eine Datenstruktur `LetterBitSet`, die die Zeichen `a`, `b`, `c`, usw. bis `z` in einer Menge speichern kann.
Die Information, ob ein Zeichen in der Menge ist oder nicht soll in einem einzelnen `int` gespeichert werden.
Ihre Datenstruktur muss mindestens die folgenden Operationen unterstützen:

* `contains` prüft, ob ein Buchstabe in der Menge enthalten ist
* `add` fügt einen Buchstaben der Menge hinzu
* `remove` löscht einen Buchstaben aus der Menge
* `intersect` bildet die Schnittmenge von zwei `LetterBitSet`s
* `union` bildet die Vereinigung von zwei `LetterBitSet`s