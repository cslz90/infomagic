*Eine Mitschrift während der Vorträge des Goldgorgonen anzufertigen ist hilfreich, bereitet aber noch lange nicht auf die Abschlussprüfung vor. Dafür müssen die wirren Kritzeleien und Gedanken erst noch in Form gebracht werden, damit sie Ihnen später auch helfen können. Berühmt geworden für seine Stundenprotokolle, die er Pergament um Pergament anfertigte, ist Seidur der Blaue in jungen Jahren. Nach seinem Studium der Infomagie bewahrte er die Protokolle in der uralten Bibliothek auf.*

Überarbeiten Sie ihre Mitschrift zu Hause noch einmal.
Formulieren Sie vollständige Sätze, fügen Sie Diagramme ein - was immer Ihnen hilft, um die Veranstaltung besser zu verstehen.
Wichtig ist für diese Abgabe, dass man erkennen kann, dass Sie sich Mühe gegeben haben, ihre Notizen in eine strukturiertere schönere Form zu bringen.

**Hinweis:** Sie können für jeden Termin nur ein Stundenprotokoll **oder** eine Mitschrift einreichen.