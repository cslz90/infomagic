*Bei Ihren fleißigen magischen Recherchen sind Sie sicher schon auf die Immerwahrs gestoßen: Klein, buntblühend und sehr empfindlich gegenüber fehlerhafter Magie. Zweimal im Jahr florieren sie und erreichen ihre größte Ausbreitung interessanterweise immer am Ende des Semesters. Dabei ernährt sich jedes kleine Immerwahr von seinem eigenen Zauber. Wählen Sie sich eines aus der unten stehenden Blütenwiese aus und kümmern sich um es, indem sie ihm versichern, dass es nie verblühen muss.* 💮

Beweisen Sie die folgenden Schleifeninvarianten. Laden Sie Ihre Abgabe in der entsprechenden Unteraufgabe hoch.

---

## Iterative Fibonacci-Folge (20 XP)

Beweisen Sie die folgende Schleifeninvariante für die Methode `fibonacciIterative` in diesem [Gist zur Fibonacci-Folge in Java](https://gist.github.com/meghakrishnamurthy/331bd9addab3dbb1b6a23802b1c6845e).

fib_i = fib(i)

prevFib_i = fib(i-1)