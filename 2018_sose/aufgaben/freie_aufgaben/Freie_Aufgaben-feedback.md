# Feedback: Freie Aufgaben


## Textaufgaben

### Konzept erklären

#### Kovarianz, Kontravarianz, Invarianz

#### Lambda-Ausdrücke

#### Default-Methoden

#### Funktionale Interfaces

#### Multiple Vererbung

#### Tail-Call optimization

### Anwendungsbeispiele schreiben

#### LinkedList: Einfacher Stackmaschinen - Interpreter

> __Dominik__:
>Ich würde dir bei der Aufgabe einen Pseudocode hinsetzen mit einer LinkedList
und die Methoden wie pushc, add etc. auf dieser Liste. Wäre es das, was du
erwarten würdest? Wenn ja, wäre eine Erwähnung, dass sie Pseudocode schreiben
könnnen sinnvoll. Das Wort "Interpreter" überfordert manche sicher.
15 XP fände ich dafür gerechtfertigt.

#### ArrayList: Run-length encoding

> __Dominik__:
Auch hier würde ich Pseudocode zulassen.
Aufgabe sonst sehr klar und 10 XP finde ich gut.

## Pseudocodeaufgaben

### Algorithmus beschreiben

#### Filter-Operation für LinkedList

> __Dominik__:
Erklärung was ein Prädikat-Objekt ist, wäre hilfreich.
Da es eine Pseudocode Aufgabe ist, glaube ich, dass 20XP für den Aufwand
zu viel sein könnten.

#### Maximum, Median und Minimum für ArrayList

> __Dominik__:
Klingt gut. Wir haben allerdings schon eine Aufgabe, die letztes Semester Pflicht
war, wo zumindest Min und Max bereits einmal berechnet werden sollten. Daher ist
die Aufgabe etwas redundant, wenn wir sie dieses Semester auch nehmen. XP ist okay.


### Datenstruktur beschreiben

#### ASCIIString

> __Dominik__:
Klingt machbar. Durch den Anstoß sich selbst noch eigene Methoden zu überlegen
wird das freie Denken gefordert :D 30 XP könnten zu viel sein, bin mir aber unsicher.

#### Bit-Set für Character

> __Dominik__:
Schöne Aufgabe, da das arbeiten mit einzelnen Bits und das entsprechende Umdenken
durchaus nützlich sein kann für spätere Semester. 35 XP könnten gut sein.

## Matheaufgaben

### Laufzeitanalyse

#### Operationen von ArrayList

> __Dominik__:
Um die Aufgabe universell einsetzbar für spätere Semester zu machen, sollten die
Konkreten Hinweise auch dort zugänglich sein.

#### Operationen von LinkedList

> __Dominik__:
Um die Aufgabe universell einsetzbar für spätere Semester zu machen, sollten die
Konkreten Hinweise auch dort zugänglich sein.

#### Eigene Lösung von AB0 - Aufgabe 4

> __Dominik__:
Aufgabe 4 muss im nächsten Semester nicht die selbe sein. Auch hier wäre eine
klare Nennung nützlich.

### Schleifeninvariante beweisen

>__Dominik__:
Nicht mein Metier, kann dazu nichts sagen.

### Klasseninvariante beweisen

>__Dominik__:
Nicht mein Metier, kann dazu nichts sagen.

### Korrektheitsbeweis

>__Dominik__:
Nicht mein Metier, kann dazu nichts sagen.

## Programmieraufgaben

### Anwendungsbeispiel implementieren

#### LinkedList: Einfacher Stackmaschinen-Interpreter

> __Dominik__:
Sollen die Implementierungen auf Dozentron hochgeladen werden? Dann muss das
erwähnt werden. 30XP ist okay.

#### ArrayList: Run-length encoding

> __Dominik__:
Sollen die Implementierungen auf Dozentron hochgeladen werden? Dann muss das
erwähnt werden. 20XP ist auch hier okay.

### Algorithmus Implementieren

#### Filter-Operation für LinkedList

> __Dominik__:
Siehe oben bei der Referenzaufgabe. Angabe wie es abgegeben werden muss ist nicht
klar.

#### Maximum, Median und Minimum für ArrayList

> __Dominik__:
Siehe oben bei der Referenzaufgabe. Angabe wie es abgegeben werden muss ist nicht
klar.

### Datenstruktur Implementieren

#### ASCIIString

> __Dominik__:
Siehe oben bei der Referenzaufgabe. Angabe wie es abgegeben werden muss ist nicht
klar.

#### Bit-Set

> __Dominik__:
Siehe oben bei der Referenzaufgabe. Angabe wie es abgegeben werden muss ist nicht
klar.

## Allgemeines:

> __Dominik__:
Es wird problematisch, wenn die freien Aufgaben mehr XP für weniger Aufwand geben,
als die Bonusaufgaben. Der Spieler in Games wählt immer den Weg des geringsten
Aufwands um sein Ziel zu erreichen. Da die Bonusaufgaben dem erforderlichen
Lehrstoff beinhalten, sind sie höher anzusiedeln als die freien Aufgaben.
