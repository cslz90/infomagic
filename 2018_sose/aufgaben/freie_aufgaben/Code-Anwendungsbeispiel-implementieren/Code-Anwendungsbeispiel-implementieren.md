*Ein Zauber ist nur zu etwas nütze, wenn man auch weiß, wann und in welcher Situation man ihn anwenden kann. Sie haben bereits einen Fundus an Zaubern in ihrem magischen Folianten und sich vielleicht schon überlegt, wie und wann man sie anwenden kann. Die Thaumaturgische Hochschule der Magie ist keine merkwürdige schottische Zaubererakademie, die ihren Schülern verbietet, die neu gelernten Zauber auch auszuprobieren. Suchen Sie sich einen Zauber aus, wenden Sie ihn auf ein reales Problem an und erleichtern Sie sich Ihren Alltag!*

Implementieren sie die folgenden Anwendungsbeispiele von in der Vorlesung erlernten Algorithmen und Datenstrukturen in einer **beliebigen Programmiersprache** (echter ausführbarer Code, kein Pseudocode).

Die Abgabe erfolgt direkt in der jeweiligen Aufgabe (bei mehreren Dateien als Zip-Archiv) und nicht über Dozentron.

---

## LinkedList: Einfacher Stackmaschinen-Interpreter (30 XP)

Problem: Schreiben Sie einen einfachen Interpreter, der die folgenden Befehle für eine Stackmaschine ausführen kann:

* `pushc x` legt den Wert `x` oben auf den Stack
* `dup` dupliziert das oberste Element des Stacks
* `add`, `sub`, `mul`, `div` ersetzen die obersten beiden Elemente durch das Ergebnis einer Addition, Subtraktion, Multiplikation bzw. Division dieser beiden Werte.

## ArrayList: Run-length encoding (20 XP)

Problem: Kodieren Sie einen DNA-String (z.B. `AGCTTTAGAAACGT`) mit Hilfe eines [Run-length encoding](https://en.wikipedia.org/wiki/Run-length_encoding).

## Iterator: Beispiel für Effizienz von LinkedList (15 XP)

Schreiben Sie ein konkretes Stück Code, bei dem die LinkedList-Implementierung von Java tatsächlich schneller ist (messen Sie die Zeit!) als die ArrayList-Implementierung.

Tipp: Das ist schwerer als man zuerst denkt und ohne Iterator wird es kaum gelingen.

## Stack: Verschachtelte Klammern (20 XP)

Finden Sie heraus ob ein String korrekt geklammert ist. Erlaubt sind runde `()`, eckige `[]` und geschweifte `{}` Klammern.

Beispiele:

* `{ foo foo [ bar [ mopp ]] yodl }` ist korrekt geklammert
* `foo [ bar [ baz ]` hier fehlt eine schließende Klammer
* `{ wango [ tango } mango ]` hier stimmt die Anzahl der klammern, aber die Reihenfolge ist falsch