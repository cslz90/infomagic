\documentclass{screxam-cs}
\usepackage{cspseudo}
\usepackage{enumitem} % to define \begin{itemize}[label=...]
\usepackage{forest}
\usepackage{array} % allows to set columns in tabular to \raggedright

\usetikzlibrary{matrix}
\usetikzlibrary{positioning}


\newcommand{\checkbox}{\tikz[anchor=base, baseline] {\node[draw=black!20, very thick, rounded corners] (a) {\phantom{X}};}}
\newcommand{\checkitem}{\item [\checkbox{}]}
\newenvironment{checkitemize}
  {\begin{itemize}[label=\protect\checkbox]}
  {\end{itemize}}

\title{Klausur A\&D SoSe 2018}
\author{Christopher Schölzel}
\lecture{A\&D Sommersemester 2018}

\lstset{language=cs-pseudo}

\newcommand{\bigO}{\ensuremath{\mathcal{O}}}
\newcommand{\Oof}[1]{\ensuremath{\mathcal{O}(#1)}}

\begin{document}
\pagenumbering{arabic}

\begin{nobreakarea}
% Zeit: 2 min
\points{7}
\section{\bigO-Notation}

Bitte kreuzen Sie korrekte Aussagen an:

\begin{checkitemize}
  \item $n^2 \in \Oof{n}$
  \item $n \in \Oof{n^2}$
  \item $n + n \in \Oof{n}$
  \item $n \cdot \log n \in \Oof{n}$
  \item $\Oof{4n^2 + 3n^3 + 7 \log n} = \Oof{n^3}$
  \item $n^{n+1} \in \Oof{n^n}$
  \item $n \cdot n! \in \Oof{(n+1)!}$
\end{checkitemize}
\end{nobreakarea}

\begin{nobreakarea}
% Zeit: 4 min
\points{12}
\section{Binäre Suche}

Gegeben Sei die folgende sortierte Array-Liste von Zahlen:

\code{lst := [1, 4, 7, 8, 9, 10, 13, 18, 22]}

\begin{itemize}
  \item[a)] Geben Sie in dem folgenden Feld die Zahlen aus der Liste an, auf die Sie zugreifen müssen, wenn Sie eine binäre Suche nach der Zahl 8 ausführen. Achten Sie auf die korrekte Zugriffsreihenfolge.

  \textarea{\parskip}
  \item[b)] Zeichnen Sie einen (balancierten) binären Suchbaum mit den Zahlen aus \code{lst}.

  \textarea{200pt}

  \item[c)] Geben Sie die Laufzeit (möglichst enge Schranke in \bigO-Notation) der binären Suche in einer Array-Liste und in einem binären Suchbaum an.

  \renewcommand{\arraystretch}{1.5}
  \begin{tabular}{l l}
    Array-Liste: & \textfield{50pt}\\
    Binärer Suchbaum: & \textfield{50pt}\\
  \end{tabular}

  \item[d)] Geben Sie an, wie viele Listenzugriffe im schlimmsten Fall für eine binäre Suche in einer Array-Liste der Länge 1024 nötig wären.

  Anzahl der Zugriffe: \textfield{200pt}
\end{itemize}
\end{nobreakarea}

\begin{nobreakarea}
% Zeit: 14 min
\points{42}
\section{Kürzeste Wege mit negativen Kanten: Bellman-Ford}

Der Bellman-Ford-Algorithmus ist eine Möglichkeit mit dem Problem der kürzesten Wege umzugehen, wenn negative Kanten im Graph enthalten sind. Er funktioniert nach dem folgenden Prinzip:

\begin{lstlisting}[numbers=left]
  algorithm shortestPathsNeg(g: WeightedGraph<E>, start: WGNode<E>)
    dist := new HashTable<WGNode<E>, real>()
    pred := new HashTable<WGNode<E>, WGNode<E>>()
    for n in g.nodes() do
      dist.add(n, Infinity)
      pred.add(n, None)
    dist.update(start, 0)
    for i := 1,2,3,..., dist.size()-1 do
      for e in g.edges() do
        if dist.lookup(e.source) + e.weight < dist.lookup(e.target) do
          dist.update(e.target, dist.lookup(e.source) + e.weight)
          pred.update(e.target, e.source)
    return dist, pred
\end{lstlisting}

\begin{itemize}
  \item[a)] Geben Sie eine möglichst enge obere Schranke für die Laufzeit dieses Algorithmus in \bigO-Notation an.

  Laufzeit: \textfield{100pt}
  \item[b)] Warum wird die äußere Schleife (Zeile 7) genau $|V|-1$ mal durchlaufen? Erklären Sie, warum weder weniger noch mehr Durchläufe sinnvoll wären.

  \textarea{100pt}
  \item[c)] Wie verhält sich der Bellman-Ford Algorithmus bei negativen Zyklen? Geben Sie an, ob der Algorithmus terminiert und ob er das korrekte Ergebnis liefert. Erläutern Sie ihre Antworten.

  \textarea{100pt}
\end{itemize}
\end{nobreakarea}

\begin{nobreakarea}
\begin{itemize}
  \item[d)] Man kann mit den Distanz-Informationen \code{dist}, die der Bellman-Ford-Algorithmus berechnet, erkennen, ob es negative Kanten in einem zusammenhängenden Graphen gibt. Beschreiben Sie dazu den folgenden Algorithmus in Pseudocode:

  \begin{solutionarea}
    \begin{lstlisting}
      algorithm hasNegativeWeights(
        g: WeightedGraph<E>,
        dist: HashTable<WGNode<E>, real>
      ): boolean
    \end{lstlisting}

    \vspace{100pt}
  \end{solutionarea}
  \item[e)] Welches Problem tritt bei unzusammenhängenden Graphen auf, wenn man mit dem Algorithmus aus d) auf Basis der Bellman-Ford-Distanzen bestimmen will, ob der Graph einen negativen Zyklus hat? Erklären Sie das Problem und skizzieren Sie eine mögliche Lösung (in Worten, nicht in Pseudocode).

  \textarea{100pt}
\end{itemize}
\end{nobreakarea}

\begin{nobreakarea}
% Zeit: 5 min
\points{18}
\section{Laufzeittabelle}

Vervollständigen Sie die folgende Tabelle gängiger Effizienzklassen:

\renewcommand{\arraystretch}{2}
\begin{tabular}{l  l  p{3.5cm}  >{\raggedright\arraybackslash}p{6cm}}
  Klasse & Name & Ungefähre Dauer für $n = 1000$ bei 1 GHz & Beispiele\\
  \hline
  $\mathcal{O}(1)$ & konstant & 1 ns & Lookup in Hashtabelle\\
  \textfield{50pt} & \textfield{65pt} & \textfield{3.5cm} & binäre Suche\\
  $\mathcal{O}(n)$ & \textfield{65pt} & \textfield{3.5cm} & \textfield{6cm}\\
  \textfield{50pt} & log-linear & \textfield{3.5cm} & \textfield{6cm}\\
  $\mathcal{O}(n^2)$ & \textfield{65pt} & \textfield{3.5cm} & \textfield{6cm}\\
  \textfield{50pt} & kubisch & \textfield{3.5cm} & dreifach verschachtelte Schleife\\
  \textfield{50pt} & \textfield{65pt} & $3,4 \cdot 10^{284}$ Jahre & alle möglichen Belegungen von $n$ Bits prüfen\\
  \textfield{50pt} & faktoriell & $1,3 \cdot 10^{2567}$ Jahre & \textfield{6cm}\\
\end{tabular}

\begin{hinweis}
  \begin{itemize}
    \item $\SI{1000}{\nano\second} = \SI{1}{\micro\second}$, $\SI{1000}{\micro\second} = \SI{1}{\milli\second}$ und $\SI{1000}{\milli\second} = \SI{1}{\second}$
    \item Nehmen Sie bei Logarithmen immer die Basis 2 an.
  \end{itemize}
\end{hinweis}

\end{nobreakarea}

\begin{nobreakarea}
% Zeit: 4 min - 9 min
\points{27}
\section{Potenzliste}

Der folgende Algorithmus erzeugt eine Liste von absteigenden Zweierpotenzen ($2^n, 2^{n-1}, 2^{n-2}, ... , 2^0$).

\begin{lstlisting}
  algorithm pow2desc(n: int)
    lst := new ArrayList<int>
    v := 1
    for i := 1,2,...,n+1 do
      lst.insert(v, 0)
      v := v * 2
    return lst
\end{lstlisting}

\begin{itemize}
  \item[a)] Welche Laufzeit hat der Algorithmus, je nach dem ob in Zeile 1 eine \code{LinkedList<E>} oder eine \code{ArrayList<E>} verwendet wird. Geben Sie jeweils eine möglichst enge obere Schranke in \bigO-Notation an.

  \renewcommand{\arraystretch}{1.3}
  \begin{tabular}{ll}
    Laufzeit mit \code{ArrayList<E>} & \textfield{100pt}\\
    Laufzeit mit \code{LinkedList<E>} & \textfield{100pt}
  \end{tabular}
  % TODO Lieber: wie könnte man den algorithmus so verändern, dass er auch für die schlechtere Datenstruktur in der besseren klasse ist (beides zusammen ist schwer zu erreichen)
  \item[b)] Wie könnte man den Algorithmus so verändern, dass er für beide Datenstrukturen in der gleichen Effizienzklasse liegt?
  Beschreiben Sie eine solche Änderung als Pseudocode.
  Die resultierende Effizienzklasse soll dabei natürlich die bessere der beiden oben genannten sein. Wenn Sie eine zusätzliche Liste definieren, muss diese immer vom gleichen Typ wie die Liste in Zeile 7 sein.

  \textarea{150pt}
\end{itemize}
\end{nobreakarea}

\begin{nobreakarea}
% Zeit: 6 min (geschätzt)
\points{18}
\section{Limit-Operation für Streams}

\begin{itemize}
  \item[a)] Beschreiben Sie einen \code{LimitingIterator<E>} in Pseudocode, der einen übergebenen \code{Iterator<E>} auf die ersten \code{limit} Elemente begrenzt. Wenn der übergebene Iterator weniger als \code{limit} Elemente liefert, soll auch der \code{LimitingIterator<E>} früher abbrechen. Verwenden Sie dazu das folgende Codegerüst:

  \begin{solutionarea}
    \begin{lstlisting}[gobble=6]
      structure LimitingIterator<E>
    \end{lstlisting}

    \vspace{50pt}

    \begin{lstlisting}[gobble=6]
        operation new(this, base: Iterator<E>, limit: int)
    \end{lstlisting}

    \vspace{50pt}

    \begin{lstlisting}[gobble=6]
        operation next(this): E
    \end{lstlisting}

    \vspace{50pt}

    \begin{lstlisting}[gobble=6]
        operation hasNext(this): boolean
    \end{lstlisting}

    \vspace{50pt}

  \end{solutionarea}
\end{itemize}
\end{nobreakarea}
\begin{nobreakarea}
\begin{itemize}
  \item[b)] Im folgenden Pseudocode sei \code{natGen} ein Algorithmus, der einen unendlichen Stream von allen natürlichen Zahlen generiert. Errechnen Sie aus diesem Stream die Summe der ersten zehn natürlichen Zahlen, die durch sieben aber nicht durch drei teilbar sind. Benutzen Sie dazu ausschließlich die folgenden Stream-Operationen:
  
  \begin{itemize}
    \item \code{limit(this: Stream<E>, limit: int): Stream<E>} (begrenzt den Stream mit einem \code{LimitingIterator<E>} auf höchstens \code{limit} Elemente)
    \item \code{filter(this: Stream<E>, p: Predicate<E>): Stream<E>} (behält nur die Elemente im Stream, für die \code{p} wahr ist)
    \item \code{reduce(this: Stream<E>, a: Accumulator<X,E>, identity: X): X} (fügt Elemente des Streams durch wiederholtes Anwenden von \code{a} zu einem Ergebnis zusammen)
  \end{itemize}

  \begin{solutionarea}
    \begin{lstlisting}
    st := natGen()
    \end{lstlisting}

    \vspace{100pt}
  \end{solutionarea}
\end{itemize}

\begin{hinweis}
  \raggedright
  Sie dürfen Implementierungen von \code{Predicate<E>} und \code{Accumulator<X,E>} auch als Lambda-Ausdrücke der Form \code{(Parameterliste) -> Anweisung} schreiben.
\end{hinweis}
\end{nobreakarea}

\begin{nobreakarea}
\points{16}
\section{Identitätsstiftende Invarianten}

Betrachten Sie die folgende Datenstruktur:

\begin{lstlisting}
  structure Something<E>
    components
      value: E
      others: List<Something<E>>
\end{lstlisting}

Wie der Name schon sagt, kann ein \code{Something<E>} verschiedene abstrakte Datentypen realisieren. Der Einzige Unterschied zwischen diesen besteht darin, welche Klasseninvarianten man für die Datenstruktur auswählt.

Im folgenden sei $D$ als die Menge der \enquote{Nachkommen} eines \code{Something<E>} definiert:

\begin{align*}
  D(n) & \subseteq n.others\\
  D(n) & \subseteq D(o) \;\; \text{für alle } o \in n.others
\end{align*}

Außerdem verwenden wir in den Invarianten $this$ um das Objekt selbst zu bezeichnen.

Ordnen Sie den folgenden Gruppen von Invarianten jeweils den Namen eines abstrakten Datentyps zu, der durch die Sicherstellung dieser Invariantengruppe entsteht.

\end{nobreakarea}

\begin{nobreakarea}
\begin{itemize}\itemsep10pt
  \item[Gruppe 1]
  \begin{itemize}
    \item[IV1] $this \notin D(this)$
    \item[IV2] $others.size() = 1$
  \end{itemize}

  \item[Gruppe 2]
  \begin{itemize}
    \item[IV1] wie Gruppe 1
    \item[IV2] $\forall n \in D(this)\colon \exists_{=1} m \in D(this)\colon n \in m.others$
  \end{itemize}

  \item[Gruppe 3]
  \begin{itemize}
    \item[IV1] wie Gruppe 2
    \item[IV2] wie Gruppe 2
    \item[IV3] $others.size() = 2$
  \end{itemize}

  \item[Gruppe 4]
  \begin{itemize}
    \item[IV1] wie Gruppe 3
    \item[IV2] wie Gruppe 3
    \item[IV3] wie Gruppe 3
    \item[IV4] $\forall n \in D(others.get(0)) \cup {others.get(0)}\colon: n.value \leq value$
    \item[IV5] $\forall n \in D(others.get(1)) \cup {others.get(1)}\colon: n.value > value$
  \end{itemize}

  \item[Gruppe 5]
  \begin{itemize}
    \item[IV1] wie Gruppe 2
    \item[IV2] wie Gruppe 2
    \item[IV3] $\forall o \in others\colon o.value \leq value$
  \end{itemize}

  \item[Gruppe 6]
  \begin{itemize}
    \item[IV1] $\forall a, b \in D(this) \cup this\colon a \in b.others \Leftrightarrow b \in a.others$
  \end{itemize}

  \item[Gruppe 7]
  \begin{itemize}
    \item[IV1] wie Gruppe 6
    \item[IV2] $others.size() = 2$
    \item[IV2] $others.get(0) = None \lor others.get(0).others.get(1) = this$
    \item[IV3] $others.get(1) = None \lor others.get(1).others.get(0) = this$
    \item[IV4] $this \notin others$
  \end{itemize}

  \item[Gruppe 8] keine Invarianten
\end{itemize}

\renewcommand{\arraystretch}{1.4}
\begin{tabular}{ll}
  Gruppe & Abstrakter Datentyp\\
  1 & \textfield{300pt}\\
  2 & \textfield{300pt}\\
  3 & \textfield{300pt}\\
  4 & \textfield{300pt}\\
  5 & \textfield{300pt}\\
  6 & \textfield{300pt}\\
  7 & \textfield{300pt}\\
  8 & \textfield{300pt}\\
\end{tabular}
\end{nobreakarea}

\end{document}