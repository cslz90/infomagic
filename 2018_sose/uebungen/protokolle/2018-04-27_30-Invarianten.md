# Protokoll für die Übung am 27. bzw. 30.4.2018

## Beweis von Schleifeninvarianten

### Generelles Schema zum Beweisen von Schleifeninvarianten

1. Finde die Variablen, die sich in der Schleife ändern und versehe sie mit einem Index.
    * Beispiel: Schleife ändert Variable x => Variable x<sub>i</sub> ist der Wert der Variable *vor* dem Schleifendurchlauf, x<sub>i+1</sub> ist der Wert der Variable *danach*.
    * Für eine while-Schleife müssen Sie eventuell einen Schleifenzähler i dazudefinieren. Wichtig ist hier, dass Sie angeben, bei welchem Wert dieser startet.
2. Schreibe die richtigen Indices an die Zuweisungen in der Schleife und an die Schleifeninvariante selbst.
    * Beispiel: Schleifeninvariante ist x = i! => x<sub>i</sub> = i! . Gleichzeitig wird z.B. eine Zuweisung `x := x * i` zur Gleichung x<sub>i+1</sub> = x<sub>i</sub> * i.
3. Setze die Startwerte für die Variablen in die Schleifeninvariante ein und löse die entstehende Gleichung auf, bis eine wahre Aussage entsteht.
4. Indunktionsvoraussetzung: Schreibe die Schleifeninvariante auf. Diese zählt jetzt als gegeben.
5. Induktionsbehauptung: Schreibe die Schleifeninvariante noch einmal auf, aber ersetze alle Vorkommnisse von der Zählvariable i (kann auch anderen Namen haben) mit i+1.
6. Versuche die neu entstandene Gleichung aus Schritt 5 wieder aufzulösen, bis eine wahre Aussage entstanden ist.
    * Hierbei helfen jetzt die Gleichungen, die man aus den Anweisungen in der Schleife generiert hat, da sie die Variablen an denen ein i+1 als Index steht mit denen verbinden, an denen ein i als Index steht.
    * Außerdem wird meistens an irgendeiner Stelle die Induktionsvoraussetzung benötigt.

### Form

* Stellen Sie sich einen Beweis immer als eine Erzählung vor.
    Genauso wie der Leser Ihnen folgen muss, wenn Sie den Hergang eines Kriminalfalls beschreiben oder wenn Sie einen erklärenden Text zu einem Fachthema verfassen, muss auch ihre mathematische Herleitung in sich schlüssig sein.

    Dafür gibt es keine festen Regeln, sonden Sie müssen selbst überlegen, welches Wissen sie voraussetzen können und was sie noch einmal genauer beschreiben müssen.

    Generell gilt: Je "trivialer" die Hauptaussage erscheint, die Sie Beweisen wollen, desto mehr müssen Sie ins Detail gehen.
* Beweise müssen mathematisch erfolgen (vollständige Induktion, Entwicklung von Gleichungen).

    Wenn die mathematischen Schritte selbsterklärend sind, reicht es, die Formel hinzuschreiben.
    Oft hilft es aber, mit einem kurzen Satz oder Halbsatz zu erwähnen, was man in dem jeweiligen Schritt tut und woher man die Information nimmt, um die Gleichungen aufzulösen.
* In Gildamesh kann man Indices wie x<sub>i+1</sub> entweder als <code>x&lt;sub&gt;i+1&lt;/sub&gt;</code> oder einfach als x_(i+1) schreiben.

### Beispiel: Fakultätsfunktion

```cs-pseudo
algorithm fac(n: int)
  res := 1
  for i := 1,2,... n do
    res := res * i
  return res
```

Für unsere mathematischen Ausdrücke verwenden wir die Bezeichnung res<sub>k</sub> für den Wert der Variable `res` *vor* dem Schleifendurchlauf für `i = k`. D.h. die Anweisung `res := res * i` entspricht der mathematischen Gleichung  res<sub>i+1</sub> = res<sub>i</sub> &sdot; i.

Schleifeninvariante: res<sub>i</sub> = (i-1)!

Beweis der Schleifeninvariante:

* IA: res<sub>1</sub> = 0! = 1
  * korrekt :heavy_check_mark:🌻
* IS:
  * Annahme: res<sub>i</sub> = (i-1)!
    * Behauptung: res<sub>i+1</sub> = i!
    * Beweis:
      * res<sub>i+1</sub> = res<sub>i</sub> &sdot; i &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(wegen `res := res * i`)
      * res<sub>i+1</sub> = (i-1)! &sdot; i = i! &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(wegen Induktionsannahme)
      * => korrekt :heavy_check_mark:🌻

Vollständiger Korrektheitsbeweis:

* Schleifeninvariante: res<sub>i</sub> = (i-1)! für alle i
* Rückgabewert: res<sub>n+1</sub> (nach dem n-ten Durchlauf der Schleife)
  * res<sub>n+1</sub> = n!
  * => fac(n) = n! :heavy_check_mark:🌻

### Beispiel: GGT nach Euklid

```cs-pseudo
algorithm ggtEuclid(a: int, b: int)
  if a = 0 then
    return a
  while b != 0 do
    if a > b then
      a := a - b
    else
      b := b - a
  return a
```

Da wir es hier mit einer Whileschleife zu tun haben, müssen wir eine Zählvariable definieren.
Diese nennen wir i und lassen sie bei 0 vor dem ersten Durchlauf starten.
Nach jedem Durchlauf der Whileschleife wird i um 1 erhöht.

Schleifeninvariante: ggt(a<sub>i</sub>, a<sub>i</sub>) = ggt(a<sub>0</sub>, b<sub>0</sub>)

(Damit ist gemeint, dass der GGT von a und b sich nach dem Schleifendurchlauf nicht geändert hat, auch wenn a und b sich ändern.)

Beweis der Schleifeninvarianten:

* IA: ggt(a<sub>0</sub>, b<sub>0</sub>) = ggt(a<sub>0</sub>, b<sub>0</sub>) ✔️🌻
  * In diesem Fall ist die Schleifeninvariante schon nach dem Einsetzen von i = 0 eine wahre Aussage.
* IS:
  * Annahme: ggt(a<sub>i</sub>, b<sub>i</sub>) = ggt(a<sub>0</sub>, b<sub>0</sub>)
  * Behauptung: ggt(a<sub>i+1</sub>, b<sub>i+1</sub>) = ggt(a<sub>0</sub>, b<sub>0</sub>)
  * Beweis:
    * Fallunterscheidung:
      * a<sub>i</sub> > b<sub>i</sub>
        * ggt(a<sub>i+1</sub>, b<sub>i+1</sub>) = ggt(a<sub>i</sub> - b<sub>i</sub>, b<sub>i</sub>) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        * (weil `a := a - b` und b sich nicht verändert)
        * Es sei k = ggt(a<sub>i</sub>, b<sub>i</sub>)
        * Da a<sub>i</sub> mod k = 0 und b<sub>i</sub> mod k = 0, gilt auch (a<sub>i</sub> - b<sub>i</sub>) mod k = 0.
        * Das heißt k ist ein Teiler von a<sub>i</sub> - b<sub>i</sub>.
        * Um zu zeigen, dass k nicht nur irgendeiner sondern auch der größte Teiler von a<sub>i</sub> - b<sub>i</sub> und b<sub>i</sub> ist, nehmen wir an es gäbe einen größeren gemeinsamen Teiler k' > k:
          * Es muss also wieder gelten b<sub>i</sub> mod k' = 0 und (a<sub>i</sub> - b<sub>i</sub>) mod k' = 0
          * Falls a<sub>i</sub> mod k' = 0, dann teilt k' > k sowohl a<sub>i</sub> als auch b<sub>i</sub>)
          * => Widerspruch, da k = ggt(a<sub>i</sub>, b<sub>i</sub>)
          * Falls a<sub>i</sub> mod k' = c mit c > 0 => (a<sub>i</sub> - b<sub>i</sub>) mod k' = c
          * => Widerspruch zur Annahme (a<sub>i</sub> - b<sub>i</sub>) mod k' = 0
          * => Es gibt kein k', was die Bedingungen erfüllt
          * => k = ggt(a<sub>i</sub> - b<sub>i</sub>, b<sub>i</sub>)
        * => ggt(a<sub>i+1</sub>, b<sub>i+1</sub>) = ggt(a<sub>i</sub> - b<sub>i</sub>, b<sub>i</sub>) = k = ggt(a<sub>i</sub>, b<sub>i</sub>)
        * nach Annahme gilt ggt(a<sub>i</sub>, b<sub>i</sub>) = ggt(a<sub>0</sub>, b<sub>0</sub>)
        * => ggt(a<sub>i+1</sub>, b<sub>i+1</sub>) = ggt(a<sub>0</sub>, b<sub>0</sub>) ✔️🌻
      * a<sub>i</sub> <= b<sub>i</sub>
        * ggt(a<sub>i+1</sub>, b<sub>i+1</sub>) = ggt(a<sub>i</sub>, b<sub>i</sub> - a<sub>i</sub>)
        * (weil `b := b - a` und a sich nicht verändert)
        * Rest des beweises verläuft analog zu a > b, weil ggt(a,b) = ggt(b,a). ✔️🌻

Für den vollständigen Korrektheitsbeweis fehlen noch zwei Dinge:

* Die Schleife muss bei irgendeinem i = t enden. (Terminierungsbeweis)
* Der Rückgabewert a<sub>t</sub> muss der korrekte GGT (also der GGT vom Ursprünglichen a<sub>0</sub> und b<sub>0</sub>) sein.
  * Das gilt, weil ggt(a<sub>t</sub>, b<sub>t</sub>) = ggt(a<sub>t</sub>,0) = a<sub>t</sub> und ggt(a<sub>t</sub>,b<sub>t</sub>) = ggt(a<sub>0</sub>,b<sub>0</sub>)

### Beispiel: Binäre Exponentiation

Die [binäre Exponentiation](https://de.wikipedia.org/wiki/Bin%C3%A4re_Exponentiation) ist ein Algorithmus, dem man wie dem euklidischen Algorithmus seine Korrektheit nicht auf den ersten Blick ansieht.
Anders als beim euklidischen Algorithmus, dessen Beweis etwas mathematiklastig ist, läuft aber hier die Beweisführung zu gleichen Teilen über mathematische Regeln und Codezeilen.

```cs-pseudo
// calculates x^k
algorithm binExp(x: int, k: int)
  res := 1
  aktpot := x
  while k > 0 do
    if k mod 2 == 1
      res := res * aktpot
    aktpot := aktpot * aktpot
    k := k / 2
```

Auch hier haben wir wieder eine Whileschleife, müssen also einen eigenen Zähler i einführen, der vor dem ersten Durchlauf bei 0 startet.

Schleifeninvariante: res<sub>i</sub> = <sub>m=0</sub>Π<sup>i-1</sup> x<sup>c<sub>m</sub>2<sup>m</sup></sup> wobei c<sub>m</sub> = k<sub>m</sub> mod 2.

Dabei ist das Π ein Produkt (von m = 0 bis m = i-1) und lässt sich wie folgt aussschreiben:

<sub>m=0</sub>Π<sup>i-1</sup> x<sup>c<sub>m</sub>2<sup>m</sup></sup> = 
x<sup>c<sub>0</sub>2<sup>0</sup></sup>
x<sup>c<sub>1</sub>2<sup>1</sup></sup>
x<sup>c<sub>2</sub>2<sup>2</sup></sup>
...
x<sup>c<sub>i-1</sub>2<sup>i-1</sup></sup> =
x<sup>1c<sub>0</sub></sup>
x<sup>2c<sub>1</sub></sup>
x<sup>4c<sub>2</sub></sup>
x<sup>8c<sub>3</sub></sup>
...
x<sup>c<sub>i-1</sub>2<sup>i-1</sup></sup>

Die Idee dieser Schleifeninvariante drückt die Idee des Algorithmus aus.
Dieser stellt nämlich ein beliebiges x<sup>k</sup> mit Hilfe der Binärdarstellung von k als Produkt dar. Ein Beispiel wäre das folgende:

x<sup>26</sup> = x<sup>0·1</sup> x<sup>1·2</sup> x<sup>0·4</sup> x<sup>1·8</sup> x<sup>1·16</sup> = x<sup>0·1 + 1·2 + 0·4 + 1·8+ 1·16</sup>

Nun aber wieder zurück zum Algorithmus und unserer Schleifeninvariante.

Für diesen Beweis brauchen wir zunächst noch eine zweite (einfachere) Schleifeninvariante:

aktpot<sub>i</sub> = x<sup>(2<sup>i</sup>)</sup>

IA(aktpot):

Wir müssen beweisen, dass

aktpot<sub>0</sub> = x<sup>(2<sup>0</sup>)</sup> = x<sup>1</sup> = x

Das gilt, da die Anweisung `aktpot := x` vor dem ersten Schleifendurchlauf ausgeführt wird. ✔️🌻

IS(aktpot):

* Annahme: aktpot<sub>i</sub> = x<sup>(2<sup>i</sup>)</sup>
* Behauptung: aktpot<sub>i+1</sub> = x<sup>(2<sup>i+1</sup>)</sup>

Wegen `aktpot := aktpot * aktpot` gilt aktpot<sub>i+1</sub> = aktpot<sub>i</sub><sup>2</sup>.
Durch Einsetzen der Annahme erhalten wir aktpot<sub>i+1</sub> = aktpot<sub>i</sub><sup>2</sup> =  (x<sup>(2<sup>i</sup>)</sup>)<sup>2</sup> = x<sup>2·(2<sup>i</sup>)</sup> = x<sup>(2<sup>i+1</sup>) ✔

=> Die Invariante aktpot<sub>i</sub> = x<sup>(2<sup>i</sup>)</sup> gilt für alle i >= 0

Nun zurück zu unserer "Hauptinvariante":

res<sub>i</sub> = <sub>m=0</sub>Π<sup>i-1</sup> x<sup>c<sub>m</sub>2<sup>m</sup></sup> = 
x<sup>c<sub>0</sub>2<sup>0</sup></sup>
x<sup>c<sub>1</sub>2<sup>1</sup></sup>
x<sup>c<sub>2</sub>2<sup>2</sup></sup>
...
x<sup>c<sub>i-1</sub>2<sup>i-1</sup></sup>

IA(Hauptinvariante):

<sub>m=0</sub>Π<sup>-1</sup> x<sup>c<sub>m</sub>2<sup>m</sup></sup> = 1

Hier erhalten wir ein leeres Produkt (da es keine Zahl m gibt für die gilt m >= 0 und m <= -1).
Per Definition ergibt das das neutrale Element der Multiplikation, also 1.

=> Die Invariante gilt für i = 0. ✔️🌻

IS(Hauptinvariante):

* Annahme: res<sub>i</sub> = <sub>m=0</sub>Π<sup>i-1</sup> x<sup>c<sub>m</sub>2<sup>m</sup></sup>
* Behauptung: res<sub>i+1</sub> = <sub>m=0</sub>Π<sup>i</sup> x<sup>c<sub>m</sub>2<sup>m</sup></sup>

Da die Whileschleife eine If-Abfrage enthält müssen wir eine Fallunterscheidung machen:

* k<sub>i</sub> mod 2 = 0

    In diesem Fall ist res<sub>i+1</sub> = res<sub>i</sub>, da die Variable `res` nicht verändert wird.

    Außerdem gilt <sub>m=0</sub>Π<sup>i</sup> x<sup>c<sub>m</sub>2<sup>m</sup></sup> = <sub>m=0</sub>Π<sup>i-1</sup> x<sup>c<sub>m</sub>2<sup>m</sup></sup>, wegen k<sub>i</sub> mod 2 = c<sub>i</sub> = 0.

    Wir haben also insgesamt:

    res<sub>i+1</sub> = res<sub>i</sub> = <sub>m=0</sub>Π<sup>i-1</sup> x<sup>c<sub>m</sub>2<sup>m</sup></sup> = <sub>m=0</sub>Π<sup>i</sup> x<sup>c<sub>m</sub>2<sup>m</sup></sup> ✔️🌻
* k<sub>i</sub> mod 2 = 1

    In diesem Fall gilt wegen `res := res * aktpot`:

    res<sub>i+1</sub> = res<sub>i</sub> · aktpot<sub>i</sub>

    Setzen wir nun unsere Invariante für aktpot ein, erhalten wir

    res<sub>i+1</sub> = res<sub>i</sub> · x<sup>(2<sup>i</sup>)</sup>

    Nun ersetzen wir res<sub>i</sub> mit der Induktionsannahme:

    res<sub>i+1</sub> = (<sub>m=0</sub>Π<sup>i-1</sup> x<sup>c<sub>m</sub>2<sup>m</sup></sup>) · x<sup>(2<sup>i</sup>)</sup>

    Da aber k<sub>i</sub> mod 2 = c<sub>i</sub> = 1 gilt, können wir das auch schreiben als

    res<sub>i+1</sub> = (<sub>m=0</sub>Π<sup>i-1</sup> x<sup>c<sub>m</sub>2<sup>m</sup></sup>) · x<sup>c<sub>i</sub>(2<sup>i</sup>)</sup> = <sub>m=0</sub>Π<sup>i-1</sup> x<sup>c<sub>m</sub>2<sup>m</sup></sup> ✔️🌻

Damit ist die Invariante für alle i >= 0 bewiesen.

Für einen vollständigen Korrektheitsbeweis fehlt hier noch:

* Der Beweis, dass die Schleife bei einem i = t terminiert.
* Der Beweis, dass c<sub>m</sub> tatsächlich das m-te Bit (vom niedrigwertigsten Bit an gezählt) von k<sub>0</sub> ist.
* Der Beweis, dass <sub>m=0</sub>Π<sup>t-1</sup> x<sup>c<sub>m</sub>2<sup>m</sup></sup> = x<sup>k<sub>0</sub></sup> ist.