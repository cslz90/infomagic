# Protokoll für die Übung am 8. bzw. 11.6.2018

## Beweis von Klasseninvarianten

### Generelles Schema zum Beweis von Klasseninvarianten

1. Beweise, dass die Invariante *nach* jedem Konstruktoraufruf gilt.
    In der Regel ist das trivial, wenn man einfach aufschreibt, welchen Wert die Variablen am Ende des Aufrufs haben.
    Manchmal muss man dabei aber aufpassen, dass keine Referenzen auf Objekte gespeichert werden, die als Parameter an den Konstruktor übergeben wurden.
    Das könnte nämlich dazu führen, dass die Invariante verletzt wird durch die Veränderung eben dieses Objektes in dem Code, der auch den Konstruktor aufgerufen hat.
2. Bestimme die Variablen, die in der Invariante eine Rolle spielen.
    Die Invariante `size <= content.length` in einer Array-Liste könnte zum Beispiel nur dann verletzt werden, wenn die Variablen `size` oder `content` verändert werden.
3. Identifiziere die Methoden, die keine der Variablen aus 2. verändern.
4. Führe für die Methoden aus 3. einen einfachen Beweis, in dem du plausibel erklärst, warum hier keine der relevanten Variablen verändert werden kann (falls das nicht schon offensichtlich ist).
    Danach reicht ein kurzer Satz wie "Da die Variablen `x`, `y` und `z` nicht verändert werden, muss die Invariante nach dem Aufruf noch gelten, wenn sie vorher gegolten hat.", um den Beweis für diese Methoden zu vollenden.
5. Jetzt geht es nur noch um die Methoden, die mindestens eine der Variablen verändern, die für die Invariante relevant sind.
    Führe für jede dieser Methoden einen Beweis nach dem folgenden Schema:

    1. Nimm an, dass die Invariante vor der Methode gegolten hat.
    2. Wenn die Methode nur eine einfache Reihenfolge von Anweisungen enthält, stelle direkt eine Gleichung für den neuen Wert der Variable(n) auf und stelle diese mit Hilfe der Annahme so um, dass wieder die Aussage der Invariante herauskommt.
    3. Wenn die Methode eine Schleife enthält oder rekursiv formuliert ist, führe den Beweis am Besten mit einer vollständigen bzw. strukturellen Induktion.

### Form

* Falls es nötig ist, zwischen dem Wert vor und nach der Anwendung einer Methode zu unterscheiden, bietet es sich an einen Strich an die Variable zu setzen für den Wert nach der Anwendung. `x` wäre also der Wert der Variable vor dem Methodenaufruf und `x'` der Wert danach.
* Ansonsten gelten alle Regeln, die auch für Schleifeninvarianten gelten.

### Beispiel: Doppelt verkettete Liste

Für die doppelt verkettete Liste, die wir in der Vorlesung kennengelernt haben, lassen sich die folgenden Invarianten aufstellen:

* IV1: $\forall \: n \in L, n.next \neq None: n.next.prev = n$
  * Stellt sicher, dass man beim gleichen Element ankommt, wenn man einmal vor und dann wieder zurück geht. L ist dabei die Menge der Knoten in der Liste.
* IV2: $\forall \: n \in L, n.prev \neq None: n.prev.next = n$
  * Das gleiche wie IV1, nur dass man diesesmal einen Schritt zurück und dann einen vor geht.
* IV3: $first = None \lor first.prev = None$
  * Stellt sicher, dass es kein Element vor first geben kann.
* IV4: $last = None \lor last.next = None$
  * Stellt sicher, dass es kein Element nach last geben kann.
* IV5: $last = first \lor first.next.next.next. ... .next = last$
  * Stellt sicher, dass first und last Teil von der gleichen verketteten Struktur sind.

Wir betrachten dabei den folgenden Konstruktor

```D
operation new(this: DoublyLinkedList<E>)
    this.size := 0
    this.first := None
    this.last := None
```

und die folgenden Methoden:

```D
operation get(this: DoublyLinkedList<E>, idx: int): E
    node := this.getNode(idx)
    return node.value

operation set(this: DoublyLinkedList<E>, el: E, idx: int)
    node := this.getNode(idx)
    node.value := el

operation size(this: DoublyLinkedList<E>): int
    return this.size

operation add(this: DoublyLinkedList<E>, el: E)
    newNode := LinkedNode<E>.new(el)
    if this.size = 0 then
        this.first := newNode
    else
        link(this.last, newNode)
    this.last := newNode
    this.size := this.size + 1

operation remove(this: DoublyLinkedList<E>, idx: int)
    assert this.hasIndex(idx)
    currentNode := this.getNode(idx)
    if this.size = 1 then
        this.first = None
        this.last = None
    else if currentNode.prev = None then
        this.first := currentNode.next
        if this.first != None then
            this.first.prev := None
    else if currentNode.next = None then
        this.last := currentNode.prev
        if this.last != None then
            this.last.next := None
    else
        prevNode := currentNode.prev
        nextNode := currentNode.next
        link(prevNode, nextNode)
    this.size := this.size - 1

operation insert(this: DoublyLinkedList<E>, el: E, idx: int)
    assert this.hasIndex(idx) or idx = this.size
    if idx = this.size then
        this.add(el)
        return
    newNode := LinkedNode<E>.new(el)
    currentNode := this.getNode(idx)
    prevNode := currentNode.prev
    link(newNode, currentNode)
    link(prevNode, newNode)
    if idx = 0 then
        this.first := newNode
    this.size := this.size + 1
```

Wir gehen dabei davon aus, dass die Hilfsoperationen wie `link` oder `getNode` nur aus dem oben genannten Code aufgerufen werden, keinesfalls aber von außerhalb. In Java könnte man das z.B. mit dem Schlüsselwort `private` sicherstellen und das ist auch nötig, da sonst durch `getNode` ein innerer Knoten nach außen preisgegeben wird, mit dem man die Invarianten leicht verletzen kann.

#### Beweis von IV1

1. Zeige, dass IV1 nach dem Aufruf von `new(this)` gilt:

    Nach dem Aufruf gilt $L = \emptyset$. Es gibt also keinen Knoten, der die Invariante verletzen könnte. ✔️🌼
2. Die Methoden `get`, `set` und `size` ändern weder `x.prev` noch `x.next` für irgendein `x`. Wenn die IV1 vor dem Aufruf einer dieser Methoden gilt, muss sie also auch noch nachher gelten. ✔️🌼
3. Die Methode `add` ändert `prev` und `next` mit Hilfe des Hilfsalgorithmus `link`. Wir zeigen also einfach, dass `link` immer unbedenklich ist:

    ```D
    algorithm link(left: LinkedNode<E>, right: LinkedNode<E>)
        if left != None then
            left.next := right
        if right != None then
            right.prev := left
    ```

    Fallunterscheidung:
    * $left = None \land right = None$

        In diesem Fall gibt es nichts zu beweisen, weil keine der beiden modifizierenden Codezeilen ausgeführt wird. ✔️🌼
    * $left = None \land right \neq None$

        Es wird nur `right.prev := left` ausgeführt. Da aber $left = None$ ist right nicht mehr in der Menge enthalten, für die wir etwas Beweisen müssen. IV1 muss also immer noch gelten, da sie für alle anderen fraglichen Knoten auch vorher galt. ✔️🌼
    * $left \neq None \land right = None$

        Wir wenden die gleiche Argumentation wie im vorherigen Fall analog an. ✔️🌼
    * $left \neq None \land right \neq None$

        Es gilt $left.next = right$ und $right.prev = left$, also ist $left.next.prev = right.prev = left$ ✔️🌼

        Es kann ein Element `x` geben, für das vorher $x.next = right$ galt.
        Falls $x \neq left$ würde jetzt also gelten $x.next.prev = right.prev = left \neq x$.
        Das würde IV1 verletzen.

        Wir können aber zeigen, dass das nicht der Fall ist, da `x` "links" von right aber "rechts" von left liegen müsste.
        Nach der Definition der doppelt verketteten Liste (siehe IV5) kann so ein Element aber nicht mehr Teil einer Liste sein, die bei `first` beginnt und bei `last` endet.
        Daraus folgt $x \neq L$ und damit gilt IV1 auch in diesem Fall noch. ✔️🌼
    * Das sind alle möglichen Fälle $\Rightarrow$ IV1 gilt für `link`.

    $\Rightarrow$ IV1 gilt für `add`.
4. Die Methode `insert` verwendet ebenfalls nur `link` zum Verändern der `prev`- und `next`-Links.

    Da IV1 nach jedem Aufruf von `link` gilt, gilt sie also auch nach jedem Aufruf von `insert`. ✔️🌼
5. Die Methode `remove` verwendet zum einen `link`, setzt aber auch `last.next := None` falls vom Ende der Liste entfernt wird.

    Die Unbedenklichkeit von `link` haben wir schon bewiesen und $last.next = None$ bedeutet, dass wir IV1 für $last$ gar nicht prüfen müssen. ✔️🌼

Anmerkung: Die Beweisführung zeigt genau den Grund auf, warum `link` als Hilfsalgorithmus bei der Definition der Datenstruktur ausgelagert wurde.
Es handelt sich dabei um ein kritisches Stück Code, was leicht zu Problemen führen kann. Wenn man es auf die wesentlichen Codezeilen herunterbricht und es einmal ordentlich definiert, muss man sich aber über die weitere Verwendung davon keine Gedanken mehr machen.

#### Beweis von IV2

Der Beweis verläuft analog zu dem Beweis für IV1, nur dass hier jeweils die "Rückrichtung" der Verkettung gezeigt werden muss. ✔️🌼

#### Beweis von IV3

1. Zeige, dass IV3 nach dem Aufruf von `new(this)` gilt:

    Nach dem Aufruf ist $first = None$ $\Rightarrow$ IV3 gilt. ✔️🌼
2. Auch hier können die Methoden `get`, `set` und `size` weder `first` noch `first.next` ändern. ✔️🌼
3. Da `add` immer hinten anfügt, kann `first` nur verändert werden, wenn vorher keine Elemente in der Liste waren, also wenn $this.size = 0$.

    In diesem Fall gilt dank der Anweisungen der Methode $first = newNode$ und, da $newNode$ neu erzeugt wurde, $first.prev = None$.
    Damit ist die Bedingung von IV3 hergestellt.
4. `remove` prüft explizit ab, ob ein Element `currentNode` gelöscht wird, für das $currentNode.prev = None$ gilt.
    Eine Veränderung von `first` kann also nur in diesem Fall geschehen.

    In diesem Fall ist entweder $first = None$ oder `first.prev` wird auf `None` gesetzt.
    Das stellt genau die Bedingung von IV3 her. ✔️🌼
5. `insert` kann `first` nur verändern, falls $idx = 0$ oder $idx = 1$.

    * Falls $idx = 0$ gilt nach dem Aufruf $first' = newNode$.
        Die Verknüpfungen dieses Knotens wurden mit zwei `link`-Aufrufen verändert.

        Der erste ist von der Form `link(first',first)`.
        Hier taucht `first'` nur auf der linken Seite als `left` auf. `first'.prev` wird daher nicht verändert.

        Der zweite ist von der Form `link(None, first')`.
        Dadurch wird $first'.prev = None$ gesetzt und die Bedingung von IV3 hergestellt. ✔️🌼

#### Beweis von IV4

1. Zeige, dass IV4 nach dem Aufruf von `new(this)` gilt:

    Nach dem Aufruf ist $last = None$ $\Rightarrow$ IV4 gilt. ✔️🌼
2. Auch hier können die Methoden `get`, `set` und `size` weder `last` noch `last.next` ändern. ✔️🌼
3. `add` setzt `last` immer auf einen gerade neu erzeugten Knoten `newNode`.
    Dieser wird zwar auch in einem Aufruf von `link` verändert, aber nur als `right`-Parameter.
    Es gilt also nach dem Aufruf immer noch $last.next = None$. ✔️🌼
4. `remove` prüft explizit ab, ob ein Element `currentNode` gelöscht wird, für das $currentNode.next = None$ gilt.
    Eine Veränderung von `last` kann also nur in diesem Fall geschehen.

    In diesem Fall ist entweder $last = None$ oder `last.next` wird auf `None` gesetzt.
    Das stellt genau die Bedingung von IV4 her. ✔️🌼
5. Es gibt zwei Fälle in denen `insert` die Variable `last` verändert.

    * Wenn ganz am Ende eingefügt wird, gilt $idx = this.size$.
        In diesem Fall ruft `insert` nur `add` auf, für das wir IV4 schon gezeigt haben. ✔️🌼
    * Wenn $currentNode = last$, dann wird `last` auch mit einem Aufruf von `link` verändert, allerdings wieder nur als Parameter `right`. ✔️🌼

    Falls $currentNode \neq last$, kann auch $currentNode.prev = last$ nicht gelten, da ansonsten nach $IV2$ gelten müsste $last.next = currentNode$. Das widerspricht der Annahme, dass $IV4$ vor dem Aufruf der Methode galt.
    Damit gibt es keine weiteren Fälle zu betrachten. ✔️🌼

#### Beweis von IV5

1. Zeige, dass IV5 nach dem Aufruf von `new(this)` gilt:

    Nach dem Aufruf gilt $last = first = None$. ✔️🌼
2. Auch hier können die Methoden `get`, `set` und `size` weder `last` noch `first` noch irgendein `x.next` ändern. ✔️🌼
3. Nach dem Aufruf von `add` gilt $last' = newNode$ und damit $last'.prev = last$.

    Es gilt nun zwei Fälle zu unterscheiden:

    * Falls $last = None$ gilt $last' = first' = newNode$. ✔️🌼
    * Falls $last \neq None$ gilt wegen der Annahme, dass IV5 vor dem Aufruf galt $first.next.next....next = last$ und $last.next = last'$.
        Also gibt es jetzt auch eine Kette $fist.next.next.....next = last'$. ✔️🌼
4. Für den Aufruf von `remove` müssen wir vier Fälle unterscheiden:

    * Falls $size = 1$, dann gilt $first' = last' = None$. ✔️🌼
    * Falls $size > 1 \land currentNode = first$, dann ist $first' = first.next$.
        Da $first.next$ teil der Kette $first.next.next.....next = last$ ist, muss also auch die Kette $first'.next.next....next = last$ existieren. ✔️🌼
    * Falls $size > 1 \land currentNode = last$, dann gilt $last' = last.prev$.
        Analog zum vorherigen Fall muss hier auch die Kette weiterhin bestehenbleiben. ✔️🌼
    * Falls $size > 1 \land currentNode \neq first \land currentNode \neq last$, dann ist der Gelöschte Knoten in der "Mitte" der Verknüpfungskette.
        Ohne Beschränkung der Allgemeinheit sei also $currentNode.prev = l$ und $currentNode.next = r$, so dass $first.next.next.....next = l$ und $r.next.next.....next = last$.
        Da mit `link(l, r)` sichergestellt wird, dass $l.next = r$ gibt es also wieder eine Kette (über l und r), für die gilt $first.next.next.....next = last$. ✔️🌼
5. Für den Aufruf von `insert` müssen wir eine Fallunterscheidung über idx machen:
    * Falls $idx = size$ wird `add` aufgerufen und wir haben schon bewiesen, dass `add` IV5 nicht verletzen kann. ✔️🌼
    * Falls $idx = 0$, ist $currentNode = first$ und $prevNode = None$ (wegen IV3).

        Aufgrund der Annahme muss gelten, dass $first.next.....next = last$ und `link(newNode, currentNode)` stellt sicher, dass $first'.next = newNode.next = currentNode = first$.
        Damit haben wir $first'.next = first$ und es gilt $first'.next.next.next.....next = last$. ✔️🌼
    * Falls $idx \neq 0 \land idx \neq size$ ist $currentNode \neq last$ und $currentNode \neq first$.
        Nach der Annahme, dass IV5 vor dem Aufruf galt muss also gelten $first.next.next.....next = currentNode.prev = prevNode$, $prevNode.next = currentNode$ und $currentNode.next.next.....next = last$.

        Mit `link(newNode, currentNode)` und `link(prevNode, newNode)` wird sichergestellt, dass $prevNode.next = newNode$ und $newNode.next = currentNode$.
        Es muss also gelten, dass $first.next.next.....next = newNode$, $newNode.next = currentNode$ und immer noch $currentNode.next.next....next = last$.

        Damit ist die Verkettung wieder vollständig hergestellt und IV4 gilt. ✔️🌼

Anmerkung: Bei dieser Beweisführung sind mir zwei kleinere Fehler in dem Pseudocode aufgefallen, die ich hiermit korrigiert habe. Eine systematische Betrachtung des Codes hilft also auch in praktischer Hinsicht. 😉