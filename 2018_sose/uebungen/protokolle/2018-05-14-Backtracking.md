# Protokoll für die Übung am 14.5.2018

## Backtracking

Die Backtracking-Aufgabe auf Dozentron ist keine Suche nach *einer* Lösung, wie die Beispiele auf den Folien, sondern eine Optimierungsaufgabe, die die *beste* Lösung fordert.
Daher muss auch der Code ein wenig anders aussehen.

Zusammen haben wir den folgenden Pseudocode für die Aufgabe erarbeitet:

```D
// generates weights to use with back27
algorithm weights27(inp: int)
  // log(1000,10) = 3, log(10000,10) = 4, ...
  zeros := floor(log(inp, 10))
  lst27 := ArrayList<int>.new()
  for w := 7*10^zeros, 2*10^zeros, 7*10^(zeros-1), 2*10^(zeros-1), ..., 2 do
    if w <= inp then
      lst27.add(w)
  return lst27

// balances out inp with the given weights
algorithm back27(inp: int, weights: List<int>)
  if inp = 0 then
    // if our input weight is zero, we do not need any weights
    // => empty list is the correct solution
    return ArrayList<int>.new()
  if inp < 0 then
    // we have no negative weights => problem is unsolvable => track back
    return None

  // start of minimum algorithm: remember current best result
  res := None
  minN := ∞
  for w in weights do
    // solve problem recursively by checking if there is a solution
    // for the remaining weight if we take the weight w as next step
    tmp := back27(inp-w, weights)
    if tmp != None then
      // tmp contains the weights needed to balance out inp-w
      // => we need to add w to balance out inp
      tmp.add(w)
      // Normal backtracking (search) would end here, since tmp is a solution.
      // We continue with the rest of the minimum algorithm instead to
      // find the _optimal_ solution.
      if tmp.size() < minN then
        res := tmp
        minN := tmp.size()
  // if there were no solutions, res is still None and we track back further
  // otherwise res is the optimal solution (due to our minimum algorithm)
  return res
```

### Ideen zur Optimierung

* Das größte Gewicht zuerst auszuprobieren (Gier)
* Keine Pfade erkunden, die Länger sind als die beste bekannte Lösung ([Branch & Bound](https://en.wikipedia.org/wiki/Branch_and_bound))
* Optimale Gewichtsverteilungen für Teilinput speichern und nutzen, falls man noch einmal auf den gleichen Teilinput stößt (Dynamic Programming)
  * voraussichtlich extrem effektiv, wenn man das gleiche Objekt zum Speichern der Teilergebnisse für *alle* auszubalancierenden Gewichte in der Inputliste verwendet