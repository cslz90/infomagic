package fileio;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by ### on 25.04.2017.
 */
public class NovizenFilter {
    private static final String LISTE = "novizen.csv";
    private static final String NEUELISTE = "berechtigte_novizen.csv";

    public static void processList() {
        List<String> personen = fileToList(LISTE);
        List<String> personen2 = new LinkedList<>();
        for(String person : personen){
            int alter = Integer.parseInt(person.split(",")[1].trim());
            if(alter>=18)
                personen2.add(person);
        }

        listToFile(NEUELISTE,personen2);
    }

    private static List<String> fileToList(String filePath) {
        List<String> list = new LinkedList<>();
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(filePath), StandardCharsets.UTF_8)) {
            String line;
            while ((line = reader.readLine()) != null) {
                list.add(line);
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }

        return list;
    }
    private static void listToFile(String filePath,List<String> list) {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(filePath), StandardCharsets.UTF_8)) {
            for(String line : list ) {
                writer.write(line);
                writer.newLine();
            }
            writer.flush();

        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }
}