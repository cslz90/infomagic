package mergesort;

public class MergeSort {
        public static int[] ar; 
        private static int [] helper ; 
        private static int number ; 
    public static void sort(int[] arr) {
        sort(arr, 0, arr.length-1);
    }
  
    private static void sort(int[] arr, int fromIndex, int toIndex) {
        number = arr.length;
        helper = new int[number];
      


        if (fromIndex < toIndex) {
            // Get the index of the element which is in the middle
            int middle = fromIndex + (toIndex - fromIndex) / 2;
            // Sort the left side of the array
            sort(arr,fromIndex, middle);
            // Sort the right side of the array
            sort(arr,middle + 1, toIndex);
            // Combine them both
            merge(arr,fromIndex, middle, toIndex);
        }
    }

   
    static void merge(int[] arr, int fromIndex, int middleIndex, int toIndex) {

        // Copy both parts into the helper array


        for (int i = fromIndex; i <= toIndex; i++) {
            helper[i] = arr[i];
        }

        int i = fromIndex;
        int j = middleIndex + 1;
        int k = fromIndex;
        // Copy the smallest values from either the left or the right side back
        // to the original array
        while (i <= middleIndex && j <= toIndex) {
            if (helper[i] <= helper[j]) {
                arr[k] = helper[i];
                i++;
            } else {
                arr[k] = helper[j];
                j++;
            }
            k++;
        }
        // Copy the rest of the left side of the array into the target array
        while (i <= middleIndex) {
            arr[k] = helper[i];
            k++;
            i++;
        }
        ar = arr ; 
    }
}