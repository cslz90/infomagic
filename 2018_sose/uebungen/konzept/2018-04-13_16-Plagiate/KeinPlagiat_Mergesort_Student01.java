package mergesort;
import java.util.ArrayList;

/**
 * Created by ### on 22.05.17.
 */
public class MergeSort {

    /*
        falls dieser Code als Plagiat angezeigt wird:
            ich Habe mich fast 1 zu 1 am "Pseudocode" aus der Vorlesung
            entlanggehangelt.

            ich gehe nicht davon aus das ich der einzige Student bin der diese
            Aufgabe so gelöst hat.
     */

    public static void sort(int[] arr) {


        sort(arr, 0, arr.length);
    }

    /*
        algorithm mergeSort(lst: List<E>)
        if size(lst) <= 1 then
        return lst
        lstl, lstr := split(lst, size(lst)/2)
        lstl := mergeSort(lstl)
        lstr := mergeSort(lstr)
        res := merge(lstl, lstr)
        return res
     */

    // fromIndex inclusive, toIndex exclusive
    private static void sort(int[] arr, int fromIndex, int toIndex) {
        if(toIndex - fromIndex > 1){
            int splitIndex = (fromIndex+toIndex)/2;
            sort(arr,fromIndex,splitIndex);
            sort(arr,splitIndex,toIndex);
            merge(arr,fromIndex,splitIndex,toIndex);
        }
    }

    /*
        algorithm merge(lstl: List<E>, lstr: List<E>)
        l := 0; r := 0;
        res := new AList<E>()
        while size(res) < size(lstl) + size(lstr) do
        moreR := r < size(lstr)
        moreL := l < size(lstl)
        if moreR and (not(moreL) or lstr[r] < lstr[l]) then
        add(res, lstr[r])
        r := r + 1
        else
        add(res, lstl[l])
        l := l + 1
        return res
     */

// first subarray: fromIndex inclusive, middleIndex exclusive
// second subarray: middleIndex inclusive, toIndex exclusive

    static void merge(int[] arr, int fromIndex, int middleIndex, int toIndex) {
        int lef [] = new int[middleIndex - fromIndex];
        int rig [] = new int[toIndex - middleIndex];

        int l = 0;
        for (int i = fromIndex; i < middleIndex; i++) {
            lef[l++] = arr[i];
        }

        int r = 0;
        for (int i = middleIndex; i < toIndex; i++) {
            rig[r++] = arr[i];
        }

        l = 0;
        r = 0;


        ArrayList<Integer> hL = new ArrayList<>();
        while(hL.size() < (lef.length + rig.length)){
            boolean moreR = (r < rig.length);
            boolean moreL = (l < lef.length);
            if(moreR && (!(moreL) || rig[r] < lef[l])){
                hL.add(rig[r++]);
            } else {
                hL.add(lef[l++]);
            }
        }
        int frDx = fromIndex;
        for (int i = 0; i < hL.size(); i++) {
            arr[frDx] = hL.get(i);
            frDx ++;
        }
    }
}