package fileio;

import java.util.Scanner;
import java.io.*;

/**
 * Created by ### on 24.04.2017.
 */
public class NovizenFilter {
  public static void processList() throws Exception{
    Scanner inputScanner = null;
    BufferedWriter outputBufferedWriter = null;
    try{
      // in
      inputScanner = new Scanner(new File("./neue-personen_test.csv"));
      // out


      outputBufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("./berechtigte_novizen.csv"),"UTF-8"));

      while (inputScanner.hasNextLine()) {
        String line = inputScanner.nextLine();
        String intCandidate = line.split(", ")[1];
        try{
          if(Integer.parseInt(intCandidate) >= 18)
            outputBufferedWriter.write(line + "\n");
        }catch (Exception e){
          throw new Exception("FEHLER\n" + e.getMessage());
        }
      }
    }
    catch (Exception e) {
      throw new Exception("FEHLER\n" + e.getMessage());
    }
    finally {
      if(inputScanner != null) inputScanner.close();
      if(outputBufferedWriter != null) outputBufferedWriter.close();
    }
  }
}