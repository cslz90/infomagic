package fileio;
import java.util.Scanner;
import java.io.*;

public class NovizenFilter{

  public static void processList() throws Exception{
    BufferedWriter outputBufferedWriter = null;
    Scanner loadScanner = new Scanner(new File("./novizen.csv"));
    try{
       


       outputBufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream ("./berechtigte_novizen.csv")));
      
      while(loadScanner.hasNextLine()){
        String line = loadScanner.nextLine();
        String intberechtigung = line.split(", ")[1];
        try{
          if(Integer.parseInt(intberechtigung) >= 18)
            outputBufferedWriter.write(line + "\n");
        } catch(Exception e){
          throw new Exception("Falsche Angabe \n" + e.getMessage());
          
        }       
      }
    }
    catch (Exception e){
      throw new Exception("Falsche Angabe \n" + e.getMessage());
    }
    finally{
      if(loadScanner != null) loadScanner.close();
      if(outputBufferedWriter != null) outputBufferedWriter.close();
    }
  }
}