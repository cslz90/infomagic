package fileio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class NovizenFilter {

    private static final String IN_FILE_NAME = "novizen.csv";

    private static final String OUT_FILE_NAME = "berechtigte_novizen.csv";

    private static final int AGE_THRESHOLD = 18;

    public static void main(String[] args) {
        processList();
    }

    public static void processList() {
        List<Person> personList = readPersonList(IN_FILE_NAME);

        writePersonList(OUT_FILE_NAME, filterPerson(personList, AGE_THRESHOLD));
    }

    private static List<Person> readPersonList(String filePath) {
        List<Person> personList = new LinkedList<>();
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(filePath), StandardCharsets.UTF_8)) {
            String line;
            while ((line = reader.readLine()) != null) {
                personList.add(new Person(line));
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
        return personList;
    }

    private static List<Person> filterPerson(List<Person> inputList, int threshold) {
        List<Person> filteredList = inputList
                .stream()
                .filter(person -> person.getAge() >= threshold)
                .collect(Collectors.toList());

        return filteredList;
    }

    private static void writePersonList(String filePath, List<Person> personList) {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(filePath), StandardCharsets.UTF_8)) {
            for (Person person : personList) {
                writer.write(person.toString());
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }

}