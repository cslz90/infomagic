package mergesort;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ### on 5/22/17.
 */
public class MergeSort {
    public static void sort(int[] arr) {


        sort(arr, 0, arr.length);
    }

    // fromIndex inclusive, toIndex exclusive
    private static void sort(int[] arr, int fromIndex, int toIndex) {
        if ((toIndex - fromIndex) > 1) {
            int h = ((toIndex + fromIndex) / 2);
            sort(arr, fromIndex, h);
            sort(arr, h, toIndex);
            merge(arr, fromIndex, h, toIndex);
        }
    }

    // first subarray: fromIndex inclusive, middleIndex exclusive
    // second subarray: middleIndex inclusive, toIndex exclusive
    static void merge(int[] arr, int fromIndex, int middleIndex, int toIndex) {
        int[] leftArr = Arrays.copyOfRange(arr, fromIndex, middleIndex);
        int[] rightArr = Arrays.copyOfRange(arr, middleIndex, toIndex);
        ArrayList res = new ArrayList();

        int l = 0;


        int r = 0;

        while (res.size() < (leftArr.length + rightArr.length)) {
            boolean moreR = r < rightArr.length;
            boolean moreL = l < leftArr.length;
            if (moreR && (!moreL || (rightArr[r] < leftArr[l]))) {
                res.add(rightArr[r]);
                r = r + 1;
            } else {
                res.add(leftArr[l]);
                l = l + 1;
            }
        }
        for (int i = 0; i < res.size(); i++) {
            arr[fromIndex] = (int) res.get(i);
            fromIndex = fromIndex + 1;
        }
    }
}