\documentclass{beamer}

\usepackage[ngerman, english]{babel}
\usepackage[utf8]{inputenc}

\usetheme{csthm}
\usepackage{csjavalst}

\usetikzlibrary{positioning}

\title{Algorithmen und Datenstrukturen}
\subtitle{2 -- Verkettete Basilisten in Haskellien}
\date{9. April 2018}
\author{Christopher Schölzel}
\titlegraphic{\includegraphics[width=\paperwidth]{img/Globus_Nuernberg.JPG}}

\newcommand{\type}[1]{\textit{#1}}
\newcommand{\term}[1]{\textcolor{hl1}{#1}}

\makeatletter
\newcommand{\emoji}[1]{%
  \tikz[baseline=(s.base)]{
    \node (s)[inner sep=0pt, outer sep=0pt] {
      \includegraphics[height=1.5ex]{#1}
    }
  }%
}
\makeatother

\newcommand{\dragon}{\emoji{img/u1F409-dragonside.pdf}}
\newcommand{\book}{\emoji{img/u1F4D6-openbook.pdf}}

\begin{document}

\maketitle

\begin{frame}{\dragon{} Erinnerung: Array-Basilist}
  \includegraphics[width=\textwidth]{img/ArrayBasilist_1024.png}
\end{frame}

\begin{frame}{\dragon{} Verketteter Basilist}
  \includegraphics[width=\textwidth]{img/LinkedListMitMagie.png}

  \vfill

  \begin{block}{Definition: Verketteter Basilist (\type{Datenkreatur})}
    \term{Verkettete Basilisten} haben im Gegensatz zu Array-Basilisten \stress{mehrere Körper}.
    Um gegen die größeren Rivalen bestehen zu können, schließen sie sich zusammen, indem sie \stress{einander in den Schwanz beißen}.
    An der Spitze sitzt dabei immer der \stress{dominante Kopf}.
    Man sollte sehr darauf achten diesem zuerst Aufmerksamkeit zukommen zu lassen, da er ansonsten bissig wird.
  \end{block}
\end{frame}

\begin{frame}{\dragon{} Nil}
  \begin{center}
    \includegraphics[width=.3\textwidth]{img/Nil.png}
  \end{center}

  \vfill

  \begin{block}{Definition: Nil (\type{Verketteter Basilist})}
    Ein \term{Nil} ist ein verketteter Basilist im Larvenstadium.
    Es ist noch zu klein, um Dinge zu speichern, oder andere Basilisten an seinem Schwanz zu tragen.
    Damit es nicht trotzdem gebissen wird, hat es einen Abwehrstachel am Hinterteil.
    Nils findet man daher entweder \stress{alleine} oder \stress{ganz am Ende} von verketteten Basilisten.
  \end{block}
\end{frame}

\begin{frame}[fragile]{\book{} Verkettete Liste}
  \begin{block}{Definition: Verkettete Liste}
    Eine \term{Verkettete Liste} hinterlegt ihre Elemente \stress{einzeln} im Speicher.
    Zusätzlich zu dem \stress{Wert} des Elements wird auch ein \stress{Verweis} auf das nächste Element gespeichert.
    Anders als bei einer Array-Liste können die Daten \stress{beliebig im Speicher verteilt} liegen.
  \end{block}

  \vfill

  \begin{center}
    \begin{tikzpicture}[cell/.style={draw, outer sep=0pt, minimum width=20pt, minimum height=20pt}]
      \node[cell] (v1) at (0,0) {8};
      \node[cell, right=0pt of v1] (p1) {};

      \node[cell, right=20pt of p1] (v2) {2};
      \node[cell, right=0pt of v2] (p2) {};

      \node[cell, right=20pt of p2] (v3) {5};
      \node[cell, right=0pt of v3] (p3) {};

      \node[cell, fill=black, right=20pt of p3] (nil) {};

      \path[->, draw, very thick] (p1.center) -- (v2.west);
      \path[->, draw, very thick] (p2.center) -- (v3.west);
      \path[->, draw, very thick] (p3.center) -- (nil);
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}{\book{} Verkettete Liste mit Nil und Cons}
  Typische Implementierung einer Verketteten Liste:

  \vfill

  \begin{columns}[onlytextwidth, T]
    \begin{column}{.49\textwidth}
      \begin{block}{Datentyp \texttt{Nil}}
        \begin{itemize}
          \item entspricht einer \stress{leeren Liste}
          \item kein Wert
          \item keine Verweise
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{.49\textwidth}
      \begin{block}{Datentyp \texttt{Cons}}
        \begin{itemize}
          \item entspricht einem \stress{Listenelement}
          \item enthält Wert
          \item und Verweis
            \begin{itemize}
              \item auf ein \lstinline|Cons|, wenn die Liste noch weiter geht
              \item auf ein \lstinline|Nil|, wenn dieses \lstinline|Cons| das letzte ist
            \end{itemize}
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}

  \vfill\pause

  Vorteil: Keine leeren Verweise (\textit{null pointer})
\end{frame}

\begin{frame}[t]{\dragon{} Reise nach Haskellien}
  \begin{center}
    \begin{tikzpicture}
        \node[anchor=south west,inner sep=0] at (0,0) {
          \includegraphics[width=.9\textwidth]{img/Karte_V1_5.jpg}
        };
        \path[blue,line width=1mm,->] (2.7,6) edge[bend right] (7.7,6.5);
    \end{tikzpicture}
  \end{center}
\end{frame}


\lstset{language=Haskell}
\lstset{
  morekeywords=[2]{Nil, Cons},
  keywordstyle=[2]\color{hl3}
}

\begin{frame}[fragile]{\book{} Verkettete Liste in Haskell}
  \begin{center}
    \begin{minipage}{.7\textwidth}
      \begin{lstlisting}
        data LinkedList a = Nil | Cons a (LinkedList a) 
          deriving (Show)
      \end{lstlisting}
    \end{minipage}
  \end{center}

  \pause
  \begin{itemize}
    \item generiere Datentyp \code{LinkedList a}
      \begin{itemize}
        \item \code{a} ist ein Typparameter (z.B. \code{LinkedList Integer})
      \end{itemize}
    \item eine Liste ist entweder ein \code{Nil} oder ein \code{Cons}
    \item ein \code{Cons} besteht aus zwei Dingen
      \begin{itemize}
        \item ein Element vom Typ \code{a}
        \item eine weitere Liste vom Typ \code{LinkedList a}
      \end{itemize}
    \item \code{deriving(Show)} generiert eine Funktion zur Anzeige von Listen
  \end{itemize}
\end{frame}


\begin{frame}[fragile]{\book{} Umgang mit Listen in Haskell}
  \begin{center}
    \begin{minipage}{.6\textwidth}
      \begin{lstlisting}
        size Nil        = 0
        size (Cons h t) = 1 + size t
      \end{lstlisting}
    \end{minipage}
  \end{center}

  \pause
  \begin{itemize}
    \item \code{size} ist eine Funktion, die eine \code{LinkedList} übernimmt
    \item was die Funktion tut, hängt von deren Zusammensetzung ab
      \begin{itemize}
        \item die Länge eines \code{Nil} ist \code{0}
        \item die Länge eines \code{Cons} ist
          \begin{itemize}
            \item seine eigene Länge (\code{1})
            \item plus die Länge des Schwanzes (\code{size t})
          \end{itemize}
      \end{itemize}
    \pause
    \item das Prinzip der Entscheidung nach dem Typ der Argumente nennt man \term{pattern matching}
    \item in funktionalen Sprachen wie Haskell sind \term{rekursive} Definitionen weit verbreitet
  \end{itemize}
\end{frame}

\begin{frame}<1-4>[fragile]{\book{} Verkettete Liste: Hinzufügen}
  \begin{center}
    \begin{minipage}{.6\textwidth}
      \begin{lstlisting}[style=cs-disabled, classoffset=1, keywordstyle=\color{hl3!50}]
        size Nil        = 0
        size (Cons h t) = 1 + size t
      \end{lstlisting}

      \vspace{10mm}

      \begin{lstlisting}[style=uncover]
        add a Nil        = §Cons a Nil§
        §§add a (Cons h t) = §§§Cons h (add a t)§§§ §§
      \end{lstlisting}
    \end{minipage}
  \end{center}
\end{frame}

\begin{frame}[fragile]{\book{} Verkettete Liste: Zugriff}
  \begin{center}
    \begin{minipage}{.6\textwidth}
      \begin{lstlisting}[style=uncover]
        get idx Nil        = §Nothing§
        §§get 0   (Cons h t) =§§ §§§Just h§§§
        §§§§get idx (Cons h t) = get (idx-1) t§§§§
      \end{lstlisting}
    \end{minipage}
  \end{center}

  \vfill

  \pause[6]
  \begin{itemize}
    \item \term{pattern matching} funktioniert mit allen Datentypen
    \item statt Exceptions gibt es in Haskell den Rückgabetyp \code{Maybe a}
      \begin{itemize}
        \item entweder \code{Nothing}
        \item oder \code{Just a}
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}<1-4>[fragile]{\book{} Verkettete Liste: Weitere Funktionen}
  \begin{lstlisting}[style=uncover]
    set a idx Nil        = Nil§
    set a 0   (Cons h t) = Cons a t
    set a idx (Cons h t) = Cons h (set a (idx-1) t)§

    remove idx Nil        = Nil§§
    remove 0   (Cons h t) = t
    remove idx (Cons h t) = Cons h (remove (idx-1) t)§§

    insrt a 0   Nil        = §§§Cons a Nil
    insrt a idx Nil        = Nil
    insrt a 0   (Cons h t) = Cons a (Cons h t)
    insrt a idx (Cons h t) = Cons h (insrt a (idx-1) t)§§§
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile,t]{\book{} Verkettete Liste: Ausgabe}
  \begin{lstlisting}
    main = do
      let lst = Cons 1 (Cons 2 Nil) :: LinkedList Integer
      putStrLn ("Lst: " ++ show lst)
  \end{lstlisting}

  \vfill

  \begin{itemize}
    \item \strut\code{do} beginnt einen Bereich mit Seiteneffekten, wie I/O-Operationen
    \item \strut\code{let} definiert Konstanten (Variablen gibt es nicht)
    \item \strut\code{putStrLn} gibt eine Zeichenkette auf der Konsole aus
    \item \strut\code{++} konkateniert Zeichenketten
    \item \strut\code{show} macht eine Zeichenkette aus einem Objekt
  \end{itemize}
\end{frame}

\begin{frame}[fragile,t]{\book{} Verkettete Liste: Beispiel}
  \begin{lstlisting}[style=cs-disabled, classoffset=1, keywordstyle=\color{hl3!50}]
    main = do
      let lst = Cons 1 (Cons 2 Nil) :: LinkedList Integer
  \end{lstlisting}

  \begin{lstlisting}[style=uncover,gobble=2]
    let lst2 = Cons 7 lst
    let lst3 = add 5 lst2
    let lst4 = insrt 10 1 (remove 2 (set 0 3 lst3))
    let get0 = get 0
  \end{lstlisting}

  \begin{itemize}
    \pause
    \item \strut\code{Cons 7 lst} hängt eine \code{7} \stress{vorne} an \code{lst} an
    \pause
    \item \strut\code{add 5 lst2} hängt eine \code{5} \stress{hinten} an \code{lst2} an
    \pause
    \item bei verschachtelten Befehlen werden Klammern nötig
    \pause
    \item \strut\code{get 0} ist eine \stress{Funktion}, vom Typ \code{LinkedList a -> a}
  \end{itemize}
\end{frame}

\begin{frame}[fragile,t]{\dragon{} Wer würde gewinnen?}

  \begin{tikzpicture}[x=.5\textwidth]
    \node[rotate=-35, xscale=-1] (linked) at (0,0) {\includegraphics[width=.5\textwidth]{img/LinkedList_nofeed.png}};
    \node[rotate=35] (array) at (1,0) {\includegraphics[width=.5\textwidth]{img/ArrayBasilist_1024.png}};
    \path[->, draw, very thick] (0.6,2) -- (0.4,0.5) -- (0.6, 1) -- (0.4, -0.5);
  \end{tikzpicture}

  \vfill

  \begin{itemize}
    \item Nach welchen Regeln läuft der Kampf?
    \begin{itemize}
      \item Vorschriften für unorganisierte Kampfhandlungen (Ja'va)?
      \item Codex Haskellia?
    \end{itemize}
    \item Was entscheidet den Kampf?
      \begin{itemize}
        \item Entscheidende Vor- oder Nachteile?
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Bildquellen}
  \begin{center}
    \includegraphics[width=.9\textwidth]{img/LinkedListMitMagie.png}
  \end{center}

  \begin{itemize}
    \item Emojis: Fxemoji, Mozilla Foundation, Lizenz: CC BY 4.0
    \item Titelbild: Christopher Schölzel
    \item Basilisten: Julia Jelitzki
    \item Weltkarte: Dennis Tentscher
    \item Sonstige Bilder: Pixabay, Lizenz: Public Domain
  \end{itemize}
\end{frame}

\end{document}
