\documentclass{beamer}

\usepackage[ngerman, english]{babel}

\usetheme{csthm}
\usepackage{csjavalst}
\usepackage{cspseudo}
\usepackage{array} % allows to set columns in tabular to \raggedright

\title{Algorithmen und Datenstrukturen}
\subtitle{4 -- Einführung in die Algomantik (I)}
\date{28. April 2017}
\author{Christopher Schölzel}
\titlegraphic{\includegraphics[width=\paperwidth]{img/Baum_Gießen.JPG}}

\newcommand{\type}[1]{\textit{#1}}
\newcommand{\term}[1]{\textcolor{hl1}{#1}}
\newcommand{\enquote}[1]{\glqq{}#1\grqq{}}

\makeatletter
\newcommand{\emoji}[1]{%
  \tikz[baseline=(s.base)]{
    \node (s)[inner sep=0pt, outer sep=0pt] {
      \includegraphics[height=1.5ex]{#1}
    }
  }%
}
\makeatother
\newcommand{\dragon}{\emoji{img/u1F409-dragonside.pdf}}
\newcommand{\book}{\emoji{img/u1F4D6-openbook.pdf}}
\newcommand{\bigO}{$\mathcal{O}$}
\newcommand{\blossom}{\emoji{img/u1F33C-blossom.pdf}}
\newcommand{\ogre}{\emoji{img/u1F479-ogre.pdf}}
\newcommand{\imp}{\emoji{img/u1F47F-imp.pdf}}
\newcommand{\movingbox}{\emoji{img/u1F4E6-package.pdf}}
\newcommand{\construct}{\emoji{img/u1F477-construct.pdf}}
\newcommand{\princess}{\emoji{img/u1F478-princess.pdf}}
\newcommand{\queen}{\emoji{img/chess-151548.pdf}}
\newcommand{\gem}{\emoji{img/u1F48E-gem.pdf}}

\AtBeginSection[]
{
  \begin{frame}
  \frametitle{Inhalt}
  \tableofcontents[currentsection]
  \end{frame}
}

\begin{document}

\maketitle

\section{Brute Force}

\begin{frame}{\dragon{} Orkschamane Brutus}
  \begin{block}{Charakterportrait: Brutus}
    \begin{columns}
      \begin{column}{.3\textwidth}
        \includegraphics[width=\textwidth]{img/u1F479-ogre.pdf}
      \end{column}
      \begin{column}{.65\textwidth}
        \begin{itemize}
          \item orkischer Schamane
          \item lebte in grauer Vorzeit
          \item Begründer der Algomantik
          \item kämpfte mit Axt und Zauberstab
          \item Lieblingstechnik: Brute Force
        \end{itemize}
      \end{column}
    \end{columns}
  \end{block}
\end{frame}

\begin{frame}{\dragon{} Brute Force}
  \begin{block}{Brute Force (\type{Zaubertechnik})}
    \begin{itemize}
      \item Brutus {\Large\ogre{}} kann Gegner wegaxten
      \item Idee: Jedes Problem in wegaxtbare Gegner verwandeln
      \item diese Standard-Gegner werden \term{Brutes} genannt
        \begin{itemize}
          \item stellen sich gern in Reih und Glied auf
          \item kämpfen immer nur einer nach dem anderen
        \end{itemize}
    \end{itemize}
  \end{block}

  \vfill

  \Huge
  \ogre \hspace{5mm} \imp \imp \imp \imp \imp \LARGE \imp \Large \imp \large \imp \normalsize \imp \small \imp ...
\end{frame}

\begin{frame}{\book{} Brute Force}
  \begin{block}{Brute Force (\type{Algorithmische Technik})}
    \begin{itemize}
      \item alle möglichen Lösungen aufzählen
      \item jede durchprobieren
        \begin{itemize}
          \item wurde die richtige Lösung gefunden: Ende
          \item sonst: nächste Möglichkeit probieren
        \end{itemize}
    \end{itemize}
  \end{block}

  \vfill

  \begin{itemize}\itemsep5pt
    \item funktioniert immer
    \item sehr schnell zu implementieren
    \item manchmal geht es gar nicht besser
    \item für große Eingaben oft langsam
  \end{itemize}
\end{frame}

\begin{frame}{\book{} Beispiel: Erschöpfende Suche}
  Aufgabe: Finde einen Gegenstand in $n$ unmarkierten Umzugskartons.

  \vfill

  {\Huge \movingbox{} \movingbox{} \movingbox{} \movingbox{} \movingbox{} ... \movingbox}

  \vfill

  \begin{itemize}\itemsep8pt
    \item kein Anhaltspunkt, wo man anfangen soll
    \item beste Strategie: ein Karton nach dem anderen
    \item Laufzeit: \pause$\mathcal{O}(n)$
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\book{} Brute Force im Code}
  Typisches Aussehen von Brute-Force-Algorithmen:

  \vfill

  \begin{lstlisting}[language=cs-pseudo]
    algorithm someBruteForceAlgo(input: InputType)
      solutions: List<Solution>
      solutions := listPossibleSolutions(input)
      for i := 0,1,2, ..., solutions.length()-1 do
        if isCorrect(solutions.get(i)) then
          return solutions.get(i)
      return None
  \end{lstlisting}

  \vfill

  \begin{itemize}
    \item Hauptmerkmal: Schleife, die über alle möglichen Lösungen läuft
  \end{itemize}
\end{frame}

\section{Gier}

\begin{frame}{\dragon{} Zwergischer Runenschmied Greedy}
  \begin{block}{Charakterportrait: Greedy}
    \begin{columns}
      \begin{column}{.3\textwidth}
        \includegraphics[width=\textwidth]{img/u1F477-construct.pdf}
      \end{column}
      \begin{column}{.65\textwidth}
        \begin{itemize}
          \item gutmütig, so lange man nicht an seinen Schatz will
          \item kann Zauber auf Gegenstände übertragen
          \item trägt immer eine runenverzierte Schaufel bei sich
          \item Lieblingstechnik: Gier
        \end{itemize}
      \end{column}
    \end{columns}
  \end{block}
\end{frame}

\begin{frame}{\dragon{} Gier}
  \begin{block}{Gier (\type{Zaubertechnik})}
    \begin{itemize}
      \item Greedy {\Large \construct{}} denkt ökonomisch
      \item magische \stress{Schaufel} zeigt in Richtung verborgener Schätze
      \item an jeder Abzweigung
        \begin{itemize}
          \item wenn möglich in Richtung gehen, die die Schaufel weist
          \item ansonsten anfangen nach dem Schatz zu graben
        \end{itemize}
      \item Greedy läuft immer nur zum Schatz hin, niemals davon weg
    \end{itemize}
  \end{block}

  \vfill

  {\Huge \hfill \construct{} \includegraphics[height=1ex]{img/spade.pdf} \hfill \gem{}\gem{}\gem{} \hfill{}}
\end{frame}

\begin{frame}{\book{} Gier}
  \begin{block}{Gier (\type{Algorithmische Technik})}
    \begin{itemize}
      \item Start an irgendeiner (zufälligen) Stelle
      \item eine \stress{Heuristik} weist die Richtung zum Ziel
      \item in jedem Schritt
        \begin{itemize}
          \item Wähle die Möglichkeit, die dich dem Ziel am nächsten bringt.
          \item Wenn alle Möglichkeiten dich weiter weg bringen, hör auf.
        \end{itemize}
    \end{itemize}
  \end{block}

  \vfill

  \begin{itemize}
    \item findet nicht immer eine optimale Lösung
    \item geht nur, wenn Heuristik vorhanden ist
    \item schnell, wenn Heuristik leicht zu prüfen ist
    \item funktioniert meist auch für große Eingaben gut
  \end{itemize}
\end{frame}

\begin{frame}{\book{} Beispiel: Apothekerwaage}

  Aufgabe: Finde die richtige Kombination von Gewichten um einen Gegenstand auszuwiegen.

  \vfill

  \begin{center}
    \includegraphics[width=.1\textwidth]{img/419px-2kg_Gewicht.jpg}%
    \hspace{2cm}\includegraphics[width=.2\textwidth]{img/scales-36417.pdf}  
  \end{center}

  \vfill

  \begin{enumerate}
    \item fange beim größten Gewicht an
    \item \label{start}lege es auf die Waage
    \item wenn das Gewicht zu groß ist, leg es beiseite
    \item nimm das nächstkleinere Gewicht und gehe zu \ref{start}
    \item wenn keine Gewichte mehr übrig sind, hör auf
  \end{enumerate}

\end{frame}

\begin{frame}{\book{} Optimalität von Gier (I)}
  Gier ist \stress{optimal} bei folgender Gewichtverteilung:

  \vfill

  \begin{center}
    $50,\hspace{2mm} 2 \times 20,\hspace{2mm} 10,\hspace{2mm} 5,\hspace{2mm} 2 \times 2,\hspace{2mm} 1$
  \end{center}

  \vfill

  Aufgabe: Gegenstand mit 16 kg auswiegen

  \begin{itemize}
    \pause\item 50 und 20 passen nicht $\Rightarrow$ weglegen
    \pause\item 10 passt $\Rightarrow$ 6 übrig
    \pause\item 5 passt $\Rightarrow$ 1 übrig
    \pause\item 2 passt nicht $\Rightarrow$ weglegen
    \pause\item 1 passt $\Rightarrow$ 0 übrig $\Rightarrow$ fertig
    \pause\item Lösung: $10 + 5 + 1 = 16$
  \end{itemize}
\end{frame}

\begin{frame}{\book{} Optimalität von Gier (II)}
  Gier ist \stress{nicht optimal} bei folgender Gewichtsverteilung:

  \vfill

  \begin{center}
    $70, \hspace{2mm} 4 \times 20, \hspace{2mm} 7, \hspace{2mm} 4 \times 2$
  \end{center}

  \vfill

  Aufgabe: Gegenstand mit 80 kg auswiegen

  \begin{itemize}
    \pause\item 70 passt $\Rightarrow$ 10 übrig
    \pause\item 20 passt nicht $\Rightarrow$ weglegen
    \pause\item 7 passt $\Rightarrow$ 3 übrig
    \pause\item 2 passt $\Rightarrow$ 1 übrig
    \pause\item nichts passt mehr
    \pause\item Lösung: $70 + 7 + 2 \approx 80$
  \end{itemize}

  \vfill

  Optimale Lösung: $20 + 20 + 20 + 20 = 80$
\end{frame}

\begin{frame}{\dragon{} \book{} Optimalität von Gier: Gradient Descent}
  \includegraphics[width=\textwidth]{img/GradientDescent.pdf}
\end{frame}

\begin{frame}[fragile]{\book{} Gier im Code}
  Typisches Aussehen von Gier-Algorithmen:

  \vfill

  \begin{lstlisting}[language=cs-pseudo]
    algorithm someGreedyAlgorithm(position: P)
      // find best direction according to heuristic
      nextPos := position
      for direction in position.listDirections() do
        if heuristic(direction) > heuristic(nextPos) then
          nextPos := direction
      if nextPos = position then
        // no improvement => stop
        return position
      // improvement found => go on
      return someGreedyAlgorithm(nextPos)
  \end{lstlisting}
\end{frame}

\section{Backtracking}

\begin{frame}{\dragon{} Ariadne, Herrin des Labyrinths}
  \begin{block}{Charakterportait: Ariadne}
    \begin{columns}
      \begin{column}{.3\textwidth}
        \includegraphics[width=\textwidth]{img/u1F478-princess.pdf}
      \end{column}
      \begin{column}{.65\textwidth}
        \begin{itemize}
          \item echte Prinzessin
          \item hält sich gern in Labyrinthen auf
          \item bei Minotauren eher unbeliebt
          \item manche behaupten, sie hätte die Algomantik noch vor Brutus entwickelt
          \item Lieblingstechnik: Zeit zurückspulen (temporales Backtracking)
        \end{itemize}
      \end{column}
    \end{columns}
  \end{block}
\end{frame}

\begin{frame}{\dragon{} Temporales Backtracking}
  \begin{block}{Temporales Backtracking (\type{Zaubertechnik})}
    \begin{itemize}
      \item Ariadne {\Large \princess{}} ist sehr vorsichtig, aber auch neugierig
      \item sie erforscht jeden Winkel ihrer Umgebung
      \item bei einem Problem (Sackgasse, Minotaurus, ...):
        \begin{itemize}
          \item Zeit zurückdrehen
            \begin{itemize}
              \item bis zur letzten unerforschten Abzweigung
            \end{itemize}
          \item anderen Weg als vorher nehmen
        \end{itemize}
    \end{itemize}
  \end{block}

  \begin{center}
    {\Huge \princess{}} \hspace{1cm} \includegraphics[width=.25\textwidth]{img/maze-1800993_640.png}    
  \end{center}
\end{frame}

\begin{frame}{\book{} Backtracking}
  \begin{block}{Backtracking (\type{Algorithmische Technik})}
    \begin{itemize}
      \item Starte bei einem leeren Lösungsvorschlag
      \item Wähle die erste Option für den ersten Lösungsschritt
      \item Wähle die erste Option für den zweiten Lösungsschritt
      \item ...
      \item Wenn eine Lösung gefunden wurde, gib diese zurück
      \item Wenn der Vorschlag ungültig wird:
        \begin{itemize}
          \item Geh einen Lösungsschritt zurück (engl. \textit{to track back})
          \item Wähle dort die nächste Option
          \item Wenn es keine Optionen mehr gibt, geh noch einen Schritt zurück
        \end{itemize}
    \end{itemize}
  \end{block}

  \vfill

  \begin{itemize}
    \item führt immer zu einer Lösung
    \item meist schneller als erschöpfende Suche
    \item sinnvoll, wenn ungültige Lösungen schnell identifiziert werden
  \end{itemize}
\end{frame}

\begin{frame}{\book{} Beispiel: 4-Damen-Problem}
  \begin{tabular}{l | l | l | l | l |}
    & A & B & C & D \\\hline
    1 & \hspace{5mm} & \hspace{5mm} & \hspace{5mm} & \hspace*{5mm} \\[5mm]\hline
    2 & \hspace{5mm} & \hspace{5mm} & \hspace{5mm} & \hspace*{5mm} \\[5mm]\hline
    3 & \hspace{5mm} & \hspace{5mm} & \hspace{5mm} & \hspace*{5mm} \\[5mm]\hline
    4 & \hspace{5mm} & \hspace{5mm} & \hspace{5mm} & \hspace*{5mm} \\[5mm]\hline
  \end{tabular} \hspace{5mm} \includegraphics[width=1cm]{img/chess-151548.pdf} \hspace{5mm} \includegraphics[width=1cm]{img/chess-151548.pdf} \hspace{5mm} \includegraphics[width=1cm]{img/chess-151548.pdf} \hspace{5mm} \includegraphics[width=1cm]{img/chess-151548.pdf}

  \vfill

  Aufgabe: Verteile 4 Damen auf einem 4x4-Feld, so dass keine die andere schlagen kann.

\end{frame}

\begin{frame}{\book{} Vier Damen im Fluss der Zeit}
  Lösung des 4-Damen-Problems mit Backtracking: Foliensatz 4.5
\end{frame}

\begin{frame}[fragile]{\book{} Backtracking im Code}

  Typisches Aussehen von Backtracking-Algorithmen:
  
  \vfill

  \begin{lstlisting}[language=cs-pseudo]
    algorithm someBacktrackingAlgo(proposal: P)
      if proposal.isInvalid() then
        // invalid => track back
        return None
      if proposal.isSolution() then
        return proposal
      for option in proposal.possibleNextSteps() do
        newProposal := proposal.apply(option)
        result := someBacktrackingAlgo(newProposal)
        if result != None then
          // solution found => return
          return result
      // no valid options found => track back
      return None
  \end{lstlisting}

  \vfill\pause

  \begin{itemize}
    \item effizienter, je öfter \code{proposal.isInvalid()} gilt
  \end{itemize}
\end{frame}

\begin{frame}{Bildquellen}
  \begin{center}
    \includegraphics[width=.3\textwidth]{img/u1F479-ogre.pdf}%
    \includegraphics[width=.3\textwidth]{img/u1F477-construct.pdf}%
    \includegraphics[width=.3\textwidth]{img/u1F478-princess.pdf}
  \end{center}

  \begin{itemize}
    \item Emojis: Fxemoji, Mozilla Foundation, Lizenz: CC BY 4.0
    \item Titelbild: Annina Hofferberth
    \item Sonstige Bilder: Pixabay, Lizenz: Public Domain
  \end{itemize}
\end{frame}

\end{document}