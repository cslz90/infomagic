\documentclass{beamer}

\usepackage[ngerman, english]{babel}
\usepackage[utf8]{inputenc}

\usetheme{csthm}
\usepackage{csjavalst}
\usepackage{cspseudo}
\usepackage{array} % allows to set columns in tabular to \raggedright
\usepackage{csquotes} % for \blockquote

\usetikzlibrary{positioning} % for right = 20pt of x1

\title{Algorithmen und Datenstrukturen}
\subtitle{9 -- Lazy Evaluation und Streams}
\date{14. Mai 2018}
\author{Christopher Schölzel}
\titlegraphic{\includegraphics[width=\paperwidth]{img/green_wheat_field.JPG}}

\newcommand{\type}[1]{\textit{#1}}
\newcommand{\term}[1]{\textcolor{hl1}{#1}}
%\newcommand{\enquote}[1]{\glqq{}#1\grqq{}}

\makeatletter
\newcommand{\emoji}[1]{%
  \tikz[baseline=(s.base)]{
    \node (s)[inner sep=0pt, outer sep=0pt] {
      \includegraphics[height=1.5ex]{#1}
    }
  }%
}
\makeatother

\newcommand{\bigO}{$\mathcal{O}$}
\newcommand{\Oof}[1]{$\mathcal{O}(#1)$}

\newcommand{\dragon}{\emoji{img/u1F409-dragonside.pdf}}
\newcommand{\book}{\emoji{img/u1F4D6-openbook.pdf}}
\newcommand{\blossom}{\emoji{img/u1F33C-blossom.pdf}}
\newcommand{\ogre}{\emoji{img/u1F479-ogre.pdf}}
\newcommand{\imp}{\emoji{img/u1F47F-imp.pdf}}
\newcommand{\movingbox}{\emoji{img/u1F4E6-package.pdf}}
\newcommand{\construct}{\emoji{img/u1F477-construct.pdf}}
\newcommand{\princess}{\emoji{img/u1F478-princess.pdf}}
\newcommand{\queen}{\emoji{img/chess-151548.pdf}}
\newcommand{\man}{\emoji{img/u1F468-man.pdf}}
\newcommand{\devilhorns}{\emoji{img/u1F608-devilhorns.pdf}}
\newcommand{\ant}{\emoji{img/u1F41C-ant.pdf}}
\newcommand{\antSide}{\emoji{img/u1F41C-ant_sideways.pdf}}
\newcommand{\folder}{\emoji{img/u1F5C2-cardindexdividers.pdf}}
\newcommand{\doc}{\emoji{img/u1F5CE-document.pdf}}
\newcommand{\gma}{\emoji{img/u1F475-gma.pdf}}
\newcommand{\cross}{\emoji{img/u274C-crossmark.pdf}}
\renewcommand{\checkmark}{\emoji{img/u2714-heavycheckmark.pdf}}
\newcommand{\cat}{\emoji{img/u1F431-cat.pdf}}

\AtBeginSection[]
{
  \begin{frame}
  \frametitle{Inhalt}
  \tableofcontents[currentsection]
  \end{frame}
}

\lstset{language=cs-pseudo}

\begin{document}

\maketitle

\section{Lazy Evaluation}

\begin{frame}{\dragon{} Die Ströme der Bequemlichkeit}
  \begin{center}
    \includegraphics[width=.8\textwidth]{img/woken-hole-1768553_1280.jpg}
  \end{center}
\end{frame}

\begin{frame}{\dragon{} Mr. Snuggelz}
  \begin{block}{Charakterportrait: Mr. Snuggelz}
    \begin{columns}[onlytextwidth]
      \begin{column}{.29\textwidth}
        \begin{center}
          \includegraphics[width=\textwidth]{img/u1F431-cat.pdf}
        \end{center}
      \end{column}
      \begin{column}{.69\textwidth}
        \begin{itemize}
          \item mächtiger gestaltwandelnder Infomagier
          \item lebt glücklich als Haustier von Dolores
          \item wird bei der Auflistung der alten Meister oft vergessen
          \item hat es gerne kuschelig
          \item Lieblingstechnik: Bequemlichkeit
        \end{itemize}
      \end{column}
    \end{columns}
  \end{block}
\end{frame}

\begin{frame}{\dragon{} Bequemlichkeit}
  \begin{block}{Bequemlichkeit (\type{Zaubertechnik})}
    \begin{itemize}
      \item Lebensphilosophie von einigen Datenkreaturen
      \item Befehle von außen werden aufgeschrieben
      \item aber nicht ausgeführt (oder nur auf eine sehr einfache Art)
      \item erst wenn eine Ausgabe gefordert wird, wird gearbeitet
    \end{itemize}
  \end{block}

  \begin{center}
    \includegraphics[width=.7\textwidth]{img/still-life-3097682_1280.jpg}
  \end{center}
\end{frame}

\begin{frame}{\book{} Lazy Evaluation}
  \begin{block}{Lazy Evaluation (\type{Auswertungsstrategie})}
    \begin{itemize}
      \item andere Namen: \textit{call-by-need}, \textit{non-strict evaluation}
      \item mehrere Befehle aneinanderreihen, ohne sie auszuwerten
      \item erst ausführen, wenn ein Ergebnis benötigt wird
    \end{itemize}
  \end{block}

  \vfill

  Vorteile: 

  \begin{itemize}
    \item vermeidet unnötiges Kopieren von Daten
    \item erlaubt Umgang mit unendlichen Datenstrukturen
    \item reduziert Speicherverbrauch
    \item erleichtert die Parallelisierung von Code
  \end{itemize}
\end{frame}

\section{Streams}

\begin{frame}{\dragon{} Stromnymphen}
  \begin{block}{Stromnymphe (\type{Rolle})}
    Um als \stress{Stromnymphe} zu arbeiten muss eine Datenkreatur sowohl magisch als auch technisch begabt sein. Stromnymphen kontrollieren den Fluss von \stress{Datenströmen}, indem sie diesen durch verschiedene \stress{Apparaturen} lenken.
  \end{block}

  \vfill

  \begin{columns}[onlytextwidth]
    \begin{column}{.69\textwidth}
      \includegraphics[width=.3\textwidth]{img/stream_quelle.jpg}
      \includegraphics[width=.3\textwidth]{img/stream_filter.jpg}
      \includegraphics[width=.3\textwidth]{img/stream_map.jpg}
      \includegraphics[width=.3\textwidth]{img/stream_reduce.jpg}
      \includegraphics[width=.3\textwidth]{img/stream_consumer.jpg}
    \end{column}
    \begin{column}{.29\textwidth}
      \includegraphics[width=\textwidth]{img/nymph-2612952_640.jpg}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{\dragon{} Wer eignet sich als Stromnymphe?}
  \pause
  \begin{center}
    \includegraphics[width=.9\textwidth]{img/stream_bowl2.pdf}
  \end{center}
\end{frame}

\begin{frame}[fragile]{\book{} Streams}
  \begin{block}{Stream (\type{abstrakter Datentyp})}
    Ein Stream repräsentiert einen (potentiell \stress{unendlichen}) \stress{Strom von Daten}, dessen Elemente man mit einem \stress{Iterator} aufzählen kann. Streams bieten typischerweise die folgenden Operationen an:

    \vspace{10pt}

    \begin{itemize}
      \item \code{map} wendet eine Funktion auf jedes Element an
        \begin{itemize}
          \item dabei kann sich der Typ des Streams ändern
        \end{itemize}
      \item \code{filter} behält nur die Elemente, für die eine Bedingung gilt
      \item \code{reduce} fügt die Elemente zu einem Ergebnis zusammen
        \begin{itemize}
          \item wird manchmal auch \code{fold} genannt
        \end{itemize}
    \end{itemize}
  \end{block}

  % \begin{lstlisting}
  %   type Stream<E>
  %     operation iterator(this): Iterator<E>
  %     operation map(this, f: Function<E,X>): Stream<X>
  %     operation filter(this, p: Predicate<E>): Stream<E>
  %     operation reduce(this, a: Accumulator<X,E>, identity: X): X
  % \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{\book{} Streams in Pseudocode}
  \begin{lstlisting}[language=cs-pseudo]
    type Stream<E>
      operation newIterator(this): Iterator<E>
      operation map(this, f: Function<E,X>): Stream<X>
      operation filter(this, p: Predicate<E>): Stream<E>
      operation reduce(this, a: Accumulator<X,E>, identity: X): X

    type Function<E,X>    // transforms some E into some X
      operation call(arg: E): X

    type Predicate<E>     // tests a condition
      operation check(arg: E): boolean

    type Accumulator<E,X> // "adds" element to acc
      operation accumulate(acc: X, element: E): X
  \end{lstlisting}
\end{frame}

\newcommand{\streamPic}{
  \lstset{basicstyle=\scriptsize\ttfamily}
  \begin{tikzpicture}[
    cell/.style={draw, thick, minimum width=20pt, minimum height=20pt, outer sep=0pt},
    reference/.style={->, very thick}
  ]
    \node[cell] (stream) at (0,0) {};
    \node[cell, right=70pt of stream] (filtering filter) {};
    \node[cell, right=0pt of filtering filter] (filtering source) {};
    \node[cell, right=70pt of filtering source] (mapping mapper) {};
    \node[cell, right=0pt of mapping mapper] (mapping source) {};
    \node[right=10pt of mapping source] (array list) {\strut\lstinline|ArrayList|};
    \node[above=10pt of filtering filter] (iseven) {\strut\lstinline|IsEven|};
    \node[above=10pt of mapping mapper] (multiplyby3) {\strut\lstinline|MultiplyBy3|};
    \node[below=5pt of stream] {\strut\lstinline|IterableStream|};
    \node[below=5pt of filtering filter.south east] {\strut\lstinline|FilteringIterable|};
    \node[below=5pt of mapping mapper.south east] {\strut\lstinline|MappingIterable|};
    \draw[reference] (mapping mapper.center) -- (multiplyby3);
    \draw[reference] (filtering filter.center) -- (iseven);
    \draw[reference] (stream.center) -- (filtering filter);
    \draw[reference] (filtering source.center) -- (mapping mapper);
    \draw[reference] (mapping source.center) -- (array list);
  \end{tikzpicture}
}

\begin{frame}[fragile]{\book{} IterableStream: Idee}
  \begin{columns}[onlytextwidth]
    \begin{column}{.69\textwidth}
      \begin{block}{IterableStream (\type{Datenstruktur})}
        \begin{itemize}
          \item realisiert den ADT \stress{Stream}
          \item nutzt etwas iterierbares als Datenquelle
            \begin{itemize}
              \item Liste, Array, anderer Stream, ...
            \end{itemize}
          \item \code{map} und \code{filter} erzeugen neue Datenquellen
          \item die speichern die übergebenen Funktionen bzw. Prädikate
          \item[$\Rightarrow$] verkettete Struktur
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{.29\textwidth}
      \includegraphics[width=\textwidth]{img/roman-soldier-1878213_640.jpg}
    \end{column}
  \end{columns}

  \pause\vfill\streamPic
\end{frame}

\begin{frame}[fragile]{\book{} IterableStream}
  \begin{lstlisting}[language=cs-pseudo]
    type Iterable<E>
      operation newIterator(): Iterator<E>
  \end{lstlisting}\pause
  \begin{lstlisting}[language=cs-pseudo]
    structure IterableStream<E> < Iterable<E>, Stream<E>
      components
        source: Iterable<E>
      
      operation new(this, source: Iterable<E>)
        this.source := source
      
      operation newIterator(this): Iterator<E>
        return this.source.newIterator()
  \end{lstlisting}\pause
  % \begin{lstlisting}[language=cs-pseudo, gobble=4]
  %     operation map(this, f: Function<E,X>): Stream<X>
  %       ...
  %     operation filter(this, p: Predicate<E>): Stream<E>
  %       ...
  %     operation reduce(this, a: Accumulator<X,E>, identity: X): X
  %       ...
  % \end{lstlisting}

  \streamPic

  % operation map(this, f: Function<E,X>): Stream<X>
  %   return IteratorStream<E>.new(MappingIterable<E,X>.new(f))
  % operation filter(this, p: Predicate<E>): Stream<E>
  %   return IteratorStream<E>.new(FilteringIterable<E>.new(p))
  % operation reduce(this, a: Accumulator<X,E>, identity: X): X
  %   res := identity
  %   it := this.iterator()
  %   while it.hasNext() do
  %     res := a.accumulate(res, it.next())
  %   return res
\end{frame}

\begin{frame}{\dragon{} Generator}
  \begin{center}
    \includegraphics[width=.7\textwidth]{img/stream_quelle.jpg}
  \end{center}
\end{frame}

\begin{frame}[fragile]{\book{} Liste als Generator}
  \begin{lstlisting}[language=cs-pseudo]
    type IterableArrayList<E> < ArrayList<E>, Iterable<E>
      operation newIterator()
        return ArrayListIterator<E>.new(this)
  \end{lstlisting}

  \vfill

  \begin{itemize}
    \item \code{ArrayList} als Quelle für \code{IterableStream}
    \item \code{IterableStream} kann \code{ArrayListIterator} benutzen
    \item \code{ArrayListIterator} greift auf \code{ArrayList} zu
    \item[$\Rightarrow$] Daten können Stück für Stück entnommen werden
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\book{} Unendlicher Generator}
  \begin{lstlisting}[language=cs-pseudo]
    structure NatGenerator < Iterable<int>
      operation newIterator()
        return NatIterator.new()

    structure NatIterator < Iterator<int>
      components
        x: int
      operation new()
        this.x := -1
      
      operation hasNext()
        return true // we can always generate a following number

      operation next()
        this.x := this.x + 1
        return this.x
  \end{lstlisting}
\end{frame}

\begin{frame}{\book{} Hinweis zu unendlichen Streams}
  Gründe für unendliche Streams:

  \begin{itemize}\itemsep10pt
    \item Nutzer kann Abbruchkriterium zu einem späteren Zeitpunkt selbst wählen
      \begin{itemize}
        \item über eine Operation \code{limit}, die den Stream auf die ersten $n$ Elemente begrenzt
        \item über eine Operation \code{until}, die eine Abbruchbedingung als Prädikat übernimmt
      \end{itemize}
    \item Absichtliche Endlosschleife
      \begin{itemize}
        \item möglichst viele Primzahlen berechnen
        \item alle Pakete eines Netzwerkstreams bearbeiten (\code{next} wartet auf nächstes Paket)
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{\dragon{} Map}
  \begin{center}
    \includegraphics[width=\textwidth]{img/stream_map.jpg}
  \end{center}
\end{frame}

\begin{frame}[fragile]{\book{} Map}
  \begin{lstlisting}[language=cs-pseudo]
    operation map(this: Stream<E>, f: Function<E,X>): Stream<X>

    type Function<E,X>    // transforms some E into some X
      operation call(arg: E): X
  \end{lstlisting}

  \vfill

  \begin{itemize}
    \item Führt eine beliebige Funktion \code{f} für jedes Element aus
    \item Rückgabewerte der Funktion dienen als Elemente des neuen Streams
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\book{} MappingIterable}
  \begin{lstlisting}
    structure MappingIterable<E,X> < Iterable<X>
      components
        f: Function<E,X>
        source: Iterable<E>
      
      operation new(this, f: Function<E,X>, source: Iterable<E>)
        this.f := f
        this.source := source
      
      operation newIterator(this)
        return MappingIterator<E,X>.new(
          this.f,
          this.source.newIterator()
        )
  \end{lstlisting}

  \vfill

  \begin{itemize}
    \item Funktion \code{f} wird an erzeugte Iteratoren übergeben
    \item erst beim Aufruf der Operation \code{next} wird \code{f} tatsächlich aufgerufen
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\book{} MappingIterator}
  \begin{lstlisting}
    structure MappingIterator<E,X> < Iterator<X>
      components
        f: Function<E,X>
        source: Iterator<E>
      
      operation new(this, f: Function<E,X>, source: Iterator<E>)
        this.f := f
        this.source := source
      
      operation hasNext(this)
        // map cannot change number of elements => forward
        return this.source.hasNext()
      
      operation next(this)
        // call mapping function f on element before returning
        return this.f.call(this.source.next())
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{\book{} Implementierung von map}
  \begin{lstlisting}
    operation map(this:IterableStream<E>,f:Function<E,X>):Stream<X>
      return IterableStream<X>.new(
        MappingIterable<E,X>.new(f, this.source)
      )
  \end{lstlisting}

  \vfill

  \begin{itemize}\itemsep5pt
    \item Datenquelle \code{source} und Funktion \code{f} werden in \code{MappingIterable} verpackt
    \item das dient einem neu erzeugten \code{IterableStream} als Datenquelle
    \item sonst macht \code{map} nichts
    \item[$\Rightarrow$] Laufzeit: \pause\Oof{1}
  \end{itemize}
\end{frame}

\begin{frame}{\dragon{} Filter}
  \begin{center}
    \includegraphics[width=.7\textwidth]{img/stream_filter.jpg}
  \end{center}
\end{frame}

\begin{frame}[fragile]{\book{} Filter}
  \begin{lstlisting}[language=cs-pseudo]
    operation filter(this: Stream<E>, p: Predicate<E>): Stream<E>
    
    type Predicate<E>     // tests a condition
      operation check(arg: E): boolean
  \end{lstlisting}

  \vfill

  \begin{itemize}
    \item filtert Elemente anhand einem Prädikat \code{p}
    \item Element \code{x} bleibt im Stream, wenn \code{p.check(x)} wahr ist
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\book{} FilteringIterable}
  \begin{lstlisting}
    structure FilteringIterable<E> < Iterable<E>
      components
        p: Predicate<E>
        source: Iterable<E>
      
      operation new(this, p: Predicate<E>, source: Iterable<E>)
        this.p := p
        this.source := source
      
      operation newIterator(this)
        return FilteringIterator<E>.new(
          this.p, this.source.newIterator()
        )
  \end{lstlisting}

  \vfill

  \begin{itemize}
    \item wie bei \code{MappingIterable}
    \item Prädikat \code{p} wird an erzeugte Iteratoren übergeben
    \item erst beim Aufruf der Operation \code{next} wird \code{p} tatsächlich aufgerufen
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\book{} FilteringIterator}
  \begin{lstlisting}[language=cs-pseudo, basicstyle=\tiny\ttfamily]
    structure FilteringIterator<E> < Iterator<E>
      components
        p: Predicate<E>
        source: Iterator<E>
        nextValid: E
      
      operation new(this, p: Predicate<E>, source: Iterator<E>)
        this.p := p
        this.source := source
        this.nextValid := None
        this.next() // we need to find the first element (if it exists)
      
      operation hasNext(this)
        return this.nextValid != None
      
      // helper operation that returns the next source element if it exists and None if it does not exist
      operation nextOrNone(this)
        if this.source.hasNext() then
          return this.source.next()
        else
          return None

      operation next(this)
        tmp := this.nextValid // remember the element that we want to return
        // find the next element
        this.nextValid = this.nextOrNone()
        while this.nextValid != None and not this.p.check(this.nextValid) do
          // skip all elements that do not pass the check
          this.nextValid := this.nextOrNone()
        return tmp // return the element that we remembered
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{\book{} FilteringIterator: next}
  \begin{lstlisting}
    operation next(this)
      tmp := this.nextE // remember the element we want to return
      // find the next element
      this.nextE = this.nextOrNone()
      while this.nextE != None and not this.p.check(this.nextE) do
        // skip all elements that do not pass the check
        this.nextE := this.nextOrNone()
      return tmp // return the element that we remembered
  \end{lstlisting}

  \vfill

  \begin{itemize}
    \item \code{this.source.hasNext()} impliziert nicht \code{this.hasNext()}
    \item Element könnte vom Prädikat abgelehnt werden
    \item[$\Rightarrow$] nächstes valides Element muss gefunden werden, bevor wir sagen können, dass es existiert
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\book{} Implementierung von filter}
  \begin{lstlisting}
    operation filter(this:IterableStream<E>,p:Predicate<E>):Stream<E>
      return IterableStream<X>.new(
        FilteringIterable<E,X>.new(p, this.source)
      )
  \end{lstlisting}

  \vfill

  \begin{itemize}\itemsep5pt
    \item Datenquelle \code{source} und Prädikat \code{p} werden in \code{FilteringIterable} verpackt
    \item das dient einem neu erzeugten \code{IterableStream} als Datenquelle
    \item sonst macht \code{filter} nichts
    \item[$\Rightarrow$] Laufzeit: \pause\Oof{1}
  \end{itemize}
\end{frame}

\begin{frame}{\dragon{} Reduce}
  \begin{center}
    \includegraphics[width=.7\textwidth]{img/stream_reduce.jpg}
  \end{center}
\end{frame}

\begin{frame}[fragile]{\book{} Reduce}
  \begin{lstlisting}
    operation reduce(this, a: Accumulator<X,E>, identity: X): X
    
    type Accumulator<E,X> // "adds" element to acc
      operation accumulate(acc: X, element: E): X
  \end{lstlisting}

  \vfill

  \begin{itemize}
    \item fügt die Elemente zu einem Ergebnis zusammen
    \item \code{identitiy} ist der Startwert (0, 1, leere Liste, ...)
    \item der \code{Accumulator} fügt ein weiteres Element \code{element} an
      \begin{itemize}
        \item \code{int} zu Summe addieren
        \item \code{int} zu Produkt multiplizieren
        \item \code{E} in \code{List<E>} einfügen
        \item ...
      \end{itemize}
    \item \code{acc} ist dabei der schon bestehende Wert
    \item im Gegensatz zu \code{map} und \code{filter} wird kein neuer Stream erzeugt
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\book{} Implementierung von reduce}
  
  \begin{lstlisting}
      operation reduce(
        this: IterableStream<E>,
        a: Accumulator<X,E>,
        identity: X
      )
        res := identity  // start value
        it := this.newIterator() // iterate over stream elements
        while it.hasNext() do
          // "add" element to result using the accumulator
          res := a.accumulate(res, it.next())
        return res
    \end{lstlisting}
  \begin{itemize}
    \item Beispiele für \code{res := a.accumulate(res, it.next())}
      \begin{itemize}
        \item \code{res := res + it.next()}
        \item \code{res := res * it.next()}
        \item \code{res.add(it.next())}
      \end{itemize}
    \item je nach dem, welches \code{a} übergeben wird
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\book{} Effizienzklasse von reduce}
  
  \begin{lstlisting}[language=cs-pseudo, basicstyle=\scriptsize\ttfamily]
      operation reduce(this: IterableStream<E>, a: Accumulator<X,E>, identity: X)
        res := identity  // start value
        it := this.newIterator() // iterate over stream elements
        while it.hasNext() do
          // "add" element to result using the accumulator
          res := a.accumulate(res, it.next())
        return res
    \end{lstlisting}

  \vfill

  In welcher Effizienzklasse liegt \code{reduce}?

  \begin{itemize}
    \item $n = $ Länge des Input-Streams
    \pause\item Schleife wird $n$ mal durchlaufen
    \item pro Durchlauf
      \begin{itemize}
        \pause\item ein \code{accumulate}
        \item ein \code{next}
      \end{itemize}
    \pause\item[$\Rightarrow$] \Oof{n \cdot (T_{\text{accumulate}}(n) + T_\text{next}(n))}
    \pause\item $T_\text{next}(n)$ kann die Kosten vieler Stream-Operationen enthalten
  \end{itemize}
\end{frame}

\begin{frame}{\dragon{} Streams zusammensetzen}
  \begin{columns}[onlytextwidth]
    \begin{column}{.29\textwidth}
      \includegraphics[width=\textwidth]{img/nymph-2612952_640.jpg}
    \end{column}
    \begin{column}{.69\textwidth}
      \includegraphics[width=.9\textwidth]{img/stream_filterMapReduce.jpg}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}<1-2>[fragile]{\book{} Streams zusammensetzen}
  \begin{columns}[onlytextwidth]
    \begin{column}{.59\textwidth}
      \begin{lstlisting}[language=cs-pseudo, style=highlight-static, basicstyle=\scriptsize\ttfamily]
        structure §MultiplyBy3§ < Function<int,int>
          operation call(x: int)
            §return x * 3§

        structure §§IsEven§§ < Predicate<int>
          operation check(x: int)
            §§return x % 2 == 0§§

        type A = Accumulator<List<int>, int>
        structure §§§AddToList§§§ < A
          operation accumulate(
            acc: List<int>, element: int
          )
            §§§acc.add(element)
            return acc§§§
      \end{lstlisting}
    \end{column}
    \begin{column}{.39\textwidth}
      \begin{lstlisting}[language=cs-pseudo, style=highlight-static, basicstyle=\scriptsize\ttfamily]
        lst := ArrayList<int>.new()
        lst.add(0)
        lst.add(1)
        lst.add(6)
        lst.add(3)
        lst.add(2)
        res := IterableStream<int>.new(lst)
          §.map(MultiplyBy3.new())§
          §§.filter(IsEven.new())§§
          §§§.reduce(
            AddToList.new(),
            ArrayList<int>.new()
          )§§§
      \end{lstlisting}
      \begin{lstlisting}[language={}, style=uncover, basicstyle=\scriptsize\ttfamily]
        // res = §[0, 18, 6]§
      \end{lstlisting}
    \end{column}
  \end{columns}

  \streamPic
\end{frame}

\begin{frame}<1-2>{\dragon{} Lambada-Ausdruckstanz bei Stromnymphen}
  \begin{center}
    \alt<1>{\includegraphics[width=.5\textwidth]{img/dancers-33395.pdf}}{\includegraphics[width=.5\textwidth]{img/dancers-33395_lambda.pdf}}
  \end{center}
\end{frame}

\begin{frame}[fragile]{\book{} Streams und Lambda-Ausdrücke}
  \begin{block}{Definition: Lambda-Ausdrücke}
    \stress{Lambda-Ausdrücke} sind eine \stress{abkürzende Schreibweise} für \stress{Funktionen} (für uns: Datentypen mit nur einer Operation).
  \end{block}

  \vspace{10pt}

  \begin{columns}[onlytextwidth,T]
    \begin{column}{.49\textwidth}
      Ohne Lambda:

      \vspace{10pt}

      \begin{lstlisting}[language=cs-pseudo, style=highlight-static]
        type Ifunc = Function<int,int>
        structure MultiplyBy3 < Ifunc
          operation call(§x: int§)
            §§return x * 3§§


        strm := strm.map(
          MultiplyBy3.new()
        )
      \end{lstlisting}
    \end{column}
    \begin{column}{.49\textwidth}
      Mit Lambda:

      \vspace{10pt}

      \begin{lstlisting}[language=cs-pseudo, style=highlight-static]
        strm := strm.map(§x§ -> §§x * 3§§)
      \end{lstlisting}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]{\book{} Vorteile von lazy evaluation: Python 2}
  \begin{lstlisting}[language=python]
    for i in range(1000):
      print("Hello "+i)
  \end{lstlisting}

  \vfill

  \begin{itemize}
    \item \code{range(1000)} erzeugt (unnötigerweise) Liste mit 1000 Elementen
      \begin{itemize}
        \item verschwendet Speicherplatz bei jeder For-Schleife
      \end{itemize}
    \item[$\Rightarrow$] Empfohlene Alternative: \code{xrange} Funktion
      \begin{itemize}
        \item erzeugt die Zahlen mit lazy evaluation
      \end{itemize}
    \item So populär, dass Python 3 \code{xrange} in \code{range} umbenannt hat
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\book{} Vorteile von lazy evaluation: Parallelisierung}
  \begin{lstlisting}[language=cs-pseudo]
    res := IterableStream<String>.new(lst)
      .filter(x -> x.length() < 100)
      .map(x -> x.length())
      .map(x -> x * x)
      .reduce((acc, e) -> acc + e, 0)
  \end{lstlisting}

  \vfill

  Frage: Was hält uns davon ab einen Teil des Streams...
  \begin{itemize}
    \pause\item ... auf einem anderen Prozessorkern zu bearbeiten?
    \pause\item ... auf einem anderen Rechner im Netzwerk zu bearbeiten?
  \end{itemize}

  \vfill\pause

  Antwort: Für \code{filter} und \code{map} gar nichts!
  \begin{itemize}
    \item solange die Funktionen und Prädikate \stress{keine Nebeneffekte} haben
    \item nur das Ergebnis von \code{reduce} muss an einer zentralen Stelle landen
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{\book{} Vorteile von lazy evaluation: Klarheit}
  Aufgabe: Berechne die Gesamt-XP des AuD-Kurses

  \begin{block}{Students.csv}
    \vspace{-10pt}
    \begin{lstlisting}[basicstyle=\scriptsize\ttfamily]
      Firstname;Lastname;XP
      Harray;Putter;345
      Rom;Measly;93
      Lu(n)a;Lovelace;251
      Herdatamine;Greater;631
      ----
      RubyUs;Hackrid;254
      AdaBUS;Dumpyourcore;897
    \end{lstlisting}
    \vspace{-10pt}
  \end{block}

  \vfill\pause

  \begin{lstlisting}[language=java]
    int course_xp = Files.lines(
      Paths.get("res/Students.csv"),
      StandardCharsets.UTF_8
    )
      .skip(1)
      .map(l -> l.split(";"))
      .filter(ar -> ar.length == 3)
      .mapToInt(ar -> Integer.parseInt(ar[2]))
      .sum();
  \end{lstlisting}
\end{frame}

\begin{frame}{Bildquellen}
  \begin{center}
    \includegraphics[width=.2\textwidth]{img/nymph-2612952_640.jpg}
    \includegraphics[width=.2\textwidth]{img/stream_quelle.jpg}
    \includegraphics[width=.2\textwidth]{img/stream_map.jpg}
    \includegraphics[width=.2\textwidth]{img/stream_filter.jpg}
  \end{center}

  \begin{itemize}
    \item Emojis: Fxemoji, Mozilla Foundation, Lizenz: CC BY 4.0
    \item Titelbild: Annina Hofferberth
    \item Stream-Apparaturen: Julia Jelitzki
    \item Sonstige Bilder: Pixabay, Lizenz: Public Domain
      \begin{itemize}
        \item falls nicht anders angegeben
      \end{itemize}
  \end{itemize}
\end{frame}

\end{document}