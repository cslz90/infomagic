\def\fileversion{1.0}
\def\filedate{2017/7/04}

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{screxam-cs}[\filedate\space Version \fileversion\space by
  Christopher Schölzel]

% documentclass based on coma script article
\LoadClass[a4paper, parskip=full]{scrartcl}

\RequirePackage[automark, headsepline]{scrlayer-scrpage}
\RequirePackage[luatex, table, cmyk]{xcolor}
\RequirePackage{graphicx}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{polyglossia}
\RequirePackage{csquotes} % for \enquote
\RequirePackage{ulem} % for \uline
\RequirePackage[framemethod=TikZ]{mdframed} %for frames with rounded corners
\RequirePackage{tcolorbox} % for nice boxes
\tcbuselibrary{listings} % enable tcolorbox features for listings
\RequirePackage{multicol} %for multicols environment
\RequirePackage{listings} %listings environments with syntax higlighting
\RequirePackage[a4paper,inner=3cm,outer=3cm,top=3cm,bottom=3cm]{geometry} %to change geometry
\RequirePackage{pageslts} % for LastPage reference
\RequirePackage{caption} % allow line breaks in captions
\RequirePackage{titling} % for \theauthor command
\RequirePackage{lstautogobble} % for autogobble=true
\RequirePackage{fontspec} % for \setmonofont
\RequirePackage{siunitx} % for \micro\seconds
%\RequirePackage{etoolbox} % for \ifstrequal
\RequirePackage{xifthen} % for \ifthenelse
\RequirePackage[hidelinks]{hyperref}

\RequirePackage{uielements-cs} % for \checkbox, \input, etc.


\setmonofont{AnonymousPro.ttf}[BoldFont=AnonymousProB.ttf, ItalicFont=AnonymousProI.ttf]

\renewcommand{\emph}[1]{\textit{#1}}
\newcommand{\term}[1]{\textit{#1}}
\newcommand{\clue}[1]{Hinweis: \textit{#1}}
\newcommand{\stress}[1]{\textbf{#1}}

% TODO: move the following in a basic listing package
% ---- from here ----

% code with rounded borders from Herr K. on Stackoverflow
% Link: http://tex.stackexchange.com/a/140175 
\newcommand\code[2][]{%
  \strut\tikz[baseline=(s.base)]{
    \node(s)[
      rounded corners,
      thick,
      fill=black!5,       % background color
      draw=black!20,      % border of box
      text=black,         % text color
      inner xsep =3pt,    % horizontal space between text and border
      inner ysep =0pt,    % vertical space between text and border
      text height=2ex,    % height of box
      text depth =0.7ex,  % depth of box
      outer sep  =0pt,    % outer margin
      #1                  % other options
    ]{\lstinline|#2|};
  }%
}

\lstset{keepspaces=true}
\lstset{columns=flexible}
\lstset{inputencoding=utf8}
\lstset{autogobble}

% ---- to here ----

% class-specific listings settings
\lstset{basicstyle=\small\ttfamily}
\lstset{backgroundcolor=\color{black!5}}

% add vertical space to a tab
\newcommand{\tabvspace}[1]{\parbox[t][#1][t]{0cm}{}}

% command for section image that can be changed to indicate type of exercise
% TODO switch to new style:
%\newcommand{\secimg}[1]{\gdef\insertsecimg{#1}}
\newcommand{\secimg}{}

% defines own title format which starts with the text "Aufgabe # --" and contains an optional section image
% \begin{section*} can still be used for normal sections
\newcounter{totalpoints}
\newcounter{totaltime}
\newcommand{\secPoints}{0}
\newcommand{\secTime}{}
%\protecting{\ifthenelse{\secTime=0}{}{, \secTime min}}
\makeatletter
\let\old@section\section
\renewcommand{\section}[1]
  {
    % we cannot use \ifthenelse inside the section command
    % because it has "moving" argumens
    % see: https://tex.stackexchange.com/questions/4736/what-is-the-difference-between-fragile-and-robust-commands
    % normally \protect would work here, but since it does not
    % for some reason, we use ifthenelse before the section command
    \ifthenelse{\equal{\secTime}{}}
      {\def\time@str{}}
      {\def\time@str{, \secTime{} min}}
    \old@section{#1 (\secPoints{} Punkte\time@str)}
  }
\makeatother

\renewcommand{\sectionformat}{Aufgabe \thesection \hspace{1mm} -- }

\setkomafont{subsection}{\normalfont\normalsize\bfseries}

% TODO add global setting to hide or view solutions
% solution environment
\newenvironment{loesung}{
\begin{mdframed}[roundcorner=10pt,backgroundcolor=lightgreen]
\textcolor{red}{\bf Lösung}: 
}
{
\end{mdframed}
}

% clue environment
\newenvironment{hinweis}{
\begin{tcolorbox}[colback=black!5,colframe=black!20]
\textit{Hinweis:}
}
{
\end{tcolorbox}
}

\definecolor{THMgreen}{HTML}{80ba24}
\definecolor{lightblue}{rgb}{0.9,0.9,1}
\definecolor{lightgreen}{rgb}{0.9,1,0.9}
\definecolor{lightgray}{rgb}{0.95,0.95,0.95}

\definecolor{grey}{rgb}{0.4, 0.4, 0.4}
\definecolor{darkblue}{rgb}{0.43, 0.55, 0.85} % alter Farbwert, n
\definecolor{brightblue}{rgb}{0.16, 0.37, 1.0}
\definecolor{darkgreen}{rgb}{0.3, 0.62, 0.3}%  alter Farbwert, n
\definecolor{darkmagenta}{rgb}{0.68, 0.02, 0.58}


% sets logo
\newcommand{\logo}[1]{\gdef\insertlogo{#1}}
\logo{img/MNI-Logo.pdf}

% sets lecture name
\newcommand{\lecture}[1]{\gdef\thelecture{#1}}
\lecture{}


\setlength{\headheight}{41pt}
\pagestyle{scrheadings}
% TODO allow to set logo heiht
\ohead{\includegraphics[height=1cm]{\insertlogo}}
\chead{}
\ihead{\textbf{\thelecture}\\\theauthor}
\ifoot{\thepage / \pageref{LastPage}}
\cfoot{}
\ofoot{\thetitle}


% settings for tcolorbox package
\tcbset{colback=white,colframe=black!20}
\newtcblisting{specification}[1][]{%
  colback=black!5,colframe=black!20,listing only,%
  listing options={autogobble, #1}
}

\renewcommand{\maketitle}{{\Large\textbf{\thetitle}}}

% changes number of points displayed for exercise sections
\newcommand{\points}[1]{
\addtocounter{totalpoints}{#1}
\renewcommand{\secPoints}{#1}
}
% changes number of minutes displayed for exercise section
\newcommand{\timeEstimate}[2][]{
\ifthenelse{\isempty{#2}}
  {}
  {\addtocounter{totaltime}{#2}}
\ifthenelse{\isempty{#1}}
 {\renewcommand{\secTime}{#2}}
 {\renewcommand{\secTime}{#1}}
}

% criteria for grading
\newenvironment{criteria}
{\textbf{Bewertungskriterien:}\\\begin{tabular}{r l}}
{\end{tabular}}
\newcommand{\pointsFor}[2]{#1 & #2 \\}

% dotted lines for text areas
\newcommand{\lines}[1]{
  \vspace{\baselineskip}
  \begin{tikzpicture}
    \foreach \x in {1,...,#1} {
      \draw[line width=.5pt, loosely dotted] (0, {-(\x-1)*\baselineskip}) -- ++(\textwidth,0);
    }
  \end{tikzpicture}
}

\newcommand{\makeInputFields}[1]{%
  \begin{tabular}{p{.4\textwidth} p{.4\textwidth}}
  Nachname: & Punkte: \hspace{1cm}/#1\\[5mm]
  Vorname: & Note:\\[5mm]
  Matrikelnummer: \\
  \end{tabular}\vspace{7mm}
}

% make minipage restore the parskip length
% source: https://tex.stackexchange.com/a/141123
\makeatletter
\newcommand{\@minipagerestore}{\setlength{\parskip}{\medskipamount}}
\makeatother

% uses minipage to ensure that elements stay on same page
\newenvironment{nobreakarea}
{\begin{minipage}{\linewidth}}
{\end{minipage}

\vspace{2\parskip}
}
