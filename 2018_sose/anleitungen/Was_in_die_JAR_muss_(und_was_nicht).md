# Was in die JAR rein muss (und was nicht)

* In der JAR sollte, wie bereits in der Anleitung zum Erstellen einer JAR-Datei via IntelliJ beschrieben, alles was auch im src (source) Ordner des Projektes liegt enthalten sein. Also alle Klassen, Interfaces usw., die Ihr selbst geschrieben habt. Das heißt, sowohl die .class, als auch die .jave Dateien. Sollte für eine Abgabe mehr benötigt werden, wird dort explizit darauf hingewiesen.

* Ein kleines Beispiel zur Veranschaulichung:

    Es sei folgende Projektstruktur gegeben:

    [Projektstruktur](https://git.thm.de/cslz90/ad-cs/blob/dev/2018_ss/anleitungen/img/WasMussInDieJAR/WasMussInDieJarGraphic1.png)

    Dann sieht der Inhalt der .jar, nachdem wie im [Tutorial zum Erstellen einer JAR mit IntelliJ](https://dozentron.mni.thm.de/java_tester/pages/how_to_jar) vorgegangen wurde, so aus:

    [JAR_1](https://git.thm.de/cslz90/ad-cs/blob/dev/2018_ss/anleitungen/img/WasMussInDieJAR/WasMussInDieJARGraphic2.png)

    Da die Klasse des Projektes zu einem Paket gehört, sieht man hier zunächst nur den Paketordner.

    [JAR_2](https://git.thm.de/cslz90/ad-cs/blob/dev/2018_ss/anleitungen/img/WasMussInDieJAR/WasMussInDieJARGraphic3.png)

    Geht man eine Ebene tiefer, sieht man auch die .jave und die .class Dateien der Klasse HelloWorld.


* Was nicht in die Abgabedatei gehört, sind unter anderem JUnit-Tests, da sonst die Software, die im Code nach Ähnlichkeiten (Plagiaten) such, anschlägt.
