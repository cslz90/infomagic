# Checkliste: Aufgaben erstellen

## Aufgabe kreieren

* Das Thema ist bereits vorgegeben
   * bestensfalls ist die Aufgabe schon vorgegeben und muss nur ausgearbeitet werden
   * falls nicht, gibt es folgende Dinge zu überlegen bzw. tun:
      * Welche Fähigkeiten soll die Aufgabe vermitteln?
      * Welche Vorkenntnisse können vorausgesetzt werden?
      * Brainstorming zur Aufgabenidee
      * (ausführlicher ist es im [Aufgabentutorial](Aufgaben-Tutorial.md))
* Formulieren der Aufgabe (in Ordner `aufgaben/AB-"Nummer"`)
   * Gamifizierter Text: 
      * für Formatierung und Benennung siehe [Formatierungs-Tutorial](FormatierungAufgabenInGildamesh.md)
      * Platzhalter für Links lassen
   * Klartext:
      * für Formatierung und Benennung siehe [Formatierungs-Tutorial](FormatierungAufgabenInGildamesh.md)
      * Muss deutlich machen, was exakt von den Studis verlangt ist (bei Progammieraufgaben muss sie alles beinhalten, was man zum erfolgreichen Abgeben braucht, etwa vorgegebene Namen etc.)
   * evtl. Musterlösung & JUnit-Tests erstellen
      * in Ordner `aufgaben/code/"Arbeitsblattkennung"-"Thema"-"Aufgabentitel"`

## Aufgabe testen

* Texte untersuchen nach: 
   * Rechtschreibung & Grammatik
   * Korrektheit der Gamifizierungsmetaphern
   * Korrekte Formatierung (sodass Gildamesh keinen Murks macht)
   * Sie die zwei Texte so geschrieben, dass man sie jeweils einfach in die Tools copy-pasten kann?
   * Weiß der Student genau, was er zu tun hat?
* Musterlösung & Test:
   * Ist die Musterlösung korrekt?
   * Deckt der Test alle nötigen Testfälle ab?

## Aufgabe übertragen

* Übertrage die Klartextaufgabe in Dozentron
* Speichere den Link in der gamifizierten Aufgabe

* Teste: Lässt sich eine Lösung abgeben?
* Ist die Bewertung (wenn eine automatische Bewertung stattfindet) korrekt?
