##Reihenfolge Unit Tests:

Um eine sinnvolle (und zwischen Dozentron und lokalen Tests einheitliche) Reihenfoge von Unit Tests sicher zu stellen, sollen die Unit Tests für die Abgaben nach folgendem Schema erstellt werden.

  - [ ]  nötige imports:
  	```
  	import org.junit.FixMethodOrder;
	import org.junit.runners.MethodSorters;
  	```
  - [ ] Annotiere die Testklasse mit:
 `@FixMethodOrder(MethodSorters.NAME_ASCENDING)` 
 - [ ]  benenne die tests immer mit **t***-Testposition*-*Aufgabe des Tests*, also z.B. `t01Empty()`
 
 
 Bei Schwierigkeiten https://github.com/junit-team/junit4/wiki/test-execution-order
 