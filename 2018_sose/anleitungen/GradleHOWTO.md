# Quellcodeorganisation, -übersetzung, -paketierung

## Organisation

Quellcode möglicher Musterlösungen und Tests landet im `aufgaben/code` Ordner.
Dieser Ordner **ist ein** [Gradle](https://gradle.org/)-Projekt.
Er enthält verschiedene Subprojekte (für jedes Thema) in Unterordnern.

Das ist auch der Grund dafür, warum es hier keine weitere Trennung in die einzelnen Aufgabenblätter gibt.
Diese findet nur über die Benennung der Unterordner statt.

## Buildtool & Übersetzung

Der Quellcode ist als Gradle-Projekt organisiert.
Um ihn zu verwenden muss gradle installiert werden oder der [gradle wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html#sec:using_wrapper) wird verwendet.
Wird Gradle direkt verwendet, bitte im folgenden in allen Befehlen `gradlew` durch `gradle` ersetzen.
Der Wrapper kann über die vorhandenen Skripte (`gradlew`, `gradlw.bat`) verwendet werden.
Der Wrapper kümmert sich um die Installation von gradle in der richtigen version.
Zur ersten installation und einem Test, ob alles funktioniert, genügt es im Ordner `code` den Befehl `gradlew tasks` auszuführen.
Unter Linux muss man noch ein `./` vor die Befehle setzen, also z.B. `./gradlew tasks`.
Wenn du die Powershell unter Windows verwendest, muss es dagegen ein `.\` sein, also z.B. `.\gradlew tasks`.

Die **allgemeine** Builddefinition steht in der `build.gradle` Datei im `code` Ordner.
Diese **gilt für alle** Subprojekte.
Subprojekte werden in der `settings.gradle` Datei registriert
(siehe auch [gradle doc](https://docs.gradle.org/current/userguide/intro_multi_project_builds.html)).

## Gradle und die IDE

Die meisten IDEs sollten Gradle-Projekte verstehen können.
IntelliJ kann es auf jeden Fall.
Wichtig ist hier nur, dass man das **Hauptprojekt** in IntelliJ importiert (die Datei `gradle.build` im Ordner `code`) und nicht ein einzelnes Unterprojekt.
Wenn man IntelliJ dann noch sagt, dass es die Konfiguration einfach von Gradle übernehmen soll, sollte man sofort ein fertig eingerichtetes IntelliJ-Projekt haben, bei dem auch die Quell- und Test-Ordner schon korrekt eingetragen sind.

Mögliche Probleme, die trotzdem auftreten können, sind die folgenden:

* Ältere IntelliJ-Versionen (z.B. 2017.1) verstehen unsere komplizierte Gradle-Konfiguration nicht.
* Wenn man mehrere JDKs auf dem Rechner hat, kann es sein, dass IntelliJ sich das falsche aussucht. Unser Projekt braucht mindestens Java 9.
* Es kann sein, dass einzelne Tests sich nicht wie gewohnt per Rechtsklick auf die Datei ausführen lassen können.
    Im Moment weiß ich noch nicht woran das liegt, aber ein einfacher Workaround ist, stattdessen den Gradle-Befehl zum Ausführen der Tests zu verwenden.

Wenn das Gradle-Projekt einmal importiert ist sollte man aus der IDE Gradle-Befehle ausführen können.
Unter IntelliJ muss man dazu durch die folgenden Menüs navigieren: `View` -> `Tool Windows` -> `Gradle`

In dem nun geöffneten Menü findet man zum Beispiel unter `Projektname` -> `verification` -> `test` den Task zum ausführen der Unittests und unter `Projektname` -> `other` -> `Dozentron` den Task zum erstellen der für Dozentron nötigen *.jar-Archive*.

## Neue Projekte erstellen

Um ein neues Subprojekt zu erstellen (egal für welchen Aufgabentyp) gibt es einen gradle task:

```sh
gradlew newProject -Ppname=Projektname
```

Dieser erstellt einen neuen Unterordner und registriert diesen als Subprojekt.
Darin werden einige leere Ordner erzeugt, die je nach Typ des Projekts unterschiedliche Bedeutungen haben.
Mehr über die Idee hinter den jeweiligen Aufgabentypen erfährst du auch im [Dozentron-HOWTO](DozentronHOWTO.md).

### JUnit-Projekt

Ein Projekt für eine Unittest-Aufgabe hat die folgenden Struktur:

* In `src` gehört die Musterlösung. Hier steht der Code, der getestet werden soll.
* In `test` müssen jetzt Unittests definiert werden.
    Diese sind im wesentlichen normale Java-Klassen mit Methoden, die die Annotation `@Test` besitzen und Testmethoden aus der Klasse `org.junit.Assert` aufrufen.
    In unserem Fall ist beim Erstellen eines Unittest auch folgendes Schema zu beachten [ad-cs/2018_ss/anleitungen/TestReihenfolgeSchema.md](https://git.thm.de/cslz90/ad-cs/blob/master/2018_ss/anleitungen/TestReihenfolgeSchema.md)

    Das [offizielle JUnit-Benutzeranleitung](https://junit.org/junit5/docs/current/user-guide/#writing-tests) erklärt das Konzept sehr im Detail.
    Für einfache Tests reicht aber auch zum Beispiel das knappere [Vogella-Tutorial](http://www.vogella.com/tutorials/JUnit/article.html).
    Um die Installation musst du dir so oder so keine Gedanken machen, da im Gradle-Projekt schon automatisch die richtigen Abhängigkeiten definiert sind.
* Den Ordner `hiddenTest` brauchst du nur, wenn es zu der Aufgabe auch zusätzliche Tests geben soll, die die Studierenden sich nicht herunterladen können.
    Hier kannst du ebenfalls Unittests definieren, aber diese werden in ein anderes *.jar-Archiv* verpackt.
* Den Ordner `res` brauchst du, wenn die Studierenden für die Aufgabe eine Datei einlesen müssen.
    Hier kannst du beliebige Dateien ablegen, die automatisch von Gradle in die passenden *.jar-Archive* verpackt werden.

    Deine Tests müssen in diesem Fall außerdem die Basisklasse `AbstractFileIOTest` aus dem commons-Projekt verwenden. Dem Konstruktor dieser Klasse kannst du beliebig viele Dateipfade (mit beliebig tiefen Unterverzeichnissen) mitgeben. Diese Dateien werden automatisch vor jedem Test aus der JAR ins Arbeitsverzeichnis extrahiert und nach dem Test wieder gelöscht. Die Dateipfade sollten relativ zum Ordner `res` angegeben werden. Sollen die Studierenden also die Datei `res/test.txt` einlesen, musst du im Konstruktor deiner Testklasse `super("test.txt");` aufrufen und der Code der Studierenden und der Musterlösung kann auf die Datei über `Paths.get("test.txt")` zugreifen.

**Bitte beachten:** Alle .java-Dateien müssen UTF-8 kodiert sein.
Windows-Benutzer müssen besonders darauf achten.

Um die richtigen Dateien zum Hochladen auf Dozentron zu erhalten, kannst du einfach den folgenden Gradle-Befehl ausführen:

```shell
gradlew :AufgabenName:dozentron
```

Damit werden die folgenden Dateien generiert:

* `AufgabenName/build/libs/AufgabenName-tests.jar` enthält sichtbare Tests aus dem Ordner `test`.
* `AufgabenName/build/libs/AufgabenName-hidden-tests.jar` enthält unsichtbare Tests aus dem Ordner `hiddenTest`.
    Falls der Ordner leer ist, wird diese Datei gar nicht generiert.
* `AufgabenName/build/libs/AufgabenName-solution.jar` enthält die Musterlösung aus dem Ordner `src`.

Ein Beispiel für eine Unittest-Aufgabe findest du auch im Repository unter dem Namen `BSP-Unittest`.

### I/O-Projekt

Ein Projekt für eine I/O-Aufgabe hat die folgende Struktur:

* In `src` gehört der *Input-Generator*.
    Der Generator ist eine Klasse mit Main-Methode, die bei der Ausführung auf der Konsole mit `System.out` einen zufälligen Input erzeugt, den die Studierenden mit ihrem Programm in den korrekten Output umwandeln müssen.
* Den Ordner `test` wirst du vermutlich gar nicht brauchen.
    Tests für I/O-Aufgaben sollten normalerweise im Ordner `hiddenTest` definiert werden.
    Ansonsten gilt für `test` das gleiche wie für `hiddenTest`, nur dass hier wirklich nur Tests liegen sollten, die **vollständig für Studenten sichtbar** sein dürfen.
* In `hiddenTest` muss ein Unittest definiert werden.
    Dieser muss zuerst den Input einlesen, der dem Studierenden zugewiesen wurde, sowie den Output, den der Studierende als Lösung eingegeben hat.
    Dazu gibt es eine Basisklasse namens `de.thm.mni.aud.commons.AbstractInputOutputTest` im Commons-Projekt.

    Da jedes unserer Gradle-Projekte automatisch von Commons abhängt kann man diese Klasse einfach direkt importieren, ohne sich um die weitere Konfiguration zu kümmern.
    Vor dem Ausführen eines Tests liest diese Klasse besagten Input und Output zeilenweise ein und speichert die Zeilen in den zwei Listen `studentInputLines` und `studentOutputLines`.
    Du musst in deinen Testmethoden also nur noch auf diese Listen zugreifen.

    Für mehr Informationen zu Unittests kannst du die Beschreibung zu [JUnit-Aufgaben](#JUnit-Projekt) zu Rate ziehen.

    **Achtung:** Du wirst vermutlich eine Musterlösung schreiben müssen, um zu testen, ob der Student oder die Studentin die richtige Ausgabe produziert hat.
    Diese Musterlösung muss auch im Ordner `hiddenTest` liegen.
    Bei der Ausführung auf Dozentron haben die Klassen in `hiddenTest` keinen Zugriff mehr auf die Klassen in `src`.

    Ein paar weitere Hinweise zum Inhalt der Tests:

    * I/O-Aufgaben sollten mehrere Tests haben und nicht nur abprüfen, ob der Output exakt der Zeichenkette entspricht, die verlangt wird.
    * Insbesondere ist ein `assertEquals` mit dem korrekten Output zu vermeiden, da `assertEquals` in der Fehlermeldung anzeigen wird, was die korrekte Ausgabe gewesen wäre.
    * Besser ist es, den Test in mehrere kleine Tests aufzuteilen - entweder nach einer Struktur, die auch im Output zu finden ist, oder man verwendet erst ganz einfache Hinweise wie "Dein Output muss länger sein.".
    * Wichtig ist, dass jeder Test dem oder der Studierenden nicht nur sagt ob er oder sie richtig oder falsch liegt, sondern auch eine *Richtung* weist, in die der Output sich verändern muss.
* Den Ordner `res` kannst du verwenden, um deine Tests lokal ausführen zu können, ohne dass du die System-Properties `input` und `student-output` manuell setzen musst.
    Erstelle dazu einfach die beiden folgenden Dateien in `res`:

    * `exampleInput.txt` enthält einen Beispielinput, der an Stelle des für den Studenten oder die Studentin ausgewürfelten Inputs eingelesen wird.
    * `exampleOutput.txt` enthält einen Beispieloutput, der an Stelle des vom Student oder von der Studentin angegebenen Outputs eingelesen wird.

Um die richtigen Dateien zum Hochladen auf Dozentron zu erhalten, kannst du einfach den folgenden Gradle-Befehl ausführen:

```shell
gradlew :AufgabenName:dozentron -PmainClass=package.MyMainClass
```

Damit werden die folgenden Dateien generiert:

* `AufgabenName/build/libs/AufgabenName-tests.jar` enthält sichtbare Tests aus dem Ordner `test`. Falls der Ordner leer ist, wird diese Datei gar nicht generiert.
* `AufgabenName/build/libs/AufgabenName-hidden-tests.jar` enthält unsichtbare Tests aus dem Ordner `hiddenTest`.
* `AufgabenName/build/libs/AufgabenName-generator.jar` enthält den Input-Generator aus dem Ordner `src`.

Ein Beispiel für eine I/O-Aufgabe findest du auch im Repository unter dem Namen `BSP-io`.

## Befehlsreferenz

Alle Befehle werden wie folgt ausgeführt:

```sh
gradlew Befehl
```

Damit man allerdings nicht alle Subprojekte auf einmal ausführt kann man auch einen Projektnamen bei der Ausführung angeben (zu finden mit dem Befehl `gradlew projects`).

```sh
gradlew :MyProjectName:Befehl
```

| Befehl | Beschreibung |
|---|---|
| tasks --all | Listet alle verfügbaren Tasks mit Beschreibung |
| projects | Listet alle Subprojekte |
| clean | Löscht den `build` Ordner |
| compileJava | Kompiliert die `main` java Dateien |
| test | Führt alle Tests aus |
| test --tests TESTNAME | Führt den Test mit dem Namen `TESTNAME` aus.  `TESTNAME` ist ein *full-qualified-Testname* |
| testHidden | Führt alle Tests im Ordner `hiddenTest` aus.
| newProject -Ppname=Name | Erstellt ein neues Subprojekt mit dem Namen `Name` |
| packageSolution | Erstellt eine jar mit der Musterlösung und zugehörigen Tests. Diese ist passend für Dozentrons `Stub` bzw. `Input Generator`. |
| packageTests | Erstellt eine jar, die nur die Tests enthält. Diese ist passend für Dozentrons `Test`. |
| packageHiddenTests | Erstellt eine jar, die nur die Tests im Ordner `hiddenTest` enthält. Diese ist passend für Dozentrons `Hidden Test`. |
| dozentron | Führt `packageSolution` & `packageTests` & `packageHiddenTests` aus |
