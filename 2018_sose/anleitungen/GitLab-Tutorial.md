# GitLab-Tutorial

GitLab ist ein Projektmanagementsystem, das mit [Git](Git-Tutorial.md)-Repositories arbeitet.
Neben der Verwaltung vom Repository und der Nutzerrechte in diesem besteht das Hauptfeature darin, dass man Aufgabenpakete - "Issues" genannt - an Projektmitglieder zuweisen kann.

## Issues

Issues werden in anderen Systemen auch "Tickets" genannt.
Im Wesentlichen handelt es sich dabei um kleine Aufgabenpakete - einen Bug, der gefixt werden soll oder ein Feature, das es zu implementieren gilt.
Die wichtigsten Eigenschaften von Issues sind:

* Assignee: Das ist derjenige, der für das Issue zuständig ist. 
    Das darf (und sollte) immer nur eine Person sein, damit sich nicht zwei Leute gegenseitig die Verantwortung zuschieben können.
* Milestone: Zu welchem Meilenstein gehört das Issue?
    Meilensteine sind Projektabschnitte mit denen man Issues gruppieren und z.B. einer gemeinsamen Deadline zuweisen kann.
* Due date: Das ist die Deadline bis zu der das Issue erledigt sein muss.
* Labels: Das ist eine weitere Möglichkeit, um Issues zu gruppieren.
    Labels kann man einerseits verwenden, um Issues besser filtern zu können oder andererseits um einen Workflow darzustellen mit hilfe eines Boards (siehe unten).

*Tipp:* Issues können kleinere Unteraufgaben haben, indem man in die Beschreibung eine Liste mit Checkboxen mit dem Code `- [ ] Beschreibung der Unteraufgabe` einfügt.

### Grundregeln für unsere Issues

Bei uns gibt es folgende Grundregeln für den Umgang mit Issues:

* Es gab nie ein Issue geben, das keinen Assignee hat.
    Wenn der Assignee mit seiner Arbeit an dem Issue fertig ist, das Issue aber noch nicht geschlossen werden kann, muss er es jemand anderem zuweisen.
* Wenn im Slack oder auf anderen Seiten wichtige Entschlüsse für ein Issue gefasst werden, müssen diese noch einmal als Kommentar im Issue selbst erwähnt werden, damit auch andere nachlesen können, was sich getan hat.

### Templates in unseren Projekten

Für Issues kann man Templates definieren, die schon vorgefertigten Text in das Beschreibungsfeld eintragen. Bei uns gibt es die folgenden Templates:

* `ad-cs`-Templates
    * `Aufgabe kreiren` für `exercise creation`-Issues
    * `Aufgabe testen` für `exercise testing`-Issues
    * `Aufgabe korrigieren` für `exercise correction`-Issues
    * `Aufgabe übertragen` für `exercise transfer`-Issues
    * `Aufgabe probelösen` für `exercise test solving`-Issues
    * `Aufgabenkriterien bestimmen` für `exercise criteria`-Issues
* `Gildamesh`-Templates
    * `Bug_report` für das Melden von Bugs
* `dozentron`-Templates
    * `Bug_report` für das Melden von Bugs

## Mentions

Man kann mit `@username` benutzer innerhalb eines Issues oder eines Kommentars erwähnen.
Diese sogenannten "Mentions" führen dazu, dass der Benutzer (wenn er das in seinen Settings eingestellt hat), eine E-Mail bekommt und ganz oben rechts neben seinem Namen ein "TODO" sieht.
Außerdem ist jemand, der in einem Issue gementionet wird, automatisch ein Teilnehmer ("Participant") an diesem Issue.
Man kann in GitLab einstellen, ob man zu allen Issues E-Mails bekommen möchte oder nur zu denen, in denen man ein Teilnehmer ist.

Es ist also empfehlenswert, jemanden mit `@username` zu erwähnen, wenn man ihn oder sie auf das Issue aufmerksam machen möchte.
Es gibt auch ein `@all`, was alle Benutzer erwähnt, aber das ist in der Regel nicht zu empfehlen, weil selten ein Issue wirklich für ALLE Projektmitglieder relevant ist.

## Board

Das Board macht Issues noch ein wenig hilfreicher, da man damit einen Workflow oder eine Prioritätsverteilung darstellen kann.

Eigentlich ist ein Board nichts weiter, als eine Liste von Labeln, zu denen jeweils alle Issues in der entsprechenden Spalte angezeigt werden.
Der Vorteil besteht darin, dass man schnell einen Überblick erhält (Wie viele wichtige Issues gibt es noch? Woran wird gerade gearbeitet? Was ist fertig?) und sicherstellen kann, dass jedes Issue mehrere Schritte durchlaufen muss (z.B. ein `ready for review`, wie wir es verwenden).
Man kann Issues auf dem Board einfach per Drag&Drop verschieben.

Das Board findet sich im Menü auf der linken Seite unter dem Überpunkt "Issues".

### Board-Struktur in unseren Projekten

### ad-cs

Allgemein durchlaufen alle Issues drei Schritte:

* `todo`: Das Issue ist gerade frisch zugeteilt.
* `work in progress`: Der Zuständige für das Issue hat mit der Arbeit daran begonnen.
* `ready for review`: Der Zuständige ist fertig und übergibt das Issue an jemand anderen, der kurz noch einmal überpüft, ob wirklich alles erledigt ist.
    In diesem Schritt sollte auch der oder die Prüfende als Assignee eingetragen werden.

Dieser Workflow ist dazu da, Missverständnisse bei Issues zu vermeiden.
Gerade bei kleinen Issues kann es leicht sein, dass derjenige, der es übernimmt, etwas anderes darunter versteht, als derjenige, der es erstellt hat.

Für das Erstellen neuer Aufgaben gibt es einen zusätzlichen Workflow, der aber nicht im Board auftaucht, weil er nicht für alle Teams relevant ist:

* `exercise creation` ist die erste Phase, in der eine Aufgabe erstellt wird.
* `exercise testing` ist die Phase in der eine andere Person testet, ob die erstellte Aufgabe auch funktioniert.
* `exercise correction` ist die Phase in der eine Aufgabe überarbeitet wird.
* `exercise transfer` ist die Phase in der eine Aufgabe auf Dozentron und Gildamesh übertragen wird.
* `exercise test solving` ist die Phase in der die Aufgabe aus studentischer Sicht probegelöst wird.
* `exercise criteria` ist die Phase in der Bewertungskriterien für die Aufgabe festgelegt werden.

Zusätzlich zu den Workflows gibt es noch weitere Labels für die jeweiligen Teams:

* `team_content`
* `team_illustrationen`
* `team_korrekturen`
* `team_probelösung`
* `team_qs`
* `team_tutorium`

### Gildamesh

In Gildamesh ist es für die Entwickler wichtig zu wissen, welche der (in der Regel vielen :wink:) Issues gerade die wichtigsten sind.
Daher stellt das Board hier keinen Workflow dar, sondern eine Prioritätsverteilung:

* `P0 - immediate fix` sind die Issues, die vor allen anderen erledigt werden sollten.
    Gibt es ein solches Issue, sollte wenigstens ein Entwickler alles andere stehen und liegen lassen, bis dieses Issue erledigt wurde.
* `P1 - important` sind die Issues, die innerhalb der nächsten Wochen benötigt werden, oder eine sehr große Arbeitserleichterung bringen würden.
* `P2 - medium term` sind die Issues, die mittelfristig noch erledigt werden sollen.
    Hier ist es nicht so schlimm, wenn es länger dauert, aber es wäre schon schön, wenn sie in etwa einem Monat erledigt wären.
* `P3 - long term` sind die Issues, die auf lange Sicht wichtig sind, aber nicht unbedingt in diesem Semester fertig werden müssen.
* `P4 - optional` sind die Issues, die "nice to have" sind, aber die nur angefasst werden sollten, wenn sie extrem leicht zu erledigen sind oder wenn sonst gerade nichts Wichtiges ansteht.

Darüber hinaus hat `Gildamesh` auch ein `ready for review` - aus dem gleichen Grund wie bei `ad-cs`.

### dozentron

Das Projekt `dozentron` folgt dem gleichen Board-Schema wie `Gildamesh`.