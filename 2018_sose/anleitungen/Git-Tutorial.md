# Git-Tutorial

## Was ist Git?

[Git](https://git-scm.com/) ist ein dezentrales Versionsverwaltungstool. Man kann es einfach offline für private lokale Projekte verwenden, oder - wie wir - zusammen mit einem Server, über den dann mehrere Personen an einem Projekt arbeiten kann. Änderungen an einem Projekt werden in einzelnen kleinen *Commits* verfasst, so dass man zu jeder Zeit sehen kann wer was wann warum geändert hat. Außerdem ist es in Git sehr einfach, mehrere *Branches* eines Projektes zu haben, damit man experimentelle Features testen kann ohne die stabile Version zu zerstören bzw. unterschiedliche Teams an unterschiedlichen Teilen des Projekts arbeiten können, ohne sich gegenseitig zu stören.

## Umgang mit Git allgemein

Tutorials zu Git gibt es wie Sand am Meer. Spaßig finde ich zum Beispiel das interaktive [Try Git](https://try.github.io) oder für ein Bisschen mehr Erklärung was da eigentlich so abgeht diese [Visual Git Reference](http://marklodato.github.io/visual-git-guide/index-en.html).

Allgemeine Regeln, die ich persönlich immer für jedes Repository einhalte sind die folgenden:

* Comittet wird auf Englisch. Das ist zwar in einem deutschsprachigen Team seltsam, aber ich habe mir das angewohnt, da es meistens um (englischen) Code geht und es so einfach einheitlicher ist, als wenn man bei unterschiedlichen Projekten unterschiedliche Sprachen benutzt.
* Wenn möglich ist ein Commit eine (kleine!) gedankliche Einheit. Wenn man ein "und" in eine Commitnachricht schreiben muss, hat man in der Regel etwas falsch gemacht.
* Es ist OK, wenn die History eines Projektes hässlich ist. Vermeide `git rebase` (und insbesondere `git rebase -i`), wann immer es geht.

## Aufbau des Repos

Unser Repo ist wie folgt aufgebaut. Ich erkläre die Ordnerstruktur mal grob:

* `include` hier liegen die submodules. Was das ist, ist in der [README](../../README.md) erklärt.
* `scripts` hier liegen kleine Ruby- und Shellscripts, die einem die Arbeit erleichtern können.
* `jahr_ss` hier liegen die Dateien für das jeweilige Semester
    * `anleitungen` hier befinden sich Tutorials, wie dieses hier
    * `aufgaben` hier kommt alles zu Aufgaben hin
        * `ABX` Aufgabentexte, die den Themen von Aufgabenblatt X zugeordnet sind, jeweils in einem Ordner `ABX-AufgabentitelInCamelCase`
        * `code` das hier ist ein [Gradle-Projekt](GradleHOWTO.md) und daher getrennt von den Aufgabentexten

            Die einzelnen Unterprojekte in denen der Aufgabencode liegt heißen jeweils `ABX-AufgabentitelInCamelCase`.
  * `feedback` hier sammle ich Studentenfeedback aus den Kursen
  * `folien` hier befinden sich meine LaTeX-Folien
  * `klausur` hier ruht die Klausur, auch als LaTeX-Dokument
  * `konzept` hier finden sich Planungsdokumente, oft nur in Stichworten
  * `protokolle` alles was wir zusammen besprochen haben zum Nachlesen
  * `uebungen` hier finden sich Protokolle des Inhalts aus den Übungsstunden, die ich für die Studierenden halte

## Branches

Im Repository `ad-cs` gibt es im wesentlichen zwei Branches:

* `master` ist der Branch für den nur ich zuständig bin
    * Ich merge ab und zu (oder auf Wunsch) die Änderungen des `dev`-Branch in den `master`.
* `dev` ist der Tutoren-Branch
    * `git checkout dev` wechselt euer Repo auf den dev-Branch
    * Ihr dürft gerne jederzeit die Änderungen vom `master` in den `dev`-Branch mergen. Das sollte aber selten nötig sein.
        * `git merge origin/master` erledigt das
            * Hier kann es zu *CONFLICS* kommen. Den Fall handle ich nicht in diesem Tutorial ab, denn das lasst ihr euch beim ersten Mal lieber von jemandem helfen, der sich damit auskennt. (Ist aber kein Hexenwerk und man kann es sich auch anlesen.)
    * Um die neuesten Änderungen zu bekommen tut ihr folgendes:
        * `git remote update` lädt neue Commits vom Server
        * `git stash save` falls ihr noch nicht comittete lokale Änderungen habt
        * `git pull` mergt die Commits in `origin/dev` in euren lokalen `dev`-Branch`
            * Hier kann es zu *CONFLICTS* kommen. Wie oben erwähnt, lasst ihr euch beim ersten Mal lieber von jemandem helfen, der sich damit auskennt.
        * `git stash pop` falls ihr vorher `git stash save` ausgeführt habt
    * Um selbst eine lokale Änderung auf den Server zu übertragen macht ihr das folgende:
        * `git gui` um das grafische Tool aufzurufen (ich bevorzuge diesen Weg)
            * als Linux-User braucht man eventuell das Paket `git-gui`
            * in der GUI sieht man links oben die geänderten Dateien
            * ein Klick auf das Symbol links neben der Datei fügt die Änderungen darin dem Commit hinzu
            * alternativ kann man aber auch auf den Namen der Datei klicken und in dem Fenster rechts mit einer Mausmarkierung un deinem Rechtsklick einzelne Zeilen für den Commit auswählen
            * unten in der Mitte befindet sich ein Fenster für die Commitnachricht
            * mit einem Klick auf "commit" wird der Commit in das lokale Repository übernommen
            * den Befehl `git push` führe ich persönlich dann trotzdem auf der Konsole aus, um alle möglichen Fehlermeldungen zu sehen und darauf reagieren zu können
        * oder alternativ auf der Konsole
            * `git status` zeigt geänderte Dateien
            * `git add Dateiname` fügt Änderungen an der Datei dem nächsten Commit hinzu
            * `git commit -m "Nachricht"` erzeugt einen Commit mit den hinzugefügten Änderungen und der Commitnachricht "Nachricht"
            * `git push` lädt die Änderungen auf den Server hoch
                * Eventuell gibt es eine Fehlermeldung, wenn der Server auch neue Änderungen hat. Dann muss man erst diese Änderungen herunterladen wie oben beschrieben.
* Eigene Branches dürft ihr gerne erstellen, aber tut das lieber nur mit gutem Grund.

## Verknüpfung mit GitLab

Wie der name schon vermuten lässt, gehören Git und GitLab zusammen.
[GitLab](GitLab-Tutorial.md) ist ein Projektmanagementsystem, das mit Git-Repositories arbeitet.
Die Verknüpfung von beiden läuft im Wesentlichen über Issues und Benutzernamen, die man im Commit erwähnen kann und die dann automatisch auf GitLab verlinkt werden.

Ein Commit mit der Nachricht `fixes spelling errors related to #18` wird zum Beispiel in GitLab angezeigt, wenn man das Issue mit der Nummer 18 anschaut.
Es ist empfehlenswert, passende Issues immer zu erwähnen, da man dann sofort aus GitLab heraus sieht, wann zuletzt was bei diesem Issue getan wurde.