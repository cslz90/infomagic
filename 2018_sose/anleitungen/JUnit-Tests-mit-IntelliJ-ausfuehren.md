# Wie führt man einen JUnit-Test in IntelliJ aus?

![JUnit1](img/JUnit-Tests-via-IntelliJ/JUnit1.png)

Die JUnit-Tests zu den jeweiligen Aufgaben lassen sich auf Dozentron herunterladen. Man geht auf die Aufgabe und schaut ganz am Ender der Seite nach. Unten neben dem **Abgabe** Button gibt es einen weiteren, **Download Tests**, der diese als .zip-Datei zum Download bereit stellt.

![JUnit2](img/JUnit-Tests-via-IntelliJ/JUnit2.png)

Die Zip-Datei muss entpackt werden (zum Beispiel mit 7zip) und in der Ordnerstruktur finden sich die Testklassen. Diese heißen jeweils, wie die für eure Klassen vorgegebenen Namen plus 'Test'.

![JUnit3](img/JUnit-Tests-via-IntelliJ/JUnit3.png)

Als nächstes muss in dem IntelliJ Projekt ein neues Directory namens 'Test' mittels **Rechtsklick auf den Projektordner -> New -> Directory** erstellt werden.

![JUnit4](img/JUnit-Tests-via-IntelliJ/JUnit4.png)

Das neue Directory muss nun noch als Test Source festgelegt werden. Und zwar mittels **Rechtsklick auf die Directory -> Mark Direktory as -> Test Sources Root**.

![JUnit5](img/JUnit-Tests-via-IntelliJ/JUnit5.png)

Sollte der zu testende Code in einem Paket liegen, ist es wichtig im Test Directory ebenfalls ein Package mit dem selben Namen zu erstellen und die Testklassen dort plazieren.

![JUnit6](img/JUnit-Tests-via-IntelliJ/JUnit6.png)

Um die Testklasse(n) dem Projekt hinzuzufügen, sucht man im Dateiverzeichnis nach dem Projektordner und kopiert die heruntergeladene Testklasse mit der Endung.java in das gewünschte Paket im 'Test'-Orner des Projekts.

![JUnit7](img/JUnit-Tests-via-IntelliJ/JUnit7.png)

Um den Test ausführbar zu machen muss noch einen letzter Schritt unternommen werde. Die Annotationen, wie **@Test** sind in manchen Fällen noch rot markiert. Man bewegt den Cursor auf eine der Notationen betätigt das Tastenkürzel **Alt + Enter** und wählt in dem Popup die Option **Add 'JUnit4' to classpath**.
Das nächste Popup wird einfach nur mit **Ok** bestätigt.

![JUnit8](img/JUnit-Tests-via-IntelliJ/JUnit8.png)

Der JUnit-Test lässt sich nun starten und zwar mit einem **Rechtsklick auf das kleine Symbol links neben dem Namen der Testklasse -> Run 'NameDerTestklasse'**.

![JUnit9](img/JUnit-Tests-via-IntelliJ/JUnit9.png)

Nach dem Ausführen wird angezeigt, welche Tests bestanden wurden und welche nicht, inklusive Fehlermeldungen.
