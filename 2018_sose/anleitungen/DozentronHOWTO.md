# Wie erstelle ich eine Abgabe in Dozentron?

## Erstellen des Codes

Die Erklärung, wie der Quellcode für die einzelnen Aufgabentypen strukturiert sein muss, findest du im [Gradle-HOWTO](GradleHOWTO.md).

## Navigation zur richtigen Gruppe

1. [dozentron.mni.thm.de](dozentron.mni.thm.de) öffnen (Nur im THM Netz verfügbar oder durch das VPN der THM).

2. Auf der Home-Seite die entsprechende (Unter-)Gruppe auswählen
    * Falls noch keine passende Gruppe existiert, kann diese durch einen Klick auf die Übergruppe und den Eintrag "Neue Gruppe" im Menü erstellt werden.
3. Auf der linken Seite gibt es ein Menü mit mehreren Einträgen zum Erstellen neuer Aufgaben.

## Unittest-Aufgabe ("Neue Aufgabe")

### Erklärung der Aufgabengattung

Unittests bieten die Möglichkeit, Testfälle für kleine abgeschlossene Programmeinheiten zu definieren.
Unter Java ist das mit dem [JUnit-Framework](https://junit.org/junit5/) möglich.

**Bitte bachte**: Die Tests sollten dem folgenden Reihenfolgeschema entsprechen [ad-cs/2018_ss/anleitungen/TestReihenfolgeSchema.md](https://git.thm.de/cslz90/ad-cs/blob/master/2018_ss/anleitungen/TestReihenfolgeSchema.md)

In einer solchen Unittest-Aufgabe laden die Studierenden ihren Code als *.jar-Archiv* hoch und bekommen dann sofort angezeigt, welche Unittests funktionieren und welche fehlgeschlagen sind.
Auch die Fehlermeldungen können direkt über Dozentron angezeigt werden.

*Hinweis: Unittests können nie alles abdecken und können auch selbst Fehler enthalten.
Daher muss jede Abgabe eines Studierenden noch einmal von Hand von einem Tutor geprüft werden.*

### Hochladen auf Dozentron

1. Klick auf "Neue Aufgabe"

2. Titel, Text, Deadline etc. angeben

3. Code als *.jar-Archiv* hochladen:
    * *Test*: Hier müssen die Unittests als Bytecode (.class) und Quellcode (.java) enthalten sein.
        **Achtung:** Diese Datei können die Studierenden sich herunterladen.

        Dateiname bei Gradle-Projekten: `AufgabenName-tests.jar`

        Sollen Die Studierenden in der Augabe Dateien einlesen, muss die Testklasse selbst dafür sorgen, dass diese Dateien im Arbeitsverzeichnis angelegt werden.
        Die Inhalte der JAR-Datei werden nicht entpackt.

        **Achtung:** Dozentron erwartet, dass die Klassennamen der Tests das Wort `Test` enthält (mit großem "T").

    * Das Feld *Hidden Test* kann meistens leer bleiben. Hier kann ein *.jar-Archiv* hochgeladen werden, dass zusätzliche Tests (wieder mit Quell- und Bytecode) enthält, die die Studierenden nicht herunterladen können.

        Dateiname bei Gradle-Projekten: `AufgabenName-hidden-tests.jar`
    * *Stub*: Hier musst du die Musterlösung hochladen.
        Auch hier wird sowohl Bytecode als auch Quellcode benötigt.
        Die Studierenden können diese Datei nicht herunterladen.
        Sie dient nur dazu, zu prüfen, dass sich die Tests auch kompilieren lassen.

        Dateiname bei Gradle-Projekten: `AufgabenName-solution.jar`

4. Nachdem die Aufgabe erfolgreich in Dozentron eingefügt wurde:
    * Aufgabe in Gildamesh verlinken
    * Eventuelle Vorgaben ebenfalls in Gildamesh hochladen
        * **TODO:** Wir haben uns darauf geeinigt, dass Dozentron weitestgehend unabhängig von anderen Servern sein sollte, damit man Aufgaben auch noch später anschauen kann. D.h.: Auf lange Sicht braucht auch Dozentron eine Möglichkeit, Dateien hochzuladen.

## Input/Output-Aufgabe ("Neue Input/Output Aufgabe")

### Erklärung der Aufgabengattung

Die Idee für Input/Output-Aufgaben stammt ursprünglich von [Advent of Code](http://adventofcode.com/). Man bekommt eine Aufgabe gestellt, irgendeinen Input in einen Output umzuwandeln. Dazu bekommt man einen Beispielinput und den korrekten Output gezeigt. Für jeden Benutzer wird ein individueller Input generiert, sodass man nicht einfach die Antwort eines Kommilitonen kopieren kann. Zum Test wird allerdings wirklich nur der Konsolenoutput des Programms verwendet. Es findet keine automatische Überprüfung des Quellcodes oder des kompilierten Programms statt. Der Vorteil davon ist, dass diese Aufgaben völlig unabhängig von der verwendeten Sprache sind. Man kann sie mit Java lösen, mit Python, Ruby, Haskell oder Brainfuck - worauf man eben Lust hat.

*Hinweis: Es findet wie gesagt tatsächlich keinerlei Prüfung des Quellcodes der Studierenden statt. Es ist also unabdinglich, dass eine solche Aufgabe noch einmal von einem Tutor überprüft wird, bevor das Dozentron-Ergebnis als Bewertung verwendet werden kann.*

### Hochladen auf Dozentron

1. Auf "Neue Input/Output Aufgabe" klicken
2. Titel, Text, Deadline angeben (alle anderen Felder auf default belassen)
3. Code hochladen (ähnliche Aufteilung wie bei JUnit-Aufgabe)
    * *Test*: Unittests mit Quellcode (.java) und Bytecode (.class).
        **Achtung:** Diese Datei können die Studierenden sich herunterladen.

        Den Input, der dem Studierenden zugeordnet wurde, kann man im Code in der Systemeigenschaft `input` abrufen mit `System.getProperty("input")`.
        Den Output, den der Studierende hochgeladen hat, findet man in der Systemeigenschaft `student-output` mit `System.getProperty("output")`.

        Dateiname bei Gradle-Projekten: `AufgabenName-tests.jar`

        **Achtung:** Dozentron erwartet, dass die Klassennamen der Tests das Wort `Test` enthält (mit großem "T").

    * *Hidden Test*: Wie "Test", nur dass Studierende diese Datei nicht herunterladen können.
        Dieser Eintrag ist optional und kann meistens leer gelassen werden.

        Dateiname bei Gradle-Projekten: `AufgabenName-hidden-tests.jar`
    * *Input Generator*: Eine **ausführbare** *.jar-Datei*, deren Main-Methode einen Zufälligen Input (per `System.out`) erzeugt, der dann von Dozentron einem Studierenden zugeordnet wird.

        Dateiname bei Gradle-Projekten: `AufgabenName-generator.jar`

## JShell-Aufgabe ("Neue JShell Aufgabe")

**TODO:** Für AuD verwenden wir keine JShell-Aufgaben. Wenn dieses Tutorial aber über AuD hinaus verwendet wird, sollte hier noch eine Erklärung dieses Aufgabentyps eingefügt werden.
