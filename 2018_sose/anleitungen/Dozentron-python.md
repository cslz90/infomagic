# Python-Aufgaben in Dozentron erstellen

Aufgaben in Python 3.x müssen im wesentlichen den gleichen Standards wie den Java Aufgaben entsprechen.
Es gibt wieder einen Test, ein Stub und ein Hidden Test.
Hier müssen alle 3 Archive *zip* Dateien sein.
Momentan wird die Test-Bibliothek [unittest](https://docs.python.org/3/library/unittest.html) aus der Standard-Bibliothek verwendet.
(Wir unterstützen natürlich nur Python 3.x.)

Der Inhalt dieser Archive sollte dem folgenden entsprechen:

Für die Tests:
``` shell
- test_name1.py
- test_name2.py
- thm/
  - test_name1.py
  - test_name2.py
```

Für den Stub und die Abgaben:
``` shell
- person.py
- student.py
- graph.py
```

Dabei darauf achten, dass die Dateien **direkt** im Archiv liegen.
Ein *root* Ordner wird momentan nicht unterstützt.

*(WIP bessere Vorschläge erwünscht !)*
Um das ganze einfach zu generieren kann man wie folgt vorgehen:
Man erstelle 2 Ordner *src* und *test*, respektive für Quellcode (src) und Testcode (test).
Nun kann man mittels folgendem Befehl und dem erweitern des Python-Suchpfades die Tests lokal ausführen:
``` shell
cd test/
PYTHONPATH=../src python3 -m unittest
```

Die Archive kann man jetzt wie folgt erstellen:

``` shell
cd src/
zip -r src-code.zip "./*"
cd ../test/
zip -r test-code.zip "./*"
```

Beispielstuktur:
```
.
├── src
│   ├── de
│   │   ├── box.py
│   │   ├── __init__.py
└── test
    └── test_box.py
```
