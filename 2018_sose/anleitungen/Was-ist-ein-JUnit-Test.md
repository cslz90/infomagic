# Was ist ein JUnit-Test?

JUnit ist ein Testframework, dass dazu genutzt wird Java-Code zu testen. Es kann dazu verwendet werden bereits bestehenden Code zu testen oder aber auch zum automatisierten Testen beim Test-First-Ansatz des Programmierens.

Die Bibliothek **junit.jar** stellt alle wesentlichen Funktionalitäten bereit.

Die JUnit-Tests, die im Zusammenhang mit den Abgaben auf Dozentron zum Download zur Verfügung stehen enthalten Testklassen. Deren Testmethoden sind mit der JUnit-Annotation **@Test** gekennzeichnet. Mit Hilfe dieser Methoden werden verschiedene Testfälle simuliert und die Ergebnisse, die Euer Code liefert mit den erwarteten Ergebnissen abgeglichen. Gibt es keine Übereinstimmung, werden Fehlermeldungen ausgegeben, die anzeigen, was genau nicht wie gedacht funktioniert hat.

Wer sich noch genauer informieren möchte, kann das unter anderem in einem  [Tutorial zu JUnit-Tests der THM](https://homepages.thm.de/~hg11260/mat/junit.pdf), in einem  [Tutorial zu JUnittesting auf vogella](http://www.vogella.com/tutorials/JUnit/article.html#junittesting) oder auf [junit.org](https://junit.org/junit5/docs/current/user-guide/#writing-tests) tun.
