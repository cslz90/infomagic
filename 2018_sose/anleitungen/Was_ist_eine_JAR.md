# Was ist eine JAR?

Eine JAR ist eine Java Archive Datei, ein spezielles Format, das Klassen und Interfaces, falls gewünscht auch komprimiert, im Zip-Vormat bündelt und diese leichter übertragbar macht.
Außerdem ist der in JARs enthaltene Code direkt ausführbar, allerdings nur, wenn auch eine Main-Class in der Manifestdatei angegeben wurde. Wird zum Beispiel über IntelliJ eine JAR wie auf der [Hilfeseite von Dozentron](https://dozentron.mni.thm.de/java_tester/pages/how_to_jar) beschrieben erstellt, geschieht dies automatisch.


Wer sich näher zum Format und dessen Funktionalitäten informieren möchte, kann das unter anderem hier tun:
* [Java ist auch nur eine Insel](http://openbook.rheinwerk-verlag.de/javainsel9/javainsel_26_003.htm#mjf3bbdf106879ed579fef84173ca9be20)
* [Java Documentation von Oracle](https://docs.oracle.com/javase/tutorial/deployment/jar/index.html)
