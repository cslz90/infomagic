# Fehlermeldungen und Troubleshooting

## Die Fehlermeldungen

* > Dateityp muss jar sein

Diese Fehlermeldungen erklärt sich eigentlich von selbst.
Die hochgeladene Datei muss die Dateiendung .jar haben, sonst funktioniert es nicht.
Infomationen darüber, was eine JAR ist und Hilfe dazu, wie man mit IntelliJ eine JAR
 erstellt,  finden sich [hier](https://dozentron.mni.thm.de/java_tester/pages/how_to_jar).

* > Abgabe kann nicht compiliert werden

Diese Fehlermeldungen zeigt an, dass mit dem hochgeladenen Code etwas nicht stimm.
Ein Klick auf den Button **Details** zeigt an, welche Fehler der JUnit-Test geworfen hat
und liefert Anhaltspunkte, wo nochmal geschaut werden sollte.
*Cannot find symbol* kann zum Beispiel auf einen Schreibfehler im Namen von
Klassen oder Variablen hindeuten, falls diese vorgegeben waren.

* > Abgabe muss die Class Dateien enthalten

Diese Fehlermeldungen ist ebenfalls recht eindeutig.
Die hochgeladene JAR enthält keine .class-Dateien.
JARs lassen sich zum Beispiel mit der Archivverwaltung öffnen, damit der Inhalt
überprüft werden kann. Im zweifel kann im [Tutorial zu JARs](https://dozentron.mni.thm.de/java_tester/pages/how_to_jar)
nochmal überprüft werden, ob beim Erstellen der Datei alle Schritte richtig gemacht wurden
und ob der richtige Ordner rein gepackt wurde.

* >Abgabe muss die Java Source Dateien enthalten!

Diese Fehlermeldungen zeigt an, dass die .java Dateien in der JAR nicht enthalten sind. Der Inhalt der JAR lässt sich zum Beispiel mit der Archivverwaltung überpüfen. Eventuell ist es hilfreich sich das [Tutorial zu JARs](https://dozentron.mni.thm.de/java_tester/pages/how_to_jar) noch einmal anzusehen und zu prüfen, ob alle Schritte richtig befolgt wurden.

* >Datei ist Fehlerhaft! Struktur Validierung wurde mit Fehler beendet!

Diese Fehlermeldungen zeigt an, dass etwas sehr schief gelaufen ist. Am besten wäre es die JAR-Datei nochmal neu zu bauen. Ein [Tutorial zu JARs](https://dozentron.mni.thm.de/java_tester/pages/how_to_jar) gibt es auf der Hilfeseite von Dozentron. Hier ist Schritt für Schritt erklärt, wie es unter IntelliJ funktioniert.
