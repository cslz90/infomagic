# Formatierungsrichtlinie
## Warum?
 - Damit das Übertragen von Markdown in Gildamesh und Dozentron einfach und schnell geht.
## Dateinamen:
- Gamifizierter Text kommt in `"Arbeitsblattkennung"-"Thema"-"Aufgabentitel"-gami.md`
- Klartext kommt in `"Arbeitsblattkennung"-"Thema"-"Aufgabentitel"-klar.md`
- Kiterien für die Korrektur kommen in `"Arbeitsblattkennung"-"Thema"-"Aufgabentitel"-korr.md`
## Wie soll eine Aufgabe in Gildamesh formatiert sein:
 - gamifizierter und Klartext in getrennte Dateien.
 - gamifizierter Text kursiv: *ich bin gamifiziert*.
 - gamifizierter Text als Appetitmacher für Gildamesh
    - Der gamifizierte Text soll vor allem Spaß machen, aber auch inhaltlich in die Aufgabe einführen. Er darf zusätzliche Tipps enthalten, die man nicht auf Dozentron findet.
    - Eine Klartextvariante ist dann auf Dozentron zu finden (siehe unten). Zu dieser kann man mit ein paar kurzen gamifizierten Sätzen überleiten und dann den Link daruntersetzen.
- eine [kurze Einleitung für das Markdown-Format](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) findet man leicht online.
    - \`code\` ist erlaubt
    - \*kursiv* zum Worte hervorheben ist nicht erlaubt
      - *Das würde sich mit dem *kursiven* Gamification-Text beißen*
      - ***fett** kann man allerdings mit \*\*fett\*\* auch im kursiven Text markieren*
    - Überschriften
        - Titel h1 #
        - Aufgabentitel h2 ##
        - Unteraufgaben h3 ###
- **TODO:** Mittlerweile versteht Gildamesh auch [AsciiDoc](http://www.methods.co.nz/asciidoc/). Das ist bei Dozentron soweit ich weiß noch nicht der Fall. Auf lange sicht werden wir AsciiDoc bevorzugen, weil es mehr Formatierungsmöglichkeiten bietet und vor allem erlaubt, die Aufgabenblätter auch als PDF zur Verfügung zu stellen.

## Was ist für Aufgaben in Dozentron zu beachten?

- Wer nicht will, sollte den gamifizierten Text nicht lesen müssen
  - => Dozentron muss alle wichtigen Informationen zur Erledigung der Aufgabe enthalten
- Es kann sein, dass die Aufgaben auf Dozentron länger Bestand haben als der Moodle- oder Gildamesh-Kurs. Daher sollten idealerweise Links und Verweise nur Zusatzinformationen enthalten. Statt zu schreiben "wie auf Foliensatz 4, Seite 3" sollte die Info von diesem Foliensatz noch einmal in Kurzform zusammengefasst werden mit dem Zusatz "siehe dazu auch Foliensatz 4, Seite 3".
- Unteraufgaben sollten eine eindeutige Reihenfolge haben (konsistent zu Gildamesh)

# Generelle Blaupause

## Für Gildamesh

Titel: `Aufgabenblatt X`

in der Description:

```md
## Inhaltsverzeichnis
* Aufgabentitel
* "
* "

## Aufgabe 1: Aufgabentitel
### Aufgabe 1.1
*Ich bin ein kursiver, gamifizierter Text*

[Aufgabe in Dozentron](http://dozentron.mni.thm.de/submission_tasks/91)

### Aufgabe 1.2
*Ich bin ein kursiver, gamifizierter Text*

```

**TODO:** Gildamesh wird demnächst verschachtelte Aufgaben beherrschen. D.h. wir können einzelne kleine Dokumente pro Unteraufgabe schreiben (so wie im Repo) und müssen kein großes Gesamtdokument mehr erstellen.


## Für Dozentron:

```md
### Aufgabe 1.1
lalalalalala ich bin Klartext und komme in Dozentron und beinhalte vielleicht Hinweise und Tipps
### Aufgabe 1.2
lalalalalala
```